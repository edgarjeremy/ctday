-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 25, 2018 at 08:07 PM
-- Server version: 5.6.32-78.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jackiet_ctd2015`
--

-- --------------------------------------------------------

--
-- Table structure for table `core_settings`
--

CREATE TABLE IF NOT EXISTS `core_settings` (
  `slug` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `default` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Stores settings for the multi-site interface';

--
-- Dumping data for table `core_settings`
--

INSERT INTO `core_settings` (`slug`, `default`, `value`) VALUES
('date_format', 'g:ia -- m/d/y', 'g:ia -- m/d/y'),
('lang_direction', 'ltr', 'ltr'),
('status_message', 'This site has been disabled by a super-administrator.', 'This site has been disabled by a super-administrator.');

-- --------------------------------------------------------

--
-- Table structure for table `core_sites`
--

CREATE TABLE IF NOT EXISTS `core_sites` (
  `id` int(5) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ref` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_on` int(11) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `core_sites`
--

INSERT INTO `core_sites` (`id`, `name`, `ref`, `domain`, `active`, `created_on`, `updated_on`) VALUES
(1, 'Default Site', 'default', 'coraltriangleday.org', 1, 1430889571, 0);

-- --------------------------------------------------------

--
-- Table structure for table `core_users`
--

CREATE TABLE IF NOT EXISTS `core_users` (
  `id` smallint(5) unsigned NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salt` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Super User Information';

--
-- Dumping data for table `core_users`
--

INSERT INTO `core_users` (`id`, `email`, `password`, `salt`, `group_id`, `ip_address`, `active`, `activation_code`, `created_on`, `last_login`, `username`, `forgotten_password_code`, `remember_code`) VALUES
(1, 'okky.sari@gmail.com', 'e883f7f8cf88918ceac8f0a4364fc765d645365c', '4b9fc', 1, '', 1, '', 1430889571, 1430889571, 'osari', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_blog`
--

CREATE TABLE IF NOT EXISTS `default_blog` (
  `id` int(9) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `intro` longtext COLLATE utf8_unicode_ci,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `parsed` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `author_id` int(11) NOT NULL DEFAULT '0',
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL DEFAULT '0',
  `comments_enabled` enum('no','1 day','1 week','2 weeks','1 month','3 months','always') COLLATE utf8_unicode_ci NOT NULL DEFAULT '3 months',
  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `type` set('html','markdown','wysiwyg-advanced','wysiwyg-simple') COLLATE utf8_unicode_ci NOT NULL,
  `preview_hash` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_blog_categories`
--

CREATE TABLE IF NOT EXISTS `default_blog_categories` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `default_ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_ci_sessions`
--

INSERT INTO `default_ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('06adbbc3a9023c17c0c52965e74c4c22', '68.229.31.188', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521554351, ''),
('20e08fc5df68904f9485f3c6acf6e79d', '202.142.115.80', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521555001, ''),
('cda2986b50516a92326eb947822ec8b3', '49.144.137.37', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521555754, ''),
('33a6f2350f5eb747d2601c626e4f1a4d', '150.242.24.2', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521555895, ''),
('44aa511cf49aed653a8b99e3a6f726de', '216.164.55.96', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_1 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0 Mobile/15C', 1521556143, ''),
('cefe1fa37810bb365851a6751faaedd7', '5.255.250.159', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521557105, ''),
('684fd6366feee6d99ca5acac4417c5b1', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521557105, ''),
('c5d276cf3ba1d83fcd85a2a14f12a42c', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521557109, ''),
('838e0079c0494d4e3cac1bd3e7a2802e', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521557109, ''),
('04a1a583f3120c1206ef6e36f1406077', '173.29.245.10', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521558200, ''),
('ecdce710cc0d7f729c20e70249893076', '188.82.112.55', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521558320, ''),
('eb3ab3fb6ca408cf893dee0a57515e38', '36.75.192.181', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521558698, ''),
('7423740d8a8d4c4ad42d44b9929d922b', '125.161.131.190', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521559867, ''),
('fcaa38d4fa13153cd4f699569fc1d597', '45.65.57.104', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521627263, ''),
('515d729c3cf4e4c363543cefe234681e', '196.1.211.98', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521628882, ''),
('41b83dd6bbc0d2d67558da55baf0c404', '152.172.131.190', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521630490, ''),
('e47b41c739574f623412dfc6e67697c0', '203.186.175.42', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521633463, ''),
('9b3465770f8eca4767e5771f3b88e852', '178.33.39.197', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0', 1521633641, ''),
('7051a1547e96ef5c21d937c4f5fab3db', '192.99.69.242', 'Mozilla/5.0 (compatible; ExtLinksBot/1.5; +https://extlinks.com/Bot.html)', 1521634173, ''),
('a921049f89b8c87efaa19beeebaa3ec6', '192.99.69.242', 'Mozilla/5.0 (compatible; ExtLinksBot/1.5; +https://extlinks.com/Bot.html)', 1521634183, ''),
('696f7dec8451027e3bee64d4433aac16', '178.140.182.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521634435, ''),
('2bea9ccb76bca190facd5b5a95d6de47', '112.207.99.20', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521640199, ''),
('88791d390609a3c2ba8b8df90511201c', '122.2.10.48', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521640265, ''),
('fa8119e997a07a07d672b949f46094a6', '111.92.28.32', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521642775, ''),
('4d37f785881ccb601ffb8347fa6958c1', '117.253.51.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521643740, ''),
('12c454923bf331b46dcb8769ff7a0f92', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521644835, ''),
('1c53f9acc425b7908222ae9a0ab0a653', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521644836, ''),
('b8fdb4ab29a2bba5f2dba0cd0569aae1', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521644839, ''),
('ca82da458ef56429445ab8af3008515b', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521644839, ''),
('8ab281c0fcde4fc77fa98a4fc221b96b', '222.124.28.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521645632, ''),
('eeb4e6e527962c4e7d14f992a65737e1', '185.198.167.30', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0', 1521648079, ''),
('2aea43c79a7cf9d4986250a729aa72b7', '95.77.235.155', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521648267, ''),
('5050bd7abcc268082448497b7a099d5d', '202.163.68.216', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521648291, ''),
('054edc2b9524fff43c191385d63fa8ec', '60.254.74.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521648597, ''),
('cac5a9ef0bb0f59be407aeed65cda39f', '207.46.13.204', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521649037, ''),
('64235f3205e0c7ed7a9e80cf29ac83fb', '40.77.167.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521649044, ''),
('19b016b3c6315e1b2efadab73abe6a97', '40.77.167.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521649046, ''),
('d3a1d4f9e53db1910c6b72c0f629f90d', '40.77.167.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521649053, ''),
('57154092bc90b062ab81b48afda1317c', '40.77.167.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521649054, ''),
('ecb787ba553820cea8f50d1cc9a8392e', '40.77.167.30', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521649056, ''),
('8aa26e38f9b862767ead7dbce1d55d79', '77.153.153.190', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521650467, ''),
('0e8b29a38c7315d486e6ff6a657c5534', '94.71.8.173', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521651569, ''),
('700bd615c1412b04d96e702722eae55f', '77.44.206.23', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521653020, ''),
('ba2fe22fc70fe1d56758648dd249a266', '54.67.59.131', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.3', 1521653689, ''),
('49b450508dffaf7beab603ba5a32ae2e', '103.58.76.186', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521659000, ''),
('09a0bc27439103ddea6e1a5e57e0b15f', '112.207.226.158', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521661063, ''),
('93a669b65c3432e7f836d9b53627656b', '197.207.200.249', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521661272, ''),
('5bf99418e582521c6ab4a6dd4ec92301', '117.233.5.60', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521661602, ''),
('d2969a6de88b88745a7d9a3cc78272dd', '186.139.238.136', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521444693, ''),
('465c218bac859b07a6ce8cf337bc3ec2', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521436568, ''),
('2b13cb653d49436a117145b4d0ee4c01', '110.136.88.60', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521438310, ''),
('f1ee2b4bf609ad396d1170c7fb358f9d', '213.74.213.98', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521440225, ''),
('0d8289c56852761689cba1d904748b1b', '180.190.39.207', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521443952, ''),
('f666f0350f4c1d019f04f9873be2c8ea', '99.28.159.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521542566, ''),
('fe8515921a6394f4d0c9fcedbb261f9f', '76.85.66.207', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521542609, ''),
('9436d9401844d41248e085ccb3eb23d2', '115.188.172.46', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521543889, ''),
('87f5c34552c0dac9c8c8c8b00a789845', '37.26.4.157', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521546160, ''),
('59b2df3c9a8786633ad0bde56115b89b', '130.95.254.56', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521547575, ''),
('13a66de083c4e4b6d7d1df8bbb2a9d40', '66.249.73.71', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521548145, ''),
('2770ab2e322abd5a7a099597df855061', '182.253.163.49', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521591454, ''),
('4ad4dbc6dee346aba8bc04d91eb5e7b2', '49.213.48.207', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521593185, ''),
('28c7f3fe636cfbe33a78841d0fdd38ec', '83.44.87.19', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521593239, ''),
('7cd1b8181be73cfc5e5d18cf691e52c9', '46.253.39.108', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521594692, ''),
('8399f3e42a0baa6b710fbd599da4e905', '93.35.247.230', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521595219, ''),
('de3ce3fc8cc00782cf8a07ac5323adcd', '58.178.24.197', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521595499, ''),
('f9f2a493317fbdf17afcc327fbc540f7', '180.76.15.7', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521595884, ''),
('1d64586eca7a434cc9fd221130c311d0', '180.76.15.159', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521595885, ''),
('f73941aba7fb44ab62d80289d9dd366d', '191.177.187.131', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521597160, ''),
('2010c726466b6f19af47065f287b3500', '78.178.236.116', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521598375, ''),
('cb9936cac9adf400a7834aff349c304c', '38.122.227.170', '0', 1521599488, ''),
('70c4b894a4a9abc3dc985bdc4b66d5eb', '66.249.65.151', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521599733, ''),
('35f1f556f7739ae48b9902a40dcfde1f', '66.249.73.69', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521536455, ''),
('3b8864566bca44e5a1774d3db38c2741', '66.249.73.69', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521536456, ''),
('6bbc3fd8a3ee027ec8132e40b2730952', '113.161.38.118', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521538052, ''),
('49d2aa26dba806fc90f205bc3cc87936', '123.191.128.11', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143', 1521538955, ''),
('09de77306d16571baddd11e2cfdaaa80', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521539328, ''),
('e340c57007d1cfab580aba1f2c398572', '178.205.96.110', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521539696, ''),
('99864d592cf03b00be3dd4ff701bb598', '71.241.135.251', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521561892, ''),
('da5f83d9e8d152e97d0d21e46ead4c66', '171.61.215.132', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521531739, ''),
('6deb883c608104bf7c558b914c8b7d6c', '191.40.89.104', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521532928, ''),
('147fb1c1ade7916ebce174f3708bdaf8', '191.217.45.132', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521536205, ''),
('e22e38a280342569dae1ed680ff05658', '105.104.30.39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521536378, ''),
('fa42d1348f3babe98914ac891dda42b0', '66.249.73.69', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521536420, ''),
('221d17bdd9137003586161da4d398804', '122.179.26.137', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521662759, ''),
('98683cdef723c51a7581300dea10e486', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521665146, ''),
('fc7671f0577065844b6a357960fbe1a6', '41.250.83.182', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521665799, ''),
('7765ec4a54718b49c07c2c3828c60232', '177.206.102.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521667264, ''),
('c9a699875b70d2fda98da86b5fff6683', '157.37.236.30', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521668137, ''),
('0192fb6c56dc17a0248f17980b915dee', '176.12.115.198', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521668603, ''),
('496e1b4141ac460b3956d90fb3994f5c', '120.57.14.141', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521669518, ''),
('f84c788409cb6aeb9bc87c5137c6aea2', '41.238.91.144', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521670555, ''),
('a05bd0a546a8b551b882144911c4ffd3', '81.147.143.152', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 1521672018, ''),
('0e2ff12385580a1efeeb0a2fff123698', '66.249.65.153', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521672436, ''),
('be56d9e365faa181853401060a59a4b1', '122.165.148.135', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521562481, ''),
('58cf3764610d40189eebf6127c5d0abb', '105.156.235.86', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521564649, ''),
('b7524d011de6b98175ee8107c21c5a2c', '49.146.135.76', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521565770, ''),
('a677d32f0861877d5fd02d3b1cd41333', '54.187.240.231', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1; http://spyonweb.com', 1521569116, ''),
('da95b1e2448a2a2ef83d6927658c4405', '54.187.240.231', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1; http://spyonweb.com', 1521569116, ''),
('7779d2e6db4d5c8a4b9861e2d09aec65', '175.107.40.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521570041, ''),
('69af98d27f43f4acec4afb36b0e36042', '221.146.168.118', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521573106, ''),
('50e5232f69c9978a88c31b59af0bc296', '95.138.74.45', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521573545, ''),
('224e7e6a04cdb400958746e7266b2ebd', '31.210.152.163', 'Mozilla/5.0 (Windows NT 5.1; rv:32.0) Gecko/20100101 Firefox/32.0', 1521574703, ''),
('ab5cc879be1dcb01f43d69d0b3325cca', '66.249.73.91', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521575970, ''),
('e2cec3091be733f480edba68d81c8ecb', '180.190.47.178', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521707313, ''),
('1d9387f12b9f7ea0979cec47a06ae6ee', '54.187.23.9', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 1521707567, ''),
('fdcfc7e6f22bde3fc49fbf7f8c7b938f', '36.81.202.248', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521709785, ''),
('15550642c58a11843c9b89ec04c878e9', '110.54.244.254', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521710159, ''),
('d5fe557ed67a3492482ac441a2681568', '181.90.120.84', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521710960, ''),
('a1d1a8fc2f6c8fd13ffb1c528dd58768', '199.16.157.183', 'Twitterbot/1.0', 1521711794, ''),
('5ef24cf564a90e7c1a20685d25c68cd7', '130.105.245.77', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521712000, ''),
('ef9f9fcd49e13299d15e24f405aeb7f8', '122.53.75.226', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521712670, ''),
('f1ccbbec6d874f23eb19d334e7d35ba0', '103.251.212.188', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521714780, ''),
('ddf3654f7eee05c58cc865f4fcfb24b9', '123.18.116.124', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521714818, ''),
('863ee41be3a532f5c955db2cf82beed1', '178.60.18.73', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521715446, ''),
('890ed5b7e71d24d03c88099cb90ab3d3', '103.106.139.108', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521716502, ''),
('a101030dce6fd1a0961cb6daaeb9e648', '117.58.241.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521716813, ''),
('d85d84b44b0cb9f42367885c357e12fa', '103.225.225.217', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521717110, ''),
('099c4a7d374ce5c666f3c84aaf1898bc', '49.151.184.35', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521717359, ''),
('f2955ba33459a8866f13452620d63675', '8.28.16.254', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; InfoPath.1; .NET CLR 2.0.50727; .NET CLR 1.1.4322; MS-RTC LM 8; .NET ', 1521717479, ''),
('d18687ae1d3641c3430370c0f1fb2817', '158.109.94.211', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521722584, ''),
('4566353e63cdc6bd786ff3b3f3161b5b', '183.82.34.193', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521722647, ''),
('956bb9ae0e41a3c5d1aafc82bf64a8aa', '103.231.216.246', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521723170, ''),
('e3c61fcb0541d6e19e51b39081024962', '190.43.13.16', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521724252, ''),
('e843d1d53869d59dbf4017af4179b5b7', '103.197.133.41', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521725870, ''),
('24bb6d404bf0433d82ef56f46739cb08', '80.62.116.197', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521725948, ''),
('a891d229775c97cc4651f9973b8d5475', '182.186.212.66', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521726158, ''),
('9c4d83957885aaec560ec67673cd7527', '180.76.15.28', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521726247, ''),
('40fe4c7508702dc634d1c0d17e1d35cc', '180.76.15.26', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521726247, ''),
('f53c7339699c85cd55670e88edeee88b', '115.132.45.186', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521727160, ''),
('3d774a3f037c31c75c79a89dd27f6c9a', '89.201.186.200', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521729225, ''),
('137137bdbfcbeb488a6deddd46fd74ab', '168.194.108.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521729295, ''),
('2e25a083b0cd23cb329a5709f5eb87d6', '93.137.121.2', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521729339, ''),
('ff54263c89c9f5cb54168779e18cda1d', '36.72.178.127', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.3', 1521729824, ''),
('6eab8ca02d21582ba6241940ebd8912f', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521730389, ''),
('8970d42d1e55e1677b2eb31d08c27c63', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521730389, ''),
('8017685b19702d5cd50edde9e2a9b1e8', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521730392, ''),
('4b7cb63817e8bc09f0d337b3b1f27516', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521730393, ''),
('284c23561faeab4d3deb55a4d52d8388', '180.191.142.88', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521731495, ''),
('6749728b9f74da4b6d998e560042bfec', '94.175.143.109', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521732813, ''),
('1768793fdd53307d5abd94da3da82c1c', '200.29.120.244', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521736069, ''),
('a2a40165091ba48335c96e93fd91ae22', '88.247.33.195', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521737619, ''),
('67fb8944cf6daeee8a2c19ed62b566c6', '115.186.191.73', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521739754, ''),
('01a18b70a5ef4a4379a75dd0005944bd', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521742129, ''),
('308fc88997e01a3e5b1977595348551a', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521672717, ''),
('0791a164b2b32504a326817dc63791d3', '152.250.133.211', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521487056, ''),
('22008329e137daf60d3f3abc1e6b9d42', '188.24.14.92', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521488620, ''),
('b0e57cf22e30f6bdb590c45837e81f5b', '54.183.165.74', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.3', 1521489268, ''),
('18b7e186b2762af66624f5445f05298f', '1.55.102.230', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521489495, ''),
('6101f2e7a3db62874b75d2657ff16b8a', '190.251.141.119', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521490668, ''),
('3d63d404874e03a99bdd041e151aa319', '94.199.151.22', 'Wotbox/2.01 (+http://www.wotbox.com/bot/)', 1521491128, ''),
('3a8c33d1808d3afa0ba70997cf539132', '94.199.151.22', 'Wotbox/2.01 (+http://www.wotbox.com/bot/)', 1521491140, ''),
('73119374b4912c1d1d600c5a5da3547c', '93.40.184.127', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521491905, ''),
('18be5af6540ad794e80df051f98a3d67', '35.166.39.85', 'python-requests/2.18.4', 1521492516, ''),
('46b392eb5998b3079e88487ed732ebe7', '124.41.240.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521494588, ''),
('3732110fb4a1e6e91c7e1afeacee43fc', '113.59.209.231', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521496744, ''),
('091d286f2d3f5d39c70ac0dd5a699f6c', '86.121.208.104', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521497348, ''),
('2573f5f48c4283053527e2ed80b9698e', '46.99.125.62', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521500096, ''),
('00f46757e2641777b7605ae9c729a583', '180.76.15.158', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2', 1521500866, ''),
('211dc58d6924bf54cf2dd7355dde5453', '180.76.15.25', 'Mozilla/5.0 (Windows NT 5.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2', 1521500866, ''),
('e5f47f3341fd2120696d13d1a59eccac', '189.76.131.145', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521500936, ''),
('bdebd34b6de6369e1ff765b3aa699466', '207.102.138.158', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.101 Safari/537.36', 1521501827, ''),
('75d717019963d79f66f78f2267d93bde', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521504169, ''),
('6935417b6288b813001f76f73feb22f7', '151.51.112.51', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521507315, ''),
('c2bad6dd5975d966772bb8bc0577a7e5', '41.250.251.181', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521512043, ''),
('20e116307044a439b5bb8bbe7213683b', '180.76.15.15', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521514247, ''),
('b6af5afb33ab926038f61e2f9b52e319', '180.87.201.112', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521515370, ''),
('386c2dda021f69bf23d5b00c31f9c7b7', '159.203.196.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', 1521515370, ''),
('13958d4231361dd88512648653048e29', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521515586, ''),
('fcd42f71cb349f5851e6bec33073a698', '186.108.200.169', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521517755, ''),
('5dffd6c77773ec0ee535b18497685427', '40.77.167.141', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521518460, ''),
('790f1defbbc2b25e3c5229595feb90df', '186.241.247.226', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521519438, ''),
('3a0ce795a1cee2b11b590d0e5232b1e5', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521519539, ''),
('9fa71ffd02e8c4618ca7703fe9be04a5', '180.76.15.32', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521520248, ''),
('8ab8696d654b66491ab43046726ef823', '180.76.15.6', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521520249, ''),
('90687af5af6edd997bb446dca42e1676', '120.154.29.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521521211, ''),
('5915a009dd3930ea6e568e391e2f367d', '84.236.186.96', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521521951, ''),
('77c01e6cd7d77012e584c75a58c18b9a', '177.206.179.255', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521522594, ''),
('84128e78fbfacf9962b120bd23a4e04c', '49.145.24.177', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521525719, ''),
('a6592f8185c8cf042dd281b91087e2f5', '84.111.22.24', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521526291, ''),
('dc49483199e1e4814a23b98aa3d5f4e6', '86.121.78.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521527051, ''),
('6ca66073b9df2f6e0a96e3a21b89b165', '66.249.65.143', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521527939, ''),
('43f53ca52b44280f6b8c16114705f464', '49.146.11.18', 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko', 1521697900, ''),
('d6c90c1965da74a11e2f024242ebb214', '66.249.73.93', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521575970, ''),
('cacc0e3d55edc32f78d5893225c8a20d', '109.95.151.80', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521581896, ''),
('f8a26d818d109c1dfb83785c67db4533', '138.121.152.167', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521577657, ''),
('a1ec5569726342d4ac73ba703201a035', '200.199.183.217', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521578207, ''),
('85453edae69a00984c45e30c5af0ad7c', '222.232.61.220', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_6 like Mac OS X) AppleWebKit/604.5.6 (KHTML, like Gecko) Mobile/15D100 [FBAN/FBI', 1521580846, ''),
('bd8bb877de8dd9f02b8e36dd68eca076', '45.127.138.227', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521581325, ''),
('76625daa396dd2d0c3870aca38cd70c6', '182.69.244.210', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521468310, ''),
('90db56273165e8637ec0fb820b2cfe00', '27.255.172.145', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521465456, ''),
('905f7c8b25f8f22183792b2b50af5a82', '92.97.29.156', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521467006, ''),
('c2bfe5e6ddfb80be52d910e133f705e0', '106.241.9.6', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 1521464399, ''),
('259791a75e0b16a94029e364291d4f28', '180.76.15.5', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521463876, ''),
('bc018f5706edfb92a854ae667b52a27a', '120.89.76.250', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521463401, ''),
('700e4ce90a000ec623cccfe227a051a3', '36.84.226.231', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', 1521461743, ''),
('d7c3b50378356297a05c5381ee5036bc', '36.84.226.231', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0', 1521461437, ''),
('d0e50b2371531e97fd70cdc6a3eec880', '38.142.192.34', '0', 1521456162, ''),
('2631be85535217d930590b228c7823de', '66.249.65.153', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521456315, ''),
('473b15d776631d24678e6f43e7bab22b', '179.126.105.44', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521455552, ''),
('a3bd54072c708624887d4f73df6db0a2', '197.246.238.146', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521455525, ''),
('49dfa1e4cac6d9a8f9ddc6dcf3c32786', '66.249.65.153', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521456315, ''),
('e5d3911015b37d2b65bf3a7e3bb84b54', '88.230.134.117', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521434124, ''),
('9d0b6438a570ad0bec7688cc4b0457a7', '139.194.76.121', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521486723, ''),
('6b516a184ea85b76f6db3dd29973c8bc', '111.94.61.140', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521486468, ''),
('eee13d630e5666ac8eeb92288d40f803', '81.82.135.143', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521484857, ''),
('0fef01e4f1185059687cc3a01737cfb0', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521482148, ''),
('4ecd8738821fac39c1e28d3eb169d179', '112.200.198.107', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521481824, ''),
('0ef3553f9f28078733d798238c125f20', '66.249.73.93', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521479556, ''),
('80aab837e018758ff7149fa8edda954f', '66.249.65.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521478325, ''),
('b426383e034bf449b294d0866a354ffc', '66.249.73.93', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521479556, ''),
('5ba2b9fe7a259b184590901c2cd91ed8', '87.118.158.247', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521480104, ''),
('a3c44d5b0844c982fd0f8d65f02a6210', '66.249.65.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521478325, ''),
('ca2d7a702ef259ac019fc8e84d16f34a', '180.76.15.137', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521473718, ''),
('c7cc5d5ecac4f2a3e118d149030ae316', '76.69.133.163', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521471041, ''),
('395c04db82f0b0f326a24c56590259c3', '78.15.180.214', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521468928, ''),
('382f07ed63611d21f9a882741c9716a7', '106.208.29.233', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521470893, ''),
('7d9c278f2826bb59c40f12aff10412c6', '112.206.164.175', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521625775, ''),
('37911f219a071391aa51bf0ec8deae00', '154.127.236.203', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521624831, ''),
('c4270d796ecfc2526cf9ab5cde6943ad', '212.83.171.240', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36', 1521622962, ''),
('378cb023774c980f42eee8b40feb08e2', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521621386, ''),
('c4cff4d75c545e9e0978793b49f53168', '179.180.201.80', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521620658, ''),
('768a0d9a9a98ea0bc3e5f488e89a6ad2', '194.44.247.218', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1521620187, ''),
('fe5cb78dad432084f2042652d864d44b', '36.82.80.91', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521619512, ''),
('d86219fffd74fc585baaed58eaaf9b7e', '110.139.89.174', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521619438, ''),
('7b7d7ce404b6186f764186566d70c040', '189.126.238.56', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521618018, ''),
('142de00966c2732f2262a577505f212f', '201.139.175.230', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521617601, ''),
('9bb3524080c5373c7d5cde41fc8bf6cd', '87.116.178.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521617028, ''),
('87c4e53c50193a3f18be38b4d72f2f7a', '180.178.74.35', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 1521615765, ''),
('36a77fd3cd7ebd0ad15c1920f1434597', '59.97.21.37', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521614916, ''),
('5894f6edf5506259bd7bfb54c435e2e0', '203.78.117.219', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521610616, ''),
('2828ab050626054b2a71ca7d00732016', '77.69.155.108', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521610598, ''),
('b02490967dec73d95bb72bea99397da3', '87.116.178.102', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521610331, ''),
('e00fbf965eed695b02deb713ff8db0ce', '109.95.151.80', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521606556, ''),
('2af991b08228ae779fa7a62ecc66d6f4', '38.142.99.42', '0', 1521605740, ''),
('80a4f01445779e3b14f5d3505009de6a', '191.100.30.151', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521606402, ''),
('59e116d04d3afd7d3203d10ac7a3c124', '180.191.87.180', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521602961, ''),
('6fa171110c326a726a066da19629af0b', '197.3.137.34', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521602426, ''),
('9548733b6f800cded6d08bcba92e6977', '37.115.112.228', 'Mozilla/4.0 (compatible; Powermarks/3.5; Windows 95/98/2000/NT)', 1521599788, ''),
('330e945b2758612de9bf150af0d01dd1', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521599775, ''),
('6f7ae25a707db1879d1f404f81f5cb77', '66.249.65.143', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521599775, ''),
('b7761d27d7804c5dd4587d937962e0d3', '66.249.65.151', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521599733, ''),
('d393ca22a8affb1d18043a11b73f5ed2', '38.142.98.194', '0', 1521744873, ''),
('e5e7ba2f6207e9386ee862e927042a3d', '2.219.79.84', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521746303, ''),
('9901d6645e3689cb04236d6897fe25ec', '52.165.148.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1521746975, ''),
('9a1cec525ec2b93f23e1145af187bcb5', '160.238.73.204', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521747282, ''),
('f3217c57a84571110f571d9a44a16a40', '109.172.199.211', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521748600, ''),
('cea699ec9950b1d2d885da3825a926ac', '180.76.15.18', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521750242, ''),
('ff31db762e1ea0b3ccb6e4725e3336fd', '37.115.112.228', 'Opera/7.54 (Windows NT 5.1; U)  [pl]', 1521751219, ''),
('656791b4f3b52bb755414d8eab245124', '37.115.112.228', 'Opera/7.54 (Windows NT 5.1; U)  [pl]', 1521751219, ''),
('1d96d646675592fa19d128e7ed123d4e', '37.115.112.228', 'Opera/7.54 (Windows NT 5.1; U)  [pl]', 1521751220, ''),
('57bb991a93346c8f6d94e63d55107039', '124.13.176.236', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521752476, ''),
('1f0a4068bdf88c4c3c7ea015275eddd7', '181.191.194.102', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521753577, ''),
('9a993262833a1dbc48c676774185e2f4', '93.36.191.95', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521755576, ''),
('78189c9236a33885e1a1cb922a10bd0f', '46.233.77.65', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.3', 1521757913, ''),
('973c29280f0285d9bb2a77c95b4f12b3', '105.225.123.220', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521759188, ''),
('c6652eef98d4e36ecd0d86cb5a2f11f4', '207.46.13.32', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521759441, ''),
('518bcdc0bff17dd0d294bef6bc36f5b0', '207.46.13.32', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521759444, ''),
('095943dcb784baebaf03f04fa16b6441', '178.149.87.119', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521759476, ''),
('c28bf0af517503017d33d3fe29e44312', '86.122.89.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521760137, ''),
('e64574cb620cf3efb63e3ac10abf0577', '187.15.124.31', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521762777, ''),
('7bed02d9ab77cd7b2fac1a2ed141d41a', '62.85.70.205', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521763437, ''),
('af0b4edeb00b22837a008999f6ab230c', '84.122.213.92', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521765367, ''),
('d09ba4793086bab8b186d521cab0f6f1', '182.253.163.2', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521767030, ''),
('790142df4ca5a2bb8fef5a99dfac3a58', '38.142.99.18', '0', 1521770037, ''),
('48a2f1b824298e95fa7f99c05c278109', '112.223.119.229', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521772592, ''),
('dc88417529270abed44be44299cd7971', '137.226.113.26', 'Mozilla/5.0 zgrab/0.x (compatible; Researchscan/t13rl; +http://researchscan.comsys.rwth-aachen.de)', 1521772599, ''),
('e86df5cfd727c5f4654cb4dc206b20a2', '202.67.34.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521775086, ''),
('7849412dc5a8313487f790d8dc5d2095', '178.166.59.104', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521775237, ''),
('8a720a6ef7e46d10e9be02fa8732c563', '103.21.231.2', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 1521775457, ''),
('6bf1d51a399e53aa3505d9a263a50ba0', '112.223.119.229', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521775479, ''),
('3b07b56fd29569e86eff8ca7fd4983d3', '82.236.126.27', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521775894, ''),
('7f187e35c71aaf900a72fc808bf627fb', '137.226.113.28', 'Mozilla/5.0 zgrab/0.x (compatible; Researchscan/t12ca; +http://researchscan.comsys.rwth-aachen.de)', 1521775949, ''),
('77543df9f0ba75b27f81f93f71ab8e20', '202.1.174.191', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 1521798201, ''),
('f121ffb8e84242ee5bc12af8d05d03ef', '213.55.107.223', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521776252, ''),
('f8fd3d097d88700b71c1adb9c623ba19', '37.216.218.118', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521777240, ''),
('ef8696454e48f8a3b0f0dcdd1b89ef9c', '41.96.175.71', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521778233, ''),
('5eb9f9019e0eefbf4316956ae819fcac', '41.234.202.55', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521780047, ''),
('9639970cfc1570844711d22e3102c5fe', '138.246.253.19', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.85 Safari/537.36', 1521780818, ''),
('2a7f08e526e5bb5f64e94b5a41dfe054', '177.1.172.192', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521781030, ''),
('6cbb9cc995545b63cc414e9de8f696dd', '137.226.113.26', 'Mozilla/5.0 zgrab/0.x (compatible; Researchscan/t12sns; +http://researchscan.comsys.rwth-aachen.de)', 1521781576, ''),
('2aef9301a93e52f90ea28de6658428d1', '38.104.1.186', '0', 1521781584, ''),
('6d7debccca86eb45479c6c8366d81872', '83.79.12.221', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521784510, ''),
('fb9150eeed0380a8f24e705bd7fd6565', '95.45.254.121', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521786213, ''),
('7a1f77e3cb81d509bfc30795fd8a0b11', '95.45.254.121', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521786221, ''),
('32b6f8502cef87cc95601d7d1c72af96', '95.45.254.121', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521786238, ''),
('1c3909c4b51b9849949b8052553cb6ed', '95.45.254.121', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521786246, ''),
('9c1fd1b8b2b605e2fd2d24e3a60ac1da', '95.45.254.121', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521786257, ''),
('f731e41125a8dd391430cf8afcbafe78', '95.45.254.121', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521786272, ''),
('67c8e1b22b9f97d30c35722c052e8800', '12.17.170.25', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.3', 1521786798, ''),
('4228a6ad74b167f89652afb87e22e6d9', '89.234.68.100', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521787532, ''),
('a2ca8fd6d42f2b1a9dcf81674466af5a', '89.234.68.100', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521787541, ''),
('016aeb465d180ccac6a3751bfda88631', '89.234.68.100', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521787549, ''),
('9c73d9888927f3b129c2631068bd51ad', '89.234.68.100', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521787556, ''),
('734c2529632b093fed9ecdb64e160785', '89.234.68.100', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521787572, ''),
('61cf26138bf30ffe2fcf26410184d30c', '89.234.68.100', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100721 Firefox/3.6.8', 1521787591, ''),
('24cb12aadb1bc81b539458d60e1c025a', '49.145.98.105', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521787638, ''),
('f07b06d98660728f7067ab8bb1b52bd0', '189.83.23.55', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521789304, ''),
('77f9f7a60faded9d8c7ad6caa8708324', '83.6.226.192', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521789807, ''),
('e5a4499c29e0e90a95b71652727ecb07', '106.120.161.66', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR ', 1521792299, ''),
('17254fda5b96613b01685d1194733b95', '101.199.112.45', 'Mozilla/5.0 (Linux; U; Android 5.0.2; zh-CN; Redmi Note 3 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Version/4', 1521792326, ''),
('6b685324d15f2cde07a35d10ca2da125', '193.201.224.32', 'Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_7_6 rv:6.0; sl-SI) AppleWebKit/531.9.1 (KHTML, like Gecko) Version/4.0.5 Safa', 1521796432, ''),
('2091ceca2f1f524c43dc58a0cbfb7e21', '59.174.144.16', 'Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20101203 Firefox/35.0', 1521796433, ''),
('07a52356e760b3e7cb7ab424cf28597a', '173.202.90.213', 'Mozilla/5.0 (X11; Linux i686; rv:6.0) Gecko/20101203 Firefox/35.0', 1521796448, ''),
('c4b2c2f074d9578ac7ab4cb68ab2e871', '75.120.17.76', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows 98; Trident/5.1)', 1521796451, ''),
('b9c95245deb100f417e2a311bcc7ad12', '194.187.170.124', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521796901, ''),
('e4b8da861b332dd4b4bde8888ffaa685', '194.187.170.124', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521796902, ''),
('87709ea7909659eaae5916a2dd378f0f', '194.187.170.124', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521797087, ''),
('e6c9e8ecdb23355f0ef5934dba5d7a7d', '194.187.170.124', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521797088, ''),
('d79e8784c97f1a3a0a2e440877d30355', '66.249.73.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521800139, ''),
('326ca0af49496d8671728f31d02f4d6f', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867119, ''),
('c0f105603598a9f37ba5fd98cb731efe', '188.132.135.91', 'Mozilla/5.0 (Windows NT 5.1; rv:32.0) Gecko/20100101 Firefox/32.0', 1521838194, 'a:1:{s:14:"admin_redirect";s:5:"admin";}'),
('7e0d9992e75e1f8cfa8357a14ccf90d8', '185.136.116.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521838656, ''),
('ba3bde97b840eec546b54162fdb2deb2', '66.249.65.139', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mob', 1521840049, ''),
('0a878823345938414f24f327a634a8da', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867149, ''),
('57b39b7cedbae88155a161da4f7967e8', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867150, ''),
('eba8fac38e29b03d98591ab1d01d83fb', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867159, ''),
('c9ec512fcbcca2826f9bd81f57bbfe50', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867165, ''),
('5921919572b1eec08216527991c73522', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867167, ''),
('53660c2605e0f1bdf3b029d42e746dd2', '169.53.184.23', '0', 1521894064, ''),
('438b18a977d100b2dcd51fb49e3da362', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521894800, ''),
('d206a7e0659acb294e5eed2e965c6b5c', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521894804, ''),
('22c728ce5a32926af7c75cec9f490cea', '54.67.59.131', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.3', 1521894830, ''),
('7ea2dd36725d42a69670888fb21dbbf6', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521894946, ''),
('dc853a2995489dbe6ac404a6c1289cb5', '34.253.212.135', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1; http://spyonweb.com', 1521899594, ''),
('00cd87af0c558029203d751d96bda823', '34.253.212.135', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1; http://spyonweb.com', 1521899594, ''),
('c609f21b94b7b5d75817fea2b0aac10c', '109.102.111.88', 'Java/1.6.0_04', 1521902261, ''),
('6513238d28fd2caa325094cef0b8f71e', '109.102.111.88', 'Java/1.6.0_04', 1521902262, ''),
('9fb4bf6191e65c7004ec4910d83a8c11', '109.102.111.88', 'Java/1.6.0_04', 1521902263, ''),
('2e9bbcd0c3fde182f9c11aa9b11c752a', '109.102.111.88', 'Java/1.6.0_04', 1521902264, ''),
('3c3c05cde271d48338ce4b7cac5f0ed1', '109.102.111.88', 'Java/1.6.0_04', 1521902266, ''),
('1a2ff72aa91d2aa4962c3131d96dfaa0', '109.102.111.88', 'Java/1.6.0_04', 1521902266, ''),
('4e50825904c97bed364505dbd09cf5ed', '109.102.111.88', 'Java/1.6.0_04', 1521902267, ''),
('62a10fc981df5ebae7e3799d71b8f7d1', '109.102.111.88', 'Java/1.6.0_04', 1521902270, ''),
('2b04230f22464d6547767e8cedb785dd', '109.102.111.88', 'Java/1.6.0_04', 1521902272, ''),
('b915b9f5a13dadafb9b983eefc396586', '109.102.111.88', 'Java/1.6.0_04', 1521902275, ''),
('457a4a2d6904bd9ca1531b10661542ea', '109.102.111.88', 'Java/1.6.0_04', 1521902276, ''),
('be97f4289e98b0bb4feeab07bda967b8', '109.102.111.88', 'Java/1.6.0_04', 1521902277, ''),
('8223edea78a71ad95d531b5ad772599a', '109.102.111.88', 'Java/1.6.0_04', 1521902281, ''),
('474ee1ef09cc483a7e53df245d0e9654', '109.102.111.88', 'Java/1.6.0_04', 1521902281, ''),
('e036bbd8d77cf61a87497f169083024f', '109.102.111.88', 'Java/1.6.0_04', 1521902282, ''),
('7596a18392413f9614ea6dd8eec99fd4', '109.102.111.88', 'Java/1.6.0_04', 1521902283, ''),
('4a4d55b4f41fbade3983dfcd7b56bdcc', '109.102.111.88', 'Java/1.6.0_04', 1521902284, ''),
('8c0eeffe4574e8a6d82b0fb78aa031e9', '109.102.111.88', 'Java/1.6.0_04', 1521902284, ''),
('26f808f2491c1a1d6e2c8064c0da5496', '109.102.111.88', 'Java/1.6.0_04', 1521902285, ''),
('dec6f0c391e47c06b17df7e1bf842dea', '109.102.111.88', 'Java/1.6.0_04', 1521902285, '');
INSERT INTO `default_ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('27dd66a00e37169d7bc2ba444e9bb1bd', '109.102.111.88', 'Java/1.6.0_04', 1521902286, ''),
('305f66cd78d8df5c8f3dd861aa21cde4', '109.102.111.88', 'Java/1.6.0_04', 1521902286, ''),
('717eb885d6290eb612fa86adbdc07f49', '109.102.111.88', 'Java/1.6.0_04', 1521902287, ''),
('a230560d330fff7aaf3671a3795b91d9', '109.102.111.88', 'Java/1.6.0_04', 1521902287, ''),
('9b41bb04f3dee14db269769cd69c2c10', '109.102.111.88', 'Java/1.6.0_04', 1521902295, ''),
('bdbc2838eb6928efaeb85de190329234', '109.102.111.88', 'Java/1.6.0_04', 1521902295, ''),
('5ff603d06bcc72cb83855554e45dfe7e', '109.102.111.88', 'Java/1.6.0_04', 1521902296, ''),
('c9db1c536eb18540003f3f72a052c1bf', '182.73.90.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521903509, ''),
('e316b6748def2645c797dedcb70cf5fe', '194.187.170.110', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521905214, ''),
('55716cbd5f38ee962f02efe26a59a439', '194.187.170.110', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521905379, ''),
('145959c19a3b93dfea0e0298bb2ba70e', '194.187.170.110', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521905379, ''),
('8642f20766de607fb4990553f2e798be', '188.2.118.121', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521906551, ''),
('5bccd3ee8c8d7e65555985d80cad0215', '217.164.187.200', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521906960, ''),
('10edf38f52a3a7fbeb2a99a441c09aa3', '36.77.145.164', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521906990, ''),
('3963cc7bda205a63c00264fc1ccb7a2b', '139.192.117.203', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521907000, ''),
('a8bc9408d910899b4693acccbb5c2134', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521908318, ''),
('63f78cb78746f17e6220d1545ea2e8b4', '93.42.75.178', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521909037, ''),
('c64ee89c7f3c8d3c08e97037ef10efe3', '116.14.201.72', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521909232, ''),
('618b08abe530756177ca3f51e1f456d4', '41.111.117.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521910865, ''),
('f2e7b2c92a3ca1ed00d42d9915ff846a', '93.78.53.214', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521912163, ''),
('c64f05a8cd89390550de7f92cb455e9e', '103.231.161.114', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521912229, ''),
('5094d54e48567ba4a45692c118d1e8d3', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521912909, ''),
('71ffdc6aff5fa9ad58255f40c69d04e1', '112.200.230.137', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521913338, ''),
('191c63b4dc76a53fb3da1ab4588f81b0', '5.21.252.109', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521915313, ''),
('67271bf84f9ffd7c240acaaef82b91bd', '191.193.116.235', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521915336, ''),
('9ad82948d5282a3d97becc01dd3dd1af', '202.162.214.236', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521915837, ''),
('c0364ffee92358cf7a170788d29d879f', '176.199.31.155', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521919326, ''),
('45872a8f04aa79437db0d6cb5012a126', '188.27.193.252', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521921863, ''),
('5252a48e7b110032708c2f259d3fdccf', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521922124, ''),
('7ebfb8f56b6d055fcc719992e98b5e0d', '66.189.156.17', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.1 Safari/603.1.30', 1521922130, ''),
('6ebf09d603051fc02ab13856890cd4b1', '207.102.138.158', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.101 Safari/537.36', 1521926093, ''),
('d2337be3a83cbe938077caf7d214ec03', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521927776, ''),
('8a47ec357180da4b6087629eb1feb2f5', '181.29.144.224', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521927942, ''),
('3320344e1987a81aad6a5ecf0f6a4bb6', '181.113.98.29', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521928950, ''),
('95ddbffaeaf71c4bc8093d2932610f24', '66.249.73.89', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521929352, ''),
('8d14f0549164fa793c36b1e8e80f6d5f', '66.249.65.95', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521888056, ''),
('ffaf93e6deca7145f5d5ed97201d2d75', '66.249.65.73', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521888065, ''),
('e09be123bc995aaf3540019d14d1d0c4', '66.249.65.71', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521888065, ''),
('21c7966c6068904881d03f77ba4f121a', '18.219.135.12', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1521888680, ''),
('9a2b19f49d0c774bc33e2ae43c1c3f94', '202.47.58.88', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521892232, ''),
('26f09d76678ce7c7698cd7e6394b1665', '103.47.246.72', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521892914, ''),
('75640e22dc0b26d439f004236081700e', '178.134.208.38', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521893147, ''),
('65111be43c0e00a55688ed73ff7e6a24', '118.67.221.66', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521893652, ''),
('0845336c358ac0980894e75dff63073e', '213.149.51.97', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521703221, ''),
('52ba4a462106c07f90eba1731e6501e2', '67.190.194.23', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', 1521701917, ''),
('2759ea42d8500ce03b0522760c1bfaec', '66.249.65.153', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521963043, ''),
('a27f6bee2462abd740a093527fbd0177', '66.249.65.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521963084, ''),
('5b96d0417f59bbd146f88fc38b3647e4', '66.249.65.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521963085, ''),
('559bedb95dcc024f2428342383302149', '64.74.215.1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', 1521965710, ''),
('3e9df0452855a27772aebfd5a331c255', '52.165.148.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5', 1521968683, ''),
('8a99cfeec505d801bdfb3e11613c2d94', '43.224.131.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521968773, ''),
('a9f8667824c5345d70109ad9dd7077ef', '62.165.226.202', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521968914, ''),
('d4c50ae8f9d7c7081b1e3253abae4bbc', '122.53.59.17', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521970307, ''),
('a94dae52fc387f8cbc75119f27bb772b', '122.52.70.15', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521971789, ''),
('fcc12a22103066c2c792f9a5f681a0dc', '142.160.129.25', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521972046, ''),
('1f4373ea391e55f2acd8f07a5de7e4ca', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521972876, ''),
('e06816d7559b966603cfc858c96dcf12', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521975983, ''),
('08695ab28dfec3e5353f255445693170', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521975986, ''),
('f83894776edb0d7f211c72be4b7ce581', '156.67.107.4', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521976229, ''),
('6872707556af9c8115e03cd6f8e57e6c', '63.78.215.163', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521976236, ''),
('5a671f50ae48f3942514df10bdcf8519', '77.88.47.92', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521977756, ''),
('dab54f17f2da7fb92e4e5f04a6c8894e', '103.88.82.55', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521979499, ''),
('0f1346af2cc415b5b0040007c7c9d0fe', '36.68.55.206', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521982629, ''),
('bc4e702f046fbfed9287cffe171b60a3', '110.54.231.115', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521983069, ''),
('7b707b6b91728e3673b14e6577cc229e', '141.8.143.144', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521983166, ''),
('46fe9479749b20cdd81f68722c7a738a', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521983190, ''),
('8efcf4c571ee55383e6ad723b5907a69', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521983793, ''),
('94eaf69dc4724ffcac773c40b8996eb1', '123.231.109.8', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521985493, ''),
('b513d848cd6a326f0d0bd8beec546562', '157.49.207.101', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521985701, ''),
('9c8098c8e223fb3c70e54f558f3e282d', '180.191.150.121', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521987788, ''),
('37b372ad89b812dfb768da36e63f47e4', '93.158.161.129', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521988029, ''),
('8c3caf0ac9733b2b14e9c6db5ffb9980', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521988139, ''),
('fa7e64645583758e2b36a03b0a27669d', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521988139, ''),
('91361bef5aba9c1f50052ce5dd4a0131', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521988143, ''),
('dbf9efe2485c58fc7b94b3aa21a43203', '123.125.67.156', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36', 1521989683, ''),
('9f5298fa58b612a339eeb7e0a6f7740b', '91.148.91.141', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521990635, ''),
('419e5990640e5c51dc54c07851f58e86', '88.15.192.230', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521992004, ''),
('93ecbae2fd96075d618a63458c365d4f', '52.165.148.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1521992984, ''),
('4ea30aee5fccb684c6aa674835614343', '103.40.197.22', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521995674, ''),
('be9aa070ec41a9c608cd7857143d3a01', '77.54.235.14', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521995992, ''),
('0b93122efc0a733bac66ecbdc44eba71', '175.143.17.206', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521998890, ''),
('c78135a80e99cb84d37f59e619cde6d3', '14.192.211.198', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522000247, ''),
('b23110f1d551aa16e2b400dbdf0fb260', '37.211.146.241', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522003555, ''),
('2533c5baf31a9d4fb3fc9582785ab940', '103.79.169.120', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522003624, ''),
('ebf4bf887c1171910fab7099dfae171f', '115.98.249.231', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522004681, ''),
('b0852396bd249a36d2235038d80466e8', '180.191.142.39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522004845, ''),
('2ea4ec85fced66e3dfa501555b56b0e5', '180.191.142.39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522004882, ''),
('5336577e79c21c48515954ba260e5a93', '88.251.62.95', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522006258, ''),
('ce36908b818e4241e392ee3274ed50ed', '105.103.62.116', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522007879, ''),
('c106ce790e5c9adbe8fb7a4302e4dd3d', '41.136.224.251', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522009168, ''),
('c7d1644d928ddf815e85acff8cd2e0e7', '81.243.58.218', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522009633, ''),
('5ec09e91eddc6da8ef3184565aa9ea8b', '81.104.76.201', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522011468, ''),
('c086af9cdd1578e13d6b4959eb12cbfc', '85.106.162.239', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522011860, ''),
('84cf6342e5b9afa8f4c2ba9f475186a6', '13.59.55.22', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1522017716, ''),
('6b5e05b3ef35c3c02b0893a2dccceb4c', '103.69.113.171', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522017898, ''),
('532a122080fdea24201835c105b0578a', '66.249.83.27', 'Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko; googleweblight) C', 1522019695, ''),
('001d0ee9e097d784510c622c1eaf18cd', '100.43.91.200', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1522021270, ''),
('bb532ef30358f91e733ec725b4a1dc49', '182.179.140.41', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522021773, ''),
('5c66f3ed2dc721a7d68ef64bbb679fc7', '88.167.82.245', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521446009, ''),
('a9b30dae5d7cc526d769d1b22594cd56', '79.23.72.144', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521448836, ''),
('3f9c8c4dfc4d7afdc05af0f7a4acd8fd', '111.251.57.16', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521449869, ''),
('82b4be5e24b0ebaa8b9e48d81bee9eb4', '100.43.90.200', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521452023, ''),
('26ef62c2216de094cfdfd6c218a11850', '100.43.85.201', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521452032, ''),
('f4c813174a69d1f298f91f426513c2ba', '111.92.26.211', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521452942, ''),
('86f5b15465b6360c542129f00c15aed9', '180.246.170.108', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521453320, ''),
('7630082eaf8ff9e059c59739bb2a69f3', '103.18.10.251', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521453702, ''),
('af4dba38f62a831e44cc9c8a7c9e1861', '66.249.65.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521454487, ''),
('e427d4fc1252ab94b6504f42e1279b09', '119.155.44.152', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521590104, ''),
('a2c6bc0846093f69d89dd8f877a93c70', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589123, ''),
('17a7a7fbb1c786005119440d19c938d2', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589121, ''),
('ee72c1688274bb0299b84ed85840c606', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589118, ''),
('0705f0b9cf35dae7425c19bc73d027a9', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589116, ''),
('88c0b7592c5b57b1aa69316b05b4f87a', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589114, ''),
('2bc99da3f6035343060f2ca5272bcdb1', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589111, ''),
('7913b4288788836004fe3e5adb4a131a', '79.137.68.85', 'Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1', 1521878371, ''),
('13bb3c6804f24c813d0a9f8e4709ada6', '103.217.244.117', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521880431, ''),
('320ee1af4158059b86ab8279bb4ff471', '59.152.97.83', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521884534, ''),
('d9fd45d1a61a84de2f2d89512d22fd96', '52.165.148.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1521884680, ''),
('4e0bda2136eba2780dd846f33fdbb370', '180.76.15.19', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521884898, ''),
('6ab4db9fdd7e009e58d48710745db020', '86.98.13.222', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521885486, ''),
('22212fc7a885ad54bc2bf0479166be5f', '59.92.51.56', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521885557, ''),
('3b199b2bba014b39c00c8397bd919e24', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885900, ''),
('9ef1b47622f56290ca661b853282da7e', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885902, ''),
('f44d1c237ade1316b286650d7d147c36', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885904, ''),
('1ddb59b1263d7bb76345f5c61bdb8474', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885907, ''),
('c7c715ba7bbfc95bafbd2e36001f29a0', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885909, ''),
('ded17c61c5c8819b18be5f2b53768ba7', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885913, ''),
('829a4284a9dd31d4640f4e2291a97ddf', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885917, ''),
('e4fe0dcfed01c659dcabbf4a73f133d0', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885921, ''),
('7b5899bbe944a89bb5bfcf241403cb6a', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885923, ''),
('1c771372c388240936e24bb6b5579264', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885926, ''),
('51fc534e06c7bc9694f695e547584e95', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885928, ''),
('b75578875f4665a58b15c387030f0637', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885930, ''),
('bc9ede794807957b9caa844775d4ca2a', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885932, ''),
('7fe2c660270e5c15c76f591e7b97d0ad', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885935, ''),
('cdcff39461902bd76e0a2c30a5412b63', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521885937, ''),
('1d57ff631b84eff0651f9716ac10a171', '103.252.202.199', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521886142, ''),
('7257cd9bd7736585468f09375c7a6e13', '66.249.65.95', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521888056, ''),
('5450ca2de0f0798e4c9304db5b564fd3', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867169, ''),
('ce6a3122912e0e52559aa0cf3aad3f16', '201.214.182.232', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521840707, ''),
('6df2e58719d47497a27307c2aa568641', '191.189.22.185', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521843074, ''),
('23b2556588afaccecd46b7fc7278b9c0', '39.51.205.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521843194, ''),
('f0a293acf329c6e09a259f0b28b97764', '199.16.157.180', 'Twitterbot/1.0', 1521844997, ''),
('b95b834b11f3d8eb72f0644d400aeeaa', '199.16.157.180', 'Twitterbot/1.0', 1521844998, ''),
('5151469c6aae017d0e1a96c77964acc8', '199.16.157.180', 'Twitterbot/1.0', 1521844998, ''),
('c8807bf74a2591078390936c41fbdf98', '199.16.157.180', 'Twitterbot/1.0', 1521844998, ''),
('11a903f26c6b8678b9e6e1ce081652fa', '190.215.83.130', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521846795, ''),
('8ff865271080ccab75433e49731237cb', '187.115.128.207', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521848314, ''),
('4ad7625e66bc67c9a9e7c55cb27a5801', '157.55.39.53', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521848561, ''),
('61b3362071368ba829f9718e0d85e8f9', '157.55.39.35', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521848563, ''),
('611602e92c19295eb2ce47abeed3b863', '93.87.163.153', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521848649, ''),
('c8c529caa38663325c5fdbba5154bb7c', '86.184.81.84', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521849228, ''),
('07f24ba38e7b72bd57b44d3e4bfea040', '178.255.215.91', 'Mozilla/5.0 (compatible; Exabot/3.0; +http://www.exabot.com/go/robot)', 1521849577, ''),
('6f683b618d3cf5b4a2a4980120687a75', '178.255.215.91', 'Mozilla/5.0 (compatible; Exabot/3.0; +http://www.exabot.com/go/robot)', 1521849578, ''),
('964b094432aaa761904dd04b6f0ab592', '46.177.0.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521849586, ''),
('63312e79cf2dec69c8c815bc1653b269', '81.43.4.181', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521851033, ''),
('480d60e11b28ac450e9f18a93f268f5e', '79.181.143.153', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521851218, ''),
('e27e11b9710e2dceaf5859bccf94af14', '180.76.15.10', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521852434, ''),
('b81ddb1f310d55104e1dd3c94cfccb0f', '217.73.129.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521853288, ''),
('56bc1b7ea650c6458950f78497183395', '201.211.119.72', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521854353, ''),
('48e7afbd66d9e10636e5407c520d9520', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521855534, ''),
('ec0f331dd9c667d9bbeda7d8bdde326d', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521855538, ''),
('f99c56a0e94bedb156b085dbf9655bfe', '180.76.15.26', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521856434, ''),
('0d3cff8cd6c38aab4e57786b2e7ee28d', '78.131.236.18', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521858733, ''),
('3ea545e54201f7ec7948dc10043303b0', '79.88.177.236', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521861995, ''),
('628daa56b2c295a2dbded0b35fe579d7', '177.55.34.67', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521863404, ''),
('9834e26b4bdb300dde2b48ea739665ac', '89.180.153.208', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521864542, ''),
('54d10f002152c5de58854004dc866e5b', '41.209.71.68', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521864748, ''),
('1da8c63191e5a472e55bb323342fd0e9', '174.216.21.148', 'Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.109 Mobi', 1521865186, ''),
('791af159bc517662e4925bb633338a78', '54.183.165.74', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.3', 1521865646, ''),
('e6fc2063e2eaf76fde5bc394a5a253a8', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867047, ''),
('bbe545d98c11cb78a2b688e382e35a29', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867049, ''),
('90d2f775752c7ff73a56c3fdfca11597', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867050, ''),
('52aafa4123a2ca7bb5438056f5d0a7c0', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867060, ''),
('bf92fe12f7ba735f45b96ccb2c611071', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867071, ''),
('30e5861b009f6071582f802e02afaa35', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867074, ''),
('023d034655e17a7c9a2712069964424e', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867077, ''),
('5d7cdf14e3c4389848a08bf8959ad030', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867081, ''),
('33b0ca475b882bdaa1f26a00efb96822', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867082, ''),
('0f4c33a35f7b4adb259a3fe2aab996bc', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867089, ''),
('5c47e4f6c6fd456a7ce557c7f3144207', '34.216.252.127', 'A6-Indexer', 1521553629, ''),
('2e231c3665e1122a372482b9f49bff82', '77.222.115.156', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521552004, ''),
('47d4bdc98463c5e10fbc1369648e69bf', '117.204.188.7', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521552738, ''),
('e47f2af8dd79da2cc7832d2905942f66', '47.223.60.199', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521626010, ''),
('63aae7d26071af6050afdbfae96e7930', '61.72.15.164', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521529701, ''),
('5c542f6aa7208dd9f53b2daa983dde48', '66.249.65.153', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521527960, ''),
('e6baa0a9716539ce4d418c5cda065523', '66.249.65.151', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521527960, ''),
('980f976bb37eede5130055c4197d688a', '66.249.65.139', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521527939, ''),
('eeb348fa32344bb2071512953282615c', '95.248.246.153', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521550712, ''),
('2eb9c6e2c4b2b9b088b81d956ba5215f', '46.102.10.3', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521550048, ''),
('562aec4a0bad345a1c6ddb8ccee1a6d2', '40.77.167.124', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521550359, ''),
('02bc08b594a2629b4c63e03f60dd735f', '49.205.155.52', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521811209, ''),
('8a6bb304cd20b8e54a2daccc5ed5ef68', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521811323, ''),
('c687fc44176783bae87617acefa1711c', '41.207.243.136', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521811005, ''),
('c357f7ed572fb46fee2890d01d215857', '66.249.65.143', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mob', 1521706313, ''),
('768d629ac87013f33b1b02652940dee6', '52.165.148.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0', 1521705275, ''),
('d1bad50b6805cad3f68277e8d60495da', '73.5.47.95', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521703975, ''),
('041d7b9747ea592dc0cf817921721d44', '194.187.170.110', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521873806, ''),
('39f040e165c4b5d0410453848e897bf0', '194.187.170.110', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521873806, ''),
('773af140e55477a690a0d1bbef0fefbf', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521875601, ''),
('9124c73ab43540fd254418b2b7f2108f', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521877530, ''),
('4fa874042ea866da505725c942385667', '66.249.73.71', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521873603, ''),
('8fdc0a7c61ca7785d1b629aa540fa59a', '78.61.26.54', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521742872, ''),
('af97f6610af566e0c37532ec8b5e9074', '41.104.198.196', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521742448, ''),
('f2e18f682bacc9072c475eb1b562a6e3', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521742393, ''),
('91170bbd2b3616c880de1566cecf47f6', '66.249.65.143', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521742393, ''),
('0e8b0dc8c06a7c157f917362c672fa9f', '66.249.65.151', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521742129, ''),
('9dbacc161bfe1a1ea6a058cc5165af7b', '170.84.48.73', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521699218, ''),
('dd906cc50044fc33e9c231bdd19d489d', '194.187.170.103', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521697705, ''),
('7f27e082e087e0d041c0c33196ce285b', '194.187.170.103', 'Mozilla/5.0 (compatible; Qwantify/2.4w; +https://www.qwant.com/)/2.4w', 1521697704, ''),
('dc468ffced262901f70daff62c2f0f18', '185.51.221.211', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521695607, ''),
('75b97cb59f8d50f87e2319e54b365027', '110.54.128.62', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521694317, ''),
('d6dcca2a1189bed053ca86cb2fa57ebf', '207.46.13.19', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521692955, ''),
('e78d005ada8327e813e74fbbd27ac1f3', '5.102.173.71', 'Mozilla/5.0 (compatible; MojeekBot/0.6; +https://www.mojeek.com/bot.html)', 1521691737, ''),
('8939532235219eafa696a0accaaff878', '41.143.69.85', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521687624, ''),
('06756685280d859ad60bc875eafa90fe', '5.102.173.71', 'Mozilla/5.0 (compatible; MojeekBot/0.6; +https://www.mojeek.com/bot.html)', 1521691737, ''),
('65237e07a7804081dcecfebccddd1737', '52.165.148.51', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1521686375, ''),
('692f10d9d293b224bdc4b729f097dacb', '42.201.221.39', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521682557, ''),
('ab64e153213da108080a7c5b99c20890', '220.104.161.192', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521683194, ''),
('4a7b65edb88946ad00534752874efe5d', '187.55.57.241', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521680935, ''),
('b391ca2eeda2af7ca924ef3c7553ce8b', '67.243.179.59', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521681020, ''),
('42b81a07de4b0978249615531386b9b6', '110.227.85.75', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521804985, ''),
('49fff4e25eaf7f53c618ed1ea59a5bfa', '46.196.0.57', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521804265, ''),
('251e726057f00e89a3eaf156c71d56c0', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521813051, ''),
('36a0d421d1aaabba2c9bf39c45c799e7', '14.139.63.2', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521813358, ''),
('361a0f294344dfb3d9fcf9269d265026', '66.249.65.153', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521813636, ''),
('114a41014ee06a0627436868d2636243', '75.138.56.47', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521815402, ''),
('1e37c3ce835a5c2834406784473e795b', '201.17.158.241', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521815424, ''),
('662c999f199ea5b7b8d4d3398b27fd07', '84.84.13.142', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521816841, ''),
('678189a5d0633355e58642e84b3fa80f', '115.79.29.13', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521818506, ''),
('820e3d3f3b7951ccaa4f15fb13b476de', '144.217.5.52', 'Mozilla/5.0 (compatible; ExtLinksBot/1.5; +https://extlinks.com/Bot.html)', 1521819326, ''),
('76f4c52ef9139b4a86f6ef0d21533cca', '86.244.93.94', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521807157, ''),
('8706bb0dfcdd32db27bc4972f8fce893', '46.118.115.22', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 1521807503, ''),
('244d3bf72dfc62f8c45d77721b9c49ab', '66.249.73.71', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521800139, ''),
('cbca2a7ff1f5a84969dd1c3ef5195629', '95.23.156.224', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521807997, ''),
('b65f240f97f09717ad79627d5f3d3dc7', '103.225.136.254', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 1521800843, ''),
('7cdd9b0e341efce540126d0759ba5994', '92.3.175.6', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521801254, ''),
('15db0102830382e6c9e853ab3db6ae13', '66.249.73.66', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521802797, ''),
('fc738a1b2570446c3e104fdf021c4de3', '109.88.163.91', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521810494, ''),
('e9a567b2874e05d92cefc30fd058d755', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867170, ''),
('150c10bc0df2b9fd981a4379c61dc5ff', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867181, ''),
('c089c15c21159595ccd8b2045d8722d7', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867182, ''),
('9c4e9dbbd14e3ffe1b678718f0e5f9b9', '66.70.182.175', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36', 1521867195, ''),
('1b9002e14ffc839c6b52cfbf627d960d', '66.70.182.175', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)', 1521867198, ''),
('fdabceea0e0f8365813b7507b6ed8190', '79.35.194.145', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521869775, ''),
('ed6c440e9a015eca544cf2547a426753', '142.177.90.143', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521871487, ''),
('1540113bd69d9c4d000876ef2b34d483', '207.102.138.158', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.101 Safari/537.36', 1521872543, ''),
('38c73c0a0be3c424e7a0694ffb6bdab0', '66.249.73.71', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521873603, ''),
('e20d370e82a7360fcffaba2288d41880', '34.207.98.73', 'ia_archiver', 1521830454, ''),
('2b6031b19ba6ffab2f0df95862ff5b1d', '34.207.98.73', 'ia_archiver', 1521830454, ''),
('8cdae09096c4f1ec8d481115b87f1dfa', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521835126, ''),
('1c9e6da03b177b683704ee635c73967a', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521835126, ''),
('61b76356f20cd1c726e1006faf6b2104', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521835130, ''),
('0aa17986ee2c84fabc49ce4ac1ae0cb4', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521835130, ''),
('cfc59d6b485d43c6c4d9dfa814191e6e', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521836089, ''),
('e0f15ae398c9a111ee4fa7743d43c9e7', '66.249.73.93', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521820475, ''),
('a74da98576dc32c104d56135011763b3', '220.181.108.139', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521822928, ''),
('451c0e7fbcd631c1ecd9fddb085c1c25', '94.119.96.0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.3', 1521822292, ''),
('74f1fd1e3d9e55541331972e703a3e1b', '117.213.39.225', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521822356, ''),
('a73a7320c8983ada4761aad4c4d5ecc6', '123.125.67.164', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36', 1521822828, ''),
('130254bcb10f7dba4925a9db1c0b47a4', '180.191.123.173', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521819976, ''),
('86faaae09bb5d386dbc89a0cbaf159c4', '66.249.73.89', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521820475, ''),
('064dffe6892dc28d759d7bc3360d6271', '182.70.75.51', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521823208, ''),
('8e2a3e609a0c1de38a514a3713a97964', '79.140.208.90', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521827407, ''),
('288d6dfc1ae8a8b96f565ea1b035d629', '119.93.121.187', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521828171, ''),
('15e29caf4c2390cdb12231291df3856d', '66.249.65.143', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521828684, ''),
('d89f598086cb4d62d4fda86ad1e4fffd', '66.249.65.143', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521819557, ''),
('f86ddb9613ae1ba63939002bd9fb6475', '144.217.5.52', 'Mozilla/5.0 (compatible; ExtLinksBot/1.5; +https://extlinks.com/Bot.html)', 1521819337, ''),
('35aa14edd25c8b0e1a9d6286678e9def', '223.24.26.97', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521812846, ''),
('a64960564c3620b929430aeb0a636366', '66.249.65.143', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521811323, ''),
('3829b052594e16a96e50a4f7f496389a', '36.72.179.204', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 1521812603, ''),
('5fe792ad95d997e895ab385a5d85ca78', '197.249.4.107', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521814939, ''),
('41467d9b8ec27b402a9b33505392f5ab', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521813636, ''),
('bf4420ab405a2a8fc34422179d777660', '180.76.15.141', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1522039318, ''),
('30936c4dd85cd5900e0850a012c0bac8', '1.168.82.227', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522041633, ''),
('58145e93ab32e488ee6f02d8d70e6a5c', '66.102.6.123', 'Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko; googleweblight) C', 1522043787, ''),
('4238cfa31eed9252a48d23ee3cf0608d', '36.84.226.147', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0', 1522044137, 'a:6:{s:8:"username";s:9:"andiewibi";s:5:"email";s:20:"andiewibi@cticff.org";s:2:"id";s:1:"3";s:7:"user_id";s:1:"3";s:8:"group_id";s:1:"1";s:5:"group";s:5:"admin";}'),
('1eedb81837b5ddf1c9c9b3ad3fac944b', '180.76.15.142', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1522037317, ''),
('3098c658f3c99d8d4e5df0909bb43e52', '158.85.81.126', '0', 1522037157, ''),
('2c34e46fc98bbcfc57e27f1cfc7b720d', '94.23.33.67', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1522036889, ''),
('b92ffdd7f9d9ac5d519cbfe38b68907f', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1522035418, ''),
('2c1ac3e2e0dbb8e0484992dba03b6558', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1522035418, ''),
('f7025064eb8cf8bd683af0dcf6a2c551', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1522035398, ''),
('09f731931ca4e6085a09adbbe97ab614', '93.28.227.157', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521431614, ''),
('41cabfc8b9487bc4a25e032467a7a752', '92.110.136.98', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521431834, ''),
('82c0eeff88162be6cc5ea4a2a928cea3', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589083, ''),
('c43490442c81326aa8dcde056235ca7d', '37.122.2.228', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521588544, ''),
('f10c91c550a90a93d0e1608c3e111e18', '185.48.183.36', 'Mozilla/5.0 (Windows NT 5.1; rv:32.0) Gecko/20100101 Firefox/32.0', 1521588127, ''),
('5208d1fc50bf6a99f2125ff2488b45fb', '188.120.196.231', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521587499, ''),
('73f19fc08cd73097881af4213d47917c', '104.156.231.254', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36', 1521587153, ''),
('c1e5df6b90e056161c10d27c0689f7bf', '177.98.145.125', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521586192, ''),
('6fb7f2f6843c29ae31eeb191f9fb17a3', '92.98.72.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521584424, ''),
('b8f0b996b574edf87769573e8e5a1ade', '38.104.222.2', '0', 1521582502, ''),
('6168a8681de89a6efc4bd5e9bc0f9cc5', '38.142.184.10', '0', 1521582554, ''),
('1a7e4065380b372f4370f5e3c18a180d', '82.233.165.191', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521679262, ''),
('81745d67a5b5a9085701d8eb9b13faab', '175.138.171.26', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521678754, ''),
('cefe76cc1a5f0cf989dbe4291a681f5c', '31.10.156.81', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521678480, ''),
('3489cb82acecf5ca5860687b8ad3c053', '182.187.10.120', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521677420, ''),
('4ebf14f8c76b983030d0a6f0ae155548', '175.140.251.11', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521677417, ''),
('f453b11d248d4ca9c76c2dde0241d6ec', '107.137.194.178', 'InDesign%20CC/13100 CFNetwork/893.13.1 Darwin/17.4.0 (x86_64)', 1521676720, ''),
('4bbd05caf15072e0b57149cbda26c187', '197.0.116.207', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521676209, ''),
('59a1a473a4faaa556b51e8256b65da5e', '196.178.68.79', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521676152, ''),
('09939224108fc429949735c4c6f3bdeb', '220.100.15.58', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521675823, ''),
('d7ede62bcb41cc2c90e085eec8d8f8d6', '119.95.145.176', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521675645, ''),
('f64c3e6a6747c0f562815a17b394e5da', '199.16.157.180', 'Twitterbot/1.0', 1521673409, ''),
('8802c85597bfc9167c893a0969c0b082', '87.16.210.195', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521675246, ''),
('4f7cdd1d5b81b32f5d30d45c4c810164', '199.16.157.180', 'Twitterbot/1.0', 1521673409, ''),
('87574b12edeee0e560ca076baab184d4', '199.16.157.180', 'Twitterbot/1.0', 1521673409, ''),
('9f66989405343ee882bf494b7be9afe3', '66.249.65.151', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521672437, ''),
('9c8875b03c9687d7d103c53ec88c3f16', '197.3.214.247', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521672519, ''),
('bb7a4f33c39212f08b4ba4c410f68005', '177.156.187.108', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521672551, ''),
('b3243c759bf40c7888f352db4e9a55ff', '66.249.65.141', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521672714, ''),
('96a5cc2551ff23a13f8cd56190180652', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589086, ''),
('a15d8a04932d4293b032e1f296685ba9', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589088, ''),
('4fb7ae8fc87b27582c5fb8f9d1133b56', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589090, ''),
('06bd8917f96f356f9337a836fc0f42a7', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589093, ''),
('260a02b33cd5a5feb8dd355add1eeb9b', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589097, ''),
('e90e64948d1e1f04c2188b75671e2a29', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589102, ''),
('606be1aea43ace6272f37630eda3c063', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589106, ''),
('3a30028d07a06c4c2fe6be93ae79b6ed', '23.237.4.26', 'Mozilla/5.0 (compatible; AlphaBot/3.2; +http://alphaseobot.com/bot.html)', 1521589109, ''),
('7b84eb0e22afe362303fcb389cd8ba0d', '46.197.93.139', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521930076, ''),
('6a0d1cc1955f06e138a2bb23e2b3354a', '66.249.73.91', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521929352, ''),
('0bd84727d441a073d5cfe0e05dcc87d0', '80.180.61.196', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522024445, ''),
('c6ce12a7e9c62d0b6e255f4f6ed796f3', '207.46.13.76', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1522025424, ''),
('99ea9bf46e12d80f8a4db82a9a26588f', '207.46.13.105', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1522025442, ''),
('8ea0fee8ef00a4f3ba55f91bc12b93e0', '94.23.33.67', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 1522026628, ''),
('2e50d6af1ffea7b2ca55ce7a8f221148', '37.115.112.228', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; MRA 4.6 (build 01425); MRSPUTNIK 1, 5, 0, 19 SW)', 1522027073, ''),
('e0dde5a69d556dc7f49001eea6ca193f', '37.115.112.228', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; MRA 4.6 (build 01425); MRSPUTNIK 1, 5, 0, 19 SW)', 1522027074, ''),
('e87f2b256e199f1e6e33c242c8b29c57', '37.115.112.228', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; MRA 4.6 (build 01425); MRSPUTNIK 1, 5, 0, 19 SW)', 1522027074, ''),
('60ba49e9a01230da4d71f2d8d1349bea', '41.107.35.193', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522029004, ''),
('8f63d509a9647532ef5faaaf934b0cad', '115.127.79.170', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522029196, ''),
('914078c289f86c5f85d26aebc6ca0e2b', '189.93.53.163', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1522033393, ''),
('367e478eefea18709cc17ba2290dbccf', '66.249.65.149', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1522035397, ''),
('21bcd97470227af8d2bcc8018a808ee2', '105.155.32.42', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521931065, ''),
('8ea4d7ff1fdce3887d7e227dce670d2e', '41.225.45.12', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521931838, ''),
('430d25a1145993fdd7f29f4aee1752ee', '200.126.150.215', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521933114, ''),
('cb11a636cdc3cde777294c9a7ffdd28f', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521934035, ''),
('cd82b16e218190a37384ddb00350fb11', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521934039, ''),
('bc325ede561ecf6ed4d63613e9a8f5d2', '180.76.15.14', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521936071, ''),
('f65356f56b3a2a9e950b196df57121e8', '89.133.141.66', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521937512, ''),
('eebfbe3ed9b980f9c17d738b49ee81d1', '180.76.15.5', 'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)', 1521938070, '');
INSERT INTO `default_ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('84017400f55ffdeea9ea341ce65259aa', '178.208.12.3', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_4) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.112 Safari/534.30', 1521939355, ''),
('519b3017beeff7dcad97eff45eb3e1bd', '178.208.12.3', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_4) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.112 Safari/534.30', 1521939355, ''),
('52a19c45309bf05fb2e75e0cf4a6fd14', '181.222.1.186', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521939816, ''),
('8d4db718391f78c65e8515d9199515e4', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521940633, ''),
('112cecf32dc098b8c4d30f8b0ad35548', '190.15.238.23', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521940884, ''),
('dc88a4dc796098f1d8d2c0d51d9bce2b', '41.100.68.30', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521942960, ''),
('b66d89f3d9ff6e99c4129ed1aa7a3bba', '87.101.19.24', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521945458, ''),
('fe9411f11dfe0ca0d388a3a0e878e414', '189.54.144.248', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521945899, ''),
('02c5d6ac3d615d497511a70bfbef21bb', '110.44.125.149', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521946751, ''),
('9fbb0802b80c189b5cbbeab4ecdfc0c0', '218.9.217.179', 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143', 1521947308, ''),
('2c1a8dfa19961275a74bd8cff538993e', '212.253.163.223', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521947377, ''),
('5d62354bb6b51d6c6d06bd04471ebdae', '192.99.69.242', 'Mozilla/5.0 (compatible; ExtLinksBot/1.5; +https://extlinks.com/Bot.html)', 1521951349, ''),
('accbc5826eed6800977e3997cc859fbd', '192.99.69.242', 'Mozilla/5.0 (compatible; ExtLinksBot/1.5; +https://extlinks.com/Bot.html)', 1521951360, ''),
('8951c977d6476f1667191532cfc140ca', '181.140.163.107', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1', 1521952760, ''),
('9d78194920c8be6e0d27369ca2365001', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521954338, ''),
('aa065c41d2bd5b61f394f8ade075bd89', '52.165.148.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36', 1521956081, ''),
('e38fe893d305ee379906c229aaa52086', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521956462, ''),
('029cd38a3c41ded9485ba3407876d967', '5.255.250.79', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521956466, ''),
('9d14355673bc707afa534605d2879259', '157.55.39.181', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)', 1521957994, ''),
('96714694f73bff0b58965e8e460ac331', '77.88.5.31', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 1521961950, ''),
('7c0f3aef325ea9ec965c354ff56cd7d1', '66.249.65.153', 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 1521963043, '');

-- --------------------------------------------------------

--
-- Table structure for table `default_comments`
--

CREATE TABLE IF NOT EXISTS `default_comments` (
  `id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `parsed` text COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `entry_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `entry_title` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entry_plural` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_comment_blacklists`
--

CREATE TABLE IF NOT EXISTS `default_comment_blacklists` (
  `id` int(11) NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_contact_log`
--

CREATE TABLE IF NOT EXISTS `default_contact_log` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `sender_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sender_ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sender_os` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sent_at` int(11) NOT NULL DEFAULT '0',
  `attachments` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_contact_log`
--

INSERT INTO `default_contact_log` (`id`, `email`, `subject`, `message`, `sender_agent`, `sender_ip`, `sender_os`, `sent_at`, `attachments`) VALUES
(1, 'lukedmcmillan@hotmail.com', 'Coral Triangle Day :: Support', 'This message was sent via the contact form on with the following details:\n				<hr />\n				IP Address: 81.133.39.92\n				OS Unknown Windows OS\n				Agent Chrome 50.0.2661.102\n				<hr />\n				Hi there,\n\nI am hugely supportive of the work you are doing. Although I haven''t entered the competition, I just wanted to make you aware of a little video i helped make a couple of years ago - might provide a few laughs and to let you know that there are people everywhere doing what they can - keep up the great work!!\n\nhttps://vimeo.com/40047407\n\nAll the best,\n\nLuke\n\n				Luke McMillan,\n\n				lukedmcmillan@hotmail.com', 'Chrome 50.0.2661.102', '81.133.39.92', 'Unknown Windows OS', 1463736791, '');

-- --------------------------------------------------------

--
-- Table structure for table `default_countries`
--

CREATE TABLE IF NOT EXISTS `default_countries` (
  `id` int(5) NOT NULL,
  `countryCode` char(2) NOT NULL DEFAULT '',
  `countryName` varchar(45) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `default_ctd`
--

CREATE TABLE IF NOT EXISTS `default_ctd` (
  `id` int(11) NOT NULL,
  `revision_id` int(11) NOT NULL DEFAULT '0',
  `country` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('draft','live','inreview','disapprove','deleted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `organiser` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sponsors` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8_unicode_ci,
  `geolocation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geocoordinates` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hide_map` tinyint(1) DEFAULT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) NOT NULL,
  `publish_on` int(11) DEFAULT NULL,
  `slide` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro` text COLLATE utf8_unicode_ci,
  `body` text COLLATE utf8_unicode_ci,
  `notes` text COLLATE utf8_unicode_ci,
  `date_from` int(11) NOT NULL DEFAULT '0',
  `date_to` int(11) NOT NULL DEFAULT '0',
  `plan_to_come` int(11) NOT NULL DEFAULT '0',
  `submitted_by_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submitted_by_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_venue` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approve` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_year` year(4) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=197 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_ctd`
--

INSERT INTO `default_ctd` (`id`, `revision_id`, `country`, `slug`, `category_id`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `organiser`, `sponsors`, `location`, `geolocation`, `geocoordinates`, `hide_map`, `created_on`, `updated_on`, `publish_on`, `slide`, `views`, `title`, `intro`, `body`, `notes`, `date_from`, `date_to`, `plan_to_come`, `submitted_by_name`, `submitted_by_email`, `website`, `contact_email`, `event_venue`, `venue_address`, `approve`, `event_year`) VALUES
(15, 0, 'ID', 'sustainable-seafood-festival', NULL, '', '', '', 'live', 'a:1:{i:0;i:86;}', NULL, 'Makassar', NULL, '-5.14476,119.40837', 0, 1433531438, 1450038680, 0, NULL, 1, 'Sustainable Seafood Festival', 'Yayasan Mattirotasi in Makassar, South Sulawesi, celebrates the World Ocean Day and Coral Triangle Day with Sustainable Seafood Festival, Sustainable Fish Market, Talkshow, and music entairtainments. This event is supported by WWF Indonesia.', NULL, NULL, 1433635200, 0, 0, 'Dwi Aryo', 'daryo@wwf.or.id', '', 'mattirotasi@hotmail.com', 'Losari Beach, Makassar', 'Losari Beach, Makassar', 'yes', 2015),
(16, 0, 'ID', 'bukan-pasar-ikan-biasa-2015', NULL, '', '', '', 'live', NULL, NULL, 'Bali', NULL, '-8.71405,115.16792', 0, 1433532500, 1450038681, 0, NULL, 1, 'Bukan Pasar Ikan Biasa 2015', 'To increase the demand for sustainable seafood products, WWF Indonesia holds Bukan Pasar Ikan Biasa (Sustainable Fish Market) in Bali. Get involved in the Turtle Run, Seafood Dine Out, and Sustainable Seafood Education Programs. Bring your own tumbler.', NULL, NULL, 1434153600, 1434240000, 0, 'Dwi Aryo', 'daryo@wwf.or.id', '', '', 'Bali Anggrek Beach', 'Kuta Beach, Bali', 'yes', 2015),
(10, 0, 'SB', 'beach-cleanup-2015', NULL, '', '', '', 'live', NULL, NULL, 'Honiara', NULL, '-9.43437,159.96707', 0, 1433316573, 1450038678, 0, NULL, 1, 'Beach clean-up 2015', 'A Beach cleanup event was organised by the Ministry of Environment, Climate Change, Disaster Management &amp; Meteorology. This was done as a build up event towards the celebration of the World Environment Day, World Oceans Day and the Coral Triangle Day 2015. Volunteers came from Solomon Islands National University, Youth@Work, NGOS ( WorldFish), UN-Habitat, Ministry of Fisheries, Honiara City council and the community members for the two communities. Partners who sponsored the event included UNDP Solomon Islands.<br />\n<br />\nTools were officially handed over to communities after the clean-up by the Ministry. At least 7 tonnes of rubbish (more than 50% PET bottles and plastics) were collected at the two sites. Communities were encouraged to keep their beaches clean.', NULL, NULL, 1433289600, 0, 0, 'Agnetha Vave-Karamui', 'agnetha.vavekaramui@gmail.com', '', 'agnetha.vavekaramui@gmail.com', 'Beach', 'Lord Howe and Mamana Wata Communities, Mataniko River Beachfront', 'yes', 2015),
(11, 0, 'SB', 'world-environment-day-world-oceans-day-and-coral-triangle-day-celebration-event', NULL, '', '', '', 'live', NULL, NULL, 'Honiara', NULL, '-9.43035,159.95320', 0, 1433317195, 1450038687, 0, NULL, 1, 'World Environment Day, World Oceans Day and Coral Triangle Day Celebration Event', 'The Ministry of Environment, Climate Change, Disaster Management and Meteorology in collaboration with the Ministry of Fisheries and Marine Resources and Youth@Work will be hosting a public event day. The World Environment Day, World Oceans Day and Coral Triangle Day will be nationally marked on the 5th of June 2015.<br />\n<br />\nThere will be public official speeches, school competitions, song and dance and mascot competitions under the environmental themes, ocean themes and the coral triangle theme. Government agencies, private sector, NGOs, schools, youths. Information and display booths by FFA, Ministry of Fisheries, TNC, Wildlife exporters, UNDP, recyclables etc will be part of the eventful day. Kids will have games and other interesting activities around nature and its resources.', NULL, NULL, 1433462400, 0, 0, 'Agnetha Vave-Karamui', 'agnetha.vavekaramui@gmail.com', '', 'agnetha.vavekaramui@gmail.com', 'National Arts Gallery', 'National Arts Gallery, Mendana Avenue', 'yes', 2015),
(12, 0, 'PH', 'ocean-trash-talk-dont-trash-our-treasures', NULL, '', '', '', 'live', 'a:4:{i:0;i:76;i:1;i:77;i:2;i:78;i:3;i:79;}', NULL, 'Pagadian', NULL, '7.57132,123.18013', 0, 1433328360, 1450038683, 0, NULL, 1, 'Ocean Trash Talk: Don''t Trash Our Treasures', 'To raise awareness on the consequences of ocean trash, the Coral Triangle Initiative - Southeast Asia (CTI-SEA) project in the Philippines is happy to join the International Coastal Cleanup movement in celebration of the Coral Triangle Day on June 9.<br />\n<br />\nThe six municipalities of the Dumanquillas Bay Protected Seascape and Landscape (DBPLS) will conduct a simultaneous bay wide beach cleanup. This will be followed by short lectures and fun games on June 10 in Margosatubig to learn how ocean trash and climate change affect marine wildlife and humans, and what is being done to address these. Both of the events are open to anyone living near or within the DBPLS.<br />\n<br />\nInterested volunteers may contact their local government offices or CTI-SEA for more information.', NULL, NULL, 1433808000, 1433894400, 0, 'Dana Rose Salonoy', 'drjsalonoy@primexinc.org', '', 'ctisoutheastasia@gmail.com', 'Dumanquillas Bay Protected Landscape and Seascape', 'Zamboanga del Sur and Zamboanga Sibugay', 'yes', 2015),
(13, 0, 'ID', 'coral-triangle-day-celebration-in-sanur-bali', NULL, '', '', '', 'live', 'a:1:{i:0;i:82;}', NULL, 'Sanur', NULL, '-8.71116,115.25457', 0, 1433410831, 1450038682, 0, NULL, 1, 'Coral Triangle Day Celebration in Sanur Bali', 'CTC with local partners will celebrate CT Day with Educational Program for local stakeholders on June 09, &#39;15 and a celebration on June 28, &#39;15 that will coincide with Sanur Sunday Market in Mertasari beach.<br />\n<strong>09 JUNE 2015 at CTC </strong><br />\n09:00 &ndash; 12:00 Educational Program - Marine &amp; Fisheries Independent Training for Tourism Practitioners and Stakeholders<br />\n<strong>28 JUNE 2015</strong> <strong>at Mertasari Beach</strong><br />\n07:00 &ndash; 08:00 Beach Cleaning ~ Competition for the Best Trash Collector (trash segregation)<br />\n08:00 &ndash; 09:00 Mangrove Planting<br />\n09:00 &ndash; 10:00 Breakfast by The Beach (Megibung) at Sanur Sunday Market<br />\n11:00 &ndash; 12:00 Trash Art Workshop with Made Bayak<br />\n13:00 &ndash; 14:00 Fun Learning Class for Children<br />\n14:00 &ndash; 16:00 Amazing Ocean Race (Treasure Hunt Games)<br />\n16:00 &ndash; 17:00 Kecak Dance for Kids<br />\n17:00 &ndash; 18:00 Music Factory (Music Performance)', NULL, NULL, 1433808000, 1435449600, 0, 'EDWIN SHRI BIMO', 'ebimo@coraltrianglecenter.org', '', 'ebimo@coraltrianglecenter.org', 'CT Day Celebration', 'Mertasari Beach, Sanur, Bali', 'yes', 2015),
(34, 0, 'ID', 'no-ordinary-fish-market-sustainable-seafood-festival', NULL, '', '', '', 'live', NULL, NULL, 'Nadi', '', '', 0, 1436435990, 1450038685, 0, NULL, 1, 'No Ordinary Fish Market - Sustainable Seafood Festival', '​A fish market where visitors can have face-to-face interaction with fishermen implementing best fishing and aquaculture practices, and buy high quality responsible seafood products. The event is supported by the community of Bintaro, JARING Nusantara (a network of best fishers and NGOs), Fish &#39;n Blues (a supplier), chefs from Trisakti Tourism Academy and WWF-Indonesia.<br />', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Sofitel Fiji Resort and Spa Denarau Island', '', NULL, 2014),
(17, 0, 'PH', 'reef-fish-recovery-project-in-siquijor-island-philippines', NULL, '', '', '', 'live', 'a:1:{i:0;i:87;}', NULL, 'Siquijor', NULL, '10.33971,123.91158', 0, 1433757450, 1450038686, 0, NULL, 1, 'Reef Fish Recovery Project in Siquijor Island, Philippines', 'On June 8-11, 2015, the Reef Fish Recovery Project of the Coastal Conservation and Education - Silliman University Institute of Environmental and Marine Sciences led by Dr. Aileen Maypa, with funding support from the Foundation for the Philippine Environment and Unico Conservation Foundation, Australia, is conducting field monitoring and biodiversity assessments of at least three rehabilitated marine protected areas (MPAs) in Lazi, Siquijor and Enrique Villanueva in the province of Siquijor, Philippines. The monitoring team is composed of CCEF Staff, SU-IEMS students and representatives from the local community and government of Siquijor.', NULL, NULL, 1433721600, 1433980800, 0, 'Moonyeen Nida R. Alava', 'executive_director@coast.ph', '', 'executive_director@coast.ph', 'Lazi, Siquijor and Enrique  Villanueva', 'Province of Siquijor', 'yes', 2015),
(18, 0, 'TL', 'troubleshooting-test-123', NULL, '', '', '', 'draft', 'a:1:{i:0;i:88;}', NULL, 'East Timor', NULL, '-8.87422,125.72754', 0, 1433759965, 1433760656, 0, NULL, 0, 'TROUBLESHOOTING TEST 123', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et', NULL, NULL, 1433808000, 0, 0, 'Rendy Mulyono', 'rendy.mulyono@catalyzecommunications.com', 'www.amazon.com', 'rendy.bee@gmail.com', 'TROUBLESHOOTER', 'Catalyze Comm 123', NULL, 2015),
(20, 0, 'MY', 'penang-international-green-carnival-2015', NULL, '', '', '', 'live', 'a:2:{i:0;i:94;i:1;i:95;}', NULL, 'Penang', NULL, '5.41248,100.33199', 0, 1433815114, 1450038682, 0, NULL, 1, 'Penang International Green Carnival 2015', 'Annual event by the Penang Green Council aiming to create awareness and develop interest among the public towards a greener and more sustainable living. WWF-Malaysia is participating to increase awareness and demand on responsible seafood sourcing and sustainable seafood.', NULL, NULL, 1434153600, 1434240000, 0, 'Angela Lim', 'alim@wwf.org.my', 'http://pgigc.com.my/2015/', '', '1st Avenue Mall', '', 'yes', 2015),
(21, 0, 'MY', 'malaysia-sustainable-seafood-festival-2015-myssf15', NULL, '', '', '', 'live', 'a:1:{i:0;i:96;}', NULL, 'Kuala Lumpur', NULL, '3.14109,101.69009', 0, 1433815816, 1450038684, 0, NULL, 1, 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', 'WWF-Malaysia&#39;s Sustainable Seafood Festival will join a week long celebration in conjunction with World Ocean Day and Coral Triangle Day organised by Ministry of Science, Technology and Innovation, with the theme of &ldquo;Healthy Oceans, Healthy Planet. MYSSF&#39;15 aims to increase awareness and demand on responsible seafood sourcing and sustainable seafood.', '', NULL, 1434067200, 1434240000, 0, 'Angela Lim', 'alim@wwf.org.my', 'http://www.saveourseafood.my', '', 'Planetarium Negara', 'No. 53, Jalan Perdana, 50480 Kuala Lumpur', 'yes', 2015),
(22, 0, 'FJ', 'coral-planting-and-beach-cleanup-with-shangrila-fijian-resort-and-spa', NULL, '', '', '', 'live', 'a:1:{i:0;i:97;}', NULL, 'Suva', NULL, '-18.14711,178.43246', 1, 1433820656, 1450038687, 0, NULL, 1, 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', 'As part of their CT Day 2015 celebrations, WWF-Pacific Fiji office will take part in a Coral replanting/Fish house building and beach cleanup activity with our Sustainable Seafood hotel partner, Shangri-la Fijian Resort and Spa. The activities will be held at and around the hotel premises. The day&#39;s activities will also include staff from the hotel and the staff from the Sigatoka Sand Dunes.', NULL, NULL, 1433808000, 0, 0, 'Vilisite Tamani', 'vtamani@wwfpacific.org', '', 'vtamani@wwfpacific.org', 'Shangri-la Fijian Resort and Spa', 'Coral Coast, Sigatoka', 'yes', 2015),
(23, 0, 'SB', 'gizo-town-foreshore-cleanup', NULL, '', '', '', 'live', NULL, NULL, 'Honiara', NULL, '-8.13357,157.52317', 0, 1433821076, 1450038679, 0, NULL, 1, 'Gizo Town Foreshore cleanup', 'As part of their Coral Triangle Day celebrations, WWF-Pacific Solomon Islands Office will be organising a clean-up of the Gizo Township and market areas. There will also be a rubbish-bin painting competition for school children.', NULL, NULL, 1433808000, 0, 0, 'Vilisite Tamani', 'vtamani@wwfpacific.org', '', 'vtamani@wwfpacific.org', 'Gizo Town', 'Western Province, Solomon Islands', 'yes', 2015),
(24, 0, 'ID', 'journalist-training', NULL, '', '', '', 'live', 'a:1:{i:0;i:98;}', NULL, 'Sorong', NULL, '-0.88312,131.26831', 0, 1433821466, 1450038678, 0, NULL, 1, 'Journalist Training', 'Writing for conservation issues. This training is for journalists.', NULL, NULL, 1433894400, 0, 0, 'Peggy Mariska', 'cpmariska@tnc.org', '', 'nprabowo@TNC.ORG', 'TNC Sorong Office', 'Jl. Sultan Hasanuddin No. 31, Klademak II A, Sorong 98413, Papua Barat', 'yes', 2015),
(27, 0, 'MY', 'world-oceans-day-and-coral-triangle-day-celebration-2015-colouring-contest-for-school-children', NULL, '', '', '', 'live', 'a:4:{i:0;i:100;i:1;i:101;i:2;i:102;i:3;i:103;}', 'a:3:{i:0;i:104;i:1;i:105;i:2;i:106;}', 'Kota Kinabalu', NULL, '6.03713,116.11891', 0, 1433823993, 1450038687, 0, NULL, 1, 'World Oceans Day & Coral Triangle Day Celebration 2015 - Colouring Contest for School Children', '<div  rgb(0, 0, 0); font-family: Calibri, sans-serif; font-size: 14px; line-height: normal;">\n<div>In celebration of the World Ocean&#39;s Day (8 June) and the Coral Triangle Day (9 June), a Colouring Contest for School Children with the them &quot;Healthy Ocean, Healthy Planet&quot; will be held on&nbsp;<span class="aBn" data-term="goog_976479471"  1px; border-bottom-style: dashed; border-bottom-color: rgb(204, 204, 204); position: relative; top: -2px; z-index: 0;" tabindex="0"><span class="aQJ"  relative; top: 2px; z-index: -1;">12 June 2015</span></span>&nbsp;from 8:00-1130 am at the Aquarium and Marine Museum. There will be two categories, namely Primary 1 to 3 (category 1) and Primary 4 to 6 (category 2). Only 30 students per category can join on a first come, first serve basis.</div>\n\n<div>&nbsp;</div>\n\n<div>Winners will receive books and stationary voucher up to RM 1,200. For each category, the first place winners will receive RM 300, 2nd place will get RM 200, and 3rd place will get RM 100. There will also be quizzes and side games for the public with exciting prizes!&nbsp;</div>\n\n<div>&nbsp;</div>\n\n<div>The organisers are Jabatan Perikanan Sabah, Coral Triangle Initiative on Coral Reefs, Fisheries and Food Security, ADB, GEF, MOSTI, and UMS.</div>\n</div>', NULL, NULL, 1434067200, 0, 0, 'Sabrina Makajil', 'sabrina.makajil@sabah.gov.my', '', '', 'Aquarium & Marine Museum, Borneo Marine Research Institute, Universiti Malaysia Sabah', 'Kota Kinabalu', 'yes', 2015),
(25, 0, 'ID', 'journalist-discussion', NULL, '', '', '', 'live', NULL, NULL, 'Manokwari', NULL, '-0.86114,134.06191', 0, 1433821675, 1450038679, 0, NULL, 1, 'Journalist Discussion', 'Journalist discussion about conservation in Bird&#39;s Head Seascape', NULL, NULL, 1433980800, 0, 0, 'Peggy Mariska', 'cpmariska@tnc.org', '', 'nprabowo@TNC.ORG', 'Manokwari', '', 'yes', 2015),
(26, 0, 'ID', 'cleaning-plastic-wastes', NULL, '', '', '', 'live', 'a:1:{i:0;i:99;}', NULL, 'Sorong', NULL, '-0.86667,131.25000', 0, 1433821995, 1450038679, 0, NULL, 1, 'Cleaning Plastic Wastes', 'Youth, journalist, representatives from waste bank, and more will be participating in this activity.', NULL, NULL, 1434153600, 0, 0, 'Peggy Mariska', 'cpmariska@tnc.org', '', 'nprabowo@TNC.ORG', 'Beaches around the walls of the city of Sorong', '', 'yes', 2015),
(28, 0, 'ID', '8th-thought-leadership-forum', NULL, '', '', '', 'live', 'a:1:{i:0;i:107;}', NULL, 'Jakarta Selatan', NULL, '-6.24438,106.79532', 0, 1433841971, 1433843770, 0, NULL, 0, '8th Thought Leadership Forum', 'The theme of the forum &quot;Healthy Marine Ecosystem for High Value Food and Ecotourism Industry&quot;.', NULL, NULL, 1433721600, 0, 0, 'Peggy Mariska', 'cpmariska@tnc.org', '', 'indonesia@tnc.org', 'Gran Mahakam Hotel', 'Jalan Mahakam I No. 6, Blok M', 'yes', 2015),
(29, 0, 'MY', 'citizen-science-outreach-program-on-climate-change-adaptation-for-tourism-industry', NULL, '', '', '', 'live', 'a:1:{i:0;i:150;}', 'a:1:{i:0;i:151;}', 'Kota Kinabalu', NULL, '5.96828,116.05815', 0, 1434090636, 1450038686, 0, NULL, 1, 'CITIZEN SCIENCE OUTREACH PROGRAM ON CLIMATE CHANGE ADAPTATION FOR TOURISM INDUSTRY', 'The &ldquo;Citizen Science Outreach Program on Climate Change Adaptation for the Tourism Industry&rdquo; is an awareness event for the Sutera Harbour (SHGCC/SHG Sdn Bhd.) staffs and public on the impact of climate change on the marine biodiversity. SHGCC/SHG Sdn Bhd. will become the first member of the tourism industry to implement this awareness and outreach programme on climate change and marine biodiversity. At the same time and in line with practicing sustainable conservation in the tourism industry, BMRI, UMS will engage in a transfer of knowledge with SHGCC/SHG Sdn Bhd. on the importance of continuous water quality monitoring to ensure that the waters surrounding the resort is kept within the acceptable standards of good water quality and also on the species identification of marine biodiversity available around the resort bay area.', NULL, NULL, 1434499200, 0, 0, 'WAHIDATUL HUSNA ZULDIN', 'wahidatul@ums.edu.my', '', 'wahidatul@ums.edu.my', 'Marina Bay, Sutera Harbour Resort', '1 Sutera Harbour Boulevard Kota Kinabalu 88100 Sabah, Malaysia', 'yes', 2015),
(30, 0, 'ID', 'celebration-in-sanur-mangrove-planting-beach-cleanup-recycling-art-class-fun-learning-class', NULL, '', '', '', 'live', 'a:1:{i:0;i:191;}', 'a:6:{i:0;i:192;i:1;i:193;i:2;i:194;i:3;i:195;i:4;i:196;i:5;i:197;}', 'Denpasar', NULL, '-8.65000,115.21667', 0, 1434614983, 1450038685, 0, NULL, 1, 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', 'Open to Public - Mertasari Beach, Sanur - 28 June 2015 - 07.00am to 06.00pm Beach Clean Up - Mangrove Adoption and Planting - Picnic Breakfast - Kids Outdoor Fun Learning (The Amazing Ocean Race) - Sanur Sunday Market - Recycled Art Workshop with Made Bayak - Kids Kecak Dance - Music Factory', NULL, NULL, 1435449600, 0, 0, 'Coral Triangle Center', 'ebimo@coraltrianglecenter.org', '', 'ebimo@coraltrianglecenter.org', 'Mertasari Beach - Sanur', 'Jalan Pantai Mertasari, Kota Denpasar, Bali, Indonesia', 'yes', 2015),
(31, 0, 'ID', 'sukseskan-coral-triangle-day-2015', NULL, '', '', '', 'live', 'a:2:{i:0;i:202;i:1;i:203;}', 'a:2:{i:0;i:204;i:1;i:205;}', 'Tatapaan', NULL, '1.28964,124.54796', 0, 1434968502, 1450038681, 0, NULL, 1, 'Sukseskan Coral Triangle Day 2015', 'Arakan villagers in South Minahasa celebrate the Coral Triangle Day with the theme: The sea is not a garbage dump, let us protect it for future generations (Laut bukan tempat sampak marikita lata jaga dan lestarikan). Activities include mangrove planting, coloring contest for children, seaweed culture and pearl culture showcases, and government sponsorship of fisheries and livelihood packages for women groups and local beneficiaries. The Ministry of Maritime Affairs and Fisheries and the Coral Triangle Initiative - Southeast Asia project organized the event.', NULL, NULL, 1433808000, 1434067200, 0, 'Dana Salonoy', 'ctisoutheastasia@gmail.com', '', '', 'Arakan, Tatapaan, South Minahasa, North Sulawesi', '', 'yes', 2015),
(33, 0, 'FJ', 'take-a-tour-through-the-coral-triangle', NULL, '', '', '', 'live', NULL, NULL, 'Sofitel Fiji Resort and Spa', '', '', 0, 1435731105, 1450038681, NULL, NULL, 1, 'Take a tour through the Coral Triangle', 'An Educational Holiday in Paradise! ​Learn about the Coral Triangle and how WWF is working with partners and communities to protect this unique region, rich in birodiversity and culture. The Sofitel Fiji Resort and Spa will host a week-long exhibition showcasing the many natural wonders of the Coral Triangle and the efforts that they are making in contributing to ensuring it&#39;s protection. There will be colourful and easy to understand infographics, brochures and factsheets to assist you in understanding how you can be part of the solution.<br />', NULL, NULL, 1402272000, 1402704000, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2014),
(35, 0, 'ID', 'general-lecture', NULL, '', '', '', 'live', NULL, NULL, 'Makassar', '', '', 0, 1436436126, 1450038676, NULL, NULL, 1, 'General lecture', '​The general lecture is open only to students and faculty members of University of Hasanuddin, local NGOs, local government of South Sulawesi Province and Marine Affairs and Fisheries. The lecture will carry the topic: &quot;The Roles of CTI CFF in Coral Reefs Conservation based on Community&quot;.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Universitas Hasanuddin, Makassar', '', NULL, 2014),
(36, 0, 'ID', 'national-forum-of-coastal-regents-in-coral-triangle-area', NULL, '', '', '', 'live', NULL, NULL, 'Lovina Singaraja', '', '', 0, 1436436251, 1450038685, NULL, NULL, 1, 'National Forum of Coastal Regents in Coral Triangle Area', 'This forum is intended as an opportunity to develop implementation plans for CTI-CFF at each location for the participants in order to achieve the goals of the National Plan of Action. The activity is called: National Forum of Coastal Regents in Coral Triangle area, with the theme of &quot;Optimizing the Role of Local Governments in Implementing CTI Plans of Action at the National Level (National Plan of Action, NPOA)&quot;.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Banyualit Spa & Resort', '', NULL, 2014),
(37, 0, 'ID', 'outreach-in-10-outer-islands-sulawesi-selatan', NULL, '', '', '', 'live', NULL, NULL, '10 Outer Islands', '', '', 0, 1436436303, 1450038683, NULL, NULL, 1, 'Outreach in 10 Outer Islands - Sulawesi Selatan', '​Outreach will be held in 10 Outer Islands (Alor, Masela, Wetar, Maratua, Kakarotan, Anambas, Marore, Bepondi, Panambulai, Manterau dan Sebatik) to raise awarness on coral reefs conservation.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, '10 Outer Islands', '', NULL, 2014),
(38, 0, 'ID', 'outreach-to-school-children-and-community-in-10-outer-islands', NULL, '', '', '', 'live', NULL, NULL, '10 Outer Islands', '', '', 0, 1436436368, 1450038686, NULL, NULL, 1, 'Outreach to school children and community in 10 Outer Islands', '​Outreach to school children and community in 10 Outer Islands (Alor, Masela, Wetar, Maratua, Kakarotan, Anambas, Marore, Bepondi, Panambulai, Manterau dan Sebatik) to raise awarness on coral reefs conservation.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, '10 Outer Islands', '', NULL, 2014),
(39, 0, 'ID', 'film-screening', NULL, '', '', '', 'live', NULL, NULL, 'Mubur Village, Anambas and Tanjung Harapan, Maratua Island', '', '', 0, 1436436424, 1450038720, NULL, NULL, 1, 'Film screening', '​Film screening with the theme of marine environment, climate change and coral reefs in Anambas Island and Maratua Island. This activity aims to provide education to the community through visualization and story-telling. The event is open to local villagers and school children.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Mubur Village, Anambas and Tanjung Harapan, Maratua Island', '', NULL, 2014),
(40, 0, 'ID', 'campaign-and-community-commitment-signing-on-coral-reef-conservation-efforts-makassar', NULL, '', '', '', 'live', NULL, NULL, 'Makassar', '', '', 0, 1436436472, 1450038728, 0, NULL, 1, 'Campaign & community commitment signing on coral reef conservation efforts - Makassar', 'The event involves a campaign and a public signing as a token of of public commitment to care for coral reef conservation efforts. This activity also marks the launch of the Indonesian Coral Reef Action Network (I-CAN), a movement of various civil society elements to work together in the management of coral reef ecosystems in an integrated manner.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Losari Beach, Makassar', '', NULL, 2014),
(41, 0, 'ID', 'underwater-and-beach-cleanup-and-community-leader-award-pajenekang-island', NULL, '', '', '', 'live', NULL, NULL, 'Pajenekang Island, Pangkep', '', '', 0, 1436436547, 1450038726, NULL, NULL, 1, 'Underwater & beach clean-up, and Community Leader Award - Pajenekang Island', 'Underwater and beach clean up at Pajanekang beach, will be involve the whole people who reside in Pajenekang Island. The Community Leader Award will be awarded to Mr. Daeng Santa, a community leader who has shown great concern with the coral reefs conservation in the Pajenekang Island.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Pajenekang Island, Pangkep', '', NULL, 2014),
(42, 0, 'ID', 'kids-competition-raja-ampat-papua-west-papua-indonesia', NULL, '', '', '', 'live', NULL, NULL, 'Raja Ampat', '', '', 0, 1436437076, 1450038721, NULL, NULL, 1, 'Kids competition, Raja Ampat, Papua, West Papua, Indonesia', 'Drawing and writing competition for kids to raise awareness on importance of marine resources for their way of life.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'TNC Field Stations in Southeast Misool and Kofiau', '', NULL, 2014),
(43, 0, 'ID', 'media-discussion-on-coral-reef-management-for-peoples-welfare-in-east-nusa-tenggara', NULL, '', '', '', 'live', NULL, NULL, 'Kupang, East Nusa Tenggara', '', '', 0, 1436437132, 1450038727, NULL, NULL, 1, 'Media Discussion on Coral Reef Management for People''s Welfare in East Nusa Tenggara', 'In collaboration with environmental journalists (under the coordination of the Society of Indonesian Environmental Journalists/SIEJ) in Kupang, East Nusa Tenggara, CT Day celebrated through a media discussion about the the province&#39;s natural resources and how they are managed to benefit people/people&#39;s welfare. Resource persons/speakers were from the provincial government, academia, environmental NGOs, fishers association and media editors.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Timor Express newspaper''s office', '', NULL, 2014),
(44, 0, 'ID', 'mini-exhibition-at-international-seminar-on-biodiversity', NULL, '', '', '', 'live', NULL, NULL, 'Waisai, Raja Ampat', '', '', 0, 1436437205, 1450038722, NULL, NULL, 1, 'Mini exhibition at International Seminar on Biodiversity', '', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Waisai, Raja Ampat, West Papua, Indonesia', '', NULL, 2014),
(45, 0, 'ID', 'twitter-campaign-indonesia-and-beyond', NULL, '', '', '', 'live', NULL, NULL, 'Jakarta', '', '', 0, 1436437254, 1450038718, NULL, NULL, 1, 'Twitter Campaign - Indonesia and Beyond', 'We popularize hashtag #CoralTriangleDay &amp; #CTDay2014 to wider public by engaging with some twitter buzzers namely @wegoID (51.8K followers), @TravelingID (17.3K followers) and @ArievRahman (94.6K followers), @Liputan9 (732K followers)<br />', NULL, NULL, 1401494400, 0, 0, NULL, NULL, NULL, NULL, 'Jakarta', '', NULL, 2014),
(46, 0, 'ID', 'talk-show-on-protect-the-coral-triangle-provide-more-fish-for-the-world', NULL, '', '', '', 'live', NULL, NULL, 'Jakarta', '', '', 0, 1436437308, 1450038727, NULL, NULL, 1, 'Talk Show on "Protect the Coral Triangle, Provide more fish for the world"', 'The talkshow is hosted by US Embassy. Three panelists Gondan Renosari from TNC, Hendra Yusran Siry from Ministry of Marine Affairs and Fisheries and Arie Parikesit, a culinary expert, will explain the threats and advantages of coral reefs for human being. The event will invite students and open for public as well. www.atamerica.or.id.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, '@america, Pacific Place 3rd Fl.', '', NULL, 2014),
(47, 0, 'ID', 'ctc-ct-day-2014-celebration-open-house-educative-games-for-children', NULL, '', '', '', 'live', NULL, NULL, 'Sanur, Bali', '', '', 0, 1436437395, 1450038727, NULL, NULL, 1, 'CTC CT Day 2014 Celebration Open House - Educative games for children', '​10.00 - 11.00 Fun Learning Class for Kids<br />\n11.00 - 12.00 Sustainable Fishing Game for Kids<br />\n12.00 - 13.00 Lunch and Prize Ceremony', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'CTC Office', 'Jl. Danau Tamblingan 78', NULL, 2014),
(48, 0, 'ID', 'ctc-ct-day-2014-celebration-fun-learning-and-games-for-kids', NULL, '', '', '', 'live', NULL, NULL, 'Nusa Lembongan, Bali', '', '', 0, 1436437454, 1450038726, NULL, NULL, 1, 'CTC CT Day 2014 Celebration Fun Learning and Games for Kids', '​10.00 - 11.00 Fun Learning Class for Kids<br />\n11.00 - 12.00 Sustainable Fishing Game for Kids<br />\n12.00 - 13.00 Lunch and Prize Ceremony<br />', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Nusa Lembongan, Bali', '', NULL, 2014),
(49, 0, 'MY', 'coral-triangle-day-with-universiti-malaysia-sabah-and-borneo-marine-research-institute', NULL, '', '', '', 'live', NULL, NULL, 'Kota Kinabalu', '', '', 0, 1436437633, 1450038728, NULL, NULL, 1, 'Coral Triangle Day with Universiti Malaysia Sabah & Borneo Marine Research Institute', 'CTI Posters exhibition and CTI quizzes activities are open to all visitors of UMS Aquarium and Museum between 23-26 June 2014. The main purpose of the activities is to introduce the role of CTI and update activities of each CTI goals in Malaysia to public. Citizen Science workshop (for adults) and Marine Resources mapping workshop (for children age 11-12) are part of awareness programs jointly organized by various local CTI Malaysia stakeholders (Wildlife Department, WWF and Reef Check Malaysia).', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Borneo Marine Research Institute', 'Universiti Malaysia Ssabah, Jalan UMS, 88400', NULL, 2014),
(50, 0, 'MY', 'first-regional-sustainable-seafood-festival-myssf14', NULL, '', '', '', 'live', NULL, NULL, 'Kuala Lumpur', '', '', 0, 1436437673, 1450038726, NULL, NULL, 1, 'First regional Sustainable Seafood Festival (MySSF14)', '​No fishy business... just plenty of fun at this event which is held in conjunction with the World Oceans Day (8 June) and Coral Triangle Day (9 June). The festival is also celebrated in Singapore, Japan and Indonesia, as part of WWF&rsquo;s ongoing initiative to raise awareness on the global decline of fish stocks and to collaborate with the government, businesses and local communities towards achieving a sustainable fisheries management.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'The Intermark', '', NULL, 2014),
(51, 0, 'MY', 'sustainable-seafood-selfie-photo-contest', NULL, '', '', '', 'live', NULL, NULL, 'Kuala Lumpur', '', '', 0, 1436437821, 1450038725, NULL, NULL, 1, 'Sustainable Seafood Selfie Photo Contest', 'Thinking of where to bring your Dad for the upcoming Fathers&#39; Day? Participating hotels will showcase new, authentic recipes using sustainable seafood choices during the festival. Here&rsquo;s your chance to enjoy delicious and delectable seafood dishes specially prepared by these hotels using responsibly-harvested seafood.&nbsp;<br />', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, '4 Participating hotels in Malaysia', '', NULL, 2014),
(52, 0, 'MY', 'be-a-sos-champion-create-the-saveourseafood-trend-', NULL, '', '', '', 'live', NULL, NULL, 'Malaysia', '', '', 0, 1436437956, 1450038725, NULL, NULL, 1, 'Be a S.O.S Champion: Create the #saveourseafood trend! -', 'We recognise that members of the public are our champions and spokespersons for this campaign. Here&rsquo;s your chance to champion the Sustainable Seafood Movement by utilising social media platforms. From June onwards, snap a photo and upload it through Instagram and hashtag #saveourseafood and mention @sos_my in the caption. You will help make global citizens aware of the sustainable seafood movement we are having here in Malaysia. YOU can decide what seafood is served on the fish counters and in restaurants. Be a part of the cause and Save Our Seafood!', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Malaysia', '', NULL, 2014),
(53, 0, 'PH', 'coral-triangle-study-tour-bataan-philippines', NULL, '', '', '', 'live', NULL, NULL, 'Bataan', '', '', 0, 1436438026, 1450038723, NULL, NULL, 1, 'Coral Triangle Study Tour - Bataan, Philippines', 'Study Tour to be organized by the NCCC for Grade 8 students to enhance the awareness of students on coastal and marine resources conservation.<br />', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Bantay Pawikan Conservation Inc. site', '', NULL, 2014),
(54, 0, 'PH', 'kickoff-ceremony-quezon-city-the-philippines', NULL, '', '', '', 'live', NULL, NULL, 'Quezon City', '', '', 0, 1436438069, 1450038723, NULL, NULL, 1, 'Kick-Off Ceremony - Quezon City, the Philippines', 'IEC materials were placed around the Ninoy Aquino Parks providing information on the Coral Triangle and the Philippines&#39; celebration to the CT Day.<br />', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Ninoy Aquino Parks and Wildife Center, Diliman, Quezon City', '', NULL, 2014),
(55, 0, 'PH', 'information-campaing-on-ct-day-and-marine-environment', NULL, '', '', '', 'live', NULL, NULL, 'Santa Fe', '', '', 0, 1436438144, 1450038724, NULL, NULL, 1, 'Information campaing on CT Day and marine environment', '<strong>7 June</strong> - this day&#39;s event will focus on the children of the islands, where a film showing and games about the ocean will be conducted, and it will culminate with the feeding program.<br />\n<strong>8 June</strong> - we will focus on the fishers, where a mass will be held, followed by talks and presentations.<br />\nActivities will be conducted by the Santo Nino Augustinian Social Development Foundation and Sea Knight', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Barangay Kinatarcan, Sante Fe, Bantayan Island', '', NULL, 2014),
(56, 0, 'PH', 'agio-forum-on-coral-triangle-initiative-and-coral-triangle-day', NULL, '', '', '', 'live', NULL, NULL, 'Cebu', '', '', 0, 1436438185, 1450038724, NULL, NULL, 1, 'Agio Forum on Coral Triangle Initiative and Coral Triangle Day', 'The forum was well attended by partner agencies. It was hosted by the Philippine Information Agency and print, TV and radio media was at hand to cover the event. The event was also covered live by a radio station. Hon. Thadeo Ouano, Provincial Board Member, was also present to discuss the Provincial Resolution on CT Day that he authored. Philippine Navy, Bureau of Fishries and Aquatic Resources and Sea Knights completed the panel of discussants for the event. &nbsp;', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'The Philippine Information Agency Office Region 7, Gorordo Avenue, Cebu City', '', NULL, 2014),
(57, 0, 'PH', 'month-of-the-ocean-culmination-activity-and-launching-of-coral-triangle-day-celebration', NULL, '', '', '', 'live', NULL, NULL, 'Barangay', '', '', 0, 1436438247, 1450038728, NULL, NULL, 1, 'Month of the Ocean Culmination Activity and Launching of Coral Triangle Day Celebration', 'This was hosted by the Department of Environment and Natural Resources Region 7. Press people (TV, print and radio) covered the event. To highlight the launching of the CT Day, Autonomous Reef Monitoring Structure was established inside the Alegria Marine Sanctuary. Dir. Mundita Lim, representatives from USAID and NOAA were present to witness the occasion.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Barangy Alegria Covered Court, Barangay Alegria, Municipality of Cordova', '', NULL, 2014),
(58, 0, 'PH', 'ct-day-celebration-in-pulchra-resorts', NULL, '', '', '', 'live', NULL, NULL, 'Barangay San Isidro', '', '', 0, 1436438334, 1450038721, NULL, NULL, 1, 'CT Day celebration in Pulchra Resorts', 'The day started with a beach clean up at 6:30 a.m. by all the employees of Pulchra Resorts. It was followed by a short parade at 8:00 a.m. inside the resort with selected employees wearing environment-themed costumes. Then each group presented a dance drama with the theme: Caring for the Environment. This was hosted by Pulchra management and family members of the entire staff were invited.&nbsp;', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Pulchra Resorts, Barangay San Isidro, Municipality of San Fernando', '', NULL, 2014),
(59, 0, 'PH', 'ct-day-advance-celebration-doong-island-bantayan-cebu', NULL, '', '', '', 'live', NULL, NULL, 'Cebu', '', '', 0, 1436438368, 1450038726, NULL, NULL, 1, 'CT Day Advance Celebration - Doong Island, Bantayan, Cebu', 'Sea Knights together with the people of Doong Island, Bantayan Cebu will join the celebration of Coral Triangle Day in an advance celebration on June 3, 2014 at Doong Island, Bantayan, Cebu. This event is open to public, and is geared towards the preservation of what&#39;s left in Bantayan Seas, restoration of the damaged underwater treasures and making Bantayanons the greatest stewards of their seas.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Doong Island, Bantayan, Cebu', '', NULL, 2014),
(60, 0, 'PH', 'sustainable-seafood-event-costa-del-hamilo', NULL, '', '', '', 'live', NULL, NULL, 'Batangas', '', '', 0, 1436438422, 1450038722, NULL, NULL, 1, 'Sustainable Seafood Event - Costa Del Hamilo', 'Culinary sensation Bobby Chinn celebrates Coral Triangle Day with WWF for the third time since its launch on in June 9, 2012. WWF-Philippines&#39; ecotourism partner Hamilo Coast shall host the event at the Pico de Loro Cove in Batangas. Bobby will prepare three dishes that will help promote better seafood alternatives and viable solutions that protect the Coral Triangle region&#39;s marine wealth. Through this partnership with Bobby Chinn and Hamilo Coast for Coral Triangle Day, WWF hopes to highlight the need to rehabilitate the Coral Triangle&rsquo;s reefs from decades of damage and build a sustainable means of livelihood for fishermen.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Arribada Lounge, Pico de Loro Cove, Hamilo Coast Nasugbu, Batangas', '', NULL, 2014),
(61, 0, 'PH', 'coral-triangle-day-and-philippines-environment-month', NULL, '', '', '', 'live', NULL, NULL, 'Manila', '', '', 0, 1436438464, 1450038725, NULL, NULL, 1, 'Coral Triangle Day and Philippines Environment month', 'Launching of book for youth &quot;In Tropical Seas&quot;, which describes the various tropical marine ecosystems and their importance and connectivity, and associated art exhibition of underwater paintings. Sponsored by THE CTI, Phil National CTI Coordinating Committee, Australian Embassy, The Podium, Zobel Foundation and UGEC printers.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'The Podium, Manila, Philippines', '', NULL, 2014),
(62, 0, 'PH', 'ocean-jam-the-philippines', NULL, '', '', '', 'live', NULL, NULL, 'Manila', '', '', 0, 1436438508, 1450038721, NULL, NULL, 1, 'Ocean Jam - the Philippines', 'An Ocean Jam will be organized by the Department of Environment and Natural Resources to bring together local artists and bands to promote awareness on Coastal and Marine Resources management.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Manila', '', NULL, 2014),
(63, 0, 'SB', 'public-awareness-honiara-solomon-islands', NULL, '', '', '', 'live', NULL, NULL, 'Point Cruz, Honiara', '', '', 0, 1436438574, 1450038722, NULL, NULL, 1, 'Public Awareness - Honiara, Solomon islands', '​Join the Ministry of Environment, Climate Change, Disaster Management &amp; Meteorology (MECDM) celebrate the World Environment Day, World Oceans Day and Coral Trinagle day by with the launch of Mataniko river Clean up and rehabilitation program. The day will start with official launch of the program, followed by Demonstrations and awareness from partners, and closed with a live band performance and youth market. The event is sponsored by the MECDM and South Pacific Community - Youth at work program.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'National Art Gallery, Point Cruz, Honiara, Solomon islands', '', NULL, 2014),
(64, 0, 'SB', 'radio-quiz-show-honiara-solomon-islands', NULL, '', '', '', 'live', NULL, NULL, 'Honiara', '', '', 0, 1436438621, 1450038722, NULL, NULL, 1, 'Radio Quiz show - Honiara, Solomon islands', 'Celebrate Coral Triangle Day with PAOA FM Radio by participating in the quiz. This will be done throught the day and including the Kids Happy Hour. This is supported by WWF South Pacific.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'PAOA FM Radio Station, Panatina Plaza, Honiara, Solomon Islands', '', NULL, 2014),
(65, 0, 'SB', 'presenting-gift-pack-to-national-leaders-honiara-solomon-islands', NULL, '', '', '', 'live', NULL, NULL, 'Honiara', '', '', 0, 1436438669, 1450038720, NULL, NULL, 1, 'Presenting Gift Pack to National Leaders - Honiara, Solomon islands', 'SI NCC will visit key M&icirc;nistries and the Prime Minister to present a gift pack containing CT Day promotional items, flash drive with key CTI documents.<br />', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Ministry of Fisheries and Marine Resources', '', NULL, 2014),
(66, 0, 'TL', 'coral-triangle-day-celebration-dili', NULL, '', '', '', 'live', NULL, NULL, 'Dili', '', '', 0, 1436438717, 1450038718, NULL, NULL, 1, 'Coral Triangle Day Celebration - Dili', '75 children from Tasi-Tolu Primary School and 5 teachers will participate in a beach clean-up. 50 government staff from the secretary of state of fisheries will also attend. The programs in this event will be a beach clean-up, as well as a diving programme to clean the litter from the reef. Following these activities CI-TL will provide morning tea to participants. CI-TL has printed tee-shirts and stickers with CTI, CI an TL goverment logo on them to distribute to participants. All of the CI staff will participate in this event.', NULL, NULL, 1402272000, 0, 0, NULL, NULL, NULL, NULL, 'Tasi Tolu, Dili', '', NULL, 2014),
(67, 0, 'ID', 'beach-cleanup-coloring-competition-talentshow-traditional-by-children', NULL, '', '', '', 'live', NULL, NULL, 'Tuban, Kuta, Bali', '', '', 0, 1436495015, 1450038698, 0, NULL, 1, 'Beach clean-up, Coloring competition, Talentshow, Traditional musical by children from the school for the deaf', 'Join the fun of the beach clean-up and a traditional musical performed by children from the school for the deaf in Segara Beach on June 8! The beauty of a clean beach and the musical will inspire you to make great drawings for the coloring competition. Don&rsquo;t go home too early because you might just miss the debut of a future superstar in the talentshow! &nbsp;', NULL, NULL, 1339113600, 0, 0, NULL, NULL, NULL, NULL, 'Jerman Beach (Segara Beach)', '', NULL, 2012),
(69, 0, 'ID', 'sustainable-seafood', NULL, '', '', '', 'live', NULL, NULL, 'South of Bali', '', '', 0, 1436504805, 1450038701, 0, NULL, 1, 'Sustainable Seafood', 'Do you know what sustainable sourced seafood tastes like? It tastes guilt-free! Try it for lunch and/or dinner at some of the top restaurants in Bali as they showcase special responsibly-sourced seafood menus for the day. Try it in the following restaurants:&nbsp;Gado Gado Restaurant, Metis Restaurants, La Lucciola, Desa Seni, Alila Ubud, Alila Manggis, Alila villas Soori Tabanan, and Alila villas Uluwatu.&nbsp;', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Various restaurants in South of Bali', '', NULL, 2012),
(71, 0, 'ID', 'family-day-with-responsibly-sourced-seafood-barbeque', NULL, '', '', '', 'live', NULL, NULL, 'Uluwatu, Bali', '', '', 0, 1436505951, 1450038705, NULL, NULL, 1, 'Family Day with Responsibly -Sourced Seafood Barbeque', 'Bring the family and be astonished by the beautiful view from the beach club. Enjoy massages, snacks and drinks while the kids get educated about reefs. Don&rsquo;t have a big lunch because the barbeque will be ready to have you experiencing the great taste of guilt-free sustainable sourced seafood. Need more action on a Saturday? Enjoy the performance by the Balinese capoeira team!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'The Bukit', '', NULL, 2012),
(72, 0, 'ID', 'beach-party', NULL, '', '', '', 'live', NULL, NULL, 'Uluwatu, Bali', '', '', 0, 1436506069, 1450038700, NULL, NULL, 1, 'Beach Party', 'Party for a cause! Join the party with internationally-renowned DJ&rsquo;s such as Stevie G which will be held at Finn&rsquo;s Beach Club (Semara). Get a chance to meet some hot celebrities such as Bobby Chinn and Nadine Chandrawinata and party with them for the night! But don&rsquo;t forget to clean-up!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'The Bukit', '', NULL, 2012),
(73, 0, 'ID', 'film-festival', NULL, '', '', '', 'live', NULL, NULL, 'Canggu, Bali', '', '', 0, 1436507944, 1450038699, NULL, NULL, 1, 'Film Festival', 'For this special occasion village resort Desa Seni invites everyone to come and see &lsquo;Whale Dreamers&rsquo;, &lsquo;The mirror never lies&rsquo; (recently awarded) and &lsquo;Shark Sanctuary&rsquo;. Watch these educational films and enjoy the amazing shots of the coral reefs!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Desa Seni', '', NULL, 2012),
(74, 0, 'ID', 'educational-open-house', NULL, '', '', '', 'live', NULL, NULL, 'Sanur, Bali', '', '', 0, 1436508114, 1450038701, NULL, NULL, 1, 'Educational Open House', 'The Coral Triangle Centre opens its doors to school children (aged 9-15) to teach them about the marine environment through fun and engaging games and activities. They will also get to watch the BBC Blue Planet Series on Coral Sea. Get your kids started young and teach them the importance of healthy seas!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Coral Triangle Center', '', NULL, 2012),
(75, 0, 'ID', 'bazaar', NULL, '', '', '', 'live', NULL, NULL, 'Jimbaran Bay, Bali', '', '', 0, 1436508190, 1450038698, NULL, NULL, 1, 'Bazaar', 'Want to not just help the environment but also yourself? Go shop! Come to the bazaar and check out all the eco-themed merchandise for sale by different brands coming together to support ocean conservation! Don&rsquo;t forget your wallet!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Kedonganan Village', '', NULL, 2012),
(76, 0, 'ID', 'sustainable-seafood-cooking-show', NULL, '', '', '', 'live', NULL, NULL, 'Jimbaran Bay, Bali', '', '', 0, 1436508306, 1450038701, NULL, NULL, 1, 'Sustainable Seafood Cooking Show', 'Discover the many creative ways to cook sustainable sourced seafood as local restaurants whip up their best seafood dishes using only sustainable sourced ingredients in 24 community-based restaurants in Kedonganan. None other than celebrity chef Bobby Chin will join this show.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Kedonganan Village', '', NULL, 2012),
(77, 0, 'ID', 'sunset-beach-carnival', NULL, '', '', '', 'live', NULL, NULL, 'Jimbaran Bay, Bali', '', '', 0, 1436508368, 1450038701, NULL, NULL, 1, 'Sunset Beach Carnival', 'Groove to the beat of Udayana University&rsquo;s marching band as they perform on Kedonganan Beach, followed by an &ldquo;ogoh-ogoh&rdquo; parade by the youth of Kedonganan Village. Get your cameras ready as this is one local treat you wouldn&rsquo;t want to miss!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Kedonganan Village', '', NULL, 2012),
(78, 0, 'ID', 'environmentallysound-seafood-dinner-with-raffle', NULL, '', '', '', 'live', NULL, NULL, 'Jimbaran Bay, Bali', '', '', 0, 1436508431, 1450038703, NULL, NULL, 1, 'Environmentally-sound Seafood Dinner with Raffle', 'Enjoy a lovely dinner with entertainment by celebrities while supporting local marine conservation efforts by buying a ticket (or more) to a special Sustainable Sourced Seafood Dinner with Raffle for Rp. 250.000. Contact adnyanawindia@gmail.com for more information. All proceeds will go to the Turtle Conservation and Education Center for marine turtle conservation.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Kedonganan Village', '', NULL, 2012),
(79, 0, 'ID', 'workshop', NULL, '', '', '', 'live', NULL, NULL, 'Bali', '', '', 0, 1436508816, 1450038700, NULL, NULL, 1, 'Workshop', 'In celebration of Coral Triangle Day, the Ministry of Marine Affairs and Fisheries is organizing a workshop on conservation area institution, mangrove planting and coral reefs transplantation, and conservation area patrol on June 11, 12 and 26.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Nusa Lembongan', '', NULL, 2012),
(80, 0, 'ID', 'mission-trip', NULL, '', '', '', 'live', NULL, NULL, 'Saparua Island, Ambon, Maluku', '', '', 0, 1436508931, 1450038693, 0, NULL, 1, 'Mission Trip', 'Mission Trip is an activity to reach out to other islands especially to assist in the development of an IT facility for local schools and raise awareness on the importance of education for future generations. The second Mission Trip to the island on 16-19 May was conducted in celebration of Coral Triangle Day.', NULL, NULL, 1337126400, 1337385600, 0, NULL, NULL, NULL, NULL, 'Paperu and Sirisori Islam Villages', '', NULL, 2012),
(81, 0, 'ID', 'sea-carnival', NULL, '', '', '', 'live', NULL, NULL, 'North Sumatra', '', '', 0, 1436509014, 1450038699, NULL, NULL, 1, 'Sea Carnival', 'Join the sunset festivities on Weh Island in Northnern Sumatra from 4 to 8pm and take part in a costume competition, have your face painted, or get some fancy tattoo art on your body while enjoying a sumptuous seafood meal along the streets of Sabang. You can also sit back and enjoy a film showing on shark conservation produced by Gecko Studios and WCS or simply enjoy a musical performance by Rafli Kande-a highly gifted Acehnese musician and motivator who performs folklore songs with conservation messages. This sea carnival is organized by WCS in partnership with the Sabang Tourism Cultural Affairs, Fisheries and Marine Affairs, Department of Education, GAPAS student group, and local NGOs.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Weh Island', '', NULL, 2012);
INSERT INTO `default_ctd` (`id`, `revision_id`, `country`, `slug`, `category_id`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `organiser`, `sponsors`, `location`, `geolocation`, `geocoordinates`, `hide_map`, `created_on`, `updated_on`, `publish_on`, `slide`, `views`, `title`, `intro`, `body`, `notes`, `date_from`, `date_to`, `plan_to_come`, `submitted_by_name`, `submitted_by_email`, `website`, `contact_email`, `event_venue`, `venue_address`, `approve`, `event_year`) VALUES
(82, 0, 'ID', 'beach-cleanup-mangrove-and-coral-plantation-enviro-education', NULL, '', '', '', 'live', NULL, NULL, 'North Sulawesi', '', '', 0, 1436509152, 1450038707, 0, NULL, 1, 'Beach Cleanup, Mangrove and Coral Plantation, Enviro Education', 'A bunch of beach activities await you the whole day from 8am to 5pm in Poopoh Village. Start the day with a beach clean-up, followed by a series of entertaining activities including a puppet show, coloring contests, and other special games. Also get a chance to win some door prizes!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Poopoh Village', '', NULL, 2012),
(83, 0, 'ID', 'photo-exhibition-and-environmental-education', NULL, '', '', '', 'live', NULL, NULL, 'North Sulawesi', '', '', 0, 1436509230, 1450038697, NULL, NULL, 1, 'Photo Exhibition & Environmental Education', 'A whole day of environmental learning awaits children as the Wildlife Conservation Society conducts a photo exhibition, including a puppet show, coloring contests, and other special games!&nbsp;', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Manado City', '', NULL, 2012),
(84, 0, 'ID', 'sale-of-handicrafts-made-of-recycled-plastic-and-rubbish', NULL, '', '', '', 'live', NULL, NULL, 'Labuan Bajo, Flores', '', '', 0, 1436509392, 1450038697, NULL, NULL, 1, 'Sale of handicrafts made of recycled plastic and rubbish', 'Join the activities in Pede Beach and buy handicrafts made by local women from recycled plastic collected by school children. For collectors, we also have rubbish for sale! There will be prizes for kids that collect the most plastics and rubbish. &nbsp;', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Pede Beach', '', NULL, 2012),
(85, 0, 'ID', 'keep-pede-beach-clean-campaign', NULL, '', '', '', 'live', NULL, NULL, 'Labuan Bajo, Flores', '', '', 0, 1436509458, 1450038693, NULL, NULL, 1, 'Keep Pede Beach Clean Campaign', 'Knowledge is power! Be involved in the campaign to keep Pede Beach clean and become aware of the importance of the ocean in your life.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Pede Beach', '', NULL, 2012),
(86, 0, 'ID', 'live-music', NULL, '', '', '', 'live', NULL, NULL, 'Labuan Bajo, Flores', '', '', 0, 1436509525, 1450038692, NULL, NULL, 1, 'Live Music', 'Enjoy a performance of PlasticMan and Paradise bands. Children will also get to enjoy the performance of a pantomime!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Pede Beach', '', NULL, 2012),
(87, 0, 'ID', 'beach-clean-up-and-coloring-contest', NULL, '', '', '', 'live', NULL, NULL, 'Kupang, East Nusa Tenggara', '', '', 0, 1436509797, 1450038696, NULL, NULL, 1, 'Beach Clean up & Coloring Contest', 'In Kupang (Savu Sea National Marine Park), East Nusa Tenggara, the Department of Fisheries of Muhammadiyah University and TNC are holding a beach clean up and coloring contest for elementary school students at Paradiso Beach. This event is in conjunction with the Department&rsquo;s Research Week.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Paradiso Beach', '', NULL, 2012),
(88, 0, 'ID', 'beach-cleanup-and-film-screening', NULL, '', '', '', 'draft', NULL, NULL, 'Raja Ampat, West Papua', '', '', 0, 1436509907, 1436509907, NULL, NULL, 0, 'Beach Clean-Up and Film Screening', 'A series of beach clean-ups is happening in Deer Village in Kofiau and Boo Islands MPA, and Harapan Jaya Village in Southeast Misool MPA, organized by local communities and TNC. There will also be a screening of environmental films after.&nbsp;', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Kofiau and Boo Islands, and Southeast Misool', '', NULL, 2012),
(89, 0, 'ID', 'beach-clean-up', NULL, '', '', '', 'live', NULL, NULL, 'Sorong, West Papua', '', '', 0, 1436509982, 1450038692, NULL, NULL, 1, 'Beach Clean up', 'In Sorong, TNC, CI, and WWF and local media partners are holding a beach clean up on Dofior Beach.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Dofior Beach', '', NULL, 2012),
(90, 0, 'MY', 'sustainable-seafood-awareness-day', NULL, '', '', '', 'live', NULL, NULL, 'Penang', '', '', 0, 1436510050, 1450038696, NULL, NULL, 1, 'Sustainable Seafood Awareness Day', 'Golden Sands Penang is celebrating the Coral Triangle Day a day earlier on June 8 by hosting a sustainable seafood awareness day for fellow hoteliers, traders, and guests.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Golden Sands', '', NULL, 2012),
(91, 0, 'PH', 'underwater-documentation', NULL, '', '', '', 'live', NULL, NULL, 'Batangas', '', '', 0, 1436510138, 1450038694, NULL, NULL, 1, 'Underwater Documentation', 'Join Hamilo Coast and WWF-Philippines in celebrating the first Coral Triangle Day by exploring the wonders of the sea! Witness the fruits of a successful coastal resource management effort between WWF-Philippines and Costa del Hamilo. Set against a rich backdrop of vibrant corals, giant clams and curious sea turtles, their underwater photo documentation team will show you just what healthy Coral Triangle seas have to offer.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Hamilo Coast', '', NULL, 2012),
(92, 0, 'PH', 'underwater-cleanup', NULL, '', '', '', 'live', NULL, NULL, 'Lapu-lapu City', '', '', 0, 1436510227, 1450038695, NULL, NULL, 1, 'Underwater Cleanup', 'See a local community in action as more than 500 volunteers come together for a common cause&mdash;to clean the shores of Kedonganan Beach! Join the fun and the action!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Boyla Hotel, Maribago', '', NULL, 2012),
(93, 0, 'PG', 'mangrove-planting-activity-and-cleanup-mission', NULL, '', '', '', 'live', NULL, NULL, 'Port Moresby', '', '', 0, 1436510315, 1450038697, NULL, NULL, 1, 'Mangrove planting activity and clean-up mission', 'Bank South Pacific in partnership with UPNG in partnership with UPNG, Motupore Island Research Facility and Department of Environment and Conservation (DEC), are conducting a mangrove planting activity and clean-up mission in Gaba Gaba community in celebration of the Coral Triangle Day.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Gaba Gaba Community', '', NULL, 2012),
(94, 0, 'PG', 'beach-cleanup-video-screening-awareness-talk-poster-displays-and-a-live-drama-performance', NULL, '', '', '', 'live', NULL, NULL, 'Port Moresby', '', '', 0, 1436510393, 1450038698, NULL, NULL, 1, 'Beach clean-up, video screening, awareness talk, poster displays, and a live drama performance', 'WWF and partners are celebrating the Coral Triangle Day in Port Moresby with many exciting activities for the day including a beach clean-up, video screening, awareness talk, poster displays, and a live drama performance!&nbsp;', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Port Moresby', '', NULL, 2012),
(95, 0, 'SB', 'marine-environment-awareness-day', NULL, '', '', '', 'draft', NULL, NULL, 'Honiara', '', '', 0, 1436510492, 1436510492, NULL, NULL, 0, 'Marine Environment Awareness Day', 'The Solomon Islands Government, supported by environment NGOs and local businesses, has organized a fun-filled and informative day. Activities start at 9am and will include environment day speeches, cultural entertainment, kids&rsquo; arts and crafts, face painting, and quiz competitions. Information stalls will be available to the public during the day. Following the day&rsquo;s activities, the Coral Triangle Initiative (CTI) website will be launched during a dinner program themed Blue Oceans Night.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Kukum Campus Playing Field', '', NULL, 2012),
(96, 0, 'SB', 'beach-clean-up-interactive-talks-snorkeling', NULL, '', '', '', 'live', NULL, NULL, 'NW Ghizo', '', '', 0, 1436510552, 1450038697, NULL, NULL, 1, 'Beach clean up, Interactive talks, snorkeling', '24 Children and their parents ( staff from Dive Gizo) will travel by boat 25 minutes from Gizo town to Njari Island &nbsp;for a Coral Reef awareness day. We plan to have some small interactive talks and a look and learn on the reef using snorkeling gear . If need be, a beach clean up of flotsam debris will also take place.&nbsp;', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Njari Island', '', NULL, 2012),
(97, 0, 'TL', 'beach-walk', NULL, '', '', '', 'live', NULL, NULL, 'Dili', '', '', 0, 1436510615, 1450038693, NULL, NULL, 1, 'Beach Walk', 'A local group is organizing a beach morning walk to help raise awareness on the Coral Triangle and marine conservation. The walk starts at Caz Bah at 6.30am and finishes back at Beachside on return around 8.30am.', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Horta Hill', '', NULL, 2012),
(98, 0, 'ID', 'sustainable-seafood-awareness', NULL, '', '', '', 'live', NULL, NULL, 'Lombok', '', '', 0, 1436510990, 1450038695, NULL, NULL, 1, 'Sustainable Seafood Awareness', 'Anova Asia are holding activities at their Data Collection Project site in Labuhan Lombok which include:&nbsp;<br />\n- Information sharing of effort and benefit of data collection activities, waste management campaign, fisheries sustainability awareness for school children', NULL, NULL, 1339113600, 1339632000, 0, NULL, NULL, NULL, NULL, 'Labuhan Lombok', '', NULL, 2012),
(99, 0, 'ID', 'beach-clean-ups', NULL, '', '', '', 'live', NULL, NULL, 'Bali', '', '', 0, 1436512437, 1450038694, NULL, NULL, 1, 'Beach Clean Up', 'Join beach clean up in the area near you: Berawa Beach, Kedonganan, and Samuh Beach in Badung. Tejakula and Lovina in Buleleng.&nbsp;', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Various beaches in South and North of Bali', '', NULL, 2012),
(100, 0, 'ID', 'reef-clean-ups', NULL, '', '', '', 'live', NULL, NULL, 'Bali', '', '', 0, 1436512665, 1450038694, NULL, NULL, 1, 'Reef Clean Ups', '<table border="0" cellpadding="0" cellspacing="0" \n collapse;width:383pt" width="511">\n <tbody>\n  <tr height="85" >\n   <td class="xl65" height="85"  width="511">The divers of Sanur, Tejakula, Menjangan, Amed, Lembongan, Pemuteran, Samuh Beach come together for one special day to join forces and clean the reefs in their community!</td>\n  </tr>\n </tbody>\n</table>', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Various reefs around Bali', '', NULL, 2012),
(101, 0, 'ID', 'reef-monitoring-beach-and-reef-clean-up', NULL, '', '', '', 'live', NULL, NULL, 'Badung, Bali', '', '', 0, 1436512765, 1450038696, NULL, NULL, 1, 'Reef monitoring, Beach & Reef Clean up', 'The District Government of Badung, supported by Bali Provincial Government, Ministry of Marine Affairs and Fisheries Indonesia, CTI-CFF Indonesia NCC, Gahawisri Kab Badung and Yayasan Terumbu Karang Badung are organizing a beach and underwater clean-up, reef monitoring to celebrate the Coral Triangle Day on June 9. They will also be inaugurating the Badung Underwater Cultural Park Inaguration', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Samuh Beach', '', NULL, 2012),
(102, 0, 'ID', 'underwater-clean-up-and-beach-bbq', NULL, '', '', '', 'live', NULL, NULL, 'Bali', '', '', 0, 1436512820, 1450038696, NULL, NULL, 1, 'Underwater clean up & beach bbq', 'Have fun while cleaning up underwater and see first hand the beauty you&rsquo;re keeping intact by conserving the oceans. Work hard so you can win the prize! Followed by a beach barbeque. Enjoy!', NULL, NULL, 1339200000, 0, 0, NULL, NULL, NULL, NULL, 'Menjangan', '', NULL, 2012),
(103, 0, 'FJ', 'uprising-beach-resort-seafood-sunday', NULL, '', '', '', 'live', NULL, NULL, 'Fiji', '', '', 0, 1436534418, 1450038719, 0, NULL, 1, 'Uprising Beach Resort Seafood Sunday', 'What better way to spend your Sunday than at the beach! Join in the celebrations as Uprising Beach Resort celebrates World Oceans Day and Coral Triangle Day, supported by WWF South Pacific, with fun on the Beach. The day kicks off with a Beach Clean Up at 8am followed by a scrumptous Seafood Spread, where you&#39;ll also get the opportunity to try out fresh MSC Certified Albacore Tuna. Details of the Seafood Buffet as follows: 12.30 to 3.30pm (Bookings preferred) $70.00 per person Lunch Buffet, including a glass of house wine. Book a table of 4 or more and receive a complimentary bottle of House Wine. Menu Appetizers and Salads Bread Rolls Kokoda Lobster Bisque Soup Hydroponic Salad Bowl Octopus Salad Tuna Nicoise Salad Tomato, Olive and Cheese Salad Cucumber and Minted Yoghurt Sashimi Platter Hot Dishes Whole Baked Snapper Chilli Mud Crabs Prawns in Coconut Lolo Seafood Marinara Local Steamed Vegetables Dalo/ Roasted Potatoes Sweets Tropical Fruit Cuts Coconut Haystack Cookies.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Uprising Beach Resort', '', NULL, 2013),
(104, 0, 'ID', 'seminar-and-training-on-trash-management-techniques', NULL, '', '', '', 'live', NULL, NULL, 'Mataram, Lombok', '', '', 0, 1436534526, 1450038708, 0, NULL, 1, 'Seminar and Training on Trash Management Techniques', 'On &nbsp;June 7 from 9am to 5pm, there will be a seminar at Hotel Graha Ayu, Mataram to formulate innovative strategies to reduce marine debris in the Western Coast of Mataram Municipality. This will be attended by 50 to 100 people. There will be a training on Trash Management Techniques the next day,&lt;strong&gt; 8 June&lt;/strong&gt; from 9am to 12nn at Bintaro Beach, Ampenan.&nbsp;', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'Hotel Graha Ayu', '', NULL, 2013),
(105, 0, 'ID', 'film-screening-and-shadow-puppet-performance', NULL, '', '', '', 'live', NULL, NULL, 'Ampenan, Lombok', '', '', 0, 1436536546, 1450038718, 0, NULL, 1, 'Film screening and shadow puppet performance', 'On 8 June, there will be a film screening and shadow puppet performance from 8 to 11pm at Bintaro Beach, Ampenan. Over 1,000 people are expected to attend this event, which aims to promote mitigation &amp;amp; adaptation on coastal disaster and climate change.', NULL, NULL, 1370649600, 0, 0, NULL, NULL, NULL, NULL, 'Bintaro Beach', '', NULL, 2013),
(106, 0, 'ID', 'beach-cleanup-and-performances', NULL, '', '', '', 'live', NULL, NULL, 'Mataram, Lombok', '', '', 0, 1436536866, 1450038716, NULL, NULL, 1, 'Beach clean-up and Performances', 'On Sunday, 9 June, there will be a beach clean-up on the beaches of of Ampenan, Bintaro &amp; Loang Balok/Tanjung Karang from 7 to 10am. More than 1,500 people are expected to participate. June 9 will be capped with an Acknowledgement Night filled with musical performances by Trie Utami, Ray D&#39;Sky &amp; Sould ID. This event is being held to raise funds for a local organization working on marine debris and to acknowledge the most dedicated This will be held from 7 to 11pm at Sangkareang Park, Mataram. More than 1,500 people are expected to attend.<br />', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Ampenan, Bintaro & Loang Balok/Tanjung Karang, Sangkareang Park', '', NULL, 2013),
(107, 0, 'ID', 'beach-clean-up-minahasa', NULL, '', '', '', 'live', NULL, NULL, 'Minahasa, North Sulawesi', '', '', 0, 1436537061, 1450038709, NULL, NULL, 1, 'Beach Clean up - Minahasa', 'A beach clean-up event will be held on 5 June from 08.00 to 15.00 WITA. This event aims to encourage the general public, local governments of Minahasa, and NGOs to take action to clean up the beach from inorganic waste. This will be held in Tanawangko Village, Tombariri District, Minahasa Region, North Sulawesi Province and will be participated by 300 around people.', NULL, NULL, 1370390400, 0, 0, NULL, NULL, NULL, NULL, 'Village of Tanawangko, district of Tombariri', '', NULL, 2013),
(108, 0, 'ID', 'world-coral-reef-conference-2014-launch', NULL, '', '', '', 'draft', NULL, NULL, 'Minahasa, North Sulawesi', '', '', 0, 1436537151, 1436537151, NULL, NULL, 0, 'World Coral Reef Conference 2014 Launch', 'Riding on this year&#39;s Coral Triangle Day, the government of North Sulawesi province will be launching a World Coral Reef Conference set for 2014. This will be done in line with the Coral Triangle Day message: Shared Waters, Shared Solutions, Coming Together as One for the World&#39;s Centre of Marine Life - the Coral Triangle. This is to make the world know that Indonesia, specifically North Sulawesi, is very serious about preserving the Coral Triangle. The launch will be held on 7 June from 09.00 to 10.00 WITA in Kumu Village.', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'Village of Kumu, district of  Tombariri', '', NULL, 2013),
(109, 0, 'ID', 'public-awareness-campaign-on-river-waste-management-minahasa', NULL, '', '', '', 'live', NULL, NULL, 'Minahasa, North Sulawesi', '', '', 0, 1436537298, 1450038705, NULL, NULL, 1, 'Public awareness campaign on river waste management - Minahasa', 'A public awareness campaign will be conducted from 28 to 29 May to raise awareness on the need for proper waste management among local communities living around the Tondano Rivers (Kab. Minahasa), local government staff of Minahasa, and primary school students.', NULL, NULL, 1369612800, 0, 0, NULL, NULL, NULL, NULL, 'Watersheds Tondano', '', NULL, 2013),
(110, 0, 'ID', 'coral-rehabilitation', NULL, '', '', '', 'live', NULL, NULL, 'Minahasa, North Sulawesi', '', '', 0, 1436537438, 1450038706, NULL, NULL, 1, 'Coral Rehabilitation', 'A coral planting activity will be held on 7 June from 10.00 to 15.00 WITA in Kumu Village. This will be done using a Coral Settlement Device (CSD). Coral seeds from an on-site nursery will be moved to areas with the most damaged coral condition.&nbsp;<br />\nThis will be participated by students from the Faculty of Marine and Fisheries of Sam Ratulangi University, private sector partners (dive center), NGOs, and other relevant agencies.', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'Village of Kumu, district of  Tombariri', '', NULL, 2013),
(111, 0, 'ID', 'turtle-hatchlings-release-minahasa', NULL, '', '', '', 'live', NULL, NULL, 'Minahasa, North Sulawesi', '', '', 0, 1436537546, 1450038707, NULL, NULL, 1, 'Turtle hatchlings release - Minahasa', 'As many as 500 turtle hatchlings will be released on 7 June from 10.00 to 10.30 in Kumu Village. This will be done by local community members, NGO and private sector partners, government staff, and students from Sam Ratulangi University', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'Village of Kumu, district of  Tombariri', '', NULL, 2013),
(112, 0, 'ID', 'mangrove-planting', NULL, '', '', '', 'live', NULL, NULL, 'Minahasa, North Sulawesi', '', '', 0, 1436537644, 1450038710, 0, NULL, 1, 'Mangrove Planting', '5,000 mangrove seedlings will be planted on 7 June from 11.00 to 15.00 WITA in the village of Kumu. This will be done by local community members, NGO and private sector partners, government staff, and students from Sam Ratulangi University.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Village of Kumu, district of  Tombariri', '', NULL, 2013),
(113, 0, 'ID', 'photo-exhibition', NULL, '', '', '', 'live', NULL, NULL, 'Manado, North Sulawesi', '', '', 0, 1436537750, 1450038713, NULL, NULL, 1, 'Photo Exhibition', 'On 21 June, North Sulawesi hopes to break the record for the longest photo exhibition line in Indonesia.<br />\nThis photo exhibition will highlight the harmony between human life and the sea, through photography, carrying the theme: &amp;ldquo;Embody human life in harmony with the sea.<br />\nParticipants are from the photography club of North Sulawesi, private sector and NGO partners, and the local government of North Sulawesi<br />\nThe photo exhibit will be open to the public from 08.00 to 18.00 at the Mega Mall, Manado.', NULL, NULL, 1371772800, 0, 0, NULL, NULL, NULL, NULL, 'Manado', '', NULL, 2013),
(114, 0, 'ID', 'coloring-contest-and-puppet-show', NULL, '', '', '', 'live', NULL, NULL, 'Manado, North Sulawesi', '', '', 0, 1436537869, 1450038716, 0, NULL, 1, 'Coloring Contest and Puppet Show', 'A coloring contest and puppet show will be held on 21 June from 10.00 to 14.00 WITA, to educate elementary school children on marine life. The event will be held in Mega Mall, Manado.', NULL, NULL, 1371772800, 0, 0, NULL, NULL, NULL, NULL, 'Manado', '', NULL, 2013),
(115, 0, 'ID', 'educational-open-house-sanur', NULL, '', '', '', 'live', NULL, NULL, 'Bali', '', '', 0, 1436538001, 1450038714, NULL, NULL, 1, 'Educational Open House - Sanur', 'On June 9, the CTC will once again open its doors to school children (aged 9-15) to teach them about the marine environment through fun and engaging games and activities. They will also get to watch a video on corals and the sea. Guest speakers with various background (conservationists, divers, surfers, fishermen) will be talking about their professions and their love of the sea. The event will be open from 10am to 3pm at the CTC office on Jalan Danau Tamblingan No 78, Sanur, Bali 80228.<br />\nThis is an annual event to engage the young generation and our neighbors and partners alike in commemorating the Coral Triangle Day in a fun way.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Sanur', '', NULL, 2013),
(116, 0, 'ID', 'marine-conservation-educational', NULL, '', '', '', 'live', NULL, NULL, 'Kuta, Bali', '', '', 0, 1436538097, 1450038716, NULL, NULL, 1, 'Marine Conservation Educational', 'The Coral Triangle Centre, together with the Body Shop Indonesia will be joining the Marine Foundation to commemorate the World Ocean Day on June 8 from 2pm to 8pm at the Kuta Beachwalk mall in Kuta, Bali.&nbsp;The CTC will host educational games, fun presentations, and a speech at the opening of a month-long ocean photography exhibition. The exhibition itself will run for 4 weeks.<br />\nThis event is geared towards general public and will be held in the newest, busy shopping mall in Kuta, Bali.&quot;<br />', NULL, NULL, 1370649600, 0, 0, NULL, NULL, NULL, NULL, 'Beach Walk', '', NULL, 2013),
(117, 0, 'ID', 'white-dolphin-saves-the-coral', NULL, '', '', '', 'live', NULL, NULL, 'Central Sulawesi', '', '', 0, 1436538162, 1450038715, NULL, NULL, 1, 'White Dolphin Saves The Coral', 'White Dolphin Save The Corals is a ceremonial event to celebrate The Coral Triangle Day in Donggala area, Central of Sulawesi, Indonesia. There will be coral transplant and cleaning, beach cleaning, fun snorkeling and children coloring competition. This event will be supported by local government and society.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Palu', '', NULL, 2013),
(118, 0, 'ID', 'fundraising-campaign-face2face-and-online', NULL, '', '', '', 'live', NULL, NULL, 'Surabaya', '', '', 0, 1436538251, 1450038712, 0, NULL, 1, 'Fundraising Campaign (Face2Face & Online)', 'Riding on Coral Triangle Day, WWF-Indonesia will conduct a fundraising campaign related to marine conservation. Two programs will be conducted: Face2Face Campaign with WWF&amp;rsquo;s In-house team for the offline program; and campaigning for virtual spot buying at mycoraltriangle.com for the online program. Both will be promoted during the event where people can directly donate online. There will be a special gimmick for people who sign up and donate during the event and may receive marine turtle plush toys and pins for certain donation amounts. The event will run from 6 to 30 June. Come find our special pretty booth with marine species and coral designs! Our booth will be open from 9am to 6pm.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Main Atrium of Grand City Mall', '', NULL, 2013),
(119, 0, 'ID', 'story-telling', NULL, '', '', '', 'live', NULL, NULL, 'Surabaya', '', '', 0, 1436538342, 1450038717, NULL, NULL, 1, 'Story Telling', 'WWF will present singing-storytelling activities on marine and Coral Triangle issues on 16 and 30 June at 2pm at the main atrium stage of the Grand City Mall.&nbsp;', NULL, NULL, 1371340800, 0, 0, NULL, NULL, NULL, NULL, 'Grand City Mall', '', NULL, 2013),
(120, 0, 'ID', 'facebook-coral-triangle-campaign', NULL, '', '', '', 'live', NULL, NULL, 'Surfer Girl''s Facebook Page', '', '', 0, 1436538491, 1450038702, NULL, NULL, 1, 'Facebook Coral Triangle Campaign', 'In celebration of the Coral Triangle Day, Surfer Girl will be displaying 4 artworks on their Facebook page with more than 2.8 million followers. Surfer Girl will be donating IDR 1,000 for each SHARE to support Coral Triangle Conservation program', NULL, NULL, 1369958400, 0, 0, NULL, NULL, NULL, NULL, 'www.facebook.com/ilovesurfergirl', '', NULL, 2013),
(121, 0, 'ID', 'facebook-coral-triangle-fundraising-campaign', NULL, '', '', '', 'live', NULL, NULL, 'Facebook', '', '', 0, 1436538589, 1450038718, NULL, NULL, 1, 'Facebook Coral Triangle Fundraising Campaign', 'On 9 June, WWF-Indonesia will encourage its fans to donate to WWF by buying virtual spots on www.mycoraltriangle.com<br />\nFor a minimum donation of IDR250,000 (minimum of 5 virtual spots) donors will get a marine turtle plush toy (for Indonesia residents only).', NULL, NULL, 1370649600, 0, 0, NULL, NULL, NULL, NULL, 'www.mycoraltriangle.com', '', NULL, 2013),
(122, 0, 'ID', 'twitter-coral-triangle-campaign', NULL, '', '', '', 'live', NULL, NULL, 'Twitter', '', '', 0, 1436538658, 1450038702, NULL, NULL, 1, 'Twitter Coral Triangle Campaign', 'Surfer Girl and WWF-Indonesia are conducting a Save the Coral Triangle Tagline Contest from 2 to 30 June. This contest is open to the people of all ages.&nbsp;', NULL, NULL, 1370044800, 0, 0, NULL, NULL, NULL, NULL, 'Surfer Girl (@summerSurferGrl) and WWF Indonesia''s (@WWF_ID) Twitter', '', NULL, 2013),
(123, 0, 'ID', 'apru-sustainability-and-climate-change-symposium-2013', NULL, '', '', '', 'live', NULL, NULL, 'Jakarta', '', '', 0, 1436538708, 1450038704, 0, NULL, 1, 'APRU Sustainability and Climate Change Symposium 2013', 'APRU International Symposium on Coastal Cities, Marine Resources and Climate Change in the Coral Triangl', NULL, NULL, 1370131200, 0, 0, NULL, NULL, NULL, NULL, 'The Sultan Hotel', '', NULL, 2013),
(124, 0, 'ID', 'public-campaign-to-collect-petitions-to-stop-shark-finning-and-shark-consumption', NULL, '', '', '', 'live', NULL, NULL, 'Denpasar', '', '', 0, 1436538794, 1450038720, NULL, NULL, 1, 'Public campaign to collect petitions to stop shark finning and shark consumption', 'From 6 to 10am on June 9, there will be a public campaign to collect petitions to stop shark finning and shark consumption.<br />\nThis will be done in conjunction with &amp;ldquo;Car Free Day.&amp;rdquo; The celebration will be hosted by Rico Ceper and Jeremy Tety. There will also be several performers on stage (i.e. Billy Beat Box, Bexxa, and Agung Hercules).<br />\nThere will also be a shark ogoh-ogoh, a bicycle and skate parade, a photo booth, theater performances, and a flash mob to add to the excitement.<br />\n#SOSharks (Save Our Sharks), launched on 12 May 2013, is a campaign to stop shark trade in markets, online stores and restaurants in Jakarta through mass media. This campaign is also supported by public figures, the Ministry of Marine Affairs and Fisheries, chefs and athletes.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Along the Way from Sudirman to Thamrin Street in conjunction with June 9th Car Free Day', '', NULL, 2013),
(125, 0, 'ID', 'mangrove-planting-ratatotok-bay', NULL, '', '', '', 'live', NULL, NULL, 'Minahasa, North Sulawesi', '', '', 0, 1436538875, 1450038716, NULL, NULL, 1, 'Mangrove Planting - RATATOTOK Bay', 'Couchsurfing Chapter Manado will conduct mangrove planting activities. Their goal is to plant 500 seedlings. Location is on RATATOTOK Bay, Southeast Minahasa-North Sulawesi. Indonesia', NULL, NULL, 1371772800, 0, 0, NULL, NULL, NULL, NULL, 'Ratatotok Bay', '', NULL, 2013),
(126, 0, 'MY', 'underwater-remotely-operated-vehichle-competition', NULL, '', '', '', 'live', NULL, NULL, '', '', '', 0, 1436538934, 1450038705, 0, NULL, 1, 'Underwater Remotely Operated Vehichle Competition', 'From 3 to 4 June, an Underwater Robotic Competition will be conducted among 20 Technical Schools. This is to encourage interest among the younger generation in Underwater Engineering.', NULL, NULL, 1370217600, 0, 0, NULL, NULL, NULL, NULL, 'Putrajaya Maritime Center', '', NULL, 2013),
(127, 0, 'MY', '1st-malaysia-ocean-renewable-energy-symposium-mores', NULL, '', '', '', 'live', NULL, NULL, '', '', '', 0, 1436538986, 1436725184, 0, NULL, 0, '1st Malaysia Ocean Renewable Energy Symposium (MORES)', '&nbsp;a symposium to increase communication and collaboration within the field of Marine Renewable Energy in Malaysia.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Putrajaya Maritime Center', '', NULL, 2013),
(128, 0, 'MY', 'seminar-on-ocean-governance-and-technologies', NULL, '', '', '', 'live', NULL, NULL, '', '', '', 0, 1436539049, 1450038712, 0, NULL, 1, 'Seminar on Ocean Governance and Technologies', 'a talk given by the French Government on their experience in oceans governance and technologies.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Putrajaya Maritime Center', '', NULL, 2013),
(129, 0, 'MY', 'world-oceans-day-and-ct-day-celebration', NULL, '', '', '', 'live', NULL, NULL, '', '', '', 0, 1436539114, 1450038707, 0, NULL, 1, 'World Oceans Day & CT Day Celebration', '8 June will mark the opening celebration of the World Oceans Day and the Coral Triangle Day. Celebrity chef Bobby Chinn will grace the event and do a cooking demo of a seafood dish made from responsibly-sourced seafood products.', NULL, NULL, 1370649600, 0, 0, NULL, NULL, NULL, NULL, 'Aquaria KLCC', '', NULL, 2013),
(130, 0, 'MY', 'world-oceans-day-celebration-with-redang-island-community', NULL, '', '', '', 'live', NULL, NULL, 'Terengganu', '', '', 0, 1436539167, 1450038714, 0, NULL, 1, 'World Oceans Day Celebration with Redang Island Community', 'From 6 to 7 June, there will be a 2-day program of activities with the local community, which will include a beach &amp;amp; reef clean up, an open day, sports competition, and feast.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Redang Island', '', NULL, 2013),
(131, 0, 'MY', 'save-our-seafood-sos-20-launching', NULL, '', '', '', 'live', NULL, NULL, 'Kuala Lumpur', '', '', 0, 1436621119, 1450038717, 0, NULL, 1, 'Save Our Seafood (S.O.S.) 2.0 Launching', 'On 9 June, WWF-Malaysia will launch an updated version of the Seafood Guide and its website. This Seafood Guide will help people choose more responsibly-caught seafood.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Double Tree', '', NULL, 2013),
(132, 0, 'MY', 'launch-and-deployment-of-artificial-reefs', NULL, '', '', '', 'live', NULL, NULL, 'Malaysia', '', '', 0, 1436621182, 1450038707, 0, NULL, 1, 'Launch & deployment of artificial reefs', 'On June 8 and 9, there will be an installation of artificial reefs as part of the local resort&amp;rsquo;s CSR program', NULL, NULL, 1370649600, 0, 0, NULL, NULL, NULL, NULL, 'The Andaman Langkawi', '', NULL, 2013),
(133, 0, 'MY', 'cticff-conference-malaysia', NULL, '', '', '', 'live', NULL, NULL, 'Kuala Lumpur', '', '', 0, 1436621263, 1450038715, NULL, NULL, 1, 'CTI-CFF Conference - Malaysia', 'On June 10 and 11, there will be a conference to highlight the achievements of the Coral Triangle Initiative on Coral Reefs, Fisheries &amp;amp; Food Security (CTI-CFF) Programme in Malaysia.', NULL, NULL, 1370822400, 0, 0, NULL, NULL, NULL, NULL, 'Marriott Putrajaya Hotel', '', NULL, 2013),
(134, 0, 'MY', 'i-love-our-ocean-camp', NULL, '', '', '', 'live', NULL, NULL, 'Kepong', '', '', 0, 1436621358, 1450038704, NULL, NULL, 1, 'I Love Our Ocean! camp', 'I Love Our Ocean! camp', NULL, NULL, 1369958400, 0, 0, NULL, NULL, NULL, NULL, 'Forest Research Institute Malaysia (FRIM), Kepong', '', NULL, 2013),
(135, 0, 'MY', 'world-oceans-day', NULL, '', '', '', 'live', NULL, NULL, 'Petaling Jaya', '', '', 0, 1436621463, 1450038701, 0, NULL, 1, 'World Oceans Day', 'World Oceans Day @ Paradigm Mall', NULL, NULL, 1369958400, 0, 0, NULL, NULL, NULL, NULL, 'Paradigm Mall', '', NULL, 2013),
(136, 0, 'MY', 'ocean-mania-costume-and-children-coloring-contest', NULL, '', '', '', 'live', NULL, NULL, 'Petaling Jaya', '', '', 0, 1436621631, 1450038708, NULL, NULL, 1, 'Ocean Mania Costume & Children Coloring Contest', 'Ocean Mania Costume &amp; Children Coloring Contest&nbsp;', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'Paradigm Mall', '', NULL, 2013),
(137, 0, 'MY', 'inos-hicoe-in-marine-science-stakeholders-meeting-on-effective-ocean-governance', NULL, '', '', '', 'live', NULL, NULL, 'Terengganu', '', '', 0, 1436621731, 1450038722, NULL, NULL, 1, 'INOS HICoE in Marine Science Stakeholders'' Meeting on Effective Ocean Governance', 'INOS HICoE in Marine Science Stakeholders&#39; Meeting on Effective Ocean Governance towards the Sustainability of Marine Endangered Species', NULL, NULL, 1371340800, 0, 0, NULL, NULL, NULL, NULL, 'Institute of Oceanography and Environment (INOS), UMT', '', NULL, 2013),
(138, 0, 'MY', 'radio-talk-show-bfm', NULL, '', '', '', 'live', NULL, NULL, 'Petaling Jaya', '', '', 0, 1436621817, 1450038711, NULL, NULL, 1, 'Radio Talk Show BFM', 'Radio Talk Show BFM', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Petaling Jaya', '', NULL, 2013),
(139, 0, 'MY', 'mns-celebrates-world-oceans-day-with-students-in-tioman', NULL, '', '', '', 'live', NULL, NULL, '', '', '', 0, 1436621878, 1450038713, NULL, NULL, 1, 'MNS Celebrates World Oceans Day with students in Tioman', 'MNS Celebrates World Oceans Day with students in Tioman', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'Tioman Island, Pahang', '', NULL, 2013),
(140, 0, 'MY', 'umt-beach-clean-up', NULL, '', '', '', 'live', NULL, NULL, 'Terengganu', '', '', 0, 1436621937, 1450038711, NULL, NULL, 1, 'UMT Beach Clean Up', 'UMT Beach Clean Up', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'UMT Beach', '', NULL, 2013),
(144, 0, 'MY', 'radio-talk-show-traxx-fm-nod', NULL, '', '', '', 'live', NULL, NULL, 'Kuala Lumpur', '', '', 0, 1436720950, 1450038709, NULL, NULL, 1, 'Radio Talk Show Traxx FM (NOD)', 'Radio Talk Show Traxx FM (NOD)', NULL, NULL, 1370304000, 0, 0, NULL, NULL, NULL, NULL, 'Angkasapuri', '', NULL, 2013),
(145, 0, 'MY', 'reef-check-malaysia-labuan-coral-reef-surveys', NULL, '', '', '', 'live', NULL, NULL, 'Labuan', '', '', 0, 1436721060, 1450038703, NULL, NULL, 1, 'Reef Check Malaysia Labuan coral reef surveys', 'Reef Check Malaysia Labuan coral reef surveys', NULL, NULL, 1370131200, 0, 0, NULL, NULL, NULL, NULL, 'Labuan', '', NULL, 2013),
(146, 0, 'MY', 'seminar-on-ocean-data-management-odm', NULL, '', '', '', 'live', NULL, NULL, 'Putrajaya', '', '', 0, 1436721138, 1450038702, NULL, NULL, 1, 'Seminar on Ocean Data Management (ODM)', 'Seminar on Ocean Data Management (ODM)', NULL, NULL, 1370304000, 0, 0, NULL, NULL, NULL, NULL, 'Putrajaya Maritime Center', '', NULL, 2013),
(143, 0, 'MY', 'malaysiafrance-joint-project-malaysian-environmental-project-for-sustainable-oceans-maleso-final', NULL, '', '', '', 'live', NULL, NULL, 'Putrajaya', '', '', 0, 1436720735, 1450038713, NULL, NULL, 1, 'Malaysia-France Joint Project: Malaysian Environmental Project for Sustainable Oceans (MALESO)', 'Malaysia-France Joint Project: Malaysian Environmental Project for Sustainable Oceans (MALESO)', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'NOD, MOSTI', '', NULL, 2013),
(147, 0, 'PH', 'beach-cleanups', NULL, '', '', '', 'live', NULL, NULL, 'Philippines', '', '', 0, 1436721382, 1450038706, 0, NULL, 1, 'Beach clean-ups', 'Beach clean ups will be conducted in the following areas:&nbsp;Municipality of Cortes- Surigao del Sur,&nbsp;Municipality of San Francisco-Cebu,&nbsp;Municipality of Ubay-Bohol,&nbsp;Municipality of Lubang, Occidental Mindoro,&nbsp;Maribago Cove, Dakit-Dakit Marine Sanctuary and Kontiki Area (supported by Bluewater Resorts),&nbsp;Barangays Ibo, Mactan, Punta Engano, Buaya in Lapu Lapu City,&nbsp;Barangays Simandagit and Suwangkagang, Pasiagan, Mun. of Bongao, Province of Tawi-Tawi,&nbsp;Hondura Beach in Puerto Galera town (Green fins), and&nbsp;Barangay Balibago, Calatagan, Batangas.', NULL, NULL, 1370649600, 1370736000, 0, NULL, NULL, NULL, NULL, 'Various beaches', '', NULL, 2013),
(148, 0, 'PH', 'mangrove-plantings', NULL, '', '', '', 'live', NULL, NULL, 'Philippines', '', '', 0, 1436723230, 1450038714, NULL, NULL, 1, 'Mangrove Plantings', 'Mangrove plantings will be conducted in the following coastal areas:&nbsp;Municipality of Cortes, Surigao del Sur,&nbsp;Municipality of San Francisco, Cebu,&nbsp;Philippines Sitio Langayan, Bgy Poblacion, Taytay, Palawan,&nbsp;Naval Forces Central Compound in Mactan Island,&nbsp;Brgy. Kalawisan in Lapu-Lapu and in Naga &nbsp;LGU, and&nbsp;Barangay Balibago, Calatagan, Batangas.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Various coastal areas', '', NULL, 2013),
(149, 0, 'PH', 'ct-day-convergence-and-press-conference', NULL, '', '', '', 'live', NULL, NULL, 'Mactan', '', '', 0, 1436723362, 1450038713, 0, NULL, 1, 'CT Day Convergence  and Press Conference', 'On June 6 from 8 to 10am, CTI Partners will converge to show their support for Coral Triangle Day and present the activities that will happen on June 9. A press conference will follow at Imperial Palace Water Park Resort and Spa in Mactan.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Mactan Covered Court, Mactan Cebu and Imperial Palace', '', NULL, 2013),
(150, 0, 'PH', 'kapihan-sa-pia-media-forum', NULL, '', '', '', 'live', NULL, NULL, 'Cebu', '', '', 0, 1436723428, 1450038704, 0, NULL, 1, 'Kapihan Sa PIA Media Forum', 'On May 28 from 10 to 11am, there will be a media conference where CTI Partners will discuss about the Coral Triangle and upcoming Coral Triangle Day activities in Cebu.', NULL, NULL, 1369699200, 0, 0, NULL, NULL, NULL, NULL, 'PIA Office, Gorordo Avenue', '', NULL, 2013),
(151, 0, 'PH', 'radio-talk-shows', NULL, '', '', '', 'live', NULL, NULL, 'Cebu', '', '', 0, 1436723514, 1450038700, 0, NULL, 1, 'Radio Talk Shows', 'On May 26 from 9 to 10am DYMF Radio Station, Good Morning Philippines program will talk about upcoming Coral Triangle Day activities in Cebu.<br />\nOn May 25 from 10 - 11am DYRC Radio Station will talk about upcoming Coral Triangle Day activities in Cebu.', NULL, NULL, 1369440000, 1369526400, 0, NULL, NULL, NULL, NULL, 'DYMF Radio Station and DYRC Radio Station', '', NULL, 2013),
(152, 0, 'PH', 'palawod-2013', NULL, '', '', '', 'live', NULL, NULL, 'Palawan', '', '', 0, 1436723741, 1450038699, 0, NULL, 1, 'Palawod 2013', 'On May 31 - June 2, Campomanes Bay Green Alert Negros, in cooperation with the Coral Triangle Initiative on Coral Reefs, Fisheries and Food Security (CTI-CFF), and partner groups will be holding the 20th Palawood - an annual coastal event that has become the venue for initiatives and discussions involving specific issues around the oceans. This event is the reason why Negros Occidental in the Philippines has the Sagay Marine Reserve. It has since catalyzed many other similar outcomes in the province.', NULL, NULL, 1369958400, 1370131200, 0, NULL, NULL, NULL, NULL, 'Palawan', '', NULL, 2013),
(153, 0, 'PH', 'under-water-cleanup-marigondon-and-maribago', NULL, '', '', '', 'live', NULL, NULL, 'Lapu-lapu City', '', '', 0, 1436723848, 1450038717, 0, NULL, 1, 'Under water clean-up - Marigondon and Maribago', 'On June 9 from 9 to 11am, volunteer scuba divers and dive operators will conduct simultaneous underwater clean-ups in two communities in Lapu Lapu City.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Barangays Marigondon and Marigondon', '', NULL, 2013),
(154, 0, 'PH', 'fish-house-launching-macatan', NULL, '', '', '', 'live', NULL, NULL, 'Cebu', '', '', 0, 1436723910, 1450038717, NULL, NULL, 1, 'Fish House Launching - Macatan', 'On June 9 at 10am, Shangri-la Hotel will launch a fish house which will be located in their beach front.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Shangri-la Hotel and Resort, Mactan', '', NULL, 2013),
(155, 0, 'PH', 'greening-supply-chains-protecting-ecosystems-and-food-security', NULL, '', '', '', 'live', NULL, NULL, 'Philippines', '', '', 0, 1436723983, 1450038719, 0, NULL, 1, 'Greening Supply Chains: Protecting Ecosystems and Food Security', 'WWF-Philippines has partnered with the Tubbataha Management Office in celebrating the second Coral Triangle Day. A day-long celebration will take place at the Palawan National High School in Puerto Princesa City. The city is the gateway and jump-off point to the Tubbataha Reefs, one of the most biologically diverse and productive areas within the Coral Triangle.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'ADB K-Hub', '', NULL, 2013),
(156, 0, 'PH', 'mpa-visit-and-community-awareness-on-mpa', NULL, '', '', '', 'live', NULL, NULL, 'Tagbilaran City, Bohol', '', '', 0, 1436724044, 1450038711, 0, NULL, 1, 'MPA Visit and Community Awareness on MPA', 'Community/Staff Lecture on MPAs, June 7, 2013', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Barangay Taloto', '', NULL, 2013),
(157, 0, 'PH', 'mural-painting-movement', NULL, '', '', '', 'live', NULL, NULL, 'Puerto Princesa', '', '', 0, 1436724158, 1450038714, 0, NULL, 1, 'Mural Painting Movement', 'The Tubbataha Management Office (TMO) and the World Wide Fund for Nature Philippines (WWF-Philippines) have partnered to celebrate the Coral Triangle Day in Puerto Princesa City. The city is the gateway and jump-off point to the Tubbataha Reefs, one of the most biologically diverse and productive areas within the Coral Triangle. The highlight of the festivities is a mural painting activity led by AG, a WWF-Philippines Hero of the Environment and a Filipino mural artist who has gone across the country to promote marine conservation through art. Participants include volunteer artists plus allies of TMO and WWF-Philippines. Through the Coral Triangle Day, TMO and WWF-Philippines seek to raise awareness about marine conservation, and empower the public to take concrete action to care for our oceans. The day-long celebration will also highlight the Tubbataha Reefs&#39; 25th Anniversary as a Marine Protected Area.', NULL, NULL, 1370736000, 0, 0, NULL, NULL, NULL, NULL, 'Puerto Princesa City', '', NULL, 2013),
(158, 0, 'PH', 'marine-biodiversity-lecture-by-smithsonian-institutes-dr-nancy-knowlton', NULL, '', '', '', 'live', NULL, NULL, 'Batangas', '', '', 0, 1436724245, 1450038719, NULL, NULL, 1, 'Marine Biodiversity Lecture by Smithsonian Institute''s Dr. Nancy Knowlton', 'Marine Biodiversity Lecture by Smithsonian Institute Dr. Nancy Knowlton', NULL, NULL, 1371340800, 0, 0, NULL, NULL, NULL, NULL, 'Batangas Provincial Capitol, Batangas National High School, Batangas State University', '', NULL, 2013),
(159, 0, 'PG', 'coral-triangle-awareness-day', NULL, '', '', '', 'live', NULL, NULL, 'Port Moresby South NCD', '', '', 0, 1436724321, 1450038709, NULL, NULL, 1, 'Coral Triangle Awareness Day', 'CTI partners in Papua New Guinea will be holding a Coral Triangle Awareness event on 6 June from 9 to 10am. Hosted by a well-known radio personality, the event will have formal speeches from the CTI National Coordinating Committee and District Members of Parliament. The event will also launch the Coral Triangle Day 2014 Logo Competition. Informative booths and stalls will also be set up around the venue.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Ela Beach', '', NULL, 2013),
(160, 0, 'PG', 'release-of-captured-turtles', NULL, '', '', '', 'live', NULL, NULL, 'Port Moresby South NCD', '', '', 0, 1436724392, 1450038708, NULL, NULL, 1, 'Release of captured turtles', 'On 6 June 2013 from 10 to 11am, captured turtles from local wet markets will be released. Special guests will tag these turtles, to help monitor their movements at sea.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Ela Beach', '', NULL, 2013),
(161, 0, 'PG', 'beach-clean-up-ela-beach', NULL, '', '', '', 'live', NULL, NULL, 'Port Moresby South NCD', '', '', 0, 1436724449, 1450038709, NULL, NULL, 1, 'Beach Clean Up - Ela Beach', 'On 6 June 2013 from 11am to 12nn, local organizations, schools, and the general public will conduct a beach clean-up activity on Ela Beach. Music and entertainment will be provided to make this activity fun!', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Ela Beach', '', NULL, 2013),
(162, 0, 'PG', 'trash-sorting-for-infographics', NULL, '', '', '', 'live', NULL, NULL, 'Port Moresby South NCD', '', '', 0, 1436724499, 1450038710, NULL, NULL, 1, 'Trash sorting for info-graphics', 'On 6 June 2013 from 1 to 2pm, a trash sorting activity will be conducted. Data from this activity will be used for an infographics material to help raise awareness on waste management. This is being organized by DEC, the University of PNG, and the District Commission to help educate nearby communities on the importance of waste management.', NULL, NULL, 1370476800, 0, 0, NULL, NULL, NULL, NULL, 'Ela Beach', '', NULL, 2013),
(163, 0, 'SB', 'gallery-exhibition-by-coral-triangle-initiative', NULL, '', '', '', 'live', NULL, NULL, 'Honiara', '', '', 0, 1436724601, 1450038719, NULL, NULL, 1, 'Gallery Exhibition by Coral Triangle Initiative', 'There will be a Gallery Exhibition by Coral Triangle Initiative key implementing partners in the Solomon Islands, which will highlight their key successes and challenges in carrying out the National Plan of Action in conserving the Coral Triangle. The morning session is open to government officials and donor partners, while the afternoon session will be opened to the public.', NULL, NULL, 1370908800, 0, 0, NULL, NULL, NULL, NULL, 'National Art Gallery', '', NULL, 2013),
(164, 0, 'TL', 'media-blitz-on-coral-triangle-day', NULL, '', '', '', 'live', NULL, NULL, 'Timor Leste', '', '', 0, 1436724714, 1450038703, NULL, NULL, 1, 'Media Blitz on Coral Triangle Day', 'In the week leading up to the Coral Triangle Day, short informational videos will be shown every hour on Timor Leste&#39;s only TV station. CTI success stories will also be published in the daily newspaper and announced daily on the radio. The streets will also be decked with Coral Triangle Banners.', NULL, NULL, 1370131200, 0, 0, NULL, NULL, NULL, NULL, 'TV, Radio, Streets', '', NULL, 2013),
(165, 0, 'TL', 'marine-biodiversity-workshop', NULL, '', '', '', 'live', NULL, NULL, 'Timor Leste', '', '', 0, 1436724793, 1450038711, NULL, NULL, 1, 'Marine Biodiversity Workshop', 'On June 8, an interactive workshop on marine biodiversity will be held. This workshop will run in partnership with the Coral Triangle Support Partnership (CTSP) and National Oceanic and Atmospheric Administration (NOAA)', NULL, NULL, 1370563200, 0, 0, NULL, NULL, NULL, NULL, 'Timor Leste', '', NULL, 2013),
(166, 0, 'ID', NULL, NULL, NULL, NULL, NULL, 'draft', NULL, NULL, 'Surabaya', 'Jl. Minangkabau No.2, Ps. Manggis, Setia Budi, Kota Jakarta SelatanKecamatan Setiabudi, Daerah Khusus Ibukota Jakarta, Indonesia', '-6.20876,106.84560', 0, 1458643872, 1458643872, NULL, NULL, 0, 'Test CTD 2016 Event', 'Whazzuuuuup fellas', NULL, NULL, 1446699600, 0, 0, 'Rendy Mulyono', 'rendy.bee@gmail.com', 'www.superindo.com', 'catsareus@gmail.com', 'Delta Plaza', 'Manyar Tirtomoyo 8/11', NULL, NULL),
(174, 0, 'SB', 'gizo-town-cleanup-plastic-bagfree-day-and-talent-competition-2016', NULL, '', '', '', 'live', 'a:1:{i:0;i:580;}', NULL, 'Gizo Town', NULL, '-8.10493,156.83504', 0, 1464775537, 1465020363, 0, NULL, 0, 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '<strong>Gizo Town Clean-Up -&nbsp;</strong><span data-term="goog_1472874861" tabindex="0">10am-12pm</span>&nbsp;| Meet at MSG Building for introduction, briefing, and gear<br />\n<br />\n<strong>#noplace4plastic Gizo Talent Competition -&nbsp;</strong><span data-term="goog_1472874862" tabindex="0">1:30pm-4:30pm</span>&nbsp;| MSG Building<br />\n***Free entry, but must register with WWF Gizo office before&nbsp;<span data-term="goog_1472874863" tabindex="0">June 6th</span><br />\n&nbsp;<br />\n<strong>Plastic Bag-Free Day -&nbsp;</strong>All day | Mid-day market awareness<br />\n&nbsp;<br />\n<strong>Awareness Program, Recycling Set-up &amp; Food Stands -&nbsp;</strong>All day | MSG Building<br />\n&nbsp;<br />\n<strong>Evening Movie Program -&nbsp;</strong><span data-term="goog_1472874864" tabindex="0">6:30pm</span>&nbsp;onwards | MSG Building<br />\n<br />\n<strong>Partners </strong>- Deanne&rsquo;s Gizo Aluminum Can Recycling, WorldFish, Ecological Solutions Solomon Islands, Western Province Environmental Health Division, Western Province Environment Division, and Gizo Town Council.', NULL, NULL, 1465444800, 0, 0, 'Rachel Wang', 'rwang@wwfpacific.org', '', 'rwang@wwfpacific.org', 'MSG Building', '', 'yes', 2016);
INSERT INTO `default_ctd` (`id`, `revision_id`, `country`, `slug`, `category_id`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `organiser`, `sponsors`, `location`, `geolocation`, `geocoordinates`, `hide_map`, `created_on`, `updated_on`, `publish_on`, `slide`, `views`, `title`, `intro`, `body`, `notes`, `date_from`, `date_to`, `plan_to_come`, `submitted_by_name`, `submitted_by_email`, `website`, `contact_email`, `event_venue`, `venue_address`, `approve`, `event_year`) VALUES
(172, 0, 'PH', 'reef-strokes-swim-at-hamilo-coast', NULL, '', '', '', 'live', 'a:1:{i:0;i:558;}', NULL, 'Batangas', NULL, '14.18197,120.60228', 0, 1464354694, 1464777278, 0, NULL, 0, 'Reef Strokes Swim at Hamilo Coast', 'Five legendary Pinoy open-water athletes will swim together to promote the protection of the Verde Island Passage (VIP) and educate the public about the need to conserve Philippine coral reefs.<br />\n<br />\nWorld Wide Fund for Nature (WWF) Hero of the Environment Atty. Ingemar Macarine, Frank Lacson, Betsy Medalla, Julian Valencia and Moi Yamoyam shall swim approximately 15 kilometers across the coves of Hamilo Coast in the Philippine province of Batangas this May 29 for Reef Strokes, an event to celebrate Coral Triangle Day and highlight the dangers of plastic pollution and climate change on the Verde Island Passage, dubbed as the center of the center of marine shorefish biodiversity.<br />\n<br />\nReef Strokes is organized by WWF, Hamilo Coast, Pico Sands Hotel, Pico de Loro Beach and Country Club and Cebu Pacific Air.&nbsp;<br />\n<br />\n<a href="https://scontent-sit4-1.xx.fbcdn.net/t31.0-8/s960x960/13268150_10154166221464618_6325352716641389014_o.jpg" target="_blank">Find the poster here.</a>', NULL, NULL, 1464494400, 0, 0, 'WWF-Philippines', 'kkp@wwf.org.ph', 'wwf.org.ph', 'kkp@wwf.org.ph', 'Pico de Loro Cove', 'Hamilo Coast', 'yes', 2016),
(173, 0, 'ID', '1st-international-seminar-on-tropical-aquatic-resources-science-and-management', NULL, '', '', '', 'live', 'a:3:{i:0;i:559;i:1;i:560;i:2;i:561;}', 'a:2:{i:0;i:562;i:1;i:563;}', 'Manado', NULL, '1.47483,124.84208', 0, 1464519151, 1464777237, 0, NULL, 0, '1st International Seminar on Tropical Aquatic Resources Science and Management', 'The seminar is in conjunction with &quot;the 1st National Congress of the Indonesian Aquatic Resources Management Association&quot; (Asosiasi Pengelola Sumber Daya Perairan Indonesia)<br />\n<br />\nTopics:\n<ol>\n <li>Sustainable Fisheries (Blue economy, EAFM, Aquaculture, Friendly Fishing Technology, Integrated Multi-trophic Aquaculture)</li>\n <li>Tropical Coastal Zone Management (MPA, Coastal Ecosystem Management, Marine Pollution and Mitigation, Ecosystem Services, Coastal Indigenous Resilience)</li>\n <li>Eco-hydrology (Sustainable Agricultural Practices, Wastewater Management, Water Security, Flood and Drought, Seawater Intrusion).</li>\n</ol>\nType of Activity: Seminar (Oral &amp; Poster presentation) and Excursion Further information, please visit: <a href="http://pasca.unsrat.ac.id/s2/ipa/" target="_blank">http://pasca.unsrat.ac.id/s2/ipa/</a>', NULL, NULL, 1475035200, 1475121600, 0, 'Markus T Lasut', 'markus_lasut@yahoo.com', 'http://pasca.unsrat.ac.id/s2/ipa/?page_id=1263', 'rose_mantiri@yahoo.com', 'Sintesa Peninsula Hotel', 'Jl. Jend Sudirman, Gunung Wenang, Sulawesi Utara Phone:(0431) 855008 (http://www.sintesahotels.com)', 'yes', 2016),
(171, 0, 'MY', 'seagrass-awareness-campaign', NULL, '', '', '', 'live', 'a:2:{i:0;i:556;i:1;i:557;}', NULL, 'Kota Kinabalu', '', '', 0, 1464068362, 1464777175, 0, NULL, 0, 'Seagrass Awareness Campaign', 'This campaign aim to educate younger generation especially those living in coastal area about important of seagrass bed and how they can help to protect them from further destruction.<br />\n<br />\nSelected 30 secondary school student from SMK Pulau Gaya will participant in this program.<br />\n<br />\nIt start with talk by Assoc. Prof Dr. John Barry Gallagher about seagrass, its function and role, how it affecting our life, climate, how we can protect and conserve them.<br />\n<br />\nProgram follow by boat ride on glass bottom boat to explore nearby seagrass meadow and learn basic monitoring activities.<br />\n<br />\nHopefully through this program students are more aware about important of seagrass which related to their daily life.', NULL, NULL, 1464321600, 0, 0, 'Michael Yap Tzuen Kiat', 'mic_yap05@hotmail.com', '', 'mic_yap05@hotmail.com', 'SMK Pulau Gaya, Kota Kinabalu, Sabah', 'SMK Pulau Gaya', 'yes', 2016),
(175, 0, 'TL', 'coral-triangle-heritage-agreement-ceremony-atauro-island-timorleste', NULL, '', '', '', 'live', 'a:1:{i:0;i:581;}', 'a:1:{i:0;i:582;}', 'Dili', NULL, '-8.14418,125.64066', 0, 1464885859, 1464885984, 0, NULL, 0, 'Coral Triangle Heritage Agreement Ceremony, Atauro Island, Timor-Leste', 'The Coral Triangle Foundation invites all to celebrate Coral Triangle Day in Timor-Leste on Thursday the 9th of June 2016 at Akrema Lagoon.\n<ul>\n <li>10:00am Akrema 50 year Watershed Catchment and Marine Heritage Agreement Ceremony between the Community of Akrema, Government, the Coral Triangle Foundation and formation of the Coral Triangle Stewards</li>\n <li>10:30am Coastal Tree &amp; Mangrove Planting</li>\n <li>11:00am Marine debris collection, tracking, recycling and disposal</li>\n</ul>\nFor further information how to travel to Atauro Island, please contact <a href="mailto:Kevin.Austin@CoralTriangle.Foundation">Kevin.Austin@CoralTriangle.Foundation</a>', NULL, NULL, 1465444800, 0, 0, 'Kevin Austin', 'Kevin.Austin@CoralTriangle.Foundation', '', 'Kevin.Austin@CoralTriangle.Foundation', 'Akrema Beach Lagoon', 'Akrema Beach Lagoon', 'yes', 2016),
(176, 0, 'ID', 'press-conference-bali-reef-health-monitoring-survey-results', NULL, '', '', '', 'live', 'a:1:{i:0;i:583;}', NULL, 'Bali', NULL, '-8.69474,115.26301', 0, 1464960540, 1464974424, 0, NULL, 0, 'Press Conference: Bali Reef Health Monitoring Survey Results', 'A press conference to announce the results of the first ever Reef Health Monitoring Survey that covers the entire Bali island, including Nusa Penida Marine Protected Area. The survey results are the latest data that highlight which areas in Bali still have healthy coral reefs and abundant fish biomass as well as the areas that are threatened. It also includes recommendations on how to better sustain and protect Bali&rsquo;s coral reef and marine ecosystems.<br />\n<br />\nThe press conference will be on June 8, 2016, in conjunction with World Oceans Day and Coral Triangle Day. It will be held at the CTC office in Sanur, Bali.', NULL, NULL, 1465358400, 0, 0, 'Leilani', 'lgallardo@coraltrianglecenter.org', '', 'lgallardo@coraltrianglecenter.org', 'CTC Office', 'Jalan Danau Tamblingan 78, Sanur, Bali', 'yes', 2016),
(177, 0, 'ID', 'aku-cinta-laut-trash-art-competition', NULL, '', '', '', 'live', 'a:1:{i:0;i:584;}', NULL, 'Bali', NULL, '-8.70204,115.25892', 0, 1464960984, 1464974369, 0, NULL, 0, 'Aku Cinta Laut: Trash Art Competition', 'Bring your trash and create works of art! A contest to raise awareness on the importance of proper waste management to protect our seas and marine life.', NULL, NULL, 1465617600, 0, 0, 'Leilani', 'lgallardo@coraltrianglecenter.org', '', 'lgallardo@coraltrianglecenter.org', 'CTC Hub', 'Jalan Betngandang 2, Sanur, Bali', 'yes', 2016),
(178, 0, 'ID', 'debris-free-friday', NULL, '', '', '', 'live', NULL, 'a:1:{i:0;i:586;}', 'Gili Trawangan', NULL, '-8.35073,116.04310', 0, 1465184384, 1465191714, 0, NULL, 0, 'Debris Free Friday', 'Cleaning the beaches of Gili Trawangan 5pm-6pm! Meet at Trawangan Dive or Manta Dive at 5pm bags and briefing provided and clean the local harbour until 6pm where our sponsors Gili Castle Backpackers will be providing all volunteers with a free beer!!', NULL, NULL, 1465531200, 0, 0, 'Sian Williams', 'Seamadeproducts@gmail.com', '', 'Seamadeproducts@gmail.com', 'Gili Castle', 'Gili Trawangan Indonesia', 'yes', 2016),
(179, 0, 'ID', 'mangrove-planting-event-for-coral-triangle-day', NULL, '', '', '', 'live', 'a:3:{i:0;i:587;i:1;i:588;i:2;i:589;}', 'a:6:{i:0;i:590;i:1;i:591;i:2;i:592;i:3;i:593;i:4;i:594;i:5;i:595;}', 'Manado', NULL, '1.58227,124.81952', 0, 1465203760, 1465753159, 0, NULL, 0, 'Mangrove Planting Event for Coral Triangle Day', 'Initiated by Manengkel Solidaritas (a North Sulawesi-based NGO), Marine Partnership Consortium of North Sulawesi (Mitra Bahari) and The Alliance of Independent Journalists from Manado, a mangrove planting activity was conducted in Bahowo, the northern most region of Manado.<br />\n<br />\nManado is the capital city of North Sulawesi Province, Indonesia. Bahowo area is the only area with mangrove vegetation as the other areas have been inundated with land reclamation. However, the mangrove vegetation in this region is under threatened as many people activities around the region. The event is purposed to build people&rsquo;s care for their environment, in particular, for mangrove ecosystem in their area.', NULL, NULL, 1465012800, 0, 0, 'Gustaf Mamangkey', 'gustaf@unsrat.ac.id', 'http://manengkelsolidaritas.org', 'manengkelsolidaritas@gmail.com', 'Bahowo, North Sulawesi', 'FPIK, Sam Ratulangi University', 'yes', 2016),
(180, 0, 'PH', 'coral-triangle-day-2016-science-camp', NULL, '', '', '', 'live', NULL, NULL, 'Puerto Galera', NULL, '13.50197,120.95404', 0, 1465453728, 1465453936, 0, NULL, 0, 'Coral Triangle Day 2016 Science Camp', 'The Philippines&#39; focal activity for this year&#39;s Coral Triangle Day Celebration is a Science Camp from June 6 to June 12, 2016 at White Beach, Puerto Galera, Oriental Mindoro. It brings together twenty (20) students from various parts of the country who participated and won during the facebook trivial quiz contest during the Month of the Ocean (MOO 2016).<br />\n<br />\nThis year&#39;s module is aligned with the theme of the CT Day 2016: &quot;Save Coral Triangle - Stop Plastic Pollution/Waste&quot; focusing on Ocean Processes and Marine Ecosystems. The camp includes various sub-activities including lectures, fellowships, and coastal clean-ups for the students.', NULL, NULL, 1465185600, 1465704000, 0, 'John Erick Avelino', 'avelinojohnerick@gmail.com', 'https://www.facebook.com/MonthOfTheOcean/?ref=br_rs', '', 'Francesca Apartelle White Beach', 'Puerto Galera, Oriental Mindoro', 'yes', 2016),
(181, 0, 'ID', 'indonesia-marine-tourism-technical-asisstance-for-manager', NULL, '', '', '', 'live', NULL, NULL, 'Manado', NULL, '1.47409,124.84239', 0, 1465906871, 1465906980, 0, NULL, 0, 'Indonesia Marine Tourism Technical Asisstance for Manager', 'Indonesia Marine Tourism Technical Asisstance for Manager', NULL, NULL, 1465790400, 1465876800, 0, 'National Committee Secretariat CTI-CFF Indonesia', 'ncc.indonesia@cticff.org', 'Nccctiindonesia.kkp.go.id', 'Ncc.indonesia@cticff.org', 'Manado', 'Manado', 'yes', 2016),
(182, 0, 'ID', 'underwater-clean-up-dive-and-adopt-a-coral-program', NULL, '', '', '', 'live', 'a:1:{i:0;i:649;}', 'a:13:{i:0;i:650;i:1;i:651;i:2;i:652;i:3;i:653;i:4;i:654;i:5;i:655;i:6;i:656;i:7;i:657;i:8;i:658;i:9;i:659;i:10;i:660;i:11;i:661;i:12;i:662;}', 'Bali', NULL, '-8.70717,115.26256', 0, 1465998339, 1466000902, 0, NULL, 0, 'Underwater Clean Up Dive and Adopt a Coral Program', 'Underwater Clean Up and Adopt a Coral', NULL, NULL, 1465963200, 0, 0, 'Leilani', 'lgallardo@coraltrianglecenter.org', '', 'lgallardo@coraltrianglecenter.org', 'Semawang Beach', 'Sanur', 'yes', 2016),
(183, 0, 'MY', 'colouring-competition', NULL, '', '', '', 'live', 'a:2:{i:0;i:663;i:1;i:664;}', NULL, 'Kota Kinabalu', NULL, '60.00000,105.00000', 0, 1466064603, 1466065177, 0, NULL, 0, 'Colouring Competition', 'The main objectives of the program were to expose the children with the underwater world and to instill the interest to love and appreciate the ocean. 30 children aged 7-12 years old from various primary schools around Kota Kinabalu participated in this competition. All children were provided with a colouring paper and a colouring kit each. Participation certificate was also given to all the children at the end of the program.', NULL, NULL, 1465012800, 0, 0, 'Coral Triangle Initiative Sabah Branch (CTI-SAB)', 'ctisabahmy@gmail.com', '', 'ctisabahmy@gmail.com', 'Marine Aquarium & Museum', 'Borneo Marine Research Institute, Universiti Malaysia Sabah', 'yes', 2016),
(184, 0, 'MY', 'monitoring-of-coral-bleaching-due-to-climate-change', NULL, '', '', '', 'live', 'a:3:{i:0;i:665;i:1;i:666;i:2;i:667;}', NULL, 'Kota Belud', NULL, '60.00000,105.00000', 0, 1466065436, 1466065500, 0, NULL, 0, 'Monitoring of Coral Bleaching due to Climate Change', 'The aim of this program is to gather information on the state of the coral bleaching along Kg. Koduko, Kota Belud coastal areas and to provide information and awareness about coral bleaching to the community. Apart from diving to monitor the corals, water quality analysis was also done on several spots along the coastal areas. Short talk on &lsquo;Water Safety&rsquo; was also delivered to the community.', NULL, NULL, 1464840000, 0, 0, 'Coral Triangle Initiative Sabah Branch (CTI-SAB)', 'ctisabahmy@gmail.com', '', 'ctisabahmy@gmail.com', 'Kg. Koduko', 'Kg. Koduko, Kota Belud', 'yes', 2016),
(185, 0, 'ID', 'coral-triangle-day-in-nusa-penida', NULL, '', '', '', 'live', NULL, NULL, 'Nusa Penida', '', '', 0, 1497182751, 1497184020, 0, NULL, 0, 'Coral Triangle Day in Nusa Penida', 'Beach Clean Up, Underwater clean up, Competition on Marine Conservation, Drama performance with theme&nbsp;', NULL, NULL, 1496721600, 1496980800, 0, NULL, NULL, NULL, NULL, 'Nusa Penida', '', 'yes', 2017),
(186, 0, 'PG', 'coral-triangle-day-awareness', NULL, '', '', '', 'live', NULL, NULL, 'Port Moresby', NULL, '-9.44792,147.10701', 0, 1497182811, 1504122278, 0, NULL, 1, 'Coral Triangle Day Awareness', '<span 3="" 5="" a="" activities="" activity="" actual="" also="" am="" and="" at="" beach="" been="" billboard="" br="" c="" city="" clean-up="" combined="" conservation.="" ct="" date="" day="" envir="" far="" forwarded="" from="" has="" in="" instagram="" is="" it="" june="" line="" mall="" marine="" message="" ocean="" oceans="" of="" out="" planned="" polluti="" popular="" received="" relays="" release="" rs.="" running="" seems="" shopping="" since="" so="" tagging="" the="" therefore="" these="" to="" turtle="" two="" very="" visi="" was="" wed.="" which="" with="" world="" young="">For June 9, the media is already aware of it and will cover. Television has also contacted to cover since they became aware of the CT6 Resident missions participating.</span>', NULL, NULL, 1496980800, 0, 0, NULL, NULL, NULL, NULL, 'Konebada Beach', 'Konebada Beach, Port Moresby', 'yes', 2017),
(187, 0, 'ID', 'movie-screening-and-talks', NULL, '', '', '', 'live', NULL, NULL, 'Manado', NULL, '1.47483,124.84345', 0, 1497184083, 1497184123, 0, NULL, 0, 'Movie Screening and talks', '<span 2="" a="" affairs="" and="" are="" around="" at="" c="" competiti="" cti-cff.="" data-sheets-userformat="{" data-sheets-value="{" declarati="" department="" eradicating="" event="" fisheries="" from="" head="" headquarter="" held="" is="" marine="" movie="" north="" of="" polluti="" poster="" presentati="" regarding="" sam="" screening="" sea="" sessi="" the="" there="" university="" youth="">There are 2 presentation from Head of Department Marine Affairs and Fisheries regarding Condition of the sea around North Sulawesi, A session on pollution from University Sam Ratulangi, movie screening &quot;Trash&quot;, poster competition and declaration youth on eradicating trash.</span>', NULL, NULL, 1496808000, 0, 0, NULL, NULL, NULL, NULL, 'CTI-CFF Headquarters', 'Jl. A.A. Maramis Kayuwatu, Kairagi II', 'yes', 2017),
(188, 0, 'ID', 'radio-talkshow', NULL, '', '', '', 'live', NULL, NULL, 'Manado', '', '', 0, 1497184291, 1497184355, 0, NULL, 0, 'Radio Talkshow', '<span about="" and="" coral="" data-sheets-userformat="{" data-sheets-value="{" fisherman="" is="" perspectives="" related="" the="" theme="" triangle="">The theme is about Coral Triangle Initiative, Coral Triangle Day, and Fisherman perspectives related trash.</span>', NULL, NULL, 1496635200, 0, 0, NULL, NULL, NULL, NULL, 'Smart FM', '', 'yes', 2017),
(189, 0, 'ID', 'beach-and-riverside-clean-up', NULL, '', '', '', 'live', NULL, NULL, 'Manado', NULL, '1.67584,124.75564', 0, 1497184518, 1504122279, 0, NULL, 1, 'Beach and riverside clean up', '', NULL, NULL, 1496980800, 0, 0, NULL, NULL, NULL, NULL, 'Bunaken National Parks', 'Bunaken National Parks', 'yes', 2017),
(190, 0, 'ID', 'underwater-clean-up-campaign-on-marine-debris-declaration-to-stop-marine-debris', NULL, '', '', '', 'live', NULL, NULL, 'Bali', NULL, '-8.53044,115.50752', 0, 1497184643, 1504122280, 0, NULL, 1, 'Underwater Clean up, Campaign on Marine Debris, Declaration to stop Marine Debris', '', NULL, NULL, 1496894400, 0, 0, NULL, NULL, NULL, NULL, 'Padang Bai, Kb Karangasem Bali', 'Jl. Silayukti, Padangbai, Manggis, Kabupaten Karangasem, Bali 80871, Indonesia', 'yes', 2017),
(191, 0, 'ID', 'beach-clean-up-drawing-competition-photography-competition-conservation-campaign', NULL, '', '', '', 'live', NULL, NULL, 'Mataram', NULL, '-8.56992,116.07194', 0, 1497184739, 1504122279, 0, NULL, 1, 'Beach Clean Up, Drawing Competition, Photography Competition, Conservation Campaign', '', NULL, NULL, 1496980800, 0, 0, NULL, NULL, NULL, NULL, 'Ampenan Brach', 'Jalan Pantai Ampenan, Ampenan, Kota Mataram, Nusa Tenggara Bar., Indonesia', 'yes', 2017),
(192, 0, 'SB', 'canoe-race', NULL, '', '', '', 'live', NULL, NULL, 'Solomon Islands', NULL, '38.32113,-76.45769', 0, 1497185010, 1497185538, 0, NULL, 1, 'Canoe race', 'Beach Clean-up - Yacht club + Mbokona Stream, Canoe race (Yacht club to Coconut Cafe and back)', NULL, NULL, 1496548800, 0, 0, NULL, NULL, NULL, NULL, 'Museum stream to Yacht club', '', 'yes', 2017),
(193, 0, 'SB', 'coral-triangle-day-celebration', NULL, '', '', '', 'live', NULL, NULL, 'Solomon Islands', '', '', 0, 1497185096, 1497185142, 0, NULL, 0, 'Coral Triangle Day celebration', 'Awareness/display booths &ndash;wildlife, videos, arts and craft,awareness talks &amp; promotions; Quizzes Entertainment, Media outreach andpromotions, Queen Show', NULL, NULL, 1496894400, 0, 0, NULL, NULL, NULL, NULL, 'Museum', '', 'yes', 2017),
(194, 0, 'PH', 'radio-interview', NULL, '', '', '', 'live', NULL, NULL, 'Quezon City', NULL, '14.65533,121.04631', 0, 1497185419, 1497185550, 0, NULL, 0, 'Radio Interview', 'Director Lim will have a radio interview at the Radyo ng Bayan (People&#39;s Radio) Radio Magazine Show - Bakasyon Pilipinas (Vacation Philippines)', NULL, NULL, 1495684800, 0, 0, NULL, NULL, NULL, NULL, 'Radyo Pilipinas Studio', 'Philippine Information Agency Building', 'yes', 2017),
(195, 0, 'SB', 'world-ocean-day-and-coral-triangle-day', NULL, '', '', '', 'live', NULL, NULL, 'Western Province, Solomon Islands', 'New Munda - Kokegolo Rd, Munda, Solomon Islands', '-8.32567,157.26989', 0, 1497349674, 1497349674, NULL, NULL, 0, 'World Ocean Day and Coral Triangle Day', '<span  rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.800000190734863px;">Coastal clean up campaign for munda from Ilangana village to Kindu village. Plastics,cans,bottles,tins mainly.</span>', NULL, NULL, 1496635200, 1497153600, 0, NULL, NULL, NULL, NULL, 'Munda (Ilangana to Kindu)', 'Munda town', NULL, 2017),
(196, 0, 'SB', NULL, NULL, NULL, NULL, NULL, 'draft', NULL, NULL, 'EYxxXlcWd', 'mvvlHewBzedJOibpQh', '', 0, 1503797220, 1503797220, NULL, NULL, 0, 'raNetqukecN', 'rNQa33  <a href="http://ilnadbrccfhh.com/">ilnadbrccfhh</a>, [url=http://mxycwywvdbkp.com/]mxycwywvdbkp[/url], [link=http://jtbrkapsxgqc.com/]jtbrkapsxgqc[/link], http://aciwvaeyybsj.com/', NULL, NULL, 1465448400, 0, 0, 'vfdjivbxtkg', 'rpnxbt@hdxejj.com', 'http://abalculngqig.com/', 'rpnxbt@hdxejj.com', 'RZthnWIRnQGzHZANb', 'yfFtidCjyChFpac', NULL, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `default_ctd_attendance`
--

CREATE TABLE IF NOT EXISTS `default_ctd_attendance` (
  `id` int(11) NOT NULL,
  `ctd_id` int(11) NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `remind_me` enum('y','n') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `inform_changes` enum('y','n') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `vcode` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='People attending the event';

-- --------------------------------------------------------

--
-- Table structure for table `default_ctd_categories`
--

CREATE TABLE IF NOT EXISTS `default_ctd_categories` (
  `id` int(11) NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Event Categories';

-- --------------------------------------------------------

--
-- Table structure for table `default_ctd_partners`
--

CREATE TABLE IF NOT EXISTS `default_ctd_partners` (
  `id` int(11) NOT NULL,
  `ctd_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT '0',
  `type` enum('a','v','d','i','o') COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `mimetype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(5) DEFAULT NULL,
  `height` int(5) DEFAULT NULL,
  `filesize` int(11) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Event partners.';

-- --------------------------------------------------------

--
-- Table structure for table `default_data_fields`
--

CREATE TABLE IF NOT EXISTS `default_data_fields` (
  `id` int(11) unsigned NOT NULL,
  `field_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `field_slug` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `field_namespace` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `field_data` blob,
  `view_options` blob,
  `is_locked` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_data_fields`
--

INSERT INTO `default_data_fields` (`id`, `field_name`, `field_slug`, `field_namespace`, `field_type`, `field_data`, `view_options`, `is_locked`) VALUES
(1, 'lang:blog:intro_label', 'intro', 'blogs', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a363a2273696d706c65223b733a31303a22616c6c6f775f74616773223b733a313a2279223b7d, NULL, 'no'),
(2, 'lang:pages:body_label', 'body', 'pages', 'wysiwyg', 0x613a323a7b733a31313a22656469746f725f74797065223b733a383a22616476616e636564223b733a31303a22616c6c6f775f74616773223b733a313a2279223b7d, NULL, 'no'),
(3, 'lang:user:first_name_label', 'first_name', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a35303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(4, 'lang:user:last_name_label', 'last_name', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a35303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(5, 'lang:profile_company', 'company', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a3130303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(6, 'lang:profile_bio', 'bio', 'users', 'textarea', 0x613a333a7b733a31323a2264656661756c745f74657874223b4e3b733a31303a22616c6c6f775f74616773223b4e3b733a31323a22636f6e74656e745f74797065223b4e3b7d, NULL, 'no'),
(7, 'lang:user:lang', 'lang', 'users', 'pyro_lang', 0x613a313a7b733a31323a2266696c7465725f7468656d65223b733a333a22796573223b7d, NULL, 'no'),
(8, 'lang:profile_dob', 'dob', 'users', 'datetime', 0x613a353a7b733a383a227573655f74696d65223b733a323a226e6f223b733a31303a2273746172745f64617465223b733a353a222d31303059223b733a383a22656e645f64617465223b4e3b733a373a2273746f72616765223b733a343a22756e6978223b733a31303a22696e7075745f74797065223b733a383a2264726f70646f776e223b7d, NULL, 'no'),
(9, 'lang:profile_gender', 'gender', 'users', 'choice', 0x613a353a7b733a31313a2263686f6963655f64617461223b733a33343a22203a204e6f742054656c6c696e670a6d203a204d616c650a66203a2046656d616c65223b733a31313a2263686f6963655f74797065223b733a383a2264726f70646f776e223b733a31333a2264656661756c745f76616c7565223b4e3b733a31313a226d696e5f63686f69636573223b4e3b733a31313a226d61785f63686f69636573223b4e3b7d, NULL, 'no'),
(10, 'lang:profile_phone', 'phone', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a32303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(11, 'lang:profile_mobile', 'mobile', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a32303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(12, 'lang:profile_address_line1', 'address_line1', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(13, 'lang:profile_address_line2', 'address_line2', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(14, 'lang:profile_address_line3', 'address_line3', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b4e3b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(15, 'lang:profile_address_postcode', 'postcode', 'users', 'text', 0x613a323a7b733a31303a226d61785f6c656e677468223b693a32303b733a31333a2264656661756c745f76616c7565223b4e3b7d, NULL, 'no'),
(16, 'lang:profile_website', 'website', 'users', 'url', NULL, NULL, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `default_data_field_assignments`
--

CREATE TABLE IF NOT EXISTS `default_data_field_assignments` (
  `id` int(11) unsigned NOT NULL,
  `sort_order` int(11) NOT NULL,
  `stream_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `is_required` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `is_unique` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `instructions` text COLLATE utf8_unicode_ci,
  `field_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_data_field_assignments`
--

INSERT INTO `default_data_field_assignments` (`id`, `sort_order`, `stream_id`, `field_id`, `is_required`, `is_unique`, `instructions`, `field_name`) VALUES
(1, 1, 1, 1, 'yes', 'no', NULL, NULL),
(2, 1, 2, 2, 'no', 'no', NULL, NULL),
(3, 1, 3, 3, 'yes', 'no', NULL, NULL),
(4, 2, 3, 4, 'yes', 'no', NULL, NULL),
(5, 3, 3, 5, 'no', 'no', NULL, NULL),
(6, 4, 3, 6, 'no', 'no', NULL, NULL),
(7, 5, 3, 7, 'no', 'no', NULL, NULL),
(8, 6, 3, 8, 'no', 'no', NULL, NULL),
(9, 7, 3, 9, 'no', 'no', NULL, NULL),
(10, 8, 3, 10, 'no', 'no', NULL, NULL),
(11, 9, 3, 11, 'no', 'no', NULL, NULL),
(12, 10, 3, 12, 'no', 'no', NULL, NULL),
(13, 11, 3, 13, 'no', 'no', NULL, NULL),
(14, 12, 3, 14, 'no', 'no', NULL, NULL),
(15, 13, 3, 15, 'no', 'no', NULL, NULL),
(16, 14, 3, 16, 'no', 'no', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_data_streams`
--

CREATE TABLE IF NOT EXISTS `default_data_streams` (
  `id` int(10) unsigned NOT NULL,
  `stream_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `stream_slug` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `stream_namespace` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stream_prefix` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_options` blob NOT NULL,
  `title_column` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sorting` enum('title','custom') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'title',
  `permissions` text COLLATE utf8_unicode_ci,
  `is_hidden` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `menu_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_data_streams`
--

INSERT INTO `default_data_streams` (`id`, `stream_name`, `stream_slug`, `stream_namespace`, `stream_prefix`, `about`, `view_options`, `title_column`, `sorting`, `permissions`, `is_hidden`, `menu_path`) VALUES
(1, 'lang:blog:blog_title', 'blog', 'blogs', NULL, NULL, 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(2, 'Default', 'def_page_fields', 'pages', NULL, 'A simple page type with a WYSIWYG editor that will get you started adding content.', 0x613a323a7b693a303b733a323a226964223b693a313b733a373a2263726561746564223b7d, NULL, 'title', NULL, 'no', NULL),
(3, 'lang:user_profile_fields_label', 'profiles', 'users', NULL, 'Profiles for users module', 0x613a313a7b693a303b733a31323a22646973706c61795f6e616d65223b7d, 'display_name', 'title', NULL, 'no', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_def_page_fields`
--

CREATE TABLE IF NOT EXISTS `default_def_page_fields` (
  `id` int(9) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_def_page_fields`
--

INSERT INTO `default_def_page_fields` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `body`) VALUES
(1, '2015-05-06 12:19:32', '2015-05-22 07:36:00', 1, NULL, '<p>Welcome to our homepage. We have not quite finished setting up our website yet, but please add us to your bookmarks and come back soon.</p>\n'),
(2, '2015-05-06 12:19:32', NULL, 1, NULL, '<p>To contact us please fill out the form below.</p>\n				{{ contact:form name="text|required" email="text|required|valid_email" subject="dropdown|Support|Sales|Feedback|Other" message="textarea" attachment="file|zip" }}\n					<div><label for="name">Name:</label>{{ name }}</div>\n					<div><label for="email">Email:</label>{{ email }}</div>\n					<div><label for="subject">Subject:</label>{{ subject }}</div>\n					<div><label for="message">Message:</label>{{ message }}</div>\n					<div><label for="attachment">Attach  a zip file:</label>{{ attachment }}</div>\n				{{ /contact:form }}'),
(3, '2015-05-06 12:19:32', NULL, 1, NULL, '{{ search:form class="search-form" }} \n		<input name="q" placeholder="Search terms..." />\n	{{ /search:form }}'),
(4, '2015-05-06 12:19:32', NULL, 1, NULL, '{{ search:form class="search-form" }} \n		<input name="q" placeholder="Search terms..." />\n	{{ /search:form }}\n\n{{ search:results }}\n\n	{{ total }} results for "{{ query }}".\n\n	<hr />\n\n	{{ entries }}\n\n		<article>\n			<h4>{{ singular }}: <a href="{{ url }}">{{ title }}</a></h4>\n			<p>{{ description }}</p>\n		</article>\n\n	{{ /entries }}\n\n        {{ pagination }}\n\n{{ /search:results }}'),
(5, '2015-05-06 12:19:32', '2015-05-29 02:12:12', 1, NULL, '<div class="containt-800" style="width: 800px; max-width: 800px; margin: 33px auto 0px; background:#f0f0f0; padding: 36px 50px 35px;">\n<div class="row" style="margin-left:0px; margin-right:0px;">\n<p style="font-size: 28px; font-weight: bold; margin-bottom: 11px;">Sorry, that page does not exist.</p>\n\n<p style="font-size: 16px; font-weight: normal; margin-bottom: 5px;">You might want to...</p>\n\n<p style="font-weight: normal; margin-bottom: 7px; margin-top: 7px;"><span style="padding-right:10px; font-size:12px; color:#2B9CBE;">►</span><a href="{{url:site}}" style="font-size: 16px;">Go back to the homepage</a></p>\n\n<p style="font-weight: normal;"><span style="padding-right:10px; font-size:12px; color:#2B9CBE;">►</span><a href="{{url:site}}contest" style="font-size: 16px;">Find out more about the Instagram contest</a></p>\n</div>\n</div>\n'),
(6, '2015-05-12 09:06:16', '2015-05-12 18:40:29', 1, 1, '<div class="clearfix">&nbsp;</div>\n\n<h3>Hashtag: #coraltriangle</h3>\n{{instagram_api:feeds limit=&quot;12&quot;}}\n\n<div class="col-sm-3"><img class="img-responsive" data-pyroimage="true" src="{{images.low_resolution.url}}" />\n<p>By {{user}} | Likes: {{likes}}</p>\n</div>\n{{/instagram_api:feeds}}\n\n<hr />\n<div class="clearfix">&nbsp;</div>\n'),
(9, '2015-05-14 09:48:54', '2017-05-18 08:13:16', 1, 3, '<div class="containt-800" style="width:800px; max-width:800px; margin:0 auto;">\n<h2 class="title-page" style="text-transform: uppercase; text-align: center; margin-top: 41px; margin-bottom: 26px; font-family: &quot;Titillium Web&quot;,Roboto,sans-serif; font-weight: bold; font-size: 33px;">about</h2>\n\n<p style="margin-bottom: 35px; line-height:1.5;">If coral reefs are the rainforests of the seas, then the Coral Triangle is the underwater equivalent of the Amazon. In this region that&rsquo;s half the size of the United States and passes through six countries (the Philippines, Indonesia, Malaysia, Papua New Guinea Solomon Islands and Timor Leste), there are more marine species than anywhere else on the planet.</p>\n</div>\n<style type="text/css">ul.list-in-pages {\n		list-style-type: disc;\n	}\n	blockquote {\n		padding: 0px 20px 14px;\n		margin: 0px 0px 20px;\n		font-size: 17.5px;\n		border-left: none;\n	}\n	.no-pad-left{\n		padding-left:0px;\n	}\n	.no-pad-right{\n		padding-right:0px;\n	}\n	.pad-right-20{\n		padding-right:20px;\n	}\n	.pad-left-20{\n		padding-left:20px;\n	}\n	.align-center{\n		text-align:center;\n	}\n</style>\n<div class="containt-800" style="width:800px; max-width:800px; margin:0 auto;">\n<div style="margin-bottom:40px;"><iframe allowfullscreen="" class="embed-youtube" frameborder="0" id="youTubePlayerAbout" src="https://www.youtube.com/embed/ZEJVNFo0Jh0?enablejsapi=1&amp;controls=0&amp;showinfo=0&amp;autohide=1" style="width:100%; height:450px;"></iframe></div>\n</div>\n\n<div class="containt-800" style="width:800px; max-width:800px; margin:0 auto;">\n<div class="row" style="margin-bottom: 36px;">\n<div class="col-sm-3">\n<div style="border-right: 1px solid rgb(215, 215, 215); width: 93%;">\n<h2 style="font-size:60px; font-family: ''Titillium Web'',Roboto,sans-serif; font-weight:bold; color:#000; margin-top:0px; margin-bottom: 30px; line-height: 39px;">76%</h2>\n\n<div style="width:74%; border-top:5px solid #37aed2; height: 0px;">&nbsp;</div>\n\n<h5 style="font-size: 14px; font-family: Roboto; text-align:left; margin-top: 24px; margin-bottom: 0px;">of all known<br />\ncoral species</h5>\n</div>\n</div>\n\n<div class="col-sm-3">\n<div style="border-right: 1px solid rgb(215, 215, 215); width: 93%;">\n<h2 style="font-size:60px; font-family: ''Titillium Web'',Roboto,sans-serif; font-weight:bold; color:#000; margin-top:0px; margin-bottom: 30px; line-height: 39px;">6/7</h2>\n\n<div style="width:74%; border-top:5px solid #37aed2; height: 0px;">&nbsp;</div>\n\n<h5 style="font-size: 14px; font-family: Roboto; text-align:left; margin-top: 24px; margin-bottom: 0px;">of the world&#39;s<br />\nmarine turtle species</h5>\n</div>\n</div>\n\n<div class="col-sm-3">\n<div style="border-right: 1px solid rgb(215, 215, 215); width: 93%;">\n<h2 style="font-size:60px; font-family: ''Titillium Web'',Roboto,sans-serif; font-weight:bold; color:#000; margin-top:0px; margin-bottom: 30px; line-height: 39px;">37%</h2>\n\n<div style="width:74%; border-top:5px solid #37aed2; height: 0px;">&nbsp;</div>\n\n<h5 style="font-size: 14px; font-family: Roboto; text-align:left; margin-top: 24px; margin-bottom: 0px;">of the world&#39;s<br />\ncoral reef fish species</h5>\n</div>\n</div>\n\n<div class="col-sm-3 last-fact">\n<h2 style="font-size:60px; font-family: ''Titillium Web'',Roboto,sans-serif; font-weight:bold; color:#000; margin-top:0px; margin-bottom: 30px; line-height: 39px;">130</h2>\n\n<div style="width:74%; border-top:5px solid #37aed2; height: 0px;">&nbsp;</div>\n\n<h5 style="font-size: 14px; font-family: Roboto; text-align:left; margin-top: 24px; margin-bottom: 0px;">million people<br />\ndirectly dependent on<br />\nmarine natural resources</h5>\n</div>\n</div>\n</div>\n\n<div class="containt-800" style="width:800px; max-width:800px; margin:0 auto;">\n<div class="row">\n<div class="col-sm-12">\n<p style="margin-bottom:27px; line-height:1.5;">Just as the Amazon is the symbol of the world&rsquo;s rainforests &ndash; the so-called lungs of the earth &ndash; the Coral Triangle is developing iconic status as a marine treasure &ndash; the centre of the global marine diversity.</p>\n\n<p style="margin-bottom:27px; line-height:1.5;">This where the Coral Triangle Day comes in.</p>\n\n<p style="margin-bottom:27px; line-height:1.5;">Held every June 9, this is a celebration of the Coral Triangle. The event is celebrated in several locations around the Coral Triangle region through simultaneous activities such as beach clean-ups, mangrove planting, sustainable seafood dinners and exhibitions, bazaars, and beach parties, among others.</p>\n\n<p style="margin-bottom:27px; line-height:1.5; font-weight:bold;"><a href="/past-coral-triangle-days/2016/">Click here to see Coral Triangle Day celebrations from 2012 to 2016</a></p>\n\n<div class="col-xs-12 align-center"><a class="btn btn-primary" href="{{ url:site }}files/download/c07e93e289f7d36" style="margin-top:7px;">DOWNLOAD THE F.A.Q</a></div>\n</div>\n</div>\n</div>\n'),
(10, '2015-05-14 09:50:08', '2017-05-18 00:18:12', 1, 4, '<div style="text-align: center; margin-top:40px;"><strong><span style="font-size:24px; ">Event Gallery<br />\n​</span></strong></div>\n\n<div style="text-align: justify;   margin: auto; max-width: 1210px;"><span style="font-size:14px;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, &nbsp;sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, &nbsp;sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</span></div>\n'),
(7, '2015-05-12 10:04:08', '2015-05-22 07:37:16', 1, 2, '{{theme:partial name=&quot;instagram&quot;}}'),
(11, '2015-05-14 10:24:42', '2015-05-22 07:37:50', 1, 5, '{{ theme:partial name=&quot;event&quot; }}'),
(12, '2017-05-18 00:16:21', NULL, 2, 6, '<span style="font-size: 13px;">{{theme:partial name=&quot;instagram-2015&quot;}}</span>'),
(13, '2017-05-18 00:16:37', NULL, 2, 7, '<span style="font-size: 13px;">{{theme:partial name=&quot;instagram-2016&quot;}}</span>'),
(14, '2017-05-18 00:16:51', NULL, 2, 8, '<span style="font-size: 13px;">{{theme:partial name=&quot;instagram-2017&quot;}}</span>');

-- --------------------------------------------------------

--
-- Table structure for table `default_email_templates`
--

CREATE TABLE IF NOT EXISTS `default_email_templates` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` int(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_email_templates`
--

INSERT INTO `default_email_templates` (`id`, `slug`, `name`, `description`, `subject`, `body`, `lang`, `is_default`, `module`) VALUES
(1, 'comments', 'Comment Notification', 'Email that is sent to admin when someone creates a comment', 'You have just received a comment from {{ name }}', '<h3>You have received a comment from {{ name }}</h3>\n				<p>\n				<strong>IP Address: {{ sender_ip }}</strong><br/>\n				<strong>Operating System: {{ sender_os }}<br/>\n				<strong>User Agent: {{ sender_agent }}</strong>\n				</p>\n				<p>{{ comment }}</p>\n				<p>View Comment: {{ redirect_url }}</p>', 'en', 1, 'comments'),
(2, 'contact', 'Contact Notification', 'Template for the contact form', '{{ settings:site_name }} :: {{ subject }}', 'This message was sent via the contact form on with the following details:\n				<hr />\n				IP Address: {{ sender_ip }}\n				OS {{ sender_os }}\n				Agent {{ sender_agent }}\n				<hr />\n				{{ message }}\n\n				{{ name }},\n\n				{{ email }}', 'en', 1, 'pages'),
(3, 'registered', 'New User Registered', 'Email sent to the site contact e-mail when a new user registers', '{{ settings:site_name }} :: You have just received a registration from {{ name }}', '<h3>You have received a registration from {{ name }}</h3>\n				<p><strong>IP Address: {{ sender_ip }}</strong><br/>\n				<strong>Operating System: {{ sender_os }}</strong><br/>\n				<strong>User Agent: {{ sender_agent }}</strong>\n				</p>', 'en', 1, 'users'),
(4, 'activation', 'Activation Email', 'The email which contains the activation code that is sent to a new user', '{{ settings:site_name }} - Account Activation', '<p>Hello {{ user:first_name }},</p>\n				<p>Thank you for registering at {{ settings:site_name }}. Before we can activate your account, please complete the registration process by clicking on the following link:</p>\n				<p><a href="{{ url:site }}users/activate/{{ user:id }}/{{ activation_code }}">{{ url:site }}users/activate/{{ user:id }}/{{ activation_code }}</a></p>\n				<p>&nbsp;</p>\n				<p>In case your email program does not recognize the above link as, please direct your browser to the following URL and enter the activation code:</p>\n				<p><a href="{{ url:site }}users/activate">{{ url:site }}users/activate</a></p>\n				<p><strong>Activation Code:</strong> {{ activation_code }}</p>', 'en', 1, 'users'),
(5, 'forgotten_password', 'Forgotten Password Email', 'The email that is sent containing a password reset code', '{{ settings:site_name }} - Forgotten Password', '<p>Hello {{ user:first_name }},</p>\n				<p>It seems you have requested a password reset. Please click this link to complete the reset: <a href="{{ url:site }}users/reset_pass/{{ user:forgotten_password_code }}">{{ url:site }}users/reset_pass/{{ user:forgotten_password_code }}</a></p>\n				<p>If you did not request a password reset please disregard this message. No further action is necessary.</p>', 'en', 1, 'users'),
(6, 'new_password', 'New Password Email', 'After a password is reset this email is sent containing the new password', '{{ settings:site_name }} - New Password', '<p>Hello {{ user:first_name }},</p>\n				<p>Your new password is: {{ new_password }}</p>\n				<p>After logging in you may change your password by visiting <a href="{{ url:site }}edit-profile">{{ url:site }}edit-profile</a></p>', 'en', 1, 'users'),
(7, 'on-june-9th', 'ON JUNE 9TH', 'ON JUNE 9TH', 'Happy Coral Triangle Day!', '<div>Dear {{submitter_name}},<br />\n&nbsp;</div>\n\n<div>We hope you had a great Coral Triangle Day on 9 June!<br />\n&nbsp;</div>\n\n<div>With this email we would like to remind you that we welcome photos of your Coral Triangle Day events through this link:&nbsp;{{photo_submission_url}}. We&#39;ll be sharing them on&nbsp;<a href="http://www.coraltriangleday.org/" target="_blank">www.coraltriangleday.org</a>&nbsp;<wbr />and <a href="https://www.flickr.com/photos/97485135@N06/collections" target="_blank">Flickr</a>&nbsp;for the world to see and relive.<br />\n&nbsp;</div>\n\n<div>Many thanks in advance!<br />\n&nbsp;</div>\n\n<div>Regards,<br />\n&nbsp;</div>\n\n<div>Coral Triangle Day Organizers</div>', 'en', 0, ''),
(8, 'upon-event-submission', 'UPON EVENT SUBMISSION', 'UPON EVENT SUBMISSION', 'Coral Triangle Day Event Submission', '<div style="font-size:12.8000001907349px">Dear {{submitted_by_name}},</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Thank you for submitting the Coral Triangle Day event &quot;{{title}}&quot;! We&#39;re very happy that you&#39;ve chosen to participate.&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Your event is now queued for review&nbsp;and you will be notified as soon as it&#39;s been approved (at which point it will apppear on <a href="http://www.coraltriangleday.org" target="_blank">www.coraltriangleday.org</a>).</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">If you wish to make any changes to your event&#39;s details, please contact&nbsp;<a href="mailto:contest@coraltriangleday.org" target="_blank">contest@coraltriangleday.org</a>.</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">If you have not already done so, please participate in the Coral Triangle Day #noplace4plastic Instagram contest and stand a chance to win a stay at one of the Coral Triangle&#39;s most exclusive 5 star nature resorts, a Borneo dive holiday, Cressi&#39;s 2016 mask &amp; snorkel, underwater dive guides &amp; more!: <a href="http://www.coraltriangleday.org/contest" target="_blank">www.coraltriangleday.org/<wbr />contest</a>.&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Regards,</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Coral Triangle Day Organizers</div>', 'en', 0, ''),
(9, 'upon-event-approval', 'UPON EVENT APPROVAL', 'UPON EVENT APPROVAL', 'Your Coral Triangle Day event has been approved!', '<div style="font-size:12.8000001907349px">Dear {{submitted_by_name}}</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Congratulations! Your Coral Triangle Day event, &quot;{{title}}&quot;, is approved and is now accessible on <a href="http://www.coraltriangleday.org" target="_blank">www.coraltriangleday.org</a>.&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">We welcome any photos from the event, which we will share on our <a href="https://www.flickr.com/photos/97485135@N06/collections">Flickr</a> gallery. Please upload your photos on: {{linksubmitphoto}}</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Good luck with your Coral Triangle Day event!</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Regards,</div>\n\n<div style="font-size:12.8000001907349px">&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Coral Triangle Day Organizers</div>', 'en', 0, ''),
(10, 'upon-event-submission-admin', 'UPON EVENT SUBMISSION ADMIN', 'UPON EVENT SUBMISSION ADMIN', 'NEW EVENT SUBMISSION', '<div style="font-size:12.8000001907349px">Dear Admin,</div>\n\n<div style="font-size:12.8000001907349px">New incoming CTD event submission alert:<br />\n&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">\n<table border="1" cellpadding="1" cellspacing="1">\n	<tbody>\n		<tr>\n			<td>Submitter</td>\n			<td>: {{submitted_by_name}}</td>\n		</tr>\n		<tr>\n			<td>Email</td>\n			<td>: {{submitted_by_email}}</td>\n		</tr>\n		<tr>\n			<td>Event</td>\n			<td>: {{title}}</td>\n		</tr>\n		<tr>\n			<td>Description</td>\n			<td>: {{intro}}</td>\n		</tr>\n		<tr>\n			<td>From</td>\n			<td>: {{date_from_str}}</td>\n		</tr>\n		<tr>\n			<td>To</td>\n			<td>: {{date_to_str}}</td>\n		</tr>\n		<tr>\n			<td>Venue</td>\n			<td>: {{event_venue}}</td>\n		</tr>\n		<tr>\n			<td>Address</td>\n			<td>: {{venue_address}}</td>\n		</tr>\n		<tr>\n			<td>Country</td>\n			<td>: {{country}}</td>\n		</tr>\n		<tr>\n			<td>City</td>\n			<td>: {{location}}</td>\n		</tr>\n		<tr>\n			<td>Event URL</td>\n			<td>: {{website}}</td>\n		</tr>\n		<tr>\n			<td>Contact</td>\n			<td>: {{contact_email}}</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<div style="font-size:12.8000001907349px"><br />\nYou can review the submission here: {{linkadminedit}}</div>\n\n<div style="font-size:12.8000001907349px">Thank you!</div>', 'en', 0, ''),
(11, 'upon-photo-submission', 'UPON PHOTO SUBMISSION', 'UPON PHOTO SUBMISSION', 'Thank you for your Coral Triangle Day photos!', '<span><span contenteditable="false" style="height: 0px; padding: 0px; margin: 0px; display: block; z-index: 9999; color: rgb(255, 255, 255); font-size: 0px; line-height: 0px; position: absolute; border-top: 1px dashed rgb(255, 0, 0); -moz-user-select: none; left: 0px; right: 0px; top: 186px;"><span style="width:0px;height:0px;padding:0px;margin:0px;display:block;z-index:9999;color:#fff;position:absolute;font-size: 0px;line-height:0px;border-color:transparent;display:block;border-style:solid;right:0px;border-right-color:#ff0000;border-width:8px 8px 8px 0;top:-8px">&nbsp;</span><span style="width:0px;height:0px;padding:0px;margin:0px;display:block;z-index:9999;color:#fff;position:absolute;font-size: 0px;line-height:0px;border-color:transparent;display:block;border-style:solid;left:0px;border-left-color:#ff0000;border-width:8px 0 8px 8px;top:-8px">&nbsp;</span><span contenteditable="false" style="width:0px;height:0px;padding:0px;margin:0px;display:block;z-index:9999;color:#fff;position:absolute;font-size: 0px;line-height:0px;height:17px;width:17px;right:17px;background:url(http://ctd.projects.catalyzecommunications.com/system/cms/themes/pyrocms/js/ckeditor/plugins/magicline/images/icon.png) center no-repeat #ff0000;cursor:pointer;top:-8px;-moz-border-radius:2px;border-radius:2px" title="Insert paragraph here">&crarr;</span></span></span>\n<div style="font-size:12.8000001907349px">Dear {{name_submit_photo}},<br />\n&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Thank you for sharing your Coral Triangle Day photos!&nbsp;<br />\n&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">We will review and add them to the gallery page at&nbsp;<a href="http://www.coraltriangleday.org/" target="_blank">www.coraltriangleday.org</a>&nbsp;or on our <a href="https://www.flickr.com/photos/97485135@N06/collections">Flickr account</a>.&nbsp;<br />\n&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Regards,<br />\n&nbsp;</div>\n\n<div style="font-size:12.8000001907349px">Coral Triangle Day Organizers</div>', 'en', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `default_files`
--

CREATE TABLE IF NOT EXISTS `default_files` (
  `id` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '1',
  `type` enum('a','v','d','i','o') COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mimetype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `width` int(5) DEFAULT NULL,
  `height` int(5) DEFAULT NULL,
  `filesize` int(11) NOT NULL DEFAULT '0',
  `alt_attribute` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download_count` int(11) NOT NULL DEFAULT '0',
  `date_added` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_files`
--

INSERT INTO `default_files` (`id`, `folder_id`, `user_id`, `type`, `name`, `filename`, `path`, `description`, `extension`, `mimetype`, `keywords`, `width`, `height`, `filesize`, `alt_attribute`, `download_count`, `date_added`, `sort`) VALUES
('83705a79fb02240', 2, 1, 'd', 'CT_Day_FAQ_Updated2015.pdf', '28ccd38881d68cfcac0a3a930ed5948c.pdf', '{{ url:site }}files/download/83705a79fb02240', '', '.pdf', 'application/pdf', '', 0, 0, 33, '', 721, 1432290027, 0),
('ddf1bf379a7f1e3', 2, 2, 'd', 'Poster-CT-Day-Sponsors-160518.pdf', '3e17e6fb072614884f4467e19c14fac8.pdf', '{{ url:site }}files/download/ddf1bf379a7f1e3', '', '.pdf', 'application/pdf', '', 0, 0, 799, 'undefined', 248, 1463569812, 0),
('08b2ef79b3a79a8', 2, 2, 'd', 'Poster-CT-Day-Sponsors-160527-alt1.pdf', '6884ed39eca3d11fd98575283e4be61f.pdf', '{{ url:site }}files/download/08b2ef79b3a79a8', '', '.pdf', 'application/pdf', '', 0, 0, 1539, 'undefined', 432, 1464416530, 0),
('c07e93e289f7d36', 4, 2, 'd', 'CT_Day_FAQ_Updated2017.pdf', 'f2dc7193ae4cedd574c0efe143d87285.pdf', '{{ url:site }}files/download/c07e93e289f7d36', '', '.pdf', 'application/pdf', '', 0, 0, 31, 'undefined', 296, 1495123971, 0);

-- --------------------------------------------------------

--
-- Table structure for table `default_file_folders`
--

CREATE TABLE IF NOT EXISTS `default_file_folders` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'local',
  `remote_container` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date_added` int(11) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_file_folders`
--

INSERT INTO `default_file_folders` (`id`, `parent_id`, `slug`, `name`, `location`, `remote_container`, `date_added`, `sort`, `hidden`) VALUES
(2, 0, 'pdf', 'pdf', 'local', '', 1432289982, 1432289982, 0),
(4, 0, 'about', 'about', 'local', '', 1495123921, 1495123921, 0);

-- --------------------------------------------------------

--
-- Table structure for table `default_galleries`
--

CREATE TABLE IF NOT EXISTS `default_galleries` (
  `id` int(10) unsigned NOT NULL,
  `gallery_year` int(11) NOT NULL DEFAULT '2015',
  `slug` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `extension` varchar(5) DEFAULT NULL,
  `thumbnail` int(11) NOT NULL,
  `status` enum('draft','live') NOT NULL,
  `created_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `default_galleries`
--

INSERT INTO `default_galleries` (`id`, `gallery_year`, `slug`, `category_id`, `title`, `subtitle`, `description`, `extension`, `thumbnail`, `status`, `created_on`) VALUES
(1, 2015, 'homepage-mosaic', 1, 'Homepage Mosaic', '', 'Homepage mosaic', NULL, 0, 'live', 1431618488),
(2, 2015, 'sponsors', 2, 'Sponsors', '', 'Sponsors gallery', NULL, 0, 'live', 1431983857),
(3, 2015, 'partners-gallery', 3, 'Partners Gallery', '', '', NULL, 0, 'live', 1431983893),
(4, 2015, 'organizer', 0, 'Organizer', '', 'Organzer&#39;s logos', NULL, 0, 'live', 1432039998),
(5, 2015, 'events', 0, 'Events', '', 'Events Photos', NULL, 0, 'live', 1432229382),
(6, 2015, 'ctd-photos-2014', 0, 'CTD Photos 2014', 'CTD Photos 2014', '', NULL, 0, 'live', 1435726832),
(7, 2015, 'ctd-sponsor-2013', 0, 'CTD Sponsor 2013', 'CTD Sponsor 2013', 'CTD Sponsor 2013', NULL, 0, 'live', 1435726877),
(8, 2015, 'ctd-photos-2013', 0, 'CTD Photos 2013', '', '', NULL, 0, 'live', 1436531322),
(9, 2015, 'ctd-photos-2012', 0, 'CTD Photos 2012', '', '', NULL, 0, 'live', 1436531344),
(10, 2015, 'ctd-sponsor-2014', 0, 'CTD Sponsor 2014', '', '', NULL, 0, 'live', 1436532813),
(11, 2016, 'homepage-mosaic-2016', 0, 'homepage mosaic 2016', 'homepage mosaic 2016', 'homepage mosaic 2016', NULL, 0, 'live', 1463569124),
(12, 2016, 'sponsors-2016', 0, 'Sponsors 2016', 'Sponsors 2016', '<span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 13.5px; background-color: rgb(245, 245, 245);">Sponsors 2016</span>', NULL, 0, 'live', 1463569144),
(13, 2016, 'organizer-2016', 0, 'Organizer 2016', 'Organizer 2016', '<span style="font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 13.5px; background-color: rgb(249, 249, 249);">Organizer 2016</span>', NULL, 0, 'live', 1463569159),
(14, 2017, 'homepage-mosaic-2012-to-2016', 0, 'Homepage mosaic 2012 to 2016', 'Homepage mosaic 2012 to 2016', '<span style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13px; background-color: rgb(249, 249, 249);">Homepage mosaic 2012 to 2016</span>', NULL, 0, 'live', 1495095109),
(15, 2017, 'sponsors-2017', 2, 'Sponsors 2017', 'Sponsors 2017', '<span background-color:="" font-size:="" helvetica="" style="font-family: ">Sponsors 2017</span>', NULL, 0, 'live', 1495095121),
(16, 2017, 'organizer-2017', 0, 'Organizer 2017', '', '', NULL, 0, 'live', 1497187620);

-- --------------------------------------------------------

--
-- Table structure for table `default_gallery_categories`
--

CREATE TABLE IF NOT EXISTS `default_gallery_categories` (
  `id` int(11) NOT NULL,
  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'live',
  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Gallery Categories';

--
-- Dumping data for table `default_gallery_categories`
--

INSERT INTO `default_gallery_categories` (`id`, `status`, `slug`, `title`) VALUES
(1, 'live', 'homepage', 'Homepage'),
(2, 'live', 'sponsors', 'Sponsors'),
(3, 'live', 'partners', 'Partners');

-- --------------------------------------------------------

--
-- Table structure for table `default_gallery_media`
--

CREATE TABLE IF NOT EXISTS `default_gallery_media` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  `description` text,
  `media` text NOT NULL,
  `extension` varchar(5) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `thumbnail` varchar(100) DEFAULT NULL,
  `thumbnail_small` varchar(200) DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'live',
  `uploaded_on` int(11) NOT NULL DEFAULT '0',
  `uploaded_by` varchar(64) DEFAULT NULL,
  `vuploaded_by` varchar(100) DEFAULT NULL,
  `vuploaded_by_email` varchar(100) DEFAULT NULL,
  `external_url` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=768 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `default_gallery_media`
--

INSERT INTO `default_gallery_media` (`id`, `gallery_id`, `position`, `title`, `subtitle`, `description`, `media`, `extension`, `meta_title`, `meta_description`, `meta_keywords`, `thumbnail`, `thumbnail_small`, `status`, `uploaded_on`, `uploaded_by`, `vuploaded_by`, `vuploaded_by_email`, `external_url`) VALUES
(130, 1, 1, 'Malaysia', 'Marine conservation talk and beach clean up', NULL, 'IMG_7350.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433997851, 'Alvin Chelliah/ Reef Check Malaysia', NULL, 'alvin@reefcheck.org.my', 'https://www.flickr.com/photos/97485135@N06/sets/72157654439395341'),
(129, 1, 2, 'Malaysia', 'Marine conservation talk and beach clean up', '', 'IMG_7355.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433997847, 'Alvin Chelliah/ Reef Check Malaysia', NULL, 'alvin@reefcheck.org.my', NULL),
(128, 1, 3, 'Malaysia', 'Marine conservation talk and beach clean up', NULL, 'IMG_7353.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433997844, 'Alvin Chelliah/ Reef Check Malaysia', NULL, 'alvin@reefcheck.org.my', 'https://www.flickr.com/photos/97485135@N06/sets/72157654439395341'),
(127, 1, 4, 'Malaysia', 'Marine conservation talk and beach clean up', NULL, 'G0250300.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1433997831, 'Alvin Chelliah/ Reef Check Malaysia', NULL, 'alvin@reefcheck.org.my', 'https://www.flickr.com/photos/97485135@N06/sets'),
(35, 3, 2, '', '', '', 'WWF.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1432056549, '1', NULL, NULL, NULL),
(36, 3, 3, '', '', '', 'Conservation_International.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1432056551, '1', NULL, NULL, NULL),
(37, 3, 4, '', '', '', 'The_Nature_Conservancy.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1432056554, '1', NULL, NULL, NULL),
(38, 3, 5, '', '', '', 'GEF.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1432056558, '1', NULL, NULL, NULL),
(39, 3, 6, '', '', '', 'ADB.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1432056561, '1', NULL, NULL, NULL),
(40, 3, 7, '', '', '', 'CTC.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1432056563, '1', NULL, NULL, NULL),
(41, 3, 8, '', '', '', 'USAID.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1432056564, '1', NULL, NULL, NULL),
(42, 3, 9, '', '', '', 'Australian_AID.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1432056564, '1', NULL, NULL, NULL),
(43, 3, 1, '', '', '', 'CTI_CFF_resize.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1432056588, '1', NULL, NULL, NULL),
(85, 4, 999, '', '', NULL, 'RCM_2011_small.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433497551, 'Julian Hyde', NULL, NULL, 'www.reefcheck.org.my'),
(86, 4, 999, '', '', '', 'Yayasan_Mattirotasi.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433531438, 'Dwi Aryo', NULL, NULL, 'https://www.facebook.com/pages/Yayasan-Mattirotasi/1535217686759091'),
(84, 2, 999, '', '', NULL, 'Logo_High_MercureResortSanur.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1433410833, 'EDWIN SHRI BIMO', NULL, NULL, ''),
(100, 4, 999, '', '', '', 'Logo_DOFS_(English).jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433823994, 'Sabrina Makajil', NULL, NULL, ''),
(101, 4, 999, '', '', '', 'UMS.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1433823994, 'Sabrina Makajil', NULL, NULL, ''),
(51, 5, 999, 'Malaysia', 'Pertunjukan Wayang Golek 2015', '', 'th_f522c54922ef46e2757a648d981cc9f0_events0396.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1432229442, '0', NULL, NULL, NULL),
(52, 5, 999, 'Indonesia', 'Giant Beach Ball Bouncing Competition', '', 'Festival_Ferry-Corsten_FlashBack-Paradiso-credits-tillate.com00_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1432230047, 'Okky Lagi', NULL, NULL, NULL),
(83, 2, 999, '', '', NULL, 'Sunday_Market_Sanur.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1433410832, 'EDWIN SHRI BIMO', NULL, NULL, ''),
(88, 4, 999, '', '', '', 'WWF.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433759965, 'Rendy Mulyono', NULL, NULL, 'www.paprika.com'),
(82, 4, 999, '', '', NULL, 'CTC_new_logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1433410831, 'EDWIN SHRI BIMO', NULL, NULL, 'coraltrianglecenter.org/'),
(71, 2, 999, '', '', '', 'Australian_AID.jpg', '.jpg', '', '', '', NULL, NULL, 'live', 1432804280, 'Rendy Mulyono', NULL, NULL, 'http://dfat.gov.au/pages/default.aspx'),
(109, 1, 5, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Briefing_for_the_Fiji_team_before_heading_into_Shangri-la_Fijian_Resort_Spa_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893813, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(108, 1, 6, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Beach_clean-up_activity_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893806, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(107, 4, 999, '', '', '', 'TNCLogoPrimary_RGB2.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433841972, 'Peggy Mariska', NULL, NULL, 'www.nature.or.id'),
(76, 4, 999, '', '', '', 'CTI-SEA.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1433328360, 'Dana Rose Salonoy', NULL, NULL, 'http://iwlearn.net/iw-projects/3589'),
(77, 4, 999, '', '', NULL, 'CTI_Logo_black_PH.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1433328368, 'Dana Rose Salonoy', NULL, NULL, 'www.coraltriangleinitiative.org'),
(78, 4, 999, '', '', '', 'DBPLS.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1433328369, 'Dana Rose Salonoy', NULL, NULL, ''),
(79, 4, 999, '', '', '', 'DENR_Logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1433328370, 'Dana Rose Salonoy', NULL, NULL, 'bmb.gov.ph'),
(80, 2, 999, '', '', NULL, 'ADB_Logo_HR.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1433328373, 'Dana Rose Salonoy', NULL, NULL, 'http://www.adb.org/'),
(81, 2, 999, '', '', NULL, 'Short-GEF_logo_colored_NOTAG_transparent.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1433328374, 'Dana Rose Salonoy', NULL, NULL, 'www.thegef.org/gef/'),
(87, 4, 999, '', '', '', 'CCEF_Logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433757450, 'Moonyeen Nida R. Alava', NULL, NULL, 'www.coast.ph'),
(90, 4, 999, '', '', '', 'FC-1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433762307, 'arifin', NULL, NULL, ''),
(91, 4, 999, '', '', '', 'FC-2.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433762307, 'arifin', NULL, NULL, ''),
(92, 2, 999, '', '', NULL, 'FC-3.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1433762307, 'arifin', NULL, NULL, ''),
(93, 2, 999, '', '', NULL, 'FC-1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1433762307, 'arifin', NULL, NULL, ''),
(94, 4, 999, '', '', '', 'SOS.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433815114, 'Angela Lim', NULL, NULL, 'http://www.saveourseafood.my'),
(95, 4, 999, '', '', '', 'Pick_The_Right_Catch.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1433815114, 'Angela Lim', NULL, NULL, ''),
(96, 4, 999, '', '', '', 'SOS1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433815816, 'Angela Lim', NULL, NULL, ''),
(97, 4, 999, '', '', '', 'WWF-logo-high-res3.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433820656, 'Vilisite Tamani', NULL, NULL, ''),
(98, 4, 999, '', '', '', 'TNCLogoPrimary_RGB.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433821466, 'Peggy Mariska', NULL, NULL, 'http://www.nature.or.id'),
(99, 4, 999, '', '', '', 'TNCLogoPrimary_RGB1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433821995, 'Peggy Mariska', NULL, NULL, 'www.nature.or.id'),
(102, 4, 999, '', '', '', 'KPIM.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433823994, 'Sabrina Makajil', NULL, NULL, ''),
(103, 4, 999, '', '', '', 'index.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433823994, 'Sabrina Makajil', NULL, NULL, ''),
(104, 2, 999, '', '', '', 'ADB_Logo_HR1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433823994, 'Sabrina Makajil', NULL, NULL, ''),
(105, 2, 999, '', '', '', 'GEF_logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1433823995, 'Sabrina Makajil', NULL, NULL, ''),
(106, 2, 999, '', '', '', 'Logo-Header-CTI.png', '.jpg', '', '', '', NULL, NULL, 'live', 1433823995, 'Sabrina Makajil', NULL, NULL, 'http://www.coraltriangleinitiative.org/'),
(110, 1, 7, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Collecting_rubbish_at_Cuvu_Beach_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893814, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(111, 1, 8, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', '', 'Team_1_took_part_in_a_coral_planting_and_Fish_house_building_activity_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893842, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', NULL),
(112, 1, 9, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', '', 'Team_1_took_part_in_a_coral_planting_and_Fish_house_building_activity.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893864, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', NULL),
(113, 1, 10, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Team_2_took_part_in_a_beach_clean-up_activity.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893866, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(114, 1, 11, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'CTP_Leader,_Jackie_Thomas,_helps_out_in_the_laying_out_of_the_cookies_that_the_coral_will_be_planted_on_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893873, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(115, 1, 12, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'CTP_Leader,_Jackie_Thomas,_putting_the_finishing_touches_to_the_fish_house_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893875, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(116, 1, 13, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'ME_Officer,_Una_Malani,_inspects_a_completed_Fish-house_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893878, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(117, 1, 14, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'The_Cuvu_Beach_Clean-up_team_with_their_bags_of_collected_rubbish.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893879, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(118, 1, 15, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Laying_out_of_the_cookies_that_will_hold_the_coral_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893883, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(119, 1, 16, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'WWF-Pacific_(Fiji)_with_GM_for_Shangri-la_Fijian_Resort_Spa.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893931, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(120, 1, 17, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Mereoni_Mataika,_Shangri-las_Service_Manager_for_Marine_Sanctuary,_gives_a_brief_on_CSR_activities_the_hotel_engages_in_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893934, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(121, 1, 18, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'The_team_mixes_the_cement_to_be_used_for_the_construction_of_the_fish_houses.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1433893939, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(122, 1, 19, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Shangri-la_Fijian_Resort_Spa_General_Manager,_Craig_Powell,_with_CTP_Leader,_Jackie_Thomas_-_Copy.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433893942, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(123, 1, 20, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Team_1.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433894314, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(124, 1, 21, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Securing_coral_pieces_on_the_cookies.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433894316, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(125, 1, 22, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'IMG_7919.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1433894320, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(126, 1, 23, 'Fiji', 'Coral Planting and Beach Clean-up with Shangri-la Fijian Resort and Spa', NULL, 'Team_members_secure_the_coral_on_the_coral_tray.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433894322, 'WWF-Pacific (Fiji)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157653913708100/'),
(131, 1, 24, 'Malaysia', 'Marine conservation talk and beach clean up', '', 'IMG_7349.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1433997854, 'Alvin Chelliah/ Reef Check Malaysia', NULL, 'alvin@reefcheck.org.my', NULL),
(132, 1, 25, 'Malaysia', 'Marine conservation talk and beach clean up', '', 'IMG_7348.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1433997863, 'Alvin Chelliah/ Reef Check Malaysia', NULL, 'alvin@reefcheck.org.my', NULL),
(133, 1, 26, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'A_student_from_Emmanuel_Christian_Academy_takes_part_in_the_rubbish_bin_painting_competition.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061103, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157654376137726'),
(134, 1, 27, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'CT_Day_was_a_great_success_due_to_the_support_of_local_businesses._.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061103, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(135, 1, 28, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'A_Gizo_Primary_student_takes_a_break_from_painting_to_smile_for_the_camera._.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061104, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(136, 1, 29, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'Clean-up_efforts_along_the_foreshore.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061105, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(137, 1, 30, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'Gizo_Secondary_School_participants_stand_next_to_their_second_prize_winning_drum.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061105, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157654376137726'),
(138, 1, 31, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'Cleaning_up_along_the_waterfront_of_Gizos_main_market.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061106, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(139, 1, 32, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'RIMG0396.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061108, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(140, 1, 33, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'RIMG0432.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061108, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(141, 1, 34, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'RIMG0475.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061111, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(142, 1, 35, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'Gizo_women_cleaning_up_around_the_town_streets.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434061111, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(143, 1, 36, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'Students_from_Gizo_Primary_school_get_creative_with_stencils_of_marine_life.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061112, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(144, 1, 37, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'JICA_Volunteer,_Naouo_(_green_t-shirt)_and_Deanne_Seppy_talk_about_waste_management_adn_the_new_recycling_program_for_Gizo._.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061113, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(145, 1, 38, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'RIMG0473.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061113, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(146, 1, 39, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'Standing_proud_in_their_CT_Day_t-shirts.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1434061114, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(147, 1, 40, 'Solomon Islands', 'Gizo Town Foreshore cleanup', NULL, 'WEST_Centre_(school_for_the_students_came_pepared_with_a_drawing_of_an_underwater_scene_to_depict_on_their_barrels._.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061118, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(148, 1, 41, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'Young_children_were_out_to_participate_in_the_CT_Day_activities.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061119, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(149, 1, 42, 'Solomon Islands', 'Gizo Town Foreshore cleanup', '', 'The_MSG_building_in_Gizo_Town_was_a_hive_of_activity_during_the_day._The_building_was_the_venue_of_the_rubbish_bin_oainting_competition_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434061121, 'WWF-Pacific (Solomon Islands)', NULL, 'vtamani@wwfpacific.org', NULL),
(150, 4, 999, '', '', '', 'logo-ums(normalbg)_(1).png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1434090636, 'WAHIDATUL HUSNA ZULDIN', NULL, NULL, 'http://www.ums.edu.my/ipmb/index.html'),
(151, 2, 999, '', '', NULL, 'logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1434090636, 'WAHIDATUL HUSNA ZULDIN', NULL, NULL, 'http://www.suteraharbour.com/marina-and-north-borneo-yacht/marina'),
(152, 1, 43, 'Malaysia', 'Penang International Green Carnival 2015', '', '11347853_10153422738988674_1378493943_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434519287, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(153, 1, 44, 'Malaysia', 'Penang International Green Carnival 2015', NULL, '11401406_839477402768182_3254560835550749768_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434519288, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/sets'),
(154, 1, 45, 'Malaysia', 'Penang International Green Carnival 2015', '', '10377077_839477256101530_2593362071451816500_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519288, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(155, 1, 46, 'Malaysia', 'Penang International Green Carnival 2015', '', '11638445_10153422739568674_514975482_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519289, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(156, 1, 47, 'Malaysia', 'Penang International Green Carnival 2015', NULL, '11638872_10153422729663674_1539644893_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434519290, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/sets'),
(157, 1, 48, 'Malaysia', 'Penang International Green Carnival 2015', '', '11211854_10153422744198674_455065593_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519291, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(158, 1, 49, 'Malaysia', 'Penang International Green Carnival 2015', '', '11638902_10153422726888674_1075683873_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519292, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(159, 1, 50, 'Malaysia', 'Penang International Green Carnival 2015', '', '11640448_10153422728703674_1120246616_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519293, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(160, 1, 51, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '1513967_840658289316760_5580619599216333778_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519367, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(161, 1, 52, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '1907487_10153985714918242_1795312769443379746_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519368, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(162, 1, 53, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '11253593_840701479312441_3873926458956848099_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519369, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(163, 1, 54, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', NULL, '10408050_838797319502857_6522961761502208781_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434519369, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/sets'),
(164, 1, 55, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '11257317_10153985715553242_6210532538093114037_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519370, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(165, 1, 56, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '10991431_840701359312453_8918040912870644172_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519371, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(166, 1, 57, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '11391358_10153985714893242_8831045360399665119_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519371, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(167, 1, 58, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '11393160_838796986169557_6084235708476425485_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519372, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(168, 1, 59, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '11406484_10153985714993242_4176365339192684123_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519373, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(169, 1, 60, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '11406901_838796906169565_1337015054310894_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519373, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(170, 1, 61, 'Malaysia', 'Malaysia Sustainable Seafood Festival 2015 (MYSSF''15)', '', '11406904_10153985715413242_4707790250864059349_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434519374, 'WWF - Malaysia / Marine Programme Team', NULL, 'cmw_210@hotmail.com', NULL),
(171, 1, 62, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG_0664_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521001, 'Yayasan Mattirotasi-Makassar', NULL, 'daryo@wwf.or.id', NULL),
(172, 1, 63, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', NULL, 'DSC_6746_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521001, 'Yayasan Mattirotasi-Makassar', NULL, 'daryo@wwf.or.id', 'https://www.flickr.com/photos/97485135@N06/sets'),
(173, 1, 64, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG_1467_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434521002, 'Yayasan Mattirotasi-Makassar', NULL, 'daryo@wwf.or.id', NULL),
(174, 1, 65, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG_1486_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521003, 'Yayasan Mattirotasi-Makassar', NULL, 'daryo@wwf.or.id', NULL),
(175, 1, 66, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG_0680_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521003, 'Yayasan Mattirotasi-Makassar', NULL, 'daryo@wwf.or.id', NULL),
(176, 1, 67, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'DSC_6744_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521005, 'Yayasan Mattirotasi-Makassar', NULL, 'daryo@wwf.or.id', NULL),
(177, 1, 68, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG-20150615-WA0046.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521276, 'WWF Indonesia', NULL, 'daryo@wwf.or.id', NULL),
(178, 1, 69, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG-20150615-WA0060.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521277, 'WWF Indonesia', NULL, 'daryo@wwf.or.id', NULL),
(179, 1, 70, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG-20150615-WA0031.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521278, 'WWF Indonesia', NULL, 'daryo@wwf.or.id', NULL),
(180, 1, 71, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG-20150615-WA0074.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521285, 'WWF Indonesia', NULL, 'daryo@wwf.or.id', NULL),
(181, 1, 72, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', NULL, 'IMG-20150615-WA0068.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434521292, 'WWF Indonesia', NULL, 'daryo@wwf.or.id', 'https://www.flickr.com/photos/97485135@N06/sets'),
(182, 1, 73, 'Indonesia', 'Bukan Pasar Ikan Biasa 2015', '', 'IMG-20150615-WA0004.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434521294, 'WWF Indonesia', NULL, 'daryo@wwf.or.id', NULL),
(183, 1, 74, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', '', '11225751_1017807938250115_3779339883573176123_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434606035, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(184, 1, 75, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', NULL, '11312964_1017807184916857_2447170123333905158_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1434606038, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(185, 1, 76, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', '', '11402643_1017807931583449_519926252846731435_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434606040, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(186, 1, 77, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', NULL, '11118809_1017807918250117_6238097445885858078_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434606041, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', 'https://www.flickr.com/photos/97485135@N06/albums'),
(187, 1, 78, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', '', '11337065_1017807608250148_989133695061833154_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434606046, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(188, 1, 79, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', '', '11426204_1017808921583350_8741608682485339654_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434606046, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(189, 1, 80, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', '', 'EMAIL_SIGNATURE_CT_DAY3.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434606106, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(190, 1, 81, 'Indonesia', 'Coral Triangle Day Celebration in Sanur Bali', '', 'CT_DAY_2015_TEMPLATE2B.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1434606119, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(191, 4, 999, '', '', '', 'CTC_Logo_NEW.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1434614983, 'Coral Triangle Center', NULL, NULL, 'http://coraltrianglecenter.org'),
(192, 2, 999, '', '', '', 'Logo_High_MercureResortSanur1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434614985, 'Coral Triangle Center', NULL, NULL, 'http://www.mercureresortsanur.com/'),
(193, 2, 999, '', '', NULL, 'Mahagiri_Villas.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1434614988, 'Coral Triangle Center', NULL, NULL, 'http://www.mahagirivillassanur.com/'),
(194, 2, 999, '', '', NULL, 'antavaya_events_01.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1434614989, 'Coral Triangle Center', NULL, NULL, 'http://www.antavaya.com/'),
(195, 2, 999, '', '', NULL, 'Screen_Shot_2015-06-18_at_14.43_.31_.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1434614989, 'Coral Triangle Center', NULL, NULL, 'http://www.all4divingindonesia.com/'),
(196, 2, 999, '', '', NULL, 'Screen_Shot_2015-06-18_at_14.12_.57_.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1434614990, 'Coral Triangle Center', NULL, NULL, 'http://spps.pphotels.com/'),
(197, 2, 999, '', '', NULL, 'biowear_bali.jpeg', '.jpeg', NULL, NULL, NULL, NULL, NULL, 'live', 1434614990, 'Coral Triangle Center', NULL, NULL, 'http://www.biowearbali.com/'),
(198, 1, 82, 'Philippines', 'Ocean Trash Talk: Don''t Trash Our Treasures', NULL, '_MG_2669.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434949378, 'Dana Rose J. Salonoy', NULL, 'drjsalonoy@primexinc.org', 'https://plus.google.com/u/0/+CoralTriangleInitiativeSoutheastAsia/posts'),
(199, 1, 83, 'Philippines', 'Ocean Trash Talk: Don''t Trash Our Treasures', NULL, '_MG_2857.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1434949379, 'Dana Rose J. Salonoy', NULL, 'drjsalonoy@primexinc.org', 'https://plus.google.com/u/0/+CoralTriangleInitiativeSoutheastAsia/posts'),
(200, 1, 84, 'Philippines', 'Ocean Trash Talk: Don''t Trash Our Treasures', NULL, 'IMG_2110.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1434949379, 'Dana Rose J. Salonoy', NULL, 'drjsalonoy@primexinc.org', 'https://plus.google.com/u/0/+CoralTriangleInitiativeSoutheastAsia/posts'),
(201, 1, 85, 'Philippines', 'Ocean Trash Talk: Don''t Trash Our Treasures', NULL, 'IMG_2160.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1434949380, 'Dana Rose J. Salonoy', NULL, 'drjsalonoy@primexinc.org', 'https://plus.google.com/u/0/+CoralTriangleInitiativeSoutheastAsia/posts'),
(202, 4, 999, '', '', '', 'CTI-SEA1.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1434968502, 'Dana Salonoy', NULL, NULL, 'http://iwlearn.net/iw-projects/3589'),
(203, 4, 999, '', '', '', 'Lambang_KKP.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1434968508, 'Dana Salonoy', NULL, NULL, ''),
(204, 2, 999, '', '', '', 'ADB_logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434968509, 'Dana Salonoy', NULL, NULL, 'adb.org'),
(205, 2, 999, '', '', '', 'GEF_logo1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1434968509, 'Dana Salonoy', NULL, NULL, 'thegef.org'),
(206, 1, 86, 'Indonesia', 'Sukseskan Coral Triangle Day 2015', NULL, '20150609_104221.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1435045856, 'Dana Rose J. Salonoy', NULL, 'drjsalonoy@primexinc.org', 'https://plus.google.com/+CoralTriangleInitiativeSoutheastAsia/posts/G8YbjxCGXqg'),
(207, 1, 87, 'Indonesia', 'Sukseskan Coral Triangle Day 2015', NULL, '20150609_113044.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1435045856, 'Dana Rose J. Salonoy', NULL, 'drjsalonoy@primexinc.org', 'https://plus.google.com/+CoralTriangleInitiativeSoutheastAsia/posts/G8YbjxCGXqg'),
(208, 1, 88, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'CT_Day_2015_SOCMED_OK.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1435211460, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(209, 1, 89, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0207.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565917, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(210, 1, 90, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0200.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565921, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(211, 1, 91, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0224.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565924, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(212, 1, 92, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0174.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565925, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(213, 1, 93, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0177.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565926, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(214, 1, 94, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0182.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565932, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(215, 1, 95, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0225.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565984, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(216, 1, 96, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0240.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435565991, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(217, 1, 97, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0243.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566009, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(218, 1, 98, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0229.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566021, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(219, 1, 99, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0235.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566023, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(220, 1, 100, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0244.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566024, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(221, 1, 101, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0245.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566047, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(222, 1, 102, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0247.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566057, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(223, 1, 103, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0250.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566061, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(224, 1, 104, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0254.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566080, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(225, 1, 105, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', NULL, 'DSC_0256.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1435566110, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(226, 1, 106, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0261.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566111, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(227, 1, 107, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0258.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566117, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(228, 1, 108, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0263.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566127, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(229, 1, 109, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0264.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566130, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(230, 1, 110, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0265.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566141, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(231, 1, 111, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0268.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566178, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(232, 1, 112, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0267.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566180, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(233, 1, 113, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0273.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566182, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(234, 1, 114, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0274.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566203, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(235, 1, 115, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0282.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566208, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(236, 1, 116, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0272.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566217, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(237, 1, 117, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0297.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566229, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(238, 1, 118, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0285.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566230, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(239, 1, 119, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0288.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566232, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(240, 1, 120, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0298.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566258, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(241, 1, 121, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0343.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566268, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(242, 1, 122, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0351.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566291, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(243, 1, 123, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0350.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566294, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(244, 1, 124, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0352.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566299, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(245, 1, 125, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0362.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566319, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(246, 1, 126, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0349.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566328, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(247, 1, 127, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0353.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566330, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(248, 1, 128, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0385.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566364, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(249, 1, 129, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0370.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566373, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(250, 1, 130, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0368.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566377, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(251, 1, 131, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0425.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566381, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(252, 1, 132, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0391.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566395, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(253, 1, 133, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0422.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566398, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(254, 1, 134, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0436.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566426, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(255, 1, 135, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0448.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566427, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(256, 1, 136, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0161.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566428, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(257, 1, 137, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0453.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566433, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(258, 1, 138, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0173.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566455, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(259, 1, 139, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0174.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566465, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(260, 1, 140, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0171.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566469, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(261, 1, 141, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0175.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566471, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(262, 1, 142, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0480.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566475, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(263, 1, 143, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'DSC_0472.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566483, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(264, 1, 144, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0189.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566484, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(265, 1, 145, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0177.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566490, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(266, 1, 146, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0211.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566505, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL);
INSERT INTO `default_gallery_media` (`id`, `gallery_id`, `position`, `title`, `subtitle`, `description`, `media`, `extension`, `meta_title`, `meta_description`, `meta_keywords`, `thumbnail`, `thumbnail_small`, `status`, `uploaded_on`, `uploaded_by`, `vuploaded_by`, `vuploaded_by_email`, `external_url`) VALUES
(267, 1, 147, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0214.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566514, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(268, 1, 148, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0216.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566517, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(269, 1, 149, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0221.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566519, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(270, 1, 150, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0228.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566525, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(271, 1, 151, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0234.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566531, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(272, 1, 152, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0245.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566539, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(273, 1, 153, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0252.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566546, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(274, 1, 154, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0236.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566552, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(275, 1, 155, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0269.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566554, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(276, 1, 156, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0302.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566558, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(277, 1, 157, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0303.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566568, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(278, 1, 158, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0292.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566569, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(279, 1, 159, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0308.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566570, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(280, 1, 160, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0313.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566583, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(281, 1, 161, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0324.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566585, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(282, 1, 162, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0346.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566593, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(283, 1, 163, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0320.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566596, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(284, 1, 164, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', NULL, 'IMG_0350.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1435566606, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(285, 1, 165, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0342.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566608, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(286, 1, 166, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0347.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566609, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(287, 1, 167, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0343.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566613, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(288, 1, 168, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0372.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566614, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(289, 1, 169, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0356.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566621, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(290, 1, 170, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0374.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566628, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(291, 1, 171, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0382.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566638, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(292, 1, 172, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0426.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566646, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(293, 1, 173, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', NULL, 'IMG_0398.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1435566650, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(294, 1, 174, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0435.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566652, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(295, 1, 175, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', NULL, 'IMG_0396.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566655, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', 'https://www.flickr.com/photos/97485135@N06/sets'),
(296, 1, 176, 'Indonesia', 'Celebration in Sanur - Mangrove Planting - Beach Cleanup - Recycling Art Class - Fun Learning Class', '', 'IMG_0436.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1435566656, 'Coral Triangle Center', NULL, 'ebimo@coraltrianglecenter.org', NULL),
(579, 11, 177, 'Philippines', 'Reef Strokes Swim at Hamilo Coast', NULL, '13310330_10153588944452091_9144158618019397256_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1464679056, 'WWF-Philippines', NULL, 'jmisa@wwf.org.ph', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(578, 11, 178, 'Philippines', 'Reef Strokes Swim at Hamilo Coast', NULL, '13335985_10153588944047091_378275058375968825_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1464679054, 'WWF-Philippines', NULL, 'jmisa@wwf.org.ph', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(574, 11, 179, 'Malaysia', 'Seagrass Awareness Campaign', NULL, 'group_foto_IMG_9386.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1464601812, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(573, 11, 180, 'Malaysia', 'Seagrass Awareness Campaign', '', 'glass_bottom_boat_IMG_9129.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1464601806, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', NULL),
(572, 11, 181, 'Malaysia', 'Seagrass Awareness Campaign', '', 'glass_bottom_boat_IMG_9082.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1464601800, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', NULL),
(571, 11, 182, 'Malaysia', 'Seagrass Awareness Campaign', NULL, 'quiz_gift_IMG_9351.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1464601795, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(570, 11, 183, 'Malaysia', 'Seagrass Awareness Campaign', '', 'glass_bottom_boat_IMG_9262.JPG', '.JPG', NULL, NULL, NULL, NULL, '', 'draft', 1464601790, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', NULL),
(535, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_18.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572830, '1', NULL, NULL, NULL),
(534, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_17.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572829, '1', NULL, NULL, NULL),
(532, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_15.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572828, '1', NULL, NULL, NULL),
(519, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_01.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572822, '1', NULL, NULL, NULL),
(520, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_06.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572822, '1', NULL, NULL, NULL),
(521, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_02.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572823, '1', NULL, NULL, NULL),
(522, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_03.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572824, '1', NULL, NULL, NULL),
(523, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_08.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572824, '1', NULL, NULL, NULL),
(524, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_10.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572825, '1', NULL, NULL, NULL),
(525, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_07.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572826, '1', NULL, NULL, NULL),
(526, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_09.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572826, '1', NULL, NULL, NULL),
(527, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_05.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572826, '1', NULL, NULL, NULL),
(528, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_11.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572827, '1', NULL, NULL, NULL),
(529, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_12.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572827, '1', NULL, NULL, NULL),
(530, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_13.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572828, '1', NULL, NULL, NULL),
(533, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_16.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572829, '1', NULL, NULL, NULL),
(531, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_14.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572828, '1', NULL, NULL, NULL),
(518, 9, 999, 'Coral Triangle 2012', '', '', 'ctd_2012_04.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572822, '1', NULL, NULL, NULL),
(569, 11, 184, 'Malaysia', 'Seagrass Awareness Campaign', NULL, 'Dr.John_welcome_IMG_8902_.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1464601786, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(568, 11, 185, 'Malaysia', 'Seagrass Awareness Campaign', '', 'Dr._Barry_present_IMG_8922_.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1464601781, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', NULL),
(567, 11, 186, 'Malaysia', 'Seagrass Awareness Campaign', NULL, 'quiz_gift_IMG_9349.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1464601777, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(563, 12, 999, '', '', NULL, 'Logo_PMB_edit.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1464519151, 'Markus T Lasut', NULL, NULL, 'http://mitrabaharisulut.blogspot.co.id/2013/03/komposisi-pengurus-konsorsium-mitra.html'),
(562, 12, 999, '', '', NULL, 'Logo_sulut.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1464519151, 'Markus T Lasut', NULL, NULL, ''),
(561, 13, 999, '', '', '', 'Poster_Ver-1-a.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464519151, 'Markus T Lasut', NULL, NULL, 'http://pasca.unsrat.ac.id/s2/ipa/?page_id=1263'),
(560, 13, 999, '', '', '', 'Logo-new-JPEG.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464519151, 'Markus T Lasut', NULL, NULL, 'http://pasca.unsrat.ac.id/s2/ipa/?page_id=1263'),
(558, 13, 999, '', '', '', 'WWF_Logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464354694, 'WWF-Philippines', NULL, NULL, ''),
(559, 13, 999, '', '', '', 'Logo_unsrat-warna.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464519151, 'Markus T Lasut', NULL, NULL, 'http://pasca.unsrat.ac.id/s2/ipa/'),
(557, 13, 999, '', '', '', 'CTI.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464068362, 'Michael Yap Tzuen Kiat', NULL, NULL, 'http://www.coraltriangleinitiative.org/'),
(544, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_09.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573021, '1', NULL, NULL, NULL),
(545, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_10.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573021, '1', NULL, NULL, NULL),
(546, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_11.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573021, '1', NULL, NULL, NULL),
(547, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_12.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573021, '1', NULL, NULL, NULL),
(548, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_13.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573022, '1', NULL, NULL, NULL),
(549, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_14.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573022, '1', NULL, NULL, NULL),
(550, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_15.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573023, '1', NULL, NULL, NULL),
(551, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_17.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573023, '1', NULL, NULL, NULL),
(552, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_18.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573024, '1', NULL, NULL, NULL),
(553, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_16.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573024, '1', NULL, NULL, NULL),
(566, 11, 187, 'Malaysia', 'Seagrass Awareness Campaign', NULL, 'glass_bottom_boat_IMG_9065.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1464601772, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(556, 13, 999, '', '', '', 'LOGO_UMS_hitam.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1464068362, 'Michael Yap Tzuen Kiat', NULL, NULL, 'http://www.ums.edu.my/ipmbv2/index.php/en'),
(536, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_02.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573018, '1', NULL, NULL, NULL),
(537, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_01.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463573018, '1', NULL, NULL, NULL),
(538, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_04.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573018, '1', NULL, NULL, NULL),
(539, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_03.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573019, '1', NULL, NULL, NULL),
(540, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_05.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573019, '1', NULL, NULL, NULL),
(541, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_06.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573019, '1', NULL, NULL, NULL),
(542, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_07.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463573020, '1', NULL, NULL, NULL),
(543, 8, 999, 'Coral Triangle 2013', '', '', 'ctd_2013_08.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463573020, '1', NULL, NULL, NULL),
(517, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_15.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572719, '1', NULL, NULL, NULL),
(515, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_13.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572718, '1', NULL, NULL, NULL),
(514, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_12.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572718, '1', NULL, NULL, NULL),
(511, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_09.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572717, '1', NULL, NULL, NULL),
(512, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_10.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572717, '1', NULL, NULL, NULL),
(509, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_07.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572716, '1', NULL, NULL, NULL),
(510, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_08.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572716, '1', NULL, NULL, NULL),
(507, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_05.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572716, '1', NULL, NULL, NULL),
(504, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_03.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572714, '1', NULL, NULL, NULL),
(505, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_2223.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572715, '1', NULL, NULL, NULL),
(577, 11, 188, 'Philippines', 'Reef Strokes Swim at Hamilo Coast', '', '13332845_10154178614434618_2418568456210662448_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464679054, 'WWF-Philippines', NULL, 'jmisa@wwf.org.ph', NULL),
(576, 11, 189, 'Philippines', 'Reef Strokes Swim at Hamilo Coast', NULL, '13263838_10154178403664618_2984251434585597277_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1464679052, 'WWF-Philippines', NULL, 'jmisa@wwf.org.ph', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(575, 11, 190, 'Malaysia', 'Seagrass Awareness Campaign', NULL, 'group_foto_IMG_9384.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1464601819, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', ''),
(516, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_14.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572719, '1', NULL, NULL, NULL),
(513, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_11.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572717, '1', NULL, NULL, NULL),
(508, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_06.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572716, '1', NULL, NULL, NULL),
(506, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_04.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572715, '1', NULL, NULL, NULL),
(503, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_02.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572714, '1', NULL, NULL, NULL),
(352, 7, 999, '', '', '', 'a54aa964d36e866a84d4f97bf1c96f55_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532511, '1', NULL, NULL, NULL),
(353, 7, 999, '', '', '', 'af935afe4627a95f77ac13d82d7852b2_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532512, '1', NULL, NULL, NULL),
(354, 7, 999, '', '', '', '434cfd24a98d3675e6f4cd40a53f05d8_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532512, '1', NULL, NULL, NULL),
(355, 7, 999, '', '', '', '537a8330f235f01fef1886f884f77f29_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532513, '1', NULL, NULL, NULL),
(356, 7, 999, '', '', '', 'd0a7fd54bd13b110f347a6ff2002b483_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532513, '1', NULL, NULL, NULL),
(357, 7, 999, '', '', '', 'ffbe05e38a03f5f686fe33654f575f99_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532514, '1', NULL, NULL, NULL),
(358, 7, 999, '', '', '', '5a542f9169de4fa70cd7e342bcfe5737_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532514, '1', NULL, NULL, NULL),
(359, 7, 999, '', '', '', 'cdc950525538d4a5ca8304de46ed691f_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532515, '1', NULL, NULL, NULL),
(360, 7, 999, '', '', '', 'd2b7f3c0575dd2f79955f8ee084bf85a_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532515, '1', NULL, NULL, NULL),
(361, 7, 999, '', '', '', 'faf6cc28a7842eb45cfa955bb491a220_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532516, '1', NULL, NULL, NULL),
(362, 7, 999, '', '', '', '855e8f619ece21dc2f47d63f5779a199_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532516, '1', NULL, NULL, NULL),
(363, 7, 999, '', '', '', '8ddb5c6f5bfddee169ed5b658a291404_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532517, '1', NULL, NULL, NULL),
(364, 7, 999, '', '', '', '490408b42068141234fa1215edb30530_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532517, '1', NULL, NULL, NULL),
(365, 7, 999, '', '', '', '7445dbcae5bb26e130c30acba725d819_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532518, '1', NULL, NULL, NULL),
(366, 7, 999, '', '', '', 'ccd136b21d911377f81c56b0015186eb_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532518, '1', NULL, NULL, NULL),
(367, 7, 999, '', '', '', '09d3edb08d0f208ca7b0b23e2f6a18a5_thumb.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532519, '1', NULL, NULL, NULL),
(368, 7, 999, '', '', '', 'b6b69d00356e10134df32cfeeffd2478_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532519, '1', NULL, NULL, NULL),
(369, 7, 999, '', '', '', 'b62507b7696b913ccc5e07ba79c15518_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532520, '1', NULL, NULL, NULL),
(370, 7, 999, '', '', '', '0e26a40a802338f214beb24d897e1030_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532520, '1', NULL, NULL, NULL),
(371, 7, 999, '', '', '', '75bcc4c8e304a0ae83f4967c70b9dcae_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532521, '1', NULL, NULL, NULL),
(372, 7, 999, '', '', '', '29f6b835519a5504c7469165336c2223_thumb.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532521, '1', NULL, NULL, NULL),
(373, 7, 999, '', '', '', '42496744846bf49f1e3b58a8f2aea01d_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532522, '1', NULL, NULL, NULL),
(374, 7, 999, '', '', '', '2b8df9e3abd94275aa3211b519548087_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532522, '1', NULL, NULL, NULL),
(375, 7, 999, '', '', '', '970489f4d6aacd186ddfca308a9c22c5_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532523, '1', NULL, NULL, NULL),
(376, 7, 999, '', '', '', '9a7c8811fc92fef964452bca535fe55b_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532523, '1', NULL, NULL, NULL),
(377, 7, 999, '', '', '', '884ca4f1d517c72b12af232e285fc2cc_thumb.PNG', '.PNG', NULL, NULL, NULL, NULL, NULL, 'live', 1436532524, '1', NULL, NULL, NULL),
(378, 7, 999, '', '', '', '43bc586360998250df0fde8b606cf8d4_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532524, '1', NULL, NULL, NULL),
(379, 7, 999, '', '', '', 'f868ad098bea49fc3b01729f1723419c_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532525, '1', NULL, NULL, NULL),
(380, 7, 999, '', '', '', '5c9638a958de31c1edb95e13bc987285_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532525, '1', NULL, NULL, NULL),
(381, 7, 999, '', '', '', '267b86df55893067682ffb466cbc7571_thumb.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532526, '1', NULL, NULL, NULL),
(382, 7, 999, '', '', '', 'd00cc0e16f216d87c8902b3f86b228cb_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532526, '1', NULL, NULL, NULL),
(383, 7, 999, '', '', '', '13c660a8a7c2336b3edaa0961eb4c2dc_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532527, '1', NULL, NULL, NULL),
(384, 7, 999, '', '', '', '3ad77175f7bfb59b1c0865f6a23309ca_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532528, '1', NULL, NULL, NULL),
(385, 7, 999, '', '', '', 'cf3fde3613b70cd790ad9234f2e89eeb_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532528, '1', NULL, NULL, NULL),
(386, 7, 999, '', '', '', '9a8978e6d7c091be93652415d0a98924_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532529, '1', NULL, NULL, NULL),
(387, 7, 999, '', '', '', '1d2f7a8c6ce1f5ed24eeece39da3355b_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532529, '1', NULL, NULL, NULL),
(388, 7, 999, '', '', '', '753346b4cf70c241c402161808a8b2a8_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532530, '1', NULL, NULL, NULL),
(389, 7, 999, '', '', '', 'd44f438d35c308127e35ae7fc39da350_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532531, '1', NULL, NULL, NULL),
(390, 7, 999, '', '', '', 'cfd9944b9dc801b7a7f4b95534c09fde_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532531, '1', NULL, NULL, NULL),
(391, 7, 999, '', '', '', '4af811b384d770b5079ac6cd716892dc_thumb.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532531, '1', NULL, NULL, NULL),
(392, 7, 999, '', '', '', 'd3f73f7eec4d5f13f22619b3ae7597c7_thumb.GIF', '.GIF', NULL, NULL, NULL, NULL, NULL, 'live', 1436532532, '1', NULL, NULL, NULL),
(393, 7, 999, '', '', '', '173316c24aaff571ef7a6dc1eb77eefc_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532532, '1', NULL, NULL, NULL),
(394, 7, 999, '', '', '', '56e89803d294cf695af3f1bc95376e1f_thumb.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532533, '1', NULL, NULL, NULL),
(395, 7, 999, '', '', '', '4a54bcdbeb16c4a784d44826c5cb7e8e_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532533, '1', NULL, NULL, NULL),
(396, 7, 999, '', '', '', 'ff68ec34d8e494b20376c27c6edf9087_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532534, '1', NULL, NULL, NULL),
(397, 7, 999, '', '', '', '5d8ef0d2963decbb9cb1b3fb532625c0_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532534, '1', NULL, NULL, NULL),
(398, 7, 999, '', '', '', '4b99b9840607ba2da48f104e11191547_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532535, '1', NULL, NULL, NULL),
(399, 7, 999, '', '', '', '95495f62862aa0f2c97693ee10cdb1d3_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532535, '1', NULL, NULL, NULL),
(400, 7, 999, '', '', '', '3f340793b48000eef4b4a8ebbb9fff05_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532536, '1', NULL, NULL, NULL),
(401, 7, 999, '', '', '', '530f4e1e1b0c6983b6f1c3b5fd5f8f2f_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532536, '1', NULL, NULL, NULL),
(402, 7, 999, '', '', '', 'd297463151c600220636aa4931b5a10b_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532537, '1', NULL, NULL, NULL),
(403, 7, 999, '', '', '', '705aed6b405ad6d8d8c29e42b7b76123_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532537, '1', NULL, NULL, NULL),
(404, 7, 999, '', '', '', '2e747f54ba73b1d6f72c8cbd0fe98a1a_thumb.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1436532537, '1', NULL, NULL, NULL),
(405, 7, 999, '', '', '', '359c14838ec1906b582e90182c3fa215_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532538, '1', NULL, NULL, NULL),
(406, 7, 999, '', '', '', '476581097dcaf5793535671dc8447b03_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532538, '1', NULL, NULL, NULL),
(407, 7, 999, '', '', '', 'af75e1903e158477732490e8b76ca143_thumb.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532539, '1', NULL, NULL, NULL),
(408, 7, 999, '', '', '', 'a326ddcbf89e314a4b334758e1547b76_thumb.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532539, '1', NULL, NULL, NULL),
(409, 7, 999, '', '', '', 'f7b0738e2317b2ce3c3c235847300e80_thumb.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532539, '1', NULL, NULL, NULL),
(410, 7, 999, '', '', '', '8ac7e9fe35ecfd81f706f1ac12c58478_thumb.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1436532540, '1', NULL, NULL, NULL),
(411, 7, 999, '', '', '', 'bfef7ea2aa2edd01481f5edad319e082_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532540, '1', NULL, NULL, NULL),
(412, 7, 999, '', '', '', '6e53cfddf03d843c8c2e1e68038b367b_thumb.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532541, '1', NULL, NULL, NULL),
(413, 10, 999, '', '', '', 'DENR.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532870, '1', NULL, NULL, NULL),
(414, 10, 999, '', '', '', 'Municipality_of_San_Francisco.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532870, '1', NULL, NULL, NULL),
(415, 10, 999, '', '', '', 'Mun_Ubay.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532871, '1', NULL, NULL, NULL),
(416, 10, 999, '', '', '', 'Mun_Lubang.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532872, '1', NULL, NULL, NULL),
(417, 10, 999, '', '', '', 'Mun_Cortes.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532873, '1', NULL, NULL, NULL),
(418, 10, 999, '', '', '', 'TNC_Logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532873, '1', NULL, NULL, NULL),
(419, 10, 999, '', '', '', 'CTSP_new_CI_logo_cropped.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532874, '1', NULL, NULL, NULL),
(420, 10, 999, '', '', '', 'WorldFish-Logo-35mm.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532876, '1', NULL, NULL, NULL),
(421, 10, 999, '', '', '', 'Western_Province_Govt.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532877, '1', NULL, NULL, NULL),
(422, 10, 999, '', '', '', 'SIG_Logo.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532878, '1', NULL, NULL, NULL),
(423, 10, 999, '', '', '', 'Hapi_Fish_Logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532878, '1', NULL, NULL, NULL),
(424, 10, 999, '', '', '', 'Short-GEF_logo_colored_NOTAG_transparent.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532879, '1', NULL, NULL, NULL),
(425, 10, 999, '', '', '', 'taytay.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532880, '1', NULL, NULL, NULL),
(426, 10, 999, '', '', '', 'Short-GEF_logo_colored_NOTAG_transparent_resize.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532880, '1', NULL, NULL, NULL),
(427, 10, 999, '', '', '', 'PIA.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532881, '1', NULL, NULL, NULL),
(428, 10, 999, '', '', '', 'Plantation_Bay.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532881, '1', NULL, NULL, NULL),
(429, 10, 999, '', '', '', 'Shangrila_Mactan.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532882, '1', NULL, NULL, NULL),
(430, 10, 999, '', '', '', 'Arbees_Bakeshop.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532882, '1', NULL, NULL, NULL),
(431, 10, 999, '', '', '', 'Philippine_National_Police.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532883, '1', NULL, NULL, NULL),
(432, 10, 999, '', '', '', '5-22-2013_9-52-28_PM.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532883, '1', NULL, NULL, NULL),
(433, 10, 999, '', '', '', '5-22-2013_9-52-52_PM.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532884, '1', NULL, NULL, NULL),
(434, 10, 999, '', '', '', '5-22-2013_9-54-00_PM.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532885, '1', NULL, NULL, NULL),
(435, 10, 999, '', '', '', 'Crimson_Logo_resize.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532885, '1', NULL, NULL, NULL),
(436, 10, 999, '', '', '', '5-22-2013_10-13-38_PM.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532886, '1', NULL, NULL, NULL),
(437, 10, 999, '', '', '', '5-22-2013_9-54-13_PM.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532886, '1', NULL, NULL, NULL),
(438, 10, 999, '', '', '', '5-22-2013_9-53-16_PM_resize.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532887, '1', NULL, NULL, NULL),
(439, 10, 999, '', '', '', 'CCEF_Logo_resize.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532888, '1', NULL, NULL, NULL),
(440, 10, 999, '', '', '', 'DENR1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532888, '1', NULL, NULL, NULL),
(441, 10, 999, '', '', '', 'CTSP_new_CI_logo_cropped_resize.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532889, '1', NULL, NULL, NULL),
(442, 10, 999, '', '', '', 'PNG.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532890, '1', NULL, NULL, NULL),
(443, 10, 999, '', '', '', 'Tiger_Club.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532890, '1', NULL, NULL, NULL),
(444, 10, 999, '', '', '', 'NTB.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532891, '1', NULL, NULL, NULL),
(445, 10, 999, '', '', '', 'udayana.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532891, '1', NULL, NULL, NULL),
(446, 10, 999, '', '', '', 'USAID.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532892, '1', NULL, NULL, NULL),
(447, 10, 999, '', '', '', 'Mataram.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532893, '1', NULL, NULL, NULL),
(448, 10, 999, '', '', '', 'Logos.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532894, '1', NULL, NULL, NULL),
(449, 10, 999, '', '', '', 'Logos_(2).jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532894, '1', NULL, NULL, NULL),
(450, 10, 999, '', '', '', 'WWF.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532895, '1', NULL, NULL, NULL),
(451, 10, 999, '', '', '', 'surfer_girl.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532895, '1', NULL, NULL, NULL),
(452, 10, 999, '', '', '', 'new_logo_wd_300px.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532896, '1', NULL, NULL, NULL),
(453, 10, 999, '', '', '', 'Lambang_Kabupaten_Buleleng.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532896, '1', NULL, NULL, NULL),
(454, 10, 999, '', '', '', 'APRU-logo.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532897, '1', NULL, NULL, NULL),
(455, 10, 999, '', '', '', 'Universitas_Indonesia.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532898, '1', NULL, NULL, NULL),
(456, 10, 999, '', '', '', 'Rare_English_Blue.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532898, '1', NULL, NULL, NULL),
(457, 10, 999, '', '', '', 'logo_sm1.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532899, '1', NULL, NULL, NULL),
(458, 10, 999, '', '', '', 'Gigaquit_Logo.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1436532900, '1', NULL, NULL, NULL),
(459, 10, 999, '', '', '', 'Indo_Climate_Council.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532900, '1', NULL, NULL, NULL),
(460, 10, 999, '', '', '', 'Logo_diponegoro_university.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532901, '1', NULL, NULL, NULL),
(461, 10, 999, '', '', '', 'Greenfins.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532901, '1', NULL, NULL, NULL),
(462, 10, 999, '', '', '', 'uneplogobig_resize.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532902, '1', NULL, NULL, NULL),
(463, 10, 999, '', '', '', '487397_10152167781115461_809493312_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532903, '1', NULL, NULL, NULL),
(464, 10, 999, '', '', '', '556549_403490859660946_1362198705_n_resize.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532904, '1', NULL, NULL, NULL),
(465, 10, 999, '', '', '', 'be_logo.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1436532905, '1', NULL, NULL, NULL),
(466, 10, 999, '', '', '', '562279_107238082742079_407076928_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532905, '1', NULL, NULL, NULL),
(467, 10, 999, '', '', '', 'Traders_New_Primary_Signature.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1436532907, '1', NULL, NULL, NULL),
(468, 10, 999, '', '', '', 'Malampaya_Foundation.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532907, '1', NULL, NULL, NULL),
(469, 10, 999, '', '', '', '389304_537456742979772_262220233_n.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532908, '1', NULL, NULL, NULL),
(470, 10, 999, '', '', '', 'LMP.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532908, '1', NULL, NULL, NULL),
(471, 10, 999, '', '', '', 'calatagan.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1436532909, '1', NULL, NULL, NULL),
(502, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_8846.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572713, '1', NULL, NULL, NULL),
(501, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_01.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1463572713, '1', NULL, NULL, NULL),
(500, 6, 999, 'Coral Triangle 2014', '', '', 'ctd_2014_02_2222.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1463572713, '1', NULL, NULL, NULL),
(564, 11, 191, 'Malaysia', 'Seagrass Awareness Campaign', '', 'glass_bottom_boat_IMG_9058.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1464601758, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', NULL),
(565, 11, 192, 'Malaysia', 'Seagrass Awareness Campaign', NULL, 'glass_bottom_boat_IMG_9047.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1464601767, 'Borneo Marine Research Institure', NULL, 'mic_yap05@hotmail.com', 'https://www.flickr.com/photos/97485135@N06/albums/72157669477673925'),
(600, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'IMG_2231.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1465482948, 'Manengkel Solidaritas', NULL, 'manengkelsolidaritas@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(479, 11, 5, 'Coral Triangle Day 2013', '', NULL, '2013_3.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571485, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157644389344818/'),
(480, 11, 4, 'Coral Triangle Day 2013', '', NULL, '2013_2.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571485, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157644389344818/'),
(484, 11, 6, 'Coral Triangle Day 2013', '', NULL, '2013_1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571487, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157644389344818/'),
(485, 11, 7, 'Coral Triangle Day 2014', '', NULL, '2014_1.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571487, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157644791085462/'),
(486, 11, 8, 'Coral Triangle Day 2014', '', NULL, '2014_3.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571488, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157644791085462/'),
(487, 11, 9, 'Coral Triangle Day 2014', '', NULL, '2014_2.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571488, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157644791085462/'),
(488, 11, 10, 'Coral Triangle Day 2014', '', NULL, '2014_4.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571488, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157644791085462/'),
(489, 11, 11, 'Coral Triangle Day 2015', '', NULL, '2015_1.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571489, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(490, 11, 12, 'Coral Triangle Day 2015', '', NULL, '2015_2.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571489, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(491, 11, 13, 'Coral Triangle Day 2015', '', NULL, '2015_3.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571490, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(492, 11, 14, 'Coral Triangle Day 2015', '', NULL, '2015_4.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571490, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(493, 11, 15, 'Coral Triangle Day 2015', '', NULL, '2015_5.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571490, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(494, 11, 16, 'Coral Triangle Day 2015', '', NULL, '2015_6.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571491, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(495, 11, 17, 'Coral Triangle Day 2015', '', NULL, '2015_7.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571491, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(496, 11, 18, 'Coral Triangle Day 2015', '', NULL, '2015_8.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1463571491, '1', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/collections/72157652113638879/'),
(580, 13, 999, '', '', '', 'WWF_for_a_living_planet_logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464775537, 'Rachel Wang', NULL, NULL, 'https://www.facebook.com/wwfsolomonislands/'),
(581, 13, 999, '', '', '', '11080400_10206113534814206_4670634809579922647_o.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464885859, 'Kevin Austin', NULL, NULL, ''),
(582, 12, 999, '', '', '', 'TADF_Logo_master_3_email.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1464885860, 'Kevin Austin', NULL, NULL, ''),
(583, 13, 999, '', '', '', 'CTC_new_logo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464960540, 'Leilani', NULL, NULL, ''),
(584, 13, 999, '', '', '', 'CTC_new_logo1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1464960984, 'Leilani', NULL, NULL, ''),
(585, 12, 999, 'Our Telekom', '', '', 'Our_Telekom_logo.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1465020252, '1', NULL, NULL, NULL),
(586, 12, 999, '', '', '', '2016-06-06_07.31_.51_.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465184384, 'Sian Williams', NULL, NULL, 'Www.Gili backpackers.com'),
(587, 13, 999, '', '', '', 'Logo_Manengkel.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465203760, 'Gustaf Mamangkey', NULL, NULL, 'Manengkel Solidaritas'),
(588, 13, 999, '', '', '', 'Logo_KMB.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465203760, 'Gustaf Mamangkey', NULL, NULL, 'Marine Partnership Consortium of North Sulawesi'),
(589, 13, 999, '', '', '', 'Logo_AJI.jpeg', '.jpeg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465203760, 'Gustaf Mamangkey', NULL, NULL, 'The Alliance of Independent Journalists '),
(590, 12, 999, '', '', '', 'Logo_CTDay.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465203760, 'Gustaf Mamangkey', NULL, NULL, 'CT Day'),
(591, 12, 999, '', '', '', 'Logo_Aqua.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465203761, 'Gustaf Mamangkey', NULL, NULL, 'AQUA'),
(592, 12, 999, '', '', '', 'Logo_Save_Bunaken.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465203761, 'Gustaf Mamangkey', NULL, NULL, 'Save Bunaken'),
(593, 12, 999, '', '', '', 'Logo_BTNB.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465203762, 'Gustaf Mamangkey', NULL, NULL, 'Bunaken National Park Authority'),
(594, 12, 999, '', '', '', 'Logo_LPAMU.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465203762, 'Gustaf Mamangkey', NULL, NULL, 'Lintas Pencinta Alam Manado Utara'),
(595, 12, 999, '', '', '', 'Logo_Kota_Manado_(1).jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465203762, 'Gustaf Mamangkey', NULL, NULL, 'City of Manado'),
(599, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'IMG_2297.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1465482941, 'Manengkel Solidaritas', NULL, 'manengkelsolidaritas@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(598, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'IMG_2225.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1465482938, 'Manengkel Solidaritas', NULL, 'manengkelsolidaritas@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(601, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'IMG_2327.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1465482955, 'Manengkel Solidaritas', NULL, 'manengkelsolidaritas@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(602, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'IMG_2288.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1465482958, 'Manengkel Solidaritas', NULL, 'manengkelsolidaritas@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(603, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'IMG_2277.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'live', 1465482963, 'Manengkel Solidaritas', NULL, 'manengkelsolidaritas@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(604, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'Foto_Dagho1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465560104, 'Politeknik Kelautan Perikanan Bitung', NULL, 'andiewibi@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(605, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'Foto_Dagho2.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465560105, 'Politeknik Kelautan Perikanan Bitung', NULL, 'andiewibi@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(606, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'Foto_Dagho3.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465560105, 'Politeknik Kelautan Perikanan Bitung', NULL, 'andiewibi@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(607, 11, 999, 'Indonesia', 'Mangrove Planting Event for CTI Day', NULL, 'Foto_Dagho_4.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465560105, 'Politeknik Kelautan Perikanan Bitung', NULL, 'andiewibi@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(608, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_171650.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710326, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(609, 11, 999, 'Indonesia', 'Debris Free Friday', NULL, '20160610_173616.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465710328, 'SeaMade', NULL, 'seamadeproducts@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(610, 11, 999, 'Indonesia', 'Debris Free Friday', NULL, '20160610_172109.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465710339, 'SeaMade', NULL, 'seamadeproducts@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(611, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_172105.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710345, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(612, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_172118.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710364, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(613, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_171436.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710384, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(614, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_174117.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710502, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(615, 11, 999, 'Indonesia', 'Debris Free Friday', NULL, '20160610_173803.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465710523, 'SeaMade', NULL, 'seamadeproducts@gmail.com', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(616, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_174758.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710542, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(617, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_174923.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710544, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL);
INSERT INTO `default_gallery_media` (`id`, `gallery_id`, `position`, `title`, `subtitle`, `description`, `media`, `extension`, `meta_title`, `meta_description`, `meta_keywords`, `thumbnail`, `thumbnail_small`, `status`, `uploaded_on`, `uploaded_by`, `vuploaded_by`, `vuploaded_by_email`, `external_url`) VALUES
(618, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_172818.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710545, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(619, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_175156.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710607, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(620, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_182259.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710706, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(621, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_175201.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710712, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(622, 11, 999, 'Indonesia', 'Debris Free Friday', '', '20160610_182100.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465710751, 'SeaMade', NULL, 'seamadeproducts@gmail.com', NULL),
(623, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '7.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465876842, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(624, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '4.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465876940, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(625, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '6.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465876973, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(626, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '9.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877000, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(627, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '2.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877003, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(628, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877042, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(629, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '3.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877050, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(630, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '11.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877059, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(631, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '8.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877062, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(632, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '14.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877090, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(633, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '13.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877094, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(634, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '17.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877102, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(635, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '19.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877126, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(636, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '12.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877132, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(637, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', NULL, '18.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465877138, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925/'),
(638, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '15.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877141, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(639, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '20.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877151, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(640, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '21.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877156, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(641, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '23.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877190, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(642, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '25.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877193, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(643, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', NULL, '26.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465877209, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925/'),
(644, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '22.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877211, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(645, 11, 999, 'Solomon Islands', 'Gizo Town Clean-Up, Plastic Bag-Free Day & Talent Competition 2016', '', '27.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465877214, 'WWF Solomon Islands', NULL, 'rwang@wwfpacific.org', NULL),
(646, 11, 999, 'Indonesia', 'Indonesia Marine Tourism Technical Asisstance for Manager', '', 'IMG-20160613-WA0013.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465907680, 'National Committee Secretariat of CTI-CFF Indonesia', NULL, 'ncc.indonesia@cticff.org', NULL),
(647, 11, 999, 'Indonesia', 'Indonesia Marine Tourism Technical Asisstance for Manager', '', 'IMG-20160613-WA0012.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465907681, 'National Committee Secretariat of CTI-CFF Indonesia', NULL, 'ncc.indonesia@cticff.org', NULL),
(648, 11, 999, 'Indonesia', 'Indonesia Marine Tourism Technical Asisstance for Manager', '', 'IMG-20160613-WA0011.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465907681, 'National Committee Secretariat of CTI-CFF Indonesia', NULL, 'ncc.indonesia@cticff.org', NULL),
(649, 13, 999, '', '', '', 'CTC_new_logo2.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1465998339, 'Leilani', NULL, NULL, ''),
(650, 12, 999, '', '', '', '11LogosMaya_03.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465998340, 'Leilani', NULL, NULL, ''),
(651, 12, 999, '', '', '', 'Fairmont-Sanur-Logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998340, 'Leilani', NULL, NULL, ''),
(652, 12, 999, '', '', '', '1407051439.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998341, 'Leilani', NULL, NULL, ''),
(653, 12, 999, '', '', '', 'Screen_Shot_2016-06-15_at_5.37_.26_PM_.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998341, 'Leilani', NULL, NULL, ''),
(654, 12, 999, '', '', '', 'Screen_Shot_2016-06-15_at_5.37_.26_PM_1.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1465998341, 'Leilani', NULL, NULL, ''),
(655, 12, 999, '', '', '', 'PHUKET_mermaidlogo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465998341, 'Leilani', NULL, NULL, ''),
(656, 12, 999, '', '', '', 'logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998342, 'Leilani', NULL, NULL, ''),
(657, 12, 999, '', '', '', 'PRAMA_SANUR_LOGO.PNG', '.PNG', NULL, NULL, NULL, NULL, NULL, 'live', 1465998342, 'Leilani', NULL, NULL, ''),
(658, 12, 999, '', '', '', 'Starbucks-logo-old.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998342, 'Leilani', NULL, NULL, ''),
(659, 12, 999, '', '', '', 'Crystal-Divers-Feature.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998343, 'Leilani', NULL, NULL, ''),
(660, 12, 999, '', '', '', 'logo-BNI.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998343, 'Leilani', NULL, NULL, ''),
(661, 12, 999, '', '', '', 'Manulife-logo-wordmark.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1465998344, 'Leilani', NULL, NULL, ''),
(662, 12, 999, '', '', '', 'axle.asia_.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1465998344, 'Leilani', NULL, NULL, ''),
(663, 13, 999, '', '', '', 'LOGO_UMS_new.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1466064603, 'Coral Triangle Initiative Sabah Branch (CTI-SAB)', NULL, NULL, ''),
(664, 13, 999, '', '', '', 'CTI_logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1466064603, 'Coral Triangle Initiative Sabah Branch (CTI-SAB)', NULL, NULL, ''),
(665, 13, 999, '', '', '', 'MOSTI.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1466065436, 'Coral Triangle Initiative Sabah Branch (CTI-SAB)', NULL, NULL, ''),
(666, 13, 999, '', '', '', 'LOGO_UMS_new1.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1466065436, 'Coral Triangle Initiative Sabah Branch (CTI-SAB)', NULL, NULL, ''),
(667, 13, 999, '', '', '', 'CTI_logo1.png', '.png', NULL, NULL, NULL, NULL, NULL, 'draft', 1466065436, 'Coral Triangle Initiative Sabah Branch (CTI-SAB)', NULL, NULL, ''),
(668, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition2.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071491, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(669, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition4.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071493, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(670, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition_14.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071495, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(671, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition8.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071497, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(672, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition3.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071499, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(673, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition-judging2.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071502, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(674, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition1.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071505, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(675, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition9.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071508, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(676, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition-judging3.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071509, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(677, 11, 999, 'Malaysia', 'Colouring Competition', '', 'Coloring_competition13.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466071512, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(678, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2806.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073368, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(679, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2830.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073370, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(680, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2812.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073372, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(681, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2831.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073375, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(682, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2803.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073377, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(683, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2816.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073379, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(684, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2807.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073383, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(685, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2850.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073385, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(686, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2841.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073387, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(687, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2858.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073389, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(688, 11, 999, 'Malaysia', 'Monitoring of Coral Bleaching due to Climate Change', '', 'IMG_2836.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466073391, 'Coral Triangle Initiative Sabah Branch', NULL, 'ctisabahmy@gmail.com', NULL),
(689, 11, 999, 'Indonesia', 'Aku Cinta Laut: Trash Art Competition', '', 'SAM_1361.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1466672338, 'Coral Triangle Center', NULL, 'lgallardo@coraltrianglecenter.org', NULL),
(690, 11, 999, 'Indonesia', 'Underwater Clean Up Dive and Adopt a Coral Program', '', 'IMG_1490.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466672696, 'Coral Triangle Center', NULL, 'lgallardo@coraltrianglecenter.org', NULL),
(691, 11, 999, 'Indonesia', 'Underwater Clean Up Dive and Adopt a Coral Program', '', 'IMG_1445.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466672698, 'Coral Triangle Center', NULL, 'lgallardo@coraltrianglecenter.org', NULL),
(692, 11, 999, 'Indonesia', 'Underwater Clean Up Dive and Adopt a Coral Program', '', 'IMG_1523.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466672701, 'Coral Triangle Center', NULL, 'lgallardo@coraltrianglecenter.org', NULL),
(693, 11, 999, 'Indonesia', 'Underwater Clean Up Dive and Adopt a Coral Program', '', 'IMG_1509.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466672728, 'Coral Triangle Center', NULL, 'lgallardo@coraltrianglecenter.org', NULL),
(694, 11, 999, 'Indonesia', 'Underwater Clean Up Dive and Adopt a Coral Program', '', 'IMG_1429.JPG', '.JPG', NULL, NULL, NULL, NULL, NULL, 'draft', 1466672734, 'Coral Triangle Center', NULL, 'lgallardo@coraltrianglecenter.org', NULL),
(695, 15, 999, '', '', '', 'axle.asia__.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095145, '2', NULL, NULL, NULL),
(696, 15, 999, '', '', '', '1407051439.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095145, '2', NULL, NULL, NULL),
(697, 15, 999, '', '', '', 'Fairmont-Sanur-Logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095145, '2', NULL, NULL, NULL),
(698, 15, 999, '', '', '', '2016-06-06_07.31__.51__.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095146, '2', NULL, NULL, NULL),
(699, 15, 999, '', '', '', '11LogosMaya_03.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095146, '2', NULL, NULL, NULL),
(700, 15, 999, '', '', '', 'Crystal-Divers-Feature.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095147, '2', NULL, NULL, NULL),
(701, 15, 999, '', '', '', 'Logo_BTNB.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095147, '2', NULL, NULL, NULL),
(702, 15, 999, '', '', '', 'Logo_Aqua.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095147, '2', NULL, NULL, NULL),
(703, 15, 999, '', '', '', 'logo.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095147, '2', NULL, NULL, NULL),
(704, 15, 999, '', '', '', 'Logo_CTDay.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095148, '2', NULL, NULL, NULL),
(705, 15, 999, '', '', '', 'Logo_LPAMU.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095148, '2', NULL, NULL, NULL),
(706, 15, 999, '', '', '', 'Logo_Save_Bunaken.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095149, '2', NULL, NULL, NULL),
(707, 15, 999, '', '', '', 'Logo_sulut.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095149, '2', NULL, NULL, NULL),
(708, 15, 999, '', '', '', 'logo-BNI.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095149, '2', NULL, NULL, NULL),
(709, 15, 999, '', '', '', 'Our_Telekom_logo.gif', '.gif', NULL, NULL, NULL, NULL, NULL, 'live', 1495095150, '2', NULL, NULL, NULL),
(710, 15, 999, '', '', '', 'Manulife-logo-wordmark.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095150, '2', NULL, NULL, NULL),
(711, 15, 999, '', '', '', 'PHUKET_mermaidlogo.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095150, '2', NULL, NULL, NULL),
(712, 15, 999, '', '', '', 'Logo_PMB_edit.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095150, '2', NULL, NULL, NULL),
(713, 15, 999, '', '', '', 'Logo_Kota_Manado_1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095151, '2', NULL, NULL, NULL),
(714, 15, 999, '', '', '', 'PRAMA_SANUR_LOGO.PNG', '.PNG', NULL, NULL, NULL, NULL, NULL, 'live', 1495095151, '2', NULL, NULL, NULL),
(715, 15, 999, '', '', '', 'TADF_Logo_master_3_email.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095152, '2', NULL, NULL, NULL),
(716, 15, 999, '', '', '', 'Screen_Shot_2016-06-15_at_5.37__.26_PM__.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095153, '2', NULL, NULL, NULL),
(717, 15, 999, '', '', '', 'Starbucks-logo-old.png', '.png', NULL, NULL, NULL, NULL, NULL, 'live', 1495095159, '2', NULL, NULL, NULL),
(718, 14, 999, 'Solomon Islands', 'Gizo beach clean up 2012', NULL, 'CTD_2012_-_Gizo_Solomon_Islands.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095311, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/17297725513/in/album-72157653119651406/'),
(719, 14, 999, 'Indonesia', 'Eco-friendly products Coral Triangle Day 2015', NULL, 'Celebration_in_Sanur_-_Recycling_Art_Class.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095311, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/19126550820/in/album-72157654681792195/'),
(720, 14, 999, 'Indonesia', 'Celebration in Ambon 2012', NULL, 'CTD_2012_-_in_Ambon_Indonesia.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095311, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/17730276768/in/album-72157652773383220/'),
(729, 14, 999, 'Malaysia', 'Coral Triangle Day 2014', NULL, 'CTD_2014_in_the_Philippines.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095314, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/14192440658/in/album-72157645124973603/'),
(735, 14, 999, 'Indonesia', 'Debris Free Friday', NULL, 'debris_free_day.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1495095317, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/sets/72157669477673925'),
(736, 11, 999, 'Indonesia', 'Coral Triangle Day in Nusa Penida', '', 'nusa-penida-pusat-peringatan-coral-triangle-day_01.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1497186644, 'catalyze', NULL, 'larasati@catalyzecommunications.com', NULL),
(742, 14, 999, 'Solomon Islands', 'Cans cans and more cans', NULL, 'cans_cans_and_more_cans.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350715, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(740, 14, 999, 'Solomon Islands', 'Children doing their part', NULL, 'Children_doing_their_part.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350714, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(741, 14, 999, 'Solomon Islands', 'Children of Munda', NULL, 'Children_of_Munda.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350714, '2', NULL, NULL, ''),
(743, 14, 999, 'Solomon Islands', 'Diving for cans', NULL, 'Diving_for_cans.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350717, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(744, 14, 999, 'Solomon Islands', 'Diving for rubbish', NULL, 'Diving_for_rubbish.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350718, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(745, 14, 999, 'Solomon Islands', 'Community effort', NULL, 'Community_effort_Munda.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350718, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(746, 14, 999, 'Solomon Islands', 'Final haul', NULL, 'Final_haul.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350718, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(747, 14, 999, 'Solomon Islands', 'No place for rubbish', NULL, 'No_place_for_rubbish.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350721, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(750, 14, 999, 'Solomon Islands', 'The whole family wants to help clean up', NULL, 'The_whole_family_wants_to_help_clean_up1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350725, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(751, 14, 999, 'Solomon Islands', 'Beach clean up', NULL, 'Beach_clean_up.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350725, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(752, 14, 999, 'Solomon Islands', 'It''s hard work cleaning the beach', NULL, 'Its_hard_work_cleaning_the_beach.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350725, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(754, 14, 999, 'Solomon Islands', 'Can hill', NULL, 'can_hill.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350726, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(755, 14, 999, 'Solomon Islands', 'Worth the effort', NULL, 'Worth_the_effort1.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'live', 1497350754, '2', NULL, NULL, 'https://www.flickr.com/photos/97485135@N06/albums/72157682089012253'),
(756, 14, 999, 'undefined', 'Movie Screening and talks', '', 'gg-5444.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683032, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(757, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5809.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683583, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(758, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5818.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683583, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(759, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5776.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683584, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(760, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5764.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683585, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(761, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5762.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683588, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(762, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5745.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683588, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(763, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5734.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683589, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(764, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5725.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683590, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(765, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5714.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683810, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(766, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5719.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683811, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL),
(767, 14, 999, 'Indonesia', 'Movie Screening and talks', '', 'gg-5545.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, 'draft', 1499683816, 'RS CTI-CFF', NULL, 'regional.secretariat@cticff.org', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_groups`
--

CREATE TABLE IF NOT EXISTS `default_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_groups`
--

INSERT INTO `default_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'user', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `default_instagram_posts`
--

CREATE TABLE IF NOT EXISTS `default_instagram_posts` (
  `id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL DEFAULT '0',
  `instagram_object_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `instagram_time` int(11) NOT NULL,
  `hashtag` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `images` text COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `finalist` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_instagram_posts`
--

INSERT INTO `default_instagram_posts` (`id`, `subscription_id`, `instagram_object_id`, `instagram_time`, `hashtag`, `updated_on`, `link`, `likes`, `images`, `user`, `username`, `profile_picture`, `user_id`, `caption`, `location`, `finalist`) VALUES
(1, 0, '1252525063040272013_30449736', 1463532644, 'noplace4plastic', 1463570569, 'https://www.instagram.com/p/BFh3OieygaN/', 21, 'O:8:"stdClass":3:{s:14:"low_resolution";O:8:"stdClass":3:{s:3:"url";s:149:"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/13260940_185169881878956_469274989_n.jpg?ig_cache_key=MTI1MjUyNTA2MzA0MDI3MjAxMw%3D%3D.2.l";s:5:"width";i:320;s:6:"height";i:320;}s:9:"thumbnail";O:8:"stdClass":3:{s:3:"url";s:164:"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c150.0.600.600/13266782_463946753729931_449659510_n.jpg?ig_cache_key=MTI1MjUyNTA2MzA0MDI3MjAxMw%3D%3D.2.c";s:5:"width";i:150;s:6:"height";i:150;}s:19:"standard_resolution";O:8:"stdClass":3:{s:3:"url";s:156:"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/13260940_185169881878956_469274989_n.jpg?ig_cache_key=MTI1MjUyNTA2MzA0MDI3MjAxMw%3D%3D.2.l";s:5:"width";i:640;s:6:"height";i:640;}}', 'juanjo carreon diez', 'juanjobanana', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/12960164_1685797341707739_70528833_a.jpg', '30449736', 'Snail & crab in a jar, nature adaptation #ocean #noplace4plastic #marinelitter', 'O:8:"stdClass":4:{s:8:"latitude";d:20.3383443966580017558953841216862201690673828125;s:4:"name";s:16:"Xcacel-Xcacelito";s:9:"longitude";d:-87.348280977466998820091248489916324615478515625;s:2:"id";i:5931168;}', NULL),
(2, 0, '1252200938324514089_3230168988', 1463494005, 'noplace4plastic', 1463570569, 'https://www.instagram.com/p/BFgth50PlUp/', 0, 'O:8:"stdClass":3:{s:14:"low_resolution";O:8:"stdClass":3:{s:3:"url";s:149:"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/13109017_578725485642029_732087593_n.jpg?ig_cache_key=MTI1MjIwMDkzODMyNDUxNDA4OQ%3D%3D.2.l";s:5:"width";i:320;s:6:"height";i:320;}s:9:"thumbnail";O:8:"stdClass":3:{s:3:"url";s:164:"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c98.0.863.863/13257058_1046208005458386_913348188_n.jpg?ig_cache_key=MTI1MjIwMDkzODMyNDUxNDA4OQ%3D%3D.2.c";s:5:"width";i:150;s:6:"height";i:150;}s:19:"standard_resolution";O:8:"stdClass":3:{s:3:"url";s:156:"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/13109017_578725485642029_732087593_n.jpg?ig_cache_key=MTI1MjIwMDkzODMyNDUxNDA4OQ%3D%3D.2.l";s:5:"width";i:640;s:6:"height";i:640;}}', 'Kreta Kei', 'k123ta', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/13166917_237850989905293_110510200_a.jpg', '3230168988', 'Sad...but this is the ugly truth. \n@Regrann from @conservationid -  Posisi pertama ditempati oleh China, lalu ketiga oleh FIlipina.\n\nMenurut kamu apa hal yang harus kita lakukan? #ciindonesia #noplace4plastic #Regrann', 'O:8:"stdClass":4:{s:8:"latitude";d:1;s:4:"name";s:18:"Jakarta, Indonesia";s:9:"longitude";d:0;s:2:"id";i:214427142;}', NULL),
(3, 0, '1252133004756577491_697370954', 1463485907, 'noplace4plastic', 1463570569, 'https://www.instagram.com/p/BFgeFVwDuzT/', 5, 'O:8:"stdClass":3:{s:14:"low_resolution";O:8:"stdClass":3:{s:3:"url";s:147:"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/13248879_246387179052278_355562517_n.jpg?ig_cache_key=MTI1MjEzMzAwNDc1NjU3NzQ5MQ%3D%3D.2";s:5:"width";i:320;s:6:"height";i:320;}s:9:"thumbnail";O:8:"stdClass":3:{s:3:"url";s:147:"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/13248879_246387179052278_355562517_n.jpg?ig_cache_key=MTI1MjEzMzAwNDc1NjU3NzQ5MQ%3D%3D.2";s:5:"width";i:150;s:6:"height";i:150;}s:19:"standard_resolution";O:8:"stdClass":3:{s:3:"url";s:138:"https://scontent.cdninstagram.com/t51.2885-15/e35/13248879_246387179052278_355562517_n.jpg?ig_cache_key=MTI1MjEzMzAwNDc1NjU3NzQ5MQ%3D%3D.2";s:5:"width";i:640;s:6:"height";i:640;}}', 'kirdneh e.k', 'falestinest', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/13129816_1545419769095130_1931985397_a.jpg', '697370954', 'SADAR!!! Laut bukan tempat sampah! .\n.\n.\n#Repost @conservationid -  Posisi pertama ditempati oleh China, lalu ketiga oleh FIlipina.\n\n#ciindonesia #buangsampahpadatempatnya #sadarlebihbaik #noplace4plastic', 'N;', NULL),
(4, 0, '1252127161487819896_1936651632', 1463485211, 'noplace4plastic', 1463570569, 'https://www.instagram.com/p/BFgcwTyGYR4/', 81, 'O:8:"stdClass":3:{s:14:"low_resolution";O:8:"stdClass":3:{s:3:"url";s:151:"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/13259664_1596585350658315_1061860033_n.jpg?ig_cache_key=MTI1MjEyNzE2MTQ4NzgxOTg5Ng%3D%3D.2.l";s:5:"width";i:320;s:6:"height";i:320;}s:9:"thumbnail";O:8:"stdClass":3:{s:3:"url";s:165:"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c108.0.863.863/13248836_1703271503267754_638673847_n.jpg?ig_cache_key=MTI1MjEyNzE2MTQ4NzgxOTg5Ng%3D%3D.2.c";s:5:"width";i:150;s:6:"height";i:150;}s:19:"standard_resolution";O:8:"stdClass":3:{s:3:"url";s:158:"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/13259664_1596585350658315_1061860033_n.jpg?ig_cache_key=MTI1MjEyNzE2MTQ4NzgxOTg5Ng%3D%3D.2.l";s:5:"width";i:640;s:6:"height";i:640;}}', 'Conservation Int''l Indonesia', 'conservationid', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/11821658_741154645996175_1381070449_a.jpg', '1936651632', 'Posisi pertama ditempati oleh China, lalu ketiga oleh FIlipina.\n\nMenurut kamu apa hal yang harus kita lakukan? #ciindonesia #noplace4plastic', 'N;', NULL),
(5, 0, '1250610749631989051_1936651632', 1463304440, 'noplace4plastic', 1463570569, 'https://www.instagram.com/p/BFbD9nMGYU7/', 78, 'O:8:"stdClass":3:{s:14:"low_resolution";O:8:"stdClass":3:{s:3:"url";s:149:"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/13249801_928757013889016_205587146_n.jpg?ig_cache_key=MTI1MDYxMDc0OTYzMTk4OTA1MQ%3D%3D.2.l";s:5:"width";i:320;s:6:"height";i:320;}s:9:"thumbnail";O:8:"stdClass":3:{s:3:"url";s:164:"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c108.0.863.863/13166765_238961506478614_908085954_n.jpg?ig_cache_key=MTI1MDYxMDc0OTYzMTk4OTA1MQ%3D%3D.2.c";s:5:"width";i:150;s:6:"height";i:150;}s:19:"standard_resolution";O:8:"stdClass":3:{s:3:"url";s:156:"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/13249801_928757013889016_205587146_n.jpg?ig_cache_key=MTI1MDYxMDc0OTYzMTk4OTA1MQ%3D%3D.2.l";s:5:"width";i:640;s:6:"height";i:640;}}', 'Conservation Int''l Indonesia', 'conservationid', 'https://scontent.cdninstagram.com/t51.2885-19/s150x150/11821658_741154645996175_1381070449_a.jpg', '1936651632', 'Bisa kita mulai dengan tidak membuang sampah ke laut. #noplace4plastic #ciindonesia', 'N;', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_instagram_receive`
--

CREATE TABLE IF NOT EXISTS `default_instagram_receive` (
  `id` int(11) NOT NULL,
  `received_data` text COLLATE utf8_unicode_ci,
  `created_on` int(11) DEFAULT NULL,
  `processed` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_instagram_subscriptions`
--

CREATE TABLE IF NOT EXISTS `default_instagram_subscriptions` (
  `id` int(11) NOT NULL,
  `date_subscribed` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `hashtag` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `data_type` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `data_object` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `data_aspect` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `data_callback_url` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_instagram_subscriptions`
--

INSERT INTO `default_instagram_subscriptions` (`id`, `date_subscribed`, `subscription_id`, `hashtag`, `data_type`, `data_object`, `data_aspect`, `data_callback_url`, `position`) VALUES
(1, 1431471124, 18114049, 'coraltriangle', 'subscription', 'tag', 'media', 'http://ctd.projects.catalyzecommunications.com/instagram_api/api', 0);

-- --------------------------------------------------------

--
-- Table structure for table `default_keywords`
--

CREATE TABLE IF NOT EXISTS `default_keywords` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_keywords`
--

INSERT INTO `default_keywords` (`id`, `name`) VALUES
(1, 'marine'),
(2, 'conservation'),
(3, 'coral triangle'),
(4, 'wwf'),
(5, 'event'),
(6, 'contest'),
(7, 'instagram');

-- --------------------------------------------------------

--
-- Table structure for table `default_keywords_applied`
--

CREATE TABLE IF NOT EXISTS `default_keywords_applied` (
  `id` int(11) NOT NULL,
  `hash` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `keyword_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_keywords_applied`
--

INSERT INTO `default_keywords_applied` (`id`, `hash`, `keyword_id`) VALUES
(33, '4a02c463fa5faee5f39388d483971429', 4),
(32, '4a02c463fa5faee5f39388d483971429', 1),
(31, '4a02c463fa5faee5f39388d483971429', 5),
(30, '4a02c463fa5faee5f39388d483971429', 3),
(29, '4a02c463fa5faee5f39388d483971429', 2),
(71, 'e2fe1951a9f32516f26940d084f37997', 4),
(70, 'e2fe1951a9f32516f26940d084f37997', 1),
(69, 'e2fe1951a9f32516f26940d084f37997', 3),
(68, 'e2fe1951a9f32516f26940d084f37997', 2),
(43, '9523b3fd881544157d537e8875c9e7c4', 7),
(42, '9523b3fd881544157d537e8875c9e7c4', 3),
(41, '9523b3fd881544157d537e8875c9e7c4', 6);

-- --------------------------------------------------------

--
-- Table structure for table `default_migrations`
--

CREATE TABLE IF NOT EXISTS `default_migrations` (
  `version` int(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_migrations`
--

INSERT INTO `default_migrations` (`version`) VALUES
(129);

-- --------------------------------------------------------

--
-- Table structure for table `default_modules`
--

CREATE TABLE IF NOT EXISTS `default_modules` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `skip_xss` tinyint(1) NOT NULL,
  `is_frontend` tinyint(1) NOT NULL,
  `is_backend` tinyint(1) NOT NULL,
  `menu` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `installed` tinyint(1) NOT NULL,
  `is_core` tinyint(1) NOT NULL,
  `updated_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_modules`
--

INSERT INTO `default_modules` (`id`, `name`, `slug`, `version`, `type`, `description`, `skip_xss`, `is_frontend`, `is_backend`, `menu`, `enabled`, `installed`, `is_core`, `updated_on`) VALUES
(1, 'a:25:{s:2:"en";s:8:"Settings";s:2:"ar";s:18:"الإعدادات";s:2:"br";s:15:"Configurações";s:2:"pt";s:15:"Configurações";s:2:"cs";s:10:"Nastavení";s:2:"da";s:13:"Indstillinger";s:2:"de";s:13:"Einstellungen";s:2:"el";s:18:"Ρυθμίσεις";s:2:"es";s:15:"Configuraciones";s:2:"fa";s:14:"تنظیمات";s:2:"fi";s:9:"Asetukset";s:2:"fr";s:11:"Paramètres";s:2:"he";s:12:"הגדרות";s:2:"id";s:10:"Pengaturan";s:2:"it";s:12:"Impostazioni";s:2:"lt";s:10:"Nustatymai";s:2:"nl";s:12:"Instellingen";s:2:"pl";s:10:"Ustawienia";s:2:"ru";s:18:"Настройки";s:2:"sl";s:10:"Nastavitve";s:2:"tw";s:12:"網站設定";s:2:"cn";s:12:"网站设定";s:2:"hu";s:14:"Beállítások";s:2:"th";s:21:"ตั้งค่า";s:2:"se";s:14:"Inställningar";}', 'settings', '1.0.0', NULL, 'a:25:{s:2:"en";s:89:"Allows administrators to update settings like Site Name, messages and email address, etc.";s:2:"ar";s:161:"تمكن المدراء من تحديث الإعدادات كإسم الموقع، والرسائل وعناوين البريد الإلكتروني، .. إلخ.";s:2:"br";s:120:"Permite com que administradores e a equipe consigam trocar as configurações do website incluindo o nome e descrição.";s:2:"pt";s:113:"Permite com que os administradores consigam alterar as configurações do website incluindo o nome e descrição.";s:2:"cs";s:102:"Umožňuje administrátorům měnit nastavení webu jako jeho jméno, zprávy a emailovou adresu apod.";s:2:"da";s:90:"Lader administratorer opdatere indstillinger som sidenavn, beskeder og email adresse, etc.";s:2:"de";s:92:"Erlaubt es Administratoren die Einstellungen der Seite wie Name und Beschreibung zu ändern.";s:2:"el";s:230:"Επιτρέπει στους διαχειριστές να τροποποιήσουν ρυθμίσεις όπως το Όνομα του Ιστοτόπου, τα μηνύματα και τις διευθύνσεις email, κ.α.";s:2:"es";s:131:"Permite a los administradores y al personal configurar los detalles del sitio como el nombre del sitio y la descripción del mismo.";s:2:"fa";s:105:"تنظیمات سایت در این ماژول توسط ادمین هاس سایت انجام می شود";s:2:"fi";s:105:"Mahdollistaa sivuston asetusten muokkaamisen, kuten sivuston nimen, viestit ja sähköpostiosoitteet yms.";s:2:"fr";s:118:"Permet aux admistrateurs de modifier les paramètres du site : nom du site, description, messages, adresse email, etc.";s:2:"he";s:116:"ניהול הגדרות שונות של האתר כגון: שם האתר, הודעות, כתובות דואר וכו";s:2:"id";s:112:"Memungkinkan administrator untuk dapat memperbaharui pengaturan seperti nama situs, pesan dan alamat email, dsb.";s:2:"it";s:109:"Permette agli amministratori di aggiornare impostazioni quali Nome del Sito, messaggi e indirizzo email, etc.";s:2:"lt";s:104:"Leidžia administratoriams keisti puslapio vavadinimą, žinutes, administratoriaus el. pašta ir kitą.";s:2:"nl";s:114:"Maakt het administratoren en medewerkers mogelijk om websiteinstellingen zoals naam en beschrijving te veranderen.";s:2:"pl";s:103:"Umożliwia administratorom zmianę ustawień strony jak nazwa strony, opis, e-mail administratora, itd.";s:2:"ru";s:135:"Управление настройками сайта - Имя сайта, сообщения, почтовые адреса и т.п.";s:2:"sl";s:98:"Dovoljuje administratorjem posodobitev nastavitev kot je Ime strani, sporočil, email naslova itd.";s:2:"tw";s:99:"網站管理者可更新的重要網站設定。例如：網站名稱、訊息、電子郵件等。";s:2:"cn";s:99:"网站管理者可更新的重要网站设定。例如：网站名称、讯息、电子邮件等。";s:2:"hu";s:125:"Lehetővé teszi az adminok számára a beállítások frissítését, mint a weboldal neve, üzenetek, e-mail címek, stb...";s:2:"th";s:232:"ให้ผู้ดูแลระบบสามารถปรับปรุงการตั้งค่าเช่นชื่อเว็บไซต์ ข้อความและอีเมล์เป็นต้น";s:2:"se";s:84:"Administratören kan uppdatera webbplatsens titel, meddelanden och E-postadress etc.";}', 1, 0, 1, 'settings', 1, 1, 1, 1430891256),
(2, 'a:11:{s:2:"en";s:12:"Streams Core";s:2:"pt";s:14:"Núcleo Fluxos";s:2:"fr";s:10:"Noyau Flux";s:2:"el";s:23:"Πυρήνας Ροών";s:2:"se";s:18:"Streams grundmodul";s:2:"tw";s:14:"Streams 核心";s:2:"cn";s:14:"Streams 核心";s:2:"ar";s:31:"الجداول الأساسية";s:2:"it";s:12:"Streams Core";s:2:"fa";s:26:"هسته استریم ها";s:2:"fi";s:13:"Striimit ydin";}', 'streams_core', '1.0.0', NULL, 'a:11:{s:2:"en";s:29:"Core data module for streams.";s:2:"pt";s:37:"Módulo central de dados para fluxos.";s:2:"fr";s:32:"Noyau de données pour les Flux.";s:2:"el";s:113:"Προγραμματιστικός πυρήνας για την λειτουργία ροών δεδομένων.";s:2:"se";s:50:"Streams grundmodul för enklare hantering av data.";s:2:"tw";s:29:"Streams 核心資料模組。";s:2:"cn";s:29:"Streams 核心资料模组。";s:2:"ar";s:57:"وحدة البيانات الأساسية للجداول";s:2:"it";s:17:"Core dello Stream";s:2:"fa";s:48:"ماژول مرکزی برای استریم ها";s:2:"fi";s:48:"Ydin datan hallinoiva moduuli striimejä varten.";}', 1, 0, 0, '0', 1, 1, 1, 1430891256),
(3, 'a:21:{s:2:"en";s:15:"Email Templates";s:2:"ar";s:48:"قوالب الرسائل الإلكترونية";s:2:"br";s:17:"Modelos de e-mail";s:2:"pt";s:17:"Modelos de e-mail";s:2:"da";s:16:"Email skabeloner";s:2:"el";s:22:"Δυναμικά email";s:2:"es";s:19:"Plantillas de email";s:2:"fa";s:26:"قالب های ایمیل";s:2:"fr";s:17:"Modèles d''emails";s:2:"he";s:12:"תבניות";s:2:"id";s:14:"Template Email";s:2:"lt";s:22:"El. laiškų šablonai";s:2:"nl";s:15:"Email sjablonen";s:2:"ru";s:25:"Шаблоны почты";s:2:"sl";s:14:"Email predloge";s:2:"tw";s:12:"郵件範本";s:2:"cn";s:12:"邮件范本";s:2:"hu";s:15:"E-mail sablonok";s:2:"fi";s:25:"Sähköposti viestipohjat";s:2:"th";s:33:"แม่แบบอีเมล";s:2:"se";s:12:"E-postmallar";}', 'templates', '1.1.0', NULL, 'a:21:{s:2:"en";s:46:"Create, edit, and save dynamic email templates";s:2:"ar";s:97:"أنشئ، عدّل واحفظ قوالب البريد الإلكترني الديناميكية.";s:2:"br";s:51:"Criar, editar e salvar modelos de e-mail dinâmicos";s:2:"pt";s:51:"Criar, editar e salvar modelos de e-mail dinâmicos";s:2:"da";s:49:"Opret, redigér og gem dynamiske emailskabeloner.";s:2:"el";s:108:"Δημιουργήστε, επεξεργαστείτε και αποθηκεύστε δυναμικά email.";s:2:"es";s:54:"Crear, editar y guardar plantillas de email dinámicas";s:2:"fa";s:92:"ایحاد، ویرایش و ذخیره ی قالب های ایمیل به صورت پویا";s:2:"fr";s:61:"Créer, éditer et sauver dynamiquement des modèles d''emails";s:2:"he";s:54:"ניהול של תבניות דואר אלקטרוני";s:2:"id";s:55:"Membuat, mengedit, dan menyimpan template email dinamis";s:2:"lt";s:58:"Kurk, tvarkyk ir saugok dinaminius el. laiškų šablonus.";s:2:"nl";s:49:"Maak, bewerk, en beheer dynamische emailsjablonen";s:2:"ru";s:127:"Создавайте, редактируйте и сохраняйте динамические почтовые шаблоны";s:2:"sl";s:52:"Ustvari, uredi in shrani spremenljive email predloge";s:2:"tw";s:61:"新增、編輯與儲存可顯示動態資料的 email 範本";s:2:"cn";s:61:"新增、编辑与储存可显示动态资料的 email 范本";s:2:"hu";s:63:"Csináld, szerkeszd és mentsd el a dinamikus e-mail sablonokat";s:2:"fi";s:66:"Lisää, muokkaa ja tallenna dynaamisia sähköposti viestipohjia.";s:2:"th";s:129:"การสร้างแก้ไขและบันทึกแม่แบบอีเมลแบบไดนามิก";s:2:"se";s:49:"Skapa, redigera och spara dynamiska E-postmallar.";}', 1, 0, 1, 'structure', 1, 1, 1, 1430891256),
(4, 'a:25:{s:2:"en";s:7:"Add-ons";s:2:"ar";s:16:"الإضافات";s:2:"br";s:12:"Complementos";s:2:"pt";s:12:"Complementos";s:2:"cs";s:8:"Doplňky";s:2:"da";s:7:"Add-ons";s:2:"de";s:13:"Erweiterungen";s:2:"el";s:16:"Πρόσθετα";s:2:"es";s:9:"Agregados";s:2:"fa";s:17:"افزونه ها";s:2:"fi";s:9:"Lisäosat";s:2:"fr";s:10:"Extensions";s:2:"he";s:12:"תוספות";s:2:"id";s:7:"Pengaya";s:2:"it";s:7:"Add-ons";s:2:"lt";s:7:"Priedai";s:2:"nl";s:7:"Add-ons";s:2:"pl";s:12:"Rozszerzenia";s:2:"ru";s:20:"Дополнения";s:2:"sl";s:11:"Razširitve";s:2:"tw";s:12:"附加模組";s:2:"cn";s:12:"附加模组";s:2:"hu";s:14:"Bővítmények";s:2:"th";s:27:"ส่วนเสริม";s:2:"se";s:8:"Tillägg";}', 'addons', '2.0.0', NULL, 'a:25:{s:2:"en";s:59:"Allows admins to see a list of currently installed modules.";s:2:"ar";s:91:"تُمكّن المُدراء من معاينة جميع الوحدات المُثبّتة.";s:2:"br";s:75:"Permite aos administradores ver a lista dos módulos instalados atualmente.";s:2:"pt";s:75:"Permite aos administradores ver a lista dos módulos instalados atualmente.";s:2:"cs";s:68:"Umožňuje administrátorům vidět seznam nainstalovaných modulů.";s:2:"da";s:63:"Lader administratorer se en liste over de installerede moduler.";s:2:"de";s:56:"Zeigt Administratoren alle aktuell installierten Module.";s:2:"el";s:152:"Επιτρέπει στους διαχειριστές να προβάλουν μια λίστα των εγκατεστημένων πρόσθετων.";s:2:"es";s:71:"Permite a los administradores ver una lista de los módulos instalados.";s:2:"fa";s:93:"مشاهده لیست افزونه ها و مدیریت آنها برای ادمین سایت";s:2:"fi";s:60:"Listaa järjestelmänvalvojalle käytössä olevat moduulit.";s:2:"fr";s:66:"Permet aux administrateurs de voir la liste des modules installés";s:2:"he";s:160:"נותן אופציה למנהל לראות רשימה של המודולים אשר מותקנים כעת באתר או להתקין מודולים נוספים";s:2:"id";s:57:"Memperlihatkan kepada admin daftar modul yang terinstall.";s:2:"it";s:83:"Permette agli amministratori di vedere una lista dei moduli attualmente installati.";s:2:"lt";s:75:"Vartotojai ir svečiai gali komentuoti jūsų naujienas, puslapius ar foto.";s:2:"nl";s:79:"Stelt admins in staat om een overzicht van geinstalleerde modules te genereren.";s:2:"pl";s:81:"Umożliwiają administratorowi wgląd do listy obecnie zainstalowanych modułów.";s:2:"ru";s:83:"Список модулей, которые установлены на сайте.";s:2:"sl";s:65:"Dovoljuje administratorjem pregled trenutno nameščenih modulov.";s:2:"tw";s:54:"管理員可以檢視目前已經安裝模組的列表";s:2:"cn";s:54:"管理员可以检视目前已经安装模组的列表";s:2:"hu";s:79:"Lehetővé teszi az adminoknak, hogy lássák a telepített modulok listáját.";s:2:"th";s:162:"ช่วยให้ผู้ดูแลระบบดูรายการของโมดูลที่ติดตั้งในปัจจุบัน";s:2:"se";s:67:"Gör det möjligt för administratören att se installerade mouler.";}', 0, 0, 1, '0', 1, 1, 1, 1430891256),
(5, 'a:17:{s:2:"en";s:4:"Blog";s:2:"ar";s:16:"المدوّنة";s:2:"br";s:4:"Blog";s:2:"pt";s:4:"Blog";s:2:"el";s:18:"Ιστολόγιο";s:2:"fa";s:8:"بلاگ";s:2:"he";s:8:"בלוג";s:2:"id";s:4:"Blog";s:2:"lt";s:6:"Blogas";s:2:"pl";s:4:"Blog";s:2:"ru";s:8:"Блог";s:2:"tw";s:6:"文章";s:2:"cn";s:6:"文章";s:2:"hu";s:4:"Blog";s:2:"fi";s:5:"Blogi";s:2:"th";s:15:"บล็อก";s:2:"se";s:5:"Blogg";}', 'blog', '2.0.0', NULL, 'a:25:{s:2:"en";s:18:"Post blog entries.";s:2:"ar";s:48:"أنشر المقالات على مدوّنتك.";s:2:"br";s:30:"Escrever publicações de blog";s:2:"pt";s:39:"Escrever e editar publicações no blog";s:2:"cs";s:49:"Publikujte nové články a příspěvky na blog.";s:2:"da";s:17:"Skriv blogindlæg";s:2:"de";s:47:"Veröffentliche neue Artikel und Blog-Einträge";s:2:"sl";s:23:"Objavite blog prispevke";s:2:"fi";s:28:"Kirjoita blogi artikkeleita.";s:2:"el";s:93:"Δημιουργήστε άρθρα και εγγραφές στο ιστολόγιο σας.";s:2:"es";s:54:"Escribe entradas para los artículos y blog (web log).";s:2:"fa";s:44:"مقالات منتشر شده در بلاگ";s:2:"fr";s:34:"Poster des articles d''actualités.";s:2:"he";s:19:"ניהול בלוג";s:2:"id";s:15:"Post entri blog";s:2:"it";s:36:"Pubblica notizie e post per il blog.";s:2:"lt";s:40:"Rašykite naujienas bei blog''o įrašus.";s:2:"nl";s:41:"Post nieuwsartikelen en blogs op uw site.";s:2:"pl";s:27:"Dodawaj nowe wpisy na blogu";s:2:"ru";s:49:"Управление записями блога.";s:2:"tw";s:42:"發表新聞訊息、部落格等文章。";s:2:"cn";s:42:"发表新闻讯息、部落格等文章。";s:2:"th";s:48:"โพสต์รายการบล็อก";s:2:"hu";s:32:"Blog bejegyzések létrehozása.";s:2:"se";s:18:"Inlägg i bloggen.";}', 1, 1, 1, 'content', 0, 1, 1, 1430891256),
(6, 'a:25:{s:2:"en";s:8:"Comments";s:2:"ar";s:18:"التعليقات";s:2:"br";s:12:"Comentários";s:2:"pt";s:12:"Comentários";s:2:"cs";s:11:"Komentáře";s:2:"da";s:11:"Kommentarer";s:2:"de";s:10:"Kommentare";s:2:"el";s:12:"Σχόλια";s:2:"es";s:11:"Comentarios";s:2:"fi";s:9:"Kommentit";s:2:"fr";s:12:"Commentaires";s:2:"fa";s:10:"نظرات";s:2:"he";s:12:"תגובות";s:2:"id";s:8:"Komentar";s:2:"it";s:8:"Commenti";s:2:"lt";s:10:"Komentarai";s:2:"nl";s:8:"Reacties";s:2:"pl";s:10:"Komentarze";s:2:"ru";s:22:"Комментарии";s:2:"sl";s:10:"Komentarji";s:2:"tw";s:6:"回應";s:2:"cn";s:6:"回应";s:2:"hu";s:16:"Hozzászólások";s:2:"th";s:33:"ความคิดเห็น";s:2:"se";s:11:"Kommentarer";}', 'comments', '1.1.0', NULL, 'a:25:{s:2:"en";s:76:"Users and guests can write comments for content like blog, pages and photos.";s:2:"ar";s:152:"يستطيع الأعضاء والزوّار كتابة التعليقات على المُحتوى كالأخبار، والصفحات والصّوَر.";s:2:"br";s:97:"Usuários e convidados podem escrever comentários para quase tudo com suporte nativo ao captcha.";s:2:"pt";s:100:"Utilizadores e convidados podem escrever comentários para quase tudo com suporte nativo ao captcha.";s:2:"cs";s:100:"Uživatelé a hosté mohou psát komentáře k obsahu, např. neovinkám, stránkám a fotografiím.";s:2:"da";s:83:"Brugere og besøgende kan skrive kommentarer til indhold som blog, sider og fotoer.";s:2:"de";s:65:"Benutzer und Gäste können für fast alles Kommentare schreiben.";s:2:"el";s:224:"Οι χρήστες και οι επισκέπτες μπορούν να αφήνουν σχόλια για περιεχόμενο όπως το ιστολόγιο, τις σελίδες και τις φωτογραφίες.";s:2:"es";s:130:"Los usuarios y visitantes pueden escribir comentarios en casi todo el contenido con el soporte de un sistema de captcha incluído.";s:2:"fa";s:168:"کاربران و مهمان ها می توانند نظرات خود را بر روی محتوای سایت در بلاگ و دیگر قسمت ها ارائه دهند";s:2:"fi";s:107:"Käyttäjät ja vieraat voivat kirjoittaa kommentteja eri sisältöihin kuten uutisiin, sivuihin ja kuviin.";s:2:"fr";s:130:"Les utilisateurs et les invités peuvent écrire des commentaires pour quasiment tout grâce au générateur de captcha intégré.";s:2:"he";s:94:"משתמשי האתר יכולים לרשום תגובות למאמרים, תמונות וכו";s:2:"id";s:100:"Pengguna dan pengunjung dapat menuliskan komentaruntuk setiap konten seperti blog, halaman dan foto.";s:2:"it";s:85:"Utenti e visitatori possono scrivere commenti ai contenuti quali blog, pagine e foto.";s:2:"lt";s:75:"Vartotojai ir svečiai gali komentuoti jūsų naujienas, puslapius ar foto.";s:2:"nl";s:52:"Gebruikers en gasten kunnen reageren op bijna alles.";s:2:"pl";s:93:"Użytkownicy i goście mogą dodawać komentarze z wbudowanym systemem zabezpieczeń captcha.";s:2:"ru";s:187:"Пользователи и гости могут добавлять комментарии к новостям, информационным страницам и фотографиям.";s:2:"sl";s:89:"Uporabniki in obiskovalci lahko vnesejo komentarje na vsebino kot je blok, stra ali slike";s:2:"tw";s:75:"用戶和訪客可以針對新聞、頁面與照片等內容發表回應。";s:2:"cn";s:75:"用户和访客可以针对新闻、页面与照片等内容发表回应。";s:2:"hu";s:117:"A felhasználók és a vendégek hozzászólásokat írhatnak a tartalomhoz (bejegyzésekhez, oldalakhoz, fotókhoz).";s:2:"th";s:240:"ผู้ใช้งานและผู้เยี่ยมชมสามารถเขียนความคิดเห็นในเนื้อหาของหน้าเว็บบล็อกและภาพถ่าย";s:2:"se";s:98:"Användare och besökare kan skriva kommentarer till innehåll som blogginlägg, sidor och bilder.";}', 0, 0, 1, 'content', 1, 1, 1, 1430891256),
(7, 'a:25:{s:2:"en";s:7:"Contact";s:2:"ar";s:14:"الإتصال";s:2:"br";s:7:"Contato";s:2:"pt";s:8:"Contacto";s:2:"cs";s:7:"Kontakt";s:2:"da";s:7:"Kontakt";s:2:"de";s:7:"Kontakt";s:2:"el";s:22:"Επικοινωνία";s:2:"es";s:8:"Contacto";s:2:"fa";s:18:"تماس با ما";s:2:"fi";s:13:"Ota yhteyttä";s:2:"fr";s:7:"Contact";s:2:"he";s:17:"יצירת קשר";s:2:"id";s:6:"Kontak";s:2:"it";s:10:"Contattaci";s:2:"lt";s:18:"Kontaktinė formą";s:2:"nl";s:7:"Contact";s:2:"pl";s:7:"Kontakt";s:2:"ru";s:27:"Обратная связь";s:2:"sl";s:7:"Kontakt";s:2:"tw";s:12:"聯絡我們";s:2:"cn";s:12:"联络我们";s:2:"hu";s:9:"Kapcsolat";s:2:"th";s:18:"ติดต่อ";s:2:"se";s:7:"Kontakt";}', 'contact', '1.0.0', NULL, 'a:25:{s:2:"en";s:112:"Adds a form to your site that allows visitors to send emails to you without disclosing an email address to them.";s:2:"ar";s:157:"إضافة استمارة إلى موقعك تُمكّن الزوّار من مراسلتك دون علمهم بعنوان البريد الإلكتروني.";s:2:"br";s:139:"Adiciona um formulário para o seu site permitir aos visitantes que enviem e-mails para voce sem divulgar um endereço de e-mail para eles.";s:2:"pt";s:116:"Adiciona um formulário ao seu site que permite aos visitantes enviarem e-mails sem divulgar um endereço de e-mail.";s:2:"cs";s:149:"Přidá na web kontaktní formulář pro návštěvníky a uživatele, díky kterému vás mohou kontaktovat i bez znalosti vaší e-mailové adresy.";s:2:"da";s:123:"Tilføjer en formular på din side som tillader besøgende at sende mails til dig, uden at du skal opgive din email-adresse";s:2:"de";s:119:"Fügt ein Formular hinzu, welches Besuchern erlaubt Emails zu schreiben, ohne die Kontakt Email-Adresse offen zu legen.";s:2:"el";s:273:"Προσθέτει μια φόρμα στον ιστότοπό σας που επιτρέπει σε επισκέπτες να σας στέλνουν μηνύμα μέσω email χωρίς να τους αποκαλύπτεται η διεύθυνση του email σας.";s:2:"fa";s:239:"فرم تماس را به سایت اضافه می کند تا مراجعین بتوانند بدون اینکه ایمیل شما را بدانند برای شما پیغام هایی را از طریق ایمیل ارسال نمایند.";s:2:"es";s:156:"Añade un formulario a tu sitio que permitirá a los visitantes enviarte correos electrónicos a ti sin darles tu dirección de correo directamente a ellos.";s:2:"fi";s:128:"Luo lomakkeen sivustollesi, josta kävijät voivat lähettää sähköpostia tietämättä vastaanottajan sähköpostiosoitetta.";s:2:"fr";s:122:"Ajoute un formulaire à votre site qui permet aux visiteurs de vous envoyer un e-mail sans révéler votre adresse e-mail.";s:2:"he";s:155:"מוסיף תופס יצירת קשר לאתר על מנת לא לחסוף כתובת דואר האלקטרוני של האתר למנועי פרסומות";s:2:"id";s:149:"Menambahkan formulir ke dalam situs Anda yang memungkinkan pengunjung untuk mengirimkan email kepada Anda tanpa memberikan alamat email kepada mereka";s:2:"it";s:119:"Aggiunge un modulo al tuo sito che permette ai visitatori di inviarti email senza mostrare loro il tuo indirizzo email.";s:2:"lt";s:124:"Prideda jūsų puslapyje formą leidžianti lankytojams siūsti jums el. laiškus neatskleidžiant jūsų el. pašto adreso.";s:2:"nl";s:125:"Voegt een formulier aan de site toe waarmee bezoekers een email kunnen sturen, zonder dat u ze een emailadres hoeft te tonen.";s:2:"pl";s:126:"Dodaje formularz kontaktowy do Twojej strony, który pozwala użytkownikom wysłanie maila za pomocą formularza kontaktowego.";s:2:"ru";s:234:"Добавляет форму обратной связи на сайт, через которую посетители могут отправлять вам письма, при этом адрес Email остаётся скрыт.";s:2:"sl";s:113:"Dodaj obrazec za kontakt da vam lahko obiskovalci pošljejo sporočilo brez da bi jim razkrili vaš email naslov.";s:2:"tw";s:147:"為您的網站新增「聯絡我們」的功能，對訪客是較為清楚便捷的聯絡方式，也無須您將電子郵件公開在網站上。";s:2:"cn";s:147:"为您的网站新增“联络我们”的功能，对访客是较为清楚便捷的联络方式，也无须您将电子邮件公开在网站上。";s:2:"th";s:316:"เพิ่มแบบฟอร์มในเว็บไซต์ของคุณ ช่วยให้ผู้เยี่ยมชมสามารถส่งอีเมลถึงคุณโดยไม่ต้องเปิดเผยที่อยู่อีเมลของพวกเขา";s:2:"hu";s:156:"Létrehozható vele olyan űrlap, amely lehetővé teszi a látogatók számára, hogy e-mailt küldjenek neked úgy, hogy nem feded fel az e-mail címedet.";s:2:"se";s:53:"Lägger till ett kontaktformulär till din webbplats.";}', 0, 0, 0, '0', 1, 1, 1, 1430891256),
(8, 'a:24:{s:2:"en";s:5:"Files";s:2:"ar";s:16:"الملفّات";s:2:"br";s:8:"Arquivos";s:2:"pt";s:9:"Ficheiros";s:2:"cs";s:7:"Soubory";s:2:"da";s:5:"Filer";s:2:"de";s:7:"Dateien";s:2:"el";s:12:"Αρχεία";s:2:"es";s:8:"Archivos";s:2:"fa";s:13:"فایل ها";s:2:"fi";s:9:"Tiedostot";s:2:"fr";s:8:"Fichiers";s:2:"he";s:10:"קבצים";s:2:"id";s:4:"File";s:2:"it";s:4:"File";s:2:"lt";s:6:"Failai";s:2:"nl";s:9:"Bestanden";s:2:"ru";s:10:"Файлы";s:2:"sl";s:8:"Datoteke";s:2:"tw";s:6:"檔案";s:2:"cn";s:6:"档案";s:2:"hu";s:7:"Fájlok";s:2:"th";s:12:"ไฟล์";s:2:"se";s:5:"Filer";}', 'files', '2.0.0', NULL, 'a:24:{s:2:"en";s:40:"Manages files and folders for your site.";s:2:"ar";s:50:"إدارة ملفات ومجلّدات موقعك.";s:2:"br";s:53:"Permite gerenciar facilmente os arquivos de seu site.";s:2:"pt";s:59:"Permite gerir facilmente os ficheiros e pastas do seu site.";s:2:"cs";s:43:"Spravujte soubory a složky na vašem webu.";s:2:"da";s:41:"Administrer filer og mapper for dit site.";s:2:"de";s:35:"Verwalte Dateien und Verzeichnisse.";s:2:"el";s:100:"Διαχειρίζεται αρχεία και φακέλους για το ιστότοπό σας.";s:2:"es";s:43:"Administra archivos y carpetas en tu sitio.";s:2:"fa";s:79:"مدیریت فایل های چند رسانه ای و فولدر ها سایت";s:2:"fi";s:43:"Hallitse sivustosi tiedostoja ja kansioita.";s:2:"fr";s:46:"Gérer les fichiers et dossiers de votre site.";s:2:"he";s:47:"ניהול תיקיות וקבצים שבאתר";s:2:"id";s:42:"Mengatur file dan folder dalam situs Anda.";s:2:"it";s:38:"Gestisci file e cartelle del tuo sito.";s:2:"lt";s:28:"Katalogų ir bylų valdymas.";s:2:"nl";s:41:"Beheer bestanden en mappen op uw website.";s:2:"ru";s:78:"Управление файлами и папками вашего сайта.";s:2:"sl";s:38:"Uredi datoteke in mape na vaši strani";s:2:"tw";s:33:"管理網站中的檔案與目錄";s:2:"cn";s:33:"管理网站中的档案与目录";s:2:"hu";s:41:"Fájlok és mappák kezelése az oldalon.";s:2:"th";s:141:"บริหารจัดการไฟล์และโฟลเดอร์สำหรับเว็บไซต์ของคุณ";s:2:"se";s:45:"Hanterar filer och mappar för din webbplats.";}', 0, 0, 1, 'content', 1, 1, 1, 1430891256),
(9, 'a:24:{s:2:"en";s:6:"Groups";s:2:"ar";s:18:"المجموعات";s:2:"br";s:6:"Grupos";s:2:"pt";s:6:"Grupos";s:2:"cs";s:7:"Skupiny";s:2:"da";s:7:"Grupper";s:2:"de";s:7:"Gruppen";s:2:"el";s:12:"Ομάδες";s:2:"es";s:6:"Grupos";s:2:"fa";s:13:"گروه ها";s:2:"fi";s:7:"Ryhmät";s:2:"fr";s:7:"Groupes";s:2:"he";s:12:"קבוצות";s:2:"id";s:4:"Grup";s:2:"it";s:6:"Gruppi";s:2:"lt";s:7:"Grupės";s:2:"nl";s:7:"Groepen";s:2:"ru";s:12:"Группы";s:2:"sl";s:7:"Skupine";s:2:"tw";s:6:"群組";s:2:"cn";s:6:"群组";s:2:"hu";s:9:"Csoportok";s:2:"th";s:15:"กลุ่ม";s:2:"se";s:7:"Grupper";}', 'groups', '1.0.0', NULL, 'a:24:{s:2:"en";s:54:"Users can be placed into groups to manage permissions.";s:2:"ar";s:100:"يمكن وضع المستخدمين في مجموعات لتسهيل إدارة صلاحياتهم.";s:2:"br";s:72:"Usuários podem ser inseridos em grupos para gerenciar suas permissões.";s:2:"pt";s:74:"Utilizadores podem ser inseridos em grupos para gerir as suas permissões.";s:2:"cs";s:77:"Uživatelé mohou být rozřazeni do skupin pro lepší správu oprávnění.";s:2:"da";s:49:"Brugere kan inddeles i grupper for adgangskontrol";s:2:"de";s:85:"Benutzer können zu Gruppen zusammengefasst werden um diesen Zugriffsrechte zu geben.";s:2:"el";s:168:"Οι χρήστες μπορούν να τοποθετηθούν σε ομάδες και έτσι να διαχειριστείτε τα δικαιώματά τους.";s:2:"es";s:75:"Los usuarios podrán ser colocados en grupos para administrar sus permisos.";s:2:"fa";s:149:"کاربرها می توانند در گروه های ساماندهی شوند تا بتوان اجازه های مختلفی را ایجاد کرد";s:2:"fi";s:84:"Käyttäjät voidaan liittää ryhmiin, jotta käyttöoikeuksia voidaan hallinnoida.";s:2:"fr";s:82:"Les utilisateurs peuvent appartenir à des groupes afin de gérer les permissions.";s:2:"he";s:62:"נותן אפשרות לאסוף משתמשים לקבוצות";s:2:"id";s:68:"Pengguna dapat dikelompokkan ke dalam grup untuk mengatur perizinan.";s:2:"it";s:69:"Gli utenti possono essere inseriti in gruppi per gestirne i permessi.";s:2:"lt";s:67:"Vartotojai gali būti priskirti grupei tam, kad valdyti jų teises.";s:2:"nl";s:73:"Gebruikers kunnen in groepen geplaatst worden om rechten te kunnen geven.";s:2:"ru";s:134:"Пользователей можно объединять в группы, для управления правами доступа.";s:2:"sl";s:64:"Uporabniki so lahko razvrščeni v skupine za urejanje dovoljenj";s:2:"tw";s:45:"用戶可以依群組分類並管理其權限";s:2:"cn";s:45:"用户可以依群组分类并管理其权限";s:2:"hu";s:73:"A felhasználók csoportokba rendezhetőek a jogosultságok kezelésére.";s:2:"th";s:84:"สามารถวางผู้ใช้ลงในกลุ่มเพื่";s:2:"se";s:76:"Användare kan delas in i grupper för att hantera roller och behörigheter.";}', 0, 0, 1, 'users', 1, 1, 1, 1430891256),
(10, 'a:17:{s:2:"en";s:8:"Keywords";s:2:"ar";s:21:"كلمات البحث";s:2:"br";s:14:"Palavras-chave";s:2:"pt";s:14:"Palavras-chave";s:2:"da";s:9:"Nøgleord";s:2:"el";s:27:"Λέξεις Κλειδιά";s:2:"fa";s:21:"کلمات کلیدی";s:2:"fr";s:10:"Mots-Clés";s:2:"id";s:10:"Kata Kunci";s:2:"nl";s:14:"Sleutelwoorden";s:2:"tw";s:6:"鍵詞";s:2:"cn";s:6:"键词";s:2:"hu";s:11:"Kulcsszavak";s:2:"fi";s:10:"Avainsanat";s:2:"sl";s:15:"Ključne besede";s:2:"th";s:15:"คำค้น";s:2:"se";s:9:"Nyckelord";}', 'keywords', '1.1.0', NULL, 'a:17:{s:2:"en";s:71:"Maintain a central list of keywords to label and organize your content.";s:2:"ar";s:124:"أنشئ مجموعة من كلمات البحث التي تستطيع من خلالها وسم وتنظيم المحتوى.";s:2:"br";s:85:"Mantém uma lista central de palavras-chave para rotular e organizar o seu conteúdo.";s:2:"pt";s:85:"Mantém uma lista central de palavras-chave para rotular e organizar o seu conteúdo.";s:2:"da";s:72:"Vedligehold en central liste af nøgleord for at organisere dit indhold.";s:2:"el";s:181:"Συντηρεί μια κεντρική λίστα από λέξεις κλειδιά για να οργανώνετε μέσω ετικετών το περιεχόμενό σας.";s:2:"fa";s:110:"حفظ و نگهداری لیست مرکزی از کلمات کلیدی برای سازماندهی محتوا";s:2:"fr";s:87:"Maintenir une liste centralisée de Mots-Clés pour libeller et organiser vos contenus.";s:2:"id";s:71:"Memantau daftar kata kunci untuk melabeli dan mengorganisasikan konten.";s:2:"nl";s:91:"Beheer een centrale lijst van sleutelwoorden om uw content te categoriseren en organiseren.";s:2:"tw";s:64:"集中管理可用於標題與內容的鍵詞(keywords)列表。";s:2:"cn";s:64:"集中管理可用于标题与内容的键词(keywords)列表。";s:2:"hu";s:65:"Ez egy központi kulcsszó lista a cimkékhez és a tartalmakhoz.";s:2:"fi";s:92:"Hallinnoi keskitettyä listaa avainsanoista merkitäksesi ja järjestelläksesi sisältöä.";s:2:"sl";s:82:"Vzdržuj centralni seznam ključnih besed za označevanje in ogranizacijo vsebine.";s:2:"th";s:189:"ศูนย์กลางการปรับปรุงคำค้นในการติดฉลากและจัดระเบียบเนื้อหาของคุณ";s:2:"se";s:61:"Hantera nyckelord för att organisera webbplatsens innehåll.";}', 0, 0, 1, 'data', 1, 1, 1, 1430891256),
(11, 'a:15:{s:2:"en";s:11:"Maintenance";s:2:"pt";s:12:"Manutenção";s:2:"ar";s:14:"الصيانة";s:2:"el";s:18:"Συντήρηση";s:2:"hu";s:13:"Karbantartás";s:2:"fa";s:15:"نگه داری";s:2:"fi";s:9:"Ylläpito";s:2:"fr";s:11:"Maintenance";s:2:"id";s:12:"Pemeliharaan";s:2:"it";s:12:"Manutenzione";s:2:"se";s:10:"Underhåll";s:2:"sl";s:12:"Vzdrževanje";s:2:"th";s:39:"การบำรุงรักษา";s:2:"tw";s:6:"維護";s:2:"cn";s:6:"维护";}', 'maintenance', '1.0.0', NULL, 'a:15:{s:2:"en";s:63:"Manage the site cache and export information from the database.";s:2:"pt";s:68:"Gerir o cache do seu site e exportar informações da base de dados.";s:2:"ar";s:81:"حذف عناصر الذاكرة المخبأة عبر واجهة الإدارة.";s:2:"el";s:142:"Διαγραφή αντικειμένων προσωρινής αποθήκευσης μέσω της περιοχής διαχείρισης.";s:2:"id";s:60:"Mengatur cache situs dan mengexport informasi dari database.";s:2:"it";s:65:"Gestisci la cache del sito e esporta le informazioni dal database";s:2:"fa";s:73:"مدیریت کش سایت و صدور اطلاعات از دیتابیس";s:2:"fr";s:71:"Gérer le cache du site et exporter les contenus de la base de données";s:2:"fi";s:59:"Hallinoi sivuston välimuistia ja vie tietoa tietokannasta.";s:2:"hu";s:66:"Az oldal gyorsítótár kezelése és az adatbázis exportálása.";s:2:"se";s:76:"Underhåll webbplatsens cache och exportera data från webbplatsens databas.";s:2:"sl";s:69:"Upravljaj s predpomnilnikom strani (cache) in izvozi podatke iz baze.";s:2:"th";s:150:"การจัดการแคชเว็บไซต์และข้อมูลการส่งออกจากฐานข้อมูล";s:2:"tw";s:45:"經由管理介面手動刪除暫存資料。";s:2:"cn";s:45:"经由管理介面手动删除暂存资料。";}', 0, 0, 1, 'data', 1, 1, 1, 1430891256),
(12, 'a:25:{s:2:"en";s:10:"Navigation";s:2:"ar";s:14:"الروابط";s:2:"br";s:11:"Navegação";s:2:"pt";s:11:"Navegação";s:2:"cs";s:8:"Navigace";s:2:"da";s:10:"Navigation";s:2:"de";s:10:"Navigation";s:2:"el";s:16:"Πλοήγηση";s:2:"es";s:11:"Navegación";s:2:"fa";s:11:"منو ها";s:2:"fi";s:10:"Navigointi";s:2:"fr";s:10:"Navigation";s:2:"he";s:10:"ניווט";s:2:"id";s:8:"Navigasi";s:2:"it";s:11:"Navigazione";s:2:"lt";s:10:"Navigacija";s:2:"nl";s:9:"Navigatie";s:2:"pl";s:9:"Nawigacja";s:2:"ru";s:18:"Навигация";s:2:"sl";s:10:"Navigacija";s:2:"tw";s:12:"導航選單";s:2:"cn";s:12:"导航选单";s:2:"th";s:36:"ตัวช่วยนำทาง";s:2:"hu";s:11:"Navigáció";s:2:"se";s:10:"Navigation";}', 'navigation', '1.1.0', NULL, 'a:25:{s:2:"en";s:78:"Manage links on navigation menus and all the navigation groups they belong to.";s:2:"ar";s:85:"إدارة روابط وقوائم ومجموعات الروابط في الموقع.";s:2:"br";s:91:"Gerenciar links do menu de navegação e todos os grupos de navegação pertencentes a ele.";s:2:"pt";s:93:"Gerir todos os grupos dos menus de navegação e os links de navegação pertencentes a eles.";s:2:"cs";s:73:"Správa odkazů v navigaci a všech souvisejících navigačních skupin.";s:2:"da";s:82:"Håndtér links på navigationsmenuerne og alle navigationsgrupperne de tilhører.";s:2:"de";s:76:"Verwalte Links in Navigationsmenüs und alle zugehörigen Navigationsgruppen";s:2:"el";s:207:"Διαχειριστείτε τους συνδέσμους στα μενού πλοήγησης και όλες τις ομάδες συνδέσμων πλοήγησης στις οποίες ανήκουν.";s:2:"es";s:102:"Administra links en los menús de navegación y en todos los grupos de navegación al cual pertenecen.";s:2:"fa";s:68:"مدیریت منو ها و گروه های مربوط به آنها";s:2:"fi";s:91:"Hallitse linkkejä navigointi valikoissa ja kaikkia navigointi ryhmiä, joihin ne kuuluvat.";s:2:"fr";s:97:"Gérer les liens du menu Navigation et tous les groupes de navigation auxquels ils appartiennent.";s:2:"he";s:73:"ניהול שלוחות תפריטי ניווט וקבוצות ניווט";s:2:"id";s:73:"Mengatur tautan pada menu navigasi dan semua pengelompokan grup navigasi.";s:2:"it";s:97:"Gestisci i collegamenti dei menu di navigazione e tutti i gruppi di navigazione da cui dipendono.";s:2:"lt";s:95:"Tvarkyk nuorodas navigacijų menių ir visas navigacijų grupes kurioms tos nuorodos priklauso.";s:2:"nl";s:92:"Beheer koppelingen op de navigatiemenu&apos;s en alle navigatiegroepen waar ze onder vallen.";s:2:"pl";s:95:"Zarządzaj linkami w menu nawigacji oraz wszystkimi grupami nawigacji do których one należą.";s:2:"ru";s:136:"Управление ссылками в меню навигации и группах, к которым они принадлежат.";s:2:"sl";s:64:"Uredi povezave v meniju in vse skupine povezav ki jim pripadajo.";s:2:"tw";s:72:"管理導航選單中的連結，以及它們所隸屬的導航群組。";s:2:"cn";s:72:"管理导航选单中的连结，以及它们所隶属的导航群组。";s:2:"th";s:108:"จัดการการเชื่อมโยงนำทางและกลุ่มนำทาง";s:2:"se";s:33:"Hantera länkar och länkgrupper.";s:2:"hu";s:100:"Linkek kezelése a navigációs menükben és a navigációs csoportok kezelése, amikhez tartoznak.";}', 0, 0, 1, 'structure', 1, 1, 1, 1430891256),
(13, 'a:25:{s:2:"en";s:5:"Pages";s:2:"ar";s:14:"الصفحات";s:2:"br";s:8:"Páginas";s:2:"pt";s:8:"Páginas";s:2:"cs";s:8:"Stránky";s:2:"da";s:5:"Sider";s:2:"de";s:6:"Seiten";s:2:"el";s:14:"Σελίδες";s:2:"es";s:8:"Páginas";s:2:"fa";s:14:"صفحه ها ";s:2:"fi";s:5:"Sivut";s:2:"fr";s:5:"Pages";s:2:"he";s:8:"דפים";s:2:"id";s:7:"Halaman";s:2:"it";s:6:"Pagine";s:2:"lt";s:9:"Puslapiai";s:2:"nl";s:13:"Pagina&apos;s";s:2:"pl";s:6:"Strony";s:2:"ru";s:16:"Страницы";s:2:"sl";s:6:"Strani";s:2:"tw";s:6:"頁面";s:2:"cn";s:6:"页面";s:2:"hu";s:7:"Oldalak";s:2:"th";s:21:"หน้าเพจ";s:2:"se";s:5:"Sidor";}', 'pages', '2.2.0', NULL, 'a:25:{s:2:"en";s:55:"Add custom pages to the site with any content you want.";s:2:"ar";s:99:"إضافة صفحات مُخصّصة إلى الموقع تحتوي أية مُحتوى تريده.";s:2:"br";s:82:"Adicionar páginas personalizadas ao site com qualquer conteúdo que você queira.";s:2:"pt";s:86:"Adicionar páginas personalizadas ao seu site com qualquer conteúdo que você queira.";s:2:"cs";s:74:"Přidávejte vlastní stránky na web s jakýmkoliv obsahem budete chtít.";s:2:"da";s:71:"Tilføj brugerdefinerede sider til dit site med det indhold du ønsker.";s:2:"de";s:49:"Füge eigene Seiten mit anpassbaren Inhalt hinzu.";s:2:"el";s:152:"Προσθέστε και επεξεργαστείτε σελίδες στον ιστότοπό σας με ό,τι περιεχόμενο θέλετε.";s:2:"es";s:77:"Agrega páginas customizadas al sitio con cualquier contenido que tu quieras.";s:2:"fa";s:96:"ایحاد صفحات جدید و دلخواه با هر محتوایی که دوست دارید";s:2:"fi";s:47:"Lisää mitä tahansa sisältöä sivustollesi.";s:2:"fr";s:89:"Permet d''ajouter sur le site des pages personalisées avec le contenu que vous souhaitez.";s:2:"he";s:35:"ניהול דפי תוכן האתר";s:2:"id";s:75:"Menambahkan halaman ke dalam situs dengan konten apapun yang Anda perlukan.";s:2:"it";s:73:"Aggiungi pagine personalizzate al sito con qualsiesi contenuto tu voglia.";s:2:"lt";s:46:"Pridėkite nuosavus puslapius betkokio turinio";s:2:"nl";s:70:"Voeg aangepaste pagina&apos;s met willekeurige inhoud aan de site toe.";s:2:"pl";s:53:"Dodaj własne strony z dowolną treścią do witryny.";s:2:"ru";s:134:"Управление информационными страницами сайта, с произвольным содержимым.";s:2:"sl";s:44:"Dodaj stran s kakršno koli vsebino želite.";s:2:"tw";s:39:"為您的網站新增自定的頁面。";s:2:"cn";s:39:"为您的网站新增自定的页面。";s:2:"th";s:168:"เพิ่มหน้าเว็บที่กำหนดเองไปยังเว็บไซต์ของคุณตามที่ต้องการ";s:2:"hu";s:67:"Saját oldalak hozzáadása a weboldalhoz, akármilyen tartalommal.";s:2:"se";s:39:"Lägg till egna sidor till webbplatsen.";}', 1, 1, 1, 'content', 1, 1, 1, 1430891256),
(14, 'a:25:{s:2:"en";s:11:"Permissions";s:2:"ar";s:18:"الصلاحيات";s:2:"br";s:11:"Permissões";s:2:"pt";s:11:"Permissões";s:2:"cs";s:12:"Oprávnění";s:2:"da";s:14:"Adgangskontrol";s:2:"de";s:14:"Zugriffsrechte";s:2:"el";s:20:"Δικαιώματα";s:2:"es";s:8:"Permisos";s:2:"fa";s:15:"اجازه ها";s:2:"fi";s:16:"Käyttöoikeudet";s:2:"fr";s:11:"Permissions";s:2:"he";s:12:"הרשאות";s:2:"id";s:9:"Perizinan";s:2:"it";s:8:"Permessi";s:2:"lt";s:7:"Teisės";s:2:"nl";s:15:"Toegangsrechten";s:2:"pl";s:11:"Uprawnienia";s:2:"ru";s:25:"Права доступа";s:2:"sl";s:10:"Dovoljenja";s:2:"tw";s:6:"權限";s:2:"cn";s:6:"权限";s:2:"hu";s:14:"Jogosultságok";s:2:"th";s:18:"สิทธิ์";s:2:"se";s:13:"Behörigheter";}', 'permissions', '1.0.0', NULL, 'a:25:{s:2:"en";s:68:"Control what type of users can see certain sections within the site.";s:2:"ar";s:127:"التحكم بإعطاء الصلاحيات للمستخدمين للوصول إلى أقسام الموقع المختلفة.";s:2:"br";s:68:"Controle quais tipos de usuários podem ver certas seções no site.";s:2:"pt";s:75:"Controle quais os tipos de utilizadores podem ver certas secções no site.";s:2:"cs";s:93:"Spravujte oprávnění pro jednotlivé typy uživatelů a ke kterým sekcím mají přístup.";s:2:"da";s:72:"Kontroller hvilken type brugere der kan se bestemte sektioner på sitet.";s:2:"de";s:70:"Regelt welche Art von Benutzer welche Sektion in der Seite sehen kann.";s:2:"el";s:180:"Ελέγξτε τα δικαιώματα χρηστών και ομάδων χρηστών όσο αφορά σε διάφορες λειτουργίες του ιστοτόπου.";s:2:"es";s:81:"Controla que tipo de usuarios pueden ver secciones específicas dentro del sitio.";s:2:"fa";s:59:"مدیریت اجازه های گروه های کاربری";s:2:"fi";s:72:"Hallitse minkä tyyppisiin osioihin käyttäjät pääsevät sivustolla.";s:2:"fr";s:104:"Permet de définir les autorisations des groupes d''utilisateurs pour afficher les différentes sections.";s:2:"he";s:75:"ניהול הרשאות כניסה לאיזורים מסוימים באתר";s:2:"id";s:76:"Mengontrol tipe pengguna mana yang dapat mengakses suatu bagian dalam situs.";s:2:"it";s:78:"Controlla che tipo di utenti posssono accedere a determinate sezioni del sito.";s:2:"lt";s:72:"Kontroliuokite kokio tipo varotojai kokią dalį puslapio gali pasiekti.";s:2:"nl";s:71:"Bepaal welke typen gebruikers toegang hebben tot gedeeltes van de site.";s:2:"pl";s:79:"Ustaw, którzy użytkownicy mogą mieć dostęp do odpowiednich sekcji witryny.";s:2:"ru";s:209:"Управление правами доступа, ограничение доступа определённых групп пользователей к произвольным разделам сайта.";s:2:"sl";s:85:"Uredite dovoljenja kateri tip uporabnika lahko vidi določena področja vaše strani.";s:2:"tw";s:81:"用來控制不同類別的用戶，設定其瀏覽特定網站內容的權限。";s:2:"cn";s:81:"用来控制不同类别的用户，设定其浏览特定网站内容的权限。";s:2:"hu";s:129:"A felhasználók felügyelet alatt tartására, hogy milyen típusú felhasználók, mit láthatnak, mely szakaszain az oldalnak.";s:2:"th";s:117:"ควบคุมว่าผู้ใช้งานจะเห็นหมวดหมู่ไหนบ้าง";s:2:"se";s:27:"Hantera gruppbehörigheter.";}', 0, 0, 1, 'users', 1, 1, 1, 1430891256),
(15, 'a:24:{s:2:"en";s:9:"Redirects";s:2:"ar";s:18:"التوجيهات";s:2:"br";s:17:"Redirecionamentos";s:2:"pt";s:17:"Redirecionamentos";s:2:"cs";s:16:"Přesměrování";s:2:"da";s:13:"Omadressering";s:2:"el";s:30:"Ανακατευθύνσεις";s:2:"es";s:13:"Redirecciones";s:2:"fa";s:17:"انتقال ها";s:2:"fi";s:18:"Uudelleenohjaukset";s:2:"fr";s:12:"Redirections";s:2:"he";s:12:"הפניות";s:2:"id";s:8:"Redirect";s:2:"it";s:11:"Reindirizzi";s:2:"lt";s:14:"Peradresavimai";s:2:"nl";s:12:"Verwijzingen";s:2:"ru";s:30:"Перенаправления";s:2:"sl";s:12:"Preusmeritve";s:2:"tw";s:6:"轉址";s:2:"cn";s:6:"转址";s:2:"hu";s:17:"Átirányítások";s:2:"pl";s:14:"Przekierowania";s:2:"th";s:42:"เปลี่ยนเส้นทาง";s:2:"se";s:14:"Omdirigeringar";}', 'redirects', '1.0.0', NULL, 'a:24:{s:2:"en";s:33:"Redirect from one URL to another.";s:2:"ar";s:47:"التوجيه من رابط URL إلى آخر.";s:2:"br";s:39:"Redirecionamento de uma URL para outra.";s:2:"pt";s:40:"Redirecionamentos de uma URL para outra.";s:2:"cs";s:43:"Přesměrujte z jedné adresy URL na jinou.";s:2:"da";s:35:"Omadresser fra en URL til en anden.";s:2:"el";s:81:"Ανακατευθείνετε μια διεύθυνση URL σε μια άλλη";s:2:"es";s:34:"Redireccionar desde una URL a otra";s:2:"fa";s:63:"انتقال دادن یک صفحه به یک آدرس دیگر";s:2:"fi";s:45:"Uudelleenohjaa käyttäjän paikasta toiseen.";s:2:"fr";s:34:"Redirection d''une URL à un autre.";s:2:"he";s:43:"הפניות מכתובת אחת לאחרת";s:2:"id";s:40:"Redirect dari satu URL ke URL yang lain.";s:2:"it";s:35:"Reindirizza da una URL ad un altra.";s:2:"lt";s:56:"Peradresuokite puslapį iš vieno adreso (URL) į kitą.";s:2:"nl";s:38:"Verwijs vanaf een URL naar een andere.";s:2:"ru";s:78:"Перенаправления с одного адреса на другой.";s:2:"sl";s:44:"Preusmeritev iz enega URL naslova na drugega";s:2:"tw";s:33:"將網址轉址、重新定向。";s:2:"cn";s:33:"将网址转址、重新定向。";s:2:"hu";s:38:"Egy URL átirányítása egy másikra.";s:2:"pl";s:44:"Przekierowanie z jednego adresu URL na inny.";s:2:"th";s:123:"เปลี่ยนเส้นทางจากที่หนึ่งไปยังอีกที่หนึ่ง";s:2:"se";s:38:"Omdirigera från en URL till en annan.";}', 0, 0, 1, 'structure', 1, 1, 1, 1430891256),
(16, 'a:10:{s:2:"en";s:6:"Search";s:2:"br";s:7:"Procura";s:2:"fr";s:9:"Recherche";s:2:"se";s:4:"Sök";s:2:"ar";s:10:"البحث";s:2:"tw";s:6:"搜尋";s:2:"cn";s:6:"搜寻";s:2:"it";s:7:"Ricerca";s:2:"fa";s:10:"جستجو";s:2:"fi";s:4:"Etsi";}', 'search', '1.0.0', NULL, 'a:10:{s:2:"en";s:72:"Search through various types of content with this modular search system.";s:2:"br";s:73:"Procure por vários tipos de conteúdo com este sistema de busca modular.";s:2:"fr";s:84:"Rechercher parmi différents types de contenus avec système de recherche modulaire.";s:2:"se";s:36:"Sök igenom olika typer av innehåll";s:2:"ar";s:102:"ابحث في أنواع مختلفة من المحتوى باستخدام نظام البحث هذا.";s:2:"tw";s:63:"此模組可用以搜尋網站中不同類型的資料內容。";s:2:"cn";s:63:"此模组可用以搜寻网站中不同类型的资料内容。";s:2:"it";s:71:"Cerca tra diversi tipi di contenuti con il sistema di reicerca modulare";s:2:"fa";s:115:"توسط این ماژول می توانید در محتواهای مختلف وبسایت جستجو نمایید.";s:2:"fi";s:76:"Etsi eri tyypistä sisältöä tällä modulaarisella hakujärjestelmällä.";}', 0, 0, 0, '0', 0, 1, 1, 1430891256),
(17, 'a:20:{s:2:"en";s:7:"Sitemap";s:2:"ar";s:23:"خريطة الموقع";s:2:"br";s:12:"Mapa do Site";s:2:"pt";s:12:"Mapa do Site";s:2:"de";s:7:"Sitemap";s:2:"el";s:31:"Χάρτης Ιστότοπου";s:2:"es";s:14:"Mapa del Sitio";s:2:"fa";s:17:"نقشه سایت";s:2:"fi";s:10:"Sivukartta";s:2:"fr";s:12:"Plan du site";s:2:"id";s:10:"Peta Situs";s:2:"it";s:14:"Mappa del sito";s:2:"lt";s:16:"Svetainės medis";s:2:"nl";s:7:"Sitemap";s:2:"ru";s:21:"Карта сайта";s:2:"tw";s:12:"網站地圖";s:2:"cn";s:12:"网站地图";s:2:"th";s:21:"ไซต์แมพ";s:2:"hu";s:13:"Oldaltérkép";s:2:"se";s:9:"Sajtkarta";}', 'sitemap', '1.3.0', NULL, 'a:21:{s:2:"en";s:87:"The sitemap module creates an index of all pages and an XML sitemap for search engines.";s:2:"ar";s:120:"وحدة خريطة الموقع تنشئ فهرساً لجميع الصفحات وملف XML لمحركات البحث.";s:2:"br";s:102:"O módulo de mapa do site cria um índice de todas as páginas e um sitemap XML para motores de busca.";s:2:"pt";s:102:"O módulo do mapa do site cria um índice de todas as páginas e um sitemap XML para motores de busca.";s:2:"da";s:86:"Sitemapmodulet opretter et indeks over alle sider og et XML sitemap til søgemaskiner.";s:2:"de";s:92:"Die Sitemap Modul erstellt einen Index aller Seiten und eine XML-Sitemap für Suchmaschinen.";s:2:"el";s:190:"Δημιουργεί έναν κατάλογο όλων των σελίδων και έναν χάρτη σελίδων σε μορφή XML για τις μηχανές αναζήτησης.";s:2:"es";s:111:"El módulo de mapa crea un índice de todas las páginas y un mapa del sitio XML para los motores de búsqueda.";s:2:"fa";s:150:"ماژول نقشه سایت یک لیست از همه ی صفحه ها به فرمت فایل XML برای موتور های جستجو می سازد";s:2:"fi";s:82:"sivukartta moduuli luo hakemisto kaikista sivuista ja XML sivukartta hakukoneille.";s:2:"fr";s:106:"Le module sitemap crée un index de toutes les pages et un plan de site XML pour les moteurs de recherche.";s:2:"id";s:110:"Modul peta situs ini membuat indeks dari setiap halaman dan sebuah format XML untuk mempermudah mesin pencari.";s:2:"it";s:104:"Il modulo mappa del sito crea un indice di tutte le pagine e una sitemap in XML per i motori di ricerca.";s:2:"lt";s:86:"struktūra modulis sukuria visų puslapių ir XML Sitemap paieškos sistemų indeksas.";s:2:"nl";s:89:"De sitemap module maakt een index van alle pagina''s en een XML sitemap voor zoekmachines.";s:2:"ru";s:144:"Карта модуль создает индекс всех страниц и карта сайта XML для поисковых систем.";s:2:"tw";s:84:"站點地圖模塊創建一個索引的所有網頁和XML網站地圖搜索引擎。";s:2:"cn";s:84:"站点地图模块创建一个索引的所有网页和XML网站地图搜索引擎。";s:2:"th";s:202:"โมดูลไซต์แมพสามารถสร้างดัชนีของหน้าเว็บทั้งหมดสำหรับเครื่องมือค้นหา.";s:2:"hu";s:94:"Ez a modul indexeli az összes oldalt és egy XML oldaltéképet generál a keresőmotoroknak.";s:2:"se";s:86:"Sajtkarta, modulen skapar ett index av alla sidor och en XML-sitemap för sökmotorer.";}', 0, 1, 0, 'content', 0, 1, 1, 1430891256),
(18, 'a:25:{s:2:"en";s:5:"Users";s:2:"ar";s:20:"المستخدمون";s:2:"br";s:9:"Usuários";s:2:"pt";s:12:"Utilizadores";s:2:"cs";s:11:"Uživatelé";s:2:"da";s:7:"Brugere";s:2:"de";s:8:"Benutzer";s:2:"el";s:14:"Χρήστες";s:2:"es";s:8:"Usuarios";s:2:"fa";s:14:"کاربران";s:2:"fi";s:12:"Käyttäjät";s:2:"fr";s:12:"Utilisateurs";s:2:"he";s:14:"משתמשים";s:2:"id";s:8:"Pengguna";s:2:"it";s:6:"Utenti";s:2:"lt";s:10:"Vartotojai";s:2:"nl";s:10:"Gebruikers";s:2:"pl";s:12:"Użytkownicy";s:2:"ru";s:24:"Пользователи";s:2:"sl";s:10:"Uporabniki";s:2:"tw";s:6:"用戶";s:2:"cn";s:6:"用户";s:2:"hu";s:14:"Felhasználók";s:2:"th";s:27:"ผู้ใช้งาน";s:2:"se";s:10:"Användare";}', 'users', '1.1.0', NULL, 'a:25:{s:2:"en";s:81:"Let users register and log in to the site, and manage them via the control panel.";s:2:"ar";s:133:"تمكين المستخدمين من التسجيل والدخول إلى الموقع، وإدارتهم من لوحة التحكم.";s:2:"br";s:125:"Permite com que usuários se registrem e entrem no site e também que eles sejam gerenciáveis apartir do painel de controle.";s:2:"pt";s:125:"Permite com que os utilizadores se registem e entrem no site e também que eles sejam geriveis apartir do painel de controlo.";s:2:"cs";s:103:"Umožňuje uživatelům se registrovat a přihlašovat a zároveň jejich správu v Kontrolním panelu.";s:2:"da";s:89:"Lader brugere registrere sig og logge ind på sitet, og håndtér dem via kontrolpanelet.";s:2:"de";s:108:"Erlaube Benutzern das Registrieren und Einloggen auf der Seite und verwalte sie über die Admin-Oberfläche.";s:2:"el";s:208:"Παρέχει λειτουργίες εγγραφής και σύνδεσης στους επισκέπτες. Επίσης από εδώ γίνεται η διαχείριση των λογαριασμών.";s:2:"es";s:138:"Permite el registro de nuevos usuarios quienes podrán loguearse en el sitio. Estos podrán controlarse desde el panel de administración.";s:2:"fa";s:151:"به کاربر ها امکان ثبت نام و لاگین در سایت را بدهید و آنها را در پنل مدیریت نظارت کنید";s:2:"fi";s:126:"Antaa käyttäjien rekisteröityä ja kirjautua sisään sivustolle sekä mahdollistaa niiden muokkaamisen hallintapaneelista.";s:2:"fr";s:112:"Permet aux utilisateurs de s''enregistrer et de se connecter au site et de les gérer via le panneau de contrôle";s:2:"he";s:62:"ניהול משתמשים: רישום, הפעלה ומחיקה";s:2:"id";s:102:"Memungkinkan pengguna untuk mendaftar dan masuk ke dalam situs, dan mengaturnya melalui control panel.";s:2:"it";s:95:"Fai iscrivere de entrare nel sito gli utenti, e gestiscili attraverso il pannello di controllo.";s:2:"lt";s:106:"Leidžia vartotojams registruotis ir prisijungti prie puslapio, ir valdyti juos per administravimo panele.";s:2:"nl";s:88:"Laat gebruikers registreren en inloggen op de site, en beheer ze via het controlepaneel.";s:2:"pl";s:87:"Pozwól użytkownikom na logowanie się na stronie i zarządzaj nimi za pomocą panelu.";s:2:"ru";s:155:"Управление зарегистрированными пользователями, активирование новых пользователей.";s:2:"sl";s:96:"Dovoli uporabnikom za registracijo in prijavo na strani, urejanje le teh preko nadzorne plošče";s:2:"tw";s:87:"讓用戶可以註冊並登入網站，並且管理者可在控制台內進行管理。";s:2:"cn";s:87:"让用户可以注册并登入网站，并且管理者可在控制台内进行管理。";s:2:"th";s:210:"ให้ผู้ใช้ลงทะเบียนและเข้าสู่เว็บไซต์และจัดการกับพวกเขาผ่านทางแผงควบคุม";s:2:"hu";s:120:"Hogy a felhasználók tudjanak az oldalra regisztrálni és belépni, valamint lehessen őket kezelni a vezérlőpulton.";s:2:"se";s:111:"Låt dina besökare registrera sig och logga in på webbplatsen. Hantera sedan användarna via kontrollpanelen.";}', 0, 0, 1, '0', 1, 1, 1, 1430891256),
(19, 'a:25:{s:2:"en";s:9:"Variables";s:2:"ar";s:20:"المتغيّرات";s:2:"br";s:10:"Variáveis";s:2:"pt";s:10:"Variáveis";s:2:"cs";s:10:"Proměnné";s:2:"da";s:8:"Variable";s:2:"de";s:9:"Variablen";s:2:"el";s:20:"Μεταβλητές";s:2:"es";s:9:"Variables";s:2:"fa";s:16:"متغییرها";s:2:"fi";s:9:"Muuttujat";s:2:"fr";s:9:"Variables";s:2:"he";s:12:"משתנים";s:2:"id";s:8:"Variabel";s:2:"it";s:9:"Variabili";s:2:"lt";s:10:"Kintamieji";s:2:"nl";s:10:"Variabelen";s:2:"pl";s:7:"Zmienne";s:2:"ru";s:20:"Переменные";s:2:"sl";s:13:"Spremenljivke";s:2:"tw";s:12:"系統變數";s:2:"cn";s:12:"系统变数";s:2:"th";s:18:"ตัวแปร";s:2:"se";s:9:"Variabler";s:2:"hu";s:10:"Változók";}', 'variables', '1.0.0', NULL, 'a:25:{s:2:"en";s:59:"Manage global variables that can be accessed from anywhere.";s:2:"ar";s:97:"إدارة المُتغيّرات العامة لاستخدامها في أرجاء الموقع.";s:2:"br";s:61:"Gerencia as variáveis globais acessíveis de qualquer lugar.";s:2:"pt";s:58:"Gerir as variáveis globais acessíveis de qualquer lugar.";s:2:"cs";s:56:"Spravujte globální proměnné přístupné odkudkoliv.";s:2:"da";s:51:"Håndtér globale variable som kan tilgås overalt.";s:2:"de";s:74:"Verwaltet globale Variablen, auf die von überall zugegriffen werden kann.";s:2:"el";s:129:"Διαχείριση μεταβλητών που είναι προσβάσιμες από παντού στον ιστότοπο.";s:2:"es";s:50:"Manage global variables to access from everywhere.";s:2:"fa";s:136:"مدیریت متغییر های جامع که می توانند در هر جای سایت مورد استفاده قرار بگیرند";s:2:"fi";s:66:"Hallitse globaali muuttujia, joihin pääsee käsiksi mistä vain.";s:2:"fr";s:92:"Gérer des variables globales pour pouvoir y accéder depuis n''importe quel endroit du site.";s:2:"he";s:96:"ניהול משתנים גלובליים אשר ניתנים להמרה בכל חלקי האתר";s:2:"id";s:59:"Mengatur variabel global yang dapat diakses dari mana saja.";s:2:"it";s:58:"Gestisci le variabili globali per accedervi da ogni parte.";s:2:"lt";s:64:"Globalių kintamujų tvarkymas kurie yra pasiekiami iš bet kur.";s:2:"nl";s:54:"Beheer globale variabelen die overal beschikbaar zijn.";s:2:"pl";s:86:"Zarządzaj globalnymi zmiennymi do których masz dostęp z każdego miejsca aplikacji.";s:2:"ru";s:136:"Управление глобальными переменными, которые доступны в любом месте сайта.";s:2:"sl";s:53:"Urejanje globalnih spremenljivk za dostop od kjerkoli";s:2:"th";s:148:"จัดการตัวแปรทั่วไปโดยที่สามารถเข้าถึงได้จากทุกที่.";s:2:"tw";s:45:"管理此網站內可存取的全局變數。";s:2:"cn";s:45:"管理此网站内可存取的全局变数。";s:2:"hu";s:62:"Globális változók kezelése a hozzáféréshez, bárhonnan.";s:2:"se";s:66:"Hantera globala variabler som kan avändas över hela webbplatsen.";}', 0, 0, 1, 'data', 1, 1, 1, 1430891256),
(20, 'a:23:{s:2:"en";s:7:"Widgets";s:2:"br";s:7:"Widgets";s:2:"pt";s:7:"Widgets";s:2:"cs";s:7:"Widgety";s:2:"da";s:7:"Widgets";s:2:"de";s:7:"Widgets";s:2:"el";s:7:"Widgets";s:2:"es";s:7:"Widgets";s:2:"fa";s:13:"ویجت ها";s:2:"fi";s:9:"Vimpaimet";s:2:"fr";s:7:"Widgets";s:2:"id";s:6:"Widget";s:2:"it";s:7:"Widgets";s:2:"lt";s:11:"Papildiniai";s:2:"nl";s:7:"Widgets";s:2:"ru";s:14:"Виджеты";s:2:"sl";s:9:"Vtičniki";s:2:"tw";s:9:"小組件";s:2:"cn";s:9:"小组件";s:2:"hu";s:9:"Widget-ek";s:2:"th";s:21:"วิดเจ็ต";s:2:"se";s:8:"Widgetar";s:2:"ar";s:14:"الودجتس";}', 'widgets', '1.2.0', NULL, 'a:23:{s:2:"en";s:69:"Manage small sections of self-contained logic in blocks or "Widgets".";s:2:"ar";s:132:"إدارة أقسام صغيرة من البرمجيات في مساحات الموقع أو ما يُسمّى بالـ"ودجتس".";s:2:"br";s:77:"Gerenciar pequenas seções de conteúdos em bloco conhecidos como "Widgets".";s:2:"pt";s:74:"Gerir pequenas secções de conteúdos em bloco conhecidos como "Widgets".";s:2:"cs";s:56:"Spravujte malé funkční části webu neboli "Widgety".";s:2:"da";s:74:"Håndter små sektioner af selv-opretholdt logik i blokke eller "Widgets".";s:2:"de";s:62:"Verwaltet kleine, eigentständige Bereiche, genannt "Widgets".";s:2:"el";s:149:"Διαχείριση μικρών τμημάτων αυτόνομης προγραμματιστικής λογικής σε πεδία ή "Widgets".";s:2:"es";s:75:"Manejar pequeñas secciones de lógica autocontenida en bloques o "Widgets"";s:2:"fa";s:76:"مدیریت قسمت های کوچکی از سایت به طور مستقل";s:2:"fi";s:81:"Hallitse pieniä osioita, jotka sisältävät erillisiä lohkoja tai "Vimpaimia".";s:2:"fr";s:41:"Gérer des mini application ou "Widgets".";s:2:"id";s:101:"Mengatur bagian-bagian kecil dari blok-blok yang memuat sesuatu atau dikenal dengan istilah "Widget".";s:2:"it";s:70:"Gestisci piccole sezioni di logica a se stante in blocchi o "Widgets".";s:2:"lt";s:43:"Nedidelių, savarankiškų blokų valdymas.";s:2:"nl";s:75:"Beheer kleine onderdelen die zelfstandige logica bevatten, ofwel "Widgets".";s:2:"ru";s:91:"Управление небольшими, самостоятельными блоками.";s:2:"sl";s:61:"Urejanje manjših delov blokov strani ti. Vtičniki (Widgets)";s:2:"tw";s:103:"可將小段的程式碼透過小組件來管理。即所謂 "Widgets"，或稱為小工具、部件。";s:2:"cn";s:103:"可将小段的程式码透过小组件来管理。即所谓 "Widgets"，或称为小工具、部件。";s:2:"hu";s:56:"Önálló kis logikai tömbök vagy widget-ek kezelése.";s:2:"th";s:152:"จัดการส่วนเล็ก ๆ ในรูปแบบของตัวเองในบล็อกหรือวิดเจ็ต";s:2:"se";s:83:"Hantera små sektioner med egen logik och innehåll på olika delar av webbplatsen.";}', 1, 0, 1, 'content', 1, 1, 1, 1430891256);
INSERT INTO `default_modules` (`id`, `name`, `slug`, `version`, `type`, `description`, `skip_xss`, `is_frontend`, `is_backend`, `menu`, `enabled`, `installed`, `is_core`, `updated_on`) VALUES
(21, 'a:10:{s:2:"en";s:7:"WYSIWYG";s:2:"br";s:7:"WYSIWYG";s:2:"fa";s:7:"WYSIWYG";s:2:"fr";s:7:"WYSIWYG";s:2:"pt";s:7:"WYSIWYG";s:2:"se";s:15:"HTML-redigerare";s:2:"tw";s:7:"WYSIWYG";s:2:"cn";s:7:"WYSIWYG";s:2:"ar";s:27:"المحرر الرسومي";s:2:"it";s:7:"WYSIWYG";}', 'wysiwyg', '1.0.0', NULL, 'a:11:{s:2:"en";s:60:"Provides the WYSIWYG editor for PyroCMS powered by CKEditor.";s:2:"br";s:64:"Provém o editor WYSIWYG para o PyroCMS fornecido pelo CKEditor.";s:2:"fa";s:73:"ویرایشگر WYSIWYG که توسطCKEditor ارائه شده است. ";s:2:"fr";s:63:"Fournit un éditeur WYSIWYG pour PyroCMS propulsé par CKEditor";s:2:"pt";s:61:"Fornece o editor WYSIWYG para o PyroCMS, powered by CKEditor.";s:2:"el";s:113:"Παρέχει τον επεξεργαστή WYSIWYG για το PyroCMS, χρησιμοποιεί το CKEDitor.";s:2:"se";s:37:"Redigeringsmodul för HTML, CKEditor.";s:2:"tw";s:83:"提供 PyroCMS 所見即所得（WYSIWYG）編輯器，由 CKEditor 技術提供。";s:2:"cn";s:83:"提供 PyroCMS 所见即所得（WYSIWYG）编辑器，由 CKEditor 技术提供。";s:2:"ar";s:76:"توفر المُحرّر الرسومي لـPyroCMS من خلال CKEditor.";s:2:"it";s:57:"Fornisce l''editor WYSIWYG per PtroCMS creato con CKEditor";}', 0, 0, 0, '0', 1, 1, 1, 1430891256),
(22, 'a:1:{s:2:"en";s:9:"Instagram";}', 'instagram_api', '2.2.5', NULL, 'a:1:{s:2:"en";s:9:"Instagram";}', 1, 1, 1, 'content', 1, 1, 0, 1465265833),
(23, 'a:2:{s:2:"en";s:4:"News";s:2:"id";s:6:"Berita";}', 'news', '2.3', NULL, 'a:2:{s:2:"en";s:18:"Post news entries.";s:2:"id";s:12:"Modul berita";}', 1, 1, 1, 'content', 1, 1, 0, 1463553289),
(28, 'a:1:{s:2:"en";s:6:"Events";}', 'ctd', '2.2.0', NULL, 'a:1:{s:2:"en";s:38:"Manage your Coral Triangle Day events.";}', 0, 1, 1, 'content', 1, 1, 0, 1463553289),
(25, 'a:2:{s:2:"en";s:9:"Galleries";s:2:"id";s:6:"Galeri";}', 'galleries', '2.00', NULL, 'a:2:{s:2:"en";s:23:"Manage media galleries.";s:2:"id";s:27:"Pengaturan galleries media.";}', 1, 1, 1, 'content', 1, 1, 0, 1463553289),
(26, 'a:1:{s:2:"en";s:8:"Sponsors";}', 'sponsors', '2.0', NULL, 'a:1:{s:2:"en";s:21:"Manage your sponsors.";}', 0, 1, 1, 'content', 0, 0, 0, 1431993951);

-- --------------------------------------------------------

--
-- Table structure for table `default_navigation_groups`
--

CREATE TABLE IF NOT EXISTS `default_navigation_groups` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `abbrev` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_navigation_groups`
--

INSERT INTO `default_navigation_groups` (`id`, `title`, `abbrev`) VALUES
(1, 'Header', 'header'),
(2, 'Sidebar', 'sidebar'),
(3, 'Footer', 'footer');

-- --------------------------------------------------------

--
-- Table structure for table `default_navigation_links`
--

CREATE TABLE IF NOT EXISTS `default_navigation_links` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` int(11) DEFAULT NULL,
  `link_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uri',
  `page_id` int(11) DEFAULT NULL,
  `module_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `navigation_group_id` int(5) NOT NULL DEFAULT '0',
  `position` int(5) NOT NULL DEFAULT '0',
  `target` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `restricted_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_navigation_links`
--

INSERT INTO `default_navigation_links` (`id`, `title`, `parent`, `link_type`, `page_id`, `module_name`, `url`, `uri`, `navigation_group_id`, `position`, `target`, `restricted_to`, `class`) VALUES
(6, 'About', 0, 'page', 9, '', '', '', 1, 1, '', '0', ''),
(4, 'Instagram contest', 0, 'page', 7, '', '', '', 1, 2, '', '0', ''),
(7, 'Gallery', 0, 'url', 0, '', 'https://www.flickr.com/photos/97485135@N06/collections', '', 1, 4, '_blank', '0', ''),
(10, 'Submit Photos', 0, 'uri', 0, '', '', 'submit-photos', 1, 3, '', '0', ''),
(11, 'Home', 0, 'uri', 0, '', '', 'home', 1, 0, '', '0', ''),
(12, '2015', 4, 'page', 12, '', '', '', 1, 2, '', '0', ''),
(13, '2016', 4, 'page', 13, '', '', '', 1, 1, '', '0', ''),
(14, '2017', 4, 'page', 14, '', '', '', 1, 0, '', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `default_news`
--

CREATE TABLE IF NOT EXISTS `default_news` (
  `id` int(11) NOT NULL,
  `news_year` year(4) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title_size` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_color` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lead` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `intro` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `parsed` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `author_id` int(11) NOT NULL DEFAULT '0',
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL DEFAULT '0',
  `comments_enabled` enum('no','1 day','1 week','2 weeks','1 month','3 months','always') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'always',
  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `type` set('html','markdown','wysiwyg-advanced','wysiwyg-simple') COLLATE utf8_unicode_ci NOT NULL,
  `preview_hash` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `credits` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `headline` int(1) NOT NULL DEFAULT '1',
  `page_format` varchar(12) COLLATE utf8_unicode_ci DEFAULT '1_column',
  `optional_images` text COLLATE utf8_unicode_ci,
  `news_quote` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `related_news` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_url` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_news`
--

INSERT INTO `default_news` (`id`, `news_year`, `title`, `title_size`, `title_color`, `subtitle`, `slug`, `category_id`, `attachment`, `lead`, `intro`, `body`, `parsed`, `keywords`, `author_id`, `created_on`, `updated_on`, `comments_enabled`, `status`, `type`, `preview_hash`, `viewed`, `credits`, `headline`, `page_format`, `optional_images`, `news_quote`, `related_news`, `external_url`) VALUES
(10, 2015, 'Coral Triangle Instagram contest promotes love for our ocean', NULL, NULL, NULL, 'coral-triangle-instagram-contest-promotes-love-for-our-ocean', 0, '', NULL, '', '', '', '', 1, 1433916720, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/what_we_do/where_we_work/coraltriangle/news/?247487/Coral-Triangle-Instagram-contest-promotes-love-for-our-ocean'),
(11, 2015, 'Ini Cara &quot;Biker&quot; Sulut Melestarikan Lingkungan Pesisir', NULL, NULL, NULL, 'ini-cara-biker-sulut-melestarikan-lingkungan-pesisir', 0, '', NULL, '', '', '', '', 1, 1433916840, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://regional.kompas.com/read/2015/06/09/20110501/Ini.Cara.Biker.Sulut.Melestarikan.Lingkungan.Pesisir'),
(12, 2015, 'Cleaner Shorelines Aimed for Environment Day', NULL, NULL, NULL, 'cleaner-shorelines-aimed-for-environment-day', 0, '', NULL, '', '', '', '', 1, 1433916840, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.solomontimes.com/news/cleaner-shorelines-aimed-for-environment-day/8404'),
(13, 2015, 'Arakan Jadi Tuan Rumah Peringatan CTI', NULL, NULL, NULL, 'arakan-jadi-tuan-rumah-peringatan-cti', 0, '', NULL, '', '', '', '', 1, 1433916900, 1433916931, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://manado.tribunnews.com/2015/06/08/arakan-jadi-tuan-rumah-peringatan-cti'),
(14, 2015, 'Coral Triangle Day 2015', NULL, NULL, NULL, 'coral-triangle-day-2015', 0, '', NULL, '', '', '', '', 1, 1433916900, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.bmb.gov.ph/index.php/mainmenu-news-events/mainmenu-news/637-coral-triangle-day-2015'),
(15, 2015, 'Celebrate Coral Triangle Day 2015', NULL, NULL, NULL, 'celebrate-coral-triangle-day-2015', 0, '', NULL, '', '', '', '', 1, 1433916900, 1433917031, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.ctknetwork.org/newsroom/celebrate-coral-triangle-day-2015/'),
(17, 2014, 'Cebu joins Coral Triangle Initiative', NULL, NULL, NULL, 'cebu-joins-coral-triangle-initiative', 0, '', NULL, '', '', '', '', 1, 1402318620, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://cebudailynews.inquirer.net/2014/05/17/cebu-joins-coral-triangle-initiative/'),
(18, 2014, 'Preserving the rich marine biodiversity of Asia''s Coral Triangle', NULL, NULL, NULL, 'preserving-the-rich-marine-biodiversity-of-asias-coral-triangle', 0, '', NULL, '', '', '', '', 1, 1402318680, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.adb.org/features/preserving-rich-marine-biodiversity-asia-s-coral-triangle'),
(19, 2014, 'Cebu to mark Coral Triangle Day', NULL, NULL, NULL, 'cebu-to-mark-coral-triangle-day', 0, '', NULL, '', '', '', '', 1, 1402318740, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.philstar.com/cebu-news/2014/05/19/1324826/cebu-mark-coral-triangle-day'),
(20, 2014, 'Collective power of consumers &amp;amp; businesses: Key to drive sustainable seafood movement', NULL, NULL, NULL, 'collective-power-of-consumers-andamp-businesses-key-to-drive-sustainable-seafood-movement', 0, '', NULL, '', '', '', '', 1, 1402318740, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/what_we_do/where_we_work/coraltriangle/news/?222910/Collective-power-of-consumers--businesses-Key-to-drive-sustainable-seafood-movement'),
(21, 2014, 'Coral Triangle Day highlights need to protect ocean for food and livelihood', NULL, NULL, NULL, 'coral-triangle-day-highlights-need-to-protect-ocean-for-food-and-livelihood', 0, '', NULL, '', '', '', '', 1, 1402318800, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/what_we_do/where_we_work/coraltriangle/news/?222870/Coral-Triangle-Day-highlights-need-to-protect-ocean-for-food-and-livelihood'),
(22, 2014, 'CTI-CFF Interim Regional Secretariat''s Coral Triangle Day 2014 message', NULL, NULL, NULL, 'cticff-interim-regional-secretariats-coral-triangle-day-2014-message', 0, '', NULL, '', '', '', '', 1, 1402318800, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://coraltriangleinitiative.org/news/cti-cff-interim-regional-secretariats-coral-triangle-day-2014-message'),
(23, 2014, 'Philippine province prepares to mark Coral Triangle Day', NULL, NULL, NULL, 'philippine-province-prepares-to-mark-coral-triangle-day', 0, '', NULL, '', '', '', '', 1, 1402318800, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://coraltriangleinitiative.org/news/philippine-province-prepares-mark-coral-triangle-day'),
(24, 2014, 'Website to help fund climate adaptation projects in the Coral Triangle launched', NULL, NULL, NULL, 'website-to-help-fund-climate-adaptation-projects-in-the-coral-triangle-launched', 0, '', NULL, '', '', '', '', 1, 1402318860, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/what_we_do/where_we_work/coraltriangle/news/?223130/Website-to-help-fund-climate-adaptation-projects-in-the-Coral-Triangle-launched'),
(25, 2014, 'International chef Bobby Chinn promotes sustainable seafood in the Philippines', NULL, NULL, NULL, 'international-chef-bobby-chinn-promotes-sustainable-seafood-in-the-philippines', 0, '', NULL, '', '', '', '', 1, 1402318860, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/what_we_do/where_we_work/coraltriangle/news/?222950/International-chef-Bobby-Chinn-promotes-sustainable-seafood-in-the-Philippines'),
(26, 2014, 'Images of Coral Triangle Day 2014: Local governments speak up', NULL, NULL, NULL, 'images-of-coral-triangle-day-2014-local-governments-speak-up', 0, '', NULL, '', '', '', '', 1, 1402318860, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.coraltriangleinitiative.org/news/images-coral-triangle-day-2014-local-governments-speak'),
(27, 2016, 'WWF joins partners in #noplace4plastic Coral Triangle Day campaign', NULL, NULL, NULL, 'wwf-joins-partners-in-noplace4plastic-coral-triangle-day-campaign', 0, '', NULL, '', '', '', '', 1, 1463635140, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/wwf_news/?267970/WWF-joins-partners-in-noplace4plastic-Coral-Triangle-Day-campaign'),
(28, 2016, 'Coral Triangle Day Appeals More Attention and Real Action on the Real Threat: Plastic Pollution', NULL, NULL, NULL, 'coral-triangle-day-appeals-more-attention-and-real-action-on-the-real-threat-plastic-pollution', 0, '', NULL, '', '', '', '', 1, 1463752320, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://coraltriangleinitiative.org/news/coral-triangle-day-appeals-more-attention-and-real-action-real-threat-plastic-pollution'),
(29, 2016, 'Let''s keep plastic out of the Coral Triangle', NULL, NULL, NULL, 'lets-keep-plastic-out-of-the-coral-triangle', 0, '', NULL, '', '', '', '', 1, 1465460280, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://blogs.adb.org/blog/lets-keep-plastic-out-coral-triangle-0'),
(30, 2017, 'Coral Triangle Day Celebration to Zoom into Plastic-Free Lifestyle to Reduce Marine Pollution', NULL, NULL, NULL, 'coral-triangle-day-celebration-to-zoom-into-plasticfree-lifestyle-to-reduce-marine-pollution', 0, '', NULL, '', '', '', '', 2, 1494352740, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://coraltriangleinitiative.org/news/coral-triangle-day-celebration-zoom-plastic-free-lifestyle-reduce-marine-pollution'),
(31, 2017, '#SayNo2Plastic on Coral Triangle Day, June 9', NULL, NULL, NULL, 'sayno2plastic-on-coral-triangle-day-june-9', 0, '', NULL, '', '', '', '', 2, 1495176360, 1495176439, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/what_we_do/where_we_work/coraltriangle/news/?300370/SayNo2Plastic-on-Coral-Triangle-Day-June-9'),
(32, 2017, 'Solomon''s Province to Ban Plastic Bags', NULL, NULL, NULL, 'solomons-province-to-ban-plastic-bags', 0, '', NULL, '', '', '', '', 2, 1496325240, 1496325399, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.radionz.co.nz/international/pacific-news/332061/solomon-s-province-to-ban-plastic-bags'),
(33, 2017, 'Western Province to Ban Plastic Bags', NULL, NULL, NULL, 'western-province-to-ban-plastic-bags', 0, '', NULL, '', '', '', '', 2, 1495547700, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://theislandsun.com/western-province-to-ban-plastic-bags/'),
(34, 2017, 'Plastic-bag ban to benefit Solomons'' tourism', NULL, NULL, NULL, 'plasticbag-ban-to-benefit-solomons-tourism', 0, '', NULL, '', '', '', '', 2, 1496749980, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.radionz.co.nz/international/pacific-news/332369/plastic-bag-ban-to-benefit-solomons-tourism'),
(35, 2017, 'Asian nations make plastic oceans promise', NULL, NULL, NULL, 'asian-nations-make-plastic-oceans-promise', 0, '', NULL, '', '', '', '', 2, 1496917560, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://www.bbc.com/news/science-environment-40195664#'),
(36, 2017, 'Coral Triangle Day encourages collective action to tackle ocean plastic pollution', NULL, NULL, NULL, 'coral-triangle-day-encourages-collective-action-to-tackle-ocean-plastic-pollution', 0, '', NULL, '', '', '', '', 2, 1497004020, 1497004164, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://wwf.panda.org/what_we_do/where_we_work/coraltriangle/news/?302230/Coral-Triangle-Day-encourages-collective-action-to-tackle-ocean-plastic-pollution'),
(37, 2017, '‘No Place for Plastics’ for Coral Triangle Day today', NULL, NULL, NULL, 'no-place-for-plastics-for-coral-triangle-day-today', 0, '', NULL, '', '', '', '', 2, 1497007500, 1497353215, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://theislandsun.com/no-place-for-plastics-for-coral-triangle-day-today/'),
(38, 2017, 'WWF Acknowledges Coral Triangle Participants', NULL, NULL, NULL, 'wwf-acknowledges-coral-triangle-participants', 0, '', NULL, '', '', '', '', 2, 1497864960, 0, 'no', 'live', 'wysiwyg-advanced', '', 0, '', 1, NULL, NULL, NULL, NULL, 'http://theislandsun.com/wwf-acknowledges-coral-triangle-participants/');

-- --------------------------------------------------------

--
-- Table structure for table `default_news_categories`
--

CREATE TABLE IF NOT EXISTS `default_news_categories` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_news_images`
--

CREATE TABLE IF NOT EXISTS `default_news_images` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_pages`
--

CREATE TABLE IF NOT EXISTS `default_pages` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uri` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `css` text COLLATE utf8_unicode_ci,
  `js` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_robots_no_index` tinyint(1) DEFAULT NULL,
  `meta_robots_no_follow` tinyint(1) DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `rss_enabled` int(1) NOT NULL DEFAULT '0',
  `comments_enabled` int(1) NOT NULL DEFAULT '0',
  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `created_on` int(11) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0',
  `restricted_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_home` int(1) NOT NULL DEFAULT '0',
  `strict_uri` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_pages`
--

INSERT INTO `default_pages` (`id`, `slug`, `class`, `title`, `uri`, `parent_id`, `type_id`, `entry_id`, `css`, `js`, `meta_title`, `meta_keywords`, `meta_robots_no_index`, `meta_robots_no_follow`, `meta_description`, `rss_enabled`, `comments_enabled`, `status`, `created_on`, `updated_on`, `restricted_to`, `is_home`, `strict_uri`, `order`) VALUES
(1, 'home', '', 'Home', 'home', 0, '1', '1', '', '', 'Home', '4a02c463fa5faee5f39388d483971429', 1, 1, 'Join us as we celebrate the Coral Triangle! On June 9th, get involved in events to celebrate the richest marine environment on the planet', 0, 0, 'live', 1430889572, 1432280160, '0', 1, 1, 0),
(2, 'contact', '', 'Contact', 'contact', 0, '1', '2', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 'live', 1430889572, 0, '', 0, 1, 5),
(3, 'search', '', 'Search', 'search', 0, '1', '3', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 'live', 1430889572, 0, '', 0, 1, 6),
(4, 'results', '', 'Results', 'search/results', 3, '1', '4', NULL, NULL, NULL, '', NULL, NULL, NULL, 0, 0, 'live', 1430889572, 0, '', 0, 0, 0),
(5, '404', '', 'Page missing', '404', 0, '1', '5', '', '', '', '', 0, 0, '', 0, 0, 'live', 1430889572, 1432865532, '0', 0, 1, 7),
(6, 'instagram-feeds', '', 'Instagram Feeds', 'instagram-feeds', 0, '1', '6', '', '', '', '', 0, 0, '', 0, 0, 'live', 1431471976, 1431506429, '0', 0, 1, 8),
(7, 'contest', '', 'Instagram Contest', 'contest', 0, '1', '7', '', '', 'Instagram contest', '9523b3fd881544157d537e8875c9e7c4', 0, 0, 'Fancy an escape to an eco-resort or a stunning underwater photo book?  Win a prize by joining this fun and easy Instagram competition,  and get creative with nature.', 0, 0, 'live', 1431475448, 1432280236, '0', 0, 1, 3),
(9, 'about', '', 'About', 'about', 0, '1', '9', '', '', 'About', 'e2fe1951a9f32516f26940d084f37997', 0, 0, 'The Coral Triangle is the underwater equivalent of the Amazon, with more marine species than anywhere else on the planet. Find out more', 0, 0, 'live', 1431647334, 1495123996, '0', 0, 1, 1),
(11, 'submit-an-event', '', 'Submit an Event', 'submit-an-event', 0, '1', '11', '', '', 'Submit your event', '', 0, 0, '', 0, 0, 'live', 1431649482, 1432280270, '0', 0, 1, 2),
(10, 'event-gallery', '', 'Event Gallery', 'event-gallery', 0, '1', '10', '', '', '', '', 0, 0, '', 0, 0, 'live', 1431647408, 1495095492, '0', 0, 1, 4),
(12, '2015', '', '2015', 'contest/2015', 7, '1', '12', '', '', '', '', 0, 0, '', 0, 0, 'live', 1495095381, 0, '0', 0, 1, 2),
(13, '2016', '', '2016', 'contest/2016', 7, '1', '13', '', '', '', '', 0, 0, '', 0, 0, 'live', 1495095397, 0, '0', 0, 1, 1),
(14, '2017', '', '2017', 'contest/2017', 7, '1', '14', '', '', '', '', 0, 0, '', 0, 0, 'live', 1495095411, 0, '0', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `default_page_types`
--

CREATE TABLE IF NOT EXISTS `default_page_types` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `stream_id` int(11) NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `css` text COLLATE utf8_unicode_ci,
  `js` text COLLATE utf8_unicode_ci,
  `theme_layout` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `updated_on` int(11) NOT NULL,
  `save_as_files` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `content_label` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_label` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_page_types`
--

INSERT INTO `default_page_types` (`id`, `slug`, `title`, `description`, `stream_id`, `meta_title`, `meta_keywords`, `meta_description`, `body`, `css`, `js`, `theme_layout`, `updated_on`, `save_as_files`, `content_label`, `title_label`) VALUES
(1, 'default', 'Default', 'A simple page type with a WYSIWYG editor that will get you started adding content.', 2, NULL, NULL, NULL, '<h2>{{ page:title }}</h2>\n\n{{ body }}', '', '', 'default', 1430889572, 'n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_permissions`
--

CREATE TABLE IF NOT EXISTS `default_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roles` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_profiles`
--

CREATE TABLE IF NOT EXISTS `default_profiles` (
  `id` int(9) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `ordering_count` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `display_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `bio` text COLLATE utf8_unicode_ci,
  `dob` int(11) DEFAULT NULL,
  `gender` set('m','f','') COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` int(11) unsigned DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_profiles`
--

INSERT INTO `default_profiles` (`id`, `created`, `updated`, `created_by`, `ordering_count`, `user_id`, `display_name`, `first_name`, `last_name`, `company`, `lang`, `bio`, `dob`, `gender`, `phone`, `mobile`, `address_line1`, `address_line2`, `address_line3`, `postcode`, `website`, `updated_on`) VALUES
(1, NULL, NULL, NULL, NULL, 1, 'Okky Sari', 'Okky', 'Sari', '', 'en', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2016-05-17 23:42:53', NULL, 1, 1, 2, 'arifin', 'arifin', 'melianto', 'catalyze', 'en', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '2018-03-15 22:30:04', NULL, 2, 2, 3, 'andiewibi', 'andiewibi', 'andiewibi', NULL, 'en', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `default_redirects`
--

CREATE TABLE IF NOT EXISTS `default_redirects` (
  `id` int(11) NOT NULL,
  `from` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `to` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(3) NOT NULL DEFAULT '302'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_redirects`
--

INSERT INTO `default_redirects` (`id`, `from`, `to`, `type`) VALUES
(1, 'ctd/submit-photos', 'submit-photos', 301),
(2, 'ctd/get_past_data', 'past-coral-triangle-days', 302),
(3, 'past-coral-triangle-days', 'past-coral-triangle-days/2015', 302),
(4, 'contest', 'contest/2017', 302);

-- --------------------------------------------------------

--
-- Table structure for table `default_search_index`
--

CREATE TABLE IF NOT EXISTS `default_search_index` (
  `id` int(10) unsigned NOT NULL,
  `title` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `keywords` text COLLATE utf8_unicode_ci,
  `keyword_hash` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_plural` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_edit_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_delete_uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_search_index`
--

INSERT INTO `default_search_index` (`id`, `title`, `description`, `keywords`, `keyword_hash`, `module`, `entry_key`, `entry_plural`, `entry_id`, `uri`, `cp_edit_uri`, `cp_delete_uri`) VALUES
(6, 'Home', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '1', 'home', 'admin/pages/edit/1', 'admin/pages/delete/1'),
(2, 'Contact', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '2', 'contact', 'admin/pages/edit/2', 'admin/pages/delete/2'),
(3, 'Search', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '3', 'search', 'admin/pages/edit/3', 'admin/pages/delete/3'),
(4, 'Results', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '4', 'search/results', 'admin/pages/edit/4', 'admin/pages/delete/4'),
(5, 'Page missing', '', NULL, NULL, 'pages', 'pages:page', 'pages:pages', '5', '404', 'admin/pages/edit/5', 'admin/pages/delete/5');

-- --------------------------------------------------------

--
-- Table structure for table `default_settings`
--

CREATE TABLE IF NOT EXISTS `default_settings` (
  `slug` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` set('text','textarea','password','select','select-multiple','radio','checkbox') COLLATE utf8_unicode_ci NOT NULL,
  `default` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `is_required` int(1) NOT NULL,
  `is_gui` int(1) NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_settings`
--

INSERT INTO `default_settings` (`slug`, `title`, `description`, `type`, `default`, `value`, `options`, `is_required`, `is_gui`, `module`, `order`) VALUES
('site_name', 'Site Name', 'The name of the website for page titles and for use around the site.', 'text', 'Un-named Website', 'Coral Triangle Day', '', 1, 1, '', 1000),
('site_slogan', 'Site Slogan', 'The slogan of the website for page titles and for use around the site', 'text', '', 'Amazon of the Oceans', '', 0, 1, '', 999),
('meta_topic', 'Meta Topic', 'Two or three words describing this type of company/website.', 'text', 'Content Management', 'Amazon of the Oceans', '', 0, 1, '', 998),
('site_lang', 'Site Language', 'The native language of the website, used to choose templates of e-mail notifications, contact form, and other features that should not depend on the language of a user.', 'select', 'en', 'en', 'func:get_supported_lang', 1, 1, '', 997),
('site_public_lang', 'Public Languages', 'Which are the languages really supported and offered on the front-end of your website?', 'checkbox', 'en', 'en', 'func:get_supported_lang', 1, 1, '', 996),
('date_format', 'Date Format', 'How should dates be displayed across the website and control panel? Using the <a target="_blank" href="http://php.net/manual/en/function.date.php">date format</a> from PHP - OR - Using the format of <a target="_blank" href="http://php.net/manual/en/function.strftime.php">strings formatted as date</a> from PHP.', 'text', 'F j, Y', '', '', 1, 1, '', 995),
('currency', 'Currency', 'The currency symbol for use on products, services, etc.', 'text', '&pound;', '', '', 1, 1, '', 994),
('records_per_page', 'Records Per Page', 'How many records should we show per page in the admin section?', 'select', '25', '', '10=10|25=25|50=50|100=100', 1, 1, '', 992),
('rss_feed_items', 'Feed item count', 'How many items should we show in RSS/blog feeds?', 'select', '25', '', '10=10|25=25|50=50|100=100', 1, 1, '', 991),
('dashboard_rss', 'Dashboard RSS Feed', 'Link to an RSS feed that will be displayed on the dashboard.', 'text', 'https://www.pyrocms.com/blog/rss/all.rss', '', '', 0, 1, '', 990),
('dashboard_rss_count', 'Dashboard RSS Items', 'How many RSS items would you like to display on the dashboard?', 'text', '5', '5', '', 1, 1, '', 989),
('frontend_enabled', 'Site Status', 'Use this option to the user-facing part of the site on or off. Useful when you want to take the site down for maintenance.', 'radio', '1', '', '1=Open|0=Closed', 1, 1, '', 988),
('unavailable_message', 'Unavailable Message', 'When the site is turned off or there is a major problem, this message will show to users.', 'textarea', 'Sorry, this website is currently unavailable.', '', '', 0, 1, '', 987),
('ga_tracking', 'Google Tracking Code', 'Enter your Google Analytic Tracking Code to activate Google Analytics view data capturing. E.g: UA-19483569-6', 'text', '', 'UA-63282926-1', '', 0, 1, 'integration', 985),
('ga_profile', 'Google Analytic Profile ID', 'Profile ID for this website in Google Analytics', 'text', '', '102735147', '', 0, 1, 'integration', 984),
('ga_email', 'Google Analytic E-mail', 'E-mail address used for Google Analytics, we need this to show the graph on the dashboard.', 'text', '', '', '', 0, 1, 'integration', 983),
('ga_password', 'Google Analytic Password', 'This is also needed to show the graph on the dashboard. You will need to allow access to Google to get this to work. See <a href="https://accounts.google.com/b/0/IssuedAuthSubTokens?hl=en_US" target="_blank">Authorized Access to your Google Account</a>', 'password', '', '', '', 0, 1, 'integration', 982),
('contact_email', 'Contact E-mail', 'All e-mails from users, guests and the site will go to this e-mail address.', 'text', 'okky.sari@gmail.com', '', '', 1, 1, 'email', 979),
('server_email', 'Server E-mail', 'All e-mails to users will come from this e-mail address.', 'text', 'admin@localhost', '', '', 1, 1, 'email', 978),
('mail_protocol', 'Mail Protocol', 'Select desired email protocol.', 'select', 'mail', 'mail', 'mail=Mail|sendmail=Sendmail|smtp=SMTP', 1, 1, 'email', 977),
('mail_smtp_host', 'SMTP Host Name', 'The host name of your smtp server.', 'text', '', '', '', 0, 1, 'email', 976),
('mail_smtp_pass', 'SMTP password', 'SMTP password.', 'password', '', '', '', 0, 1, 'email', 975),
('mail_smtp_port', 'SMTP Port', 'SMTP port number.', 'text', '', '', '', 0, 1, 'email', 974),
('mail_smtp_user', 'SMTP User Name', 'SMTP user name.', 'text', '', '', '', 0, 1, 'email', 973),
('mail_sendmail_path', 'Sendmail Path', 'Path to server sendmail binary.', 'text', '', '', '', 0, 1, 'email', 972),
('mail_line_endings', 'Email Line Endings', 'Change from the standard \\r\\n line ending to PHP_EOL for some email servers.', 'select', '1', '1', '0=PHP_EOL|1=\\r\\n', 0, 1, 'email', 972),
('admin_force_https', 'Force HTTPS for Control Panel?', 'Allow only the HTTPS protocol when using the Control Panel?', 'radio', '0', '', '1=Yes|0=No', 1, 1, '', 0),
('api_enabled', 'API Enabled', 'Allow API access to all modules which have an API controller.', 'select', '0', '0', '0=Disabled|1=Enabled', 0, 0, 'api', 0),
('api_user_keys', 'API User Keys', 'Allow users to sign up for API keys (if the API is Enabled).', 'select', '0', '0', '0=Disabled|1=Enabled', 0, 0, 'api', 0),
('cdn_domain', 'CDN Domain', 'CDN domains allow you to offload static content to various edge servers, like Amazon CloudFront or MaxCDN.', 'text', '', '', '', 0, 1, 'integration', 1000),
('addons_upload', 'Addons Upload Permissions', 'Keeps mere admins from uploading addons by default', 'text', '0', '1', '', 1, 0, '', 0),
('default_theme', 'Default Theme', 'Select the theme you want users to see by default.', '', 'default', 'coraltrianglePreContest', 'func:get_themes', 1, 0, '', 0),
('admin_theme', 'Control Panel Theme', 'Select the theme for the control panel.', '', '', 'pyrocms', 'func:get_themes', 1, 0, '', 0),
('akismet_api_key', 'Akismet API Key', 'Akismet is a spam-blocker from the WordPress team. It keeps spam under control without forcing users to get past human-checking CAPTCHA forms.', 'text', '', '', '', 0, 1, 'integration', 981),
('enable_comments', 'Enable Comments', 'Enable comments.', 'radio', '1', '1', '1=Enabled|0=Disabled', 1, 1, 'comments', 968),
('moderate_comments', 'Moderate Comments', 'Force comments to be approved before they appear on the site.', 'radio', '1', '1', '1=Enabled|0=Disabled', 1, 1, 'comments', 967),
('comment_order', 'Comment Order', 'Sort order in which to display comments.', 'select', 'ASC', 'ASC', 'ASC=Oldest First|DESC=Newest First', 1, 1, 'comments', 966),
('comment_markdown', 'Allow Markdown', 'Do you want to allow visitors to post comments using Markdown?', 'select', '0', '0', '0=Text Only|1=Allow Markdown', 1, 1, 'comments', 965),
('files_cache', 'Files Cache', 'When outputting an image via site.com/files what shall we set the cache expiration for?', 'select', '480', '480', '0=no-cache|1=1-minute|60=1-hour|180=3-hour|480=8-hour|1440=1-day|43200=30-days', 1, 1, 'files', 986),
('files_enabled_providers', 'Enabled File Storage Providers', 'Which file storage providers do you want to enable? (If you enable a cloud provider you must provide valid auth keys below', 'checkbox', '0', '0', 'amazon-s3=Amazon S3|rackspace-cf=Rackspace Cloud Files', 0, 1, 'files', 994),
('files_s3_access_key', 'Amazon S3 Access Key', 'To enable cloud file storage in your Amazon S3 account provide your Access Key. <a href="https://aws-portal.amazon.com/gp/aws/securityCredentials#access_credentials">Find your credentials</a>', 'text', '', '', '', 0, 1, 'files', 993),
('files_s3_secret_key', 'Amazon S3 Secret Key', 'You also must provide your Amazon S3 Secret Key. You will find it at the same location as your Access Key in your Amazon account.', 'text', '', '', '', 0, 1, 'files', 992),
('files_s3_geographic_location', 'Amazon S3 Geographic Location', 'Either US or EU. If you change this you must also change the S3 URL.', 'radio', 'US', 'US', 'US=United States|EU=Europe', 1, 1, 'files', 991),
('files_s3_url', 'Amazon S3 URL', 'Change this if using one of Amazon''s EU locations or a custom domain.', 'text', 'http://{{ bucket }}.s3.amazonaws.com/', 'http://{{ bucket }}.s3.amazonaws.com/', '', 0, 1, 'files', 991),
('files_cf_username', 'Rackspace Cloud Files Username', 'To enable cloud file storage in your Rackspace Cloud Files account please enter your Cloud Files Username. <a href="https://manage.rackspacecloud.com/APIAccess.do">Find your credentials</a>', 'text', '', '', '', 0, 1, 'files', 990),
('files_cf_api_key', 'Rackspace Cloud Files API Key', 'You also must provide your Cloud Files API Key. You will find it at the same location as your Username in your Rackspace account.', 'text', '', '', '', 0, 1, 'files', 989),
('files_upload_limit', 'Filesize Limit', 'Maximum filesize to allow when uploading. Specify the size in MB. Example: 5', 'text', '5', '5', '', 1, 1, 'files', 987),
('auto_username', 'Auto Username', 'Create the username automatically, meaning users can skip making one on registration.', 'radio', '1', '', '1=Enabled|0=Disabled', 0, 1, 'users', 964),
('enable_profiles', 'Enable profiles', 'Allow users to add and edit profiles.', 'radio', '1', '0', '1=Enabled|0=Disabled', 1, 1, 'users', 963),
('activation_email', 'Activation Email', 'Send out an e-mail with an activation link when a user signs up. Disable this so that admins must manually activate each account.', 'select', '1', '0', '0=activate_by_admin|1=activate_by_email|2=no_activation', 0, 1, 'users', 961),
('registered_email', 'User Registered Email', 'Send a notification email to the contact e-mail when someone registers.', 'radio', '1', '', '1=Enabled|0=Disabled', 0, 1, 'users', 962),
('enable_registration', 'Enable user registration', 'Allow users to register in your site.', 'radio', '1', '0', '1=Enabled|0=Disabled', 0, 1, 'users', 961),
('profile_visibility', 'Profile Visibility', 'Specify who can view user profiles on the public site', 'select', 'public', 'hidden', 'public=profile_public|owner=profile_owner|hidden=profile_hidden|member=profile_member', 0, 1, 'users', 960),
('ckeditor_config', 'CKEditor Config', 'You can find a list of valid configuration items in <a target="_blank" href="http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.config.html">CKEditor''s documentation.</a>', 'textarea', '', '{{# this is a wysiwyg-simple editor customized for the blog module (it allows images to be inserted) #}}\n$(''textarea#intro.wysiwyg-simple'').ckeditor({\n	toolbar: [\n		[''pyroimages''],\n		[''Bold'', ''Italic'', ''-'', ''NumberedList'', ''BulletedList'', ''-'', ''Link'', ''Unlink'']\n	  ],\n	extraPlugins: ''pyroimages'',\n	width: ''99%'',\n	height: 100,\n	dialog_backgroundCoverColor: ''#000'',\n	defaultLanguage: ''{{ helper:config item="default_language" }}'',\n	language: ''{{ global:current_language }}''\n});\n\n{{# this is the config for all wysiwyg-simple textareas #}}\n$(''textarea.wysiwyg-simple'').ckeditor({\n	toolbar: [\n		[''Bold'', ''Italic'', ''-'', ''NumberedList'', ''BulletedList'', ''-'', ''Link'', ''Unlink'']\n	  ],\n	width: ''99%'',\n	height: 100,\n	dialog_backgroundCoverColor: ''#000'',\n	defaultLanguage: ''{{ helper:config item="default_language" }}'',\n	language: ''{{ global:current_language }}''\n});\n\n{{# and this is the advanced editor #}}\n$(''textarea.wysiwyg-advanced'').ckeditor({\n	toolbar: [\n		[''Maximize''],\n		[''pyroimages'', ''pyrofiles''],\n		[''Cut'',''Copy'',''Paste'',''PasteFromWord''],\n		[''Undo'',''Redo'',''-'',''Find'',''Replace''],\n		[''Link'',''Unlink''],\n		[''Table'',''HorizontalRule'',''SpecialChar''],\n		[''Bold'',''Italic'',''StrikeThrough''],\n		[''JustifyLeft'',''JustifyCenter'',''JustifyRight'',''JustifyBlock'',''-'',''BidiLtr'',''BidiRtl''],\n		[''Format'', ''FontSize'', ''Subscript'',''Superscript'', ''NumberedList'',''BulletedList'',''Outdent'',''Indent'',''Blockquote''],\n		[''ShowBlocks'', ''RemoveFormat'', ''Source'']\n	],\n	extraPlugins: ''pyroimages,pyrofiles'',\n	width: ''99%'',\n	height: 400,\n	dialog_backgroundCoverColor: ''#000'',\n	removePlugins: ''elementspath'',\n	defaultLanguage: ''{{ helper:config item="default_language" }}'',\n	language: ''{{ global:current_language }}''\n});', '', 1, 1, 'wysiwyg', 993),
('instagram_api_key', 'Instagram API Key', 'Your Instagram API key', 'text', '', 'd2a86bd002c947198137cabbdeeb31d6', '', 0, 1, 'instagram', 1008),
('instagram_api_secret', 'Instagram API Secret', 'Your Instagram API secret', 'text', '', 'f0f3d423c6bd431f8a207670a0f654f3', '', 0, 1, 'instagram', 1007),
('mailchimp_list_id', 'Mailchimp list ID', 'ID of the List at Mailchimp to subscribe', 'text', '', '', '', 0, 1, 'Mailchimp', 800),
('mailchimp_api', 'Mailchimp API Key', 'API Key to connect to Mailchimp', 'text', '', '', '', 0, 1, 'Mailchimp', 800),
('newsimg_width', 'News Image Width', 'Width of news image', 'text', '262', '262', '', 0, 1, 'images', 1003),
('newsimg_height', 'News Image Height', 'Height of news image', 'text', '175', '175', '', 0, 1, 'images', 1004),
('news_to_twitter', 'Integrate News with Twitter', 'Post update when posting new article', 'radio', '0', '0', '1=Yes|0=No', 0, 1, 'twitter', 1009),
('twitter_consumer_key', 'Twitter Consumer Key', 'Your Twitter consumer key', 'text', '', '', '', 0, 1, 'twitter', 1008),
('twitter_consumer_key_secret', 'Twitter Consumer Key Secret', 'Your Twitter consumer key secret', 'text', '', '', '', 0, 1, 'twitter', 1007),
('twitter_access_token', 'Twitter Access Token', 'Your Twitter access token', 'text', '', '', '', 0, 1, 'twitter', 1006),
('twitter_access_token_secret', 'Twitter Access Token Secret', 'Your Twitter access token secret', 'text', '', '', '', 0, 1, 'twitter', 1005),
('upon_event_submission', 'UPON EVENT SUBMISSION', 'for Admin', 'text', 'contest@coraltriangle.org', 'contest@coraltriangleday.org', '', 1, 1, 'email', 979);

-- --------------------------------------------------------

--
-- Table structure for table `default_theme_options`
--

CREATE TABLE IF NOT EXISTS `default_theme_options` (
  `id` int(11) NOT NULL,
  `slug` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` set('text','textarea','password','select','select-multiple','radio','checkbox','colour-picker') COLLATE utf8_unicode_ci NOT NULL,
  `default` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `is_required` int(1) NOT NULL,
  `theme` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_theme_options`
--

INSERT INTO `default_theme_options` (`id`, `slug`, `title`, `description`, `type`, `default`, `value`, `options`, `is_required`, `theme`) VALUES
(1, 'pyrocms_recent_comments', 'Recent Comments', 'Would you like to display recent comments on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(2, 'pyrocms_news_feed', 'News Feed', 'Would you like to display the news feed on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(3, 'pyrocms_quick_links', 'Quick Links', 'Would you like to display quick links on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(4, 'pyrocms_analytics_graph', 'Analytics Graph', 'Would you like to display the graph on the dashboard?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'pyrocms'),
(5, 'show_breadcrumbs', 'Show Breadcrumbs', 'Would you like to display breadcrumbs?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'default'),
(6, 'layout', 'Layout', 'Which type of layout shall we use?', 'select', '2 column', '2 column', '2 column=Two Column|full-width=Full Width|full-width-home=Full Width Home Page', 1, 'default'),
(7, 'background', 'Background', 'Choose the default background for the theme.', 'select', 'fabric', 'fabric', 'black=Black|fabric=Fabric|graph=Graph|leather=Leather|noise=Noise|texture=Texture', 1, 'base'),
(8, 'slider', 'Slider', 'Would you like to display the slider on the homepage?', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'base'),
(9, 'color', 'Default Theme Color', 'This changes things like background color, link colors etc…', 'select', 'pink', 'pink', 'red=Red|orange=Orange|yellow=Yellow|green=Green|blue=Blue|pink=Pink', 1, 'base'),
(10, 'show_breadcrumbs', 'Do you want to show breadcrumbs?', 'If selected it shows a string of breadcrumbs at the top of the page.', 'radio', 'yes', 'yes', 'yes=Yes|no=No', 1, 'base');

-- --------------------------------------------------------

--
-- Table structure for table `default_users`
--

CREATE TABLE IF NOT EXISTS `default_users` (
  `id` smallint(5) unsigned NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `salt` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgotten_password_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Registered User Information';

--
-- Dumping data for table `default_users`
--

INSERT INTO `default_users` (`id`, `email`, `password`, `salt`, `group_id`, `ip_address`, `active`, `activation_code`, `created_on`, `last_login`, `username`, `forgotten_password_code`, `remember_code`) VALUES
(1, 'okky.sari@gmail.com', 'e883f7f8cf88918ceac8f0a4364fc765d645365c', '4b9fc', 1, '', 1, '', 1430889571, 1521033388, 'osari', '5e55748644c3302bf9011ab5125438dc69307a4a', '7a147309ef05d1a30d57127d1a442f3f048b5bab'),
(2, 'arifin@catalyzecommunications.com', 'e18cd5dcd9121c13ee290b9385ef4449f7822416', '15f3c5', 1, '101.128.120.96', 1, NULL, 1463557373, 1521201563, 'arifin', NULL, '69657608fe7b89ed2341a76aedc1f63f9c645ac4'),
(3, 'andiewibi@cticff.org', 'b2b34cd073c8ac668bcb4aae9b7cf187a4f28e5b', '0b93a7', 1, '36.82.27.163', 1, NULL, 1521189004, 1522044278, 'andiewibi', NULL, 'ae520f92a33ac48e58a95dfa39d0c6e56fa8f2cc');

-- --------------------------------------------------------

--
-- Table structure for table `default_variables`
--

CREATE TABLE IF NOT EXISTS `default_variables` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `default_widgets`
--

CREATE TABLE IF NOT EXISTS `default_widgets` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `enabled` int(1) NOT NULL DEFAULT '1',
  `order` int(10) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_widgets`
--

INSERT INTO `default_widgets` (`id`, `slug`, `title`, `description`, `author`, `website`, `version`, `enabled`, `order`, `updated_on`) VALUES
(1, 'google_maps', 'a:10:{s:2:"en";s:11:"Google Maps";s:2:"el";s:19:"Χάρτης Google";s:2:"nl";s:11:"Google Maps";s:2:"br";s:11:"Google Maps";s:2:"pt";s:11:"Google Maps";s:2:"ru";s:17:"Карты Google";s:2:"id";s:11:"Google Maps";s:2:"fi";s:11:"Google Maps";s:2:"fr";s:11:"Google Maps";s:2:"fa";s:17:"نقشه گوگل";}', 'a:10:{s:2:"en";s:32:"Display Google Maps on your site";s:2:"el";s:78:"Προβάλετε έναν Χάρτη Google στον ιστότοπό σας";s:2:"nl";s:27:"Toon Google Maps in uw site";s:2:"br";s:34:"Mostra mapas do Google no seu site";s:2:"pt";s:34:"Mostra mapas do Google no seu site";s:2:"ru";s:80:"Выводит карты Google на страницах вашего сайта";s:2:"id";s:37:"Menampilkan Google Maps di Situs Anda";s:2:"fi";s:39:"Näytä Google Maps kartta sivustollasi";s:2:"fr";s:42:"Publiez un plan Google Maps sur votre site";s:2:"fa";s:59:"نمایش داده نقشه گوگل بر روی سایت ";}', 'Gregory Athons', 'http://www.gregathons.com', '1.0.0', 1, 1, 1431725538),
(2, 'html', 's:4:"HTML";', 'a:10:{s:2:"en";s:28:"Create blocks of custom HTML";s:2:"el";s:80:"Δημιουργήστε περιοχές με δικό σας κώδικα HTML";s:2:"br";s:41:"Permite criar blocos de HTML customizados";s:2:"pt";s:41:"Permite criar blocos de HTML customizados";s:2:"nl";s:30:"Maak blokken met maatwerk HTML";s:2:"ru";s:83:"Создание HTML-блоков с произвольным содержимым";s:2:"id";s:24:"Membuat blok HTML apapun";s:2:"fi";s:32:"Luo lohkoja omasta HTML koodista";s:2:"fr";s:36:"Créez des blocs HTML personnalisés";s:2:"fa";s:58:"ایجاد قسمت ها به صورت اچ تی ام ال";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.0.0', 1, 2, 1431725538),
(3, 'login', 'a:10:{s:2:"en";s:5:"Login";s:2:"el";s:14:"Σύνδεση";s:2:"nl";s:5:"Login";s:2:"br";s:5:"Login";s:2:"pt";s:5:"Login";s:2:"ru";s:22:"Вход на сайт";s:2:"id";s:5:"Login";s:2:"fi";s:13:"Kirjautuminen";s:2:"fr";s:9:"Connexion";s:2:"fa";s:10:"لاگین";}', 'a:10:{s:2:"en";s:36:"Display a simple login form anywhere";s:2:"el";s:96:"Προβάλετε μια απλή φόρμα σύνδεσης χρήστη οπουδήποτε";s:2:"br";s:69:"Permite colocar um formulário de login em qualquer lugar do seu site";s:2:"pt";s:69:"Permite colocar um formulário de login em qualquer lugar do seu site";s:2:"nl";s:32:"Toon overal een simpele loginbox";s:2:"ru";s:72:"Выводит простую форму для входа на сайт";s:2:"id";s:32:"Menampilkan form login sederhana";s:2:"fi";s:52:"Näytä yksinkertainen kirjautumislomake missä vain";s:2:"fr";s:54:"Affichez un formulaire de connexion où vous souhaitez";s:2:"fa";s:70:"نمایش یک لاگین ساده در هر قسمتی از سایت";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.0.0', 1, 3, 1431725538),
(4, 'navigation', 'a:10:{s:2:"en";s:10:"Navigation";s:2:"el";s:16:"Πλοήγηση";s:2:"nl";s:9:"Navigatie";s:2:"br";s:11:"Navegação";s:2:"pt";s:11:"Navegação";s:2:"ru";s:18:"Навигация";s:2:"id";s:8:"Navigasi";s:2:"fi";s:10:"Navigaatio";s:2:"fr";s:10:"Navigation";s:2:"fa";s:10:"منوها";}', 'a:10:{s:2:"en";s:40:"Display a navigation group with a widget";s:2:"el";s:100:"Προβάλετε μια ομάδα στοιχείων πλοήγησης στον ιστότοπο";s:2:"nl";s:38:"Toon een navigatiegroep met een widget";s:2:"br";s:62:"Exibe um grupo de links de navegação como widget em seu site";s:2:"pt";s:62:"Exibe um grupo de links de navegação como widget no seu site";s:2:"ru";s:88:"Отображает навигационную группу внутри виджета";s:2:"id";s:44:"Menampilkan grup navigasi menggunakan widget";s:2:"fi";s:37:"Näytä widgetillä navigaatio ryhmä";s:2:"fr";s:47:"Affichez un groupe de navigation dans un widget";s:2:"fa";s:71:"نمایش گروهی از منوها با استفاده از ویجت";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.2.0', 1, 4, 1431725538),
(5, 'rss_feed', 'a:10:{s:2:"en";s:8:"RSS Feed";s:2:"el";s:24:"Τροφοδοσία RSS";s:2:"nl";s:8:"RSS Feed";s:2:"br";s:8:"Feed RSS";s:2:"pt";s:8:"Feed RSS";s:2:"ru";s:31:"Лента новостей RSS";s:2:"id";s:8:"RSS Feed";s:2:"fi";s:10:"RSS Syöte";s:2:"fr";s:8:"Flux RSS";s:2:"fa";s:19:"خبر خوان RSS";}', 'a:10:{s:2:"en";s:41:"Display parsed RSS feeds on your websites";s:2:"el";s:82:"Προβάλετε τα περιεχόμενα μιας τροφοδοσίας RSS";s:2:"nl";s:28:"Toon RSS feeds op uw website";s:2:"br";s:48:"Interpreta e exibe qualquer feed RSS no seu site";s:2:"pt";s:48:"Interpreta e exibe qualquer feed RSS no seu site";s:2:"ru";s:94:"Выводит обработанную ленту новостей на вашем сайте";s:2:"id";s:42:"Menampilkan kutipan RSS feed di situs Anda";s:2:"fi";s:39:"Näytä purettu RSS syöte sivustollasi";s:2:"fr";s:39:"Affichez un flux RSS sur votre site web";s:2:"fa";s:46:"نمایش خوراک های RSS در سایت";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.2.0', 1, 5, 1431725538),
(6, 'social_bookmark', 'a:10:{s:2:"en";s:15:"Social Bookmark";s:2:"el";s:35:"Κοινωνική δικτύωση";s:2:"nl";s:19:"Sociale Bladwijzers";s:2:"br";s:15:"Social Bookmark";s:2:"pt";s:15:"Social Bookmark";s:2:"ru";s:37:"Социальные закладки";s:2:"id";s:15:"Social Bookmark";s:2:"fi";s:24:"Sosiaalinen kirjanmerkki";s:2:"fr";s:13:"Liens sociaux";s:2:"fa";s:52:"بوکمارک های شبکه های اجتماعی";}', 'a:10:{s:2:"en";s:47:"Configurable social bookmark links from AddThis";s:2:"el";s:111:"Παραμετροποιήσιμα στοιχεία κοινωνικής δικτυώσης από το AddThis";s:2:"nl";s:43:"Voeg sociale bladwijzers toe vanuit AddThis";s:2:"br";s:87:"Adiciona links de redes sociais usando o AddThis, podendo fazer algumas configurações";s:2:"pt";s:87:"Adiciona links de redes sociais usando o AddThis, podendo fazer algumas configurações";s:2:"ru";s:90:"Конфигурируемые социальные закладки с сайта AddThis";s:2:"id";s:60:"Tautan social bookmark yang dapat dikonfigurasi dari AddThis";s:2:"fi";s:59:"Konfiguroitava sosiaalinen kirjanmerkki linkit AddThis:stä";s:2:"fr";s:43:"Liens sociaux personnalisables avec AddThis";s:2:"fa";s:71:"تنظیم و نمایش لینک های شبکه های اجتماعی";}', 'Phil Sturgeon', 'http://philsturgeon.co.uk/', '1.0.0', 1, 6, 1431725538),
(7, 'archive', 'a:2:{s:2:"en";s:12:"News Archive";s:2:"id";s:12:"Arsip Berita";}', 'a:2:{s:2:"en";s:64:"Display a list of old months with links to posts in those months";s:2:"id";s:63:"Menampilkan daftar bulan beserta tautan post di setiap bulannya";}', 'Okky Sari', 'http://santalaya.com/', '1.0', 1, 7, 1513779411),
(8, 'blog_categories', 'a:8:{s:2:"en";s:15:"Blog Categories";s:2:"br";s:18:"Categorias do Blog";s:2:"pt";s:18:"Categorias do Blog";s:2:"el";s:41:"Κατηγορίες Ιστολογίου";s:2:"fr";s:19:"Catégories du Blog";s:2:"ru";s:29:"Категории Блога";s:2:"id";s:12:"Kateori Blog";s:2:"fa";s:28:"مجموعه های بلاگ";}', 'a:8:{s:2:"en";s:30:"Show a list of blog categories";s:2:"br";s:57:"Mostra uma lista de navegação com as categorias do Blog";s:2:"pt";s:57:"Mostra uma lista de navegação com as categorias do Blog";s:2:"el";s:97:"Προβάλει την λίστα των κατηγοριών του ιστολογίου σας";s:2:"fr";s:49:"Permet d''afficher la liste de Catégories du Blog";s:2:"ru";s:57:"Выводит список категорий блога";s:2:"id";s:35:"Menampilkan daftar kategori tulisan";s:2:"fa";s:55:"نمایش لیستی از مجموعه های بلاگ";}', 'Stephen Cozart', 'http://github.com/clip/', '1.0.0', 1, 8, 1431725538),
(9, 'latest_posts', 'a:8:{s:2:"en";s:12:"Latest posts";s:2:"br";s:24:"Artigos recentes do Blog";s:2:"fa";s:26:"آخرین ارسال ها";s:2:"pt";s:24:"Artigos recentes do Blog";s:2:"el";s:62:"Τελευταίες αναρτήσεις ιστολογίου";s:2:"fr";s:17:"Derniers articles";s:2:"ru";s:31:"Последние записи";s:2:"id";s:12:"Post Terbaru";}', 'a:8:{s:2:"en";s:39:"Display latest blog posts with a widget";s:2:"br";s:81:"Mostra uma lista de navegação para abrir os últimos artigos publicados no Blog";s:2:"fa";s:65:"نمایش آخرین پست های وبلاگ در یک ویجت";s:2:"pt";s:81:"Mostra uma lista de navegação para abrir os últimos artigos publicados no Blog";s:2:"el";s:103:"Προβάλει τις πιο πρόσφατες αναρτήσεις στο ιστολόγιό σας";s:2:"fr";s:68:"Permet d''afficher la liste des derniers posts du blog dans un Widget";s:2:"ru";s:100:"Выводит список последних записей блога внутри виджета";s:2:"id";s:51:"Menampilkan posting blog terbaru menggunakan widget";}', 'Erik Berman', 'http://www.nukleo.fr', '1.0.0', 1, 9, 1431725538),
(10, 'latest_news', 'a:2:{s:2:"en";s:11:"Latest news";s:2:"id";s:14:"Berita Terkini";}', 'a:2:{s:2:"en";s:39:"Display latest news posts with a widget";s:2:"id";s:53:"Menampilkan posting berita terkini menggunakan widget";}', 'Erik Berman', 'http://www.nukleo.fr', '1.0', 1, 10, 1513779411),
(11, 'news_categories', 'a:2:{s:2:"en";s:15:"News Categories";s:2:"id";s:14:"Kateori Berita";}', 'a:2:{s:2:"en";s:30:"Show a list of news categories";s:2:"id";s:34:"Menampilkan daftar kategori berita";}', 'Stephen Cozart', 'http://github.com/clip/', '1.0', 1, 11, 1513779411);

-- --------------------------------------------------------

--
-- Table structure for table `default_widget_areas`
--

CREATE TABLE IF NOT EXISTS `default_widget_areas` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `default_widget_areas`
--

INSERT INTO `default_widget_areas` (`id`, `slug`, `title`) VALUES
(1, 'sidebar', 'Sidebar');

-- --------------------------------------------------------

--
-- Table structure for table `default_widget_instances`
--

CREATE TABLE IF NOT EXISTS `default_widget_instances` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `widget_id` int(11) DEFAULT NULL,
  `widget_area_id` int(11) DEFAULT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) NOT NULL DEFAULT '0',
  `created_on` int(11) NOT NULL DEFAULT '0',
  `updated_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `core_settings`
--
ALTER TABLE `core_settings`
  ADD PRIMARY KEY (`slug`), ADD UNIQUE KEY `unique - slug` (`slug`), ADD KEY `index - slug` (`slug`);

--
-- Indexes for table `core_sites`
--
ALTER TABLE `core_sites`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `Unique ref` (`ref`), ADD UNIQUE KEY `Unique domain` (`domain`), ADD KEY `ref` (`ref`), ADD KEY `domain` (`domain`);

--
-- Indexes for table `core_users`
--
ALTER TABLE `core_users`
  ADD PRIMARY KEY (`id`), ADD KEY `email` (`email`);

--
-- Indexes for table `default_blog`
--
ALTER TABLE `default_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_blog_categories`
--
ALTER TABLE `default_blog_categories`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_slug` (`slug`), ADD UNIQUE KEY `unique_title` (`title`), ADD KEY `slug` (`slug`);

--
-- Indexes for table `default_ci_sessions`
--
ALTER TABLE `default_ci_sessions`
  ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `default_comments`
--
ALTER TABLE `default_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_comment_blacklists`
--
ALTER TABLE `default_comment_blacklists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_contact_log`
--
ALTER TABLE `default_contact_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_countries`
--
ALTER TABLE `default_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_ctd`
--
ALTER TABLE `default_ctd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_ctd_attendance`
--
ALTER TABLE `default_ctd_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_ctd_categories`
--
ALTER TABLE `default_ctd_categories`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug - unique` (`slug`), ADD UNIQUE KEY `title - unique` (`title`), ADD KEY `slug - normal` (`slug`);

--
-- Indexes for table `default_ctd_partners`
--
ALTER TABLE `default_ctd_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_data_fields`
--
ALTER TABLE `default_data_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_data_field_assignments`
--
ALTER TABLE `default_data_field_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_data_streams`
--
ALTER TABLE `default_data_streams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_def_page_fields`
--
ALTER TABLE `default_def_page_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_email_templates`
--
ALTER TABLE `default_email_templates`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug_lang` (`slug`,`lang`);

--
-- Indexes for table `default_files`
--
ALTER TABLE `default_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_file_folders`
--
ALTER TABLE `default_file_folders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_galleries`
--
ALTER TABLE `default_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_gallery_categories`
--
ALTER TABLE `default_gallery_categories`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug - unique` (`slug`), ADD UNIQUE KEY `title - unique` (`title`), ADD KEY `slug - normal` (`slug`);

--
-- Indexes for table `default_gallery_media`
--
ALTER TABLE `default_gallery_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_groups`
--
ALTER TABLE `default_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_instagram_posts`
--
ALTER TABLE `default_instagram_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_instagram_receive`
--
ALTER TABLE `default_instagram_receive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_instagram_subscriptions`
--
ALTER TABLE `default_instagram_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_keywords`
--
ALTER TABLE `default_keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_keywords_applied`
--
ALTER TABLE `default_keywords_applied`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_modules`
--
ALTER TABLE `default_modules`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug` (`slug`), ADD KEY `enabled` (`enabled`);

--
-- Indexes for table `default_navigation_groups`
--
ALTER TABLE `default_navigation_groups`
  ADD PRIMARY KEY (`id`), ADD KEY `abbrev` (`abbrev`);

--
-- Indexes for table `default_navigation_links`
--
ALTER TABLE `default_navigation_links`
  ADD PRIMARY KEY (`id`), ADD KEY `navigation_group_id` (`navigation_group_id`);

--
-- Indexes for table `default_news`
--
ALTER TABLE `default_news`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_title` (`title`), ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `default_news_categories`
--
ALTER TABLE `default_news_categories`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_slug` (`slug`), ADD UNIQUE KEY `unique_title` (`title`), ADD KEY `slug` (`slug`);

--
-- Indexes for table `default_news_images`
--
ALTER TABLE `default_news_images`
  ADD PRIMARY KEY (`id`), ADD KEY `gallery_id` (`filename`);

--
-- Indexes for table `default_pages`
--
ALTER TABLE `default_pages`
  ADD PRIMARY KEY (`id`), ADD KEY `slug` (`slug`), ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `default_page_types`
--
ALTER TABLE `default_page_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_permissions`
--
ALTER TABLE `default_permissions`
  ADD PRIMARY KEY (`id`), ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `default_profiles`
--
ALTER TABLE `default_profiles`
  ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `default_redirects`
--
ALTER TABLE `default_redirects`
  ADD PRIMARY KEY (`id`), ADD KEY `from` (`from`);

--
-- Indexes for table `default_search_index`
--
ALTER TABLE `default_search_index`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique` (`module`,`entry_key`,`entry_id`(190)), ADD FULLTEXT KEY `full search` (`title`,`description`,`keywords`);

--
-- Indexes for table `default_settings`
--
ALTER TABLE `default_settings`
  ADD PRIMARY KEY (`slug`), ADD UNIQUE KEY `unique_slug` (`slug`), ADD KEY `slug` (`slug`);

--
-- Indexes for table `default_theme_options`
--
ALTER TABLE `default_theme_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_users`
--
ALTER TABLE `default_users`
  ADD PRIMARY KEY (`id`), ADD KEY `email` (`email`);

--
-- Indexes for table `default_variables`
--
ALTER TABLE `default_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_widgets`
--
ALTER TABLE `default_widgets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_widget_areas`
--
ALTER TABLE `default_widget_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_widget_instances`
--
ALTER TABLE `default_widget_instances`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `core_sites`
--
ALTER TABLE `core_sites`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `core_users`
--
ALTER TABLE `core_users`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `default_blog`
--
ALTER TABLE `default_blog`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_blog_categories`
--
ALTER TABLE `default_blog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_comments`
--
ALTER TABLE `default_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_comment_blacklists`
--
ALTER TABLE `default_comment_blacklists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_contact_log`
--
ALTER TABLE `default_contact_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `default_countries`
--
ALTER TABLE `default_countries`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_ctd`
--
ALTER TABLE `default_ctd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=197;
--
-- AUTO_INCREMENT for table `default_ctd_attendance`
--
ALTER TABLE `default_ctd_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_ctd_categories`
--
ALTER TABLE `default_ctd_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_ctd_partners`
--
ALTER TABLE `default_ctd_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_data_fields`
--
ALTER TABLE `default_data_fields`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `default_data_field_assignments`
--
ALTER TABLE `default_data_field_assignments`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `default_data_streams`
--
ALTER TABLE `default_data_streams`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `default_def_page_fields`
--
ALTER TABLE `default_def_page_fields`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `default_email_templates`
--
ALTER TABLE `default_email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `default_file_folders`
--
ALTER TABLE `default_file_folders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `default_galleries`
--
ALTER TABLE `default_galleries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `default_gallery_categories`
--
ALTER TABLE `default_gallery_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `default_gallery_media`
--
ALTER TABLE `default_gallery_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=768;
--
-- AUTO_INCREMENT for table `default_groups`
--
ALTER TABLE `default_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `default_instagram_posts`
--
ALTER TABLE `default_instagram_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `default_instagram_receive`
--
ALTER TABLE `default_instagram_receive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_instagram_subscriptions`
--
ALTER TABLE `default_instagram_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `default_keywords`
--
ALTER TABLE `default_keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `default_keywords_applied`
--
ALTER TABLE `default_keywords_applied`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `default_modules`
--
ALTER TABLE `default_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `default_navigation_groups`
--
ALTER TABLE `default_navigation_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `default_navigation_links`
--
ALTER TABLE `default_navigation_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `default_news`
--
ALTER TABLE `default_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `default_news_categories`
--
ALTER TABLE `default_news_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_news_images`
--
ALTER TABLE `default_news_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_pages`
--
ALTER TABLE `default_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `default_page_types`
--
ALTER TABLE `default_page_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `default_permissions`
--
ALTER TABLE `default_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_profiles`
--
ALTER TABLE `default_profiles`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `default_redirects`
--
ALTER TABLE `default_redirects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `default_search_index`
--
ALTER TABLE `default_search_index`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `default_theme_options`
--
ALTER TABLE `default_theme_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `default_users`
--
ALTER TABLE `default_users`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `default_variables`
--
ALTER TABLE `default_variables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `default_widgets`
--
ALTER TABLE `default_widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `default_widget_areas`
--
ALTER TABLE `default_widget_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `default_widget_instances`
--
ALTER TABLE `default_widget_instances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
