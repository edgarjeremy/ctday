function collage2() {
	var $container = $('#mcontainer');
	$('#mcontainer .item').imagesLoaded( function() {
		$container.fadeIn('fast', function(){
			$('#loading').slideUp('fast');
	        collage();
		});
	});
};

function collage() {
    $('#mcontainer').removeWhitespace().collagePlus(
        {
            'fadeSpeed'     : 700,
            'targetHeight'  : 280,
            'direction'     : 'vertical'
        }
    );
};

function fadingInContainer()
{
	$('#mcontainer .item').imagesLoaded( function() {
		$('.masonry-wrapper').fadeTo('slow', 1);
	});
}


// This is just for the case that the browser window is resized
var resizeTimer = null;
$(window).bind('resize', function() {
    // hide all the images until we resize them
    $('#mcontainer .item').css("opacity", 0);
    // set a timer to re-apply the plugin
    if (resizeTimer) clearTimeout(resizeTimer);
    resizeTimer = setTimeout(collage2, 200);
});

(function($) {
	$(function(){

		var $container = $('#mcontainer');
		$('#mcontainer .item').imagesLoaded( function() {
			//$container.fadeIn('fast', function(){
				$('#loading').slideUp('fast', function(){
			        $container.fadeIn('slow');
			        collage();
			        $('#mosaic_loadmore2').fadeIn('fast');
				});
			//});
		});

	});
})(jQuery);


