"use strict";

var Catalyze = Catalyze || {};

//Google map api
Catalyze.map = Catalyze.Map || {};
Catalyze.map.element;

Catalyze.map.style = [{
						    "featureType": "administrative",
						    "elementType": "labels.text.fill",
						    "stylers": [{
						        "color": "#444444"
						    }]
						}, {
						    "featureType": "landscape",
						    "elementType": "all",
						    "stylers": [{
						        "color": "#f2f2f2"
						    }]
						}, {
						    "featureType": "poi",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "road",
						    "elementType": "all",
						    "stylers": [{
						        "saturation": -100
						    }, {
						        "lightness": 45
						    }]
						}, {
						    "featureType": "road.highway",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "simplified"
						    }]
						}, {
						    "featureType": "road.arterial",
						    "elementType": "labels.icon",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "transit",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "water",
						    "elementType": "all",
						    "stylers": [{
						        "color": "#46bcec"
						    }, {
						        "visibility": "on"
						    }]
						}];

Catalyze.map.init = function(){
	var mapOptions = {
		styles: Catalyze.map.style,
		zoom: 5,
		center: new google.maps.LatLng(-1.4894799, 121.8823101)
	};
	Catalyze.map.element = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
};

Catalyze.map.toggleSideMenu = function(){
	$(".sidemenu-country-wrapper").animate({width:'toggle'},350);
};

Catalyze.map.toggledetailevent = function(){
	$(".sidemenu-country-wrapper").animate({width:'toggle'},350);
	$(".sidedetail-event-wrapper").animate({width:'toggle'},350);
};

Catalyze.map.hidemodal = function(){
	$(".map-wrapper .modal-map").hide();
	Catalyze.map.toggleSideMenu();
	
};


$(function(){
	
	 $(".navbar-default .navbar-nav a").append("<span></span>");
	 
	if($("#map-canvas").length > 0)
	{
		google.maps.event.addDomListener(window, 'load', Catalyze.map.init);
	}
	
	$(".coloumn-body .head").click(function(){
		$(this).next(".coloumn-detail").slideToggle();
		$(this).parent().toggleClass("active");
		
	});
	
	$(".sidemenu-country-wrapper ul li").click(function(){
		Catalyze.map.toggledetailevent();
	});
	
	 $('#media').carousel({
	    pause: true,
	    interval: false,
	  });
	  
	 
});
