<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function thumbnail($filename)
{
	//return strrchr($filename, ".");
	$ext = substr(strrchr($filename, "."), 1);
	return str_ireplace(substr(strrchr($filename, "."), 0), '_thumb.'.$ext, $filename);
}

function mediumname($filename)
{
	//return strrchr($filename, ".");
	$ext = substr(strrchr($filename, "."), 1);
	return str_ireplace(substr(strrchr($filename, "."), 0), '_med.'.$ext, $filename);
}

function smallname($filename)
{
	//return strrchr($filename, ".");
	$ext = substr(strrchr($filename, "."), 1);
	return str_ireplace(substr(strrchr($filename, "."), 0), '_small.'.$ext, $filename);
}

function filename($filename)
{
	//return strrchr($filename, ".");
	$ext = substr(strrchr($filename, "."), 1);
	return str_ireplace(substr(strrchr($filename, "."), 0), '.'.$ext, $filename);
}


function fullname($filename)
{
	//return strrchr($filename, ".");
	$ext = substr(strrchr($filename, "."), 1);
	return str_ireplace(substr(strrchr($filename, "."), 0), '_full.'.$ext, $filename);
}

?>