jQuery(function($) {

	// close cbox
	$('.cancel.close-cbox').livequery('click', function(){
		$.colorbox.close();
	});

	$('a[rel="colorbox"]').livequery(function(){
		$(this).colorbox({
			maxWidth	: '80%',
			maxHeight	: '80%'
		});
	});

	$('#grid').livequery(function(){
		if ($.cookie('content_file_view') != 'grid')
		{
			$('#grid').hide();
		}
		else
		{
			$('#list').hide();
			$('#grid').fadeIn();
			$('a.active-view').removeClass('active-view');
			$("a[title='grid']").addClass('active-view');
		}
	});

	$('a.toggle-view').livequery('click', function(e){
		e.preventDefault();

		var view = $(this).attr('title');

		// remember the user's preference
		$.cookie('content_file_view', view);

		$('a.active-view').removeClass('active-view');
		$(this).addClass('active-view');

		if (view == 'grid')
		{
			hide_view = 'list';
		}
		else
		{
			hide_view = 'grid';
		}

		$('#'+hide_view).fadeOut(50, function() {
			$('#'+view).fadeIn(500);   
		});            
	});

	pyro.init_upload = function($form){
		$form.find('form').fileUploadUI({
			fieldName       : 'file',
			uploadTable     : $('#files-uploader-queue'),
			downloadTable   : $('#files-uploader-queue'),
			previewSelector : '.file_upload_preview div',
			cancelSelector  : '.file_upload_cancel div.cancel-icon',
			buildUploadRow	: function(files, index, handler){
				var resize = '',
					type = files[index]['type'];
				// if it isn't an image then they can't resize it
				if (type && type.search('image') >= 0) {
					resize = 	'<label id="width">'+pyro.lang.width+'</label>'+
								'<select name="width" class="skip"><option value="0">'+pyro.lang.full_size+'</option><option value="100">100px</option><option value="200">200px</option><option value="300">300px</option><option value="400">400px</option><option value="500">500px</option><option value="600">600px</option><option value="700">700px</option><option value="800">800px</option><option value="900">900px</option><option value="1000">1000px</option><option value="1100">1100px</option><option value="1200">1200px</option><option value="1300">1300px</option><option value="1400">1400px</option><option value="1500">1500px</option><option value="1600">1600px</option><option value="1700">1700px</option><option value="1800">1800px</option><option value="1900">1900px</option><option value="2000">2000px</option></select>'+
								'<label id="height">'+pyro.lang.height+'</label>'+
								'<select name="height" class="skip"><option value="0">'+pyro.lang.full_size+'</option><option value="100">100px</option><option value="200">200px</option><option value="300">300px</option><option value="400">400px</option><option value="500">500px</option><option value="600">600px</option><option value="700">700px</option><option value="800">800px</option><option value="900">900px</option><option value="1000">1000px</option><option value="1100">1100px</option><option value="1200">1200px</option><option value="1300">1300px</option><option value="1400">1400px</option><option value="1500">1500px</option><option value="1600">1600px</option><option value="1700">1700px</option><option value="1800">1800px</option><option value="1900">1900px</option><option value="2000">2000px</option></select>'+
								'<label id="ratio">'+pyro.lang.ratio+'</label>'+
								'<input name="ratio" type="checkbox" value="1" checked="checked"/>'+
								'<label id="alt">'+pyro.lang.alt_attribute+'</label>'+
								'<input type="text" name="alt_attribute" class="alt_attribute" />';
				}
				// build the upload html for this file
				return $('<li>'+
							'<div class="file_upload_preview ui-corner-all"><div class="ui-corner-all preview-container"></div></div>' +
							'<div class="filename"><label for="file-name">' + files[index].name + '</label>' +
								'<input class="file-name" type="hidden" name="name" value="'+files[index].name+'" />' +
							'</div>' +
							'<div class="file_upload_progress"><div></div></div>' +
							'<div class="file_upload_cancel">' +
								'<div title="'+pyro.lang.start+'" class="start-icon ui-helper-hidden-accessible"></div>'+
								'<div title="'+pyro.lang.cancel+'" class="cancel-icon"></div>' +
							'</div>' +
							'<div class="image_meta">'+
								resize+
							'</div>'+
						'</li>');
			},
			buildDownloadRow: function(results){
				if (results.message)
				{
					$(window).trigger('show-message', results);
				}
			},
			beforeSend: function(event, files, index, xhr, handler, callBack){

				if( ! handler.uploadRow ) //happens if someone trys to upload more than he's allowed to, e.g. during file replace
				{
					return;
				}

				var $progress_div = handler.uploadRow.find('.file_upload_progress'),
					regexp;

				// check if the server can handle it
				if (files[index].size > pyro.files.max_size_possible) {
					$progress_div.html(pyro.lang.exceeds_server_setting);
					return false;
				} else if (files[index].size > pyro.files.max_size_allowed) {
					$progress_div.html(pyro.lang.exceeds_allowed);
					return false;
				}

				// is it an allowed type?
				regexp = new RegExp(pyro.files.valid_extensions);
				// Using the filename extension for our test,
				// as legacy browsers don't report the mime type
				if (!regexp.test(files[index].name.toLowerCase())) {
					$progress_div.html(pyro.lang.file_type_not_allowed);
					return false;
				}

				handler.uploadRow.find('div.start-icon').on('click', function() {
					handler.formData = {
						name: handler.uploadRow.find('input.file-name').val(),
						width: handler.uploadRow.find('[name="width"]').val(),
						height: handler.uploadRow.find('[name="height"]').val(),
						ratio: handler.uploadRow.find('[name="ratio"]').is(':checked'),
						alt_attribute: handler.uploadRow.find('[name="alt_attribute"]').val(),
						folder_id: pyro.files.upload_to,
						replace_id: 'mode' in pyro.files && pyro.files.mode == 'replace' ? pyro.files.$last_r_click.attr('data-id') : 0,
						csrf_hash_name: $.cookie(pyro.csrf_cookie_name)
					};
					callBack();
				});
			},

			onComplete: function (event, files, index, xhr, handler){
				if (files.length === index + 1) {
					$('#files-uploader a.cancel-upload').click();
				}
			}
		});

		$form.on('click', '.start-upload', function(e){
			e.preventDefault();
			$('#files-uploader-queue div.start-icon').click();
		});

		$form.on('click', '.cancel-upload', function(e){
			e.preventDefault();
			$('#files-uploader-queue div.cancel-icon').click();
			$.colorbox.close();
		});

	};

	pyro.init_upload($('#files-uploader'));


});
