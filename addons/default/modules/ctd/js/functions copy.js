jQuery(function($) {

	// close cbox
	$('.cancel.close-cbox').livequery('click', function(){
		$.colorbox.close();
	});

	// Bind an event to window.onhashchange that, when the hash changes, gets the
	// hash and adds reload contents

	$(window).hashchange(function(){
		var hash = location.hash.substr(2),
			uri,
			path = '';

		if (hash.match('path='))
		{
			uri = (hash == '' || ( ! (path = hash.match(/path=(.+?)(&.*|)$/)) && ! (hash = ''))) ? 'create' : 'contents/';
		}
		else
		{
			uri		= 'create';
			hash	= '#event-partners';
			path	= null;
		}

///		$.get(SITE_URL + 'admin/content/' + uri, hash, function(data){
		$.get(SITE_URL + 'admin/ctd/logos', function(data){

			if (data.status == 'success')
			{
//				data.navigation && $('#files-browser-nav').html(data.navigation);
				data.content && $('#files-browser-contents').html(data.content);
			}
			else if (data.status == 'error')
			{
				parent.location.hash = null;
				pyro.add_notification(data.message);
			}
			
			// Update Chosen
			pyro.chosen();

		}, 'json');
	});

	// Files -------------------------------------------------------

	$(".edit_file").livequery(function(){
		$(this).colorbox({
			scrolling	: false,
			width		: '85%',
			height		: '500',
			onComplete: function(){
				var form = $('form#files_crud'),
					$loading = $('#cboxLoadingOverlay, #cboxLoadingGraphic');

				$.colorbox.resize();
				
				// Update Chosen
				pyro.chosen();

				form.find(':input:last').keypress(function(e){
					if (e.keyCode == 9 && ! e.shiftKey)
					{
						e.preventDefault();
						form.find(':input:first').focus();
					}
				});

				form.find(':input:first').keypress(function(e){
					if (e.keyCode == 9 && e.shiftKey)
					{
						e.preventDefault();
						form.find(':input:last').focus();
					}
				});
			},
			onClosed: function(){}
		});
	});

	$('.open-files-uploader').livequery(function(){
		$(this).colorbox({
			scrolling	: false,
			inline		: true,
			href		: '#files-uploader',
			width		: '800',
			height		: '80%',
			onComplete	: function(){
				$('#files-uploader-queue').empty();
				$.colorbox.resize();
			},
			onCleanup : function(){
				$(window).hashchange();
			}
		});
	});

	var upload_form = $('#files-uploader form'),
		upload_vars	= upload_form.data('fileUpload');

	upload_form.fileUploadUI({
		fieldName       : 'userfiles',
		uploadTable     : $('#files-uploader-queue'),
		downloadTable   : $('#files-uploader-queue'),
		previewSelector : '.file_upload_preview div',
    cancelSelector  : '.file_upload_cancel button.cancel',
		buildUploadRow	: function(files, index, handler){
			return $('<li><div class="file_upload_preview ui-corner-all"><div class="ui-corner-all"></div></div>' +
					'<div class="filename"><label for="file-name">' + files[index].name + '</label>' +
					'<input class="file-name" type="hidden" name="name" value="'+files[index].name+'" />' +
					'<div><b>Title</b>:<br /><input id="filetitle" type="text" name="filetitle"><br />'+
					'<b>Description</b>:<br /><textarea id="filedescription" rows="3" cols="30" name="filedescription"></textarea></div>'+
					'</div>' +
					'<div class="file_upload_progress"><div></div></div>' +
					'<div class="file_upload_cancel buttons buttons-small">' +
					'<button class="button start ui-helper-hidden-accessible"><span>' + upload_vars.lang.start + '</span></button>'+
					'<button class="button cancel"><span>' + upload_vars.lang.cancel + '</span></button>' +
					'</div>' +
					'</li>');

		},
		buildDownloadRow: function(response){
			if (response.message)
			{
				pyro.add_notification(response.message, {
					clear: false
				});
			}
			if (response.status == 'success')
			{
				return $('<li><div>' + response.file.name + '</div></li>');
			}
			return;
		},
		beforeSend: function(event, files, index, xhr, handler, callBack){
			handler.uploadRow.find('button.start').click(function(){
				handler.formData = {
					name: handler.uploadRow.find('input.file-name').val(),
					content_id: $('input[name=content_id]', '#files-toolbar').val(),
					filetitle: handler.uploadRow.find('#filetitle').val(),
					filedescription: handler.uploadRow.find('#filedescription').val(),
				};
				callBack();
			});
		},
		onComplete: function (event, files, index, xhr, handler){
			handler.onCompleteAll(files);
		},
		onCompleteAll: function (files){
			if ( ! files.uploadCounter)
			{
				files.uploadCounter = 1;  
			}
			else
			{
				files.uploadCounter = files.uploadCounter + 1;
			}

			if (files.uploadCounter === files.length)
			{
				$('#files-uploader a.cancel-upload').click();
			}
		}
	});

	$('#files-uploader a.start-upload').click(function(e){
		e.preventDefault();
		$('#files-uploader-queue button.start').click();
	});

	$('#files-uploader a.cancel-upload').click(function(e){
		e.preventDefault();
		$('#files-uploader-queue button.cancel').click();
		$.colorbox.close();
	});

	$('a[rel="colorbox"]').livequery(function(){
		$(this).colorbox({
			maxWidth	: '80%',
			maxHeight	: '80%'
		});
	});

	$('#grid').livequery(function(){
		if ($.cookie('content_file_view') != 'grid')
		{
			$('#grid').hide();
		}
		else
		{
			$('#list').hide();
			$('#grid').fadeIn();
			$('a.active-view').removeClass('active-view');
			$("a[title='grid']").addClass('active-view');
		}
	});

	$('a.toggle-view').livequery('click', function(e){
		e.preventDefault();

		var view = $(this).attr('title');

		// remember the user's preference
		$.cookie('content_file_view', view);

		$('a.active-view').removeClass('active-view');
		$(this).addClass('active-view');

		if (view == 'grid')
		{
			hide_view = 'list';
		}
		else
		{
			hide_view = 'grid';
		}

		$('#'+hide_view).fadeOut(50, function() {
			$('#'+view).fadeIn(500);   
		});            
	});


});
