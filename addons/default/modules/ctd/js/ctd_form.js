(function($) {
	$(function(){

		var form = $('form.crud');

		// generate a slug when the user types a title in
		pyro.generate_slug('#frmEvent input[name="title"]', 'input[name="slug"]');
		
		$('#addCategory').colorbox({
			srollable: false,
			innerWidth: 530,
			innerHeight: 280,
			href: BASE_URI + 'admin/ctd/categories/create_ajax',
			onComplete: function() {
				$.colorbox.resize();
				$('form#categories').removeAttr('action');
				$('form#categories').livequery('submit', function(e) {
					
					var form_data = $(this).serialize();
					
					$.ajax({
						url: BASE_URL + 'admin/ctd/categories/create_ajax',
						type: "POST",
					    data: form_data,
						success: function(data) {
							
							var obj = $.parseJSON(data);
							if(obj.status == 'ok') {
								
								//succesfull db insert do this stuff
								var select = 'select[name=category_id]';
								var opt_val = obj.category_id;
								var opt_text = obj.title;
								var option = '<option value="'+opt_val+'" selected="selected">'+opt_text+'</option>';
								
								//append to dropdown the new option
								$(select).append(option);
								//$.uniform.update();
																
								//uniform workaround
								$('#event-content li.category_id span').html(obj.title);
								
								//close the colorbox
								$.colorbox.close();
							} else {
								//no dice
							
								//append the message to the dom
								$('#cboxLoadedContent').html(obj.message + obj.form).show();
								$.colorbox.resize();
							}
						}
						
						
					});
					e.preventDefault();
				});
				
			}
		});

		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd'});

		//Show and Initialize Google Maps
		$('#geolink').click(function () {
			var latlong = $('.geocoordinates').val();
			var address =  latlong != '' ? latlong : $('.geolocation').val();

			$('.map_area').slideDown('500', function() {
				initialize(address);
			});
		});
		$('.geolocation').keyup(function(){
			$('#address').val($(this).val());
		});
		$('#address').keyup(function(){
			$('.geolocation').val($(this).val());
		});

		//Hide Google Maps
		$('.close_map').click(function () {
			$('.map_area').slideUp('500');
		});

	});
})(jQuery);

// Google Maps Handling Functions
var map;
var geocoder;
var marker;

function initialize(address) {
	geocoder = new google.maps.Geocoder();
	
	var myOptions = {
		zoom: 13,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

	marker = new google.maps.Marker({
		map:map,
		draggable:true,
		animation: google.maps.Animation.DROP
	});

	google.maps.event.addListener(marker, 'click', toggleBounce);
	google.maps.event.addListener(marker, 'dragend', updateDrag);
	
	if(address && address != '')
		mapByLatLong(address);
	else 
		mapGeolocation();
}

function mapGeolocation() {
	// Try HTML5 geolocation
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			handleMap(position.coords.latitude, position.coords.longitude);
		},
		function() {
			mapNoGeolocation(true);
		});
	} else {
		// Browser doesn't support Geolocation
		mapNoGeolocation(false);
	}
}
function mapGeolocationXX() {
	// Try HTML5 geolocation
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			handleMap(position.coords.latitude, position.coords.longitude);
		},
		function() {
			mapNoGeolocation(true);
		});
	} else {
		// Browser doesn't support Geolocation
		mapNoGeolocation(false);
	}
}

function mapNoGeolocation(errorFlag) {
	if (errorFlag)
		var content = 'Error: The Geolocation service failed. Please check your browser settings to enable Geolocation';
	else
		var content = 'Error: Your browser doesn\'t support Geolocation.';

	alert(content);

	handleMap(60, 105);
}

function handleMap(lati, longi) {
	var pos = new google.maps.LatLng(lati, longi);
	returnCoord(pos);
	map.setCenter(pos);
	marker.setPosition(pos);
	returnaddress(pos);
}

function updateDrag() {
	pos = marker.getPosition();
	returnCoord(pos);
	map.setCenter(pos);
	returnaddress(pos);
}

function mapByLatLong(address) {
	//addr = address ? address.split(',') : $('#address').val();
	addr = address ? address : $('#address').val();
	geocoder.geocode( { 'address': addr}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK)
			handleMap(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		else
			alert("Could not locate the address for the following reason: \n\n" + status);
	});
}

function mapByAddress(address) {
	addr = address ? address : $('#address').val();
	geocoder.geocode( { 'address': addr}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK)
			handleMap(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		else
			alert("Could not locate the address for the following reason: \n\n" + status);
	});
}

function returnCoord(pos) {
	document.getElementById("lati").innerHTML = pos.lat().toFixed(5);
	document.getElementById("longi").innerHTML = pos.lng().toFixed(5);
	$('.geocoordinates').val(pos.lat().toFixed(5) + ',' + pos.lng().toFixed(5));
}

function returnaddress(pos) {
	geocoder.geocode( { 'location': pos}, function(results, status) {
		document.getElementById("address").value = results[0].formatted_address;
		$('.geolocation').val(results[0].formatted_address);
	});
}

function toggleBounce() {
	if (marker.getAnimation() != null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}
