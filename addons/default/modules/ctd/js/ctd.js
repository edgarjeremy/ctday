(function($) {
	$(function(){

/*
		$("#frmReg").live("submit", function() {

			$.colorbox.showActivity();
		
			$.ajax({
				type		: "POST",
				cache		: false,
				url			: $("#frmReg").attr("action"),
				data		: $(this).serializeArray(),
				success: function(data) {
					$.fancybox(data);
				}
			});
		
			return false;
		});
*/

		//load Event map
		if($("#map_canvas").length > 0) {
			var pos = $(".geocoordinates").attr("data-value");
			pos = pos.split(",");
			initialize(pos);
		}
		// Fancybox modal window
		var current_module = $('.contentArea h2:first').text();
		$('a[rel=modal], a.modal').livequery(function() {
			$(this).colorbox({
				width: "50%",
				height: "65%",
				onComplete: function() {
					//$.uniform.update();
				},
				scrolling: true,
				//current: current_module + " {current} / {total}"
				iframe: true,
				escKey: true
			});
		});

		$('a[rel="modal-large"], a.modal-large').livequery(function() {
			$(this).colorbox({
				width: "75%",
				height: "85%",
				iframe: true,
				//current: current_module + " {current} / {total}"
				scrolling: true,
				escKey: true
			});
		});
		// End Fancybox modal window

	});
})(jQuery);

// Google Maps Handling Functions
var map;
var marker;

function initialize(pos) {
	
	var myOptions = {
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

	marker = new google.maps.Marker({
		map:map,
		draggable:false
	});

	if(pos)
		handleMap(pos[0], pos[1]);
	else
		document.getElementById("map_canvas").style.display = 'none';
}




function handleMap(lati, longi) {
	var pos = new google.maps.LatLng(lati, longi);
	map.setCenter(pos);
	marker.setPosition(pos);
}

// google.maps.event.addDomListener(window, 'load', initialize);

// End Of Google Maps Handling Functions
 
