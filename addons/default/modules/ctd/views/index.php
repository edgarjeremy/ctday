	<h2><?php echo lang('agenda_agenda_title'); ?></h2>


	<?php if (!empty($category->title) && $category->title): ?>
		<h2 id="page_title"><?php echo lang('agenda_category_label') . ': '; echo $category->title; ?></h2>
	<?php endif; ?>

	<?php if (!empty($ctd)): ?>

	<?php
		foreach ($ctd as $article):
	?>

			<div class="agenda">
				<h3><?php echo  anchor('ctd/' . $article->slug, $article->title); ?></h3>
				<?php 
					$day = date('d', $article->date_from);
					$month = date('M', $article->date_from);
					$year = date('Y', $article->date_from);
				?>
				<span class="date">
					<span class="caldate" title="<?php echo date('l', $article->date_from); ?>">
						<span class="calday"><?php echo $day; ?></span>
						<span class="calmonth"><?php echo $month; ?></span>
						<span class="calyear"><?php echo $year; ?></span>
					</span>
				</span>
				<div class="clr"><br /></div>
				<!-- intro -->
				<?php echo ( $article->intro ? $article->intro : stripslashes(word_limiter(strip_tags($article->body, 150))) ); ?>
				<!-- /intro -->
			</div>
			<div class="clr"><br /></div>

	<?php
		endforeach;

	else:
		echo lang('agenda_no_events');
	endif;

	?>

	<?php echo $pagination['links']; ?>

