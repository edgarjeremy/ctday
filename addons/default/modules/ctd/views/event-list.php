
	<?php echo $calendar; ?>

		<div id="upcoming">
			<ul>
			<?php if (!empty($events)): ?>
            	<?php foreach($events as $e): ?>
				<li>
					<h2><a href="<?php echo site_url('agenda/'.$e->slug); ?>"><?php echo $e->title; ?></a></h2>
					<div class="date"><?php echo strftime('%d %B', $e->date_from) . ( ($e->date_to && date('Y/m/d', $e->date_to) <> date('Y/m/d', $e->date_from) ) ? ' - ' .strftime('%d %B', $e->date_to) : '' ) ; ?></div>
					<?php if ($show_intro == 'true') echo $e->intro; ?>
				</li>
                <?php endforeach; ?>
			<?php else: ?>
				<li><?php echo lang('agenda_no_events_in_month'); ?></li>
			<?php endif; ?>
			</ul>
		</div>

