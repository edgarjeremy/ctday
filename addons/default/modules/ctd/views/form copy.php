<div class="row" style="margin-bottom: -32px;">
    <div class="form-event-wrapper">
        <?php echo form_open_multipart($this->uri->uri_string(), 'class="crud row" id="frmEvent" style="margin: 0px;"'); ?>
            <label class="col-xs-12 title">Event Submission</label>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="name">Your Name*</label>
                <input type="text" class="form-control" id="name">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        Name are for event clarification only, and will not be displayed on the website
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="email">Your Email Address*</label>
                <input type="text" class="form-control" id="email">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        Email are for event clarification only, and will not be displayed on the website
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 form-group">
                <label for="eventname">Event Name* <span><span id="eventcharacter">67</span> character left</span>
                </label>
                <input type="text" class="form-control" id="eventname" maxlength="67">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        E.g Underwater & beach clean-up 2015
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 form-group">
                <label for="eventdescription">Event Description* <span><span id="desccharacter">530</span> character left</span>
                </label>
                <textarea class="form-control" name="eventdescription" id="eventdescription" maxlength="530"></textarea>
                <ul class="dropdown-menu popovernote top textarea" role="menu">
                    <li role="presentation">
                        Describe the event's activities and if open to public: what must they bring/do to participate
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="eventdate">Event Date*</label>
                <input type="text" class="form-control" id="eventdate" placeholder="From">
                <ul class="dropdown-menu popovernote top" role="menu">
                    <li role="presentation">
                        You only need to fill this date field if it's a one-day event
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="checkboxmultidayevent"> Multi-Day Event
                    </label>
                </div>
                <input type="text" class="form-control" placeholder="To" id="multievent" disabled>
                <ul class="dropdown-menu popovernote top" role="menu">
                    <li role="presentation">
                        Fill this date field if the event goes beyond one day
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="eventvenue">Event Venue*</label>
                <input type="text" class="form-control" id="eventvenue">
                <ul class="dropdown-menu popovernote top" role="menu">
                    <li role="presentation">
                        Venue is for the location of the vent, e.g. hotel, beach, university, building
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="venueaddress">Venue Address*</label>
                <input type="text" class="form-control" id="venueaddress">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        The complete address of the venue
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="country">Country*</label>
                <input type="text" class="form-control" id="country">
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="city">City*</label>
                <input type="text" class="form-control" id="city">
            </div>
            <div class="col-xs-12 form-group">
                <label for="googlemap">Google Map*</label>
                <input type="text" class="form-control" id="searchgooglemap" placeholder="Geolocation">
                 <ul class="dropdown-menu popovernote top" role="menu">
                    <li role="presentation">
                         You know your city best, help us and the public locate your event by adding/correcting a Google Map pin on the venue
                    </li>
                </ul>
                <div class="form-control map-event-wrapper" id="mapevent"></div>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="website">Website</label>
                <input type="text" class="form-control" id="website">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        Only include a website if it’s directly related with the event - not intended for sponsor/organizer’s website
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="email">Contact Email</label>
                <input type="text" class="form-control" id="email">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        This email will be made visible to the public, so they can contact for inquiries
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label >Organizer <span class="more" onclick="addimgorganizer()">+ Add more organizer</span></label>
                <div class="clearfix"><br /></div>
                <ul class="list-form-upload" id="listimgorganizer">
                    <li>
                        <div class="input-group">
                            <input type="text" class="form-control" data-name="textimgorganizer" placeholder="Upload organizer logo" style="margin-bottom: 0px;" readonly>
                            <div class="input-group-btn">
                                <div class="fileUpload btn btn-default btn-browse">
                                    <span>Browse</span>
                                    <input type="file" class="upload" name="imgorganizer[]" />
                                </div>
                            </div>
                        </div>
                        <div class="input-group" style="width:100%">
                            <input class="form-control" placeholder="Add organizer website address" name="websiteorganizer[]" />
                        </div>
                    </li>

                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label>Sponsor <span class="more" onclick="addimgsponsor()">+ Add more sponsor</span></label>
                <div class="clearfix"><br /></div>
                <ul class="list-form-upload" id="listimgsponsor">
                    <li>
                        <div class="input-group">
                            <input type="text" class="form-control" data-name="textimgsponsor" placeholder="Upload organizer logo" style="margin-bottom: 0px;" readonly>
                            <div class="input-group-btn">
                                <div class="fileUpload btn btn-default btn-browse">
                                    <span>Browse</span>
                                    <input type="file" class="upload" name="imgsponsor[]" />
                                </div>
                            </div>
                        </div>
                        <div class="input-group" style="width:100%">
                            <input class="form-control" placeholder="Add organizer website address" name="websitesponsor[]" />
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <button class="btn btn-primary pull-right">Submit Event</button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
<script>
    function initialize() {
        var markers = [];
        var map = new google.maps.Map(document.getElementById('mapevent'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 5,
            center: new google.maps.LatLng(-1.4894799, 121.8823101)
        });

        //				  var defaultBounds = new google.maps.LatLngBounds(
        //				      new google.maps.LatLng(-33.8902, 151.1759),
        //				      new google.maps.LatLng(-33.8474, 151.2631));
        //				  map.fitBounds(defaultBounds);

        // Create the search box and link it to the UI element.
        var input = document.getElementById('searchgooglemap');
        //				  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var searchBox = new google.maps.places.SearchBox(input);

        // [START region_getplaces]
        // Listen for the event fired when the user selects an item from the
        // pick list. Retrieve the matching places for that item.
        google.maps.event.addListener(searchBox, 'places_changed', function() {

            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }

            // For each place, get the icon, place name, and location.
            markers = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    draggable: true,
                    position: place.geometry.location
                });

                markers.push(marker);

                bounds.extend(place.geometry.location);
                map.setCenter(place.geometry.location);
                map.setZoom(15);
            }

            //				    map.fitBounds(bounds);
        });
        // [END region_getplaces]

        // Bias the SearchBox results towards places that are within the bounds of the
        // current map's viewport.
        google.maps.event.addListener(map, 'bounds_changed', function() {
            var bounds = map.getBounds();
            searchBox.setBounds(bounds);
        });
    }


    function addimgorganizer() {
        var html = '<li>' 
                + '	<div class="input-group">' 
                + '		<input type="text" class="form-control" data-name="textimgorganizer" placeholder="Upload organizer logo" style="margin-bottom: 0px;" readonly>' 
                + '		<div class="input-group-btn">' 
                + '		  <div class="fileUpload btn btn-default btn-browse">' 
                + '			    <span>Browse</span>' 
                + '			    <input type="file" class="upload" name="imgorganizer[]"/>' 
                + '			</div>' 
                + '		</div>' 
                + '	</div>'
                + '	<div class="input-group" style="width:100%">' 
                + '		<input class="form-control" placeholder="Add organizer website address" name="websiteorganizer[]"/>' 
                + '	</div>' 
                + '</li>';

        $("#listimgorganizer").append(html);

        $("input[name='imgorganizer[]']").change(function() {
            var index = $("input[name='imgorganizer[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgorganizer]:eq(" + index + ")").val(value);
        });
    }

    function addimgsponsor() {
        var html = '<li>' 
                + '		<div class="input-group">' 
                + '			<input type="text" class="form-control" data-name="textimgsponsor" placeholder="Upload organizer logo" style="margin-bottom: 0px;" readonly>' 
                + '			<div class="input-group-btn">' + '			  <div class="fileUpload btn btn-default btn-browse">' 
                + '				    <span>Browse</span>'
                 + '				    <input type="file" class="upload" name="imgsponsor[]"/>' 
                 + '				</div>' 
                 + '			</div>' 
                 + '		</div>' 
                 + '		<div class="input-group" style="width:100%">' 
                 + '			<input class="form-control" placeholder="Add organizer website address" name="websitesponsor[]"/>' 
                 + '		</div>' 
                 + '	</li>';

        $("#listimgsponsor").append(html);

        $("input[name='imgsponsor[]']").change(function() {
            var index = $("input[name='imgsponsor[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgsponsor]:eq(" + index + ")").val(value);
        });
    }

    $(function() {
        Catalyze.map.style = [];
        google.maps.event.addDomListener(window, 'load', initialize);

        $("input[name='imgorganizer[]']").change(function() {
            var index = $("input[name='imgorganizer[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgorganizer]:eq(" + index + ")").val(value);
        });

        $("input[name='imgsponsor[]']").change(function() {
            var index = $("input[name='imgsponsor[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgsponsor]:eq(" + index + ")").val(value);
        });

        $("#eventname").keydown(function() {
            var length = $(this).attr("maxlength") - $(this).val().length;
            $("#eventcharacter").text(length);
        });
        $("#eventdescription").keydown(function() {
            var length = $(this).attr("maxlength") - $(this).val().length;
            $("#desccharacter").text(length);
        });

        $("#checkboxmultidayevent").click(function() {

            if (this.checked) {
                $('#multievent').removeAttr("disabled");
            } else {
                $(this).val('');
                $('#multievent').attr("disabled", "disabled");
            }

        })

		$('#eventdate').datepicker();
		$('#multievent').datepicker();

        $('input[type=text]').focus(function() {
            $(this).next(".popovernote").toggle();
        });
        $('textarea').focus(function() {
            $(this).next(".popovernote").toggle();
        });
        $('input[type=text]').blur(function() {
            $(this).next(".popovernote").toggle();
        })
        $('textarea').blur(function() {
            $(this).next(".popovernote").toggle();
        })
    });
</script>