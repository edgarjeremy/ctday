
    <label class="title"><?php echo $event->title; ?></label>
    <label>
    	<?php echo date('d M Y', $event->date_from); ?>
    	<?php
    	if ($event->date_to)
    		echo ' - ' . date('d M Y', $event->date_to);
    	?>
    </label>

    <p class="description"><?php echo $event->intro; ?></p>

    <label class="title">VENUE ADDRESS</label>
    <p><?php echo $event->venue_address; ?></p>

    <?php if ($event->contact_email): ?>
	    <label class="title">CONTACT</label>
	    <p><?php echo $event->contact_email ? $event->contact_email : '--'; ?></p>
	<?php endif; ?>

    <?php if ($event->website): ?>
    <label class="title">EVENT URL</label>
    <p>
	<?php    	
    		$website = str_ireplace('http://', '', $event->website);
    		$url = 'http://'.$website;
    		echo '<a href="'.$url.'" target="_blank">'.$website.'</a>';
    ?>
    </p>
    <?php endif; ?>

	<?php if ($organizers): ?>
    <div class="organizer-wrapper">
        <label class="title">ORGANIZER</label>

        	<?php foreach($organizers as $organizer): ?>
        		<?php if ($organizer->external_url): ?>
					<?php
						if (substr($organizer->external_url, 0, 5) <> 'http:')
						$organizer->external_url = 'http://'.$organizer->external_url;
					?>
	        		<a target="_blank" href="<?php echo $organizer->external_url ?>"><img style="height:40px;margin-right:10px;" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$organizer->gallery_id.'/'.thumbnail($organizer->media)) ?>" /></a>
        		<?php else: ?>
	        		<img style="height:40px;margin-right:10px;" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$organizer->gallery_id.'/'.thumbnail($organizer->media)) ?>" />
	        	<?php endif; ?>
        	<?php endforeach; ?>
	</div>
	<?php endif; ?>

	<?php if ($sponsors): ?>
    <div class="sponsor-wrapper">
        <label class="title">SPONSOR</label>

        	<?php foreach($sponsors as $sponsor): ?>
        		<?php if ($sponsor->external_url): ?>
					<?php
						if (substr($sponsor->external_url, 0, 5) <> 'http:')
						$sponsor->external_url = 'http://'.$sponsor->external_url;
					?>
	        		<a target="_blank" href="<?php echo $sponsor->external_url ?>"><img style="height:40px;margin-right:10px;" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$sponsor->gallery_id.'/'.thumbnail($sponsor->media)) ?>" /></a>
        		<?php else: ?>
	        		<img style="height:40px;margin-right:10px;" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$sponsor->gallery_id.'/'.thumbnail($sponsor->media)) ?>" />
	        	<?php endif; ?>
        	<?php endforeach; ?>
	</div>
	<?php endif; ?>


