<h1></h1>
<div class="float-left width-two-thirds">
	<?php if (!empty($events)):
		$i=1;
	?>

		<?php foreach ($events as $article): ?>

		<div class="news_article">
			<!-- Article heading -->
			<?php if ($article->attachment): ?>
				<img src="/media/agenda/thumbs/<?php echo $article->attachment; ?>" align="left" style="padding-right: 10px; padding-bottom: 10px;" />
			<?php endif; ?>
			<div class="article_heading">
				<h2><?php echo  anchor('agenda/' . $article->slug, $article->title); ?> <?php if (@$article->repeat_type > 0) echo theme_image('repeated.png'); ?></h2>
				<p class="article_date"><?php echo lang('events_date_label');?>: <?php echo date('M d, Y; H:i', $article->date_from) .' GMT '. ($article->time_zone ? (string)$timezones[$article->time_zone] : $timezones[$this->settings->default_timezone]); ?></p>
				<?php if($article->category_slug): ?>
				<p class="article_category">
					<?php echo lang('events_category_label');?>: <?php echo anchor('agenda/category/'.$article->category_slug, $article->category_title);?>
				</p>
				<?php endif; ?>
			</div>
			<div class="article_body">
				<p><?php echo ( $article->intro ? $article->intro : stripslashes(word_limiter(strip_tags($article->body, 150))) ); ?></p>
			</div>
		</div>
        <div class="clr"></div>
        <hr />

		<?php endforeach; ?>

		<?php echo $pagination['links']; ?>

	<?php else: ?>

		<p style="width:760px;"><?php echo lang('news_currently_no_articles');?></p>

	<?php endif; ?>
</div>

