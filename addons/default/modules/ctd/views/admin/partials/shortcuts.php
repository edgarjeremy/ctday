<nav id="shortcuts">
	<h6><?php echo lang('cp_shortcuts_title'); ?></h6>
	<ul class="list-links">
		<li><?php echo anchor('/admin/agenda/create', lang('agenda_create_title'), 'class="add"') ?></li>
		<li><?php echo anchor('/admin/agenda', lang('agenda_list_title')); ?></li>
		<li><?php echo anchor('/admin/agenda/categories/create', lang('cat_create_title'), 'class="add"'); ?></li>
		<li><?php echo anchor('/admin/agenda/categories', lang('cat_list_title'))?></li>
	</ul>
	<br class="clear-both" />
</nav>

