<style>
.filter-am{margin:0px auto!important;}
</style>
<fieldset id="filters">
	<legend><?php echo lang('global:filters'); ?></legend>
	<?php echo form_open(''); ?>
		<ul>
			<li class="filter-am">
        		<?php echo form_dropdown('f_year', array(''=>'Year')+$filter_year); ?>
    		</li>
			<li class="filter-am">
        		<?php echo form_dropdown('f_country', array(''=>'All Country')+$countries); ?>
    		</li>
		</ul>
	<?php echo form_close(); ?>
</fieldset>