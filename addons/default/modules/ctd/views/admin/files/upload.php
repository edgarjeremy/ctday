<h2><?php echo lang('content_edit_slide_title'); ?> - <?php //echo $slide->filename;?></h2>
<?php
echo "<pre>";
print_r($slide);
echo "</pre>";
?>
<?php echo form_open_multipart(uri_string(), array('class' => 'crud', 'id' => 'files_crud')); ?>
<fieldset>
	<ul>
		<li class="even">
		<label></label>
			<img title="<?php echo $slide->filename; ?>" src="<?php echo '/'.UPLOAD_PATH.'news/slides/'. substr($slide->filename, 0, -4) . '_thumb' . substr($slide->filename, -4);?>" alt="<?php echo $slide->filename; ?>" />
		</li>
		<li>
			<?php echo form_label(lang('content_title_label'), 'title'); ?>
			<?php echo form_input('name', $slide->name); ?>
		</li>
		<li class="even">
			<?php echo form_label(lang('content_description_label'), 'description'); ?>
			<?php echo form_textarea(array(
				'name'	=> 'description',
				'id'	=> 'description',
				'value'	=> $slide->description,
				'rows'	=> '3',
				'cols'	=> '30'
			)); ?>
		</li>
	</ul>

	<div class="align-right buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
	</div>
</fieldset>
<?php echo form_close(); ?>