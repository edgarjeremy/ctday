<?php 
//echo "<pre>";
//print_r($file);
//echo "</pre>";
// Ini file kaga kepake braaaaayy lasuut aah
?>
<h2><?php echo sprintf(lang('files.edit_title'), $file->filename); ?></h2>

<?php echo form_open_multipart(uri_string(), array('class' => 'crud', 'id' => 'files_crud')); ?>
<fieldset>
	<ul>
		<li class="odd">
		<label></label>
		  <?php if($file->type == 'i'): ?>
			<img title="<?php echo $file->filename; ?>" src="<?php echo '/'.UPLOAD_PATH.'ctd/'. thumbnail($file->filename);?>" alt="<?php echo $file->filename; ?>" />
			<?php endif; ?>
		</li>
		<li>
			
		<li class="even">
			<?php echo form_label(lang('files.name_label'), 'name'); ?>
			<?php echo form_input('name', $file->name, 'class="crud"'); ?>
			<?php echo form_hidden('filename', $file->filename); ?>	
		</li>
		<li>
			<?php echo form_label(lang('files.description_label'), 'description'); ?>
			<?php echo form_textarea(array(
				'name'	=> 'description',
				'id'	=> 'description',
				'value'	=> $file->description,
				'rows'	=> '3',
				'cols'	=> '50'
			)); ?>
		</li>
		<li>
			<?php echo form_label(lang('files.file_label'), 'userfile'); ?>
			<?php echo form_upload(array(
				'name'	=> 'userfile',
				'id'	=> 'userfile'
			)); ?>
		</li>
	</ul>

	<div class="align-right buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
	</div>
</fieldset>
<?php echo form_close(); ?>