<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

	<?php if ($files): ?>

	<div id="files-display">
    <a href="#" title="grid" class="toggle-view"><?php echo lang('ctd_display_grid'); ?></a>
    <a href="#" title="list" class="toggle-view active-view"><?php echo lang('ctd_display_list'); ?></a>
	</div>
	
	<table border="0" class="table-list" id="list" width="100%">
		<thead>
			<tr>
				<!-- <th width="20" class="align-center">#</th> -->
				<th width="150"><?php echo lang('ctd_file_name_label'); ?></th>
				<th width="150"><?php echo lang('ctd_partner_name_label'); ?></th>
				<th width="300"><?php echo lang('ctd_description_label'); ?></th>
				<th width="100" class="align-center"><?php echo lang('ctd_file_created_label'); ?></th>
				<th width="200"> </th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="7">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($files as $file): ?>
			<tr>
				<!-- <td class="align-center"><?php echo $file->id; ?></td> -->
				<?php echo form_hidden('file_id[]', $file->id); ?>
				<td><a class="colorbox" title="<?php echo $file->name; ?>" href="<?php echo base_url() . UPLOAD_PATH . 'ctd/' . $file->filename; ?>" rel="cbox1"><?php echo strlen($file->filename) > 25 ? '<span title="' . $file->filename . '">' . ellipsize($file->filename, 25, .8) . '</span>' : $file->filename; ?></a></td>
				<td><?php echo strlen($file->name) > 25 ? '<span title="' . $file->name . '">' . ellipsize($file->name, 25, .8) . '</span>' : $file->name; ?></td>
				<td><?php echo $file->description; ?></td>
				<td class="align-center"><?php echo format_date($file->date_added); ?></td>
				<td class="actions">
				<?php
						echo anchor('ctd/download/' . $file->id, lang('global:view'), array('class' => 'button download download_file')).' ';
						echo anchor('admin/ctd/editfile/' . $file->id, lang('buttons:edit'), array('class' => 'button edit edit_file')).' ';
						echo anchor('admin/ctd/deletepartner/' . $file->id, lang('buttons:delete'), array('class'=>'button delete'));
				?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

  <div id="grid" class="list-items">
        <?php //echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all grid-check-all')); ?><?php //echo lang('global:check-all'); ?><br />
        <ul class="grid clearfix">
        <?php foreach($files as $file): ?>
            <li>
                <div class="actions">
                <?php //echo form_checkbox('action_to[]', $file->id); ?>
				<?php
						echo anchor('ctd/download/' . $file->id, lang('global:view'), array('class' => 'download_file'));
						echo anchor('admin/ctd/editfile/' . $file->id, lang('buttons:edit'), array('class' => 'edit_file'));
						echo anchor('admin/ctd/deletepartner/' . $file->id, lang('buttons:delete'), array('class'=>'delete'));
				?>
                </div>
            <?php if ($file->type === 'i'): ?>
            <a title="<?php echo $file->name; ?>" href="<?php echo base_url() . UPLOAD_PATH . 'ctd/' . $file->filename; ?>" rel="colorbox">
                <img title="<?php echo $file->name; ?>" src="<?php echo site_url(UPLOAD_PATH.'ctd/' . thumbnail($file->filename)); ?>" alt="<?php echo $file->name; ?>" />
            </a>
            <?php else: ?>
                <?php echo image($file->type . '.png', 'files'); ?>
            <?php endif; ?>
            </li>
        <?php endforeach; ?>
        </ul>
	</div>
			
	<?php else: ?>
	<div class="no_data files">
		<p><?php echo lang('files:no_files'); ?></p>
	</div>
	<?php endif; ?>