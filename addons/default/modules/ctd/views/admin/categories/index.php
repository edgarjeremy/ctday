<section class="title">
	<h4><?php echo lang('cat_list_title');?></h4>
</section>

<section class="item">
<div class="content">

<?php if (!empty($categories)): ?>

<?php echo form_open('admin/ctd/categories/delete'); ?>
	<table border="0" class="table-list">
		<thead>
		<tr>
			<th style="width: 20px;"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
			<th><?php echo lang('cat_category_label');?></th>
			<th style="width:10em"><span><?php echo lang('cat_actions_label');?></span></th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="3">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ($categories as $category): ?>
			<tr>
				<td><?php echo form_checkbox('action_to[]', $category->id); ?></td>
				<td><?php echo $category->title;?></td>
				<td>
					<?php echo anchor('admin/ctd/categories/edit/' . $category->id, lang('cat_edit_label'), 'class="btn orange"') ?>
					<?php echo anchor('admin/ctd/categories/delete/' . $category->id, lang('cat_delete_label'), array('class'=>'confirm btn red'));?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div class="buttons float-right padding-top">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )); ?>
	</div>
<?php echo form_close(); ?>

<?php else: ?>

	<div class="no_data"><?php echo lang('cat_no_categories'); ?></div>

<?php endif; ?>

</div>
</section>