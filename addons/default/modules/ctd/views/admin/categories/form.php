
<section class="title">
<?php if($this->method == 'create'): ?>
	<h4><?php echo lang('cat_create_title');?></h4>
<?php else: ?>
	<h4><?php echo sprintf(lang('cat_edit_title'), $category->title);?></h4>
<?php endif; ?>
</section>

<section class="item">
<div class="content">

<?php echo form_open($this->uri->uri_string(), 'class="crud" id="categories"'); ?>

<fieldset>
	<ul>
		<li class="even">
			<label for="title"><?php echo lang('cat_title_label');?><span>*</span></label>
			<div class="input">
				<?php echo  form_input('title', $category->title, 'id="title"'); ?>
			</div>
		</li>
	</ul>

<div class="buttons float-right padding-top">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
</div>

</fieldset>

<?php echo form_close(); ?>

</div>
</section>