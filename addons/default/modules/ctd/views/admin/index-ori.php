<section class="title">
		<h4><?php echo lang('ctd_list_title');?></h4>
</section>


<section class="item">
<div class="content">

<?php echo form_open('admin/ctd/action');?>


		<?php if (!empty($ctd)): ?>
				
			<table border="0" class="table-list">    
				<thead>
					<tr>
						<th class="width-5"><?php echo form_checkbox('action_to_all', NULL, NULL, ' class="check-all"');?></th>
						<th class="width-15"><?php echo lang('ctd_post_label');?></th>
						<th class="width-10"><?php echo lang('ctd_date_label');?></th>
						<th class="width-10">Country</th>
						<th class="width-5">City</th>
						<th class="width-5"><?php echo lang('ctd_status_label');?></th>
						<th class="width-5">Approved?</th>
						<th class="width-10"><span><?php echo lang('ctd_actions_label');?></span></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="9">
							<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($ctd as $article): ?>
						<tr>
							<td><?php echo form_checkbox('action_to[]', $article->id);?></td>
							<td><?php echo $article->title;?></td>
							<td><?php echo date('d-M-Y', $article->date_from);?></td>
							<td><?php echo $countries[$article->country];?></td>
							<td><?php echo $article->location;?></td>
							<td><?php echo lang('ctd_'.$article->status.'_label');?></td>
							<td><?php if (!empty($article->approve)):?><font style="color:green;">Approved</font><?php else:?><font style="color:red;">Not Approved</font><?php endif;?></td>
							<td>
								<?php echo anchor('ctd/' . $article->slug, lang($article->status == 'live' ? 'ctd_view_label' : 'ctd_preview_label'), 'class="iframe btn green" target="_blank"'); ?>
								<?php echo anchor('admin/ctd/edit/' . $article->id, lang('ctd_edit_label'), 'class="btn orange"');?> 
								<?php echo anchor('admin/ctd/delete/' . $article->id, lang('ctd_delete_label'), array('class'=>'confirm btn red')); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>	
			</table>
			
<div class="table_action_buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete', 'publish') )); ?>
</div>

		<?php else: ?>

			<div class="no_data"><?php echo lang('ctd_currently_no_articles'); ?></div>

		<?php endif; ?>

<?php echo form_close();?>

</div>
</section>