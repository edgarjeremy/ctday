<section class="title">
	<h4><?php echo lang('ctd_att_list_title') . ' - ' . $event->title;?></h4>
</section>

<section class="item">
<div class="content">

<?php if (!empty($atts)): ?>

<?php echo form_open('admin/ctd/attendance/delete'); ?>

<!--
<div class="tabs">

	<ul class="tab-menu">
		<li><a href="#with-email"><span>With Email</span></a></li>
		<li><a href="#no-email"><span>No Email</span></a></li>
	</ul>
-->

	<div id="with-email">

	<table border="0" class="table-list">
		<thead>
		<tr>
			<th style="width: 20px;"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
			<th><?php echo lang('ctd_reg_email_label');?></th>
			<th><?php echo lang('ctd_reminder_label');?></th>
			<th><?php echo lang('ctd_notify_on_change_label');?></th>
			<th style="width:10em"><span><?php echo lang('ctd_actions_label');?></span></th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="3">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ($atts as $att): ?>
			<tr>
				<td><?php echo form_checkbox('action_to[]', $att->id); ?></td>
				<td><?php echo $att->email ? $att->email : '--';?></td>
				<td><?php echo lang($att->remind_me == 'y' ? 'ctd_yes_label' : 'ctd_no_label');?></td>
				<td><?php echo lang($att->inform_changes == 'y' ? 'ctd_yes_label' : 'ctd_no_label');?></td>
				<td>
					<?php echo anchor('admin/ctd/attendance/edit/' . $att->id, lang('ctd_edit_label'), 'class="btn orange"') ?>
					<?php echo anchor('admin/ctd/attendance/delete/' . $att->id, lang('ctd_delete_label'), array('class'=>'confirm btn red'));?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div class="buttons float-right padding-top">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )); ?>
	</div>

	</div><!-- /#with-email -->

<!--
	<div id="no-email">
		<p>No email..</p>
	</div>

</div><!-- /.tabs -->

<?php echo form_close(); ?>

<?php else: ?>

	<div class="no_data"><?php echo lang('ctd_no_attendance'); ?></div>

<?php endif; ?>


</div>
</section>