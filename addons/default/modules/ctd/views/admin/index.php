<section class="title">
	<h4><?php echo lang('ctd_list_title');?></h4>
</section>

<section class="item">
	<div class="content">
		<p><a href="admin/ctd/blast" class="btn orange blast">Email Blast</a></p>
		<?php $this->load->view('admin/partials/filters'); ?>
		<?php echo form_open('admin/ctd/action'); ?>

			<div id="filter-stage">
				<?php $this->load->view('admin/tables/posts'); ?>
			</div>
				
		<?php echo form_close(); ?>
	</div>
</section>
<script>
$(document).ready(function(){
	$('a.blast').click(function(){
		if (!confirm('Are you sure you want to send the email?'))
		{
			return false;
		}
		var h = $(this).attr('href');
		$(this).colorbox({
			width: "60%",
			height: "60%",
			fixed: true,
			overlayClose: false,
			escKey: false,
			html: '<p style="font-size:1.2em;height:25px;width:100%;text-align:center;">Please wait, sending email blast<br /></p>',
			onLoad: function(){
				//$('#cboxLoadedContent').html();
			}
			, onComplete: function(){
				$.ajax({
					url: h,
					success: function(data){
						$('#cboxLoadedContent').html(data);
						//$.colorbox.resize();
					}
				})
				//$.colorbox.resize();
			}
		});
	});
});
</script>