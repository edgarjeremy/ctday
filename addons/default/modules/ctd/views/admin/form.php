
<section class="title">
<?php if($this->method == 'create'): ?>
	<h4><?php echo lang('ctd_create_title');?></h4>
<?php else: ?>
	<h4><?php echo sprintf(lang('ctd_edit_title'), $article->title);?></h4>
<?php endif; ?>
</section>

<section class="item">
<div class="content">

<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud" id="frmEvent"'); ?>

<input name="article_id" id="article_id" value="<?php if (!empty($article->id)) echo $article->id; ?>" type="hidden">

			<div class="tabs">
			
				<ul class="tab-menu">
					<li><a href="#event-content"><span>Event</span></a></li>
					<li><a href="#event-location"><span><?php echo lang('ctd_location_label'); ?></span></a></li>
					<li><a href="#event-metadata"><span><?php echo lang('ctd_meta_data_label'); ?></span></a></li>
				</ul>
				
				<!-- Options content -->
				<div class="form_inputs" id="event-content">

					<fieldset>
					<ul>

						<li>
							<label for="title"><?php echo lang('ctd_title_label');?> </label>
							<div class="input"><?php echo form_input('title', htmlspecialchars_decode($article->title), 'style="width:450px;maxlength="100" id="title"'); ?></div>
						</li>

						<li>
							<label for="slug">Slug </label>
							<div class="input"><?php echo form_input('slug', $article->slug, 'maxlength="100" style="width:450px;" id="slug"'); ?></div>
						</li>

						<li class="date-meta">
    	            		<!-- date from -->
        	        		<div class="one_quarter">
					            <label for="date_from"><?php echo lang('ctd_from_label') . ' ' . lang('ctd_date_label');?> </label>                      
				                      <?php
				                      if($article->date_from){
				                        $date_from = date('Y-m-d', $article->date_from);
				                      }
				                      else
				                      {
						                $date_from = '';
				                      }
				                      ?>  
						            <div class="input datetime_input">
									<?php echo form_input('date_from_str', htmlspecialchars_decode($date_from), 'maxlength="10" id="date_from" class="text width-20 datepicker"'); ?>&nbsp;
						           	</div>
	                		</div>

							<div class="one_quarter">
					            <label for="date_to"><?php echo lang('ctd_to_label') . ' ' . lang('ctd_date_label');?></label>
	                      
			                      <?php
			                      if($article->date_to){
			                        $date_to = date('Y-m-d', $article->date_to);
			                      }
			                      else
			                      {
					                $date_to = '';
			                      }
	
			                      ?>
	  
					            <div class="input datetime_input">
								<?php echo form_input('date_to_str', htmlspecialchars_decode($date_to), 'maxlength="10" id="date_to" class="text width-20 datepicker"'); ?>&nbsp;
					           	</div>
							</div>
							<div class="clearfix"></div>
				        </li>

						<li>
							<label for="status"><?php echo lang('ctd_status_label');?></label>
							<div class="input"><?php echo form_dropdown('status', $ctd_status, $article->status, 'id="status"') ?></div>
						</li>

						<li>
							<div class="one_quarter">
								<label for="submitted_by_name">Submitted By</label>
								<div class="input">
									<?php echo form_input('submitted_by_name', $article->submitted_by_name, 'maxlength="100" id="submitted_by_name"'); ?>
								</div>
							</div>

							<div class="one_quarter">
								<label>Email</label>
								<div class="input">
									<?php echo form_input('submitted_by_email', $article->submitted_by_email, 'maxlength="100" id="submitted_by_email"'); ?>
								</div>
							</div>
							<div class="clearfix"></div>
						</li>

						<li>
							<div class="one_quarter">
								<label for="contact_email">Event Contact</label>
								<div class="input">
									<?php echo form_input('contact_email', $article->contact_email, 'maxlength="100" id="contact_email"'); ?>
								</div>
							</div>

							<div class="one_quarter">
								<label for="website">Website</label>
								<div class="input">
									<?php echo form_input('website', $article->website, 'maxlength="100" id="website"'); ?>
								</div>
							</div>
							<div class="clearfix"></div>
						</li>

						<li class="even">
							<label for="intro"><?php echo lang('ctd_intro_label');?></label><br />

							<?php echo form_textarea(array('id'=>'intro', 'name'=>'intro', 'value' => $article->intro, 'rows' => 5, 'class'=>'wysiwyg-simple')); ?>
						</li>
						
						<li>
							<label for="sponsors">Sponsors</label>
							<div class="input">
								<input type="file" name="sponsors[]" multiple accept="image/jpg,image/png,image/jpeg,image/gif" />
							</div>
						</li>

						<li>
							<label for="sponsors">Organisers</label>
							<div class="input">
								<input type="file" name="organisers[]" multiple accept="image/jpg,image/png,image/jpeg,image/gif" />
							</div>
						</li>

					</ul>
					</fieldset>
				
				</div>

				<!-- Location tab -->
				<div class="form_inputs" id="event-location">
					<fieldset>
						<ul>
							<li>
								<label for="event_venue"><?php echo lang('ctd_event_venue');?></label>
								<div class="input">
									<?php echo form_input('event_venue', @$article->event_venue, 'maxlength="250" class="event_venue"'); ?>
								</div>
							</li>
							
							<li>
								<label for="venue_address"><?php echo lang('ctd_event_venue_address');?></label>
								<div class="input">
									<?php echo form_input('venue_address', @$article->venue_address, 'maxlength="250" class="venue_address"'); ?>
								</div>
							</li>
						
							<li>
								<label for="location"><?php echo lang('ctd_location_label');?></label>
								<div class="input">
									<?php echo form_input('location', @$article->location, 'maxlength="250" class="location"'); ?>
								</div>
							</li>

							<li>
								<label for="country"><?php echo lang('ctd_country_label');?></label>
								<div class="input">
									<?php echo form_dropdown('country', $countries, @$article->country, 'class="country"'); ?>
								</div>
							</li>

							<li>
								<label for="geolocation"><?php echo lang('ctd_geolocation_label');?></label>
								<div class="input">
									<?php echo form_input('geolocation', @$article->geolocation, 'maxlength="128" class="geolocation"'); ?>
									<label class="inline"><?php echo form_checkbox('hide_map', 1, @$article->hide_map == 1); ?> <?php echo lang('ctd_do_not_display_map'); ?></label>
									<input type="hidden" value="<?php echo @$article->geocoordinates?>" name="geocoordinates" class="geocoordinates">
								</div>
							</li>
						
							<li>
								<label class="inline"><a id="geolink">[<?php echo lang('ctd_geolocation_label'); ?>]</a></label>
								<?php
									$displaymap = 'none';
								?>
								<div class="map_area" style="width:100%; margin-top:10px; display:<?php echo $displaymap ?>;">
									<p align="left"><a href="javascript:void(0);" class="close_map"><strong><?php echo lang('ctd_close_map_label'); ?></strong></a></p>
									<div style="margin-bottom:10px; ">
										<div style="width:300px; float:right; margin-right:20px; margin-top:10px">
											<input id="address" type="textbox" size="30" value=""> <input type="button" value="Search" onclick="mapByAddress();">
										</div>
										<div style="margin-top:10px;">
										<label class="inline"><?php echo lang('ctd_latitude_label'); ?></label> <span id="lati"></span><br />
										<label class="inline"><?php echo lang('ctd_longitude_label'); ?></label> <span id="longi"></span>
										</div>
										<div style="clear:both;"></div>
									</div>
									<div id="map_canvas" style="width:100%; height:550px; border: 1px solid #cdcdcd;"></div>
								</div>
							</li>
						

						</ul>
					</fieldset>
				</div>

				<!-- Metadata tab -->
				<div class="form_inputs" id="event-metadata">

					<fieldset>
					<ul>
						<li>
							<label for="meta_title"><?php echo lang('ctd_meta_title_label');?></label>
							<div class="input"><input type="text" id="meta_title" name="meta_title" maxlength="255" value="<?php echo $article->meta_title; ?>" /></div>
						</li>
						<li class="even">
							<label for="meta_description"><?php echo lang('ctd_meta_desc_label');?></label>
							<div class="input"><?php echo form_textarea(array('name' => 'meta_description', 'value' => $article->meta_description, 'rows' => 5)); ?></div>
						</li>
						<li>
							<label for="meta_keywords"><?php echo lang('ctd_meta_keywords_label');?></label>
							<div class="input"><input type="text" id="meta_keywords" name="meta_keywords" maxlength="255" value="<?php echo $article->meta_keywords; ?>" /></div>
						</li>
					</ul>
					</fieldset>
				</div>

			</div>	
		
<div class="buttons float-right padding-top">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
	<?php if (!empty($article->approve)):?>
		<input class="btn" type="submit" name="treasure" value="Approve" style="padding: 7px 15px; font-size: 15px;" disabled="">
	<?php else:?>
		<input class="btn" type="submit" name="treasure" value="Approve" style="padding: 7px 15px; font-size: 15px;">
	<?php endif;?>
</div>

		<?php echo form_close(); ?>

</div>
</section>

<div class="hidden">

	<div id="new-file-upload">
		<div style="float: left; padding: 15px; border: 1px solid #ccc; margin: 5px;">
			<label><?php echo lang('ctd_attachment_label'); ?> <small>[Max <?php echo ini_get('upload_max_filesize'); ?>]</small></label>
				<div class="input">
					<input type="file" name="userfiles[]">
				</div>
				<label><?php echo lang('ctd_attachment_name_label') ?></label>
				<div class="input">
					<input type="text" name="partner_name[]" />
				</div>
				<label><?php echo lang('ctd_attachment_desc_label') ?></label>
				<div class="input">
					<textarea class="skip" style="width: 220px; height: 75px;" name="partner_desc[]" rows="15" cols="3"></textarea>
				</div>
		</div>
	</div>

	<div id="files-uploader">

		<div class="files-uploader-browser">
			<?php echo form_open_multipart('admin/ctd/upload'); ?>
			<label for="userfile" class="upload"><?php echo lang('ctd_attachment_label'); ?></label>
				<?php echo form_upload('userfile', NULL, 'multiple="multiple"'); ?>
				<div style="visibility:hidden;">
				 <input type="text" name="filetitle">
				 <textarea rows="3" cols="30" name="filedescription" class="skip" style="width:220px;height:75px;"></textarea>
				</div>
				<?php if (!empty($article->id)): ?>
					<input type="hidden" id="ctd_id" name="ctd_id" value="<?php echo $article->id; ?>" />
				<?php endif; ?>
			<?php echo form_close(); ?>
			<ul id="files-uploader-queue" class="ui-corner-all"></ul>
		</div>
		
		<div class="buttons align-right padding-top">
			<a href="#" title="" class="button start-upload"><?php echo lang('buttons:upload'); ?></a>
			<a href="#" title="" class="button cancel-upload"><?php echo lang('buttons:cancel');?></a>
		</div>

   </div>
  </div>

<script type="text/javascript">
(function($){
	$("#add-file-upload").click(function(e){
		e.preventDefault();
		$("#file-upload .fupload").append($("#new-file-upload").html());
	});
	// Store data for filesUpload plugin
	$('#files-uploader form').data('fileUpload', {
		lang : {
			start : 'Start',
			cancel : '<?php echo lang('global:delete'); ?>'
		}
	});
})(jQuery);
</script>

<style type="text/css">
form.crud li.date-meta div.selector {
    float: left;
    width: 30px;
}
form.crud li.date-meta div input#datepicker { width: 8em; }
.form_inputs .input > input[type=text].width-5 { width: 20px !important; }
form.crud li.date-meta div input.width-5 { width:3em !important;}
form.crud li.date-meta div.selector { width: 5em; }
form.crud li.date-meta div.selector span { width: 1em; }
form.crud li.date-meta label.time-meta { min-width: 4em; width:4em; }
</style>