<div id="regform" style="background: #fff;">

<? if (@$msg): ?>
	<? echo "<br />" . $msg ?>
<? else: ?>

	<form action="<?= site_url('agenda/unregmail/'.$event->id) ?>" method="post" name="frmReg" id="frmReg" class="crud">
	
	<h1>Cancel Your Registration</h1>

	<p>
		"<? echo $event->title; ?>"<br />
		<p><?php echo lang('events_date_label')?>: <?php if (isset($event->date_from) && $event->date_from) echo (date('m/d/y H:i', $event->date_from)); else echo ' [no date from]'; ?> - <?php if (isset($event->date_to) && $event->date_to) echo (date('m/d/y H:i', $event->date_to)); else echo '[no date to]'; ?></p>
	</p>

	<? if (@$errmsg) echo '<p style="float:left;">' .$errmsg . "</p><br />\n"; ?>

		<ol>

			<li>
				<?php echo lang('events_cancel_ask_email_msg'); ?>
			</li>
			<li>
				<label><?php echo lang('events_reg_email_label'); ?></label>
				<input name="email" id="email" size="40" value="<?php echo (!empty($uemail) ? $uemail : '') ; ?>" />
			</li>
	
			<li><input id="btnSubmit" type="submit" value="<?php echo lang('events_next_label'); ?>"></li>
			
		</ol>
	
	</form>
<script>
	document.frmReg.email.focus();
</script>
<? endif; ?>
</div>
