<div class="row" style="margin-bottom: -32px;">
    <?php
        if($this->session->flashdata('success')){
            echo '<div class="alert alert-success message-wrapper" role="alert">'.$this->session->flashdata("success").'</div>';
        } 
        
         if(validation_errors()){
            echo '<div class="alert alert-danger message-wrapper" role="alert">'.validation_errors('<p class="form_error">','</p>').'</div>';
        }     
    ?>
    <div class="form-event-wrapper">
        <?php echo form_open_multipart($this->uri->uri_string(), 'class="crud row" id="frmEvent" style="margin: 0px;"'); ?>
            <label class="col-xs-12 title">SUBMIT AN EVENT</label>
			<p style="margin-bottom:31px; padding-left:15px; padding-right:15px;">Thank you for participating in the Coral Triangle Day. Please note that each event submission is subject to approval. All approved events will be displayed on the homepage. If you wish to modify your event after submitting it, please email us <a href="mailto:contest@coraltriangleday.org?Subject=Changes to event submission" class="s_link">here</a>. </p>
			<!-- HTML to write -->
			<div class="col-md-6 col-xs-12 form-group">
                <label for="name">Your Name*</label>
				<input name="submitted_by_name" value="<?php echo $event->submitted_by_name ?>" type="text" class="form-control" id="name">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        For event confirmation purpose only - will not appear on the website
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="email">Your Email Address*</label>
				<input name="submitted_by_email" value="<?php echo $event->submitted_by_email ?>" type="text" class="form-control" id="email">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        For event confirmation purpose only - will not appear on the website
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 form-group">
                <label for="eventname">Event Name* <span><span id="eventcharacter">100</span> characters left</span>
                </label>
				<input type="text" name="title" value="<?php echo $event->title ?>" class="form-control" id="eventname" maxlength="100">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        E.g. Underwater & beach clean-up 2015
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 form-group">
                <label for="eventdescription">Event Description* <span><span id="desccharacter">850</span> characters left</span>
                </label>
				<textarea class="form-control" name="intro" id="eventdescription" maxlength="850"><?php echo $event->intro ?></textarea>
                <ul class="dropdown-menu popovernote top textarea" role="menu">
                    <li role="presentation">
                        Describe the event's activities, and if open to public: what must they bring/do to participate. Word limit of 150 to 850 characters applies.
                    </li>
                </ul>
            </div>
			
			
			
            <div class="col-md-6 col-xs-12 form-group">
                <label for="eventdate">Event Date*</label>
				<input type="text" class="form-control" id="eventdate" placeholder="From" name="date_from_str" value="<?php if($event->date_from_str == '') echo '06/09/2016'; else echo $event->date_from_str?>">
                <ul class="dropdown-menu popovernote top" role="menu">
                    <li role="presentation">
                        Only fill this date field for a one-day event
                    </li>
                </ul>
            </div>
			
            <div class="col-md-6 col-xs-12 form-group">
                <div class="checkbox" style="margin-bottom:0px;">
                    <label>
                        <input type="checkbox" id="checkboxmultidayevent" style="margin-top: 2px;"> Check the box for multi-day event
                    </label>
                </div>	
				<input type="text" class="form-control" placeholder="To" id="multievent" name="date_to_str" value="<?php if($event->date_to_str == '') echo '06/09/2016'; else echo $event->date_to_str?>" disabled>
                <ul class="dropdown-menu popovernote top checkbox" role="menu">
                    <li role="presentation">
                        Fill this date field if the event is more than one day
                    </li>
                </ul>
            </div>
			
            <div class="col-md-6 col-xs-12 form-group">
                <label for="eventvenue">Event Venue*</label>
				<input type="text" class="form-control" id="eventvenue" name="event_venue" value="<?php echo $event->event_venue ?>">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        Venue is for the location of the vent, e.g. hotel, beach, university, building
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="venueaddress">Venue Address</label>
				<input type="text" class="form-control" id="venueaddress" name="venue_address" value="<?php echo $event->venue_address ?>">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
						The complete address of the venue
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="country">Country*</label>
                <?php echo form_dropdown('country', $countries, $event->country, 'id="country" class="form-control country"'); ?>
                <!-- <input type="text" class="form-control" id="country"> -->
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="location">City*</label>
                <input type="text" class="form-control" id="location" name="location" value="<?php echo $event->location ?>">
            </div>
            <div class="col-xs-12 form-group">
<!--
                <label for="googlemap">Geolocation*</label>
                <input name="geolocation" type="text" id="address" class="col-md-10 form-control geolocation" placeholder="Geolocation" /><br />
					<div class="col-md-2"
	    	            <input class="btn btn-default" type="button" value="Search" onclick="mapByAddress();">
					</div>
-->

                <!-- <a href="#" id="geolink">Show map</a> -->
                <!-- <input type="text" class="form-control" id="searchgooglemap" placeholder="Geolocation"> -->
                <!-- <div class="form-control map-event-wrapper" id="mapevent"></div> -->
				<div class="map_area col-sm-12" style="width:100%; margin-top:10px; padding:0; display:block;">

					<div class="row">

								<div class="col-sm-12">
									<label>Geolocation</label><br />
									<div class="row">
										<div class="col-sm-10">
											<input name="geolocation" id="address" type="text" class="form-control" value="<?php echo $event->geolocation ?>">
											<ul class="dropdown-menu popovernote" role="menu">
												<li role="presentation">
													You know your city best, help us and the public locate your event by adding a Google Maps pin on the venue. Type the event’s venue/address here, and click “Search”. Reposition the appearing red pin or refine search keywords if suggested location is incorrect.
												</li>
											</ul>
										</div>
										<div class="col-sm-2">
											<input class="btn btn-default" type="button" value="Search" onclick="mapByAddress();" style="padding:9px 25px;">
										</div>
										<input type="hidden" value="<?php echo $event->geocoordinates ?>" name="geocoordinates" class="geocoordinates">
									</div>

									<div class="clearfix"></div>

									<div>
										<div style="margin-top:10px;">
										<label style="display:inline;"><?php echo lang('ctd_latitude_label'); ?></label> <span id="lati"></span><br />
										<label style="display:inline;"><?php echo lang('ctd_longitude_label'); ?></label> <span id="longi"></span>
										</div>
										<div class="clearfix"></div>
									</div>
									<div id="map_canvas" style="width:100%; height:400px; border: 1px solid #cdcdcd;"></div>

								</div>

					</div>

				</div>


            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="website">Event URL</label>
				<input type="text" class="form-control" id="website" name="website" value="<?php echo $event->website ?>">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        Only include a website if it’s directly related to the event
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label for="contact_email">Contact Email</label>
				<input type="text" class="form-control" id="contact_email" name="contact_email" value="<?php echo $event->contact_email ?>">
                <ul class="dropdown-menu popovernote" role="menu">
                    <li role="presentation">
                        This email will be made visible to the public, so they can contact you for inquiries
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label >Organizer</label>
                <div class="clearfix"><br /></div>
                <ul class="list-form-upload" id="listimgorganizer">
                    <li>
                        <div class="input-group">
                            <input type="text" class="form-control" data-name="textimgorganizer" placeholder="Upload organizer logo" style="margin-bottom: 0px;" readonly>
                            <div class="input-group-btn">
                                <div class="fileUpload btn btn-default btn-browse">
                                    <span>Browse</span>
                                    <input type="file" class="upload" name="imgorganizer[]" />
                                </div>
                            </div>
                        </div>
                        <div class="input-group" style="width:100%">
                            <input class="form-control" placeholder="Add organizer website address" name="websiteorganizer[]" />
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
                <label><span class="more" onclick="addimgorganizer()">+ Add more organizers</span></label>
            </div>
            <div class="col-md-6 col-xs-12 form-group">
                <label>Sponsor</label>
                <div class="clearfix"><br /></div>
                <ul class="list-form-upload" id="listimgsponsor">
                    <li>
                        <div class="input-group">
                            <input type="text" class="form-control" data-name="textimgsponsor" placeholder="Upload sponsor logo" style="margin-bottom: 0px;" readonly>
                            <div class="input-group-btn">
                                <div class="fileUpload btn btn-default btn-browse">
                                    <span>Browse</span>
                                    <input type="file" class="upload" name="imgsponsor[]" />
                                </div>
                            </div>
                        </div>
                        <div class="input-group" style="width:100%">
                            <input class="form-control" placeholder="Add sponsor's website address" name="websitesponsor[]" />
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
                <label>
                	<span class="more" onclick="addimgsponsor()">+ Add more sponsors</span>
                </label>
            </div>
            <div class="col-xs-12">
                <button class="btn btn-primary pull-right">Submit Event</button>
                <!--<span class="pull-left" style="margin-top: 30px;">
                    NB: If you wish to modify a submitted event, please <a href="mailto:admin@catalyzecommunications.com">contact us</a>
                </span>-->
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script> -->
<script>
(function($) {
	$(function(){

		var latlong = $('.geocoordinates').val();
		var address =  latlong != '' ? latlong : $('.geolocation').val();
		$('.map_area').slideDown('500', function() {
			initialize(address);
			if (window.height < 480)
				$('#map_canvas').css('height', '300');
		});

		//Show and Initialize Google Maps
		$('#geolink').click(function (e) {
			e.preventDefault();
			var o = $('.map_area').css('display');
			if (o=='none')
			{
				$(this).html('Close map');
				var latlong = $('.geocoordinates').val();
				var address =  latlong != '' ? latlong : $('.geolocation').val();
				$('.map_area').slideDown('500', function() {
					initialize(address);
					if (window.height < 480)
						$('#map_canvas').css('height', '300');
				});
			}
			else
			{
				$('.map_area').slideUp('500', function(){
					$(this).html('Show map');
				});
			}
			$(this).blur();
		});
		$('.geolocation').keyup(function(){
			$('#address').val($(this).val());
		});
		$('#address').keyup(function(){
			$('.geolocation').val($(this).val());
		});

		//Hide Google Maps
		$('.close_map').click(function () {
			$('.map_area').slideUp('500');
		});

		// remove logo
		$('.remove_me').livequery('click', function(e){
			e.preventDefault();
			if (confirm('Are you sure you want to remove the logo?'))
			{
				$(this).parent().fadeOut('fast', function(){
					$(this).remove();
				});
			}
			$(this).blur();
			return false;
		})

	});
})(jQuery);

// Google Maps Handling Functions
var map;
var geocoder;
var marker;

function initialize(address) {
	geocoder = new google.maps.Geocoder();
	
	var myOptions = {
		zoom: 13,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

	marker = new google.maps.Marker({
		map:map,
		draggable:true,
		animation: google.maps.Animation.DROP
	});

	google.maps.event.addListener(marker, 'click', toggleBounce);
	google.maps.event.addListener(marker, 'dragend', updateDrag);

	if(address && address != '')
		mapByLatLong(address);
	else 
		mapGeolocation();
}

function mapGeolocation() {
	// Try HTML5 geolocation
	if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			handleMap(position.coords.latitude, position.coords.longitude);
		},
		function() {
			mapNoGeolocation(true);
		});
	} else {
		// Browser doesn't support Geolocation
		mapNoGeolocation(false);
	}
}

function mapNoGeolocation(errorFlag) {
	if (errorFlag)
		var content = 'Error: The Geolocation service failed. Please check your browser settings to enable Geolocation';
	else
		var content = 'Error: Your browser doesn\'t support Geolocation.';

	alert(content);

	handleMap(60, 105);
}

function handleMap(lati, longi) {
	var pos = new google.maps.LatLng(lati, longi);
	returnCoord(pos);
	map.setCenter(pos);
	marker.setPosition(pos);
	returnaddress(pos);
}

function updateDrag() {
	pos = marker.getPosition();
	returnCoord(pos);
	map.setCenter(pos);
	returnaddress(pos);
}

function mapByLatLong(address) {
	//addr = address ? address.split(',') : $('#address').val();
	addr = address ? address : $('#address').val();
	geocoder.geocode( { 'address': addr}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK)
			handleMap(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		else
			alert("Could not locate the address for the following reason: \n\n" + status);
	});
}

function mapByAddress(address) {
	addr = address ? address : $('#address').val();
	geocoder.geocode( { 'address': addr}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK)
			handleMap(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		else
			alert("Could not locate the address for the following reason: \n\n" + status);
	});
}

function returnCoord(pos) {
	document.getElementById("lati").innerHTML = pos.lat().toFixed(5);
	document.getElementById("longi").innerHTML = pos.lng().toFixed(5);
	$('.geocoordinates').val(pos.lat().toFixed(5) + ',' + pos.lng().toFixed(5));
}

function returnaddress(pos) {
	geocoder.geocode( { 'location': pos}, function(results, status) {
		document.getElementById("address").value = results[0].formatted_address;
		$('.geolocation').val(results[0].formatted_address);
	});
}

function toggleBounce() {
	if (marker.getAnimation() != null) {
		marker.setAnimation(null);
	} else {
		marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

</script>


<script>
    function addimgorganizer() {
        var html = '<li>' 
               
                + '	<div class="input-group">' 
                + '		<input type="text" class="form-control" data-name="textimgorganizer" placeholder="Upload organizer logo" style="margin-bottom: 0px;" readonly>' 
                + '		<div class="input-group-btn">' 
                + '		  <div class="fileUpload btn btn-default btn-browse">' 
                + '			    <span>Browse</span>' 
                + '			    <input type="file" class="upload" name="imgorganizer[]"/>' 
                + '			</div>' 
                + '		</div>' 
                + '	</div>'
                + '	<div class="input-group" style="width:100%">' 
                + '		<input class="form-control" placeholder="Add organizer website address" name="websiteorganizer[]"/>' 
                + '	</div>' 
                + '		<div class="clearfix"></div>'
                 + '		<a href="#" class="remove_me" title="Remove this logo">&times;</a>' 
                + '</li>';

        $("#listimgorganizer").append(html);

        $("input[name='imgorganizer[]']").change(function() {
            var index = $("input[name='imgorganizer[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgorganizer]:eq(" + index + ")").val(value);
        });
    }

    function addimgsponsor() {
        var html = '<li>'
                + '		<div class="input-group">' 
                + '			<input type="text" class="form-control" data-name="textimgsponsor" placeholder="Upload sponsor logo" style="margin-bottom: 0px;" readonly>' 
                + '			<div class="input-group-btn">' + '			  <div class="fileUpload btn btn-default btn-browse">' 
                + '				    <span>Browse</span>'
                 + '				    <input type="file" class="upload" name="imgsponsor[]"/>' 
                 + '				</div>' 
                 + '			</div>' 
                 + '		</div>' 
                 + '		<div class="input-group" style="width:100%">' 
                 + '			<input class="form-control" placeholder="Add sponsor\'s website address" name="websitesponsor[]"/>' 
                 + '		</div>'
                 + '		<div class="clearfix"></div>'
                  + '		<a href="#" class="remove_me" title="Remove this logo">&times;</a>' 
                 + '	</li>';

        $("#listimgsponsor").append(html);

        $("input[name='imgsponsor[]']").change(function() {
            var index = $("input[name='imgsponsor[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgsponsor]:eq(" + index + ")").val(value);
        });
    }

    $(function() {
        $("input[name='imgorganizer[]']").change(function() {
            var index = $("input[name='imgorganizer[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgorganizer]:eq(" + index + ")").val(value);
        });

        $("input[name='imgsponsor[]']").change(function() {
            var index = $("input[name='imgsponsor[]']").index(this);
            var value = $(this).val().split('\\').pop();
            $("input[data-name=textimgsponsor]:eq(" + index + ")").val(value);
        });

        $("#eventname").keydown(function() {
            var length = $(this).attr("maxlength") - $(this).val().length;
            $("#eventcharacter").text(length);
        });
        $("#eventdescription").keydown(function() {
            var length = $(this).attr("maxlength") - $(this).val().length;
            $("#desccharacter").text(length);
        });

        $("#checkboxmultidayevent").click(function() {
            if (this.checked) {
                $('#multievent').removeAttr("disabled");
            } else {
                $(this).val('');
                $('#multievent').attr("disabled", "disabled");
				$("#multievent").val("");
            }
        })
		
		$("#multievent").val("");
		
		$('#eventdate').datepicker();
		$('#multievent').datepicker();

        $('input[type=text]').focus(function() {
            $(this).next(".popovernote").toggle();
        });
        $('textarea').focus(function() {
            $(this).next(".popovernote").toggle();
        });
        $('input[type=text]').blur(function() {
            $(this).next(".popovernote").toggle();
        })
        $('textarea').blur(function() {
            $(this).next(".popovernote").toggle();
        })
        
        $('input,select').keypress(function(event) { return event.keyCode != 13; });
    });
</script>
<script>
				/* validation date */
				//var nowTemp = new Date();
				var nowTemp = new Date(document.getElementById("eventdate").value);
				var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
						 
				var checkin = $('#eventdate').datepicker({
					onRender: function(date) {
					return date.valueOf() < now.valueOf() ? '' : '';
					}
				}).on('changeDate', function(ev) {
					if (ev.date.valueOf() > checkout.date.valueOf()) {
						var newDate = new Date(ev.date)
						newDate.setDate(newDate.getDate() + 0);
						checkout.setValue(newDate);
					}
					else if(ev.date.valueOf() < checkout.date.valueOf()) {
						var newDate = new Date(ev.date)
						newDate.setDate(newDate.getDate() + 0);
						checkout.setValue(newDate);
					}
					checkin.hide();
					$('#multievent')[0].focus();
				}).data('datepicker');
					
				var checkout = $('#multievent').datepicker({
				onRender: function(date) {
					return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
				}
				}).on('changeDate', function(ev) {
					checkout.hide();
				}).data('datepicker');
				/* end validation date */
			</script>