<div id="regform" style="background: #fff;">

	<h1><?php if (!empty($ptitle)) echo $ptitle; ?></h1>

	<?php if (!empty($errmsg)) echo '<p>'.$errmsg . "</p>\n"; ?>

	<p>
		"<? echo $event->title; ?>"<br />
		<p><?php echo lang('events_date_label')?>: <?php if (isset($event->date_from) && $event->date_from) echo (date('M d, Y, H:i', $event->date_from)); else echo ' [no date from]'; ?> - <?php if (isset($event->date_to) && $event->date_to) echo (date('M d, Y, H:i', $event->date_to)); else echo '[no date to]'; ?></p>
	</p>

<?php if (@$msg): ?>
	<? echo "<br />" . $msg ?>
<?php endif; ?>

</div>
