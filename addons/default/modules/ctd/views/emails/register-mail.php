<html>
<head>
<style>
* {font-family: Arial, sans-serif; font-size: 12px;}
table {width: 500px;}
td {border-bottom: 1px solid #ccc; padding: 7px 0 5px 0;}
</style>
</head>
<body>

 <? echo sprintf( lang('events_reg_email_message'), $event->title )?>
<br />
<table border="0" cellspacing="0">

			<tr><td width="150">
				<?php echo lang('events_reg_name_label');?></td>
				<td><?php echo @$fvalue['name']; ?>
			</td></tr>
			
			<tr><td width="150">
				<?php echo lang('events_reg_organisation_label');?></td>
				<td><?php echo ( isset($fvalue['organisation']) && $fvalue['organisation']) ? $fvalue['organisation'] : "&nbsp;"; ?>
			</td></tr>
	
			<tr><td width="150">
				<?php echo lang('events_reg_email_label');?></td>
				<td><?php echo ( isset($fvalue['email']) && $fvalue['email']) ? $fvalue['email'] : "&nbsp;"; ?>
			</td></tr>
			
			<tr><td width="150">
				<?php echo lang('events_reg_phone_label');?></td>
				<td><?php echo ( isset($fvalue['phone']) && $fvalue['phone']) ? $fvalue['phone'] : "&nbsp;"; ?>
			</td></tr>
	
			<tr><td width="150">
				<?php echo lang('events_reg_cellphone_label');?></td>
				<td><?php echo ( isset($fvalue['cellphone']) && $fvalue['cellphone']) ? $fvalue['cellphone'] : "&nbsp;"; ?>
			</td></tr>
	
			<tr><td width="150" valign="top">
				<?php echo lang('events_reg_address_label');?></td>
				<td><?php
					if (isset($fvalue['address']) && $fvalue['address']) {
						echo stripslashes(nl2br($fvalue['address']));
					} else {
						echo "&nbsp;";
					}
					?>
			</td></tr>

</table>

<small><?php echo site_url(); ?></small>

</body>

</html>