<html>
<head>
<style>
* {font-family: Arial, sans-serif;}
table {width: 500px;}
td {border-bottom: 1px solid #ccc; padding: 7px 0 5px 0;}
</style>
</head>
<body>

Dear <?php echo ($reg->name?$reg->name:$reg->email);?>,<br />

<p>You requested to cancel your attendance to the event "<?php echo $event->title; ?>" on <?php date('d M Y, H:i'); ?></p>

<p>To make sure it is you who made the request, please follow the instructions below:</p>

<ul>
	<li>Go to <?php echo site_url('agenda/verifycancel/'.$event->id.'/'.$reg->id.'/'.$vkey); ?></li>
	<li>If you are prompted to enter the key, enter '<?php echo $vkey; ?>' (without quotes).</li>
	<li>Click submit.</li>
</ul>

<p>If you did not make this request, please ignore this and report it back to us.</p>

<p>Thank you and we are sorry not to be able to meet you at the event.</p>

Regards,<br />
<br />
Event Management.<br />
<br />
<?php echo site_url(); ?><br />

</body>

</html>