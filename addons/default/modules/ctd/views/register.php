<div id="regform" style="background: #fff;">

<!-- <pre><? print_r($this->user); ?></pre> -->

<? if (@$msg): ?>
	<? echo "<br />" . $msg ?>
<? else: ?>

	<form action="<?= site_url('agenda/register/'.$eid) ?>" method="post" name="frmReg" id="frmReg" class="crud">
	
	<h2>Event Registration</h2>

	<? if (@$errmsg) echo $errmsg . "<br />\n"; ?>

	<p>
		<? echo lang('events_you_are_registering_to'); ?> "<? echo $event->title; ?>"<br />
		<p><?php echo lang('events_date_label')?>: <?php if (isset($event->date_from) && $event->date_from) echo (date('m/d/y H:i', $event->date_from)); else echo ' [no date from]'; ?> - <?php if (isset($event->date_to) && $event->date_to) echo (date('m/d/y H:i', $event->date_to)); else echo '[no date to]'; ?></p>
	</p>

		<ol>
	
			<li>
				<label for="name"><?php echo lang('events_reg_name_label');?> *</label>
				<?php echo form_input('name', @$fvalue->name, 'maxlength="100"'); ?>
				<span class="required-icon tooltip"><?php echo lang('required_label');?></span>
				<?php echo form_error('name'); ?>
			</li>
			
			<li>
				<label for="organisation"><?php echo lang('events_reg_organisation_label');?></label>
				<?php echo form_input('organisation', @$fvalue->organisation, 'maxlength="100"'); ?>
				<?php echo form_error('organisation'); ?>
			</li>
	
			<li>
				<label for="email"><?php echo lang('events_reg_email_label');?> *</label>
				<?php echo form_input('email', @$fvalue->email, 'maxlength="100"'); ?>
				<span class="required-icon tooltip"><?php echo lang('required_label');?></span>
				<?php echo form_error('email'); ?>
			</li>
			
			<li>
				<label for="phone"><?php echo lang('events_reg_phone_label');?> *</label>
				<?php echo form_input('phone', @$fvalue->phone, 'maxlength="100"'); ?>
				<span class="required-icon tooltip"><?php echo lang('required_label');?></span>
				<?php echo form_error('phone'); ?>
			</li>
	
			<li>
				<label for="cellphone"><?php echo lang('events_reg_cellphone_label');?></label>
				<?php echo form_input('cellphone', @$fvalue->cellphone, 'maxlength="100"'); ?>
				<span class="required-icon tooltip"><?php echo lang('required_label');?></span>
				<?php echo form_error('cellphone'); ?>
			</li>
	
			<li>
				<label for="address"><?php echo lang('events_reg_address_label');?></label>
				<?php echo form_textarea(array('id'=>'address', 'name'=>'address', 'value' =>  stripslashes(@$fvalue->address), 'rows' => 5, 'cols' => '50')); ?>
				<?php echo form_error('address'); ?>
			</li>
	
			<li><input id="btnSubmit" type="submit" value="<?php echo lang('events_button_register'); ?>"> <input type="reset" value="Reset"></li>
			
		</ol>
	
	</form>
<script>
	document.frmReg.name.focus();
</script>
<? endif; ?>
</div>
