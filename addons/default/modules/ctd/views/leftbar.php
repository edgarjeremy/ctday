<!-- left column starts -->
<aside class="publishbar left pad-rt">

                <section class="TCategories">
                    <h3><?php echo lang('cat_categories_label'); ?></h3>
                    <ul>
                    <?php if (!empty($categories)):
                    		foreach($categories as $cat):
                    ?>
                    	<li><a href="<?php echo site_url('agenda/category/'.$cat->slug); ?>"><?php echo $cat->title; ?></a></li>
                    <?php 	endforeach; ?>
                    <?php else: ?>
	                    <li><?php echo lang('cat_no_categories'); ?></li>
					<?php endif; ?>
	                </ul>
                </section>
                <!--Top Categories Ends-->
                <section class="Tags">
                    <h3><?php echo lang('events_tags_label'); ?></h3>
                    <ul>
                    	<?php if (!empty($toptags)):
                    			foreach($toptags as $ttags):
                    	?>
                        	<li><a href="<?php echo site_url('agenda/tags/'.(trim($ttags->tag))); ?>"><span><?php echo deaccented(trim($ttags->tag)); ?></span></a></li>
                        <?php 	endforeach; ?>
                        <?php else: ?>
	                        <li><?php echo lang('events_no_tags_msg'); ?></li>
						<?php endif; ?>
                    </ul>
                    <div class="clr"></div>
                </section><!--Categories Ends-->
                <section class="smCategories">
                    <h3>Op&ccedil;&otilde;es</h3>
                    <ul>
                    <li><a href="<?php echo site_url(); ?>agenda/rss/all.rss">RSS</a></li>
                    <li><a href="<?php echo site_url(); ?>agenda/ical/all.ics">iCal</a></li>
                </ul>
                </section><!--Categories Ends-->
</aside>
<!-- left column ends -->