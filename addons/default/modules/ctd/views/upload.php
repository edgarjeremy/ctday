<link rel="stylesheet" href="{{url:site}}{{ theme:path }}/js/jqueryupload/css/style.css">
<link rel="stylesheet" href="{{url:site}}{{ theme:path }}/js/jqueryupload/css/jquery.fileupload.css">
<link rel="stylesheet" href="{{url:site}}{{ theme:path }}/js/jqueryupload/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{url:site}}{{ theme:path }}/js/jqueryupload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="{{url:site}}{{ theme:path }}/js/jqueryupload/css/jquery.fileupload-ui-noscript.css"></noscript>
<style>
	#mCSB_1_container {
		min-height: 50px!important;
	}
	.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
		vertical-align: middle;
	}
	.mCustomScrollBox {
		position: relative;
		overflow: hidden;
		height: 100%;
		max-width: 100%;
		outline: medium none;
		direction: ltr;
		border-right: 1px solid #DDD;
		border-left: 1px solid #DDD;
		border-bottom: 1px solid #DDD;
	}
</style>
<script>
$(document).ready(function(){
	$("#loading").fadeOut(0);
	document.getElementById("imgfile1").setAttribute("disabled", "disabled");
	document.getElementById("upload_photos_button").setAttribute("disabled", "disabled");
})
</script>
<div id="form-container" class="row">
<div class="form-event-wrapper" style="margin-top:0px;">

	<div class="clearfix"></div>
	{{ session:messages success="alert alert-success" notice="alert alert-info" error="alert alert-error" }}
	<div id="message"></div>
	<div class="clearfix"></div>

	<?php
       /*  if($this->session->flashdata('success')){
            echo '<div class="alert alert-success message-wrapper" role="alert">'.$this->session->flashdata("success").'</div>';
        } 
        
         if(validation_errors()){
            echo '<div class="alert alert-danger message-wrapper" role="alert">'.validation_errors('<p class="form_error">','</p>').'</div>';
        }   */   
    ?>
	
	<?php echo form_open_multipart('', array('class' => 'crud', 'id' => 'fileupload')); ?>
	<label class="col-xs-12 title">UPLOAD EVENT PHOTOS</label>
	<p style="margin-bottom:31px;">In this page you can upload the photos from your Coral Triangle Day event. Please note that photos are subject to review and approval, and selected photos may be displayed on the coraltriangleday.org website, our Flickr account, and our social media channels.</p>
	<div class="clearfix"></div>
	<?php if (!empty($err_msg)): ?>
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p><?php echo $err_msg; ?></p>
		</div>
		<div class="clearfix"></div>
	<?php endif; ?>

	<div class="row">
	    <div class="col-md-6 col-xs-12 form-group">
	        <label for="name">Your Name and/or Organization*</label>
	        <input name="name" value="<?php echo $file->name ?>" type="text" class="form-control" id="name" onchange="beforeuploadimage(this);">
			<ul class="dropdown-menu popovernote" role="menu">
				<li role="presentation">
					Will be used for the photo credits
				</li>
			</ul>
	    </div>
		
	    <div class="col-md-6 col-xs-12 form-group">
	        <label for="email">Your Email Address*</label>
	        <input name="email" value="<?php echo $file->email ?>" type="text" class="form-control" id="email" onchange="beforeuploadimage(this);">
			<ul class="dropdown-menu popovernote" role="menu">
				<li role="presentation">
					For clarification purpose only - will not appear on the website
				</li>
			</ul>
	    </div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group">
		<div class="row">
			<div class="col-sm-6">
				<label>Country*</label>
				
				<?php print_r($file->country);?>
				
                <div class="form-group">
					<?php echo form_dropdown('country', $countries, $file->country, 'class="form-control" onchange="getEvents(this.value); beforeuploadimage(this);" id="country"'); ?>
					
					<script>
						$("#country, dd_event").hover(function () {
							var fullname_country_input = document.getElementById('country').value;
								
							if(fullname_country_input == 'ID'){
								var full_contry = 'Indonesia';
							}
							else if(fullname_country_input == 'FJ'){
								var full_contry = 'Fiji';
							}
							else if(fullname_country_input == 'MY'){
								var full_contry = 'Malaysia';
							}
							else if(fullname_country_input == 'PG'){
								var full_contry = 'Papua New Guinea';
							}
							else if(fullname_country_input == 'PH'){
								var full_contry = 'Philippines';
							}
							else if(fullname_country_input == 'SB'){
								var full_contry = 'Solomon Islands';
							}
							else if(fullname_country_input == 'TL'){
								var full_contry = 'Timor Leste';
							}
							
							document.getElementById("f_name_country").value = full_contry;
						});
					</script>
					
					<input type="text" name="full_name_country" id="f_name_country" value="" style="display:none;"/>
                </div>
			</div>

			<div class="col-sm-6">
				<label>Event Name*</label>
				<div id="loading" style="position: absolute; width:92%;height:40px;background:#fff url({{site:url}}/img/loading.gif) no-repeat center center; background-size: 40px 40px;"></div>
                <div class="form-group" id="event_name_group">
                	<?php 
                		if ($file->event)
							echo form_dropdown('event', $events, $file->event, 'id="dd_event" class="form-control" id="event"');
						else
							echo form_dropdown('event', array(''=>'Select'), $file->event, 'id="dd_event" class="form-control" onchange="beforeuploadimage(this);"');
					?>
					<ul class="dropdown-menu popovernote top" role="menu">
						<li role="presentation">
							Please choose a country first if you haven&rsquo;t
						</li>
					</ul>
                </div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="row">

			<!--<div class="col-sm-6">
				<label>Image*</label>
		        <div class="input-group">
		            <input type="text" id="fname" class="form-control" data-name="texteventphoto" placeholder="Upload event photo" style="margin-bottom: 0px;" readonly onclick="$('#imgfile').trigger('click');">
		            <div class="input-group-btn">
		                <div class="fileUpload btn btn-default btn-browse">
		                    <span>Browse</span>
		                    <input id="imgfile" type="file" class="upload" name="imgfile" />
		                </div>
		            </div>
		            <input type="hidden" name="filecheck">
		        </div>
			</div>-->
			<div class="fileupload-buttonbar">
				<div class="col-xs-12 col-md-8">
					<span id="imgfile1" class="btn btn-default fileinput-button">
						<i class="glyphicon glyphicon-plus"></i>
						<span>Choose photos...</span>
						<input type="file" name="imgfile[]" id="imgfile" accept="image/jpg,image/png,image/jpeg,image/gif" multiple>
					</span>
					<ul class="dropdown-menu popovernote" role="menu" id="hoversectionafterclick">
						<li role="presentation">
							You can choose multiple photos by holding shift while clicking on the image files you wish to share. Only JPEG, BMP, and PNG are accepted.
						</li>
					</ul>
				<!--
					<button type="reset" class="btn btn-default cancel">
						<i class="glyphicon glyphicon-ban-circle"></i>
						<span>Cancel upload</span>
					</button>
					<button type="button" class="btn btn-default delete">
						<i class="glyphicon glyphicon-trash"></i>
						<span>Delete</span>
					</button>
					<input type="checkbox" class="toggle">
					<span class="fileupload-process"></span>
				-->
				</div>
				<!--
					<div class="col-xs-12 col-md-4 fileupload-progress fade">
						<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
							<div class="progress-bar progress-bar-success" style="width:0%;"></div>
						</div>
						<div class="progress-extended">&nbsp;</div>
					</div>
				-->
			</div>
        
			<div class="content-out-upload-images" style="display: none;">
			<div class="content-upload-images mCustomScrollbar">
				<table id="flist" role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
			</div>
			</div>
		
		</div>
	</div>
	<div class="form-group fileupload-buttonbar">
		<button type="submit" class="btn btn-primary pull-right start" id="upload_photos_button">UPLOAD PHOTOS</button>
	</div>

<!--	<div class="form-group">
		<button class="btn btn-primary pull-right">Upload</button>-->

<!-- 		<input type="submit" name="btnSubmit" value="Upload" /> -->
<!--	</div>-->

	<div class="clearfix"></div>

	<div class="form-group" style="margin-top:17px;">
		<ul style="padding-left: 13px;">
			<li>By uploading this image you confirm that you are the rightful owner of the image or that you have obtained the necessary permission to share it.</li>
			<li>WWF, contest organizers and the sponsor reserve the right to showcase the submitted contest photos in perpetuity on the <a href="http://www.coraltriangleday.org/">www.coraltriangleday.org</a>, <a href="https://www.flickr.com/photos/97485135@N06/collections" target="_blank">Flickr</a>, and on several social media platforms affiliated to the contest's organizers.</li>
		</ul>
	</div>

<?php echo form_close() ?>
</div>
</div>


<script src="{{url:site}}{{ theme:path }}/js/jqueryupload/js/vendor/jquery.ui.widget.js"></script>
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="{{url:site}}{{ theme:path }}/js/jqueryupload/js/jquery.iframe-transport.js"></script>
<script src="{{url:site}}{{ theme:path }}/js/jqueryupload/js/jquery.fileupload.js"></script>
<script src="{{url:site}}{{ theme:path }}/js/jqueryupload/js/jquery.fileupload-process.js"></script>
<script src="{{url:site}}{{ theme:path }}/js/jqueryupload/js/jquery.fileupload-image.js"></script>
<script src="{{url:site}}{{ theme:path }}/js/jqueryupload/js/jquery.fileupload-validate.js"></script>
<script src="{{url:site}}{{ theme:path }}/js/jqueryupload/js/jquery.fileupload-ui.js"></script>


<script>
function getEvents(v)
{
	if (v)
	{
		$.ajax({
			url: '<?php echo site_url('ctd/getevents') ?>/'+v,
			beforeSend: function() {
				$("#event_name_group").fadeOut();
				$("#loading").fadeIn(0);
			}
		})
		.done(function( data ) {
			if (data)
			{
				$('#dd_event').html(data);
				$("#loading").fadeOut(0);
				$("#event_name_group").fadeIn();
			}
		});
	}
}

var alerted = false;
$(document).ready(function(){
		$("#loading").fadeOut(0);
	
		/*
			$("#imgfile").change(function() {
				var value = $(this).val().split('\\').pop();
				$("#fname").val(value);
			});
		*/

		$('#fileupload').fileupload({
			url: '<?php echo site_url('ctd/uploadimage') ?>',
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			submit: function(e, data){
			    var nm = $('#name').val();
			    var em = $('#email').val();
			    var co = $('#country').val();
			    var ev = $('#dd_event').val();
			    if (!nm || !em || !co || !ev)
			    {
			    	if (!alerted)
			    	{
			    		alerted=true;
				     	alert("Please fill in all of the required fields");
			    	}
			        data.context.find('button').prop('disabled', false);
	     			return false;
			    }
			   },
		    filesContainer: $('tbody.files'),
		    uploadTemplateId: null,
		    downloadTemplateId: null,
			success: function(e, file){
				succesupload = true;
			},
			fail: function(e, file){
				succesupload = false;
			},
			stop: function (e, data) {
				if(succesupload){
					$("#fileupload")[0].reset();
					var text = '<div class="alert alert-success alert-dismissible" role="alert">'
								+'  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
								+'  <strong>Thank you for uploading! We will review and make it online as soon as possible. Feel free to upload more photos!</strong> '
								+'</div>';
					$("#message").html(text);
				}
				else{
					var text = '<div class="alert alert-danger alert-dismissible" role="alert">'
								+'  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
								+'  <strong>Upload Failed Please Try Again</strong> '
								+'</div>';
					$("#message").html(text);
					$("tr.template-upload.fade").remove();
				}
				$(".col-xs-12.col-md-4.fileupload-progress.fade").removeClass('in');
			},
		    uploadTemplate: function (o) {
		        var rows = $();
		        $.each(o.files, function (index, file) {
					if($.inArray(file.type, ['image/gif','image/png','image/jpg','image/jpeg']) == -1) {
					    alert('File type not allowed!');
					}
					else
					{
						$(".content-out-upload-images").addClass("urgentshow");

			            var row = $('<tr class="template-upload fade">' +
			                '<td><span class="preview"></span></td>' +
			                '<td><p class="name"></p>' +
//			                '<div class="error"></div>' +
			                '</td>' +
			               '<td>' +
					        '    <p class="size">Processing...</p>' +
					        '   <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>' +
					       ' </td>' +
			                '<td>' +
			                (!index && !o.options.autoUpload ?
			                    '<button class="start" disabled style="display:none;">Start</button>' : '') +
			                (!index ? '<button class="btn btn-warning cancel" onClick="removeFile($(this));">Cancel</button>' : '') +
			                '</td>' +
			                '</tr>');
			            row.find('.name').text(file.name);
			            row.find('.size').text(o.formatFileSize(file.size));
			            if (file.error) {
			                row.find('.error').text(file.error);
			            }
			            rows = rows.add(row);
					}
		        });
		        return rows;
				
		    },
		    downloadTemplate: function (o) {
		        var rows = $();
		        $.each(o.files, function (index, file) {
		            var row = $('<tr class="template-download fade">' +
		                '<td><span class="preview"></span></td>' +
		                '<td><p class="name"></p>' +
		                (file.error ? '<div class="error"></div>' : '') +
		                '</td>' +
		                '<td><span class="size"></span></td>' +
		                '<td><button class="delete">Delete</button></td>' +
		                '</tr>');
		            row.find('.size').text(o.formatFileSize(file.size));
		            if (file.error) {
		                row.find('.name').text(file.name);
		                row.find('.error').text(file.error);
		            } else {
		                row.find('.name').append($('<a></a>').text(file.name));
		                if (file.thumbnailUrl) {
		                    row.find('.preview').append(
		                        $('<a></a>').append(
		                            $('<img>').prop('src', file.thumbnailUrl)
		                        )
		                    );
		                }
		                row.find('a')
		                    .attr('data-gallery', '')
		                    .prop('href', file.url);
		                row.find('button.delete')
		                    .attr('data-type', file.delete_type)
		                    .attr('data-url', file.delete_url);
		            }
		            rows = rows.add(row);
		        });
		        return rows;
		    },

/*
add: function(e, data) {
console.log('data:');
console.log(data);
        var uploadErrors = [];
        var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
        if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
            uploadErrors.push('Please upload only GIF, JPG or PNG format images.');
        }
        if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
            uploadErrors.push('Filesize is too big');
        }
        if(uploadErrors.length > 0) {
            alert(uploadErrors.join("\n"));
        }
        else
        {
        	continue;
        }
},
*/			
			done: function (e, data) {
				if (data.textStatus=='success')
				{
					$('#flist tr').fadeOut(function(){
						$(this).remove();
						$(".content-out-upload-images").removeClass("urgentshow");
					});
					
					$.ajax({
						url: '<?php echo site_url('ctd/email_success_photo') ?>',
						method: "POST",
						data: { name: $('#name').val(), email: $('#email').val() }
					})
					
					$('form#fileupload')[0].reset();
						
					var text = '<div class="alert alert-success alert-dismissible" role="alert">'
								+'  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
								+'  <strong>Thank you for uploading! We will review and make it online as soon as possible. Feel free to upload more photos!</strong> '
								+'</div>';
					$("#message").html(text);
					
					$('html, body').animate({scrollTop:0}, 'slow');
				}
				$(".col-xs-12.col-md-4.fileupload-progress.fade").removeClass('in');
				//var obj = JSON.parse(data);
	        }

			/* done: function (e, data) {
				if (data.textStatus=='success')
				{
					$('#flist .progress-striped, #flist .cancel').fadeOut(function(){
						$(this).remove();
					});
					$(".content-out-upload-images").addClass("urgentshow");
					$('form#fileupload')[0].reset();
					
					var text = '<div class="alert alert-success alert-dismissible" role="alert">'
								+'  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
								+'  <strong>Thank you for uploading! We will review and make it online as soon as possible. Feel free to upload more photos!</strong> '
								+'</div>';
					$("#message").html(text);
				}
				$(".col-xs-12.col-md-4.fileupload-progress.fade").removeClass('in');
				//var obj = JSON.parse(data);
	        } */
		});

});

function beforeuploadimage() { 
    var name_fill = document.getElementById('name').value; 
    var email_fill = document.getElementById('email').value; 
    var dd_event = document.getElementById('dd_event').value; 
    var country = document.getElementById('country').value; 
    if(!name_fill.match(/\S/)) {
        document.getElementById("imgfile1").setAttribute("disabled", "disabled");
        document.getElementById("upload_photos_button").setAttribute("disabled", "disabled");
        return false;
    }
	else if(!email_fill.match(/\S/)) {
        document.getElementById("imgfile1").setAttribute("disabled", "disabled");
        document.getElementById("upload_photos_button").setAttribute("disabled", "disabled");
        return false;
    } 
	else if(!dd_event.match(/\S/)) {
        document.getElementById("imgfile1").setAttribute("disabled", "disabled");
        document.getElementById("upload_photos_button").setAttribute("disabled", "disabled");
        return false;
    } 
	else if(!country.match(/\S/)){
		document.getElementById("imgfile1").setAttribute("disabled", "disabled");
		document.getElementById("upload_photos_button").setAttribute("disabled", "disabled");
		return false;
	}
	else {
        document.getElementById("imgfile1").removeAttribute("disabled", "disabled");
        document.getElementById("upload_photos_button").removeAttribute("disabled", "disabled");
        return true;
    }
}

$(function() {
		$( "#imgfile1" )
		.mouseenter(function(){
			$(this).next(".popovernote").fadeIn('fast');
			
			if($("#hoversectionafterclick" ).hasClass( "displaynone" )){
				$("#hoversectionafterclick").removeClass("displaynone");
			}
		})
		.mouseleave(function(){
			$(this).next(".popovernote").fadeOut('fast');
		})
		
		$( "#imgfile1" ).click(function() {
			$("#hoversectionafterclick").addClass("displaynone");
		});
		
        $('input[type=text]').focus(function() {
            $(this).next(".popovernote").toggle();
        });
        $('textarea').focus(function() {
            $(this).next(".popovernote").toggle();
        });
		$('select').focus(function() {
            $(this).next(".popovernote").toggle();
        });
		/* $('#imgfile1').blur(function() {
            $(this).next(".popovernote").toggle();
        }); */
        $('input[type=text]').blur(function() {
            $(this).next(".popovernote").toggle();
        })
        $('select').blur(function() {
            $(this).next(".popovernote").toggle();
        })
        $('textarea').blur(function() {
            $(this).next(".popovernote").toggle();
        })
    });


function removeFile(t)
{
	t.parent().parent().fadeOut('fast', function(){
		$(this).remove();
	})
}
</script>