
<div class="e-container">

	<div class="event-partners">
		<div class="partner-image-container">
			<a href="#" class="pimg-prev"></a>
			<a href="#" class="pimg-next"></a>
			<div class="partner-images">
				<?php foreach($article->partners as $p): ?>
					<img src="<?php echo site_url(UPLOAD_PATH.'ctd/' . thumbnail($p->filename)); ?>" />
				<?php endforeach; ?>
			</div>
		</div>
		<div class="shareit">
			<p>Share this event..</p>
			<a class="fb socmed-share" href="http://www.facebook.com/share.php?s=100&p[url]=<?php echo site_url('ctd/'.$article->slug)?>&p[title]=<?php echo $article->title ?> | {{settings:site_name}}&p[images][0]=http://www.thecoraltriangle.com/day/addons/default/themes/ctd/img/ctd_logo.gif&p[summary]=<?php echo stripslashes(strip_tags($article->intro)); ?>"></a>
			<a class="twitter socmed-share" href="https://twitter.com/share?url=<?php echo site_url('ctd/'.$article->slug)?>"></a>
			<a class="gplus socmed-share" href="https://plus.google.com/share?url=<?php echo site_url('ctd/'.$article->slug)?>"></a>
		</div>
	</div>

	<div class="event-details">

		<h2><?php echo $article->title; ?></h2>


		<div class="dateloc">
			<?php 
				$day = date('d', $article->date_from);
				$month = date('F', $article->date_from);
				$year = date('Y', $article->date_from);
				$time = date('H:i', $article->date_from);
	
				$ttime = date('H:i', $article->date_to);
	
				$day_format = "$day $month, $year";
	
				if ($time <> $ttime && $time <> '00:00' && $ttime <> '00:00')
				{
					$time_format = "$time - $ttime";
				}
				elseif ($time == $ttime && $time <> '00:00' && $ttime <> '00:00')
				{
					$time_format = "$time";
				}
				else
				{
					$time_format = $time;
				}
	
			?>
			<div class="dateloc-label">Location:</div><?php echo $article->location ?><br />
			<div class="dateloc-label">Date:</div><?php echo $day_format . ', ' . $time_format ?>
		</div>

		<?php if ($article->category_slug): ?>
		<p><?php echo lang('ctd_category_label'); ?>: <?php echo anchor('ctd/category/'.$article->category_slug, $article->category_title) ?></p>
		<?php endif; ?>

		<div class="intro">
			<b>Description:</b><br />
			<?php echo stripslashes($article->intro); ?>
		</div>

		<div class="clear"><br /></div>
		<div id="form-container">
		<form id="frmAttend" action="<?php site_url('ctd/register/'.$article->id) ?>" method="post">
			<p>
			<label><input class="ckbox" type="checkbox" name="remind" value="1"/> Remind me of this event by email</label>
			<label><input class="ckbox" type="checkbox" name="inform" value="1"/> Inform me by email if there are changes to this page</label>
			</p>
			<p id="emaildiv">Your email: <input data-validation-engine="validate[required,custom[email]]" type="text" name="email" value="" /></p>
			<?php if ($article->plan_to_come): ?>
			<p>
				<span><?php echo $article->plan_to_come . ($article->plan_to_come > 1 ? ' people</span> plan' : ' person</span> plans') ?></span> to attend this event
			</p>
			<?php else: ?>
				<p><span>No one</span> plans to attend this event</p>
			<?php endif; ?>
			<input class="attend" type="submit" value="I will attend this event" name="submit">
		</form>
		</div>
		<div id="msgdiv"></div>

<script>
$(document).ready(function(){

	$(".ckbox").livequery('click', function(){
		if ( !$("input[name='remind']").is(':checked') && !$("input[name='inform']").is(':checked') )
		{
			$("#emaildiv").slideUp('500', function(){
				$.colorbox.resize();
			})
		}
		else if ( $("input[name='remind']").is(':checked') || $("input[name='inform']").is(':checked') )
		{
			$("#emaildiv").slideDown('500', function(){
				$.colorbox.resize();
			})
		}
	});

	// partner slider
	$(".partner-images").carouFredSel({
		circular: false,
		infinite: false,
		auto 	: false,
		direction: "up",
		width	: 125,
		height	: 90,
		align	: "top",
		onCreate: function(){
			$(".partner-images").children(':first').css("top", "0");
		},
		items	: {
			visible	: 1
		},
		prev	: {	
			button	: ".pimg-prev"
		},
		next	: { 
			button	: ".pimg-next"
		}
	});

	$('form#frmAttend').removeAttr('action');
	$('form#frmAttend').livequery('submit', function(e) {

		e.preventDefault();

		var processing = $('#loading');
		var form_data = $(this).serialize();
		$.ajax({
			url: SITE_URL + 'ctd/register/' + '<?php echo $article->slug ?>',
			type: "POST",
		    data: form_data,
		    beforeSend: function(){
		    	$("#msgdiv").fadeOut();
		    	processing.show();
		    },
			success: function(data) {
				processing.hide();
				var obj = $.parseJSON(data);
				if(obj.status == 'success') {
					$("#msgdiv").html(obj.message);
					$("#form-container").slideUp('fast', function(){
						$("#msgdiv").fadeIn(500);
					});
				} else {
					$("#msgdiv").html(obj.message);
					$("#msgdiv").fadeIn(500);
				}
			}
			
			
		});
	});

});
</script>

	</div>

	<div class="clear"></div>

</div>

<div class="clear"></div>

