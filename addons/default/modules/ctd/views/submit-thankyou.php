<div class="row">
    <?php       
         if(validation_errors()){
            echo '<div class="alert alert-danger message-wrapper" role="alert">'.validation_errors('<p class="form_error">','</p>').'</div>';
        }     
    ?>

</div>


<div class="row">

	<div class="message-wrapper">
		<div class="well">
			<h3>Thank you!</h3>
			<p>Thank you for submitting your Coral Triangle Day event. We will now process your submission, and notify you when it has been approved and published on the website.</p>
			<p> Please note that any materials submitted to the website may be used to promote the Coral Triangle Day. </p>
			<p> Have you entered the Coral Triangle Day Instagram contest? You could win big! Find out more <a href="{{url:site}}contest">here</a>. </p>
		</div>
	</div>
</div>

