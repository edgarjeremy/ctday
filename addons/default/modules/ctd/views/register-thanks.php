<div id="regform" style="background: #fff; height: 100%;">

<h1>Event Registration</h1>

	<p>
		<? echo lang('events_you_are_registering_to'); ?> "<? echo $event->title; ?>"<br />
		<p><?php echo lang('events_date_label')?>: <?php if (isset($event->date_from) && $event->date_from) echo (date('m/d/y H:i', $event->date_from)); else echo ' [no date from]'; ?> - <?php if (isset($event->date_to) && $event->date_to) echo (date('m/d/y H:i', $event->date_to)); else echo '[no date to]'; ?></p>
	</p>

<br />

<p><?php if (!empty($msg)) echo $msg; ?></p>

<br />
<table border="0" cellspacing="0">

			<tr><td width="150">
				<?php echo lang('events_reg_name_label');?></td>
				<td><?php echo @$fvalue['name']; ?>
			</td></tr>
			
			<tr><td width="150">
				<?php echo lang('events_reg_organisation_label');?></td>
				<td><?php echo ( isset($fvalue['organisation']) && $fvalue['organisation']) ? $fvalue['organisation'] : "&nbsp;"; ?>
			</td></tr>
	
			<tr><td width="150">
				<?php echo lang('events_reg_email_label');?></td>
				<td><?php echo ( isset($fvalue['email']) && $fvalue['email']) ? $fvalue['email'] : "&nbsp;"; ?>
			</td></tr>
			
			<tr><td width="150">
				<?php echo lang('events_reg_phone_label');?></td>
				<td><?php echo ( isset($fvalue['phone']) && $fvalue['phone']) ? $fvalue['phone'] : "&nbsp;"; ?>
			</td></tr>
	
			<tr><td width="150">
				<?php echo lang('events_reg_cellphone_label');?></td>
				<td><?php echo ( isset($fvalue['cellphone']) && $fvalue['cellphone']) ? $fvalue['cellphone'] : "&nbsp;"; ?>
			</td></tr>
	
			<tr><td width="150" valign="top">
				<?php echo lang('events_reg_address_label');?></td>
				<td><?php
					if (isset($fvalue['address']) && $fvalue['address']) {
						echo stripslashes(nl2br($fvalue['address']));
					} else {
						echo "&nbsp;";
					}
					?>
			</td></tr>

</table>

</div>