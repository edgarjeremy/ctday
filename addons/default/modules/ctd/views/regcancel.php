<div id="regform" style="background: #fff;">

<? if (@$msg): ?>
	<? echo "<br />" . $msg ?>
<? else: ?>

	<form action="<?= site_url('agenda/register/'.$event->id.'/?act=cancel') ?>" method="post" name="frmReg" id="frmReg" class="crud">
	
	<h1>Cancel Your Registration</h1>

	<? if (@$errmsg) echo $errmsg . "<br />\n"; ?>

	<p>
		"<? echo $event->title; ?>"<br />
		<p><?php echo lang('events_date_label')?>: <?php if (isset($event->date_from) && $event->date_from) echo (date('m/d/y H:i', $event->date_from)); else echo ' [no date from]'; ?> - <?php if (isset($event->date_to) && $event->date_to) echo (date('m/d/y H:i', $event->date_to)); else echo '[no date to]'; ?></p>
	</p>

		<ol>

			<li>
				<?php echo lang('events_cancel_attending_msg'); ?>
			</li>
			<li>
				<textarea name="reason" rows="4" cols="50"></textarea>
			</li>
	
			<li><input id="btnSubmit" type="submit" value="<?php echo lang('events_cancel_attendance_label'); ?>"></li>
			
		</ol>
	
	</form>
<script>
	document.frmReg.name.focus();
</script>
<? endif; ?>
</div>
