<?php if ($this->method == 'verifycancel'): ?>
<div class="contentArea">
<?php endif; ?>

<div id="regform" style="background: #fff;">

<? if (@$msg): ?>
	<? echo "<br />" . $msg ?>
<? else: ?>

	<form action="<?php echo site_url('agenda/verifycancel/'.$event->id.'/'.$uid.'/'.$vkey); ?>" method="post" name="frmReg" id="frmReg" class="crud">
	
	<h1>Cancel Your Registration</h1>

	<p>
		"<? echo $event->title; ?>"<br />
		<p><?php echo lang('events_date_label')?>: <?php if (isset($event->date_from) && $event->date_from) echo (date('M d, Y H:i', $event->date_from)); else echo ' [no date from]'; ?> - 
		<?php
		if (isset($event->date_to) && $event->date_to)
			echo ( date( ($event->date_to==$event->date_from ? 'M d, Y ' : '') .'H:i', $event->date_to));
		else
			echo '[no date to]'; ?></p>
	</p>

	<? if (@$errmsg) echo '<p style="padding:7px;clear:both;display:block;margin-right:180px;border:1px solid #ff1122;">' .$errmsg . "</p><br />\n"; ?>

		<ol>

			<li>
				<?php echo lang('events_cancel_ask_email_msg'); ?>
			</li>
			<li>
				<label><?php echo lang('events_reg_email_label'); ?></label>
				<input name="uemail" id="email" size="40" value="<?php echo (!empty($uemail) ? $uemail : '') ; ?>" />
			</li>

			<li>
				<label><?php echo lang('event_verify_key_label'); ?></label>
				<input name="vkey" id="vkey" size="40" value="<?php echo (!empty($vkey) ? $vkey : '') ; ?>" />
			</li>

			<li>
				<p><?php echo lang('events_cancel_attending_msg'); ?></p>
				<textarea name="notes" rows="5" cols="50"><?php echo (!empty($notes) ? $notes : '') ; ?></textarea>
			</li>
	
			<li><input id="btnSubmit" type="submit" value="<?php echo lang('events_cancel_attendance_label'); ?>"></li>
			
		</ol>
	
	</form>
<script>
	document.frmReg.email.focus();
</script>
<? endif; ?>
</div>

<?php if ($this->method == 'verifycancel'): ?>
</div>
<?php endif; ?>
