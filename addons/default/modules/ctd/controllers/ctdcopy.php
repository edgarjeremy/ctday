<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ctdcopy extends Public_Controller
{
	public $limit = 10; // TODO: PS - Make me a settings option

	public $vkey;
	public $notes;
	public $uemail;
	public $eid;

	public $register_rules = array(
			array(
				'field'				=> 'remind',
				'label'				=> 'lang:ctd_reg_email_label',
					'rules'	 			=> 'trim'
			),
			array(
				'field'				=> 'inform',
				'label'				=> 'lang:ctd_reg_email_label',
				'rules'	 			=> 'trim'
			),
			'email' => array(
				'field'				=> 'email',
				'label'				=> 'lang:ctd_reg_email_label',
				'rules'	 			=> 'trim|max_length[200]'
			)
		);

	protected $upload_cfg = array(
		'allowed_types'		=> 'jpg|gif|png|jpeg',
		'max_size'			=> '10000',
		'remove_spaces'		=> TRUE,
		'overwrite'			=> FALSE,
		'encrypt_name'		=> FALSE,
	);

	/**
	 * Width of thumbnail image
	 */
	public $t_gallery_w = 100;

	/**
	 * Height of thumbnail image
	 */
	public $t_gallery_h = 75;


	/**
	 * Width of small images
	 */
	public $s_gallery_w = 350;

	/**
	 * Height of small image
	 */
	public $s_gallery_h = 220;

	/**
	 * Maximum width of image
	 */
	public $m_gallery_w = 705;

	/**
	 * Maximum height of the image
	 */
	public $m_gallery_h = 469;

	/**
	 * Accepted image formats
	 */
	public $format_images = array('jpg', 'gif', 'png', 'jpeg');


	private $_file_rules = array(
		array(
			'field' => 'name',
			'label' => 'Your Name',
			'rules' => 'trim|required|max_length[100]'
		),
		array(
			'field' => 'email',
			'label' => 'Email address',
			'rules' => 'trim|required|valid_email|max_length[100]'
		),
		array(
			'field' => 'event',
			'label' => 'Event',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'country',
			'label' => 'Country',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'filecheck',
			'label' => 'Image',
			'rules' => 'trim|callback__check_file'
		),
	);

	protected $countries = array(
			'' => 'Select',
			'FJ' => 'Fiji',
			'ID' => 'Indonesia',
			'MY' => 'Malaysia',
			'PG' => 'Papua New Guinea',
			'PH' => 'Philippines',
			'SB' => 'Solomon Islands',
			'TL' => 'Timor Leste',
		);


	function __construct()
	{
		parent::__construct();		

		$this->load->model(
			array(
				'ctd_m',
				'ctd_categories_m',
				'ctd_partners_m',
				'galleries/galleries_m',
			)
		);
		$this->load->helper(array('text', 'filename'));
		$this->lang->load(array('ctd', 'categories'));

		// get categories
		$categories = $this->ctd_categories_m->get_all();
		$this->template->set('categories', $categories);

		// RSS Feeds
		$this->template->append_metadata('<link rel="alternate" type="application/rss+xml" title="Event RSS" href="'.site_url('ctd/rss').'" />');
		
		$this->template->set('countries', $this->countries);
	}

	// ctd/page/x also routes here
	function index()
	{	
		$total_rows = $this->ctd_m->count_by(array('status' => 'live'));
		$pagination = create_pagination('ctd/page', $total_rows, NULL, 3);

		$ctd = $this->ctd_m->limit($pagination['limit'], $pagination['offset'])->get_many_by(array('status' => 'live', 'sort'=>'DESC'));	
/*
		$this->pyrocache->model('ctd_m', 'limit', array($pagination['limit']));
		$ctd = $this->pyrocache->model('ctd_m', 'get_many_by', array(array('status'=>'live')));
*/

		// Set meta description based on article titles
		$meta = $this->_articles_metadata($ctd);

			$this->template
				->set_metadata('description', $meta['description'])
				->set_metadata('keywords', $meta['keywords'])
				->set_breadcrumb($this->lang->line('ctd_articles_title') )
				->title('Events')
				->set("pagination", $pagination)
				->set("ctd", $ctd)
				->build('index');

	}

	public function subscribe()
	{
		if (!$this->settings->mailchimp_api && !$this->settings->mailchimp_list_id)
		{
			echo json_encode(array('status'=> 'notice', 'message' => 'Unable to process at this time.'));
		}
		else
		{
			$email = $this->input->post('email');

			$this->load->library('form_validation');
			$rules = array(
				array(
					'field'				=> 'email',
					'label'				=> 'Email address',
					'rules'	 			=> 'trim|required|valid_email|max_length[200]'
				),
			);
			$this->form_validation->set_rules($rules);

			if ($this->form_validation->run())
			{
				$config = array(
					'apikey'	=> $this->settings->mailchimp_api,
					'secure'	=> TRUE
				);

				$this->load->library('MCAPI', $config, 'mail_chimp');

				$sub = $this->mail_chimp->listSubscribe($this->settings->mailchimp_list_id, $email, null, null, false, null, null, true);
				if($sub)
				{
					$ret = array(
						'status'	=> 'success',
						'message'	=> 'Thank you, you have been subscribed successfully.'
					);
				}
				else
				{
					$er = $this->mail_chimp->errorCode;
					switch($er)
					{
						case '214':
							$message = 'You have already subscribed to our email updates.';
							break;
						default:
							$message = 'Can not process subscriptions at this time.';
							break;
					}
					$ret = array(
						'status'	=> 'error',
						'message'	=> $message
					);
				}

			}
			else
			{
				if (isset($_POST))
				{
					$ret = array(
						'status'	=> 'error',
						'message'	=> form_error('email'),
						'email'		=> $email
					);
				}
			}

			echo json_encode($ret);
		}
	}

	public function event($id)
	{
		if (!$id) return false;
		$event = $this->ctd_m->get($id);
		if ($id)
		{
			$this->template
				->set_layout(FALSE)
				->set('event', $event)
				->build('view-ajax');
		}
/*
		echo "<h3>OTOKOWOK!</h3>\n";
		echo "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n";
*/
	}

	public function submitevent()
	{
//$this->output->enable_profiler(TRUE);

		$this->validation_rules = array(
			array(
				'field'				=> 'submitted_by_name',
				'label'				=> 'Your Name',
				'rules'	 			=> 'trim|required|max_length[100]'
			),
			array(
				'field'				=> 'submitted_by_email',
				'label'				=> 'Your Email Address',
				'rules'	 			=> 'trim|required|valid_email|max_length[100]'
			),
			array(
				'field'				=> 'title',
				'label'				=> 'lang:ctd_title_label',
				'rules'	 			=> 'trim|required|max_length[200]'
			),
			array(
				'field'				=> 'slug',
				'label'				=> 'lang:ctd_slug_label',
				'rules'	 			=> 'trim'
			),
			array(
				'field' 			=> 'intro',
				'label'				=> 'lang:ctd_intro_label',
				'rules' 			=> 'trim|required'
			),
			array(
				'field' 			=> 'date_from_str',
				'label'				=> lang('ctd_from_label') . ' ' . lang('ctd_date_label'),
				'rules' 			=> 'trim|required'
			),
			array(
				'field'				=> 'location',
				'label'				=> 'City',
				'rules'	 			=> 'trim|required|max_length[250]'
			),
			array(
				'field'				=> 'geolocation',
				'label'				=> 'lang:ctd_geolocation_label',
				'rules'	 			=> 'trim'
			),
			array(
				'field'				=> 'geocoordinate',
				'label'				=> 'lang:ctd_geolocation_label',
				'rules'	 			=> 'trim'
			),
			array(
				'field' 			=> 'hide_map',
				'label'				=> 'lang:ctd_do_not_display_map',
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'views',
				'label'				=> 'lang:ctd_views_label',
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'category_id',
				'label'				=> 'lang:ctd_category_label',
				'rules' 			=> 'trim|alpha_numeric'
			),
			array(
				'field' 			=> 'date_to_str',
				'label'				=> lang('ctd_to_label') . ' ' . lang('ctd_date_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'date_publish_str',
				'label'				=> 'lang:ctd_publish_date_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'partner_logos[]',
				'label'				=> 'Partner Logos',
				'rules'				=> 'trim'
			),
			array(
				'field' 			=> 'meta_title',
				'label'				=> 'lang:ctd_meta_title_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'meta_description',
				'label'				=> 'lang:ctd_meta_desc_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'meta_keywords',
				'label'				=> 'lang:ctd_meta_keywords_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'date_to_hour',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_to_label') . ' ' .lang('ctd_hour_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'date_to_minute',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_to_label') . ' ' .lang('ctd_minute_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'date_from_hour',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_from_label') . ' ' .lang('ctd_hour_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'date_from_minute',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_from_label') . ' ' .lang('ctd_minute_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'publish_on_hour',
				'label'				=> lang('ctd_hour_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'publish_on_minute',
				'label'				=> lang('ctd_minute_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'event_venue',
				'label'				=> 'Event Venue',
				'rules' 			=> 'trim|required'
			),
			array(
				'field' 			=> 'venue_address',
				'label'				=> 'Venue Address',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'country',
				'label'				=> 'Country',
				'rules' 			=> 'trim|required'
			),
			array(
				'field' 			=> 'geocoordinates',
				'label'				=> 'Geocoordinates',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'website',
				'label'				=> lang('ctd_website_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'contact_email',
				'label'				=> lang('ctd_contact_email_label'),
				'rules' 			=> 'trim|valid_email|max_length[100]'
			),
		);

		$this->hours = array_combine($hours = range(0, 23), $hours);
		$this->minutes = array_combine($minutes = range(0, 59), $minutes);

		$this->categories = array();
		if($categories = $this->ctd_categories_m->get_all())
		{
			foreach($categories as $category)
			{
				$this->categories[$category->id] = $category->title;
			}
		}
		
		$this->ctd_status = array(
			'draft'=>lang('ctd_draft_label'),
			'live'=>lang('ctd_live_label')
		);

		$this->upload_cfg['upload_path'] = UPLOAD_PATH . 'ctd';
		$this->_check_dir($this->upload_cfg['upload_path']);

		$this->_path = FCPATH . $this->config->item('files_folder');
		$this->_check_dir($this->_path);

		$this->load->library('form_validation');

		$this->form_validation->set_rules($this->validation_rules);
		
		$input = $this->input->post();

		$event = new stdClass();
		
		if ($this->input->post('date_publish_str'))
		{
			$df = explode('-', $this->input->post('date_publish_str'));
			$date_publish_day = $df[2];
			$date_publish_month = $df[1];
			$date_publish_year = $df[0];
			$date_publish = mktime((int)$input['publish_on_hour'], (int)$input['publish_on_minute'], 0, $date_publish_month, $date_publish_day, $date_publish_year);
			unset($df);
		} else {
			$date_publish = NULL;
		}

		if ($this->input->post('date_from_str'))
		{
			$df = explode('/', $this->input->post('date_from_str'));
			@$date_from_day = $df[1];
			@$date_from_month = $df[0];
			@$date_from_year = $df[2];
			@$date_from = mktime((int)$this->input->post('date_from_hour'), (int)$this->input->post('date_from_minute'), 0, $date_from_month, $date_from_day, $date_from_year);
			@$date_to = mktime($this->input->post('date_to_hour'), $this->input->post('date_to_minute'), 0, $date_from_month, $date_from_day, $date_from_year);
			unset($df);
		} else {
			$date_from = 0;
			$date_to = 0;
		}


		if ($this->input->post('date_to_str'))
		{
			$dt = explode('/', $this->input->post('date_to_str'));
			$date_to_day = $dt[1];
			$date_to_month = $dt[0];
			$date_to_year = $dt[2];
			$date_to = mktime($this->input->post('date_to_hour'), $this->input->post('date_to_minute'), 0, $date_to_month, $date_to_day, $date_to_year);
			unset($dt);
		} else {
			$date_to = 0;
		}

		// read partners
		$files = $this->input->post('file_id');
		$partners = '';
		if (!empty($partners))
		{
			$partners = $this->load->view('admin/files/contents', array('files'=>$files), TRUE);
		}

		if ($this->form_validation->run())
		{
			$input = array(
	            'title'				=> $this->input->post('title'),
	            'intro'				=> $this->input->post('intro'),
	            'body'				=> $this->input->post('body'),
				'notes'	 			=> $this->input->post('notes'),

				'slug' 				=> $this->input->post('slug'),
	            'category_id'		=> $this->input->post('category_id'),
	            'status'			=> 'draft',

				'location' 			=> $this->input->post('location'),
				'geolocation' 		=> $this->input->post('geolocation'),
				'geocoordinates' 	=> $this->input->post('geocoordinates'),
				'hide_map'			=> (int)$this->input->post('hide_map'),

				'meta_title'	 	=> $this->input->post('meta_title'),
				'meta_description' 	=> $this->input->post('meta_description'),
				'meta_keywords' 	=> $this->input->post('meta_keywords'),
				
				'date_from' 		=> $date_from,
				'date_to' 			=> $date_to,
				'event_venue' 		=> $this->input->post('event_venue'),
				'venue_address' 	=> $this->input->post('venue_address'),
				
				'country' 			=> $this->input->post('country'),
				//'city' 				=> $this->input->post('city'),
				'website' 			=> $this->input->post('website'),
				'contact_email' 	=> $this->input->post('contact_email'),

				'submitted_by_name' => $this->input->post('submitted_by_name'),
				'submitted_by_email' => $this->input->post('submitted_by_email'),
				
//				'publish_on' 		=> $date_publish,
//				'date_from' 		=> $date_from,
//				'date_to' 			=> $date_to

	    	);

			if (!empty($filename))
			{
				$input['slide'] = $filename;
			}

			$ctd_id = $this->ctd_m->insert($input);
    	
			if (!empty($ctd_id))
			{

				// process uploaded files
				// load galleries model
				$this->load->model(array('galleries/galleries_m', 'galleries/gallery_categories_m', 'galleries/gallery_media_m'));

				if (!empty($_FILES['imgorganizer']))
				{
					$gallery_id = $this->galleries_m->get_id_from_slug('organizer');

					if ($gallery_id)
					{
						// Setup upload config
						$this->upload_cfg['upload_path'] = UPLOAD_PATH . 'galleries/'.$gallery_id;
						$this->_check_dir($this->upload_cfg['upload_path']);
						$this->load->library('upload');

						$cnt = count($_FILES['imgorganizer']['name']);
						$files = $_FILES['imgorganizer'];
						for($i=0; $i<$cnt; $i++)
						{
							$_FILES['userfile']['name'] = $files['name'][$i];
							$_FILES['userfile']['type'] = $files['type'][$i];
							$_FILES['userfile']['tmp_name'] = $files['tmp_name'][$i];
							$_FILES['userfile']['error'] = $files['error'][$i];
							$_FILES['userfile']['size'] = $files['size'][$i]; 


							// initialize
							$this->upload->initialize($this->upload_cfg);

							// File upload error
							if ( ! $this->upload->do_upload())
							{
								$status		= 'error';
								$message	= $this->upload->display_errors();
				
								if ($this->input->is_ajax_request())
								{
									$data = array();
									$data['messages'][$status] = $message;
									$message = $this->load->view('admin/partials/notices', $data, TRUE);
				
									return $this->template->build_json(array(
										'status'	=> $status,
										'message'	=> $message
									));
								}

								$messages[$status] = $message;
							}
							// file upload success
							else
							{
								$file = $this->upload->data();
								$data = array(
									'media'			=> $file['file_name'],
									'gallery_id'	=> $gallery_id,
									'title'			=> '',
									'subtitle'		=> '',
									'description'	=> '',
									'status'		=> 'draft',
									'extension'		=> $file['file_ext'],
									'uploaded_by'	=> $input['submitted_by_name'],
									'uploaded_on'	=> now(),
									'external_url'	=> $_POST['websiteorganizer'][$i],
									'position'		=> 999
								);

								if ($file['is_image'])
								{
									$this->load->library('image_lib');
									$filename = $file['file_name'];
				
									$ext = substr(strrchr($filename, "."), 1);
									$fn = str_ireplace('.'.$ext, '', $filename);
									$thumbfile = $fn . '_thumb.' . $ext;
									$smallfile = $fn . '_small.' . $ext;
									$medfile = $fn . '_med.' . $ext;
									/*---------------------------------------------------------------------------------
									// create thumb - admin
									*/
									$image_cfg['source_image'] = $file['full_path'];
									$image_cfg['maintain_ratio'] = TRUE;
									$image_cfg['master_dim'] = 'height';
									$image_cfg['width'] = $this->t_gallery_w;// ($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
									$image_cfg['height'] = $this->t_gallery_h; //($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
									$image_cfg['create_thumb'] = FALSE;
									$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
									$this->image_lib->initialize($image_cfg);
									$img_ok = $this->image_lib->resize();
									unset($image_cfg);
									$this->image_lib->clear();
					
									/*---------------------------------------------------------------------------------
									// create thumb - frontend
									*/
									$image_cfg['source_image'] = $file['full_path'];
									$image_cfg['maintain_ratio'] = TRUE;
									$image_cfg['master_dim'] = 'height';
									$image_cfg['width'] = $this->s_gallery_w; //($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
									$image_cfg['height'] = $this->s_gallery_h; //($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
									$image_cfg['create_thumb'] = FALSE;
									$image_cfg['new_image'] = $file['file_path'] . $smallfile;
									$this->image_lib->initialize($image_cfg);
									$img_ok = $this->image_lib->resize();
									unset($image_cfg);
									$this->image_lib->clear();
				
										/*
										/* image resize - medium
										*/
										$image_cfg['source_image'] = $file['full_path'];
										$image_cfg['maintain_ratio'] = TRUE;
										$image_cfg['width'] = $this->m_gallery_w; //($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
										$image_cfg['height'] = $this->m_gallery_h; //($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
										$image_cfg['create_thumb'] = FALSE;
										$image_cfg['new_image'] = $file['file_path'] . $medfile;
										$this->image_lib->initialize($image_cfg);
										$img_ok = $this->image_lib->resize();
										unset($image_cfg);
										$this->image_lib->clear();
				
								}
				
								// Insert success
								if ($id = $this->galleries_m->insertimg($gallery_id, $data))
								{
									$organizers_images[] = $id;
									$status		= 'success';
									$message	= lang('galleries.upload_img_success');
								}
								// Insert error
								else
								{
									$error[] = 'Failed saving organizer\'s logo: '.$file['client_name'];
									$this->_unlinkfile($file['file_name'], $gallery_id);
								}
				
							}	// end of file upload success
						}		// end for(i=0....)
					}
				}


				if (!empty($_FILES['imgsponsor']))
				{
					$sponsor_gallery_id = $this->galleries_m->get_id_from_slug('sponsors');

					if ($sponsor_gallery_id)
					{
						// Setup upload config
						$this->upload_cfg['upload_path'] = UPLOAD_PATH . 'galleries/'.$sponsor_gallery_id;
						$this->_check_dir($this->upload_cfg['upload_path']);
						$this->load->library('upload');

						$cnt = count($_FILES['imgsponsor']['name']);
						$files = $_FILES['imgsponsor'];
						for($i=0; $i<$cnt; $i++)
						{
							$_FILES['userfile']['name'] = $files['name'][$i];
							$_FILES['userfile']['type'] = $files['type'][$i];
							$_FILES['userfile']['tmp_name'] = $files['tmp_name'][$i];
							$_FILES['userfile']['error'] = $files['error'][$i];
							$_FILES['userfile']['size'] = $files['size'][$i]; 


							// initialize
							$this->upload->initialize($this->upload_cfg);

							// File upload error
							if ( ! $this->upload->do_upload())
							{
								$status		= 'error';
								$message	= $this->upload->display_errors();
				
								if ($this->input->is_ajax_request())
								{
									$data = array();
									$data['messages'][$status] = $message;
									$message = $this->load->view('admin/partials/notices', $data, TRUE);
				
									return $this->template->build_json(array(
										'status'	=> $status,
										'message'	=> $message
									));
								}

								$messages[$status] = $message;
							}
							// file upload success
							else
							{
								$file = $this->upload->data();
								$data = array(
									'media'			=> $file['file_name'],
									'gallery_id'	=> $sponsor_gallery_id,
									'title'			=> '',
									'subtitle'		=> '',
									'description'	=> '',
									'status'		=> 'draft',
									'extension'		=> $file['file_ext'],
									'uploaded_by'	=> $input['submitted_by_name'],
									'uploaded_on'	=> now(),
									'external_url'	=> $_POST['websitesponsor'][$i],
									'position'		=> 999
								);

								if ($file['is_image'])
								{
									$this->load->library('image_lib');
									$filename = $file['file_name'];
				
									$ext = substr(strrchr($filename, "."), 1);
									$fn = str_ireplace('.'.$ext, '', $filename);
									$thumbfile = $fn . '_thumb.' . $ext;
									$smallfile = $fn . '_small.' . $ext;
									$medfile = $fn . '_med.' . $ext;
									/*---------------------------------------------------------------------------------
									// create thumb - admin
									*/
									$image_cfg['source_image'] = $file['full_path'];
									$image_cfg['maintain_ratio'] = TRUE;
									$image_cfg['master_dim'] = 'height';
									$image_cfg['width'] = $this->t_gallery_w;// ($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
									$image_cfg['height'] = $this->t_gallery_h; //($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
									$image_cfg['create_thumb'] = FALSE;
									$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
									$this->image_lib->initialize($image_cfg);
									$img_ok = $this->image_lib->resize();
									unset($image_cfg);
									$this->image_lib->clear();
					
									/*---------------------------------------------------------------------------------
									// create thumb - frontend
									*/
									$image_cfg['source_image'] = $file['full_path'];
									$image_cfg['maintain_ratio'] = TRUE;
									$image_cfg['master_dim'] = 'height';
									$image_cfg['width'] = $this->s_gallery_w; //($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
									$image_cfg['height'] = $this->s_gallery_h; //($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
									$image_cfg['create_thumb'] = FALSE;
									$image_cfg['new_image'] = $file['file_path'] . $smallfile;
									$this->image_lib->initialize($image_cfg);
									$img_ok = $this->image_lib->resize();
									unset($image_cfg);
									$this->image_lib->clear();
				
										/*
										/* image resize - medium
										*/
										$image_cfg['source_image'] = $file['full_path'];
										$image_cfg['maintain_ratio'] = TRUE;
										$image_cfg['width'] = $this->m_gallery_w; //($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
										$image_cfg['height'] = $this->m_gallery_h; //($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
										$image_cfg['create_thumb'] = FALSE;
										$image_cfg['new_image'] = $file['file_path'] . $medfile;
										$this->image_lib->initialize($image_cfg);
										$img_ok = $this->image_lib->resize();
										unset($image_cfg);
										$this->image_lib->clear();
				
								}
				
								// Insert success
								if ($id = $this->galleries_m->insertimg($sponsor_gallery_id, $data))
								{
									$sponsors_images[] = $id;
									$status		= 'success';
									$message	= lang('galleries.upload_img_success');
								}
								// Insert error
								else
								{
									$error[] = 'Failed saving organizer\'s logo: '.$file['client_name'];
									$this->_unlinkfile($file['file_name'], $sponsor_gallery_id);
								}
				
							}	// end of file upload success
						}		// end for(i=0....)
					}
				}

				if (!empty($organizers_images))
				{
					$update_ctd['organiser'] = serialize($organizers_images);
					$this->ctd_m->update($ctd_id, $update_ctd);
				}

				if (!empty($sponsors_images))
				{
					$update_ctd['sponsors'] = serialize($sponsors_images);
					$this->ctd_m->update($ctd_id, $update_ctd);
				}

				$this->pyrocache->delete_all('ctd_m');
				$this->session->set_flashdata('success', 'Your event titled "<b>'.$this->input->post('title').'</b>" has been saved successfully.<br />');
				redirect('ctd/thankyou/'.$id);

				// read and update partner logo's id
//				$file_ids = $this->input->post('file_id');
//				if (!empty($file_ids))
//				{
//					$input = array();
//					$pos = 1;
//					foreach($this->input->post('file_id') as $filedata)
//					{
//						$input['ctd_id'] = $id;
//						$input['position'] = $pos;
//						$this->ctd_partners_m->updatefile($filedata, $input);
//						$pos++;
//					}
//				}

			}
			else
			{
				$this->session->set_flashdata('error', $this->lang->line('ctd_article_add_error'));
			}			

			// Redirect back to the form or main page
			//$this->input->post('btnAction') == 'save_exit' ? redirect('admin/ctd') : redirect('admin/ctd/edit/'.$id);
		}
		else{
			
			$this->session->set_flashdata('error', $this->lang->line('ctd_article_add_error'));
		}
//		else
//		{
			// Go through all the known fields and get the post values
			foreach($this->validation_rules as $key => $field)
			{
				$event->$field['field'] = set_value($field['field']);
				if ($field['field'] == 'curslide' && $this->input->post('curslide'))
				{
					$event->slide = $this->input->post('curslide');
					$event->curslide = $this->input->post('curslide');
				}
			}

			if (!empty($filename))
			{
				$event->slide = $filename;
				$event->curslide = $filename;
			}

			if ($this->input->post('article_id')) $event->id = $this->input->post('article_id');
			//$event->created_on = now();

//		}
		
//		$event->date_from = $date_from;
//		$event->date_to = $date_to;
		//$this->data->article =& $event;
		
		//Load google maps API
#		$this->template->append_metadata('<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>');

		if ($this->input->is_ajax_request())
			$this->template->set_layout(FALSE);

		$data = array(
			'event' 	=> $event,
			'hours'		=> $this->hours,
			'minutes'	=> $this->minutes,
			'categories'=> $this->categories,
			'ctd_status'=> $this->ctd_status
		);
		
		
		
		$this->template
			->title($this->module_details['name'], lang('ctd_create_title'))
			//->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			//->append_css('module::files.css' )
			//->append_css('module::jquery.fileupload-ui.css')
			//->append_js('module::jquery.cooki.js')
			//->append_js('module::jquery.fileupload.js')
			//->append_js('module::jquery.fileupload-ui.js')
			//->append_js('module::jquery.ba-hashchange.min.js')
			//->append_metadata('<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>')
			//->append_js('module::functions.js' )
			//->append_js('module::ctd_form.js' )
			->append_js('module::jquery/jquery.livequery.min.js' )
			->set('partners', $partners)
			->set('event', $event)
			->set('hours', $this->hours)
			->set('minutes', $this->minutes)
			->set('categories', $this->categories)
			->set('ctd_status', $this->ctd_status)
			->build('form');
			
	}

	function thankyou($id=0)
	{
		$this->template->build('submit-thankyou');
	}

	function getevents($country)
	{
		if (!$country) return false;

		$res = array();

		$events = $this->ctd_m->get_many_by(array('country'=>$country, 'status'=>'live', 'orderby'=>'title', 'sort'=>'asc'));
		if ($events)
		{
			$res[] = '<option value="">Select the event</option>';
			foreach($events as $event)
			{
				$res[] = '<option value="'.$event->title.'">'.$event->title.'</option>';
			}
			echo implode("\n", $res);
		}
		else
		{
			echo '<option value="">No events for '.$this->countries[$country].'</option>';
		}
	}

	function uploadimage()
	{
		$gallery_id = $this->galleries_m->get_id_from_slug('events');
		if (!$gallery_id)
		{
			// we should create it on the fly
		}
if ($_POST)
{
echo json_encode(array('status'=>'ok'));
exit;
?>
<pre>
$_POST:
<? print_r($_POST) ?>
<hr />

$_FILES:
<? print_r($_FILES) ?>
</pre>
<?
exit;
}

		$countries = array();
		foreach($this->countries as $key=>$country)
		{
			if ($country=='Select')
				$countries[""]=$country;
			else
				$countries[$key]=$country;
		}

		$this->lang->load('galleries/galleries');

		$gallery = $this->galleries_m->get($gallery_id);

		$ctd = $this->ctd_m->get_many_by(array('status'=>'live', 'orderby'=>'title', 'sort'=>'asc'));

		$events = array(''=>'Select event');

		if (!empty($ctd))
		{
			foreach($ctd as $c)
			{
				$events[$c->id] = $c->title;
			}
			$this->template->set('events', $events);
		}

		if ($this->input->post('event'))
		{
			$event = $this->ctd_m->get($this->input->post('event'));
		}

		//$form		= $this->load->view('uploads/upload-form', array('file'=>$file), TRUE);

		$file = $data = new stdClass();
		foreach($this->_file_rules as $rule)
		{
			$file->{$rule['field']} = $this->input->post($rule['field']);
		}


		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_file_rules);

		// Setup upload config
		$this->upload_cfg['upload_path'] = UPLOAD_PATH . 'galleries/'.$gallery_id;
		//$this->upload_cfg['upload_path'] .= '/'.$gallery_id;
		$this->load->library('upload');
		$this->upload->initialize($this->upload_cfg);

		$this->_check_dir($this->upload_cfg['upload_path']);

		if ($this->form_validation->run())
		{

			// check directory exists
			//$this->check_dir($this->upload_cfg['upload_path']);



			if (!empty($_FILES['imgfile']))
					{
						$sponsor_gallery_id = $this->galleries_m->get_id_from_slug('events');
	
						if ($sponsor_gallery_id)
						{
							// Setup upload config
							$this->upload_cfg['upload_path'] = UPLOAD_PATH . 'galleries/'.$sponsor_gallery_id;
							$this->_check_dir($this->upload_cfg['upload_path']);
							$this->load->library('upload');
	
							$cnt = count($_FILES['imgfile']['name']);
							$files = $_FILES['imgfile'];
							for($i=0; $i<$cnt; $i++)
							{
								$_FILES['userfile']['name'] = $files['name'][$i];
								$_FILES['userfile']['type'] = $files['type'][$i];
								$_FILES['userfile']['tmp_name'] = $files['tmp_name'][$i];
								$_FILES['userfile']['error'] = $files['error'][$i];
								$_FILES['userfile']['size'] = $files['size'][$i]; 
	
	
								// initialize
								$this->upload->initialize($this->upload_cfg);
	
								// File upload error
								if ( ! $this->upload->do_upload())
								{
									$status		= 'error';
									$message	= $this->upload->display_errors();
					
									if ($this->input->is_ajax_request())
									{
										$data = array();
										$data['messages'][$status] = $message;
										//$message = $this->load->view('admin/partials/notices', $data, TRUE);
					
										return $this->template->build_json(array(
											'status'	=> $status,
											'message'	=> $message
										));
									}
	
									$messages[$status] = $message;
								}
								// file upload success
								else
								{
									$file = $this->upload->data();
									$data = array(
										'media'			=> $file['file_name'],
										'gallery_id'	=> $gallery_id,
										'title'			=> $this->input->post('country'),
										'subtitle'		=> $file['file_name'],
										'description'	=> '',
										'extension'		=> $file['file_ext'],
										'status'		=> 'draft',
										'uploaded_by'	=> $this->input->post('name'),
										'uploaded_on'	=> now(),
										'vuploaded_by_email'	=> $this->input->post('email'),
										'position'		=> 999
									);
	
									if ($file['is_image'])
									{
										$this->load->library('image_lib');
										$filename = $file['file_name'];
					
										$ext = substr(strrchr($filename, "."), 1);
										$fn = str_ireplace('.'.$ext, '', $filename);
										$thumbfile = $fn . '_thumb.' . $ext;
										$smallfile = $fn . '_small.' . $ext;
										$medfile = $fn . '_med.' . $ext;
										/*---------------------------------------------------------------------------------
										// create thumb - admin
										*/
										$image_cfg['source_image'] = $file['full_path'];
										$image_cfg['maintain_ratio'] = TRUE;
										$image_cfg['master_dim'] = 'height';
										$image_cfg['width'] = $this->t_gallery_w;// ($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
										$image_cfg['height'] = $this->t_gallery_h; //($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
										$image_cfg['create_thumb'] = FALSE;
										$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
										$this->image_lib->initialize($image_cfg);
										$img_ok = $this->image_lib->resize();
										unset($image_cfg);
										$this->image_lib->clear();
						
										/*---------------------------------------------------------------------------------
										// create thumb - frontend
										*/
										$image_cfg['source_image'] = $file['full_path'];
										$image_cfg['maintain_ratio'] = TRUE;
										$image_cfg['master_dim'] = 'height';
										$image_cfg['width'] = $this->s_gallery_w; //($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
										$image_cfg['height'] = $this->s_gallery_h; //($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
										$image_cfg['create_thumb'] = FALSE;
										$image_cfg['new_image'] = $file['file_path'] . $smallfile;
										$thumbnail = $file['file_path'] . $smallfile;
										$this->image_lib->initialize($image_cfg);
										$img_ok = $this->image_lib->resize();
										unset($image_cfg);
										$this->image_lib->clear();
										
											/*
											/* image resize - medium
											*/
											$image_cfg['source_image'] = $file['full_path'];
											$image_cfg['maintain_ratio'] = TRUE;
											$image_cfg['width'] = $this->m_gallery_w; //($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
											$image_cfg['height'] = $this->m_gallery_h; //($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
											$image_cfg['create_thumb'] = FALSE;
											$image_cfg['new_image'] = $file['file_path'] . $medfile;
											$this->image_lib->initialize($image_cfg);
											$img_ok = $this->image_lib->resize();
											unset($image_cfg);
											$this->image_lib->clear();
					
									}
					
															
									// Insert success
									if ($id = $this->galleries_m->insertimg($gallery_id, $data))
									{
										$status		= 'success';
										$message	= 'Thank you for your photo! We will review and make it online as soon as possible.';
									}
									// Insert error
									else
									{
										$this->_unlinkfile($file['file_name'], $gallery_id);
										$status		= 'error';
										$message	= sprintf(lang('galleries.dbstore_img_error'), $file['file_name']);
									}
					
									if ($this->input->is_ajax_request())
									{
										$data = array();
										$data['messages'][$status] = $message;
										//$message = $this->load->view('notices', $data, TRUE);
					
										return $this->template->build_json(array(
											'status'	=> $status,
											'message'	=> $message,
											'file'		=> array(
												'name'	=> $file['file_name'],
												'type'	=> $file['file_type'],
												'size'	=> $file['file_size']
											)
										));
									}
									else
									{
										$this->session->set_flashdata($status, $message);
										redirect('ctd/uploadimage');
									}
					
									if ($status === 'success')
									{
										$this->session->set_flashdata($status, $message);
										redirect('ctd/uploadimage');
									}
									else
									{
										if ($this->input->is_ajax_request())
										{
											$data = array();
											$data['messages'][$status] = $message;
											//$message = $this->load->view('notices', $data, TRUE);
							
										}
									}
					
								}	// end of file upload success
							}		// end for(i=0....)
						}
					}

		}
		elseif (validation_errors())
		{
			$err_msg = validation_errors();
			$this->template->set('err_msg', $err_msg);
		}

		$this->template
			->title()
			->set('file', $file)
			->set('countries', $countries)
			->build('upload');
	}

	function category($slug = '')
	{	
		if(!$slug) redirect('ctd');
		
		// Get category data
		$category = $this->ctd_categories_m->get_by('slug', $slug);
		
		if(!$category) show_404();
		
		// RSS Feeds
		$this->template->append_metadata('<link rel="alternate" type="application/rss+xml" title="Event RSS by Category '.$category->title.'" href="'.site_url('ctd/rss/category/'.$slug).'" />');

		//$this->data->category =& $category;
		
		// Count total event articles and work out how many pages exist
		$pagination = create_pagination('ctd/category/'.$slug, $this->ctd_m->count_by(array(
			'category'=>$slug,
			'status' => 'live'
		)), $this->limit, 4);
		
		// Get the current page of event articles
		$ctd = $this->ctd_m->limit($pagination['limit'])->get_many_by(array(
			'category'=>$slug,
			'status' => 'live'
		));
		
		// Set meta description based on article titles
		$meta = $this->_articles_metadata($ctd);

		// Build the page
		$this->template->title( lang('ctd_ctd_title'), $category->title )		
			->set_metadata('description', $category->title.'. '.$meta['description'] )
			->set_metadata('keywords', $category->title )
			->set_breadcrumb( lang('ctd_ctd_title'), 'ctd')
			->set_breadcrumb( lang('ctd_category_label') . ': ' . $category->title )
			->set("pagination", $pagination)
			->set("ctd", $ctd)
			->build( 'index' );
	}	
	
	function calendar($year = NULL, $month = '01', $intro = 'true')
	{
		if(!$year) $year = date('Y');		
		$month_date = new DateTime($year.'-'.$month.'-01');

		if ($intro == 'true')
		{
			$show_intro = '';
		}
		else
		{
			$show_intro = '/false';
		}
/*
		$this->data->pagination = create_pagination('ctd/archive/'.$year.'/'.$month, $this->ctd_m->count_by(array('year'=>$year,'month'=>$month)), $this->limit, 5);
		$evts = $this->ctd_m->limit($this->data->pagination['limit'])->get_many_by(array('year'=> $year,'month'=> $month));
*/
		$evts = $this->ctd_m->get_many_by(array('year'=> $year,'month'=> $month));
		$this->data->month_year = $month_date->format("F 'y");

		$prefs['show_next_prev'] = TRUE;
		$prefs['next_prev_url'] = site_url('ctd/calendar');
		$prefs['template'] = '
   {div_open}<div id="calendar><!-- div_open -->{/div_open}

   {heading_row_start}<tr>{/heading_row_start}

   {heading_previous_cell}<td><div class="left"><a class="prev" href="{previous_url}'.$show_intro.'"><span>&lt;&lt;</span></a></td>{/heading_previous_cell}
   {heading_title_cell}<td colspan="{colspan}"><h3>{heading}</h3></td>{/heading_title_cell}
   {heading_next_cell}<td><div class="right"><a class="next" href="{next_url}'.$show_intro.'"><span>&gt;&gt;</span></a></td>{/heading_next_cell}

   {heading_row_end}</tr>{/heading_row_end}

   {table_open}<table border="0" cellpadding="0" cellspacing="0">{/table_open}

   {week_row_start}<tr>{/week_row_start}
   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
   {week_row_end}</tr>{/week_row_end}

   {cal_row_start}<tr>{/cal_row_start}
   {cal_cell_start}<td>{/cal_cell_start}

   {cal_cell_content}<a class="tiptip" href="{content}">{day}</a><div class="tipContent">{title}</div>{/cal_cell_content}
   {cal_cell_content_today}<a class="tiptip highlight" href="{content}">{day}</a><div class="tipContent">{title}</div>{/cal_cell_content_today}

   {cal_cell_no_content}{day}{/cal_cell_no_content}
   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

   {cal_cell_blank}&nbsp;{/cal_cell_blank}

   {cal_cell_end}</td>{/cal_cell_end}
   {cal_row_end}</tr>{/cal_row_end}

   {table_close}</table>{/table_close}
   {div_close}<!-- div_close --></div>{/div_close}
		';

		if (!empty($evts))
		{
			foreach($evts as $e)
			{
				$hours = date('H:i; ', $e->date_from);
				if ($hours == '00:00; ' && ($e->date_from == $e->date_to OR !$e->date_to) )
					$hours = '';
				$evt[date('j', $e->date_from)][] = array(
					'url'	=> site_url('ctd/'.date('Y', $e->date_from).'/'.date('m', $e->date_from).'/'.$e->slug),
					'title'	=> '<p class="title">'.$hours.$e->title.'</p>'.'<p>'.word_limiter(strip_tags($e->intro), 10).'</p>' //date('j/m', $e->date_from) . '; ' . $e->title
				);
			}

			foreach($evts as $e)
			{
				$title = array();
				$date = date('j', $e->date_from);
				$ctd[$date]['url'] = site_url('ctd/'.date('Y', $e->date_from).'/'.date('m', $e->date_from));
				foreach($evt[$date] as $ed)
				{
					if ($ed['title'])
						$title[] = $ed['title'];
				}
				$ctd[$date]['title'] = implode('', $title);//implode('<br/>', $evt[date('j', $e->date_from)]); //site_url('');
			}

		}
		else
		{
			$ctd = $evt = array();
		}

		$this->load->library('calendar', $prefs);
		$this->data->calendar = $this->calendar->generate($year,$month, $ctd);
		$this->data->show_intro = $intro;
		$this->data->events =& $evts;
		$this->load->view('event-list', $this->data);
//		echo $this->calendar->generate($year,$month, $ctd);

#		$this->load->library('ctd', $prefs);
#		$this->data->ctd = $this->ctd->generate($year,$month, $ctd);
#		$this->data->ctd =& $evts;
#		$this->load->view('event-list', $this->data);
	}


	function date($year=null, $month=null, $day=null)
	{
		if(!$year) $year = date('Y');
		if(!$month) $month = date('m');
		if(!$day) $day = date('d');

		$month_date = new DateTime($year.'-'.$month.'-01');
		$this->data->pagination = create_pagination('ctd/archive/'.$year.'/'.$month, $this->ctd_m->count_by(array('year'=>$year,'month'=>$month)), $this->limit, 5);
		$this->data->ctd = $this->ctd_m->limit($this->data->pagination['limit'])->get_many_by(array('year'=> $year,'month'=> $month, 'day'=>$day));
		$this->data->month_year = $month_date->format("F 'y");

		// Set meta description based on article titles
		$meta = $this->_articles_metadata($this->data->event);
		
		$this->template->title( $this->data->month_year, $this->lang->line('ctd_archive_title'), $this->lang->line('ctd_ctd_title'))		
			->set_metadata('description', $this->data->month_year.'. '.$meta['description'])
			->set_metadata('keywords', $this->data->month_year.', '.$meta['keywords'])
			->set_breadcrumb('Home', 'home')
			->set_breadcrumb($this->lang->line('ctd_articles_title'), 'ctd')
			->set_breadcrumb($this->lang->line('ctd_articles_title').': '.$month_date->format("F 'y"))
			->build('index', $this->data);
	}

	function detail($slug='')
	{
		$this->view($slug);
	}

	// Public: View an article
	function view($slug = '')
	{
		if (!$slug or !$article = $this->pyrocache->model('ctd_m', 'get', array($slug)))
		{
			show_404();
			//echo "Unknown&nbsp;event..";
			//return false;
		}

		if($article->status != 'live' && !$this->ion_auth->is_admin())
		{
			show_404();
			//echo "Unknown&nbsp;event..";
			//return false;
		}

		if (now() < $article->publish_on)
			show_404();
		
		$this->session->set_flashdata(array('referrer'=>$this->uri->uri_string));	

?>
<!--
<pre>
$_COOKIE:
<?php print_r($_COOKIE) ?>
</pre>
-->
<?php

		$cookies = array();
		if (!empty($_COOKIE["ctd"]))
		{
			$cookies = $_COOKIE["ctd"];
		}

		$registered = FALSE;		
		if (is_array($cookies) && !empty($cookies))
		{
			if (in_array($event->id, $cookies["event"]))
			{
				$registered = TRUE;
			}
		}
		
		$this->template->set('registered', $registered);

		// update number of views
		$views = (int)$article->views + 1;
		$this->ctd_m->update($article->id, array('views' => $views));

		// Set meta description based on article titles
		$meta = $this->_articles_metadata($article);

		$article->partners = $this->ctd_partners_m->getuploadedimages($article->id);
		$article->plan_to_come = $this->ctd_m->get_total_attendance($article->id);

		if ($this->input->is_ajax_request())
		{
			$this->template
				->set_layout(FALSE)
				->set("article", $article)
				->build( 'view-ajax' );
		}
		else
		{
			// Build the page
			$this->template->title( lang('ctd_ctd_title'), (isset($meta['title']) ? $meta['title'] : $article->title))
				->append_css('module::colorbox.css')
				->append_js('module::jquery/jquery.livequery.min.js')
				->append_js('module::jquery/jquery.colorbox.min.js')
				->set_metadata('description', $article->meta_description.'. '.$meta['description'] )
				->set_metadata('keywords', $article->meta_keywords ? $article->meta_keywords : '' )
				->set_breadcrumb( lang('ctd_ctd_title'), 'ctd')
				->set_breadcrumb( $article->title)		
				->append_js('module::ctd.js')
				->set("article", $article)
				->build( 'view' );
		}
	}	
	
	function _getGeocodeByLatLng($latlng = "")
	{
		if($latlng == "") return FALSE;
		else
		{
			$url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$latlng&sensor=false";
			$handle = fopen($url, "rb");
			if( ! $handle) return FALSE;
			else
				$result = json_decode(stream_get_contents($handle), TRUE);
			fclose($handle);
			
			//$result = count($result['results'] > 1) ? $result['results'][0]['address_components'] : $result['results']['address_components'];
			$formatted = array();
			
			foreach($result['results'][0]['address_components'] as  $param)
				$formatted[$param['types'][0]] = $param['long_name'];
			
			return $formatted;
		}
	}
	

	function tags($tag=null) {
		if (!$tag) show_404();

		$tagurl = $tag;
		$tag = str_replace('_', ' ', $tag);
		$this->data->tag = $tag;

		// Count total event articles and work out how many pages exist
		$this->data->pagination = create_pagination('ctd/tags/'.$tagurl, $this->ctd_m->count_by(array(
			'tag'=>$tag,
			'status' => 'live'
		)), $this->limit, 4);
		
		// Get the current page of event articles
		$this->data->ctd = $this->ctd_m->limit($this->data->pagination['limit'])->get_many_by(array(
			'tag'=>$tag,
			'status' => 'live'
		));
		
		// Set meta description based on article titles
		$meta = $this->_articles_metadata($this->data->ctd);
		
		// Build the page
		$this->template->title($this->module_details['name'])
			->set_metadata('description', $meta['description'])
			->set_metadata('keywords', $meta['keywords'])
			->set_breadcrumb($this->lang->line('ctd_articles_title'), 'ctd')
			->set_breadcrumb($this->lang->line('ctd_tags_label').': '. deaccented($tag) )
			->build('index', $this->data);
	}

	/* online registration */
	function register($slug=null)
	{
		$this->load->library('form_validation');
		$event = $this->ctd_m->get_by(array('slug' => $slug));

		if (!$event)
		{
			$json = array(
				'status' => 'error',
				'message' => '<div class="error">'.lang('ctd_retrieve_error').'</div>'
			);
			echo json_encode($json);
			return true;
		}

		if ($this->input->post('remind') OR $this->input->post('inform'))
		{
			$email_rule = array(
				'email' => array(
					'field'				=> 'email',
					'label'				=> 'lang:ctd_reg_email_label',
					'rules'	 			=> 'trim|max_length[200]'
				)
			);
			$this->register_rules['email']['rules'] .= '|required|valid_email|callback__check_already_registered['.$event->id.']';
			//array_merge($this->register_rules, $email_rule);

		}

		$this->form_validation->set_rules($this->register_rules);

		// Loop through each validation rule
		foreach($this->register_rules as $rule)
		{
			$form->{$rule['field']} = set_value($rule['field']);
		}
		
		if ($this->form_validation->run())
		{
			$input = array(
				'ctd_id'		=> $event->id,
				'email'			=> $this->input->post('email') ? $this->input->post('email') : '',
				'remind_me'		=> $this->input->post('remind') ? $this->input->post('remind') : 'n',
				'inform_changes'=> $this->input->post('inform') ? $this->input->post('inform') : 'n',
			);
			if (!$this->ctd_m->attending($input))
			{
				$status = 'error';
				$message = lang('ctd_db_attend_error');
			}
			else
			{
				$status = 'success';
				$message = lang('ctd_thanks_for_attending');
				$cookie_expired = mktime(0,0,0,4,cal_days_in_month(CAL_GREGORIAN, 4, date('Y',now())+1),date('Y',now())+1);
				setcookie("ctd[event][]", $event->id, $cookie_expired);
			}
			$json = array(
				'status'  => $status,
				'message' => '<div class="'.$status.'">'.$message.'</div>'
			);
			echo json_encode($json);
			return true;
		}	
		else
		{
			$json = array(
				'status' => 'error',
				'message' => '<div class="error">'.validation_errors().'</div>'
			);
			echo json_encode($json);
			return true;
		}
	}

	function tools($slug)
	{
		switch($slug)
		{
			case 'saverating':
				$this->saverating();
				break;
		}
	}

	/* process rating - user clicks one of the stars */
	public function saverating()
	{
		$id = $this->input->post('id');
		$vote = $this->input->post('vote');
		return $this->ctd_m->save_rating(array('id'=>$id, 'rating'=>$vote));
	}

	// when people unreg using email verification
	function unregmail($eid=0)
	{

		if (!$eid or !$event = $this->ctd_m->get($eid))
		{
			show_404();
		}

		if($event->status != 'live')
		{
			show_404();
		}

		if (now() < $event->publish_on)
			show_404();

		$regrules = array(
			array(
				'field'		=> 'email',
				'label'		=> lang('ctd_reg_email_label'),
				'rules'		=> 'trim|required|valid_email'
			)
		);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="message error">', '</div>');
		$this->form_validation->set_rules($regrules);

		if ($event->reg_open <> 'y') {
			echo "<h1>".$event->title."</h1>\n";
			echo "<p>".lang('ctd_registration_not_open')."</p>\n";
			return false;
		}

		$this->data->uemail = $this->input->post('email', TRUE);
		if ($this->form_validation->run())
		{
			$reg = $this->ctd_m->get_reg($this->data->uemail, $eid);
			if (!empty($reg))
			{
				if ($reg->cancelled == 1)
				{
					$this->data->errmsg = '<div class="error">'.lang('ctd_already_unreg_error').'</div>';
				}
				else
				{
					$sentnotify = $this->ctd_m->sendverify($event, $reg);
					if ($sentnotify)
					{
						$reg->cancel_key = '';
						$this->data->vkey = '';
						$this->data->ptitle = 'Cancel My Registration';
						$this->data->errmsg = lang('event_verify_mail_sent');
						$this->data->event =& $event;
						$this->data->uemail = $reg->email;
						$this->data->msg = lang('event_verify_mail_sent_msg');
						$this->template->set_layout('event-register')->build('regblank', $this->data);
						return true;
					}
					else
					{
						$this->data->errmsg = '<div class="error">'.lang('event_verify_mail_not_sent').'</div>';
					}
				}
			}
			else
			{
				$this->data->errmsg = '<div class="error">'.lang('ctd_failed_unreg').'</div>';
			}
		}
		elseif (isset($_POST))
		{
			$this->data->errmsg = validation_errors();
		}

		$this->data->event =& $event;

		$this->template->set_layout('event-register')
			->append_metadata(css('form.css', 'ctd'))
//			->append_metadata(js('form.js', 'ctd'))
			->build('regcancel-email', $this->data);

	}

	function verifycancel($eid=0, $uid=0, $key='')
	{
		$event = $this->ctd_m->get($eid);
		$this->data->event =& $event;

		if ($eid && $uid && $key) $ret = $this->ctd_m->get_reg_by_id($uid);
		$this->data->uemail = $ret->email;
		$this->data->vkey = $ret->cancel_key;
		$this->data->uid = $uid;

		$regrules = array(
			array(
				'field'		=> 'uemail',
				'label'		=> lang('ctd_reg_email_label'),
				'rules'		=> 'trim|required|valid_email'
			),
			array(
				'field'		=> 'vkey',
				'label'		=> lang('event_key_label'),
				'rules'		=> 'trim|required'
			),
			array(
				'field'		=> 'notes',
				'label'		=> lang('ctd_notes_label'),
				'rules'		=> 'trim'
			)
		);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="message error">', '</div>');
		$this->form_validation->set_rules($regrules);

		if ($this->form_validation->run())
		{
			$vkey 	= $this->input->post('vkey', TRUE);
			$uemail	= $this->input->post('uemail', TRUE);
			$notes	= $this->input->post('notes', TRUE);

			$this->vkey = $vkey;
			$this->uemail = $uemail;
			$this->notes = $notes;

			if ($ret->cancelled <> 1 && $ret->cancel_key == $vkey)
			{
				$input['cancelled'] = 1;
				$input['cancel_notes'] = $notes;
				$input['cancel_key'] = '';
				if ($this->ctd_m->unreg($eid, $input))
				{
					$this->session->set_flashdata('success', lang('ctd_unreg_success'));
					$this->_regnotattending($eid);
					return true;
				}
			}
			else
			{
				$this->data->errmsg = lang('ctd_already_unreg_error');
			}
		}
		else
		{
		}

		foreach ($regrules as $k=>$v) {
			if (isset($_POST[$v['field']])) {
				$this->data->$v['field'] = set_value($v['field']);
			}
		}

		// display unreg form
		$this->template->title($this->module_details['name'])
			->set_breadcrumb($this->lang->line('event_reg_title'), 'ctd')
			->set_breadcrumb($this->lang->line('ctd_cancel_attendance_label') )
			->build('regcancel-verify', $this->data);
	}

	function _check_already_registered($email='', $id=0)
	{
		$reg = $this->ctd_m->check_email_attendant(array('email'=>$email, 'ctd_id'=>$id));
		if (!empty($reg))
		{
			$this->form_validation->set_message('_check_already_registered', lang('ctd_already_registered_error'));
			return FALSE;
		}

		return TRUE;
	}

	private function _regcancel($eid)
	{
		$this->data->event = $this->ctd_m->get($eid);

		$field['email'] = $this->data->uemail;
		$field['vkey'] = $this->data->vkey;
		$field['notes'] = $this->data->notes;
	}

	private function _regmaybe($eid)
	{
		$article = $this->ctd_m->get($eid);
		// update number of maybes
		$maybes = (int)$article->maybes + 1;
		$this->ctd_m->update($eid, array('maybes' => $maybes));
		$this->session->set_flashdata('success', lang('ctd_thanks_maybe'));
		redirect('ctd/'.$article->slug);
	}

	private function _regnotattending($eid)
	{
		$article = $this->ctd_m->get($eid);
		// update number of not_attending
		$not = (int)$article->maybes + 1;
		$this->ctd_m->update($eid, array('not_attending' => $not));
		$this->session->set_flashdata('notice', lang('ctd_thanks_notattending'));
		redirect('ctd/'.$article->slug);
	}


	// Private methods not used for display
	private function _articles_metadata(&$article = array())
	{
		$keywords = array();
		$description = array();

		// Loop through articles and use titles for meta description
		if(!empty($articles))
		{
			foreach($articles as &$article)
			{
				$description[] = $article->title; 
			}
		}
		
		return array(
			'keywords' => implode(', ', $keywords),
			'description' => implode(', ', $description)
		);
	}

	private function _check_dir($dir)
	{
		// check directory
		$fileOK = array();
		$fdir = explode('/', $dir);
		$ddir = '';
		for($i=0; $i<count($fdir); $i++)
		{
			$ddir .= $fdir[$i] . '/';
			if (!is_dir($ddir))
			{
				if (!@mkdir($ddir, 0777)) {
					$fileOK[] = 'not_ok';
				}
				else
				{
					$fileOK[] = 'ok';
				}
			}
			else
			{
				$fileOK[] = 'ok';
			}
		}
		return $fileOK;

	}

	/**
	 * Phisically delete a file
	 *
	 * @var string $filename File name to delete
	 * @var int $gallery_id gallery ID the file is attached to
	 * @access public
	 */
	private function _unlinkfile($filename, $gallery_id)
	{
			$fullfile = substr($filename, 0, -4) . '_full' . substr($filename, -4);
			$thumbfile = substr($filename, 0, -4) . '_thumb' . substr($filename, -4);
			$smallfile = substr($filename, 0, -4) . '_small' . substr($filename, -4);
			$medfile = substr($filename, 0, -4) . '_med' . substr($filename, -4);

			$stat = array();
					// delete the files
					if (file_exists($this->_path . $gallery_id .'/' . $fullfile))
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $fullfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $fullfile);
						}
					}

					if (file_exists($this->_path . $gallery_id .'/' . $thumbfile))
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $thumbfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $thumbfile);
						}
					}
	
					if (file_exists($this->_path . $gallery_id .'/' . $smallfile))
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $smallfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $smallfile);
						}
					}
	
					if (file_exists($this->_path . $gallery_id .'/' . $medfile))
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $medfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $medfile);
						}
					}
	
					if (file_exists($this->_path . $gallery_id .'/' . $filename))
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $filename))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $filename);
						}
					}

					if (in_array('error', $stat))
					{
						$status = 'error';
						$message = implode('<br />', $msg);
					}
					else
					{
						$status = 'success';
						$message = sprintf(lang('galleries.delete_img_success'), $filename);
					}

		return (array($status, $message));
	}

	function _check_file($file='')
	{
		if ( isset($_FILES['imgfile']) && !empty($_FILES['imgfile']['tmp_name']) )
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('_check_file', 'Please upload an image');
			return false;
		}
	}


}
?>