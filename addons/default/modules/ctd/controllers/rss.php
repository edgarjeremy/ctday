<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rss extends Public_Controller
{
	function __construct()
	{
		parent::__construct();	
		$this->load->model('events_m');
		$this->load->helper('xml');
		$this->load->helper('date');
		$this->lang->load('events');
	}
	
	function index()
	{
		$posts = $this->cache->model('events_m', 'get_many_by', array(array(
			'status' => 'live',
			'limit' => $this->settings->item('rss_feed_items'))
		), $this->settings->item('rss_cache'));
		
		$this->_build_feed( $posts );		
		$this->data->rss->feed_name .= $this->lang->line('events_rss_name_suffix');		
		$this->output->set_header('Content-Type: application/rss+xml');
		$this->load->view('rss', $this->data);
	}
	
	function category( $slug = '')
	{ 
		$this->load->model('events_categories_m');
		
		if(!$category = $this->events_categories_m->get_by('slug', $slug))
		{
			redirect('events/rss/index');
		}
		
		$posts = $this->cache->model('events_m', 'get_many_by', array(array(
			'status' => 'live',
			'category' => $slug,
			'limit' => $this->settings->item('rss_feed_items') )
		), $this->settings->item('rss_cache'));
		
		$this->_build_feed( $posts );		
		$this->data->rss->feed_name .= $this->lang->line('events_rss_category_suffix').' - '. $category->title ;
		$this->output->set_header('Content-Type: application/rss+xml');
		$this->load->view('rss', $this->data);
	}
	
	function _build_feed( $posts = array() )
	{
		$this->data->rss->encoding = $this->config->item('charset');
		$this->data->rss->feed_name = $this->settings->item('site_name');
		$this->data->rss->feed_url = base_url();
		$this->data->rss->page_description = sprintf($this->lang->line('events_rss_articles_title'), $this->settings->item('site_name'). ' - ' . $this->settings->item('site_slogan '));
		$this->data->rss->page_language = CURRENT_LANGUAGE; //'en-gb';
		$this->data->rss->creator_email = $this->settings->item('contact_email');
		
		if(!empty($posts))
		{        
			foreach($posts as $row)
			{
				//$row->created_on = human_to_unix($row->created_on);
				$row->link = site_url('events/'. $row->slug);
				$row->date_from = standard_date('DATE_RSS', $row->date_from);
				
				$item = array(
					//'author' => $row->author,
					'title' => xml_convert($row->title),
					'link' => $row->link,
					'guid' => $row->link,
					'description'  => $row->intro,
					'date' => $row->date_from
				);				
				$this->data->rss->items[] = (object) $item;
			} 
		}	
	}
}
?>
