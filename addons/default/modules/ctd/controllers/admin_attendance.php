<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 */
class Admin_attendance extends Admin_Controller
{
	/**
	 * The current active section
	 * @access protected
	 * @var string
	 */
	protected $section = 'attendance';

	/**
	 * Array that contains the validation rules
	 * @access protected
	 * @var array
	 */
	protected $validation_rules;

	public $id;

	/** 
	 * The constructor
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ctd_m');
		$this->load->model('ctd_attendance_m');
		$this->lang->load('ctd');
		
	    $this->template->set_partial('shortcuts', 'admin/partials/shortcuts');

		$this->module_details['slug'] = 'ctd/attendance';

		// Set the validation rules
		$this->validation_rules = array(
			array(
				'field' => 'title',
				'label' => lang('cat_title_label'),
				'rules' => 'trim|required|max_length[250]|callback__check_title'
			),
		);
		
		// Load the validation library along with the rules
		//$this->load->library('form_validation');
		//$this->form_validation->set_rules($this->validation_rules);
	}
	
	/**
	 * Index method, lists all categories
	 * @access public
	 * @return void
	 */
	public function index($slug=NULL)
	{
		if (!$slug) redirect('admin/ctd');
		$event = $this->ctd_m->get(is_numeric($slug) ? $slug : $this->ctd_m->get_id_from_slug($slug));
		$eid = $event->id;
		// Create pagination links
		$total_rows = $this->ctd_attendance_m->count_by(array('ctd_id'=>$eid));
		$pagination = create_pagination('admin/ctd/attendance/index', $total_rows);	
			
		// Using this data, get the relevant results
		$atts = $this->ctd_attendance_m->order_by('email')->limit($pagination['limit'], $pagination['offset'])->get_many_by(array('ctd_id'=>$eid));

		$this->template
			->title($this->module_details['name'], lang('att_list_title'))
			->set('event', $event)
			->set('atts', $atts)
			->set('pagination', $pagination)
			->build('admin/attendance/index');
	}
	
	/**
	 * Create method, creates a new category
	 * @access public
	 * @return void
	 */
	public function create()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validation_rules);

		// Validate the data
		if ($this->form_validation->run())
		{
			$this->ctd_attendance_m->insert($_POST)
				? $this->session->set_flashdata('success', sprintf( lang('cat_add_success'), $this->input->post('title')) )
				: $this->session->set_flashdata(array('error'=> lang('cat_add_error')));

			redirect('admin/ctd/categories');
		}
		
		// Loop through each validation rule
		foreach($this->validation_rules as $rule)
		{
			$category->{$rule['field']} = set_value($rule['field']);
		}
		
		// Render the view	
		$this->template
			->title($this->module_details['name'], lang('cat_create_title'))
			->set("category", $category)
			->build('admin/categories/form');	
	}
	
	/**
	 * Edit method, edits an existing category
	 * @access public
	 * @param int id The ID of the category to edit 
	 * @return void
	 */
	public function edit($id = 0)
	{	
		// Get the category
		$category = $this->ctd_attendance_m->get($id);
		
		// ID specified?
		$category or redirect('admin/ctd/categories/index');

		$this->id = $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules(array_merge($this->validation_rules, array(
			array(
				'field' => 'title',
				'label' => lang('categories.title_label'),
				'rules' => 'trim|required|max_length[250]|callback__check_title['.$id.']'
			),
		)));

		// Validate the results
		if ($this->form_validation->run())
		{		
			$this->ctd_attendance_m->update($id, $_POST)
				? $this->session->set_flashdata('success', sprintf( lang('cat_edit_success'), $this->input->post('title')) )
				: $this->session->set_flashdata(array('error'=> lang('cat_edit_error')));
			
			redirect('admin/ctd/categories/index');
		}
		
		// Loop through each rule
		foreach($this->validation_rules as $rule)
		{
			if($this->input->post($rule['field']))
			{
				$category->{$rule['field']} = $this->input->post($rule['field']);
			}
		}

		// Render the view
		$this->template
			->title($this->module_details['name'], sprintf(lang('cat_edit_title'), $category->title))
			->set("category", $category)
			->build('admin/categories/form');
	}	

	/**
	 * Delete method, deletes an existing category (obvious isn't it?)
	 * @access public
	 * @param int id The ID of the category to edit 
	 * @return void
	 */
	public function delete($id = 0)
	{	
		$id_array = (!empty($id)) ? array($id) : $this->input->post('action_to');
		
		// Delete multiple
		if(!empty($id_array))
		{
			$deleted = 0;
			$to_delete = 0;
			foreach ($id_array as $id) 
			{
				if($this->ctd_attendance_m->delete($id))
				{
					$deleted++;
				}
				else
				{
					$this->session->set_flashdata('error', sprintf($this->lang->line('cat_mass_delete_error'), $id));
				}
				$to_delete++;
			}
			
			if( $deleted > 0 )
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('cat_mass_delete_success'), $deleted, $to_delete));
			}
		}		
		else
		{
			$this->session->set_flashdata('error', $this->lang->line('cat_no_select_error'));
		}
		
		redirect('admin/ctd/categories/index');
	}
		
	/**
	 * Callback method that checks the title of the category
	 * @access public
	 * @param string title The title to check
	 * @return bool
	 */
	public function _check_title($title = '', $id = NULL)
	{
		if ($this->ctd_attendance_m->check_title($title, $id))
		{
			$this->form_validation->set_message('_check_title', sprintf($this->lang->line('cat_already_exist_error'), $title));
			return FALSE;
		}

		return TRUE;
	}
	
	/**
	 * Create method, creates a new category via ajax
	 * @access public
	 * @return void
	 */
	public function create_ajax()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validation_rules);

		// Loop through each validation rule
		foreach($this->validation_rules as $rule)
		{
			$category->{$rule['field']} = set_value($rule['field']);
		}
		
		$this->method = 'create';	
		//$this->data->category = $category;
		
		if ($this->form_validation->run())
		{
			$id = $this->ctd_attendance_m->insert_ajax($_POST);
			
			if($id > 0)
			{
				$message = sprintf( lang('cat_add_success'), $this->input->post('title'));
			}
			else
			{
				$message = lang('cat_add_error');
			}
			
			$json = array('message' => $message,
					'title' => $this->input->post('title'),
					'category_id' => $id,
					'status' => 'ok'
					);
			echo json_encode($json);
		}	
		else
		{		
			// Render the view
			$errors = validation_errors();
			$form = $this->load->view('admin/categories/form', array("category" => $category), TRUE);
			if(empty($errors))
			{
				
				echo $form;
			}
			else
			{
				$data = array();
				//$data['messages']['error'] = $errors;
				$message = $this->load->view('admin/partials/notices', $data, TRUE);
				//$message = $errors;

				$json = array('message' => $message,
					      'status' => 'error',
					      'form' => $form
					     );
				echo json_encode($json);
			}
		}
	}
}