<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ical extends Public_Controller
{
	var $timezones = array();
	var $week = 0;
	var $days3 = 0;
	var $days1 = 0;

	function __construct()
	{
		parent::Public_Controller();	
		$this->load->model('events_m');
		$this->load->model('timezone/timezone_m');
		$this->load->library('ical/icalcreator');
		$this->load->helper('date');
		$this->lang->load('events');

		$this->data->timezones = array();
		$this->data->timezones[0] = '-- timezone --';
		if($timezones = $this->timezone_m->get_all())
		{
			foreach($timezones as $tz)
			{
				$this->timezones[$tz->id] = $tz->name;
			}
		}

		$this->week = 60*60*24*7;
		$this->days3 = 60*60*24*3;
		$this->days1 = 60*60*24;

//echo 'Default timezone: '.$this->timezones[$this->settings->default_timezone]."<br />\n";
	}
	
	function index()
	{
		$posts = $this->cache->model('events_m', 'get_many_by', array(array(
			'status' => 'live',
			'limit' => $this->settings->item('rss_feed_items'))
		), $this->settings->item('rss_cache'));

		$v = new icalcreator( array( 'unique_id' => $_SERVER['HTTP_HOST'] ));
		
		$v->setProperty( 'X-WR-CALNAME'
		               , $this->settings->item('site_name') );          // set some X-properties, name, content.. .
		$v->setProperty( 'X-WR-CALDESC'
		               , lang('events_event_title') );
		$v->setProperty( 'X-WR-TIMEZONE'
		               , $this->timezones[$this->settings->default_timezone] );
		
		foreach($posts as $article)
		{
			$e = & $v->newComponent( 'vevent' );           // initiate a new EVENT
			$e->setProperty( 'categories'
			               , $article->category_slug );                   // categorize
			$e->setProperty( 'dtstart'
							,date('Y', $article->date_from), date('m', $article->date_from), date('d', $article->date_from), date('G', $article->date_from), date('i', $article->date_from), 00 );
		//	               , 2007, 12, 24, 19, 30, 00 );   // 24 dec 2007 19.30
			$e->setProperty( 'dtend'
							,date('Y', $article->date_to), date('m', $article->date_to), date('d', $article->date_to), date('G', $article->date_to), date('i', $article->date_to), 00 );
		
			$e->setProperty( 'summary'
			               , $article->title );    // describe the event
		
			$e->setProperty( 'description'
			               , ( $article->intro ? strip_tags($article->intro) : stripslashes(word_limiter(strip_tags($article->body, 150))) ) );    // describe the event
			$e->setProperty( 'location'
			               , $article->geolocation );                     // locate the event
		
			if ($article->event_reminder)
			{
				$etime = $article->event_reminder_time;
				if ( $etime <= $this->week && $etime > $this->days3 )
					$trigger = array('week'=>1);
				elseif ( $etime <= $this->days3 && $etime > $this->days1 )
					$trigger = array('day'=>3);
				elseif ( $etime <= $this->days1 )
					$trigger = array('day'=>1);
		
				$a = & $e->newComponent( 'valarm' );           // initiate ALARM
				$a->setProperty( 'action'
				               , 'DISPLAY' );                  // set what to do
				$a->setProperty( 'description'
				               , $article->title );          // describe alarm
				$a->setProperty( 'trigger'
				               , $trigger);        // set trigger one week before
			}
		}
		
		/* alt. production */
		$v->returnCalendar();                          // generate and redirect output to user browser
		/* alt. dev. and test */
#		$str = $v->createCalendar();                   // generate and get output in string, for testing?
#		echo nl2br($str);

	}
	
	function category( $slug = '')
	{ 
		$this->load->model('events_categories_m');
		
		if(!$category = $this->events_categories_m->get_by('slug', $slug))
		{
			redirect('agenda/rss/index');
		}
		
		$posts = $this->cache->model('events_m', 'get_many_by', array(array(
			'status' => 'live',
			'category' => $slug,
			'limit' => $this->settings->item('rss_feed_items') )
		), $this->settings->item('rss_cache'));
		
		$v = new icalcreator( array( 'unique_id' => $_SERVER['HTTP_HOST'] ));
		
		$v->setProperty( 'X-WR-CALNAME'
		               , $this->settings->item('site_name') );          // set some X-properties, name, content.. .
		$v->setProperty( 'X-WR-CALDESC'
		               , lang('events_event_title') );
		$v->setProperty( 'X-WR-TIMEZONE'
		               , $this->timezones[$this->settings->default_timezone] );
		
		foreach($posts as $article)
		{
			$e = & $v->newComponent( 'vevent' );           // initiate a new EVENT
			$e->setProperty( 'categories'
			               , $article->category_slug );                   // catagorize
			$e->setProperty( 'dtstart'
							,date('Y', $article->date_from), date('m', $article->date_from), date('d', $article->date_from), date('G', $article->date_from), date('i', $article->date_from), 00 );
		//	               , 2007, 12, 24, 19, 30, 00 );   // 24 dec 2007 19.30
			$e->setProperty( 'dtend'
							,date('Y', $article->date_to), date('m', $article->date_to), date('d', $article->date_to), date('G', $article->date_to), date('i', $article->date_to), 00 );
		
			$e->setProperty( 'summary'
			               , $article->title );    // describe the event
		
			$e->setProperty( 'description'
			               , ( $article->intro ? strip_tags($article->intro) : stripslashes(word_limiter(strip_tags($article->body, 150))) ) );    // describe the event
			$e->setProperty( 'location'
			               , $article->geolocation );                     // locate the event
		
			if ($article->event_reminder)
			{
				$etime = $article->event_reminder_time;
				if ( $etime <= $this->week && $etime > $this->days3 )
					$trigger = array('week'=>1);
				elseif ( $etime <= $this->days3 && $etime > $this->days1 )
					$trigger = array('day'=>3);
				elseif ( $etime <= $this->days1 )
					$trigger = array('day'=>1);
		
				$a = & $e->newComponent( 'valarm' );           // initiate ALARM
				$a->setProperty( 'action'
				               , 'DISPLAY' );                  // set what to do
				$a->setProperty( 'description'
				               , $article->title );          // describe alarm
				$a->setProperty( 'trigger'
				               , $trigger);        // set trigger one week before
			}
		}
		
		/* alt. production */
		$v->returnCalendar();                          // generate and redirect output to user browser
		/* alt. dev. and test */
#		$str = $v->createCalendar();                   // generate and get output in string, for testing?
#		echo nl2br($str);
	}
	
	function _build_feed( $posts = array() )
	{
		$this->data->rss->encoding = $this->config->item('charset');
		$this->data->rss->feed_name = $this->settings->item('site_name');
		$this->data->rss->feed_url = base_url();
		$this->data->rss->page_description = sprintf($this->lang->line('events_rss_articles_title'), $this->settings->item('site_name'). ' - ' . $this->settings->item('site_slogan '));
		$this->data->rss->page_language = 'en-gb';
		$this->data->rss->creator_email = $this->settings->item('contact_email');
		
		if(!empty($posts))
		{        
			foreach($posts as $row)
			{
				//$row->created_on = human_to_unix($row->created_on);
				$row->link = site_url('events/'. $row->slug);
				$row->date_from = standard_date('DATE_RSS', $row->date_from);
				
				$item = array(
					//'author' => $row->author,
					'title' => xml_convert($row->title),
					'link' => $row->link,
					'guid' => $row->link,
					'description'  => $row->intro,
					'date' => $row->date_from
				);				
				$this->data->rss->items[] = (object) $item;
			} 
		}	
	}
}
?>