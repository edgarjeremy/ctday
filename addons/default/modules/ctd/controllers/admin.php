<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller
{
	/**
	 * The current active section
	 * @access protected
	 * @var string
	 */
	protected $section = 'ctd';

	// Validation rules to be used for create and edit
	public $validation_rules = array();

	private $_folders	= array();
	private $_path 		= '';
	private $_type 		= NULL;
	private $_ext 		= NULL;
	private $_filename	= NULL;

	protected $thumb_width = 150;
	protected $thumb_height = 150;
	protected $max_width = 300;
	protected $max_height = 400;

	protected $upload_cfg = array(
		'allowed_types'		=> 'jpg|gif|png|jpeg',
		'max_size'			=> '10000',
		'remove_spaces'		=> TRUE,
		'overwrite'			=> FALSE,
		'encrypt_name'		=> FALSE,
	);

	// event type
	public $event_types;

	private $_file_rules = array(

		array(
			'field' => 'name',
			'label' => 'lang:files:name_label',
			'rules' => 'trim|max_length[250]'
		),
		array(
			'field' => 'filetitle',
			'label' => 'lang:content_title_label',
			'rules' => 'trim|max_length[100]'
		),
		array(
			'field' => 'filedescription',
			'label' => 'lang:content_description_label',
			'rules' => 'trim'
		),
	);

	protected $current_year;

	function __construct()
	{
		parent::__construct();

		$this->load->model(
			array(
				'ctd_m',
				'ctd_categories_m',
				'ctd_partners_m',
			)
		);

		$this->load->helper('filename');

		$this->load->helper('date');
		$this->lang->load(array('ctd', 'categories', 'files/files'));
		$this->config->load('files');

		$this->validation_rules = array(
			array(
				'field'				=> 'title',
				'label'				=> 'lang:ctd_title_label',
				'rules'	 			=> 'trim|max_length[200]'
			),
			array(
				'field'				=> 'slug',
				'label'				=> 'lang:ctd_slug_label',
				'rules'	 			=> 'trim|alpha_dot_dash|max_length[100]|callback__check_slug'
			),
			array(
				'field' 			=> 'intro',
				'label'				=> 'lang:ctd_intro_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'body',
				'label'				=> 'lang:ctd_body_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'status',
				'label'				=> 'lang:ctd_status_label',
				'rules' 			=> 'trim|required'
			),
			array(
				'field' 			=> 'date_from_str',
				'label'				=> lang('ctd_from_label') . ' ' . lang('ctd_date_label'),
				'rules' 			=> 'trim|required'
			),
			array(
				'field'				=> 'event_venue',
				'label'				=> 'lang:ctd_event_venue',
				'rules'	 			=> 'trim|required|max_length[100]'
			),
			array(
				'field'				=> 'venue_address',
				'label'				=> 'lang:ctd_event_venue_address',
				'rules'	 			=> 'trim|max_length[100]'
			),
			array(
				'field'				=> 'country',
				'label'				=> 'lang:ctd_country_label',
				'rules'	 			=> 'trim|required'
			),
			array(
				'field'				=> 'location',
				'label'				=> 'lang:ctd_location_label',
				'rules'	 			=> 'trim|max_length[250]'
			),
			array(
				'field'				=> 'geolocation',
				'label'				=> 'lang:ctd_geolocation_label',
				'rules'	 			=> 'trim'
			),
			array(
				'field' 			=> 'hide_map',
				'label'				=> 'lang:ctd_do_not_display_map',
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'allow_comments',
				'label'				=> 'Allow comments',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'views',
				'label'				=> 'lang:ctd_views_label',
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'category_id',
				'label'				=> 'lang:ctd_category_label',
				'rules' 			=> 'trim|alpha_numeric'
			),
			array(
				'field' 			=> 'date_to_str',
				'label'				=> lang('ctd_to_label') . ' ' . lang('ctd_date_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'date_publish_str',
				'label'				=> 'lang:ctd_publish_date_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'partner_logos[]',
				'label'				=> 'Partner Logos',
				'rules'				=> 'trim'
			),
			array(
				'field' 			=> 'meta_title',
				'label'				=> 'lang:ctd_meta_title_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'meta_description',
				'label'				=> 'lang:ctd_meta_desc_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'meta_keywords',
				'label'				=> 'lang:ctd_meta_keywords_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'date_to_hour',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_to_label') . ' ' .lang('ctd_hour_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'date_to_minute',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_to_label') . ' ' .lang('ctd_minute_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'date_from_hour',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_from_label') . ' ' .lang('ctd_hour_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'date_from_minute',
				'label'				=> lang('ctd_date_label') . ' ' . lang('ctd_from_label') . ' ' .lang('ctd_minute_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'publish_on_hour',
				'label'				=> lang('ctd_hour_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'publish_on_minute',
				'label'				=> lang('ctd_minute_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'slideshow',
				'label'				=> lang('ctd_slideshow_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'notes',
				'label'				=> 'Contacts',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'sponsors',
				'label'				=> 'Sponsors',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'curslide',
				'label'				=> lang('ctd_slideshow_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'oldslide',
				'label'				=> lang('ctd_slideshow_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'geocoordinates',
				'label'				=> 'Geocoordinates',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'website',
				'label'				=> lang('ctd_website_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field'				=> 'submitted_by_name',
				'label'				=> 'Submitter Name',
				'rules'	 			=> 'trim|max_length[100]'
			),
			array(
				'field'				=> 'submitted_by_email',
				'label'				=> 'Submitter Email Address',
				'rules'	 			=> 'trim|valid_email|max_length[100]'
			),
			array(
				'field' 			=> 'contact_email',
				'label'				=> lang('ctd_contact_email_label'),
				'rules' 			=> 'trim|valid_email|max_length[100]'
			),
			array(
				'field' 			=> 'event_year',
				'label'				=> 'event_year',
				'rules' 			=> 'trim'
			),
		);

		$this->hours = array_combine($hours = range(0, 23), $hours);
		$this->minutes = array_combine($minutes = range(0, 59), $minutes);

		$this->categories = array();
		if($categories = $this->ctd_categories_m->get_all())
		{
			foreach($categories as $category)
			{
				$this->categories[$category->id] = $category->title;
			}
		}

		$this->ctd_status = array(
			'draft'=>lang('ctd_draft_label'),
			'live'=>lang('ctd_live_label')
		);

		$this->upload_cfg['upload_path'] = UPLOAD_PATH . 'ctd';
		$this->check_dir($this->upload_cfg['upload_path']);

		$this->_path = FCPATH . $this->config->item('files_folder');
		$this->check_dir($this->_path);

		$countries = array(
			'' => 'Select',
			'FJ' => 'Fiji',
			'ID' => 'Indonesia',
			'MY' => 'Malaysia',
			'PG' => 'Papua New Guinea',
			'PH' => 'Philippines',
			'SB' => 'Solomon Islands',
			'TL' => 'Timor Leste',
		);
		
		
		$f_year_backend = $this->ctd_m->get_year();
		$current_year = 0;
		foreach($f_year_backend as $y)
		{
			$this->current_year = ($current_year < $y->event_year) ? $y->event_year : $current_year;
			$filter_year[$y->event_year] = $y->event_year;
		}
		
		$this->template
			->set('countries', $countries)
			->title('Events')
			->set('filter_year', $filter_year);
//		$this->template->set_partial('shortcuts', 'admin/partials/shortcuts');
	}
	
	// Admin: List ctd
	function index()
	{
		//set the base/default where clause
		//$f_datenow = $this->current_year; //'2015';date('Y');
		//$base_where = array('event_year' => $f_datenow);
		//$base_where['year'] = $f_datenow;

		//add post values to base_where if f_module is posted
		if ($this->input->post('f_year'))
		{
			$base_where['year'] = $this->input->post('f_year');
		}else{
			//$base_where['year'] = $f_datenow;
			$base_where['year'] = date('Y');
		}
		if ($this->input->post('f_country'))
		{
			$base_where['country'] = $this->input->post('f_country');
		}
		//$base_where['year'] = $this->input->post('f_year') ? $this->input->post('f_year') : NULL;

		// Create pagination links
		$total_rows = $this->ctd_m->count_by($base_where);
		$pagination = create_pagination('admin/ctd/index', $total_rows);

		// Using this data, get the relevant results
		$events = $this->ctd_m->limit($pagination['limit'], $pagination['offset'])->get_many_by($base_where);
		
		//$this->output->enable_profiler(TRUE);
		
		foreach($events as $e) {
			$e->plan_to_come = $this->ctd_m->get_total_attendance($e->id);
		}
		
		//do we need to unset the layout because the request is ajax?
		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);
		
		$this->template
			->set('hours', $this->hours)
			->set('minutes', $this->minutes)
			->set('categories', $this->categories)
			->set('ctd_status', $this->ctd_status)
			->set('ctd', $events)
			->append_js('admin/filter.js')
			->set_partial('filters', 'admin/partials/filters')
			->set('pagination', $pagination);
		
		$this->input->is_ajax_request()
			? $this->template->build('admin/tables/posts')
			: $this->template->build('admin/index');
	}
	
	// Admin: Create a new event
	function create()
	{
		//$this->output->enable_profiler(TRUE);

		$this->load->library('form_validation');

		$this->form_validation->set_rules($this->validation_rules);
		$input = $this->input->post();

		$article = (object) array();
		
		if ($this->input->post('date_publish_str'))
		{
			$df = explode('-', $this->input->post('date_publish_str'));
			$date_publish_day = $df[2];
			$date_publish_month = $df[1];
			$date_publish_year = $df[0];
			$date_publish = mktime((int)$input['publish_on_hour'], (int)$input['publish_on_minute'], 0, $date_publish_month, $date_publish_day, $date_publish_year);
			unset($df);
		} else {
			$date_publish = NULL;
		}

		if ($this->input->post('date_from_str'))
		{
			$df = explode('-', $this->input->post('date_from_str'));
			$date_from_day = $df[2];
			$date_from_month = $df[1];
			$date_from_year = $df[0];
			$date_from = mktime((int)$this->input->post('date_from_hour'), (int)$this->input->post('date_from_minute'), 0, $date_from_month, $date_from_day, $date_from_year);
			unset($df);
		} else {
			$date_from = 0;
			$date_to = 0;
		}

		if ($this->input->post('date_to_str'))
		{
			$dt = explode('-', $this->input->post('date_to_str'));
			$date_to_day = $dt[2];
			$date_to_month = $dt[1];
			$date_to_year = $dt[0];
			$date_to = mktime($this->input->post('date_to_hour'), $this->input->post('date_to_minute'), 0, $date_to_month, $date_to_day, $date_to_year);
			unset($dt);
		} else {
			$date_to = 0;
		}

		// read partners
		$files = $this->input->post('file_id');
		$partners = '';
		if (!empty($partners))
		{
			$partners = $this->load->view('admin/files/contents', array('files'=>$files), TRUE);
		}

			// uploaded image slideshow ends
			if ($this->input->post('curslide'))
			{
				$filename = $this->input->post('curslide');
			}

			// uploaded image slideshow starts
			if (!empty($_FILES['slideshow']))
			{
				// Setup upload config
				$config = $this->upload_cfg;
				$config['upload_path'] = UPLOAD_PATH . 'ctd/slides';
				$this->load->library('upload', $config);

				// check directory exists
				$this->check_dir($config['upload_path']);

				if ( ! $this->upload->do_upload('slideshow'))
				{
				}
				else
				{
					$file = $this->upload->data('');
					$data = array(
						'filename'		=> $file['file_name'],
						'uploaded_by'	=> $this->current_user->id,
						'uploaded_on'	=> now(),
						'position'		=> 999
					);

					if ($file['is_image'])
					{
						$this->load->library('image_lib');
						$filename = $file['file_name'];
						$thumbfile = thumbnail($filename);
						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->thumb_width ? $file['image_width'] : $this->thumb_width);
						$image_cfg['height'] = ($file['image_height'] < $this->thumb_height ? $file['image_height'] : $this->thumb_height);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['master_dim'] = 'height';
						$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();
	
						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->settings->slideshow_width ? $file['image_width'] : $this->settings->slideshow_width);
						$image_cfg['height'] = ($file['image_height'] < $this->settings->slideshow_height ? $file['image_height'] : $this->settings->slideshow_height);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['master_dim'] = 'width';
						//$image_cfg['new_image'] = $file['file_path'] . $medfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

						if ($this->input->post('oldimage'))
						{
							$this->unlinkslide($this->input->post('oldslide'));
						}

						if ($this->input->post('curslide'))
						{
							$this->unlinkslide($this->input->post('curslide'));
						}
					}

				}
			}

		if ($this->form_validation->run())
		{
			$input = array(
	            'title'				=> $this->input->post('title'),
	            'intro'				=> $this->input->post('intro'),
	            'body'				=> $this->input->post('body'),
				'notes'	 			=> $this->input->post('notes'),
				//'sponsors'	 		=> $this->input->post('sponsors'),

				'slug' 				=> $this->input->post('slug'),
	            'category_id'		=> $this->input->post('category_id'),
	            'status'			=> $this->input->post('status'),

				'country' 			=> $this->input->post('country'),
				'location' 			=> $this->input->post('location'),
				'geolocation' 		=> $this->input->post('geolocation'),
				'geocoordinates'	=> $this->input->post('geocoordinates'),
				'hide_map'			=> (int)$this->input->post('hide_map'),

				'meta_title'	 	=> $this->input->post('meta_title'),
				'meta_description' 	=> $this->input->post('meta_description'),
				'meta_keywords' 	=> $this->input->post('meta_keywords'),

				'publish_on' 		=> $date_publish,
				'date_from' 		=> $date_from,
				'date_to' 			=> $date_to,
				
				'event_year' 		=> $date_from_year,
				
				'event_venue' 		=> $this->input->post('event_venue'),
				'venue_address' 	=> $this->input->post('venue_address'),
	    	);

			if (!empty($filename))
			{
				$input['slide'] = $filename;
			}

			$id = $this->ctd_m->insert($input);
    	
			if (!empty($id))
			{
				$this->pyrocache->delete_all('ctd_m');
				$this->session->set_flashdata('success', sprintf($this->lang->line('ctd_article_add_success'), $this->input->post('title')));

				// read and update partner logo's id
				$file_ids = $this->input->post('file_id');
				if (!empty($file_ids))
				{
					$input = array();
					$pos = 1;
					foreach($this->input->post('file_id') as $filedata)
					{
						$input['ctd_id'] = $id;
						$input['position'] = $pos;
						$this->ctd_partners_m->updatefile($filedata, $input);
						$pos++;
					}
				}
			}
			
			else
			{
				$this->session->set_flashdata('error', $this->lang->line('ctd_article_add_error'));
			}			

			// Redirect back to the form or main page
			$this->input->post('btnAction') == 'save_exit' ? redirect('admin/ctd') : redirect('admin/ctd/edit/'.$id);
		}
//		else
//		{
			// Go through all the known fields and get the post values
			foreach($this->validation_rules as $key => $field)
			{
				$article->$field['field'] = set_value($field['field']);
				if ($field['field'] == 'curslide' && $this->input->post('curslide'))
				{
					$article->slide = $this->input->post('curslide');
					$article->curslide = $this->input->post('curslide');
				}
			}

			if (!empty($filename))
			{
				$article->slide = $filename;
				$article->curslide = $filename;
			}

			if ($this->input->post('article_id')) $article->id = $this->input->post('article_id');
			//$article->created_on = now();

//		}
		
		$article->date_from = $date_from;
		$article->date_to = $date_to;
		//$this->data->article =& $article;
		
		//Load google maps API
#		$this->template->append_metadata('<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>');

		$this->template
			->title($this->module_details['name'], lang('ctd_create_title'))
			->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			->append_css('module::files.css' )
			->append_css('module::jquery.fileupload-ui.css')
			->append_js('module::jquery.cooki.js')
			->append_js('module::jquery.fileupload.js')
			->append_js('module::jquery.fileupload-ui.js')
			->append_js('module::jquery.ba-hashchange.min.js')
			->append_metadata('<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>')
			->append_js('module::functions.js' )
			->append_js('module::ctd_form.js' )
			->set('partners', $partners)
			->set('article', $article)
			->set('hours', $this->hours)
			->set('minutes', $this->minutes)
			->set('categories', $this->categories)
			->set('ctd_status', $this->ctd_status)
			->build('admin/form');
			
	}
	
	// Admin: Edit an article
	function edit($id = 0)
	{
		if (!$id)
		{
			redirect('admin/ctd');
		}

	    $this->id	 	= $id;
    	$article = $this->ctd_m->get($id);

		if (empty($article)) {
			$this->session->set_flashdata('error', $this->lang->line('ctd_retrieve_error'));
			redirect('admin/ctd');
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules(array_merge($this->validation_rules, array(
			'slug' => array(
				'field' => 'slug',
				'label' => 'lang:ctd_slug_label',
				'rules' => 'trim|alpha_dot_dash|max_length[100]|callback__check_slug['.$id.']'
			),
		)));

		if ($this->input->post('date_publish_str'))
		{
			$df = explode('-', $this->input->post('date_publish_str'));
			$publish_on_day = $df[2];
			$publish_on_month = $df[1];
			$publish_on_year = $df[0];
			$publish_on_hour = $this->input->post('publish_on_hour');
			$publish_on_minute = $this->input->post('publish_on_minute');
			$date_publish = mktime($publish_on_hour, $publish_on_minute, 0, $publish_on_month, $publish_on_day, $publish_on_year);
			unset($df);
		} else {
			if (!empty($_POST))
				$date_publish = 0;
			else
				$date_publish = $article->publish_on;
		}

		if ($this->input->post('date_from_str'))
		{
			$df = explode('-', $this->input->post('date_from_str'));
			$date_from_day = $df[2];
			$date_from_month = $df[1];
			$date_from_year = $df[0];
			$date_from = mktime($this->input->post('date_from_hour'), $this->input->post('date_from_minute'), 0, $date_from_month, $date_from_day, $date_from_year);
			$date_to = mktime($this->input->post('date_to_hour'), $this->input->post('date_to_minute'), 0, $date_from_month, $date_from_day, $date_from_year);
			unset($df);
		} else {
			if (!empty($_POST))
			{
				$date_from = 0;
				$date_to = 0;
			}
			else
			{
				$date_from = $article->date_from;
				$date_to = $article->date_to;
			}
		}

		if ($this->input->post('date_to_str'))
		{
			$dt = explode('-', $this->input->post('date_to_str'));
			$date_to_day = $dt[2];
			$date_to_month = $dt[1];
			$date_to_year = $dt[0];
			$date_to = mktime($this->input->post('date_to_hour'), $this->input->post('date_to_minute'), 0, $date_to_month, $date_to_day, $date_to_year);
			unset($dt);
		} else {
			if (!empty($_POST))
				$date_to = 0;
			else
				$date_to = $article->date_to ? $article->date_to : $date_from;
		}

			// uploaded image slideshow ends
			if ($this->input->post('curslide'))
			{
				$filename = $this->input->post('curslide');
			}

			// uploaded image slideshow starts
			if (!empty($_FILES['slideshow']))
			{
				// Setup upload config
				$config = $this->upload_cfg;
				$config['upload_path'] = UPLOAD_PATH . 'ctd/slides';
				$this->load->library('upload', $config);

				// check directory exists
				$this->check_dir($config['upload_path']);

				if ( ! $this->upload->do_upload('slideshow'))
				{
				}
				else
				{
					$file = $this->upload->data('');
					$data = array(
						'filename'		=> $file['file_name'],
						'uploaded_by'	=> $this->current_user->id,
						'uploaded_on'	=> now(),
						'position'		=> 999
					);

					if ($file['is_image'])
					{
						$this->load->library('image_lib');
						$filename = $file['file_name'];
						$thumbfile = thumbnail($filename);
						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->thumb_width ? $file['image_width'] : $this->thumb_width);
						$image_cfg['height'] = ($file['image_height'] < $this->thumb_height ? $file['image_height'] : $this->thumb_height);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['master_dim'] = 'width';
						$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();
	
						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->settings->slideshow_width ? $file['image_width'] : $this->settings->slideshow_width);
						$image_cfg['height'] = ($file['image_height'] < $this->settings->slideshow_height ? $file['image_height'] : $this->settings->slideshow_height);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['master_dim'] = 'width';
						//$image_cfg['new_image'] = $file['file_path'] . $medfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

						if ($this->input->post('oldimage'))
						{
							$this->unlinkslide($this->input->post('oldslide'));
						}

						if ($this->input->post('curslide'))
						{
							$this->unlinkslide($this->input->post('curslide'));
						}
					}

				}
			}
		
			
		if ($this->form_validation->run())
		{
			
			$input = array(
	            'title'				=> $this->input->post('title'),
	            'intro'				=> $this->input->post('intro'),
	            'body'				=> $this->input->post('body'),
				'notes'	 			=> $this->input->post('notes'),
				//'sponsors'	 		=> $this->input->post('sponsors'),

				'slug' 				=> $this->input->post('slug'),
	            'category_id'		=> $this->input->post('category_id'),
	            'status'			=> $this->input->post('status'),

				'country' 			=> $this->input->post('country'),
				'location' 			=> $this->input->post('location'),
				'geolocation' 		=> $this->input->post('geocoordinates') ? NULL : $this->input->post('geolocation'),
				'geocoordinates'	=> $this->input->post('geocoordinates'),
				'hide_map'			=> (int)$this->input->post('hide_map'),

				'meta_title'	 	=> $this->input->post('meta_title'),
				'meta_description' 	=> $this->input->post('meta_description'),
				'meta_keywords' 	=> $this->input->post('meta_keywords'),

				'publish_on' 		=> $date_publish,
				'date_from' 		=> $date_from,
				'date_to' 			=> $date_to,
				
				'event_year' 		=> $date_from_year,
				
				'event_venue' 		=> $this->input->post('event_venue'),
				'venue_address' 	=> $this->input->post('venue_address'),
	    	);

			/* ========== setting email alert approve ========== */
			if (isset($_POST['treasure'])){
				$data = $input;
				$data['slug'] = 'upon-event-approval';
				
				/* list email template */
				$data['title'] = $this->input->post('title');
				$data['submitted_by_name'] = $this->input->post('submitted_by_name');
				$data['linksubmitphoto'] = anchor('/submit-photos');
				/* list email template */
				
				$data['email'] = $this->input->post('submitted_by_email');
				$data['from'] = 'admin@catalyzecommunications.com';
				
				$to_and_cc_list = array($this->input->post('submitted_by_email'));
				$data['to'] = $to_and_cc_list;

				$data['name'] = 'Coral Triangle Day Organizers';
				Events::trigger('email', $data, 'array'); // send notification approve event
				
				//echo $this->input->post('submitted_by_email');
				
				$input['approve'] = 'yes';
			}
			/* ========== end setting email alert ========== */
			
			if (!empty($filename))
			{
				$input['slide'] = $filename;
			}
			
			/* ========== setting email alert approve ========== */
			if (isset($_POST['treasure'])){
				$input['approve'] = 'yes';
			}
			/* ========== end setting email alert ========== */
			
			// now update the event
			$result = $this->ctd_m->update($id, $input);

			// Wipe cache for this model as the data has changed
			$this->pyrocache->delete_all('ctd_m');

			// Set the flashdata message and redirect the user
			$link = anchor('admin/ctd/preview/'.$id, $this->input->post('title'), 'class="modal-large"');
			$this->session->set_flashdata(sprintf($this->lang->line('ctd_edit_success'), $this->input->post('title')));

			/************************* revision *************************/
			
			if ($result)
			{
				$file_ids = $this->input->post('file_id');
				if (!empty($file_ids))
				{
					$input = array();
					$pos = 1;
					foreach($this->input->post('file_id') as $filedata)
					{
						$input['ctd_id'] = $id;
						$input['position'] = $pos;
						$this->ctd_partners_m->updatefile($filedata, $input);
						$pos++;
					}
				}

				$this->session->set_flashdata(array('success'=> sprintf($this->lang->line('ctd_edit_success'), $this->input->post('title'))));

				// The twitter module is here, and enabled!
				if($this->input->post('tw_publish') && $this->settings->item('twitter_news') == 1 && ($article->status != 'live' && $this->input->post('status') == 'live'))
				{
					$url = shorten_url('ctd/'.url_title($this->input->post('title')));
					$this->load->model('twitter/twitter_m');
					$this->twitter_m->update(sprintf($this->lang->line('ctd_twitter_posted'), $this->input->post('title'), $url));
				}
				// End twitter code
			}
			
			else
			{
				$this->session->set_flashdata(array('error'=> $this->lang->line('ctd_edit_error')));
			}
			
			// Redirect back to the form or main page
			$this->input->post('btnAction') == 'save_exit' ? redirect('admin/ctd') : redirect('admin/ctd/edit/'.$id);
		}

			// Go through all the known fields and get the post values
			foreach($this->validation_rules as $key => $field)
			{
				if( isset($_POST[$field['field']]) )
				{
					$article->$field['field'] = set_value($field['field']);
					if ($field['field'] == 'curslide' && $this->input->post('curslide'))
					{
						$article->slide = $this->input->post('curslide');
						$article->curslide = $this->input->post('curslide');
					}
				}
			}

		if (!empty($filename))
		{
			$article->slide = $filename;
			$article->curslide = $filename;
		}

		$files = $this->ctd_partners_m->getuploadedimages($id);
		$partners = '
			<div class="no_data files">
				<p>'.lang('files:no_files').'</p>
			</div>
		';
		if (!empty($files))
		{
			//$this->data->files =& $files;
			$partners = $this->load->view('admin/files/contents', array('files'=>$files), TRUE);
		}
		
		$this->template
			->title($this->module_details['name'], sprintf(lang('ctd_edit_title'), $article->title))
			->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			->append_css('module::files.css' )
			->append_css('module::jquery.fileupload-ui.css')
			->append_js('module::jquery.cooki.js')
			->append_js('module::jquery.fileupload.js')
			->append_js('module::jquery.fileupload-ui.js')
			->append_js('module::jquery.ba-hashchange.min.js')
			->append_metadata('<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>')
			->append_js('module::functions.js' )
			->append_js('module::ctd_form.js' )
			//->append_metadata('<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>')
			->set('hours', $this->hours)
			->set('minutes', $this->minutes)
			->set('categories', $this->categories)
			->set('ctd_status', $this->ctd_status)
			->set('partners', $partners)
			->set('article', $article)
			->build('admin/form');
	}	
	
	function preview($id = 0)
	{		
		$article = $this->ctd_m->get($id);
		
		$this->template
			->set('article', $article)
			->set_layout('modal', 'admin')
			->build('admin/preview');
	}
	

	// Admin: Different actions
	function action()
	{
		switch($this->input->post('btnAction'))
		{
			case 'publish':
				$this->publish();
			break;
			case 'delete':
				$this->delete();
			break;
			case 'export':
				$this->export();
			break;
			case 'delreg':
				$this->delreg();
			break;
			default:
				redirect('admin/ctd');
			break;
		}
	}

	// Admin: Publish an article
	function publish($id = 0)
	{
		// Publish one
		$ids = ($id) ? array($id) : $this->input->post('action_to');
		
		if(!empty($ids))
		{
			// Go through the array of slugs to publish
			$article_titles = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if($article = $this->ctd_m->get($id) )
				{
					$this->ctd_m->publish($id);
					
					// Wipe cache for this model, the content has changed
					$this->pyrocache->delete_all('ctd_m');				
					$article_titles[] = $article->title;
				}
			}
		}
	
		// Some articles have been published
		if(!empty($article_titles))
		{
			// Only publishing one article
			if( count($article_titles) == 1 )
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('ctd_publish_success'), $article_titles[0]));
			}			
			// Publishing multiple articles
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('ctd_mass_publish_success'), implode('", "', $article_titles)));
			}
		}		
		// For some reason, none of them were published
		else
		{
			$this->session->set_flashdata('notice', $this->lang->line('ctd_publish_error'));
		}
		
		redirect('admin/ctd');
	}

	// Admin: Delete an event
	function delete($id = 0)
	{
		// Delete one
		$ids = ($id) ? array($id) : $this->input->post('action_to');
		
		// Go through the array of slugs to delete
		if(!empty($ids))
		{
			$article_titles = array();
			foreach ($ids as $id)
			{
				// Get the current event so we can grab the id too
				if($article = $this->ctd_m->get($id) )
				{
					$this->ctd_m->delete($id);
					
					// Wipe cache for this model, the content has changed
					$this->pyrocache->delete_all('ctd_m');				
					$article_titles[] = $article->title;
				}
			}
		}
		
		// Some data have been deleted
		if(!empty($article_titles))
		{
			// Only deleting one page
			if( count($article_titles) == 1 )
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('ctd_delete_success'), $article_titles[0]));
			}			
			// Deleting multiple pages
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('ctd_mass_delete_success'), implode('", "', $article_titles)));
			}
		}		
		// For some reason, none of them were deleted
		else
		{
			$this->session->set_flashdata('notice', lang('ctd_delete_error'));
		}
		
		redirect('admin/ctd');
	}

	public function upload($ctd_id = 0)
	{
		$this->_check_ext();
	
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_file_rules);
						
		if ($this->form_validation->run())
		{
			// Setup upload config
			$this->load->library('upload', array(
				'upload_path'	=> $this->_path,
				'allowed_types'	=> $this->_ext,
				'file_name'		=> $this->_filename
			));

			// File upload error
			if ( ! $this->upload->do_upload('userfiles'))
			{
				$status		= 'error';
				$message	= $this->upload->display_errors();

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message
					));
				}

				$thisdata->messages[$status] = $message;
			}

			// File upload success
			else
			{
				$file = $this->upload->data();

				// resize image starts
						$this->load->library('image_lib');
						$filename = $file['file_name'];
						$thumbfile = thumbnail($filename);
						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->thumb_width ? $file['image_width'] : $this->thumb_width);
						$image_cfg['height'] = ($file['image_height'] < $this->thumb_height ? $file['image_height'] : $this->thumb_height);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

						if ($this->input->post('oldimage'))
						{
							$this->unlinkfile($this->input->post('oldimage'));
						}
				// resize image ends

				$data = array(
					'ctd_id'		=> (int) $this->input->post('ctd_id'),
					'user_id'		=> (int) $this->current_user->id,
					'type'			=> $this->_type,
					'name'			=> $this->input->post('filetitle'),
					'description'	=> $this->input->post('filedescription') ? $this->input->post('filedescription') : '',
					'filename'		=> $file['file_name'],
					'extension'		=> $file['file_ext'],
					'mimetype'		=> $file['file_type'],
					'filesize'		=> $file['file_size'],
					'width'			=> (int) $file['image_width'],
					'height'		=> (int) $file['image_height'],
					'date_added'	=> now()
				);

				// Insert success
				if ($id = $this->ctd_partners_m->insert($data))
				{
					$status		= 'success';
					$message	= sprintf(lang('files:create_success'), $file['file_name']);
				}
				// Insert error
				else
				{
					$status		= 'error';
					$message	= lang('files:create_error');
				}

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'id'	=> $id,
						'ctd_id'	=> $ctd_id,
						'file'		=> array(
							'name'	=> $file['file_name'],
							'type'	=> $file['file_type'],
							'size'	=> $file['file_size'],
							'thumb'	=> base_url() . 'content/thumb/' . $id . '/80'
						)
					));
				}

				if ($status === 'success')
				{
					$this->session->set_flashdata($status, $message);
					redirect('admin/content/creates#content-attachment-tab');
				}
			}
		}
		elseif (validation_errors())
		{
			// if request is ajax return json data, otherwise do normal stuff
			if ($this->input->is_ajax_request())
			{
				$message = $this->load->view('admin/partials/notices', array(), TRUE);

				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
		}

		if ($this->input->is_ajax_request())
		{
			// todo: debug errors here
			$status		= 'error';
			$message	= 'unknown';

			$data = array();
			$data['messages'][$status] = $message;
			$message = $this->load->view('admin/partials/notices', $data, TRUE);

			return $this->template->build_json(array(
				'status'	=> $status,
				'message'	=> ""
			));
		}

		// Loop through each validation rule

		foreach ($this->_validation_rules as $rule)
		{
			if ($rule['field'] == 'ctd_id')
			{
				$thisdata->file->{$rule['field']} = set_value($rule['field'], $ctd_id);
			}
			else
			{
				$thisdata->file->{$rule['field']} = set_value($rule['field']);
			}
			
		}

		$this->template
			->title()
			->build('admin/files/upload', $thisdata);
	}

	public function logos($id = 0, $filter = '')
	{
		if ($id)
		{
			$folder = $this->ctd_partners_m->getuploadedimages($id);
		}
		$this->load->library('table');

		$files = $this->ctd_partners_m
			->order_by('date_added', 'DESC')
			->getuploadedimages($id);

		$data->folder =& $folder;
		$data->files =& $files;

		// Response ajax
		if ($this->input->is_ajax_request())
		{
				$content	= $this->load->view('admin/files/contents', $data, TRUE);

			return $this->template->build_json(array(
				'status'	=> 'success',
				'content'	=> $content,
			));
		}

		$this->template
			->append_metadata( css('files:css', 'files') )
			->set("folder", $folder)
			->set("files", $files)
			->build('admin/files/contents', $this->data);
	}

	function check_dir($dir)
	{
		// check directory
		$fileOK = array();
		$fdir = explode('/', $dir);
		$ddir = '';
		for($i=0; $i<count($fdir); $i++)
		{
			$ddir .= $fdir[$i] . '/';
			if (!is_dir($ddir))
			{
				if (!@mkdir($ddir, 0777)) {
					$fileOK[] = 'not_ok';
				}
				else
				{
					$fileOK[] = 'ok';
				}
			}
			else
			{
				$fileOK[] = 'ok';
			}
		}
		return $fileOK;

	}

	// Callback: from create()
	function _check_slug($slug = '', $id = null)
	{
		$check_slug = $this->ctd_m->check_slug($slug, $id);
		if(!$check_slug)
		{
			$this->form_validation->set_message('_check_slug', lang('ctd_already_exist_error'));
			return FALSE;
		}
		
		return TRUE;
	}

	/**
	 * Validate upload file name and extension and remove special characters.
	 */
	function _check_ext()
	{
		if ( ! empty($_FILES['userfiles']['name']))
		{
			$ext		= pathinfo($_FILES['userfiles']['name'], PATHINFO_EXTENSION);
			$allowed	= $this->config->item('files_allowed_file_ext');

			foreach ($allowed as $type => $ext_arr)
			{				
				if (in_array(strtolower($ext), $ext_arr))
				{
					$this->_type		= $type;
					$this->_ext			= implode('|', $ext_arr);
					$this->_filename	= trim(url_title($_FILES['userfiles']['name'], 'dash', TRUE), '-');

					break;
				}
			}

			if ( ! $this->_ext)
			{
				$this->form_validation->set_message('_check_ext', lang('files:invalid_extension'));
				return FALSE;
			}
		}		
		elseif ($this->method === 'upload')
		{
			$this->form_validation->set_message('_check_ext', lang('files:upload_error'));
			return FALSE;
		}

		return TRUE;
	}

	public function unlinkfile($filename)
	{
			$thumbfile = thumbnail($filename);

			$stat = array();
					// delete the files
					if (file_exists($this->upload_cfg['upload_path'] . '/' . $thumbfile))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/' . $thumbfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('content_delete_img_error'), $thumbfile);
						}
					}
		
					if (file_exists($this->upload_cfg['upload_path'] . '/' . $filename))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/' . $filename))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('content_delete_img_error'), $filename);
						}
					}

					if (in_array('error', $stat))
					{
						$status = 'notice';
						$message = implode('<br />', $msg);
					}
					else
					{
						$status = 'notice';
						$message = sprintf(lang('content_delete_img_success'), $filename);
					}

		return (array($status, $message));
	}

	public function unlinkslide($filename)
	{
			$thumbfile = thumbnail($filename);

			$stat = array();
					// delete the files
					if (file_exists($this->upload_cfg['upload_path'] . '/slides/' . $thumbfile))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/slides/' . $thumbfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('content_delete_img_error'), $thumbfile);
						}
					}
		
					if (file_exists($this->upload_cfg['upload_path'] . '/slides/' . $filename))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/slides/' . $filename))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('content_delete_img_error'), $filename);
						}
					}

					if (in_array('error', $stat))
					{
						$status = 'notice';
						$message = implode('<br />', $msg);
					}
					else
					{
						$status = 'notice';
						$message = sprintf(lang('content_delete_img_success'), $filename);
					}

		return (array($status, $message));
	}

	public function deletepartner($id=0)
	{
		$ids = $id
			? is_array($id)
				? $id
				: array($id)
			: (array) $this->input->post('action_to');

		if ($id)
		{
			$file = $this->ctd_partners_m->get($id);
			$status = $this->ctd_partners_m->delete($id) ? 'success' : 'error';
			if ($status == 'success')
				$message = sprintf(lang('ctd_partner_deleted'), $file->name ? $file->name : $file->filename);
			elseif ($status == 'error')
				$message = sprintf(lang('ctd_partner_not_deleted'), $file->name ? $file->name : $file->filename);

			$data = array();
			$data['messages'][$status] = $message;
			$message = $this->load->view('admin/partials/notices', $data, TRUE);

			$files = $this->ctd_partners_m->getuploadedimages($file->ctd_id);
			$partners = '
				<div class="no_data files">
					<p>'.lang('files:no_files').'</p>
				</div>
			';
			if (!empty($files))
			{
				$partners = $this->load->view('admin/files/contents', array('files'=>$files), TRUE);
			}
			
		}
		else
		{
			return FALSE;
		}


		if ($this->input->is_ajax_request())
		{
			return $this->template->build_json(array(
				'status'	=> $status,
				'message'	=> $message,
				'content'	=> $partners
			));
		}

//		redirect('admin/ctd/create#event-partners');
		// Redirect
//		isset($folder) ? redirect('admin/content/#!path=' . $folder->virtual_path) : redirect('admin/content');
	}

	/**
	 * Delete a file
	 *
	 * @params 	int The file id
	 */
	public function deletefile($id = 0)
	{
		$ids = $id
			? is_array($id)
				? $id
				: array($id)
			: (array) $this->input->post('action_to');

		$total		= sizeof($ids);
		$deleted	= array();

		// Try do deletion
		foreach ($ids as $id)
		{
			// Get the row to use a value.. as title, name
			if ($file = $this->ctd_partners_m->get($id))
			{
				// Make deletion retrieving an status and store an value to display in the messages
				$deleted[($this->ctd_partners_m->delete($id) ? 'success': 'error')][] = $file->filename;

//				$folder	= $this->_folders[$file->folder_id];
			}
		}

		// Set status messages
		foreach ($deleted as $status => $values)
		{
			// Mass deletion
			if (($status_total = sizeof($values)) > 1)
			{
				$last_value		= array_pop($values);
				$first_values	= implode(', ', $values);

				// Success / Error message
				$this->session->set_flashdata($status, sprintf(lang('files:mass_delete_' . $status), $status_total, $total, $first_values, $last_value));
			}

			// Single deletion
			else
			{
				// Success / Error messages
				$this->session->set_flashdata($status, sprintf(lang('files:delete_' . $status), $values[0]));
			}
		}

		// He arrived here but it was not done nothing, certainly valid ids not were selected
		if ( ! $deleted)
		{
			$this->session->set_flashdata('error', lang('files:no_select_error'));
		}

		if ($this->input->is_ajax_request())
		{
			return $this->template->build_json(array(
				'status'	=> $status,
				'message'	=> $message,
			));
		}

		redirect('admin/ctd/create#event-partners');
		// Redirect
//		isset($folder) ? redirect('admin/content/#!path=' . $folder->virtual_path) : redirect('admin/content');
	}


	// ------------------------------------------------------------------------

	/**
	 * Edit Uploaded file
	 *
	 */
	public function editfile($id=0)
	{
		$this->load->library('form_validation');
		$slide_rules = array(
			array(
				'field' => 'name',
				'label' => 'lang:content_title_label',
				'rules' => 'trim|max_length[250]'
			),
			array(
				'field' => 'description',
				'label' => 'lang:content_description_label',
				'rules' => 'trim'
			),
		);
		$this->form_validation->set_rules($slide_rules);
				
		if ( ! ($id && ($file = $this->ctd_partners_m->get($id))))
		{
			$status		= 'error';
			$message	= lang('files:file_label_not_found');

			if ($this->input->is_ajax_request())
			{
				$data = array();
				$data['messages'][$status] = $message;
				$message = $this->load->view('admin/partials/notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> $status,
					'message'	=> $message
				));
			}

			$this->session->set_flashdata($status, $message);
			redirect('admin/ctd/create#event-partners');
		}

		//$this->data->slide =& $file;
		
		if ($this->form_validation->run())
		{
			$this->_check_ext();
			
			// We are uploading a new file
			if ( ! empty($_FILES['userfile']['name']))
			{
				// Setup upload config
				$this->load->library('upload', array(
					'upload_path'	=> $this->_path,
					'allowed_types'	=> $this->_ext
				));

				// File upload error
				if ( ! $this->upload->do_upload('userfile'))
				{
					$status		= 'error';
					$message	= $this->upload->display_errors();

					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);

						return $this->template->build_json(array(
							'status'	=> $status,
							'message'	=> $message
						));
					}

					$this->data->messages[$status] = $message;
				}
				// File upload success
				else
				{
					// Remove the original file
					$this->ctd_partners_m->delete_file($id);

					$file = $this->upload->data();
					$data = array(
						'ctd'			=> (int) $this->input->post('ctd_id'),
						'user_id'		=> (int) $this->current_user->id,
						'type'			=> $this->_type,
						'name'			=> $this->input->post('name'),
						'description'	=> $this->input->post('description'),
						'filename'		=> $file['file_name'],
						'extension'		=> $file['file_ext'],
						'mimetype'		=> $file['file_type'],
						'filesize'		=> $file['file_size'],
						'width'			=> (int) $file['image_width'],
						'height'		=> (int) $file['image_height'],
					);

					if ($this->ctd_partners_m->update($id, $data))
					{
						$status		= 'success';
						$message	= lang('files:edit_success');
					}
					else
					{
						$status		= 'error';
						$message	= lang('files:edit_error');
					};

					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);

						return $this->template->build_json(array(
							'status'	=> $status,
							'message'	=> $message,
							'title'		=> $status === 'success' ? sprintf(lang('files:edit_title'), $this->input->post('name')) : $file->name
						));
					}

					if ($status === 'success')
					{
						$this->session->set_flashdata($status, $message);
						redirect ('admin/files');
					}
				}
			}

			// Upload data
			else
			{
				$data = array(
					'user_id'		=> $this->current_user->id,
					'name'			=> $this->input->post('name'),
					'description'	=> $this->input->post('description')
				);

				if ($this->ctd_partners_m->update($id, $data))
				{
					$status		= 'success';
					$message	= lang('files:edit_success');
				}
				else
				{
					$status		= 'error';
					$message	= lang('files:edit_error');
				};

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'title'		=> $status === 'success' ? sprintf(lang('files:edit_title'), $this->input->post('name')) : $file->name
					));
				}

				if ($status === 'success')
				{
					$this->session->set_flashdata($status, $message);
					redirect ('admin/ctd');
				}
			}
		}
		elseif (validation_errors())
		{
			// if request is ajax return json data, otherwise do normal stuff
			if ($this->input->is_ajax_request())
			{
				$message = $this->load->view('admin/partials/notices', array(), TRUE);

				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
		}

		$this->input->is_ajax_request() && $this->template->set_layout(FALSE);

		$this->template
			->title('')
			->set("file", $file)
			->build('admin/files/editfile');
	}

	public function blast($last_id=0)
	{
		$data = array();
		$ctds = $this->ctd_m->get_many_by(array('status'=>'live'));
		if (!empty($ctds))
		{
			foreach($ctds as $num=>$ctd)
			{
				$data['email']	= $ctd->submitted_by_email;
				/* list email template */
				$data['slug'] = 'on-june-9th';
				$data['bcc'] = 'rendy.mulyono@catalyzecommunications.com';
				$data['submitter_name'] = $ctd->submitted_by_name;
				$data['photo_submission_url'] = site_url('submit-photos');
				$data['from'] = 'contest@coraltriangleday.org';
				$data['name'] = 'Coral Triangle Day Organizers';
				$sent = Events::trigger('email', $data, 'array'); // send notification approve event
				if ($sent)
				{
					$status = 'ok';
					$message = "Emailing ".$ctd->submitted_by_email." (".$ctd->title.") status: <b>Sent</b><br />\n";
					echo $message;
				}
				else
				{
					$status = 'error';
					$message = "Emailing ".$ctd->submitted_by_email." (".$ctd->title.") status: <b>Failed</b><br />\n";
					echo $message;
				}
				$results[] = array(
					'status'	=> $status,
					'message'	=> $message,
					'ctd_id'	=> $ctd->id,
				);
			}
		}
	}
}
?>