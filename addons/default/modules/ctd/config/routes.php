<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/
$route['ctd/subscribe'] = "ctd/subscribe/";
$route['ctd/subscribe(:any)'] = "ctd/subscribe/";
$route['ctd/page'] = "ctd/index";
$route['ctd/page/(:num)'] = "ctd/index/$1";
$route['ctd/admin/categories(:any)?'] = 'admin_categories$1';
$route['ctd/admin/attendance(:any)?'] = 'admin_attendance$1';
$route['ctd/tools/(:any)'] = "ctd/tools/$1";

$route['ctd/rss/all.rss'] = "rss/index";
$route['ctd/rss/(:any).rss'] = "rss/category/$1";

$route['ctd/ical/all.ics'] = "ical/index";
$route['ctd/ical/(:any).ics'] = "ical/category/$1";
/*
/ical.ics
category/categoryname/ical.ics
*/
//
$route['ctd/ctdcopy'] = "ctd/ctdcopy";
/* */
$route['ctd/uploadimage'] = "ctd/uploadimage";
$route['ctd/submitevent'] = "ctd/submitevent";

$route['ctd/get_past_data'] = "ctd/get_past_data";
$route['ctd/get_past_data/(:any)'] = "ctd/get_past_data/$1";

$route['ctd/thankyou'] = "ctd/thankyou";
$route['ctd/email_success_photo'] = "ctd/email_success_photo";
$route['ctd/(:num)/(:num)'] = "ctd/date/$1/$2";

$route['ctd/view/(:any)'] = 'error_404';
$route['ctd/([a-zA-Z_-]+)-(:any)'] = 'ctd/view/$1-$2';
$route['ctd/([a-zA-Z_-]+)?'] = 'ctd/view/$1';

?>