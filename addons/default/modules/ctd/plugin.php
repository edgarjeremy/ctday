<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Events Plugin
 *
 * Create a list of events
 */
class Plugin_Ctd extends Plugin
{

	protected $countries = array(
			'' => 'Select',
			'FJ' => 'Fiji',
			'ID' => 'Indonesia',
			'MY' => 'Malaysia',
			'PG' => 'Papua New Guinea',
			'PH' => 'Philippines',
			'SB' => 'Solomon Islands',
			'TL' => 'Timor Leste',
		);


	public function __construct()
	{
		$this->load->model('ctd_m');
		$this->lang->load('ctd');
		$this->load->helper(array('date', 'filename'));
	}

	function number_of_events()
	{
		$res = $this->ctd_m->count_by(array('status'=>'live'));
		return $res;
	}

	function events_table()
	{
		$countries = array(
			'' => 'Select',
			'FJ' => 'Fiji',
			'ID' => 'Indonesia',
			'MY' => 'Malaysia',
			'PG' => 'Papua New Guinea',
			'PH' => 'Philippines',
			'SB' => 'Solomon Islands',
			'TL' => 'Timor Leste',
		);
		
		//$f_datenow = '2015'; //date('Y');
		$f_datenow = date('Y');
		
		$res = $this->ctd_m->get_many_by(array('status'=>'live', 'event_year'=>$f_datenow, 'orderby'=>'date_from',  'sort'=>'ASC') );
		
		//print_r($res);
		//$this->output->enable_profiler(TRUE);
		
		foreach($res as $i => $r)
		{
			$sponsors = $organizers = array();
			if ($r->sponsors)
				$sponsors = $this->ctd_m->get_images_by_ids(unserialize($r->sponsors));
			if ($r->organiser)
				$organizers = $this->ctd_m->get_images_by_ids(unserialize($r->organiser));

			$date_from = date('d M Y', $r->date_from);
			$date_to = '';
			if ($r->date_to)
			{
				if (date('Y', $r->date_to) == date('Y', $r->date_from))
				{
					//$date_from = date('d M', $r->date_from);
					$date_to = date('d M Y', $r->date_to);
				}
			}

			$ret[$r->country][] = array(
					'title' => $r->title,
					'location'	=> $r->location,
					'geolocation'	=> $r->geolocation,
					'event_venue'	=> $r->event_venue,
					'contact_email'	=> $r->contact_email,
					'date'	=> date('d M Y', $r->date_from),
					'date_from' => $date_from,
					'time_from' => date('H:i', $r->date_from),
					'date_to' => $date_to,
					'time_to' => $r->date_to ? date('H:i', $r->date_to) : NULL,
					'venue_address' => $r->venue_address,
					'intro' => $r->intro,
					'sponsors'	=> $sponsors,
					'organizers'=> $organizers,
					'contact'	=> $r->notes,
					'event_year'	=> $r->event_year,
					'website'	=> $r->website,
			);
		}
/*
		if(!empty($ret)){
			return $ret;
		} else{
			$ret = '';
		}
*/

		//$this->load_view('events', array('events'=>$ret, 'countries'=>$countries));		
		//return $this->load_view('events', array('events'=>$ret, 'countries'=>$countries));

		if(!empty($ret)){
			return $this->load_view('events', array('events'=>$ret, 'countries'=>$countries));
		} else{
			$ret = '';
			return $this->load_view('events', array('events'=>$ret, 'countries'=>$countries));
		}
		
		//return $ret;
	}

	function get_countries()
	{
		$ret = array();
		$res = $this->ctd_m->get_many_by(array('status'=>'live', 'orderby'=>'data', 'sort'=>'ASC', 'groupby'=>'country') );
		if (!empty($res))
		{
			foreach($res as $i => $r)
			{
				$ret[$r->country] = array(
					'country_code'	=> $r->country,
					'country_name'	=>  $this->countries[$r->country],
				);
			}
		}
		return $ret;
	}

	function js_map()
	{
		$countries = array(
			'' => 'Select',
			'FJ' => 'Fiji',
			'ID' => 'Indonesia',
			'MY' => 'Malaysia',
			'PG' => 'Papua New Guinea',
			'PH' => 'Philippines',
			'SB' => 'Solomon Islands',
			'TL' => 'Timor Leste',
		);
		
		$res = $this->ctd_m->get_many_by(array('status'=>'live', 'orderby'=>'data', 'sort'=>'ASC') );
		
		//$this->output->enable_profiler(TRUE);
		
		foreach($res as $i => $r)
		{
			$geo = explode(',', $r->geocoordinates);
			
			$ret[] = array(
				$r->title,
				!empty($geo[0])?$geo[0]:'',
				!empty($geo[1])?$geo[1]:'',
				$r->intro,
				$r->id,
				$r->country
			);
		}
		
		if(!empty($ret)){
			return json_encode($ret);
		} else{
			$ret = '';
		}
		
		//return json_encode($ret);
	}

	protected function load_viewXY($view, $data)
	{
		$CI =& get_instance();
		$ext = pathinfo($view, PATHINFO_EXTENSION) ? '' : '.php';
		$path = @$CI->template->get_views_path().'partials/';
//echo "\$path: $path<br />\n";
		$CI->load->set_view_path($path);
		$CI->load->vars($data);
		return $CI->load->_ci_load(array('_ci_view' => $view, '_ci_return' => true));
	}

	protected function load_view($view, $data)
	{
	 $ext = pathinfo($view, PATHINFO_EXTENSION) ? '' : '.php';
	
	 $path = ci()->template->get_views_path().'partials/';
	
	 // add this view location to the array
	 ci()->load->set_view_path($path);
	 ci()->load->vars($data);
	 return ci()->load->_ci_load(array('_ci_view' => $view, '_ci_return' => true));
	}

	function display_all()
	{
		$limit = $this->attribute('limit', 0);
		$country = $this->attribute('country');

		if (!$country) return false;

		$show_intro = $this->attribute('intro', 'true');

		$f_datenow = date('Y');
		
		$res = $this->ctd_m->get_many_by(array('status'=>'live', 'event_year'=>$f_datenow, 'orderby'=>'date_from', 'sort'=>'ASC') );

		$result = array();

		if ($res)
		{
			$num = 0;
			foreach($res as $r)
			{
				foreach($r as $k=>$v)
				{
					if ($k=='intro') {
						// 30 words of intro
						$result[$num]['short_intro'] = word_limiter($v, 30);
					}
	
					if ($k=='date_from')
					{
						// human format of news date
						$result[$num]['date_from_str'] = date("d/m/Y",$v);
					}
	
					if ($k=='date_to' && $v > 0)
					{
						// human format of news date
						$result[$num]['date_to_str'] = date('d F',$v);
					}
	
					if (empty($result[$num]['date_to_str']))
						$result[$num]['date_to_str'] = '';
					$result[$num][$k] = $v;
				}
				$num++;
			}
			return $result;
		}
		return FALSE;
	}

	function any_upcoming($data=array())
	{

		$limit = $this->attribute('limit', 4);
		$category = $this->attribute('category');
		$show_intro = $this->attribute('intro', 'true');

		$res = $this->ctd_m->count_by(array('status'=>'live', 'limit'=>$limit, 'month'=>date('m', now()), 'year'=>date('Y', now()) ));

		return $res;

	}

	function upcoming($data=array())
	{
		$limit = $this->attribute('limit', 4);
		$category = $this->attribute('category');
		$show_intro = $this->attribute('intro', 'true');

		$res = $this->ctd_m->get_many_by(array('status'=>'live', 'limit'=>$limit, 'month'=>date('m', now()), 'year'=>date('Y', now()) ));

		$result = array();

		if ($res)
		{
			$num = 0;
			foreach($res as $r)
			{
				foreach($r as $k=>$v)
				{
					if ($k=='intro') {
						// 30 words of intro
						$result[$num]['short_intro'] = word_limiter($v, 30);
					}
	
					if ($k=='date_from')
					{
						// human format of news date
						$result[$num]['date_from_str'] = date('d F',$v);
					}
	
					if ($k=='date_to' && $v > 0)
					{
						// human format of news date
						$result[$num]['date_to_str'] = date('d F',$v);
					}
	
					if (empty($result[$num]['date_to_str']))
						$result[$num]['date_to_str'] = '';
					$result[$num][$k] = $v;
				}
				$num++;
			}
			return $result;
		}
		return FALSE;
	}

	function calendar($data=array())
	{
		$show_intro = $this->attribute('intro', 'true');

		if ($show_intro == 'true')
		{
			$intro = '';
		}
		else
		{
			$intro = '/false';
		}

		$ctd = $evt = array();

		$prefs['show_next_prev'] = true;
		$prefs['next_prev_url'] = site_url('ctd/calendar');

		$prefs['template'] = '
   {div_open}<div id="calendar><!-- div_open -->{/div_open}

   {heading_row_start}<tr>{/heading_row_start}

   {heading_previous_cell}<td><div class="left"><a class="prev" href="{previous_url}'.$intro.'"><span>&lt;&lt;</span></a></td>{/heading_previous_cell}
   {heading_title_cell}<td colspan="{colspan}"><h3>{heading}</h3></td>{/heading_title_cell}
   {heading_next_cell}<td><div class="right"><a class="next" href="{next_url}'.$intro.'"><span>&gt;&gt;</span></a></td>{/heading_next_cell}

   {heading_row_end}</tr>{/heading_row_end}

   {table_open}<table border="0" cellpadding="0" cellspacing="0">{/table_open}

   {week_row_start}<tr>{/week_row_start}
   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
   {week_row_end}</tr>{/week_row_end}

   {cal_row_start}<tr>{/cal_row_start}
   {cal_cell_start}<td {class}>{/cal_cell_start}

   {cal_cell_content}<a class="tiptip" href="{content}">{day}</a><div class="tipContent">{title}</div>{/cal_cell_content}
   {cal_cell_content_today}<a class="tiptip highlight" href="{content}">{day}</a><div class="tipContent">{title}</div>{/cal_cell_content_today}

   {cal_cell_no_content}{day}{/cal_cell_no_content}
   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

   {cal_cell_blank}&nbsp;{/cal_cell_blank}

   {cal_cell_end}</td>{/cal_cell_end}
   {cal_row_end}</tr>{/cal_row_end}

   {table_close}</table>{/table_close}
   {div_close}<!-- div_close --></div>{/div_close}
		';

		$evts = $this->ctd_m->get_many_by(array('year'=>date('Y', now()), 'month'=>date('m', now()) ));
		if (!empty($evts))
		{
			foreach($evts as $e)
			{
				$date = date('H:i', $e->date_from);
				if ($date == "00:00")
					$dt = '';
				else
					$dt = $date.'; ';
				$evt[date('j', $e->date_from)][] = array(
					'url'	=> site_url('ctd/'.date('Y', $e->date_from).'/'.date('m', $e->date_from).'/'.$e->slug),
					'title'	=> '<p class="title">'.$dt.$e->title.'</p>'.'<p>'.word_limiter(strip_tags($e->intro), 10).'</p>' //date('j/m', $e->date_from) . '; ' . $e->title
				);
			}

			foreach($evts as $e)
			{
				$title = array();
				$date = date('j', $e->date_from);
				$ctd[$date]['url'] = site_url('ctd/'.date('Y', $e->date_from).'/'.date('m', $e->date_from));
				foreach($evt[$date] as $ed)
				{
					if ($ed['title'])
						$title[] = $ed['title'];
				}
				$ctd[$date]['title'] = implode('', $title);//implode('<br/>', $evt[date('j', $e->date_from)]); //site_url('');
			}

		}
		else
		{
			$ctd = $evt = array();
		}

		$this->load->library('calendar', $prefs);
		return $this->calendar->generate(null,null, $ctd);
	}

	function subscriptionform()
	{
		$form = '
			<h2>Sign up for email updates</h2>
			<div id="signup-container">
				<form method="post" name="frmSignup" id="frmSignup" action="'.site_url('ctd/subscribe').'">
					<input type="text" name="email" id="frmEmail" class="newsletter-email" placeholder="Your email" />
					<input type="submit" name="btnSubmit" id="btnSubmit" class="newsletter-submit" value="Sign-up" />
				</form>
			</div>
			<div id="msgsignup"></div>
		';
		return $form;
	}

}


if ( ! function_exists('now'))
{
	function now()
	{
		$CI =& get_instance();
	
		if (strtolower($CI->config->item('time_reference')) == 'gmt')
		{
			$now = time();
			$system_time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));
	
			if (strlen($system_time) < 10)
			{
				$system_time = time();
				log_message('error', 'The Date class could not set a proper GMT timestamp so the local time() value was used.');
			}
	
			return $system_time;
		}
		else
		{
			return time();
		}
	}
}

/* End of file plugin.php */