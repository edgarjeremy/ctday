<?php  defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * PyroCMS
 *
 * An open source CMS based on CodeIgniter
 *
 * @package		PyroCMS
 * @author		PyroCMS Dev Team
 * @license		Apache License v2.0
 * @link		http://pyrocms.com
 * @since		Version 1.0-dev
 * @filesource
 */

/**
 * PyroCMS File Model
 *
 * Interacts with the files table in the database.
 *
 * @author		Dan Horrigan <dan@dhorrigan.com>
 * @author		Eric Barnes <eric@pyrocms.com>
 * @package		PyroCMS
 * @subpackage	Files
 */
class Ctd_partners_m extends MY_Model {

	protected $table = 'ctd_partners';

	// ------------------------------------------------------------------------

	/**
	 * Exists
	 *
	 * Checks if a given file exists.
	 *
	 * @access	public
	 * @param	int		The file id
	 * @return	bool	If the file exists
	 */
	public function exists($file_id)
	{
		return (bool) (parent::count_by(array('id' => $file_id)) > 0);
	}

	// ------------------------------------------------------------------------

	/**
	 * Delete a file
	 *
	 * Deletes a single file by its id and remove it from the db.
	 *
	 * @params	int	The file id
	 * @return 	bool
	 */
	public function delete($id)
	{
//return TRUE;
		$this->load->helper('file');

		if ( ! $image = parent::get($id))
		{
			return FALSE;
		}

		@unlink(FCPATH.'/' . $this->config->item('files_folder').'/'.$image->filename);
		@unlink(FCPATH.'/' . $this->config->item('files_folder').'/'.thumbnail($image->filename));

		parent::delete($image->id);

		return TRUE;
	}

	// ------------------------------------------------------------------------

	/**
	 * Delete a file
	 *
	 * Deletes a single file by its id
	 *
	 * @params	int	The file id
	 * @return 	bool
	 */
	public function delete_file($id)
	{
		$this->load->helper('file');

		if ( ! $image = parent::get($id))
		{
			return FALSE;
		}

		@unlink(FCPATH.'/' . $this->config->item('files_folder').'/'.$image->filename);
		@unlink(FCPATH.'/' . $this->config->item('files_folder').'/'.thumbnail($image->filename));

		return TRUE;
	}

	// ------------------------------------------------------------------------

	/**
	 * Delete multiple files
	 *
	 * Delete all files contained within a folder.
	 *
	 * @params int	Folder id
	 * @return void
	 */
	public function delete_files($ctd_id)
	{
		$this->load->helper('file');

		$image = parent::get_many_by(array('ctd_id' => $ctd_id));

		if ( ! $image)
		{
			return FALSE;
		}

		foreach ($image as $item)
		{
			@unlink(FCPATH.'/' . $this->config->item('files_folder').'/'.$item->filename);
			@unlink(FCPATH.'/' . $this->config->item('files_folder').'/'.thumbnail($item->filename));
			parent::delete($item->id);
		}

		return TRUE;
	}
	
	/**
	 * Retrieve slide images
	 * @param int ID of news
	 * @return array Array of data objects
	 */
	function getuploadedimages($id=0)
	{
		if ($id)
			$this->db->where('ctd_id', $id);
		else
		{
			$this->db->where('ctd_id', '0');
//			$this->db->where('user_id', $this->user->id);
		}
		$this->db->order_by('position');
		return $this->db->get('ctd_partners')->result();
	}


	/**
	 * Update news slide database
	 *
	 * @param int ID of image database to update
	 * @param array Input array
	 * @return bool
	 */
	function updatefile($id, $input)
	{
		$this->db->where('id', $id);
		return $this->db->update('ctd_partners', $input);
	}

	/**
	 * Retrieve single image
	 *
	 * @param int|array ID of image(s) to retrieve
	 * @return objects Table row data object
	 */
	function getsinglefile($id)
	{
		if (empty($id)) return FALSE;
		$this->db->where('id', $id);
		return $this->db->get('ctd_partners')->row();
	}
			
}

/* End of file file_m.php */