<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ctd_m extends MY_Model
{
	protected $_table = 'ctd';
	
	function get_year(){
		$this->db->select('ctd.*')->order_by( 'event_year','desc' );
		return $this->db->get('ctd')->result();
	}
	
    function get_all($params=array())
    {
    	$select = '
    		'. $this->db->dbprefix('ctd') .'.*,
    		ctd_categories.title AS category_title, ctd_categories.slug AS category_slug
    	';

		$sort = 'DESC';
		if (isset($params['sort']))
			$sort = $params['sort'];

		if (isset($params['orderby']))
		{
			if ($params['orderby'] == 'data')
			{
				$this->db->join("
				(SELECT country, COUNT(*) AS country_count
				      FROM ". $this->db->dbprefix('ctd') ."
				     GROUP BY country
				   ) AS c_count
				", "c_count.country = ctd.country");
		    	$this->db->order_by('c_count.country_count', 'desc', FALSE);
		    	$this->db->order_by('country', 'asc');
		    	$this->db->order_by('id', 'asc');
			}
			else
			{
		    	$this->db->order_by($params['orderby'], $sort);
		    }
	    }
	    else
	    	$this->db->order_by('date_from', $sort);

		if (!empty($params['groupby']))
		{
			$this->db->group_by($params['groupby']);
		}
		
		if (!empty($params['event_year']))
		{
			$this->db->where('event_year', $params['event_year']);
		}

    	$this->db->select($select);
       	$this->db->join('ctd_categories', $this->db->dbprefix('ctd') .'.category_id = ctd_categories.id', 'left');

        return $this->db->get('ctd')->result();
    }

	function get($id=null,$autosave=false) {
		if (!$id) return false;
		if ($autosave) {
	    	$select = '
	    		'. $this->db->dbprefix('ctd') .'.*,
	    		c.title AS category_title, c.slug AS category_slug
	    	';
		} else {
	    	$select = '
	    		'. $this->db->dbprefix('ctd') .'.*,
	    		c.title AS category_title, c.slug AS category_slug
	    	';
		}
    	$this->db->select($select);

       	$this->db->join('ctd_categories c', $this->db->dbprefix('ctd') .'.category_id = c.id', 'left');

		if (is_numeric($id))
			$this->db->where($this->db->dbprefix('ctd') .'.id', $id);
		elseif ($id)
			$this->db->where($this->db->dbprefix('ctd') .'.slug', $id);

		return $this->db->get('ctd')->row();
	}

	function get_images_by_ids($ids=array())
	{
		if (empty($ids)) return false;
		return $this->db->where_in('id', $ids)->get('gallery_media')->result();
	}

	function get_many_by($params = array())
    {
    	$this->load->helper('date');

		// get by category
		if (!empty($params['category']))
		{
			if (is_numeric($params['category']))
				$this->db->where('ctd_categories.id', $params['category']);
			else
				$this->db->where('ctd_categories.slug', $params['category']);
		}


		// get by country
		if (!empty($params['country'])) {
			$this->db->where('country', $params['country']);
		}

    	
    	if(!empty($params['day']))
    	{
    		$this->db->where('DAYOFMONTH(FROM_UNIXTIME(date_from))', $params['day']);
    	}
    	
    	if(!empty($params['month']))
    	{
    		$this->db->where('MONTH(FROM_UNIXTIME(date_from))', $params['month']);
//    		$this->db->or_where('MONTH(FROM_UNIXTIME(er.date_to))', $params['month']);
    	}
    	
    	if(!empty($params['year']))
    	{
    		//$this->db->where('YEAR(FROM_UNIXTIME(date_from))', $params['year']);
			//$this->db->or_where('YEAR(FROM_UNIXTIME(er.date_to))', $params['month']);
			$this->db->where('event_year', $params['year']);
    	}
    	
    	// Is a status set?
    	if( !empty($params['status']) )
    	{
    		// If it's all, then show whatever the status
    		if($params['status'] != 'all')
    		{
	    		// Otherwise, show only the specific status
    			$this->db->where('status', $params['status']);
    		}
    	}
    	// Nothing mentioned, show live only (general frontend stuff)
    	else
    	{
    		//$this->db->where('status', 'live');
    	}

    	// By default, dont show unpublished articles
    	if(!isset($params['show_future']) || (isset($params['show_future']) && $params['show_future'] == FALSE))
    	{
    		$this->db->where('IF(publish_on IS NOT NULL, publish_on <= '.now().', (publish_on IS NULL OR publish_on = \'0\'))', '', FALSE);
    	}

		if (!isset($params['year']))
		{
			$this->db->where('event_year', '2015');
		}

       	// Limit the results based on 1 number or 2 (2nd is offset)
       	if(isset($params['limit']) && is_array($params['limit'])) $this->db->limit($params['limit'][0], $params['limit'][1]);
       	elseif(isset($params['limit'])) $this->db->limit($params['limit']);
    	
    	return $this->get_all($params);
    }

	function get_next($id)
	{
		if (!$id)
		{
			$next = $this->db
				->where('status', 'live')
				->limit(1)
				->order_by('id', 'asc')
				->get('ctd')->row();
		}
		else
		{
			$next = $this->db
				->where('id >', $id)
				->where('status', 'live')
				->limit(1)
				->order_by('id', 'asc')
				->get('ctd')->row();
		}
		return $next;
	}

	function get_next_prev($id, $country='')
	{
		if (!$id) return false;
		$prev = $this->db
			->where('id <', $id)
			->where('status', 'live')
			->where('country', $country)
			->limit(1)
			->order_by('id', 'desc')
			->get('ctd')->row();
		$next = $this->db
			->where('id >', $id)
			->where('status', 'live')
			->where('country', $country)
			->limit(1)
			->order_by('id', 'asc')
			->get('ctd')->row();
		if ($prev OR $next)
			return array('prev'=>$prev, 'next'=>$next);
		return false;
	}


	function count_by($params = array())
    {
    	if(!empty($params['slug']))
    	{
    		$this->db->where('slug', $params['slug']);
    	}
    	
    	if(!empty($params['id']) && $params['id'] > 0)
    	{
    		$this->db->where('id !=', $params['id']);
    	}
    	
    	if(!empty($params['month']))
    	{
    		$this->db->where('MONTH(FROM_UNIXTIME(date_from))', $params['month']);
    	}
    	
    	if(!empty($params['year']))
    	{
    		$this->db->where('YEAR(FROM_UNIXTIME(date_from))', $params['year']);
    	}
    	
    	// Is a status set?
    	if( !empty($params['status']) )
    	{
    		// If it's all, then show whatever the status
    		if($params['status'] != 'all')
    		{
	    		// Otherwise, show only the specific status
    			$this->db->where('status', $params['status']);
    		}
    	}
    	// Nothing mentioned, show live only (general frontend stuff)
    	else
    	{
    		$this->db->where('status', 'live');
    	}

		return $this->db->count_all_results('ctd');
    }

    function insert($input = array(), $image = array())
    {
    	if(isset($input['date_from_day']) && isset($input['date_from_month']) && isset($input['date_from_year']) )
    	{
    		$input['date_from'] = mktime($input['date_from_hour'], $input['date_from_minute'], 0, $input['date_from_month'], $input['date_from_day'], $input['date_from_year']);
    		
    		unset($input['date_from_hour'], $input['date_from_minute'], $input['date_from_month'], $input['date_from_day'], $input['date_from_year']);
    	}

    	if(isset($input['date_to_day']) && isset($input['date_to_month']) && isset($input['date_to_year']) )
    	{
    		$input['date_to'] = mktime($input['date_to_hour'], $input['date_to_minute'], 0, $input['date_to_month'], $input['date_to_day'], $input['date_to_year']);
    		
    		unset($input['date_to_hour'], $input['date_to_minute'], $input['date_to_month'], $input['date_to_day'], $input['date_to_year']);
    	}
    	
   		$this->load->helper('date');
   		$input['created_on'] = now();
   		$input['updated_on'] = now();

//		$this->db->insert("default_ctd",$input);
//	    $last_id = $this->db->insert_id();
//	    return $last_id;
	
    	return parent::insert($input);
    }
    
    function update($id, $input, $image=array())
    {
    	$this->load->helper('date');
            
    	$input['updated_on'] = now();

    	if(isset($input['date_from_day']) && isset($input['date_from_month']) && isset($input['date_from_year']) )
    	{
    		$input['date_from'] = mktime($input['date_from_hour'], $input['date_from_minute'], 0, $input['date_from_month'], $input['date_from_day'], $input['date_from_year']);
    		
    		unset($input['date_from_hour'], $input['date_from_minute'], $input['date_from_month'], $input['date_from_day'], $input['date_from_year']);
    	}

    	if(isset($input['date_to_day']) && isset($input['date_to_month']) && isset($input['date_to_year']) )
    	{
    		$input['date_to'] = mktime($input['date_to_hour'], $input['date_to_minute'], 0, $input['date_to_month'], $input['date_to_day'], $input['date_to_year']);
    		
    		unset($input['date_to_hour'], $input['date_to_minute'], $input['date_to_month'], $input['date_to_day'], $input['date_to_year']);
    	}

    	return $this->db->update('ctd', $input, "id = $id");
    }
    
    function publish($id = 0)
    {
    	return parent::update($id, array('status' => 'live'));
    }

	function save_rating($input = array())
	{
		return $this->db->insert('event_ratings', $input);
	}

	function attending($input)
	{
		return $this->db->insert('ctd_attendance', $input);
	}

	function get_total_attendance($ctd_id)
	{
		$this->db->where('ctd_id', $ctd_id);
		return $this->db->count_all_results('ctd_attendance');
	}

    // -- Archive ---------------------------------------------
    
    function get_archive_months()
    {
    	$this->load->helper('date');
    	
    	$this->db->select('UNIX_TIMESTAMP(DATE_FORMAT(FROM_UNIXTIME(t1.date_from), "%Y-%m-02")) AS `date`', FALSE);
    	$this->db->distinct();
		$this->db->select('(SELECT count(id) FROM ctd t2 
							WHERE MONTH(FROM_UNIXTIME(t1.date_from)) = MONTH(FROM_UNIXTIME(t2.date_from)) 
								AND YEAR(FROM_UNIXTIME(t1.date_from)) = YEAR(FROM_UNIXTIME(t2.date_from)) 
								AND status = "live"
								AND date_from <= '.now().'
						   ) as article_count');
		
		$this->db->where('status', 'live');
    	$this->db->where('date_from <=', now());
		$this->db->having('article_count >', 0);
		$this->db->order_by('t1.date_from DESC');
		$query = $this->db->get('ctd t1');

		return $query->result();
    }

    // DIRTY frontend functions. Move to views
    function get_ctd_fragment($params = array())
    {
    	$this->load->helper('date');
    	
    	$this->db->where('status', 'live');
    	$this->db->where('created_on <=', now());
       	
       	$string = '';
        $this->db->order_by('created_on', 'DESC');
        $this->db->limit(5);
        $query = $this->db->get('ctd');
        if ($query->num_rows() > 0) {
        		$this->load->helper('text');
            foreach ($query->result() as $ctd) {
                $string .= '<p>' . anchor('ctd/' . $ctd->slug, $ctd->title) . '<br />' . strip_tags($ctd->intro). '</p>';
            }
        }
        return $string ;
    }

	function get_id_from_slug($slug)
	{
		$this->db->where('slug', $slug);
		$ret = $this->db->get('ctd')->row();
		return $ret->id;
	}

	function check_email_attendant($params=array())
	{
		$this->db->where('email', $params['email']);
		if (!empty($params['ctd_id']))
			$this->db->where('ctd_id', $params['ctd_id']);
		return $this->db->get('ctd_attendance')->result();
	}

	function user_attending($id=0, $eid)
	{
		$this->db->where('user_id', $id);
		$this->db->where('event_id', $eid);
		return $this->db->count_all_results('event_registrations');
	}

	function check_slug($slug = '', $id=0)
    {
		//return parent::count_by('slug', $slug) == 0;
		//return $this->count_by(array('slug'=>$slug, 'id'=>$id, 'status'=>'all')) == 0;
		$params['slug'] = $slug;
		$params['id !='] = (int) $id;

		return parent::count_by($params) == 0;
    }

	function get_countries()
	{
		return $this->db->order_by('countryName')->get('countries')->result();
	}

}

?>
