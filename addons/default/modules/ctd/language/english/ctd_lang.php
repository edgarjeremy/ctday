<?php
// titles
$lang['ctd_event_title'] 						= 'Events'; 
$lang['cat_list_title'] 						= 'Categories'; 
$lang['ctd_att_list_title'] 					= 'Attendance List'; 

$lang['ctd_attendance_title'] 					= 'Attendance';

// labels
$lang['ctd_event_venue'] 						= 'Event Venue'; 
$lang['ctd_event_venue_address'] 				= 'Venue Address'; 

$lang['ctd_country_label'] 						= 'Country'; 
$lang['ctd_posted_label'] 						= 'Posted'; 
$lang['ctd_posted_label_alt']					= 'Posted at';
$lang['ctd_category_label'] 					= 'Category';
$lang['ctd_post_label'] 						= 'Event Titles';
$lang['ctd_date_label'] 						= 'Date';
$lang['ctd_expired_on_label'] 					= 'Expired On';
$lang['ctd_publish_date_label'] 				= 'Date to Publish';
$lang['ctd_publish_date_msg'] 					= 'Event will not be displayed before this date.';
$lang['ctd_no_attachment'] 						= 'This event does not have an image attachment.';
$lang['ctd_image_label'] 						= 'Image';
$lang['ctd_attachment_label'] 					= 'Upload image';
$lang['ctd_delete_image_label'] 				= 'Delete image';
$lang['ctd_add_more_label'] 					= 'Add more file';

$lang['ctd_attachment_name_label'] 				= 'Partner\'s Name';
$lang['ctd_attachment_desc_label'] 				= 'Description';

$lang['ctd_partner_deleted']					= 'Partner logo "%s" was removed';
$lang['ctd_partner_not_deleted']				= 'Partner logo "%s" could not be removed';

$lang['ctd_attendance_title'] 					= 'Attendance';


$lang['ctd_notes_label'] 						= 'Notes';
$lang['ctd_from_label'] 						= 'From';
$lang['ctd_to_label'] 							= 'To';
$lang['ctd_date_at']							= 'at'; 
$lang['ctd_time_label'] 						= 'Time';
$lang['ctd_hour_label'] 						= 'Hour';
$lang['ctd_minute_label'] 						= 'Minute';


$lang['ctd_status_label'] 						= 'Status';
$lang['ctd_draft_label'] 						= 'Draft';
$lang['ctd_live_label'] 						= 'Live';
$lang['ctd_inreview_label'] 					= 'In Review';
$lang['ctd_disapprove_label'] 					= 'Disapprove';
$lang['ctd_deleted_label'] 						= 'Deleted';

$lang['ctd_slideshow_label'] 					= 'Slideshow Image';
$lang['ctd_slideshow_msg'] 						= 'Image will be resized to %spx X %spx. Uploading new image will overwrite old one.';

$lang['ctd_actions_label'] 						= 'Actions';
$lang['ctd_view_label'] 						= 'View';
$lang['ctd_preview_label'] 						= 'Preview';
$lang['ctd_edit_label'] 						= 'Edit';
$lang['ctd_delete_label'] 						= 'Delete';
$lang['ctd_reg_label'] 							= 'Reg';
$lang['ctd_cancel_label'] 						= 'Cancel';

$lang['ctd_registration_label'] 				= 'Accept Registration';
$lang['ctd_registration_not_open'] 				= 'Sorry, but this event currently does not accept registration.';

$lang['ctd_registration_email_label'] 			= 'Send registration info to';
$lang['ctd_send_registration_to_msg']			= 'Enter email address(es) to receive registration info. Separate addresses with a comma. If empty, email will be sent to the Contact E-mail in Settings.';
$lang['ctd_yes_label'] 							= 'Yes';
$lang['ctd_no_label'] 							= 'No';
$lang['ctd_content_label'] 						= 'Content';
$lang['ctd_options_label'] 						= 'Options';
$lang['ctd_title_label'] 						= 'Title';
$lang['ctd_body_label'] 						= 'Body';
$lang['ctd_location_label'] 					= 'Location';
$lang['ctd_organiser_label'] 					= 'Organiser';
$lang['ctd_slug_label'] 						= 'URL';
$lang['ctd_intro_label'] 						= 'Introduction';
$lang['ctd_no_category_select_label'] 			= '-- None --';
$lang['ctd_new_category_label'] 				= 'Add a category';
$lang['ctd_subscribe_to_rss_label'] 			= 'Subscribe to RSS';
$lang['ctd_all_articles_label'] 				= 'All events';
$lang['ctd_articles_of_category_suffix'] 		= ' events';
$lang['ctd_rss_name_suffix'] 					= ' Events';
$lang['ctd_rss_category_suffix'] 				= ' Events';
$lang['ctd_author_name_label'] 					= 'Author name';
$lang['ctd_timezone_label'] 					= 'Timezone';
$lang['ctd_visibility_label'] 					= 'Visibility';
$lang['ctd_visibility_password_label'] 			= 'Password';
$lang['ctd_visibility_groups_label'] 			= 'Groups';
$lang['ctd_next_label'] 						= 'Next';

$lang['ctd_file_name_label'] 					= 'File';
$lang['ctd_partner_name_label'] 				= 'Partner\'s Name';
$lang['ctd_description_label'] 					= 'Description';

$lang['ctd_file_created_label'] 				= 'Date Uploaded';

$lang['ctd_display_grid'] 						= 'Display as Grid';
$lang['ctd_display_list'] 						= 'Display as List';


$lang['ctd_participant_limit_label'] 			= 'Participants Limit';
$lang['ctd_participant_limit_msg'] 				= 'Enter the number of participants you want to limit the confirmation';

$lang['ctd_notification_label'] 				= 'Notify on Changes';
$lang['ctd_notification_msg'] 					= 'All participants will receive notification via email when dates or description of the event is changed';
$lang['ctd_when_label'] 						= 'When';
$lang['ctd_reminder_label'] 					= 'Reminder';
$lang['ctd_notify_on_change_label'] 			= 'Notify on Change';

$lang['ctd_geolocation_label']					= 'Geolocation';
$lang['ctd_get_address_label']					= 'Get Address';
$lang['ctd_close_map_label']					= 'Close map';
$lang['ctd_do_not_display_map']					= 'Do not display map';
$lang['ctd_latitude_label']						= 'Latitude';
$lang['ctd_longitude_label']					= 'Longitude';

$lang['ctd_partners_label']						= 'Partners';
$lang['ctd_partner_logos_label']				= 'Partners Logos';
$lang['ctd_partner_logos_label']				= 'Partners Logos';


$lang['ctd_meta_data_label']					= 'Metadata';
$lang['ctd_meta_title_label']					= 'Meta Title';
$lang['ctd_meta_desc_label']					= 'Meta Description';
$lang['ctd_meta_keywords_label']				= 'Meta Keywords';

$lang['ctd_statistics_title']					= 'Statistics';
$lang['ctd_comments_title']						= 'Comments';

$lang['ctd_number_of_views_label']				= 'Number of Views';
$lang['ctd_ratings_label']						= 'Ratings';

$lang['ctd_all_day_label'] 						= 'All Day';

$lang['ctd_tags_label'] 						= 'Tags';
$lang['ctd_tags_text_msg'] 						= 'Autosuggests, press TAB or , (comma) for multiple tags.';

$lang['ctd_maybes_label'] 						= 'Maybe';
$lang['ctd_attendees_label'] 					= 'Attending';
$lang['ctd_attendees_past_label'] 				= 'Attended';
$lang['ctd_cancel_attendance_label'] 			= 'Cancel My Registration';
$lang['ctd_not_attending_label'] 				= 'Not Attending';
$lang['ctd_not_attending_past_label'] 			= 'Did Not Attend';


$lang['ctd_read_more_label'] 					= 'More&nbsp;&raquo;';
$lang['ctd_read_less_label'] 					= '&laquo;&nbsp;Less';

// event registrations
$lang['event_reg_title']						= 'Event Registrations';
$lang['event_reg_back']							= 'Back to event list';
$lang['ctd_register'] 							= 'Register to this Event';
$lang['ctd_reg_name_label']						= 'Full Name';
$lang['ctd_reg_organisation_label']				= 'Organisation';
$lang['ctd_reg_email_label']					= 'Email Address';
$lang['ctd_reg_phone_label']					= 'Phone';
$lang['ctd_reg_cellphone_label']				= 'Cellphone';
$lang['ctd_reg_address_label']					= 'Complete Address';
$lang['ctd_reg_delete_success'] 				= 'The registration by "%s" has been deleted.';
$lang['ctd_reg_mass_delete_success'] 			= 'The registrations by "%s" have been deleted.';
$lang['ctd_reg_delete_error'] 					= 'No registrations were deleted.';
$lang['ctd_reg_thanks'] 						= 'Thank you %s, for your registration.';
$lang['ctd_reg_mail_sent'] 						= 'An email has been sent to %s with the data you entered.';
$lang['ctd_reg_email_message'] 					= 'Thank you for your registration, below are your registration information to "%s".';
$lang['ctd_you_are_registering_to'] 			= 'You are registering to the event';
$lang['ctd_button_register'] 					= 'Register';
$lang['delete_reg_label'] 						= 'Delete';
$lang['export_label'] 							= 'Export to CSV';
$lang['ctd_register_prefilled'] 				= 'The registration form has been prefilled. Please revise the information and click Register.';
$lang['ctd_reg_full'] 							= 'Sorry, but we have reached the participant limit of %s';


// titles
$lang['ctd_create_title'] 						= 'Add event';
$lang['ctd_edit_title'] 						= 'Edit event "%s"';
$lang['ctd_archive_title'] 						= 'Archive';
$lang['ctd_articles_title'] 					= 'Events';
$lang['ctd_rss_articles_title'] 				= 'Happening events at %s';
$lang['ctd_ctd_title'] 							= 'Events';
$lang['ctd_list_title'] 						= 'Events';
$lang['ctd_no_registrations'] 					= 'There are no registrations.';


// messages
$lang['ctd_no_events_in_month'] 				= 'No events available for this month.';

$lang['ctd_no_attendance'] 						= 'No attendance has been recorded for this event.';


$lang['ctd_thanks_notattending'] 				= 'Sorry for hearing you are not going to attend, but thanks anyway.';

$lang['ctd_people_already_registered'] 			= ' people already registered.';

$lang['ctd_no_events'] 							= 'There are no events at the moment.';
$lang['ctd_no_ctd_in_month'] 					= 'There are no events for this month.';
$lang['ctd_subscripe_to_rss_desc'] 				= 'Get articles straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['ctd_currently_no_articles'] 				= 'There are no events at the moment.';
$lang['ctd_retrieve_error']		 				= 'An error occurred while trying to retrieve event data or event does not exist.';
$lang['ctd_article_add_success'] 				= 'The event "%s" was added.';
$lang['ctd_article_add_error'] 					= 'An error occured.';
$lang['ctd_edit_success'] 						= 'The event "%s" was updated.';
$lang['ctd_edit_error'] 						= 'An error occurred.';
$lang['ctd_publish_success'] 					= 'The event "%s" has been published.';
$lang['ctd_mass_publish_success'] 				= 'The events "%s" have been published.';
$lang['ctd_publish_error'] 						= 'No events were published.';
$lang['ctd_delete_success'] 					= 'The event "%s" has been deleted.';
$lang['ctd_mass_delete_success'] 				= 'The events "%s" have been deleted.';
$lang['ctd_delete_error'] 						= 'No events were deleted.';
$lang['ctd_already_exist_error'] 				= 'An event with this URL already exists.';

$lang['ctd_db_attend_error'] 					= 'An error occurred processing your attendance.';
$lang['ctd_thanks_for_attending'] 				= 'Thank you, we hope to see you at the event.';

$lang['ctd_email_is_required_error']			= 'A valid email address is required';

$lang['ctd_already_registered_error'] 			= 'The email address you supplied has been used.';

$lang['ctd_cancel_attending_msg'] 				= 'You have the option of telling us why you cancelled. Please fill in the form below.';

$lang['ctd_twitter_posted'] 					= 'Posted "%s" %s';

$lang['ctd_no_tags_msg'] 						= 'No tags currently defined.';



?>