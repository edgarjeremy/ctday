<?php defined('BASEPATH') or exit('No direct script access allowed');

$lang['ctd.role_add_contest']			= 'Add Event';
$lang['ctd.role_edit_contest']			= 'Edit Event';
$lang['ctd.role_delete_contest']			= 'Delete Event';
