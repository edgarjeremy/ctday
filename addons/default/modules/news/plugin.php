<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * News Plugin
 *
 * Create lists of posts
 * 
 * @author		PyroCMS Dev Team
 * @package		PyroCMS\Core\Modules\News\Plugins
 */
class Plugin_News extends Plugin
{
	public $version = '2.0';
	public $name = array(
		'en' => 'News',
	);
	public $description = array(
		'en' => 'Gets news articles',
	);

	/**
	 * News List
	 *
	 * Creates a list of news posts
	 *
	 * Usage:
	 * {{ news:posts order-by="title" limit="5" }}
	 *		<h2>{{ title }}</h2>
	 *		<p> {{ body }} </p>
	 * {{ /news:posts }}
	 *
	 * @param	array
	 * @return	array
	 */
	public function posts()
	{
		$wordlimit	= (int)$this->attribute('word-limit');
		$charlimit 	= (int)$this->attribute('char-limit'); 
		$limit		= $this->attribute('limit');
		$category	= $this->attribute('category');
		$notcategory= $this->attribute('notcategory');
		$order_by 	= $this->attribute('order-by', 'created_on');
		$order_dir	= $this->attribute('order-dir', 'DESC');
		$f_year	= $this->attribute('f_year');
		$offset		= $this->attribute('offset');
		$break		= $this->attribute('break');
		$socmed		= $this->attribute('share', 0);
		$first_image= $this->attribute('first-image', 0);
		$nointro	= $this->attribute('no-intro', 0);
		//$headline	= $this->attribute('headline', 1);

		$mostread	= $this->attribute('mostread', NULL);

		if ($mostread)
		{
			$order_by = 'viewed';
			$order_dir = 'desc';
			$yr = date('Y');
			$this->db->where("FROM_UNIXTIME(created_on, '%Y') = $yr");
			//$this->db->where('( viewed > 50 AND viewed < 200 )');
		}

		$this->load->helper('filename');

		if ($category)
		{
			$categories = explode('|', $category);
			$category = array_shift($categories);

			$this->db->where('news_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);

			foreach($categories as $category)
			{
				$this->db->or_where('news_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);
			}
		}

		if ($notcategory)
		{
			$notcategories = explode('|', $notcategory);
			$notcategory = array_shift($notcategories);

			$this->db->where('news_categories.' . (is_numeric($category) ? 'id' : 'slug') . ' !=', $notcategory);

			foreach($notcategories as $ncategory)
			{
				$this->db->where('news_categories.' . (is_numeric($ncategory) ? 'id' : 'slug') . ' != ', $ncategory);
			}
		}

		if ($limit)
		{
			$this->db->limit($limit, $offset ? $offset : 0);
		}

		$posts = $this->db
			->select('news.*')
			->select('news_categories.title as category_title, news_categories.slug as category_slug')
			->select('p.display_name as author_name')
			->where(array('status' => 'live', 'news_year'=>$f_year))
			//->where('created_on <=', now())
			->join('news_categories', 'news.category_id = news_categories.id', 'left')
			->join('profiles p', 'news.author_id = p.user_id', 'left')
			->order_by('news.' . $order_by, $order_dir)
			->get('news')
			->result();

		if ($first_image)
		{
			$index = 1;
		}
		
		$f_datenow = date('Y');
		
		foreach ($posts as &$post)
		{
			$intro = '';
			$post->url = site_url('news/'.date('Y', $post->created_on).'/'.date('m', $post->created_on).'/'.$post->slug);
			if ($post->lead) $post->intro = $post->lead;
			$post->intro = strip_tags($post->intro);
			$post->short_intro = word_limiter($post->intro, 35);
			if ($nointro)
			{
				$post->intro = $post->short_intro = NULL;
			}
			if ($charlimit)
			{
				$post->intro = character_limiter($post->intro, $charlimit);
/*
echo "<pre>\n";
echo "intro: " . $post->intro . "\n";
echo "char: " . strlen($post->intro) . "\n";
echo "\n --------------------------------- \n";
echo "</pre>\n";
*/
			}
			else
			{
				if ($wordlimit === -1)
					$post->intro = $post->intro;
				elseif ($wordlimit > 0)
					$post->intro = word_limiter($post->intro, $wordlimit);
				else
					$post->intro = word_limiter($post->intro, 35);
			}
//			$post->intro = $wordlimit === 0 ? $post->intro : $wordlimit > 0 ? word_limiter($post->intro, $wordlimit) : word_limiter($post->intro, 40);
			$post->date = date('d/m/Y H:i', $post->created_on);
			$post->date_only = date('d F Y', $post->created_on);
			$post->hours = date('H:i', $post->created_on);

			$post->thumbnail = $post->attachment ? UPLOAD_PATH . 'news/'. thumbnail($post->attachment) : '';

/*
			if (!empty($index) && $index == 1)
			{
				$post->thumbnail = $post->attachment ? UPLOAD_PATH . 'news/'. ($post->attachment) : '';
				$post->first_class = $this->attribute('first-class', '');
				$index++;
			}
			else
			{
				$post->thumbnail = $post->first_class =  $post->intro = NULL;
			}
*/

/*
echo "<pre>\n";
print_r($post);
echo "</pre>\n";
*/
/*
			if ($first_image)
			{
				if (!empty($index) && $index <> 1)
					$post->thumbnail = NULL;
				else
				{
					if(!empty($index) && $index == 1)
						$post->thumbnail = $post->attachment ? UPLOAD_PATH . 'news/'. ($post->attachment) : '';
					else
						$post->thumbnail = $post->attachment ? UPLOAD_PATH . 'news/'. thumbnail($post->attachment) : '';
				}
				$index++;
			}
			else
			{
				$post->thumbnail = $post->attachment ? UPLOAD_PATH . 'news/'. ($post->attachment) : '';
			}
*/
			$post->socmed = $socmed;
		}

		return $posts;
	}

	public function headlines()
	{
		$wordlimit	= (int)$this->attribute('word-limit');
		$charlimit 	= (int)$this->attribute('char-limit'); 
		$limit		= $this->attribute('limit', 10);
		$category	= $this->attribute('category');
		$notcategory= $this->attribute('notcategory');
		$order_by 	= $this->attribute('order-by', 'created_on');
		$order_dir	= $this->attribute('order-dir', 'DESC');
		$offset		= $this->attribute('offset');
		$break		= $this->attribute('break');
		$headline	= $this->attribute('headlines', 1);

		$mostread	= $this->attribute('mostread', NULL);

		if ($mostread)
		{
			$order_by = 'viewed';
			$order_dir = 'desc';
		}

		$this->load->helper('filename');

		if ($headline)
		{
			$this->db->where('headline', 1);
		}

		if ($category)
		{
			$array_where = array();
			$categories = explode('|', $category);
			//$category = array_shift($categories);

			//$this->db->where('news_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);

			foreach($categories as $category)
			{
				$array_where[] = $this->db->dbprefix('news_categories') . '.' . (is_numeric($category) ? 'id' : 'slug') . ' = \'' . $category . '\'';
				//$this->db->or_where('news_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);
			}

			$where = ' ( ' . implode(' OR ', $array_where) . ' ) ';
			$this->db->where($where);
		}

		if ($notcategory)
		{
			$notcategories = explode('|', $notcategory);
			$notcategory = array_shift($notcategories);

			$this->db->where('news_categories.' . (is_numeric($category) ? 'id' : 'slug') . ' !=', $notcategory);

			foreach($notcategories as $ncategory)
			{
				$this->db->or_where('news_categories.' . (is_numeric($ncategory) ? 'id' : 'slug') . ' != ', $ncategory);
			}
		}

		$posts = $this->db
			->select('news.*')
			->select('news_categories.title as category_title, news_categories.slug as category_slug')
			->select('p.display_name as author_name')
			->join('news_categories', 'news.category_id = news_categories.id', 'left')
			->join('profiles p', 'news.author_id = p.user_id', 'left')
			->where('status', 'live')
			->where('created_on <=', now())
			->order_by('news.' . $order_by, $order_dir)
			->limit($limit, $offset ? $offset : 0)
			->get('news')
			->result();

		$index = 1;
		foreach ($posts as &$post)
		{
			if ($index === 1)
			{
				$post->active = 'active';
				$index = 2;
			}
			else
				$post->active = '';
			$post->url = site_url('news/'.date('Y', $post->created_on).'/'.date('m', $post->created_on).'/'.$post->slug);
			if ($post->lead) $post->intro = $post->lead;
			$post->short_intro = word_limiter($post->intro, 35);
			if ($charlimit)
			{
				$post->intro = character_limiter($post->intro, $charlimit);
			}
			else
			{
				if ($wordlimit === -1)
					$post->intro = $post->intro;
				elseif ($wordlimit > 0)
					$post->intro = word_limiter($post->intro, $wordlimit);
				else
					$post->intro = word_limiter($post->intro, 35);
			}
//			$post->intro = $wordlimit === 0 ? $post->intro : $wordlimit > 0 ? word_limiter($post->intro, $wordlimit) : word_limiter($post->intro, 40);
			$post->date = date('d/m/Y H:i', $post->created_on);
			$post->thumbnail = $post->attachment ? UPLOAD_PATH . 'news/'. thumbnail($post->attachment) : '';
			$post->image = $post->attachment ? UPLOAD_PATH . 'news/'. ($post->attachment) : '';
		}
		
		return $posts;
	}

	public function homeslider()
	{
		$wordlimit	= (int)$this->attribute('word-limit');
		$charlimit 	= (int)$this->attribute('char-limit'); 
		$limit		= $this->attribute('limit', 10);
		$category	= $this->attribute('category');
		$order_by 	= $this->attribute('order-by', 'created_on');
		$order_dir	= $this->attribute('order-dir', 'DESC');
		$offset		= $this->attribute('offset');
		$break		= $this->attribute('break');
		$column		= $this->attribute('column');
		$mostread	= $this->attribute('mostread', NULL);
		$notcategory= $this->attribute('notcategory');
		$class		= ' class="'.$this->attribute('li-class').'" ';
		$dateclass 	= $this->attribute('dateclass', 'icon-calendar-empty');

		if ($mostread)
		{
			$order_by = 'viewed';
			$order_dir = 'desc';
		}

		if ($notcategory)
		{
			$notcategories = explode('|', $notcategory);
			$notcategory = array_shift($notcategories);

			$this->db->where('news_categories.' . (is_numeric($category) ? 'id' : 'slug') . ' !=', $notcategory);

			foreach($notcategories as $ncategory)
			{
				$this->db->or_where('news_categories.' . (is_numeric($ncategory) ? 'id' : 'slug') . ' != ', $ncategory);
			}
		}


		$this->load->helper('filename');

		$result = array();
		$posts = $this->db
			->select('news.*')
			->select('news_categories.title as category_title, news_categories.slug as category_slug')
			->select('p.display_name as author_name')
			->where('status', 'live')
			//->where('created_on <=', now())
			->join('news_categories', 'news.category_id = news_categories.id', 'left')
			->join('profiles p', 'news.author_id = p.user_id', 'left')
			->order_by('news.' . $order_by, $order_dir)
			->limit($limit, $offset ? $offset : 0)
			->get('news')
			->result();

		$retdiv = '';

		if (!empty($posts))
		{
			$idx = 1;
			foreach($posts as $r)
			{
				if ($r->lead) $r->intro = $r->lead;
				if ($idx == 1)
				{
					$retdiv .= '
							<li '.$class.'>
								<!-- <div class="li-items"> -->
					';
				}
				if ($idx <> ($break+1))
				{
					$retdiv .= '
									<div class="li-items">';
					$r->url = site_url('news/'.date('Y', $r->created_on).'/'.date('m', $r->created_on).'/'.$r->slug);
					$retdiv .= '<h5><small><i class="'.$dateclass.'"></i> '.date('d/m/Y', $r->created_on) . ' <i class="fa fa-clock-o"></i> '.date('H:i', $r->created_on).'</small><br /><a href="'.$r->url.'">'.$r->title.'</a></h5>'.PHP_EOL;
					$retdiv .= '
									</div>
					';
					$idx++;
				}
				if ($idx == ($break+1))
				{
					$retdiv .= '
								<!-- </div> -->
							</li>
					';
					$idx = 1;
				}
			} // endforeach

			if ($idx <> 1)
			{
					$retdiv .= '
								<!-- </div> -->
							</li>
					';
			}
			return $retdiv;
		}

	}

	/**
	 * Categories
	 *
	 * Creates a list of news categories
	 *
	 * Usage:
	 * {{ news:categories order-by="title" limit="5" }}
	 *		<a href="{{ url }}" class="{{ slug }}">{{ title }}</a>
	 * {{ /news:categories }}
	 *
	 * @param	array
	 * @return	array
	 */
	public function categories()
	{
		$limit		= $this->attribute('limit');
		$order_by 	= $this->attribute('order-by', 'title');
		$order_dir	= $this->attribute('order-dir', 'ASC');

		if ($limit) $this->db->limit($limit);

		$categories = $this->db
			->select('title, slug')
			->order_by($order_by, $order_dir)
			->get('news_categories')
			->result();

		foreach ($categories as &$category)
		{
			$category->url = site_url('news/category/'.$category->slug);
		}
		
		return $categories;
	}

	/**
	 * Count Posts By Column
	 *
	 * Usage:
	 * {{ news:count_posts author_id="1" }}
	 *
	 * The attribute name is the database column and 
	 * the attribute value is the where value
	 */
	public function count_posts()
	{
		$wheres = $this->attributes();
		unset($wheres['parse_params']);

		// make sure they provided a where clause
		if (count($wheres) == 0) return FALSE;

		$category	= $this->attribute('category');
		if ($category)
		{
			$categories = explode('|', $category);
			$category = array_shift($categories);

			$this->db->where('news_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);

			foreach($categories as $category)
			{
				$this->db->or_where('news_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);
			}
			unset($wheres['category']);
		}

		foreach ($wheres AS $column => $value)
		{
			$this->db->where($column, $value);
		}

		return $this->db
			->select('news.*')
			->select('news_categories.title as category_title, news_categories.slug as category_slug')
			->where('status', 'live')
			->where('created_on <=', now())
			->join('news_categories', 'news.category_id = news_categories.id', 'left')
			->count_all_results('news');
	}
}

/* End of file plugin.php */