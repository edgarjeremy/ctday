<?php defined('BASEPATH') OR exit('No direct script access allowed');

class News_m extends MY_Model
{
	protected $_table = 'news';

	public function get_all($params=array())
	{
		$this->db
			->select('news.*, news_categories.title AS category_title, news_categories.slug AS category_slug, profiles.display_name')
			->join('news_categories', 'news.category_id = news_categories.id', 'left')
			->join('profiles', 'profiles.user_id = news.author_id', 'left');

		if (!empty($params['order_by']) && !empty($params['order_dir']))
			$this->db->order_by($params['order_by'], $params['order_dir']);
		else
			$this->db->order_by('created_on', 'DESC');

		return $this->db->get('news')->result();
	}

	public function get($id)
	{
		$ret = $this->db
			->select('news.*, profiles.display_name')
			->join('profiles', 'profiles.user_id = news.author_id', 'left')
			->where(array('news.id' => $id))
			->get('news')
			->row();
		if (!empty($ret))
		{
			if (empty($ret->optional_images))
			{
				$ret->optional_images = $this->getuploadedimages($ret->id);
			}
			else
			{
				$ret->optional_images = json_decode($ret->optional_images);
				$ret->old_format = TRUE;
			}
			$ret->related = serialize($ret->related_news) OR NULL;
			return $ret;
		}

		return false;
	}
	
	public function get_by($key = null, $value = null)
	{
		$this->db
			->select('news.*, profiles.display_name')
			->join('profiles', 'profiles.user_id = news.author_id', 'left');
			
		if (is_array($key))
		{
			$this->db->where($key);
		}
		else
		{
			$this->db->where($key, $value);
		}

		$ret = $this->db->get($this->_table)->row();

		if (!empty($ret))
		{
			if (empty($ret->optional_images))
			{
				$ret->optional_images = $this->getuploadedimages($ret->id);
			}
			else
			{
				$ret->optional_images = json_decode($ret->optional_images);
				$ret->old_format = TRUE;
			}

			$ret->related = serialize($ret->related_news) OR NULL;

			return $ret;
		}
	}

	public function get_next_previous($d=0, $id=0)
	{
		$prev = $this->db->select('slug,created_on,title')->where('id !=', $id)->where('status', 'live')->where('created_on >=', $d)->limit(1)->order_by('created_on', 'asc')->get('news')->row();
		$next = $this->db->select('slug,created_on,title')->where('id !=', $id)->where('status', 'live')->where('created_on <=', $d)->limit(1)->order_by('created_on', 'desc')->get('news')->row();
		return array($prev,$next);
	}

	public function get_many_by($params = array())
	{
		$this->load->helper('date');

		if (!empty($params['category']))
		{
			if (is_numeric($params['category']))
				$this->db->where('news_categories.id', $params['category']);
			else
				$this->db->where('news_categories.slug', $params['category']);
		}

		if (!empty($params['id']))
		{
			$this->db->where_in('news.id', $params['id']);
		}

		if (!empty($params['notid']))
		{
			$this->db->where('news.id !=', $params['notid']);
		}

		if (!empty($params['month']))
		{
			$this->db->where('MONTH(FROM_UNIXTIME(created_on))', $params['month']);
		}

		if(!empty($params['year']))
    	{
    		$this->db->where('news_year', $params['year']);
    	}

		if (!isset($params['year']))
		{
			$this->db->where('news_year', date("Y"));
		}
		
		if ( ! empty($params['keywords']))
		{
			$this->db->join('keywords_applied', 'keywords_applied.hash=news.keywords', 'left');
			$this->db->join('keywords', 'keywords.id=keywords_applied.keyword_id', 'left');
			$this->db
				->like('news.title', trim($params['keywords']))
				->or_like('profiles.display_name', trim($params['keywords']));
		}

		// Is a status set?
		if (!empty($params['status']))
		{
			// If it's all, then show whatever the status
			if ($params['status'] != 'all')
			{
				// Otherwise, show only the specific status
				$this->db->where('status', $params['status']);
			}
		}

		// Nothing mentioned, show live only (general frontend stuff)
		else
		{
			$this->db->where('status', 'live');
		}

		if (!empty($params['slugnot']))
		{
			$this->db->where("news.slug !=", $params['slugnot']);
		}

		// By default, dont show future posts
		if (!isset($params['show_future']) || (isset($params['show_future']) && $params['show_future'] == FALSE))
		{
			$this->db->where('created_on <=', now());
		}

		// Limit the results based on 1 number or 2 (2nd is offset)
		if (isset($params['limit']) && is_array($params['limit']))
			$this->db->limit($params['limit'][0], $params['limit'][1]);
		elseif (isset($params['limit']))
			$this->db->limit($params['limit']);

		return $this->get_all($params);
	}

	public function news_dropdown($params=array())
	{
		$this->db->select('id,title');
		if (!empty($params['id']))
		{
			$this->db->where_in('id', $params['id']);
		}

		if (!empty($params['notid']))
		{
			$this->db->where('id !=', $params['notid']);
		}
		$news = $this->db->get('news');
		foreach($news as $n)
		{
			$rn[$n->id] = $n->title;
		}
		return form_dropdown('related_news', $rn, (!empty($params['id'])?$params['id']:'' ));
	}
	
	public function count_tagged_by($tag, $params)
	{
		return $this->db->select('*')
			->from('news')
			->join('keywords_applied', 'keywords_applied.hash = news.keywords')
			->join('keywords', 'keywords.id = keywords_applied.keyword_id')
			->where('keywords.name', str_replace('-', ' ', $tag))
			->where($params)
			->count_all_results();
	}
	
	public function get_tagged_by($tag, $params)
	{
		if (!empty($params['order_by']) && !empty($params['order_dir']))
		{
			$this->db->order_by($params['order_by'], $params['order_dir']);
			unset($params['order_by'], $params['order_dir']);
		}
		else
			$this->db->order_by('created_on', 'DESC');

		return $this->db->select('news.*, news.title title, news.slug slug, news_categories.title category_title, news_categories.slug category_slug, profiles.display_name')
			->from('news')
			->join('keywords_applied', 'keywords_applied.hash = news.keywords')
			->join('keywords', 'keywords.id = keywords_applied.keyword_id')
			->join('news_categories', 'news_categories.id = news.category_id', 'left')
			->join('profiles', 'profiles.user_id = news.author_id', 'left')
			->where('keywords.name', str_replace('-', ' ', $tag))
			->where($params)
			->get()
			->result();
	}

	public function get_many_by_keywords($key, $params)
	{
		if (is_array($key))
		{
			$this->db->where_in('keywords.name', $key);

		}
		else
		{
			$this->db->where('keywords.name', $key);
		}

		if (!empty($params['slugnot']))
		{
			$this->db->where("news.slug !=", $params['slugnot']);
			unset($params['slugnot']);
		}

		if (!empty($params['order_by']) && !empty($params['order_dir']))
		{
			$this->db->order_by($params['order_by'], $params['order_dir']);
			unset($params['order_by'], $params['order_dir']);
		}
		else
			$this->db->order_by('created_on', 'DESC');

		if (isset($params['limit']) && is_array($params['limit']))
			$this->db->limit($params['limit'][0], $params['limit'][1]);
		elseif (isset($params['limit']))
			$this->db->limit($params['limit']);
		unset($params['limit']);

		return $this->db->select('news.*, news.title title, news_categories.title category_title, news_categories.slug category_slug, profiles.display_name')
			->distinct('news.slug slug')
			->from('news')
			->join('keywords_applied', 'keywords_applied.hash = news.keywords')
			->join('keywords', 'keywords.id = keywords_applied.keyword_id')
			->join('news_categories', 'news_categories.id = news.category_id', 'left')
			->join('profiles', 'profiles.user_id = news.author_id', 'left')
			->where($params)
			->get()
			->result();
	}

	public function count_by($params = array())
	{
		$this->db->join('news_categories', 'news.category_id = news_categories.id', 'left')
			// we need the display name joined so we can get an accurate count when searching
			->join('profiles', 'profiles.user_id = news.author_id', 'left');

		if (!empty($params['category']))
		{
			if (is_numeric($params['category']))
				$this->db->where('news_categories.id', $params['category']);
			else
				$this->db->where('news_categories.slug', $params['category']);
		}

		if (!empty($params['notid']))
		{
			$this->db->where('news.id !=', $params['notid']);
		}


		if (!empty($params['month']))
		{
			$this->db->where('MONTH(FROM_UNIXTIME(created_on))', $params['month']);
		}

		if(!empty($params['year']))
    	{
    		$this->db->where('news_year', $params['year']);
    	}

		if (!isset($params['year']))
		{
			$this->db->where('news_year', date("Y"));
		}

		if ( ! empty($params['keywords']))
		{
			$this->db
				->like('news.title', trim($params['keywords']))
				->or_like('profiles.display_name', trim($params['keywords']));
		}

		// Is a status set?
		if (!empty($params['status']))
		{
			// If it's all, then show whatever the status
			if ($params['status'] != 'all')
			{
				// Otherwise, show only the specific status
				$this->db->where('status', $params['status']);
			}
		}

		// Nothing mentioned, show live only (general frontend stuff)
		else
		{
			$this->db->where('status', 'live');
		}

		return $this->db->count_all_results('news');
	}

/*
	public function insert($input=array(), $skip_validation = false)
	{
return 1001;
//		return parent::insert($input);
	}
*/

//	public function update($id, $input)
	public function update($primary_value, $data, $skip_validation = false)
	{
		$data['updated_on'] = now();
        if(!empty($data['status']) && $data['status'] == "live" and $data['preview_hash'] !='') $data['preview_hash'] = '';
		return parent::update($primary_value, $data);
	}

	public function insert_temp($input=array())
	{
		if ($this->db->insert('newstemp', $input))
			return $this->db->insert_id();
		else
			return false;
	}

	public function update_temp($id=0, $input=array())
	{
		if (!$id) return false;
		if ($this->db->where('id', $id)->update('newstemp', $input))
			return $id;
		else
			return false;
	}

	public function delete_temp($id=0)
	{
		if (!$id) return true;
		$this->db->where('id', $id)->delete('newstemp');
	}

	public function get_temp($id)
	{
		$ret = $this->db
			->where(array('newstemp.id' => $id))
			->get('newstemp')
			->row();
		if (!empty($ret))
		{
			if (empty($ret->optional_images))
			{
				$ret->optional_images = $this->getuploadedimages($ret->id);
			}
			else
			{
				$ret->optional_images = json_decode($ret->optional_images);
				$ret->old_format = TRUE;
			}
			$ret->related = serialize($ret->related_news) OR NULL;
			return $ret;
		}

		return false;
	}

	public function publish($id = 0)
	{
		return parent::update($id, array('status' => 'live','preview_hash'=>''));
	}

	// -- Archive ---------------------------------------------

	public function get_archive_months()
	{
		$this->db->select('UNIX_TIMESTAMP(DATE_FORMAT(FROM_UNIXTIME(t1.created_on), "%Y-%m-02")) AS `date`', FALSE);
		$this->db->from('news t1');
		$this->db->distinct();
		$this->db->select('(SELECT count(id) FROM ' . $this->db->dbprefix('news') . ' t2
							WHERE MONTH(FROM_UNIXTIME(t1.created_on)) = MONTH(FROM_UNIXTIME(t2.created_on))
								AND YEAR(FROM_UNIXTIME(t1.created_on)) = YEAR(FROM_UNIXTIME(t2.created_on))
								AND status = "live"
								AND created_on <= ' . now() . '
						   ) as post_count');

		$this->db->where('status', 'live');
		$this->db->where('created_on <=', now());
		$this->db->having('post_count >', 0);
		$this->db->order_by('t1.created_on DESC');
		$query = $this->db->get();

		return $query->result();
	}

	// DIRTY frontend functions. Move to views
	public function get_news_fragment($params = array())
	{
		$this->load->helper('date');

		$this->db->where('status', 'live');
		$this->db->where('created_on <=', now());

		$string = '';
		$this->db->order_by('created_on', 'DESC');
		$this->db->limit(5);
		$query = $this->db->get('news');
		if ($query->num_rows() > 0)
		{
			$this->load->helper('text');
			foreach ($query->result() as $news)
			{
				$string .= '<p>' . anchor('news/' . date('Y/m') . '/' . $news->slug, $news->title) . '<br />' . strip_tags($news->intro) . '</p>';
			}
		}
		return $string;
	}

	public function check_exists($field, $value = '', $id = 0)
	{
		if (is_array($field))
		{
			$params = $field;
			$id = $value;
		}
		else
		{
			$params[$field] = $value;
		}
		$params['id !='] = (int) $id;

		return parent::count_by($params) == 0;
	}

	/**
	 * Insert new slide image
	 *
	 * @param array array of data to input
	 * @return int|FALSE ID of inserted data on success, or FALSE on failure
	 */
	function insertimg($input)
	{
		//return TRUE;
		if ($this->db->insert('news_images', $input))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Update news slide database
	 *
	 * @param int ID of image database to update
	 * @param array Input array
	 * @return bool
	 */
	function updateimg($id, $input)
	{
		$this->db->where('id', $id);
		return $this->db->update('news_images', $input);
	}

	/**
	 * Retrieve slide images
	 * @param int ID of news
	 * @return array Array of data objects
	 */
	function getuploadedimages($id=0)
	{
		if ($id)
			$this->db->where('news_id', $id);
		else
		{
			$this->db->where('news_id', '0');
			$this->db->where('user_id', $this->current_user->id);
		}
		$this->db->order_by('position');
		return $this->db->get('news_images')->result();
	}

	/**
	 * Retrieve single image
	 *
	 * @param int|array ID of image(s) to retrieve
	 * @return objects Table row data object
	 */
	function getsingleimage($id)
	{
		if (empty($id)) return FALSE;
		$this->db->where('id', $id);
		return $this->db->get('news_images')->row();
	}

	/**
	 * Delete image from database
	 *
	 * @param int ID of the image to delete
	 * @return bool
	 */
	function imgdelete($imgid=0)
	{
		if (!$imgid) return FALSE;
		$this->db->where('id', $imgid);
		return $this->db->delete('news_images');
	}

	/**
	 * Update image order
	 *
	 * @param int ID of image to reorder
	 * @param array array of info to input
	 * @return bool
	 */
	function update_imgorder($id=0, $input)
	{
		if (!$id) return FALSE;
		$this->db->where('id', $id);
		return $this->db->update('news_images', $input);
	}

	/**
	 * Searches news posts based on supplied data array
	 * @param $data array
	 * @return array
	 */
	public function search($data = array())
	{
		if (array_key_exists('category_id', $data))
		{
			$this->db->where('category_id', $data['category_id']);
		}

		if (array_key_exists('status', $data))
		{
			$this->db->where('status', $data['status']);
		}

		if (array_key_exists('keywords', $data))
		{
			$matches = array();
			if (strstr($data['keywords'], '%'))
			{
				preg_match_all('/%.*?%/i', $data['keywords'], $matches);
			}

			if (!empty($matches[0]))
			{
				foreach ($matches[0] as $match)
				{
					$phrases[] = str_replace('%', '', $match);
				}
			}
			else
			{
				$temp_phrases = explode(' ', $data['keywords']);
				foreach ($temp_phrases as $phrase)
				{
					$phrases[] = str_replace('%', '', $phrase);
				}
			}

			$counter = 0;
			foreach ($phrases as $phrase)
			{
				if ($counter == 0)
				{
					$this->db->like('news.title', $phrase);
				}
				else
				{
					$this->db->or_like('news.title', $phrase);
				}

				$this->db->or_like('news.body', $phrase);
				$this->db->or_like('news.intro', $phrase);
				$this->db->or_like('profiles.display_name', $phrase);
				$counter++;
			}
		}
		return $this->get_all();
	}

	function get_year(){
		$this->db->select('news.*')->order_by( 'news_year','desc' );
		return $this->db->get('news')->result();
	}
	
}