<div class="row-fluid">

	<div class="span12">

<?php if (isset($category->title)): ?>
	<h2 id="page_title"><?php echo $category->title; ?></h2>

<?php elseif (isset($tag)): ?>
	<h2 id="page_title"><?php echo lang('news:tagged_label').': '.$tag; ?></h2>

<?php else: ?>
	<h2 id="page_title">{{ template:title }}</h2>

<?php endif; ?>

<?php if ( ! empty($news)): ?>

	<?php foreach ($news as $post): ?>

			<div>
				<!-- <div class="span1"></div> -->
				<div class="span4" style="padding-top: 15px;">
				<?php if ($post->attachment): ?>
						<img class="img-polaroid pull-right" src="<?php echo site_url(UPLOAD_PATH . 'news/'.($post->attachment)) ?>" />
				<?php endif; ?>
				</div>
				<div class="span8">
					<h4 class="media-heading"><?php echo  anchor('news/'.date('Y/m/', $post->created_on).$post->slug, $post->title); ?><br />
						<small><i class="icon-calendar-empty"></i> <?php echo date('d-m-Y H:i', $post->created_on); ?>
						<?php if ($post->category_slug): ?>
							<i class="icon-reorder"></i> <?php //echo lang('news:category_label');?>
							<span><?php echo anchor('news/category/'.$post->category_slug, $post->category_title);?></span>
							&nbsp;
						<?php endif; ?>
						<?php if ($post->keywords): ?>
							<i class="icon-tags"></i> <?php //echo lang('news:tagged_label');?>
							<span>
							<?php foreach ($post->keywords as $keyword): ?>
								<?php echo anchor('news/tagged/'.$keyword->name, $keyword->name, 'class="keyword"') ?>
							<?php endforeach; ?>
							</span>
							&nbsp;
						<?php endif; ?>
					</small></h4>

					<?php echo $post->intro; ?>

				</div>

					<div class="icon-socmed pull-right">
						<a href="http://www.facebook.com/share.php?s=100&p[url]=<?php echo site_url().uri_string() ?>&p[title]=<?php echo $post->title ?>&p[images][0]=<?php echo site_url(UPLOAD_PATH . 'news/'.($post->attachment)) ?>&p[summary]=<?php echo strip_tags($post->short_intro) ?>"><i class="icon-facebook-sign"></i></a>
						<a href="https://plus.google.com/share?url=<?php echo site_url().uri_string() ?>"><i class="icon-google-plus"></i></a>
						<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url().uri_string() ?>&title=<?php echo $post->title ?>&summary=<?php echo strip_tags($post->intro) ?>&source=HarianTangerang"><i class="icon-linkedin-sign"></i></a>
						<a href="https://twitter.com/share?text=<?php echo character_limiter(strip_tags($post->intro),90) ?>&url=<?php echo site_url().uri_string() ?>&via=hariantangerang"><i class="icon-twitter-sign"></i></a>
					</div>

			<div class="clearfix"></div>
			<div class="divider"></div>

			</div>

	<?php endforeach; ?>

	<?php echo $pagination['links']; ?>

	<div class="clearfix"><br /></div>
<?php else: ?>

	<div class="alert"><?php echo lang('news:currently_no_posts');?></div>

<?php endif; ?>


	</div><!-- /span12 -->

</div> <!-- /row-fluid -->

			<div class="clearfix"></div>
