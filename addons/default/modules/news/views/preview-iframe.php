
		<div class="news-header">
			<?php if (!empty($post->nextprev[0])): ?>
				<?php
					$u = site_url() . 'news/'. date('Y', $post->nextprev[0]->created_on) . '/' . date('m', $post->nextprev[0]->created_on) . '/' . $post->nextprev[0]->slug;
					$t = $post->nextprev[0]->title;
				?>
				<a href="<?php echo $u ?>" title="<?php echo $t ? $t : 'Previous article' ?>" class="news-previous"><i class="fa fa-chevron-left"></i></a>
			<?php endif; ?>

			<?php if (!empty($post->nextprev[1])): ?>
				<?php
					$u = site_url() . 'news/'. date('Y', $post->nextprev[1]->created_on) . '/' . date('m', $post->nextprev[1]->created_on) . '/' . $post->nextprev[1]->slug;
					$t = $post->nextprev[1]->title;
				?>
				<a href="<?php echo $u ?>" title="<?php echo $t ? $t : 'Next article' ?>" class="news-next"><i class="fa fa-chevron-right"></i></a>
			<?php endif; ?>

			<div class="news-header-image-container" >
			<?php if (!empty($post->attachment)): ?>
				<img src="<?php echo site_url(UPLOAD_PATH . 'news/'.($post->attachment)) ?>">
			<?php else: ?>
				{{ theme:image file="backgrounds/bg-header.jpg" }}
			<?php endif; ?>
			</div>

			<div class="news-header-title">
				<div class="container">
					<h2><?php echo $post->title; ?></h2>
					<?php echo date('d M Y', $created_on) ?>
				</div>
			</div>
		</div>

<div class="news-container"><div class="container"><div class="row">

	<div class="col-sm-12 col-md-12 center-block">


		<div class="n-content">

			<?php if (!empty($post->lead)): ?>
				<div class="lead"><?php echo $post->lead; ?></div>
			<?php else: ?>
				<div class="lead"><?php echo strip_tags($post->intro); ?></div>
			<?php endif; ?>

			<?php if ($post->news_quote): ?>
				<p></p>
				<div class="col-md-3 pull-left">
					<div class="clearfix"></div>
					<blockquote><small><?php echo strip_tags($post->news_quote); ?></small></blockquote>
					<div class="clearfix"></div>
				</div>
			<?php endif; ?>

			<div><?php echo $post->body; ?></div>

			<?php if ($post->external_url): ?>
				<div>
					<small>Source: <a target="_blank" href="<?php echo $post->external_url; ?>"><?php echo $post->external_url; ?></a></small>
				</div>
			<?php endif; ?>

			<div class="clearfix"><br /></div>

			<?php //echo $author; ?>

			<div class="clearfix"><br /></div>

		</div>	<!-- //n-content -->

	</div>

</div></div></div>

	<div class="clearfix"></div>


<?php if (Settings::get('enable_comments')): ?>

<div class="container"><div class="row"><div class="col-md-12">

	<div id="comments">

		<div id="existing-comments">
			<h3><?php echo lang('comments:title') ?></h3>
			<?php echo $this->comments->display() ?>

		</div>

		<?php if ($form_display): ?>

			<?php //echo $this->comments->form() ?>

		<?php else: ?>
		<?php echo sprintf(lang('news:disabled_after'), strtolower(lang('global:duration:'.str_replace(' ', '-', $post->comments_enabled)))) ?>
		<?php endif ?>

	</div>

</div></div></div>

<div class="clearfix"></div>

<?php endif ?>

	<?php if (!empty($related_news)): ?>

		<div class="relatednews-container">
			<div class="container">

				<div class="row">

					<div class="col-md-12">

						<h3>Related Articles</h3>

						<div class="whoweare ">
							<div class="whoweare-container">
							<?php foreach($related_news as $on): ?>
								<div class="col-md-4">
									<span class="k-img-caption" >
										<a <?php echo $on->external_url ? 'target="_blank"' : '' ?> href="<?php echo $on->external_url; ?>">
										<?php if ($on->attachment): ?>
											<img src="<?php echo site_url(UPLOAD_PATH.'news/'.thumbnail($on->attachment)) ?>" />
										<?php else: ?>
											<img src="{{theme:image_path file='backgrounds/bg-news-default.gif'}}" />
										<?php endif; ?>
										<span class="caption"><?php echo $on->title?></span></a>
									</span>
									<div class="relatednews-lead"><?php echo $on->lead?></div>
								</div>
							<?php endforeach; ?>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

	<?php endif; ?>


	<div class="clearfix"></div>
