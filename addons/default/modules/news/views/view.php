<div class="row-fluid">

	<div class="span12">

		<h3><?php echo $post->title; ?></h3>


		<div class="span11">
			<h4 class="media-heading">
				<small><i class="icon-calendar-empty"></i> <?php echo date('d/m/Y', $post->created_on); ?>, <?php echo date('H:i', $post->created_on); ?>
				<?php if (!empty($post->category->slug)): ?>
					<i class="icon-reorder"></i> <?php //echo lang('news:category_label');?>
					<span><?php echo anchor('news/category/'.$post->category->slug, $post->category->title);?></span>
					&nbsp;
				<?php endif; ?>
				<?php if ($post->keywords): ?>
					<i class="icon-tags"></i> <?php //echo lang('news:tagged_label');?>
					<span class="keywords">
					<?php foreach ($post->keywords as $keyword): ?>
						<?php echo anchor('news/tagged/'.$keyword->name, $keyword->name . ' &nbsp;', 'class="keyword"') ?>
					<?php endforeach; ?>
					</span>
					&nbsp;
				<?php endif; ?>
			</small></h4>
		</div>
		<div class="clearfix"></div>

		<div class="span11 n-content">
		<?php if ($post->attachment): ?>
				<img class="img-polaroid" src="<?php echo site_url(UPLOAD_PATH . 'news/'.($post->attachment)) ?>" />
				<?php if ($post->credits): ?>
				<span class="img-caption"><?php echo $post->credits; ?></span>
				<?php endif; ?>
		<?php endif; ?>
		<div class="clearfix"><br /></div>

			<div><?php echo $post->intro; ?></div><br />

			<div><?php echo $post->body; ?></div>

<!-- 			<div><?php echo str_ireplace(".<br />\n", ".<br /><br />", $post->body); ?></div> -->

		</div>

		<div class="clearfix"></div><br />
		<div class="icon-socmed pull-left">
			<a href="http://www.facebook.com/share.php?s=100&p[url]=<?php echo site_url().uri_string() ?>&p[title]=<?php echo $post->title ?>&p[images][0]=<?php echo site_url(UPLOAD_PATH . 'news/'.($post->attachment)) ?>&p[summary]=<?php echo strip_tags($post->intro) ?>"><i class="icon-facebook-sign"></i></a>
			<a href="https://plus.google.com/share?url=<?php echo site_url().uri_string() ?>"><i class="icon-google-plus"></i></a>
			<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url().uri_string() ?>&title=<?php echo $post->title ?>&summary=<?php echo strip_tags($post->intro) ?>&source=HarianTangerang"><i class="icon-linkedin-sign"></i></a>
			<a href="https://twitter.com/share?text=<?php echo character_limiter(strip_tags($post->intro),90) ?>&url=<?php echo site_url().uri_string() ?>&via=hariantangerang"><i class="icon-twitter-sign"></i></a>
		</div>
		<p class="pull-right"><small class="muted">Dibaca <b><?php echo number_format($post->viewed); ?></b> kali</small></p>

	</div>

	<div class="clearfix"></div>

<?php if (Settings::get('enable_comments')): ?>

	<div class="divider"></div>

	<div id="comments">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/all.js#xfbml=1&appId=213853518765674";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


		<div id="existing-comments">
			<h3><?php echo lang('comments:title') ?></h3>
			<?php echo $this->comments->display() ?>

			<?php if (site_url() <> 'http://hariantangerang.local/'): ?>
				<div class="fb-comments" data-href="<?php echo site_url(uri_string()); ?>" data-colorscheme="light" style="width:100%;"></div>
			<?php endif; ?>

		</div>

		<?php if ($form_display): ?>

			<?php //echo $this->comments->form() ?>

			{{ disqus:show shortname="hariantangerang" id="<?php echo $post->id ?>" url="<?php echo site_url() . uri_string(); ?>" title="<?php echo $post->title ?>" dev="off" }}

		<?php else: ?>
		<?php echo sprintf(lang('news:disabled_after'), strtolower(lang('global:duration:'.str_replace(' ', '-', $post->comments_enabled)))) ?>
		<?php endif ?>

	</div>

	<?php if (!empty($related_news)): ?>
		<div class="divider"></div>

		<div class="clearfix"></div>

		<h3>Berita Terkait</h3>

		<ul class="thumbnails">
		<?php foreach($related_news as $on): ?>
			<li class="span3">
				<a href="<?php echo site_url('news/'.date('Y/m/', $on->created_on).'/'.$on->slug) ?>" class="thumbnail">
					<div class="img-container">XYZ
						<?php if ($on->attachment): ?>
							<img src="<?php echo site_url(UPLOAD_PATH.'news/'.thumbnail($on->attachment)) ?>" />
						<?php else: ?>
							<img src="{{theme:image file="backgrounds/bg-news-default.gif"}}" />
						<?php endif; ?>
					</div>
					<h5><?php echo $on->title?></h5>
				</a>
			</li>
		<?php endforeach; ?>
		</ul>
	<?php endif; ?>


<?php endif ?>

</div>

