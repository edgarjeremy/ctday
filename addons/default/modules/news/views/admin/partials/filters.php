<style>
.filter-am{margin:0px auto!important;}
.keyword-input-text{vertical-align:top; padding-top:5px;}
.btn-cancel{vertical-align: top;padding-top: 9px;}
</style>
<fieldset id="filters">
	<legend><?php echo lang('global:filters'); ?></legend>
	<?php echo form_open(''); ?>
	<?php echo form_hidden('f_module', $module_details['slug']); ?>
		<ul>
			<li class="filter-am">
        		<?php echo form_dropdown('f_year', array(''=>'Year')+$filter_year); ?>
    		</li>
			<li class="filter-am">
        		<?php echo form_dropdown('f_status', array('' => 'Status', 'draft'=>lang('news:draft_label'), 'live'=>lang('news:live_label'))); ?>
    		</li>
			<li class="filter-am">
        		<?php echo form_dropdown('f_category', array('' => 'Categories') + $categories); ?>
    		</li>
			<li class="filter-am keyword-input-text">
        		<?php echo form_input('f_keywords'); ?>
    		</li>
			<li class="filter-am btn-cancel">
        		<?php echo anchor(current_url() . '#', lang('buttons:cancel'), 'class="cancel"'); ?>
    		</li>
		</ul>
	<?php echo form_close(); ?>
</fieldset>