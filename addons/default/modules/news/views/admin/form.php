<section class="title">
<?php if ($this->method == 'create'): ?>
	<h4><?php echo lang('news:create_title'); ?></h4>
<?php else: ?>
	<h4><?php echo sprintf(lang('news:edit_title'), $post->title); ?></h4>
<?php endif; ?>
</section>

<section class="item">
<div class="content">

<?php
	$attributes = array('id' => 'createNews');
?>
<?php echo form_open_multipart(uri_string(), $attributes); ?>

	<!-- Content tab -->
	<div class="form_inputs" id="news-content-tab">

		<fieldset>
	
		<ul>
			<li>
					<label for="title"><?php echo lang('global:title'); ?> <span>*</span><small><?php echo lang('news:title_description_msg'); ?></small></label>
					<div class="input"><?php echo form_input('title', htmlspecialchars_decode($post->title), 'maxlength="250" style="width:350px;" id="title" '); ?>
			</li>

			<li>
					<label for="slug"><?php echo lang('global:slug'); ?> <span>*</span> <small><?php echo lang('news:slug_description_msg'); ?></small></label>
					<div class="input"><?php echo form_input('slug', $post->slug, 'maxlength="250"  style="width:350px;"'); ?></div>
			</li>

			<li>
				<label for="external_url"><?php echo lang('news:external_url_label'); ?> </label>
				<div class="input"><?php echo form_input('external_url', htmlspecialchars_decode($post->external_url), 'id="external_url" style="width:550px;"'); ?></div>
			</li>
			
			<li>
				<div style="display:block;width:30%;float:left;">
					<label for="status"><?php echo lang('news:status_label'); ?></label>
					<div class="input"><?php echo form_dropdown('status', array('draft' => lang('news:draft_label'), 'live' => lang('news:live_label')), $post->status) ?></div>
				</div>
				<div style="display:none;width:30%;float:left;">
					<label for="headline"><?php echo lang('news:headline_label'); ?></label>
					<div class="input"><?php echo form_dropdown('headline', array('1' => lang('news:yes_label'),'0' => lang('news:no_label')), $post->status) ?></div>
				</div>
				<div class="clearfix"></div>
			</li>

			<li class="category">
				<div style="display:block;width:30%;float:left;">
					<label for="category_id"><?php echo lang('news:category_label'); ?></label>
					<div class="input">
					<?php echo form_dropdown('category_id', array(lang('news:no_category_select_label')) + $categories, @$post->category_id) ?><br />
						[ <?php echo anchor('admin/news/categories/create', lang('news:new_category_label'), 'class="" target="_blank"'); ?> ]
					</div>
				</div>
				<?php if ($this->settings->news_to_twitter): ?>
					<div style="display:block;width:30%;float:left;">
						<label for="status"><?php echo lang('news:send_to_twitter_label'); ?></label>
						<div class="input"><?php echo form_dropdown('tweet', array('n' => lang('news:no_label'), 'y' => lang('news:yes_label')), $post->tweet ? $post->tweet : 'y') ?></div>
					</div>
				<?php endif; ?>
				<div class="clearfix"></div>
			</li>

			<li class="date-meta">
				<label><?php echo lang('news:date_label'); ?></label>
				<div class="input">
					<?php echo form_input('created_on', date('Y-m-d', $post->created_on), 'maxlength="10" id="datepicker" class="text width-20"'); ?> &nbsp;
					<div class="datetime_input">
					<?php echo form_dropdown('created_on_hour', $hours, date('H', $post->created_on), 'class="time-meta"') ?> : 
					<?php echo form_dropdown('created_on_minute', $minutes, date('i', ltrim($post->created_on, '0')), 'class="time-meta"') ?>
					</div>				
				</div>
			</li>
			
		</ul>
        <?php echo form_hidden('preview_hash',$post->preview_hash)?>
        <?php echo form_hidden('type','wysiwyg-advanced')?>
        <?php echo form_hidden('comments_enabled','no')?>
        <?php echo form_hidden('body','')?>
		</fieldset>
		
	</div>

<div class="buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel'))); ?>
</div>

<input type="hidden" id="tn_id" name="tn_id" value="" />
<input type="hidden" id="tn_image" name="tn_image" value="" />
<input type="hidden" id="cu_image" name="cu_image" value="<?php echo $post->attachment ?>" />

<?php echo form_close(); ?>

			<div class="hidden">

				<!-- slideshow uploader -->
				<div id="files-uploader">
					<div class="files-uploader-browser">
						<form action="<?php echo site_url('/admin/news/upload/'.(!empty($post->id)?$post->id:'')) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" name="frmUpload">
							<label for="userfile" class="upload">Upload Files</label>
							<input type="file" name="userfile" value="" class="no-uniform" multiple="multiple" />
							<div style="visibility:hidden;">
							<input type="text" name="slidetitle">
							<textarea rows="3" cols="30" name="slidedescription"></textarea>
							</div>
							<?php if (!empty($post->id)): ?>
								<input type="hidden" id="news_id" name="news_id" value="<?php echo $post->id; ?>" />
							<?php endif; ?>
						</form>
						<ul id="files-uploader-queue" class="ui-corner-all"></ul>
		
					</div>
					<div class="buttons align-right padding-top">
						<a href="#" title="" class="button start-upload">Upload</a>
						<a href="#" title="" class="button cancel-upload">Cancel</a>
					</div>
				</div>

			</div>

</div>
</section>

<div style="display:none;">
	<div id="wait">
		<div id="output"></div>
	</div>
</div>

<script>
	var news_id = jQuery('#news_id').val();
	var startbtn = 'Start';
	var cancelbtn = 'Cancel';

$(document).ready(function(){

	$('#title_color').click(function(e){
		e.preventDefault();
		if ($('#title_size_choices').css('display')=='block') $('#title_size_choices').toggle();
		$('#title_color_choices').toggle();
	});
	$('#title_size').click(function(e){
		e.preventDefault();
		if ($('#title_color_choices').css('display')=='block') $('#title_color_choices').toggle();
		$('#title_size_choices').toggle();
	});

	$('#related_news').click(function(){
		var v = $('#related_news_ids').val();
		var url = $(this).attr('href');
		$(this).colorbox({
			width: "75%",
			height: "80%",
			href: url,
			data: {ids: v, csrf_hash_name: $.cookie(pyro.csrf_cookie_name)},
			onClosed: function(){
				var v2 = $('#related_news_ids').val();

				$.post( "/admin/news/getrelatednews", { ids: v2, csrf_hash_name: $.cookie(pyro.csrf_cookie_name) })
				  .done(function( data ) {
				    $('#rn').html(data);
				});
			}
		});
	});

	$('.deletern').live('click', function(e){
		e.preventDefault();
		if (confirm("Are you sure you want to remove it?\nThis can not be undone."))
		{
			CheckUnCheckForms($(this).attr('href'), false);
			$(this).parent().fadeOut();
		}
	});

	$('.checkuncheck').live('click', function(e){
		var c = $(this).is(':checked');
		var i = $(this).val();
		if (i)
		{
			var tf = $('#related_news_ids').val();
			var file_array = tf.split(',');
			if (file_array.length >= 3 && c)
			{
				alert('You already reached the maximum articles to link..');
				return false;
			}
			else
				CheckUnCheckForms(i, c);
		}
	});

    var options = { 
        target:        '#cboxLoadedContent', // target element(s) to be updated with server response 
        //beforeSubmit:  showCbox,  // pre-submit callback 
        success:       showCbox,  // post-submit callback 
 
        // other available options: 
        url:       "<?php echo site_url(); ?>admin/news/xpreview", // override for form's 'action' attribute 
        dataType: "json",
        type: "post"
    }; 

	$('.preview').click(function(e){
		e.preventDefault();
		$('#createNews').ajaxSubmit(options);
	});

});

function showCbox(data)
{
	if (data.status == 'ok')
	{
		$.colorbox({
			href: data.redirect,
			iframe: true,
			width: "97%",
			height: "90%",
			scrolling: true,
			fixed: true
		});
		if (data.tn_id != undefined)
			$('#tn_id').val(data.tn_id);
		if (data.attachment != undefined)
			$('#tn_image').val(data.attachment);
	}
	else
	{
		alert('Sorry, unable to complete your request.');
	}
}

function in_array (needle, haystack, argStrict) {
	var key = '', strict = !!argStrict;

	if (strict) {
		for (key in haystack) {
			if (haystack[key] === needle) {
				return true;
			}
		}
	} else {
		for (key in haystack) {
			if (haystack[key] == needle) {
				return true;
			}
		}
	}

	return false;
}
function CheckUnCheckForms(v, c) {
	var tf = $('#related_news_ids').val();
	var file_array = tf.split(',');
	var new_array;
	if (c) {
		if (in_array(v, file_array)) {
			return true;
		} else {
			$('#related_news_ids').val( (tf ? tf+',' : '') + v );
		}
	} else {
		var i=0;
		for (i; i < file_array.length; i++) {
			if (file_array[i] == v) {
				//$('#debug').append('f['+i+']: '+file_array[i]);
				file_array.splice(i,1);
			}
		}
		new_array = file_array.join(',');
		$('#related_news_ids').val(new_array);
	}
}

function displayOptionalImages(d)
{
	if (d.is(":checked"))
	{
		var v = d.attr('rel');

		$('.page_format_column').not('#'+v).each(function(){
			$('input type[=file]', this).each(function(){
				$(this).replaceWith($(this).val('').clone(true));
			});
		});

		if (v === '1_column')
			$('#2_column').hide();
		else
			$('#2_column').show();

	}
}
</script>

<style>
.opt-images {
	width: 100%;
	border: 1px solid #ccc;
	padding: 10px;
	margin: 0 0 15px 15px;
}
.font-util {
	z-index: 100;
	background: #fff;
	padding: 10px;
	border-radius: 5px;
	border: 1px solid #ccc;
	position: absolute;
}
</style>