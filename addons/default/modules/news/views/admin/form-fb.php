<section class="title">
<?php if ($this->method == 'create'): ?>
	<h4><?php echo lang('news:create_title'); ?></h4>
<?php else: ?>
	<h4><?php echo sprintf(lang('news:edit_title'), $post->title); ?></h4>
<?php endif; ?>
</section>

<section class="item">
<div class="content">

<?php echo form_open_multipart(); ?>

	<div class="form_inputs" id="news-content-tab">

		<fieldset>
	
		<ul>
			<li>
				<label for="title"><?php echo lang('global:title'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('title', htmlspecialchars_decode($post->title), 'maxlength="100" id="title"'); ?></div>				
			</li>
		</ul>

		</fieldset>

	</div>

<div class="buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel'))); ?>
</div>

<?php echo form_close(); ?>

</div>
</section>

<style type="text/css">
#datepicker {min-width: 85px; width: 85px; max-width: 85px; }
.datetime_input {display: inline;}
.chzn-container-single {top: 9px;}

/*
form.crudli.date-meta div.selector {
    float: left;
    width: 30px;
    border: 1px solid blue;
}
form.crud li.date-meta div input#datepicker { width: 8em; }
form.crud li.date-meta div.selector { width: 5em !important; }
form.crud li.date-meta div.selector span { width: 1em; border: 1px solid red !important;}
form.crud li.date-meta label.time-meta { min-width: 4em; width:4em; }
form.crud .time-meta { min-width: 3em; width:3em; }
*/
</style>