<?php
/**
 * News admin view - displays newly uploaded images
 *
 * @package  	News
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<!-- newly uploaded images -->
<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * views/admin/files/contents.php
 *
 * Contents view file, used when displaying newly uploaded files or
 * attached file images
 *
 *
 */
?>
	<?php if (!empty($files)): ?>
        <?php foreach($files as $file): ?>
            <li class="ui-corner-all">
                <div class="actions">
				<?php
					echo anchor('admin/news/editimage/'.$file->id, lang('global:edit'), 'class="imgedit"' );
					if (group_has_role('news', 'delete_image')) 
					{
						echo ' | '. anchor('admin/news/imgdelete', lang('global:delete'), 'class="imgdel"' );
					}
				?>
                </div>
				<a title="<?php echo $file->caption; ?>" href="<?php echo '/'.UPLOAD_PATH .'news/images/'. $file->filename; ?>" rel="cb_0" class="modal">
	                <img title="<?php echo $file->caption; ?>" src="<?php echo '/'.UPLOAD_PATH.'news/images/'. thumbnail($file->filename);?>" alt="<?php echo $file->filename; ?>" />
				</a><br />
	            <div class="ui-corner-all" style="font-size: 0.85em">
	            	<?php if (!empty($file->caption)): ?>
	            		<b><?php echo $file->caption; ?></b><br />
	            	<?php endif; ?>
	            	<?php if (!empty($file->description)): ?>
	            		<?php echo $file->description; ?>
	            	<?php endif; ?>
	            </div>
	            <input type="hidden" name="img_slide_id[]" value="<?php echo $file->id; ?>" />
            </li>
        <?php endforeach; ?>

	<?php endif; ?>
