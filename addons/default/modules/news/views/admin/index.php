<section class="title">
	<h4><?php echo lang('news_posts_title'); ?></h4>
</section>

<section class="item">
<div class="content">

	<?php template_partial('filters'); ?>

	<?php echo form_open('admin/news/action'); ?>

		<div id="filter-stage">
			<?php template_partial('tables/posts'); ?>
		</div>

		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete', 'publish'))); ?>
		</div>

	<?php echo form_close(); ?>

</div>
</section>
