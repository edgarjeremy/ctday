<h1>Related News <small>Check the related news, max 3 items.</small></h1>
<div id="debug"></div>
<?php if ($news) : ?>
	<table border="0" class="table-list">
		<thead>
			<tr>
				<th width="20"></th>
				<th width="40%"><?php echo lang('news:post_label'); ?></th>
				<th class="collapse"><?php echo lang('news:category_label'); ?></th>
				<th class="collapse"><?php echo lang('news:date_label'); ?></th>
				<th class="collapse"><?php echo lang('news:status_label'); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="8">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ($news as $post) : ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $post->id, in_array($post->id, $ids), 'class="checkuncheck"'); ?></td>
					<td><?php echo $post->title; ?></td>
					<td class="collapse"><?php echo $post->category_title; ?></td>
					<td class="collapse"><?php echo date("d/m/Y H:i", $post->created_on); ?></td>
					<td><?php echo lang('news:'.$post->status.'_label'); ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<a href="#" onClick="$.colorbox.close(); return false;" class="btn blue">Done</a>
<?php else: ?>
	<div class="no_data"><?php echo lang('news:currently_no_posts'); ?></div>
<?php endif; ?>

