<section class="title">
<?php if ($this->method == 'create'): ?>
	<h4><?php echo lang('news:create_title'); ?></h4>
<?php else: ?>
	<h4><?php echo sprintf(lang('news:edit_title'), $post->title); ?></h4>
<?php endif; ?>
</section>

<section class="item">
<div class="content">

<?php echo form_open_multipart(); ?>

<div class="tabs">

	<ul class="tab-menu">
		<li><a href="#news-content-tab"><span><?php echo lang('news:content_label'); ?></span></a></li>
		<li><a href="#news-options-tab"><span><?php echo lang('news:options_label'); ?></span></a></li>
	</ul>
	
	<!-- Content tab -->
	<div class="form_inputs" id="news-content-tab">

		<fieldset>
	
		<ul>
			<li>
				<div style="display:block;width:40%;float:left;">
					<label for="title"><?php echo lang('global:title'); ?> <span>*</span> <small>Slug otomatis mengikuti judul</small></label>
					<div class="input"><?php echo form_input('title', htmlspecialchars_decode($post->title), 'maxlength="55" id="title" style="width:90%;"'); ?>
					<a href="#" id="title_color" title="Font color" class="btn blue small"><i class="icon icon-white icon-font"></i></a> <a href="#" title="Font size" id="title_size" class="btn blue small"><i class="icon icon-white icon-resize-vertical"></i></a></div><br />

					<!-- <label><a href="#" id="title_color" class="btn gray small"><i class="icon icon-font"></i> <?php echo lang('news:title_color_label'); ?> &raquo;</a></label> -->
					<div class="input font-util" id="title_color_choices" style="display:none; padding-top:10px;">
						<?php $checked = empty($post->title_color) ? 'checked="checked"' : ''; ?>
						<label style="display:inline-block;text-align:center;margin:auto;"> <span style="display:block;width:25px;height:25px;background-color: #536373;"></span><input name="title_color" type="radio" value="default" <?php echo $checked; ?> /></label> &nbsp;&nbsp;
						<?php foreach($title_colors as $tc): ?>
							<?php $checked = $post->title_color && $post->title_color == $tc ? 'checked="checked"' : '';  ?>
							<label style="display:inline-block;text-align:center;margin:auto;"> <span style="display:block;width:25px;height:25px;background-color: <?php echo $tc?>;"></span><input name="title_color" type="radio" value="<?php echo $tc?>" <?php echo $checked ?> /></label>
						<?php endforeach; ?>
					</div>
					<div class="input font-util" id="title_size_choices" style="display:none;">
						<?php $checked = empty($post->title_size) ? 'checked="checked"' : ''; ?>
							<label style="display:inline-block;text-align:center;margin:auto;"><input name="title_size" type="radio" value="34" <?php echo $checked; ?> /> 34px</label> <span style="font-size:34px;line-height:1;">Lorem ipsum</span><br />
						<?php foreach($title_sizes as $size): ?>
							<?php $checked = $post->title_size && $post->title_size == $size ? 'checked="checked"' : '';  ?>
							<label style="display:inline-block;text-align:center;margin:auto;"><input name="title_size" type="radio" value="<?php echo $size; ?>" <?php echo $checked; ?> /> <?php echo $size; ?>px</label> <span style="font-size:<?php echo $size; ?>px;line-height:1;">Lorem ipsum</span><br />
						<?php endforeach; ?>
					</div>

				</div>
				<div style="display:block;width:40%;float:left;">
					<label for="slug"><?php echo lang('global:slug'); ?> <span>*</span> <small>Mohon untuk tidak diubah secara manual.</small></label>
					<div class="input"><?php echo form_input('slug', $post->slug, 'maxlength="55"  style="width:90%;"'); ?></div><br />

<!--
					<label><a href="#" id="title_size" class="btn gray small"><i class="icon icon-resize-vertical"></i> <?php echo lang('news:title_size_label'); ?> &raquo;</a></label>
					<div class="input" id="title_size_choices" style="display:none;">
						<?php $checked = empty($post->title_size) ? 'checked="checked"' : ''; ?>
							<label style="display:inline-block;text-align:center;margin:auto;"><input name="title_size" type="radio" value="34" <?php echo $checked; ?> /> 34px</label> <span style="font-size:34px;line-height:1;">Lorem ipsum</span><br />
						<?php foreach($title_sizes as $size): ?>
							<?php $checked = $post->title_size && $post->title_size == $size ? 'checked="checked"' : '';  ?>
							<label style="display:inline-block;text-align:center;margin:auto;"><input name="title_size" type="radio" value="<?php echo $size; ?>" <?php echo $checked; ?> /> <?php echo $size; ?>px</label> <span style="font-size:<?php echo $size; ?>px;line-height:1;">Lorem ipsum</span><br />
						<?php endforeach; ?>
					</div>
-->

				</div>
				<div class="clearfix"></div>
			</li>

			<li>
				<label for="subtitle"><?php echo lang('news:subtitle_label'); ?> <span>*</span><small>Max.: 100</small></label>
				<div class="input"><?php echo form_input('subtitle', htmlspecialchars_decode($post->subtitle), 'maxlength="100" id="subtitle" style="width:550px;"'); ?></div>
			</li>
			
			<li>
				<div style="display:block;width:30%;float:left;">
					<label for="status"><?php echo lang('news:status_label'); ?></label>
					<div class="input"><?php echo form_dropdown('status', array('draft' => lang('news:draft_label'), 'live' => lang('news:live_label')), $post->status) ?></div>
				</div>
				<div style="display:block;width:30%;float:left;">
					<label for="headline"><?php echo lang('news:headline_label'); ?></label>
					<div class="input"><?php echo form_dropdown('headline', array('1' => lang('news:yes_label'),'0' => lang('news:no_label')), $post->status) ?></div>
				</div>
				<div class="clearfix"></div>
			</li>

			<li class="category">
				<div style="display:block;width:30%;float:left;">
					<label for="category_id"><?php echo lang('news:category_label'); ?></label>
					<div class="input">
					<?php echo form_dropdown('category_id', array(lang('news:no_category_select_label')) + $categories, @$post->category_id) ?>
						[ <?php echo anchor('admin/news/categories/create', lang('news:new_category_label'), 'target="_blank"'); ?> ]
					</div>
				</div>
				<?php if ($this->settings->news_to_twitter): ?>
					<div style="display:block;width:30%;float:left;">
						<label for="status"><?php echo lang('news:send_to_twitter_label'); ?></label>
						<div class="input"><?php echo form_dropdown('tweet', array('n' => lang('news:no_label'), 'y' => lang('news:yes_label')), $post->tweet ? $post->tweet : 'y') ?></div>
					</div>
				<?php endif; ?>
				<div class="clearfix"></div>
			</li>

			<li>
				<div style="display:block;width:30%;float:left;">
					<label for="userfile"><?php echo lang('news:userfile_label'); ?></label>
					<div class="input">
						<?php echo form_upload('userfile', $post->attachment, 'id="userfile"') ?>
						<?php if ($post->attachment): ?>
							<div>
								<a href="<?php echo site_url(UPLOAD_PATH.'news/'.$post->attachment); ?>" class="modal"><img src="<?php echo site_url(UPLOAD_PATH.'news/'.thumbnail($post->attachment)); ?>" /></a><br />
								<input type="checkbox" name="imagedel" /> <?php echo lang('news:delete_image_label'); ?>
								<input type="hidden" name="oldimage" value="<?php echo $post->attachment ?>" />
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div style="display:block;width:30%;float:left;">
				</div>
					<label for="credits"><?php echo lang('news:credits_label'); ?><small><?php echo lang('news:credits_msg_label'); ?></small></label>
					<div class="input"><?php echo form_input('credits', $post->credits, 'id="credits"') ?></div>
				<div class="clearfix"></div>
			</li>
						
			<li>
				<label for="keywords"><?php echo lang('global:keywords'); ?></label>
				<div class="input"><?php echo form_input('keywords', $post->keywords, 'id="keywords"') ?></div>
			</li>
						
			<li class="date-meta">
				<label><?php echo lang('news:date_label'); ?></label>
				<div class="input">
					<?php echo form_input('created_on', date('Y-m-d', $post->created_on), 'maxlength="10" id="datepicker" class="text width-20"'); ?> &nbsp;
					<div class="datetime_input">
					<?php echo form_dropdown('created_on_hour', $hours, date('H', $post->created_on), 'class="time-meta"') ?> : 
					<?php echo form_dropdown('created_on_minute', $minutes, date('i', ltrim($post->created_on, '0')), 'class="time-meta"') ?>
					</div>				
				</div>
			</li>
			
				<?php if ( ! module_enabled('comments')): ?>
					<?php echo form_hidden('comments_enabled', 'no'); ?>
				<?php else: ?>
					<li>
						<label for="comments_enabled"><?php echo lang('news:comments_enabled_label');?></label>
						<div class="input">
							<?php echo form_dropdown('comments_enabled', array(
								'no' => lang('global:no'),
								'1 day' => lang('global:duration:1-day'),
								'1 week' => lang('global:duration:1-week'),
								'2 weeks' => lang('global:duration:2-weeks'),
								'1 month' => lang('global:duration:1-month'),
								'3 months' => lang('global:duration:3-months'),
								'always' => lang('global:duration:always'),
							), $post->comments_enabled ? $post->comments_enabled : '3 months') ?>
						</div>
					</li>
				<?php endif; ?>

			<li>
				<?php $maxleadtext = 250; ?>
				<label for="lead"><?php echo lang('news:lead_label'); ?></label>
				<br style="clear: both;" />
				<textarea
					onKeyDown="limitText(this.form.lead,this.form.countdown,<?php echo $maxleadtext?>);" 
					onKeyUp="limitText(this.form.lead,this.form.countdown,<?php echo $maxleadtext?>);"
					maxlength="<?php echo $maxleadtext?>"
					rows="4"
					cols="40"
					style="width: 90%;"
					name="lead"
					id="lead"><?php echo $post->lead ? $post->lead : strip_tags($post->intro) ?></textarea><br />
				<small>(Maximum characters: <?php echo $maxleadtext?>)<br />
				You have <input readonly type="text" name="countdown" size="3" value="<?php echo !empty($post->lead) ? $maxleadtext - strlen($post->lead) : $maxleadtext ?>"> characters left.</small>
				<script>
					var leadtext = document.getElementById("lead");
					leadtext.onpaste = function(e){
						//do some IE browser checking for e
						var max = leadtext.getAttribute("maxlength");
						e.clipboardData.getData('text/plain').slice(0, max);
						limitText(this.form.lead,this.form.countdown, max);
					};

					function limitText(limitField, limitCount, limitNum) {
						if (limitField.value.length > limitNum) {
							limitField.value = limitField.value.substring(0, limitNum);
						} else {
							limitCount.value = limitNum - limitField.value.length;
						}
					}
				</script>
			</li>

<!--
			<li>
				<label for="intro"><?php echo lang('news:intro_label'); ?></label>
				<br style="clear: both;" />
				<?php echo form_textarea(array('id' => 'intro', 'name' => 'intro', 'value' => $post->intro, 'rows' => 5, 'class' => 'news wysiwyg-simple')); ?>
			</li>
			
-->
			<li class="editor">
				<label for="body"><?php echo lang('news:content_label'); ?></label>
				
				<div class="input">
					<?php echo form_dropdown('type', array(
						'html' => 'html',
						'markdown' => 'markdown',
						'wysiwyg-simple' => 'wysiwyg-simple',
						'wysiwyg-advanced' => 'wysiwyg-advanced',
					), $post->type); ?>
				</div>
				
				<br style="clear:both"/>
				
				<?php echo form_textarea(array('id' => 'body', 'name' => 'body', 'value' => $post->body, 'rows' => 30, 'class' => $post->type)); ?>
				
			</li>
		</ul>
        <?php echo form_hidden('preview_hash',$post->preview_hash)?>
		</fieldset>
		
	</div>

	<div class="form_inputs" id="news-options-tab">

		<fieldset>

			<ul>
				<li id="debug"></li>
				<li>
					<label for="news_quote"><?php echo lang('news:news_quote_label'); ?></label>
					<div class="input">
						<textarea
							onKeyDown="limitText(this.form.news_quote,this.form.countdown2,<?php echo $maxleadtext?>);" 
							onKeyUp="limitText(this.form.news_quote,this.form.countdown2,<?php echo $maxleadtext?>);"
							maxlength="<?php echo $maxleadtext?>"
							rows="4"
							cols="40"
							style="width: 90%;"
							name="news_quote"
							id="news_quote"><?php echo $post->news_quote ?></textarea><br />
						<small>(Maximum characters: <?php echo $maxleadtext?>)<br />
						You have <input readonly type="text" name="countdown2" size="3" value="<?php echo !empty($post->news_quote) ? $maxleadtext - strlen($post->news_quote) : $maxleadtext ?>"> characters left.</small>

						<!-- <textarea id="news_quote" name="news_quote" style="width:350px;height:40px" cols="30" rows="4"><?php echo $post->news_quote ?></textarea> -->
					</div>
				</li>
				<li>
					<label><?php echo lang('news:page_format_label') ?></label>
					<div class="input">
					<?php
					$p_pf = $post->page_format ? $post->page_format : '1_column';
					foreach($page_format as $pf) 
					{
						echo '<label class="col_'.$pf.'">'.form_radio('page_format', $pf, $p_pf == $pf, 'onClick="displayOptionalImages($(this))" rel="'.$pf.'" ') . "</label>\n";
					}
					?>
					</div>
					<?php $display = $post->page_format ? 'block' : 'none' ?>
					<div id="2_column" class="page_format_column" style="display:<?php echo $display; ?>;float:left;width:250px;">
						<h4>Images</h4>
						<div class="input" id="2_column_upload">
							<?php 
								for($i=0; $i<5; $i++)
								{
								?>
									<div class="opt-images">
										<label>Image #<?php echo ($i+1); ?></label>
								<?
									if (!empty($post->sidebarimages[$i]))
									{
								?>
										<a href="<?php echo site_url(UPLOAD_PATH.'news/'.$post->sidebarimages[$i]); ?>" class="modal"><img src="<?php echo site_url(UPLOAD_PATH.'news/'.thumbnail($post->sidebarimages[$i])); ?>" /></a><br />
										<input type="checkbox" value="y" name="sidebarimagedel[]" /> <?php echo lang('news:delete_image_label'); ?>
								<?php
									} // end of if (!empty($sidebarimages[$i]))
									echo form_upload('file_2_column[]');
									echo form_hidden('oldsidebarimage[]', !empty($post->sidebarimages[$i]) ? $post->sidebarimages[$i] : '');
									echo '</div>'.PHP_EOL;
//									echo '<hr size="1" />'.PHP_EOL;
								}
							?>
						</div>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
<!--
					<div id="1_column" class="page_format_column" style="display:none;">
						<label><?php echo lang('news:1_column_label') ?></label>
						<div class="input" id="1_column_upload">
							<?php echo form_upload('2_column_1'); ?><br />
							<?php echo form_upload('2_column_2'); ?>
						</div>
					</div>
-->
				</li>

				<li>
					<label><?php echo lang('news_slide_image_label'); ?></label>
					<div style="float: left;">
						<a href="<?php echo site_url('admin/news/upload/');?>" class="button open-files-uploader"><?php echo lang('news_upload_label'); ?></a>
						[Max <?php echo ini_get('upload_max_filesize'); ?>]
						<div class="clear-both text-small1"><?php echo lang('news_slide_image_msg'); ?></div>
						<ul id="files-uploaded" class="grid clear-both">
						<?php if (!empty($slides)) echo $slides; ?>
						</ul>
					</div>
				</li>

			</ul>

		</fieldset>

	</div>
</div>

<div class="buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel'))); ?>
</div>

<?php echo form_close(); ?>

			<div class="hidden">

				<!-- slideshow uploader -->
				<div id="files-uploader">
					<div class="files-uploader-browser">
						<form action="<?php echo site_url('/admin/news/upload/'.(!empty($post->id)?$post->id:'')) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" name="frmUpload">
							<label for="userfile" class="upload">Upload Files</label>
							<input type="file" name="userfile" value="" class="no-uniform" multiple="multiple" />
							<div style="visibility:hidden;">
							<input type="text" name="slidetitle">
							<textarea rows="3" cols="30" name="slidedescription"></textarea>
							</div>
							<?php if (!empty($post->id)): ?>
								<input type="hidden" id="news_id" name="news_id" value="<?php echo $post->id; ?>" />
							<?php endif; ?>
						</form>
						<ul id="files-uploader-queue" class="ui-corner-all"></ul>
		
					</div>
					<div class="buttons align-right padding-top">
						<a href="#" title="" class="button start-upload">Upload</a>
						<a href="#" title="" class="button cancel-upload">Cancel</a>
					</div>
				</div>

			</div>

</div>
</section>
<script>
	var news_id = jQuery('#news_id').val();
	var startbtn = 'Start';
	var cancelbtn = 'Cancel';

$(document).ready(function(){
	$('#title_color').click(function(e){
		e.preventDefault();
		if ($('#title_size_choices').css('display')=='block') $('#title_size_choices').toggle();
		$('#title_color_choices').toggle();
	});
	$('#title_size').click(function(e){
		e.preventDefault();
		if ($('#title_color_choices').css('display')=='block') $('#title_color_choices').toggle();
		$('#title_size_choices').toggle();
	});
});

function displayOptionalImages(d)
{
	if (d.is(":checked"))
	{
		var v = d.attr('rel');

		$('.page_format_column').not('#'+v).each(function(){
			$('input type[=file]', this).each(function(){
				$(this).replaceWith($(this).val('').clone(true));
			});
		});

		if (v === '1_column')
			$('#2_column').hide();
		else
			$('#2_column').show();

	}
}
</script>

<style>
.opt-images {
	width: 100%;
	border: 1px solid #ccc;
	padding: 10px;
	margin: 0 0 15px 15px;
}
.font-util {
	z-index: 100;
	background: #fff;
	padding: 10px;
	border-radius: 5px;
	border: 1px solid #ccc;
	position: absolute;
}
</style>