<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Public News module controller
 *
 * @author		PyroCMS Dev Team
 * @package 	PyroCMS\Core\Modules\News\Controllers
 */
class News extends Public_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_m');
		$this->load->model('news_categories_m');
		//$this->load->model('comments/comment_m');
		$this->load->library(array('keywords/keywords'));
		$this->load->helper('filename');
		$this->lang->load('news');

		$cats = $this->news_categories_m->get_all();
		foreach ($cats as $c)
		{
			$categories[$c->id] = $c->title;
		}
		$this->template->set('categories', $categories);

	}

	/**
	 * Shows the news index
	 *
	 * news/page/x also routes here
	 */
	public function index()
	{
		$total_rows = $this->news_m->count_by(array('status' => 'live'));
		$pagination = create_pagination('news/page', $total_rows, NULL, 3);

		$_news = $this->news_m->limit($pagination['limit'], $pagination['offset'])->get_many_by(array('status' => 'live'));

		foreach ($_news as &$post)
		{
			$post->keywords = Keywords::get($post->keywords);
			$post->short_intro = word_limiter($post->intro, 35);
			$post->url = site_url('news/'.date('Y', $post->created_on).'/'.date('m', $post->created_on).'/'.$post->slug);
		}

		// Set meta description based on post titles
		$meta = $this->_posts_metadata($_news);

		$this->template
			->title($this->module_details['name'])
			->set_breadcrumb(lang('news:news_title'))
			->set_metadata('description', $meta['description'])
			->set_metadata('keywords', $meta['keywords'])
			->set('pagination', $pagination)
			->set('news', $_news)
			->build('posts');
	}


    /**
     * preview a post
     *
     * @param string $hash the preview_hash of post
     */
    public function preview($hash = '')
    {
        if ( ! $hash or ! $post = $this->news_m->get_by('preview_hash', $hash))
        {
            redirect('news');
        }

        if ($post->status == 'live')
        {
            redirect('news/' . date('Y/m',$post->created_on) . '/' . $post->slug);
        }

        //set index nofollow to attempt to avoid search engine indexing
        $this->template
            ->set_metadata('index','nofollow');

        $this->_single_view($post, $post->page_format ? 'templates/'.$post->page_format : 'templates/1_column');

    }

	/**
	 * @todo Document this.
	 *
	 * @param array $posts
	 *
	 * @return array
	 */
	private function _posts_metadata(&$posts = array())
	{
		$keywords = array();
		$description = array();

		// Loop through posts and use titles for meta description
		if ( ! empty($posts))
		{
			foreach ($posts as &$post)
			{
				if ($post->category_title)
				{
					//$keywords[$post->category_id] = $post->category_title.', '.$post->category_slug;
					foreach($post->keywords as $pk)
					{
						$keywords[] = $pk->name;
					}
				}
				$description[] = $post->title;
			}
		}

		return array(
			'keywords' => implode(', ', $keywords),
			'description' => implode(', ', $description)
		);
	}

	public function xpreview($id=0)
	{
		if (empty($this->current_user->id) OR !$id) return false;

		$post = $this->news_m->get_temp($id); //$this->db->where('id', $id)->get('newstemp');

		if ($post->related_news)
		{
			$rn_ids = explode(',', $post->related_news);
			$related_news = $this->news_m->get_many_by(array('id'=>$rn_ids));
			foreach($related_news as $r)
			{
				if ($r->external_url)
					$r->url = $r->external_url;
				else
					$r->url = site_url( 'news/'. date('Y', $r->created_on) . '/' . date('d', $r->created_on) . '/' . $r->slug );
			}
		}

		$this->template
			->set('related_news', !empty($related_news)?$related_news:NULL)
			->set('author', NULL)
			->set('post', $post)
			->build('templates/preview');

	}


    private function _single_view($post,$build='view')
    {
        // if it uses markdown then display the parsed version
        if ($post->type == 'markdown')
        {
            $post->body = $post->parsed;
        }

		$post->body = str_replace(array('<br />','<br/>','<br>'), "\n", trim($post->body) );
		$post->body = str_replace("<div>&nbsp;</div>", "", $post->body);
		$post->body = preg_replace('/\n[\n]+/', "\n",$post->body);
		$post->body = str_replace("\n", "\n<p></p>\n", $post->body);

		$post->nextprev = $this->news_m->get_next_previous($post->created_on, $post->id);

		//$post->optional_images = json_decode($post->optional_images);

        // IF this post uses a category, grab it
        if ($post->category_id && ($category = $this->news_categories_m->get($post->category_id)))
        {
            $post->category = $category;
        }

        // Set some defaults
        else
        {
            $post->category = (object) array(
            	'id' => 0,
				'slug' => '',
				'title' => '',
			);
        }

        $this->session->set_flashdata(array('referrer' => $this->uri->uri_string));

        $this->template->title($post->title, lang('news:news_title'))
            ->set_metadata('description', $post->intro)
            ->set_metadata('keywords', implode(', ', Keywords::get_array($post->keywords)))
            ->set_breadcrumb(lang('news:news_title'), 'news');

        if ($post->category->id > 0)
        {
            $this->template->set_breadcrumb($post->category->title, 'news/category/'.$post->category->slug);
        }

        $post->keywords = Keywords::get($post->keywords);
        //$post->body=str_ireplace('\r', '', $post->body);

		if ($post->related_news)
		{
			$rn_ids = explode(',', $post->related_news);
			$related_news = $this->news_m->get_many_by(array('id'=>$rn_ids));
			foreach($related_news as $r)
			{
				if ($r->external_url)
					$r->url = $r->external_url;
				else
					$r->url = site_url( 'news/'. date('Y', $r->created_on) . '/' . date('d', $r->created_on) . '/' . $r->slug );
			}
		}
		else
		{
			$kwords = array();
			// Add in OG keywords
			foreach ($post->keywords as $keyword)
			{
				$this->template->set_metadata('article:tag', $keyword->name, 'og');
				$kwords[] = $keyword->name;
			}


			$other_news = $this->news_m->get_many_by(array('slugnot'=>$post->slug, 'limit'=>3, 'month'=>date('m', $post->created_on), 'year'=>date('Y', $post->created_on), 'order_by'=>'slug', 'order_dir'=>'random'));

			if (count($kwords))
			{
				$related_news = $this->news_m->get_many_by_keywords($kwords,
					array(
						'slugnot'=>$post->slug,
						'limit'=>4,
						'status'=>'live',
						'order_by'=>'slug',
						'order_dir'=>'random'
					)
				);
			}
			else
			{
				$related_news = array();
			}
		}

		$viewed = (int)$post->viewed + 1;

		$session_id = $this->session->userdata('session_id');
		$sess_userdata = $this->session->userdata('news_id');
		if (empty($sess_userdata))
		{
			$sess_details = array('news_id'=>array($post->id));
			$this->session->set_userdata($sess_details);
			$this->news_m->update($post->id, array('viewed'=>$viewed));
		}
		else
		{
			if (!in_array($post->id, $sess_userdata))
			{
				//$sess_details['news_id'][] = $post->id;
				$array = array_merge($sess_userdata,array($post->id));
				$sess_details = array('news_id'=>$array);
				$this->session->set_userdata($sess_details);
				$this->news_m->update($post->id, array('viewed'=>$viewed));
			}
		}

		// If comments are enabled, go fetch them all
		if (Settings::get('enable_comments'))
		{
			// Load Comments so we can work out what to do with them
			$this->load->library('comments/comments', array(
				'entry_id' => $post->id,
				'entry_title' => $post->title,
				'module' => 'news',
				'singular' => 'news:post',
				'plural' => 'news:posts',
			));

			// Comments enabled can be 'no', 'always', or a strtotime compatable difference string, so "2 weeks"
			$this->template->set('form_display', (
				$post->comments_enabled === 'always' or
					($post->comments_enabled !== 'no' and time() < strtotime('+'.$post->comments_enabled, $post->created_on))
			));
		}

		if ($post->attachment)
			$this->template->set_metadata('og:image', site_url(UPLOAD_PATH.'news/'.$post->attachment), 'og');
		else
			$this->template->set_metadata('og:image', site_url('harian_tangerang.png'));

        $this->template
            //->set_breadcrumb($post->title)
			->set_metadata('og:type', 'article', 'og')
			->set_metadata('og:url', current_url(), 'og')
			->set_metadata('og:title', $post->title, 'og')
			->set_metadata('og:site_name', Settings::get('site_name'), 'og')
			->set_metadata('og:description', $post->intro, 'og')
			->set_metadata('fb:app_id', '213853518765674')
			->set_metadata('article:published_time', date(DATE_ISO8601, $post->created_on), 'og')
			->set_metadata('article:modified_time', date(DATE_ISO8601, $post->updated_on), 'og')
			->set_metadata('description', $post->intro)
            //->set('author', $author)
            ->set('post', $post)
            ->set('related_news', $related_news)
            ->build($build);
    }

}
