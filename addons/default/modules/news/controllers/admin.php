<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 * @author 		PyroCMS Dev Team
 * @package 	PyroCMS\Core\Modules\News\Controllers
 */
class Admin extends Admin_Controller
{
	/**
	 * The current active section
	 *
	 * @var string
	 */
	protected $section = 'posts';

	/**
	 * Array that contains the validation rules
	 *
	 * @var array
	 */
	protected $validation_rules = array(
		'title' => array(
			'field' => 'title',
			'label' => 'lang:global:title',
			'rules' => 'trim|htmlspecialchars|required|max_length[250]|callback__check_title'
		),
		'slug' => array(
			'field' => 'slug',
			'label' => 'lang:global:slug',
			'rules' => 'trim|required|alpha_dot_dash|max_length[250]|callback__check_slug'
		),
		array(
			'field' => 'title_color',
			'label' => 'lang:news:title_color_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'title_size',
			'label' => 'lang:news:title_size_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'subtitle',
			'label' => 'lang:news:subtitle_label',
			'rules' => 'trim|htmlspecialchars|max_length[100]|'
		),
		array(
			'field' => 'category_id',
			'label' => 'lang:news:category_label',
			'rules' => 'trim|numeric'
		),
		array(
			'field' => 'attachment',
			'label' => 'lang:news:userfile_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'credits',
			'label' => 'lang:news:credits_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'keywords',
			'label' => 'lang:global:keywords',
			'rules' => 'trim'
		),
		array(
			'field' => 'lead',
			'label' => 'lang:news:lead_label',
			'rules' => 'trim|max_length[350]'
		),
		array(
			'field' => 'intro',
			'label' => 'lang:news:intro_label',
			'rules' => 'trim'
		),
		'body' => array(
			'field' => 'body',
			'label' => 'lang:news:content_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'type',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'status',
			'label' => 'lang:news:status_label',
			'rules' => 'trim|alpha'
		),
		array(
			'field' => 'created_on',
			'label' => 'lang:news:date_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'created_on_hour',
			'label' => 'lang:news:created_hour',
			'rules' => 'trim|numeric|required'
		),
		array(
			'field' => 'created_on_minute',
			'label' => 'lang:news:created_minute',
			'rules' => 'trim|numeric|required'
		),
        array(
			'field' => 'comments_enabled',
			'label'	=> 'lang:news:comments_enabled_label',
			'rules'	=> 'trim|required'
		),
        array(
            'field' => 'preview_hash',
            'label' => '',
            'rules' => 'trim'
        ),
		array(
			'field' => 'headline',
			'label' => 'lang:news:headline_label',
			'rules' => 'trim|numeric'
		),
		
		array(
			'field' => 'news_quote',
			'label' => 'lang:news:news_quote_label',
			'rules' => 'trim|max_length[250]'
		),
		array(
			'field' => 'page_format',
			'label' => 'lang:news:page_format_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'optional_images',
			'label' => 'lang:news:optional_images_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'related_news',
			'label' => 'lang:news:related_news_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'external_url',
			'label' => 'lang:news:external_url_label',
			'rules' => 'trim'
		),
	);

	private $_file_rules = array(
		array(
			'field' => 'name',
			'label' => 'lang:files.name_label',
			'rules' => 'trim|required|max_length[250]'
		),
		array(
			'field' => 'slidecaption',
			'label' => 'lang:news_title_label',
			'rules' => 'trim|max_length[100]'
		),
		array(
			'field' => 'slidedescription',
			'label' => 'lang:news_description_label',
			'rules' => 'trim'
		),
	);


	protected $upload_cfg = array(
		'allowed_types'		=> 'jpg|gif|png|jpeg',
		'max_size'			=> '10000',
		'remove_spaces'		=> TRUE,
		'overwrite'			=> FALSE,
		'encrypt_name'		=> TRUE,
	);

	protected $thumb_width = 125;
	protected $thumb_height = 125;
	protected $max_width = 940;
	protected $max_height = 550;

	protected $max_sidebar_image_width = 600;
	protected $max_sidebar_image_height = 500;

	protected $sidebar_thumb_width = 195;
	protected $sidebar_thumb_height = 195;

	/**
	 * Maximum width of slide images
	 */
	protected $slidew = 650;

	/**
	 * Maximum height of slide images
	 */
	protected $slideh = 430;

	protected $twitter_m_exists;

	/* years */
	protected $current_year;
	
	/**
	 * The constructor
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('news_m', 'news_categories_m'));
		$this->lang->load(array('news', 'categories'));

		$this->load->library(array('keywords/keywords', 'form_validation'));
		$this->load->helper('filename');

/*
		if ($tm = module_exists('twitter'))
		{
			$this->lang->load('twitter/twitter');
			$this->validation_rules[] = array(
	            'field' => 'tweet',
	            'label' => '',
	            'rules' => 'trim'
			);
			$this->twitter_m_exists = TRUE;
		}
*/

		// Date ranges for select boxes
		$this->template
			->set('hours', array_combine($hours = range(0, 23), $hours))
			->set('minutes', array_combine($minutes = range(0, 59), $minutes))
		;

		$_categories = array();
		if ($categories = $this->news_categories_m->order_by('title')->get_all())
		{
			foreach ($categories as $category)
			{
				$_categories[$category->id] = $category->title;
			}
		}
		$this->template->set('categories', $_categories);

		$_title_colors = array('#16a085', '#27ae60', '#2980b9', '#8e44ad', '#2c3e50', '#f39c12', '#d35400', '#c0392b', '#bdc3c7', '#7f8c8d', );

		$_title_sizes = array( 40, 46, 52, 56, 60, 64);

		if ($this->settings->newsimg_width)
			$this->max_width = $this->settings->newsimg_width;

		if ($this->settings->newsimg_height)
			$this->max_height = $this->settings->newsimg_height;

		$this->upload_cfg['upload_path'] = UPLOAD_PATH . 'news';

		$f_year_backend = $this->news_m->get_year();
		$current_year = 0;
		foreach($f_year_backend as $y)
		{
			$this->current_year = ($current_year < $y->news_year) ? $y->news_year : $current_year;
			$filter_year[$y->news_year] = $y->news_year;
		}
		
		$_page_format = array('1_column', '2_column', '2_column_l');
		$this->template
			->set('page_format', $_page_format)
			->set('title_colors', $_title_colors)
			->set('title_sizes', $_title_sizes)
			->append_js('module::jquery.form.js')
			->append_css('module::news.css')
			->set('filter_year', $filter_year);
			;
	}

	/**
	 * Show all created news posts
	 * 
	 * @return void
	 */
	public function index()
	{
		//set the base/default where clause
		$base_where = array('show_future' => TRUE, 'status' => 'all');

		if ($this->input->post('f_year'))
		{
			$base_where['year'] = $this->input->post('f_year');
		}else{
			//$base_where['year'] = $f_datenow;
			$base_where['year'] = date('Y');
		}
		
		//add post values to base_where if f_module is posted
		if ($this->input->post('f_category')) 	$base_where['category'] = $this->input->post('f_category');
		if ($this->input->post('f_status')) 	$base_where['status'] 	= $this->input->post('f_status');
		if ($this->input->post('f_keywords')) 	$base_where['keywords'] = $this->input->post('f_keywords');

		// Create pagination links
		$total_rows = $this->news_m->count_by($base_where);
		$pagination = create_pagination('admin/news/index', $total_rows);

		// Using this data, get the relevant results
		$news = $this->news_m->limit($pagination['limit'], $pagination['offset'])->get_many_by($base_where);

		//do we need to unset the layout because the request is ajax?
		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);

		$this->template
			->title($this->module_details['name'])
			->append_js('admin/filter.js')
			->set_partial('filters', 'admin/partials/filters')
			->set('pagination', $pagination)
			->set('news', $news);

		$this->input->is_ajax_request()
			? $this->template->build('admin/tables/posts')
			: $this->template->build('admin/index');

	}

	public function facebook()
	{
		$config = array(
			'appId'  => $this->settings->fb_app_id,
			'secret' => $this->settings->fb_app_secret,
			'cookie' => true,
		);
		$this->load->library('facebook/facebook', $config);
?>
<pre>
$_SESSION:
<?php print_r($_SESSION); ?>
</pre>
<?
		$loginUrl = $this->facebook->getLoginUrl();

		//$token = $this->facebook->getAccessToken();

		$fbuser = $this->facebook->getUser();

		if($fbuser){
			$token = $this->facebook->getAccessToken();
		}
		else
		{
			if (!empty($_GET['code']))
			{
				setcookie('code', $_GET['code']);
				$res = $this->facebook->api('oauth/access_token?client_id='.$this->settings->fb_app_id.'&client_secret='.$this->settings->fb_app_secret.'&code='.$_GET['code'].'&redirect_uri='.site_url(uri_string()));
echo "<pre>";
echo "\$res:\n";
print_r($res);
echo "</pre>";
			}
			else
			{
//echo "No code.. (at else..)<br />\n";
				//$loginUrl = $this->facebook->getLoginUrl();
				header("location: $loginUrl");
			}
		}

//		$user = $this->facebook->api('/me');
			echo "<pre>";
			echo "\$token:\n";
			print_r($token);
			echo "\n\n\$fbuser:\n";
			print_r($fbuser);
			echo "</pre>";
//exit;

		echo "<pre>";
		echo "\$loginUrl:\n";
		print_r($loginUrl);
		echo "</pre>";

		$post =  array(
			'access_token' => $token,
			'message' => 'This message is posted with access token - ' . date('Y-m-d H:i:s')
		);

		echo "<pre>";
		echo "\$post:\n";
		print_r($post);
		echo "</pre>";

		//and make the request
/*
		$res = $this->facebook->api('/me/feed', 'POST', $post);

		echo "<pre>";
		echo "\$res:\n";
		print_r($res);
		echo "</pre>";
*/
	}

	/**
	 * Create new post
	 *
	 * @return void
	 */
	public function create()
	{
		if ($this->input->post('created_on'))
		{
			$dco = explode('-', $this->input->post('created_on'));
			$date_created_year = $dco[0];
			
			$created_on = strtotime(sprintf('%s %s:%s', $this->input->post('created_on'), $this->input->post('created_on_hour'), $this->input->post('created_on_minute')));
		}
		else
		{
			$created_on = now();
		}
        $hash = $this->_preview_hash();

		if ($this->input->post('external_url'))
		{
			$this->validation_rules['body'] = array(
				'field' => 'body',
				'label' => 'lang:news:content_label',
				'rules' => 'trim'
			);
		}

		$rn_ids = $this->input->post('related_news_ids');

		$related_news = array();

		if ($rn_ids)
		{
			$ids = explode(',',$rn_ids);
			$related_news = $this->news_m->get_many_by(array('id'=>$ids));
		}

		$this->form_validation->set_rules($this->validation_rules);

		//$related = $this->news_m->get

		if ($this->form_validation->run())
		{
			// They are trying to put this live
			if ($this->input->post('status') == 'live')
			{
				role_or_die('news', 'put_live');

                $hash = "";
			}

			if (!empty($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name']))
			{
				// Setup upload config
				$this->load->library('upload', $this->upload_cfg);

				// check directory exists
				$this->check_dir($this->upload_cfg['upload_path']);

				if ( ! $this->upload->do_upload('userfile'))
				{
				}
				else
				{
					$file = $this->upload->data('');
					$data = array(
						'filename'		=> $file['file_name'],
						'credits'		=> $this->input->post('credits'),
						'uploaded_by'	=> $this->current_user->id,
						'uploaded_on'	=> now(),
						'position'		=> 999
					);

					if ($file['is_image'])
					{
						$this->load->library('image_lib');
						$filename = $file['file_name'];
						$thumbfile = thumbnail($filename);
						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['master_dim'] = 'width';
						$image_cfg['width'] = ($file['image_width'] < $this->thumb_width ? $file['image_width'] : $this->thumb_width);
						$image_cfg['height'] = ($file['image_height'] < $this->thumb_height ? $file['image_height'] : $this->thumb_height);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();
	
						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['master_dim'] = 'width';
						$image_cfg['width'] = ($file['image_width'] < $this->max_width ? $file['image_width'] : $this->max_width);
						$image_cfg['height'] = ($file['image_height'] < $this->max_height ? $file['image_height'] : $this->max_height);
						$image_cfg['create_thumb'] = FALSE;
						//$image_cfg['new_image'] = $file['file_path'] . $medfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

						if ($this->input->post('oldimage'))
						{
							$this->unlinkfile($this->input->post('oldimage'));
						}
					}

				}
			}

			$input = array(
				'title'				=> $this->input->post('title'),
				'title_color'		=> $this->input->post('title_color'),
				'title_size'		=> $this->input->post('title_size'),
				'slug'				=> $this->input->post('slug'),
				'subtitle'			=> $this->input->post('subtitle'),
				'category_id'		=> $this->input->post('category_id'),
				'credits'			=> !empty($filename) ? $this->input->post('credits') : '',
				'keywords'			=> Keywords::process($this->input->post('keywords')),
				'lead'				=> $this->input->post('lead'),
				'intro'				=> $this->input->post('intro') ? $this->input->post('intro') : "",
				'body'				=> $this->input->post('body'),
				'status'			=> $this->input->post('status'),
				'created_on'		=> $created_on,
				'comments_enabled'	=> $this->input->post('comments_enabled'),
				'author_id'			=> $this->current_user->id,
				'type'				=> $this->input->post('type'),
				'parsed'			=> ($this->input->post('type') == 'markdown') ? parse_markdown($this->input->post('body')) : '',
				'headline'			=> $this->input->post('headline'),
                'preview_hash'      => $hash,
				'news_quote'		=> $this->input->post('news_quote'),
				'page_format'		=> $this->input->post('page_format'),
				'related_news'		=> $rn_ids,
				'external_url'		=> $this->input->post('external_url'),
				'external_url'		=> $this->input->post('external_url'),
				
				'news_year'		=> $date_created_year,
			);

			if (!empty($filename))
			{
				$input['attachment'] = $filename;
				$input['credits']	 = $this->input->post('credits');
			}

			if ($this->input->post('imagedel'))
			{
				$this->unlinkfile($post->attachment);
				$input['attachment'] = NULL;
			}

			$optionalimages = array();

			// Setup upload config
			$this->load->library('upload', $this->upload_cfg);
			$oldsidebarimage = $this->input->post('oldsidebarimage');

			if ($id = $this->news_m->insert($input))
			{
				// delete newstemp
				if ($this->input->post('tn_id'))
				{
					$this->news_m->delete_temp($this->input->post('tn_id'));
				}
				// delete image temp
				if ($this->input->post('tn_image'))
				{
					$this->unlinkfile_temp($this->input->post('tn_image'));
				}

				$this->pyrocache->delete_all('news_m');
				$this->session->set_flashdata('success', sprintf($this->lang->line('news:post_add_success'), $this->input->post('title')));

				$pos = 1;
				if ($this->input->post('img_slide_id'))
				{
					foreach($this->input->post('img_slide_id') as $img)
					{
						$input_image['news_id'] = $id;
						$input_image['position'] = $pos;
						$this->news_m->updateimg($img, $input_image);
						$pos++;
					}
				}

				// News article has been updated, may not be anything to do with publishing though
				//Events::trigger('post_created', $id);

				// They are trying to put this live
				if ($this->input->post('status') == 'live')
				{
					// Fire an event, we're posting a new news!
					//Events::trigger('post_published', $id);
				}

//				if ($this->twitter_m_exists)
//				{
					// The twitter module is here, and enabled!
						if($this->settings->news_to_twitter == 1 && $this->settings->twitter_access_token != NULL && $this->settings->twitter_access_token_secret != NULL && $this->input->post('status') == 'live' && $this->input->post('tweet')=='y')
						{

							$tconfig = array(
								'consumer_key'		=>$this->settings->twitter_consumer_key,
								'consumer_secret'	=>$this->settings->twitter_consumer_key_secret,
								'oauth_token'		=>$this->settings->twitter_access_token,
								'oauth_token_secret'=>$this->settings->twitter_access_token_secret
							);
							$this->load->library('EpiTwitter', $tconfig);//, $tconfig);
							$this->epitwitter->useApiVersion('1.1');

							$title = $this->input->post('title');
							$tlength = strlen($title);
							$url = shorten_url('news/'.date("Y", $created_on).'/'.date("m", $created_on).'/'.$this->input->post('slug'));
							$ulength = strlen($url);
							if ($tlength+$ulength>140)
							{
								$title = character_limiter($title, (140-$ulength-2), '..');
							}

							if (!empty($input['attachment']))
							{
								$status_update = $this->epitwitter->post(
									'/statuses/update_with_media.json',
									array(
										'@media[]'=>"@".(UPLOAD_PATH . 'news/'.$input['attachment']),						
										'status' => sprintf($this->lang->line('news:twitter_text'), $this->input->post('title'), $url),

									)
								);
							}
							else
							{
								$status_update = $this->epitwitter->post('/statuses/update.json', array('status' => sprintf($this->lang->line('news:twitter_text'), $this->input->post('title'), $url)));
							}

							if(empty($status_update->id))
							{
								$this->session->set_flashdata('error', lang('news:twitter_error') . ": " . 'Unable to update Twitter status');
							}
							else
							{
								$this->session->set_flashdata('notice', sprintf(lang('news:twitter_posted'), $this->input->post('title'), $url ));
							}
						}
						// End twitter code
//				}
			}
			else
			{
				$this->session->set_flashdata('error', lang('news:post_add_error'));
			}

			// Redirect back to the form or main page
			$this->input->post('btnAction') == 'save_exit' ? redirect('admin/news') : redirect('admin/news/edit/' . $id);
		}
		else
		{
			// Go through all the known fields and get the post values
			$post = new stdClass;
			foreach ($this->validation_rules as $key => $field)
			{
				$post->$field['field'] = set_value($field['field']);
			}
			$post->created_on = $created_on;
			// if it's a fresh new article lets show them the advanced editor
			$post->type or $post->type = 'wysiwyg-advanced';
		}

		$post->tweet = set_value('tweet');

		$related = array();
		if ($post->related_news)
		{
			$related = array();
		}

		$this->template
			->title($this->module_details['name'], lang('news:create_title'))
			->append_metadata($this->load->view('fragments/wysiwyg', array(), TRUE))
			->append_css('module::files.css')
			->append_css('module::jquery.fileupload-ui.css')
			->append_js('module::jquery.cooki.js')
			->append_js('module::jquery.fileupload.js')
			->append_js('module::jquery.fileupload-ui.js')
			->append_js('jquery/jquery.tagsinput.js')
			->append_js('module::news_form.js')
			->append_css('jquery/jquery.tagsinput.css')
			->set('related', $related)
			->set('post', $post)
			->build('admin/form');
	}

	/**
	 * Edit news post
	 *
	 * 
	 * @param int $id the ID of the news post to edit
	 * @return void
	 */
	public function edit($id = 0)
	{

		$id OR redirect('admin/news');

		$post = $this->news_m->get($id);

		//$post->sidebarimages = (array) json_decode($post->optional_images);

		// If we have keywords before the update, we'll want to remove them from keywords_applied
		$old_keywords_hash = (trim($post->keywords) != '') ? $post->keywords : null;

		$post->keywords = Keywords::get_string($post->keywords);

		// If we have a useful date, use it
		if ($this->input->post('created_on'))
		{
			$dco = explode('-', $this->input->post('created_on'));
			$date_created_year = $dco[0];
			
			$created_on = strtotime(sprintf('%s %s:%s', $this->input->post('created_on'), $this->input->post('created_on_hour'), $this->input->post('created_on_minute')));
		}

		else
		{
			$created_on = $post->created_on;
		}


		if ($this->input->post('external_url'))
		{
			$this->validation_rules['body'] = array(
				'field' => 'body',
				'label' => 'lang:news:content_label',
				'rules' => 'trim'
			);
		}

/*
		if ($post->related_news)
		{
			$rn_ids = $post->related_news;
		}
*/

		$rn_ids = $this->input->post('related_news_ids') ? $this->input->post('related_news_ids') : $post->related_news;

		$related_news = array();

		if ($rn_ids)
		{
			$ids = explode(',',$rn_ids);
			$related_news = $this->news_m->get_many_by(array('id'=>$ids));
		}

		$this->form_validation->set_rules(array_merge($this->validation_rules, array(
			'title' => array(
				'field' => 'title',
				'label' => 'lang:global:title',
				'rules' => 'trim|htmlspecialchars|required|max_length[250]|callback__check_title['.$id.']'
			),
			'slug' => array(
				'field' => 'slug',
				'label' => 'lang:global:slug',
				'rules' => 'trim|required|alpha_dot_dash|max_length[250]|callback__check_slug['.$id.']'
			),
		)));
        $hash = $this->input->post('preview_hash');

        if ($this->input->post('status') == 'draft' and $this->input->post('preview_hash') == '')
        {

            $hash = $this->_preview_hash();
        }
		
		if ($this->form_validation->run())
		{
			// They are trying to put this live
			if ($post->status != 'live' and $this->input->post('status') == 'live')
			{
				role_or_die('news', 'put_live');
			}

			$author_id = empty($post->display_name) ? $this->current_user->id : $post->author_id;

			if (!empty($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name']))
			{
				// Setup upload config
				$this->load->library('upload', $this->upload_cfg);

				// check directory exists
				$this->check_dir($this->upload_cfg['upload_path']);

				if ( ! $this->upload->do_upload('userfile'))
				{
				}
				else
				{
					$file = $this->upload->data('');
					$data = array(
						'filename'		=> $file['file_name'],
						'uploaded_by'	=> $this->current_user->id,
						'uploaded_on'	=> now(),
						'position'		=> 999
					);

					if ($file['is_image'])
					{
						$this->load->library('image_lib');
						$filename = $file['file_name'];
						$thumbfile = thumbnail($filename);
						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['master_dim'] = 'width';
						$image_cfg['width'] = ($file['image_width'] < $this->thumb_width ? $file['image_width'] : $this->thumb_width);
						$image_cfg['height'] = ($file['image_height'] < $this->thumb_height ? $file['image_height'] : $this->thumb_height);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();
	
						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['master_dim'] = 'width';
						$image_cfg['width'] = ($file['image_width'] < $this->max_width ? $file['image_width'] : $this->max_width);
						$image_cfg['height'] = ($file['image_height'] < $this->max_height ? $file['image_height'] : $this->max_height);
						$image_cfg['create_thumb'] = FALSE;
						//$image_cfg['new_image'] = $file['file_path'] . $medfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

						if ($this->input->post('oldimage'))
						{
							$this->unlinkfile($this->input->post('oldimage'));
						}
					}

				}
			}

			$input = array(
				'title'				=> $this->input->post('title'),
				'title_color'		=> $this->input->post('title_color'),
				'title_size'		=> $this->input->post('title_size'),
				'slug'				=> $this->input->post('slug'),
				'subtitle'			=> $this->input->post('subtitle'),
				'category_id'		=> $this->input->post('category_id'),
				'credits'			=> ($post->attachment OR !empty($filename)) ? $this->input->post('credits') : '',
				'keywords'			=> Keywords::process($this->input->post('keywords'), $old_keywords_hash),
				'lead'				=> $this->input->post('lead'),
				'intro'				=> $this->input->post('intro') ? $this->input->post('intro') : "",
				'body'				=> $this->input->post('body'),
				'status'			=> $this->input->post('status'),
				'created_on'		=> $created_on,
				'comments_enabled'	=> $this->input->post('comments_enabled'),
				'author_id'			=> $author_id,
				'type'				=> $this->input->post('type'),
				'parsed'			=> ($this->input->post('type') == 'markdown') ? parse_markdown($this->input->post('body')) : '',
				'headline'			=> $this->input->post('headline'),
                'preview_hash'      => $hash,
				'news_quote'		=> $this->input->post('news_quote'),
				'page_format'		=> $this->input->post('page_format'),
				'related_news'		=> $rn_ids,
				'external_url'		=> $this->input->post('external_url'),
				
				'news_year'		=> $date_created_year,
			);

			if (!empty($filename))
			{
				$input['attachment'] = $filename;
			}

			if ($this->input->post('imagedel'))
			{
				$this->unlinkfile($post->attachment);
				$input['attachment'] = '';
			}


			$result = $this->news_m->update($id, $input);

			if ($result)
			{
				// delete newstemp
				if ($this->input->post('tn_id'))
					$this->news_m->delete_temp($this->input->post('tn_id'));
				// delete image temp
				if ($this->input->post('tn_image'))
				{
					$this->unlinkfile_temp($this->input->post('tn_image'));
				}

				$this->session->set_flashdata(array('success' => sprintf(lang('news:edit_success'), $this->input->post('title'))));

				$pos = 1;
				if ($this->input->post('img_slide_id'))
				{
					foreach($this->input->post('img_slide_id') as $img)
					{
						$input_image['news_id'] = $id;
						$input_image['position'] = $pos;
						$this->news_m->updateimg($img, $input_image);
						$pos++;
					}
				}


				// News article has been updated, may not be anything to do with publishing though
				//Events::trigger('post_updated', $id);

				// They are trying to put this live
				if ($post->status != 'live' and $this->input->post('status') == 'live')
				{
					// Fire an event, we're posting a new news!
					//Events::trigger('post_published', $id);

					//if ($this->twitter_m_exists)
					//{
						if($this->settings->news_to_twitter == 1 && $this->settings->twitter_access_token != NULL && $this->settings->twitter_access_token_secret != NULL && $this->input->post('status') == 'live' && $this->input->post('tweet')=='y')
						{

							$tconfig = array(
								'consumer_key'		=>$this->settings->twitter_consumer_key,
								'consumer_secret'	=>$this->settings->twitter_consumer_key_secret,
								'oauth_token'		=>$this->settings->twitter_access_token,
								'oauth_token_secret'=>$this->settings->twitter_access_token_secret
							);
							$this->load->library('EpiTwitter', $tconfig);//, $tconfig);
							$this->epitwitter->useApiVersion('1.1');

							$title = $this->input->post('title');
							$tlength = strlen($title);
							$url = shorten_url('news/'.date("Y", $created_on).'/'.date("m", $created_on).'/'.$this->input->post('slug'));
							$ulength = strlen($url);
							if ($tlength+$ulength>140)
							{
								$title = character_limiter($title, (140-$ulength-2), '..');
							}

							$auth = TRUE;

							if ($auth)
							{
								if (!empty($input['attachment']))
								{
									$status_update = $this->epitwitter->post(
										'/statuses/update_with_media.json',
										array(
											'@media[]'=>"@".(UPLOAD_PATH . 'news/'.$input['attachment']),						
											'status' => sprintf($this->lang->line('news:twitter_text'), $this->input->post('title'), $url),
	
										)
									);
								}
								else
								{
									$status_update = $this->epitwitter->post('/statuses/update.json', array('status' => sprintf($this->lang->line('news:twitter_text'), $this->input->post('title'), $url)));
								}


								if(empty($status_update->id))
								{
									$this->session->set_flashdata('error', lang('news:twitter_error') . ": " . 'Unable to update Twitter status');
								}
								else
								{
									$this->session->set_flashdata('notice', sprintf(lang('news:twitter_posted'), $this->input->post('title'), $url ));
								}
							}
							else
							{
								$this->session->set_flashdata('notice', lang('news:twitter_error') . ": " . 'Unable to post to Twitter');
							}
						}
						// End twitter code
					//}
				}
			}
			
			else
			{
				$this->session->set_flashdata('error', lang('news:edit_error'));
			}

			// Redirect back to the form or main page
			$this->input->post('btnAction') == 'save_exit' ? redirect('admin/news') : redirect('admin/news/edit/' . $id);
		}

		// Go through all the known fields and get the post values
		foreach ($this->validation_rules as $key => $field)
		{
			if (isset($_POST[$field['field']]))
			{
				$post->$field['field'] = set_value($field['field']);
			}
		}

		$files = $this->news_m->getuploadedimages($id);
		$slides = '';
		if (!empty($files))
		{
			$post->files =& $files;
			$slides = $this->load->view('admin/files/contents', array('files'=>$files), TRUE);
		}


		$post->created_on = $created_on;
		$post->tweet = set_value('tweet');
		
		$this->template
			->title($this->module_details['name'], sprintf(lang('news:edit_title'), $post->title))
			->append_metadata($this->load->view('fragments/wysiwyg', array(), TRUE))
			->append_css('module::files.css')
			->append_css('module::jquery.fileupload-ui.css')
			->append_js('module::jquery.cooki.js')
			->append_js('module::jquery.fileupload.js')
			->append_js('module::jquery.fileupload-ui.js')
			->append_js('jquery/jquery.tagsinput.js')
			->append_js('module::news_form.js')
			->append_css('jquery/jquery.tagsinput.css')
			->set('slides', $slides)
			->set('related_news', $related_news)
			->set('post', $post)
			->build('admin/form');
	}

	public function getnews($id=0)
	{
		$ids = $this->input->post('ids');
		$ids = explode(',', $ids);

		//set the base/default where clause
		$base_where = array('show_future' => TRUE, 'status' => 'live');

		//add post values to base_where if f_module is posted
		if ($this->input->post('f_category')) 	$base_where['category'] = $this->input->post('f_category');
		if ($this->input->post('f_status')) 	$base_where['status'] 	= $this->input->post('f_status');
		if ($this->input->post('f_keywords')) 	$base_where['keywords'] = $this->input->post('f_keywords');

		if ($id) $base_where['notid'] = $id;

		// Create pagination links
		$total_rows = $this->news_m->count_by($base_where);
		$pagination = create_pagination('admin/news/index', $total_rows, NULL, 5);

		// Using this data, get the relevant results
		$news = $this->news_m->limit($pagination['limit'], $pagination['offset'])->get_many_by($base_where);

		//do we need to unset the layout because the request is ajax?
		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);

		$this->template
			->title($this->module_details['name'])
			->append_js('admin/filter.js')
			->set_partial('filters', 'admin/partials/filters')
			->set('pagination', $pagination)
			->set('ids', $ids)
			->set('news', $news)
			->build('admin/tables/getnews');

	}

	public function getrelatednews()
	{
		$ids = explode(',', $this->input->post('ids'));
		$news = $this->news_m->get_many_by(array('id'=>$ids));
		foreach($news as $n)
		{
			echo "<li><a href=\"". $n->id ."\" class=\"button deletern\">Remove</a> <h3 class=\"inline\">".$n->title."</h3></li>\n";
		}
/*
		$this->template
			->set('ids', $ids)
			->set('news', $news)
			->build('admin/tables/getnews');
*/
	}

	public function newsdropdown($id=0)
	{
		if ($id)
		{
			$news = $this->news_m->news_dropdown(array('notid'=>$id));
		}
		else
			$news = $this->news_m->news_dropdown();
		return $news;
	}

	public function xpreview()
	{
		$upload_cfg = $this->upload_cfg;
		$upload_cfg['upload_path'] = UPLOAD_PATH . 'newstmp';
		$upload_cfg['encrypt_name'] = FALSE;


		if ($this->input->post('created_on'))
		{
			$created_on = strtotime(sprintf('%s %s:%s', $this->input->post('created_on'), $this->input->post('created_on_hour'), $this->input->post('created_on_minute')));
		}
		else
		{
			$created_on = now();
		}
        $hash = $this->_preview_hash();

		if ($this->input->post('external_url'))
		{
			$this->validation_rules['body'] = array(
				'field' => 'body',
				'label' => 'lang:news:content_label',
				'rules' => 'trim'
			);
		}

		$rn_ids = $this->input->post('related_news_ids');

		$related_news = array();

		if ($rn_ids)
		{
			$ids = explode(',',$rn_ids);
			$related_news = $this->news_m->get_many_by(array('id'=>$ids));
		}

		$this->form_validation->set_rules($this->validation_rules);

		//$related = $this->news_m->get

		//if ($this->form_validation->run())
		if (!empty($_POST))
		{

			if (!empty($_FILES['userfile']['name']) && !empty($_FILES['userfile']['tmp_name']))
			{
				if ($_FILES['userfile']['name'] <> $this->input->post('tn_image'))
				{
					// Setup upload config
					$this->load->library('upload', $upload_cfg);
	
					// check directory exists
					$this->check_dir($upload_cfg['upload_path']);
	
					if ( ! $this->upload->do_upload('userfile'))
					{
					}
					else
					{
						$file = $this->upload->data('');
						$data = array(
							'filename'		=> $file['file_name'],
							'credits'		=> $this->input->post('credits'),
							'uploaded_by'	=> $this->current_user->id,
							'uploaded_on'	=> now(),
							'position'		=> 999
						);
	
						if ($file['is_image'])
						{
							$this->load->library('image_lib');
							$filename = $file['file_name'];
							$thumbfile = thumbnail($filename);
							/*---------------------------------------------------------------------------------
							// create thumb - admin
							*/
							$image_cfg['source_image'] = $file['full_path'];
							$image_cfg['maintain_ratio'] = TRUE;
							$image_cfg['master_dim'] = 'width';
							$image_cfg['width'] = ($file['image_width'] < $this->thumb_width ? $file['image_width'] : $this->thumb_width);
							$image_cfg['height'] = ($file['image_height'] < $this->thumb_height ? $file['image_height'] : $this->thumb_height);
							$image_cfg['create_thumb'] = FALSE;
							$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
							$this->image_lib->initialize($image_cfg);
							$img_ok = $this->image_lib->resize();
							unset($image_cfg);
							$this->image_lib->clear();
		
							/*
							/* image resize - medium
							*/
							$image_cfg['source_image'] = $file['full_path'];
							$image_cfg['maintain_ratio'] = TRUE;
							$image_cfg['master_dim'] = 'width';
							$image_cfg['width'] = ($file['image_width'] < $this->max_width ? $file['image_width'] : $this->max_width);
							$image_cfg['height'] = ($file['image_height'] < $this->max_height ? $file['image_height'] : $this->max_height);
							$image_cfg['create_thumb'] = FALSE;
							//$image_cfg['new_image'] = $file['file_path'] . $medfile;
							$this->image_lib->initialize($image_cfg);
							$img_ok = $this->image_lib->resize();
							unset($image_cfg);
							$this->image_lib->clear();
	
							if ($this->input->post('tn_image') && $_FILES['userfile']['name'] <> $this->input->post('tn_image'))
							{
								$this->unlinkfile_temp($this->input->post('tn_image'));
							}
	
							$ret['attachment'] = $_FILES['userfile']['name'];
	
						}
	
					}
				}
				else
				{
					$filename = $ret['attachment'] = $_FILES['userfile']['name'];
				}
			}

			$input = array(
				'user_id'			=> !empty($this->current_user->id)?$this->current_user->id:NULL,
				'title'				=> $this->input->post('title'),
				'title_color'		=> $this->input->post('title_color'),
				'title_size'		=> $this->input->post('title_size'),
				'slug'				=> $this->input->post('slug'),
				'subtitle'			=> $this->input->post('subtitle'),
				'category_id'		=> $this->input->post('category_id'),
				'credits'			=> !empty($filename) ? $this->input->post('credits') : '',
				//'keywords'			=> Keywords::process($this->input->post('keywords')),
				'lead'				=> $this->input->post('lead'),
				'intro'				=> $this->input->post('intro') ? $this->input->post('intro') : "",
				'body'				=> $this->input->post('body'),
				'status'			=> $this->input->post('status'),
				'created_on'		=> $created_on,
				'comments_enabled'	=> $this->input->post('comments_enabled'),
				'author_id'			=> $this->current_user->id,
				'type'				=> $this->input->post('type'),
				'parsed'			=> ($this->input->post('type') == 'markdown') ? parse_markdown($this->input->post('body')) : '',
				'headline'			=> $this->input->post('headline'),
                'preview_hash'      => $hash,
				'news_quote'		=> $this->input->post('news_quote'),
				'page_format'		=> $this->input->post('page_format'),
				'related_news'		=> $rn_ids,
				'external_url'		=> $this->input->post('external_url'),
			);

			if (!empty($filename))
			{
				$input['attachment'] = $filename;
				$input['credits']	 = $this->input->post('credits');
			}

			if ($this->input->post('oldimage'))
			{
				$input['current_attachment'] = $this->input->post('oldimage');
			}

			if ($this->input->post('imagedel'))
			{
				$this->unlinkfile($post->attachment);
				$input['attachment'] = NULL;
			}

			$optionalimages = array();

			// Setup upload config
			$this->load->library('upload', $this->upload_cfg);
			$oldsidebarimage = $this->input->post('oldsidebarimage');

			if ($this->input->post('tn_id'))
			{
				$id = $this->news_m->update_temp($this->input->post('tn_id'), $input);
			}
			else
			{
				$id = $this->news_m->insert_temp($input);
			}

			if ($id)
			{
				$this->pyrocache->delete_all('news_m');

				$pos = 1;
				if ($this->input->post('img_slide_id'))
				{
					foreach($this->input->post('img_slide_id') as $img)
					{
						$input_image['news_id'] = $id;
						$input_image['position'] = $pos;
						$this->news_m->updateimg($img, $input_image);
						$pos++;
					}
				}

				$ret['redirect'] = site_url() . 'news/xpreview/'.$id;
				$ret['tn_id'] = $id;

			}
/*
			else
			{
				$this->session->set_flashdata('error', lang('news:post_add_error'));
			}
*/

			// Redirect back to the form or main page
			//$this->input->post('btnAction') == 'save_exit' ? redirect('admin/news') : redirect('admin/news/edit/' . $id);
		}

		$ret['status'] = 'ok';
		echo json_encode($ret);

/*
		$related = array();
		if ($post->related_news)
		{
			$related = array();
		}

		$this->template
			->title($this->module_details['name'], lang('news:create_title'))
			->append_metadata($this->load->view('fragments/wysiwyg', array(), TRUE))
			->append_css('module::files.css')
			->append_css('module::jquery.fileupload-ui.css')
			->append_js('module::jquery.cooki.js')
			->append_js('module::jquery.fileupload.js')
			->append_js('module::jquery.fileupload-ui.js')
			->append_js('jquery/jquery.tagsinput.js')
			->append_js('module::news_form.js')
			->append_css('jquery/jquery.tagsinput.css')
			->set('related', $related)
			->set('post', $post)
			->build('admin/form');
*/
	}


	/**
	 * Preview news post
	 * 
	 * @param int $id the ID of the news post to preview
	 * @return void
	 */
	public function preview($id = 0)
	{
		if (!$id) redirect('admin/news');
		$post = $this->news_m->get($id);

		$this->template
				->set_layout('modal', 'admin')
				->set('post', $post)
				->build('admin/preview');
	}

	/**
	 * Helper method to determine what to do with selected items from form post
	 * 
	 * @return void
	 */
	public function action()
	{
		switch ($this->input->post('btnAction'))
		{
			case 'publish':
				$this->publish();
			break;
			
			case 'delete':
				$this->delete();
			break;
			
			default:
				redirect('admin/news');
			break;
		}
	}

	/**
	 * Publish news post
	 * 
	 * @param int $id the ID of the news post to make public
	 * @return void
	 */
	public function publish($id = 0)
	{
		role_or_die('news', 'put_live');

		// Publish one
		$ids = ($id) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			// Go through the array of slugs to publish
			$post_titles = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($post = $this->news_m->get($id))
				{
					$this->news_m->publish($id);

					// Wipe cache for this model, the content has changed
					$this->pyrocache->delete('news_m');
					$post_titles[] = $post->title;
				}
			}
		}

		// Some posts have been published
		if ( ! empty($post_titles))
		{
			// Only publishing one post
			if (count($post_titles) == 1)
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('news:publish_success'), $post_titles[0]));
			}
			// Publishing multiple posts
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('news:mass_publish_success'), implode('", "', $post_titles)));
			}
		}
		// For some reason, none of them were published
		else
		{
			$this->session->set_flashdata('notice', $this->lang->line('news:publish_error'));
		}

		redirect('admin/news');
	}

	/**
	 * Delete news post
	 * 
	 * @param int $id the ID of the news post to delete
	 * @return void
	 */
	public function delete($id = 0)
	{
		$this->load->model('comments/comment_m');

		role_or_die('news', 'delete_live');

		// Delete one
		$ids = ($id) ? array($id) : $this->input->post('action_to');

		// Go through the array of slugs to delete
		if ( ! empty($ids))
		{
			$post_titles = array();
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($post = $this->news_m->get($id))
				{
					if ($this->news_m->delete($id))
					{
						$this->comment_m->where('module', 'news')->delete_by('entry_id', $id);

						// Wipe cache for this model, the content has changed
						$this->pyrocache->delete('news_m');
						$post_titles[] = $post->title;
						$deleted_ids[] = $id;
						$this->unlinkfile($post->attachment);
					}
				}
			}
			
			// Fire an event. We've deleted one or more news posts.
			//Events::trigger('post_deleted', $deleted_ids);
		}

		// Some pages have been deleted
		if ( ! empty($post_titles))
		{
			// Only deleting one page
			if (count($post_titles) == 1)
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('news:delete_success'), $post_titles[0]));
			}
			// Deleting multiple pages
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('news:mass_delete_success'), implode('", "', $post_titles)));
			}
		}
		// For some reason, none of them were deleted
		else
		{
			$this->session->set_flashdata('notice', lang('news:delete_error'));
		}

		redirect('admin/news');
	}


	public function upload($news_id = 0)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_file_rules);

		if ($this->form_validation->run())
		{
			// Setup upload config
			$fconfig = $this->upload_cfg;
			$fconfig['upload_path'] .= '/images';
			$this->load->library('upload', $fconfig);

			// check directory exists
			$this->check_dir($fconfig['upload_path']);

			// File upload error
			if ( ! $this->upload->do_upload('userfile'))
			{
				$status		= 'error';
				$message	= $this->upload->display_errors();

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message
					));
				}

				$this->data->messages[$status] = $message;
			}

			// File upload success
			else
			{
				$file = $this->upload->data();

				$data = array(
					'filename'		=> $file['file_name'],
					'news_id'		=> (int) $this->input->post('news_id'),
					'user_id'		=> $this->current_user->id,
					'caption'			=> $this->input->post('slidecaption'),
					'description'	=> $this->input->post('slidedescription'),
					'position'		=> 999
				);

				$this->load->library('image_lib');
				$filename = $file['file_name'];
				$thumbfile = thumbnail($filename); //substr($filename, 0, -4) . '_thumb' . substr($filename, -4);
				/*---------------------------------------------------------------------------------
				// create thumb
				*/
				$image_cfg['source_image'] = $file['full_path'];
				$image_cfg['maintain_ratio'] = TRUE;
				$image_cfg['width'] = ($file['image_width'] < $this->sidebar_thumb_width ? $file['image_width'] : $this->sidebar_thumb_width);
				$image_cfg['height'] = ($file['image_height'] < $this->sidebar_thumb_height ? $file['image_height'] : $this->sidebar_thumb_height);
				$image_cfg['create_thumb'] = FALSE;
				$image_cfg['master_dim'] = 'width';
				$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
				$this->image_lib->initialize($image_cfg);
				$img_ok = $this->image_lib->resize();
				unset($image_cfg);
				$this->image_lib->clear();

				if ($file['image_width'] <> $this->slidew OR $file['image_height'] <> $this->slideh)
				{
					/*
					/* image resize
					*/
					$image_cfg['source_image'] = $file['full_path'];
					$image_cfg['maintain_ratio'] = TRUE;
					$image_cfg['width'] = ($file['image_width'] < $this->slidew ? $file['image_width'] : $this->slidew);
					$image_cfg['height'] = ($file['image_height'] < $this->slideh ? $file['image_height'] : $this->slideh);
					$image_cfg['create_thumb'] = FALSE;
					$image_cfg['master_dim'] = 'width';
					$this->image_lib->initialize($image_cfg);
					$img_ok = $this->image_lib->resize();
					unset($image_cfg);
					$this->image_lib->clear();
				}

				// Insert success
				if ($id = $this->news_m->insertimg($data))
				{
					$status		= 'success';
					$message	= lang('news_upload_img_success');
				}
				// Insert error
				else
				{
					$status		= 'error';
					$message	= lang('news_dbstore_img_error');
				}

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'file'		=> array(
							'name'	=> $file['file_name'],
							'type'	=> $file['file_type'],
							'size'	=> $file['file_size'],
							'thumb'	=> base_url() . 'uploads/news/slides/' . $thumbfile
						)
					));
				}
				else
				{
					$this->session->set_flashdata($status, $message);
					redirect('admin/news');
				}

				if ($status === 'success')
				{
					$this->session->set_flashdata($status, $message);
					redirect('admin/news');
				}
				else
				{
					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);
		
						return $this->template->build_json(array(
							'status'	=> 'error',
							'message'	=> $message
						));
					}
				}
			}
		}
		elseif (validation_errors())
		{
			// if request is ajax return json data, otherwise do normal stuff
			if ($this->input->is_ajax_request())
			{
				$data = array();
				$status = 'error';
				$data['messages'][$status] = validation_errors();
				$message = $this->load->view('admin/partials/notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
		}

		if ($this->input->is_ajax_request())
		{
			// todo: debug errors here
			$status		= 'error';
			$message	= 'unknown';

			$data = array();
			$data['messages'][$status] = $message;
			$message = $this->load->view('admin/partials/notices', $data, TRUE);

			return $this->template->build_json(array(
				'status'	=> $status,
				'message'	=> $message
			));
		}

		$this->template
			->title()
			->build('admin/files/upload', $this->data);
	}

	/**
	 * Get the images just uploaded.
	 *
	 * View file: views/admin/files/contents.php
	 *
	 * @param int ID of the product
	 * @return void
	 *
	 */
	public function getuploaded($id=0)
	{
		$id		= $this->input->post('news_id');

		if (!$id && $this->input->get('news_id'))
		{
			$id = $this->input->get('news_id', TRUE);
		}
		$files = $this->news_m->getuploadedimages($id);
		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);
		$this->template
			->set('files', $files)
			->build('admin/files/contents');
	}

	/**
	 * Delete slide image.
	 * This is an AJAX call
	 *
	 * @param int ID of image to delete
	 */
	public function imgdelete($id=0)
	{
		$imgid = $id ? $id : $this->input->post('imgid');

		if (!$imgid) {
			if ($this->input->is_ajax_request())
			{
				$status = 'error';
				$data = array();
				$data['messages'][$status] = lang('news:delete_img_db_error');
				$message = $this->load->view('admin/partials/notices', $data, TRUE);
				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
			else
			{
				return FALSE;
			}
		}

		// get image detail first
		$file = $this->news_m->getsingleimage($imgid);
		$imagethumb = thumbnail($file->filename);
		$image = $file->filename;

			$stat = array();
			$msg = array();

			if($this->news_m->imgdelete($imgid))
			{

				if (!empty($file))
				{
					// delete the files
					if (file_exists($this->upload_cfg['upload_path'] . '/images/' . $imagethumb))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/images/' . $imagethumb))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('news:delete_img_error'), $this->upload_cfg['upload_path'] . '/images/' . $imagethumb);
						}
					}

					if (file_exists($this->upload_cfg['upload_path'] . '/images/' . $image))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/images/' . $image))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('news:delete_img_error'), $this->upload_cfg['upload_path'] . '/images/' . $image);
						}
					}
	
					if (in_array('error', $stat))
					{
						$status = 'error';
						$message = implode('<br />', $msg);
					}
					else
					{
						$status = 'success';
						$message = sprintf(lang('news:delete_img_success'), $image);
					}
					
				}
				else
				{
					$status = 'success';
					$message = lang('news:delete_img_db_success');
				}

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);
					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message
					));
				}
				else
				{
					if ($status == 'success')
						return TRUE;
					else
						return FALSE;
				}
			}
			else
			{
				if ($this->input->is_ajax_request())
				{
					$status = 'error';
					$data = array();
					$data['messages'][$status] = lang('news:delete_img_db_error');
					$message = $this->load->view('admin/partials/notices', $data, TRUE);
					return $this->template->build_json(array(
						'status'	=> 'error',
						'message'	=> $message
					));
				}
				else
				{
					return FALSE;
				}
			}

	}

	/**
	 *
	 * Re-order image positions
	 *
	 */
	public function imgorder()
	{
		$ids = explode(',', $this->input->post('order'));

		$i = 1;
		foreach ($ids as $id)
		{
			$this->news_m->update_imgorder($id, array(
				'position' => $i
			));
			++$i;
		}
	}

	/**
	 * Callback method that checks the title of an post
	 * 
	 * @param string title The Title to check
	 * @return bool
	 */
	public function _check_title($title, $id = null)
	{
		$this->form_validation->set_message('_check_title', sprintf(lang('news:already_exist_error'), lang('global:title')));
		return $this->news_m->check_exists('title', $title, $id);			
	}
	
	/**
	 * Callback method that checks the slug of an post
	 * 
	 * @param string slug The Slug to check
	 * @return bool
	 */
	public function _check_slug($slug, $id = null)
	{
		$this->form_validation->set_message('_check_slug', sprintf(lang('news:already_exist_error'), lang('global:slug')));
		return $this->news_m->check_exists('slug', $slug, $id);
	}

    private function _preview_hash()
    {

        return md5(microtime() + mt_rand(0,1000));

    }

	public function unlinkfile($filename)
	{
			$thumbfile = thumbnail($filename);

			$stat = array();
					// delete the files
					if (file_exists($this->upload_cfg['upload_path'] . '/' . $thumbfile))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/' . $thumbfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('news_delete_img_error'), $thumbfile);
						}
					}
		
					if (file_exists($this->upload_cfg['upload_path'] . '/' . $filename))
					{
						if (!@unlink($this->upload_cfg['upload_path'] . '/' . $filename))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('news_delete_img_error'), $filename);
						}
					}

					if (in_array('error', $stat))
					{
						$status = 'error';
						$message = implode('<br />', $msg);
					}
					else
					{
						$status = 'success';
						$message = sprintf(lang('news_delete_img_success'), $filename);
					}

		return (array($status, $message));
	}


	public function unlinkfile_temp($filename)
	{
		$filename = str_ireplace(' ', '_', $filename);
		$upload_cfg = $this->upload_cfg;
		$upload_cfg['upload_path'] = UPLOAD_PATH . 'newstmp';

			$thumbfile = thumbnail($filename);

			$stat = array();
					// delete the files
					if (file_exists($upload_cfg['upload_path'] . '/' . $thumbfile))
					{
						if (!@unlink($upload_cfg['upload_path'] . '/' . $thumbfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('news_delete_img_error'), $thumbfile);
						}
					}
		
					if (file_exists($upload_cfg['upload_path'] . '/' . $filename))
					{
						if (!@unlink($upload_cfg['upload_path'] . '/' . $filename))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('news_delete_img_error'), $filename);
						}
					}

					if (in_array('error', $stat))
					{
						$status = 'error';
						$message = implode('<br />', $msg);
					}
					else
					{
						$status = 'success';
						$message = sprintf(lang('news_delete_img_success'), $filename);
					}

		//return (array($status, $message));
	}

	/**
	 * Check attachment dir, and create accordingly
	 *
	 * @param string Directory to check
	 * @return array
	 */
	function check_dir($dir)
	{
		// check directory
		$fileOK = array();
		$fdir = explode('/', $dir);
		$ddir = '';
		for($i=0; $i<count($fdir); $i++)
		{
			$ddir .= $fdir[$i] . '/';
			if (!is_dir($ddir))
			{
				if (!@mkdir($ddir, 0777)) {
					$fileOK[] = 'not_ok';
				}
				else
				{
					$fileOK[] = 'ok';
				}
			}
			else
			{
				$fileOK[] = 'ok';
			}
		}
		return $fileOK;

	}

}
