<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rss extends Public_Controller
{
	function __construct()
	{
		parent::__construct();	
		$this->load->model('news_m');
		$this->load->helper('xml');
		$this->load->helper('date');
		$this->lang->load('news');
	}
	
	function index()
	{
/*
		$posts = $this->pyrocache->model('news_m', 'get_many_by', array(array(
			'status' => 'live',
			'limit' => $this->settings->get('rss_feed_items'))
		), $this->settings->get('rss_cache'));
*/

		$posts = $this->news_m->get_many_by(array('status' => 'live', 'limit' => $this->settings->get('rss_feed_items') ));
		
		$this->output->set_header('Content-Type: application/rss+xml');
		//$html = $this->load->view('rss', $this->_build_feed($posts, $this->lang->line('news_rss_name_suffix')), true);

		$this->load->view('rss', $this->_build_feed($posts, $this->lang->line('news_rss_name_suffix')) );

//		echo $this->parser->parse_string($html, $posts);
	}
	
	function category( $slug = '')
	{
		$this->load->model('news_categories_m');
		
		if(!$category = $this->news_categories_m->get_by('slug', $slug))
		{
			redirect('news/rss/all.rss');
		}
		
		$posts = $this->pyrocache->model('news_m', 'get_many_by', array(array(
			'status' => 'live',
			'category' => $slug,
			'limit' => $this->settings->get('rss_feed_items') )
		), $this->settings->get('rss_cache'));
		
		$this->_build_feed( $posts );		
		//$data->rss->feed_name .= ' '. $category->title . $this->lang->line('news_rss_category_suffix');		
		$this->output->set_header('Content-Type: application/rss+xml');

		$html = $this->load->view('rss', $this->_build_feed($posts, $category->title.$this->lang->line('news:rss_category_suffix')), true);

		echo $this->parser->parse_string($html, $posts);
	}
	
	function _build_feed( $posts = array() )
	{
	$data = new stdClass;
	$data->rss = (object) '';
		$data->rss->encoding = $this->config->item('charset');
		$data->rss->feed_name = $this->settings->get('site_name');
		$data->rss->feed_url = base_url();
		$data->rss->page_description = sprintf($this->lang->line('news_rss_posts_title'), $this->settings->get('site_name'));
		$data->rss->page_language = 'id-id';
		$data->rss->creator_email = $this->settings->get('contact_email');

		if(!empty($posts))
		{
			foreach($posts as $row)
			{
				//$row->created_on = human_to_unix($row->created_on);
				$row->link = site_url('news/' .date('Y/m', (int)$row->created_on) .'/'. $row->slug);
				$row->created_on = standard_date('DATE_RSS', (int)$row->created_on);
				
				$item = array(
					//'author' => $row->author,
					'title' => xml_convert($row->title),
					'link' => $row->link,
					'guid' => $row->link,
					'description'  => $row->lead?$row->lead:$row->intro,
					'date' => $row->created_on,
					'category' => $row->category_title
				);				
				$data->rss->items[] = (object) $item;
			}
		}

		return $data;
	}
}
?>
