(function($) {
	$(function(){
		
		// generate a slug when the user types a title in
		pyro.generate_slug('#news-content-tab input[name="title"]', 'input[name="slug"]');
		
		// needed so that Keywords can return empty JSON
		$.ajaxSetup({
			allowEmpty: true
		});

		$('#keywords').tagsInput({
			autocomplete_url:'admin/keywords/autocomplete'
		});
		
		// editor switcher
		$('select[name^=type]').live('change', function() {
			chunk = $(this).closest('li.editor');
			textarea = $('textarea', chunk);
			
			// Destroy existing WYSIWYG instance
			if (textarea.hasClass('wysiwyg-simple') || textarea.hasClass('wysiwyg-advanced')) 
			{
				textarea.removeClass('wysiwyg-simple');
				textarea.removeClass('wysiwyg-advanced');
					
				var instance = CKEDITOR.instances[textarea.attr('id')];
			    instance && instance.destroy();
			}
			
			
			// Set up the new instance
			textarea.addClass(this.value);
			
			pyro.init_ckeditor();
			
		});
		
		$('ul li.category a').colorbox({
			srollable: false,
			innerWidth: 600,
			innerHeight: 280,
			href: SITE_URL + 'admin/news/categories/create_ajax',
			onComplete: function() {
				$.colorbox.resize();
				$('form#categories').removeAttr('action');
				$('form#categories').live('submit', function(e) {
					
					var form_data = $(this).serialize();
					
					$.ajax({
						url: SITE_URL + 'admin/news/categories/create_ajax',
						type: "POST",
					        data: form_data,
						success: function(obj) {
							
							if(obj.status == 'ok') {
								
								//succesfull db insert do this stuff
								var select = 'select[name=category_id]';
								var opt_val = obj.category_id;
								var opt_text = obj.title;
								var option = '<option value="'+opt_val+'" selected="selected">'+opt_text+'</option>';
								
								//append to dropdown the new option
								$(select).append(option);
								$(select).trigger("liszt:updated");

								// TODO work this out? //uniform workaround
								//$('#news-options-tab li:first span').html(obj.title);
								$('li.category span').html(obj.title);
								
								//close the colorbox
								$.colorbox.close();
							} else {
								//no dice
							
								//append the message to the dom
								$('#cboxLoadedContent').html(obj.message + obj.form);
								$('#cboxLoadedContent p:first').addClass('notification error').show();
							}
						}
						
						
					});
					e.preventDefault();
				});
				
			}
		});

		// multiple file upload
		$('.open-files-uploader').livequery('click', function(){
			$(this).colorbox({
				scrolling	: false,
				inline		: true,
				href		: '#files-uploader',
				width		: '880',
				height		: '90%',
				onComplete	: function(){
					$('#files-uploader-queue').empty();
					$.colorbox.resize();
				},
				onCleanup : function(){
					//$(window).hashchange();
				}
			});
		});

		var upload_form = $('#files-uploader form'),
			upload_vars	= upload_form.data('fileUpload'),
			$loading = $('#cboxLoadingOverlay, #cboxLoadingGraphic');

		upload_form.fileUploadUI({
			fieldName		: 'userfile',
			uploadTable		: $('#files-uploader-queue'),
			downloadTable	: $('#files-uploader-queue'),
			previewSelector	: '.file_upload_preview div',
			buildUploadRow	: function(files, index, handler){
				return $('<li><div class="file_upload_preview ui-corner-all"><div class="ui-corner-all"></div></div>' +
						'<div class="filename"><label for="file-name">' + files[index].name + '</label>' +
						'<input class="file-name" type="hidden" name="name" value="'+files[index].name+'" />' +
						'<div><b>Caption</b>:<br /><input id="slidecaption" size="20" maxlength="100" type="text" name="slidecaption"><br />'+
						'<b>Description</b>:<br /><textarea id="slidedescription" style="width:200px" rows="3" cols="30" name="slidedescription"></textarea></div>'+
						'</div>' +
						'<div class="file_upload_progress"><div></div></div>' +
						'<div class="file_upload_cancel buttons buttons-small">' +
						'<button class="button start ui-helper-hidden-accessible"><span>' + startbtn + '</span></button>'+
						'<button class="button cancel"><span>' + cancelbtn + '</span></button>' +
						'</div>' +
						'</li>');
			},
			buildDownloadRow: function(data){
				if (data.status == 'success')
				{
					return $('<li><div>' + data.file.name + '</div></li>');
				}
				return false;
			},
			beforeSend: function(event, files, index, xhr, handler, callBack){
				handler.uploadRow.find('button.start').click(function(){
					handler.formData = {
						name: handler.uploadRow.find('input.file-name').val(),
						news_id: news_id,
						slidecaption: handler.uploadRow.find('#slidecaption').val(),
						slidedescription: handler.uploadRow.find('#slidedescription').val(),
						csrf_hash_name: $.cookie(pyro.csrf_cookie_name)
					};
					//$loading.show();
					callBack();
				});
			},
			onComplete: function (event, files, index, xhr, handler){
				handler.onCompleteAll(files);
				$.post('admin/news/getuploaded', { news_id: news_id },
					function(data) {
						$('#files-uploaded').html(data);
					}
				);
				//$loading.hide();
			},
			onCompleteAll: function (files){
				if ( ! files.uploadCounter)
				{
					files.uploadCounter = 1;  
				}
				else
				{
					files.uploadCounter = files.uploadCounter + 1;
				}

				if (files.uploadCounter === files.length)
				{
					$('#files-uploader a.cancel-upload').click();
				}
				$.colorbox.close();
//alert('onCompleteAll');
			}
		});

		$('#files-uploader a.start-upload').click(function(e){
			e.preventDefault();
			$('#files-uploader-queue button.start').click();
		});
		
		$('#files-uploader a.cancel-upload').click(function(e){
			e.preventDefault();
			$('#files-uploader-queue button.cancel').click();
			$.colorbox.close();
		});

		// image delete
		$('.actions a.imgdel').livequery('click', function(e){
			var url = $(this).attr('href');
			var imgid = $(this).parent().parent().find('input[name="img_slide_id[]"]').val();
			var imgdiv = $(this).parent().parent();

			e.preventDefault();
			imgdiv.fadeTo('slow',0.3);
			if (!confirm(pyro.lang.dialog_message))
			{
				$(this).parent().parent().fadeTo('slow',1);
				return false;
			}

			$.post( url, { imgid: imgid } )
			.success(function(data) {
				if (data.status == 'error')
				{
					pyro.add_notification(data.message, {method: 'prepend'});
					imgdiv.fadeTo('slow',1);
				}
				else if (data.status == 'success')
				{
					pyro.add_notification(data.message, {method: 'prepend'});
					imgdiv.remove();
				}
			})
			.error(function(data) {
				imgdiv.fadeTo('slow',1);
			});
		});

		$('#2_column ul.sortable').livequery(function(){
			$(this).sortable({
				start: function(event, ui) {
					ui.helper.find('a').unbind('click').die('click');
				},
				update: function() {
					order = new Array();
					$('li', this).each(function(){
						order.push( $(this).find('input[name="img_slide_id[]"]').val() );
					});
					order = order.join(',');
					$.post(SITE_URL + 'admin/news/imgorder', { order: order });
				}
			}).disableSelection();
		});


	});
})(jQuery);