<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * News module
 *
 * @author PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\News
 */
class Module_News extends Module {

	public $version = '2.3';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'News',
				'id' => 'Berita',
			),
			'description' => array(
				'en' => 'Post news entries.',
				'id' => 'Modul berita',
			),
			'frontend'	=> true,
			'backend'	=> true,
			'skip_xss'	=> true,
			'menu'		=> 'content',

			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),

			'sections' => array(
			    'posts' => array(
				    'name' => 'news:posts_title',
				    'uri' => 'admin/news',
				    'shortcuts' => array(
						array(
					 	   'name' => 'news:create_title',
						    'uri' => 'admin/news/create',
						    'class' => 'add'
						),
					),
				),
				'categories' => array(
				    'name' => 'cat_list_title',
				    'uri' => 'admin/news/categories',
				    'shortcuts' => array(
						array(
						    'name' => 'cat_create_title',
						    'uri' => 'admin/news/categories/create',
						    'class' => 'add'
						),
				    ),
			    ),
		    ),
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('news_categories');
		$this->dbforge->drop_table('news_images');
		$this->dbforge->drop_table('news');

		$tables = array(
			'news_categories' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true, 'key' => true),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true),
			),
		);

		$news = "
			CREATE TABLE " . $this->db->dbprefix('news') . " (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `news_year` int(11) NOT NULL DEFAULT '2015',
			  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `title_size` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `title_color` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `subtitle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
			  `category_id` int(11) NOT NULL,
			  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			  `lead` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `intro` text COLLATE utf8_unicode_ci NOT NULL,
			  `body` text COLLATE utf8_unicode_ci NOT NULL,
			  `parsed` text COLLATE utf8_unicode_ci NOT NULL,
			  `keywords` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			  `author_id` int(11) NOT NULL DEFAULT '0',
			  `created_on` int(11) NOT NULL,
			  `updated_on` int(11) NOT NULL DEFAULT '0',
			  `comments_enabled` enum('no','1 day','1 week','2 weeks','1 month','3 months','always') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'always',
			  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
			  `type` set('html','markdown','wysiwyg-advanced','wysiwyg-simple') COLLATE utf8_unicode_ci NOT NULL,
			  `preview_hash` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			  `viewed` int(11) NOT NULL DEFAULT '0',
			  `credits` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			  `headline` int(1) NOT NULL DEFAULT '1',
			  `page_format` varchar(12) COLLATE utf8_unicode_ci DEFAULT '1_column',
			  `optional_images` text COLLATE utf8_unicode_ci,
			  `news_quote` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `related_news` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `external_url` text COLLATE utf8_unicode_ci,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `unique_title` (`title`),
			  KEY `category_id` (`category_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";

		$news_images = "
			CREATE TABLE " . $this->db->dbprefix('news_images') . " (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `news_id` int(11) NOT NULL,
			  `user_id` int(11) NOT NULL DEFAULT '0',
			  `caption` varchar(100) NOT NULL,
			  `description` text NOT NULL,
			  `filename` varchar(64) NOT NULL,
			  `position` int(11) DEFAULT '0',
			  PRIMARY KEY (`id`),
			  KEY `gallery_id` (`filename`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";

		$settings = "
			INSERT INTO " . $this->db->dbprefix('settings') . "
				(`slug`, `title`, `description`, `type`, `default`, `value`, `options`, `is_required`, `is_gui`, `module`, `order`) VALUES
			('newsimg_width', 'News Image Width', 'Width of news image', 'text', '262', '262', '', 0, 1, 'images', 1003),
			('newsimg_height', 'News Image Height', 'Height of news image', 'text', '175', '175', '', 0, 1, 'images', 1004),
			('news_to_twitter', 'Integrate News with Twitter', 'Post update when posting new article', 'radio', '0', '0', '1=Yes|0=No', 0, 1, 'twitter', 1009),
			('twitter_consumer_key', 'Twitter Consumer Key', 'Your Twitter consumer key', 'text', '', '', '', 0, 1, 'twitter', 1008),
			('twitter_consumer_key_secret', 'Twitter Consumer Key Secret', 'Your Twitter consumer key secret', 'text', '', '', '', 0, 1, 'twitter', 1007),
			('twitter_access_token', 'Twitter Access Token', 'Your Twitter access token', 'text', '', '', '', 0, 1, 'twitter', 1006),
			('twitter_access_token_secret', 'Twitter Access Token Secret', 'Your Twitter access token secret', 'text', '', '', '', 0, 1, 'twitter', 1005)
			;
		";

		if ($this->install_tables($tables) && $this->db->query($settings) && $this->db->query($news_images) && $this->db->query($news) )
			return TRUE;

		return FALSE;
	}

	public function uninstall()
	{
		$this->dbforge->drop_table('news_categories');
		$this->dbforge->drop_table('news');
		$this->dbforge->drop_table('news_images');

		$settings = "
			DELETE FROM " . $this->db->dbprefix('settings') . "
			WHERE 
			slug = 'twitter_access_token' OR
			slug = 'twitter_access_token_secret' OR
			slug = 'twitter_consumer_key' OR
			slug = 'twitter_consumer_key_secret' OR
			slug = 'news_to_twitter' OR
			slug = 'newsimg_width' OR
			slug = 'newsimg_height'
			;
		";

		if ( $this->db->query($settings) )
			return TRUE;


		return false;
	}

	public function upgrade($old_version)
	{
		$upg = "ALTER TABLE " . $this->db->dbprefix('news') . " ADD  `related_news` VARCHAR( 255 ) NULL";
		if ( $this->db->query($upg) )
			return TRUE;

		return false;
	}
}
