<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['news:post']                	  = 'News';
$lang['news:posts']                   = 'News';

// labels
$lang['news:posted_label']                   = 'Posted';
$lang['news:posted_label_alt']               = 'Posted at';
$lang['news:external_url_label']             = 'External URL';
$lang['news:written_by_label']				= 'Written by';
$lang['news:author_unknown']				= 'Unknown';
$lang['news:keywords_label']				= 'Keywords';
$lang['news:tagged_label']					= 'Tagged';
$lang['news:title_color_label']             = 'Title Color';
$lang['news:title_size_label']               = 'Title Size';
$lang['news:category_label']                 = 'Category';
$lang['news:post_label']                     = 'Post';
$lang['news:date_label']                     = 'Date';
$lang['news:date_at']                        = 'at';
$lang['news:time_label']                     = 'Time';
$lang['news:status_label']                   = 'Status';
$lang['news:draft_label']                    = 'Draft';
$lang['news:live_label']                     = 'Live';
$lang['news:content_label']                  = 'Content';
$lang['news:options_label']                  = 'Options';
$lang['news:intro_label']                    = 'Introduction';
$lang['news:lead_label']                     = 'Lead';
$lang['news:no_category_select_label']       = '-- None --';
$lang['news:new_category_label']             = 'Add a category';
$lang['news:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['news:all_posts_label']           	 = 'All posts';
$lang['news:posts_of_category_suffix']    	 = ' posts';
$lang['news:rss_name_suffix']                = ' News';
$lang['news:rss_category_suffix']            = ' News';
$lang['news:author_name_label']              = 'Author name';
$lang['news:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['news:created_hour']                   = 'Created on Hour';
$lang['news:created_minute']                 = 'Created on Minute';
$lang['news:comments_enabled_label']         = 'Comments Enabled';
$lang['news:userfile_label']        		 = 'Image Attachment';
$lang['news:credits_label']        		 	 = 'Image Caption';
$lang['news:credits_msg_label']        		 = 'Caption below the image';
$lang['news:delete_image_label']        	 = 'Remove Image';
$lang['news:disabled_after']        	 	 = 'Comments are disabled after %s';

$lang['news:headline_label']        	 	= 'Put this in headline?';
$lang['news:no_label']        	 			= 'No';
$lang['news:yes_label']        	 			= 'Yes';

$lang['news:subtitle_label']        	 	= 'Subtitle';
$lang['news:news_quote_label']        	 	= 'Quote';
$lang['news:page_format_label']        	 	= 'Page Format';
$lang['news:optional_images_label']        	= 'Optional Images';
$lang['news:2_column_label']        	 	= '2 Column';
$lang['news:3_column_label']        	 	= '3 Column';

$lang['news:upload_images_label']        	= 'Upload Images';
$lang['news:slide_image_label']				= 'Optional Images';

// twitter
$lang['news:send_to_twitter_label']        	 = 'Post to Twitter?';
$lang['news:twitter_text'] 					= '%s %s';
$lang['news:twitter_posted'] 				= 'Posted to Twitter "%s" %s';
$lang['news:twitter_error'] 				= 'Twitter Error';

// titles
$lang['news:create_title']                   = 'Add Post';
$lang['news:edit_title']                     = 'Edit post "%s"';
$lang['news:archive_title']                 = 'Archive';
$lang['news:posts_title']					= 'Posts';
$lang['news:rss_posts_title']				= 'News posts for %s';
$lang['news:news_title']					= 'News';
$lang['news:list_title']					= 'List Posts';

// messages
$lang['news:slug_description_msg'] 			= 'Slug will be created automatically. Please do not change this manually unless you have to.';
$lang['news:title_description_msg']          = 'Maximum 250 characters.';

$lang['news:delete_img_db_error']            = 'Error removing image data from database.';
$lang['news:delete_img_db_success']          = 'Image data has been removed from database.';
$lang['news:delete_img_error']             	 = 'Error deleting image file "%s".';
$lang['news:delete_img_success']             = 'Image file "%s" has been deleted.';
$lang['news:slide_image_msg']         		 = 'Images will be resized to 400 x 300 pixels';

$lang['news:no_posts']                    = 'There are no posts.';
$lang['news:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['news:currently_no_posts']          = 'There are no posts at the moment.';
$lang['news:post_add_success']            = 'The post "%s" was added.';
$lang['news:post_add_error']              = 'An error occured.';
$lang['news:edit_success']                   = 'The post "%s" was updated.';
$lang['news:edit_error']                     = 'An error occurred.';
$lang['news:publish_success']                = 'The post "%s" has been published.';
$lang['news:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['news:publish_error']                  = 'No posts were published.';
$lang['news:delete_success']                 = 'The post "%s" has been deleted.';
$lang['news:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['news:delete_error']                   = 'No posts were deleted.';
$lang['news:already_exist_error']            = 'An post with this URL already exists.';

// date
$lang['news:archive_date_format']		= "%B %Y";