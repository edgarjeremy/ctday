<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Show RSS feeds in your site
 * 
 * @package 	PyroCMS\Core\Modules\News\Widgets
 * @author		Phil Sturgeon
 * @author		PyroCMS Development Team
 */
class Widget_Archive extends Widgets
{

        public $title = array(
            'en'         => 'News Archive',
            'id'         => 'Arsip Berita',
        );
        public $description = array(
            'en'     => 'Display a list of old months with links to posts in those months',
            'id'     => 'Menampilkan daftar bulan beserta tautan post di setiap bulannya',
        );
        public $author  = 'Okky Sari';
        public $website = 'http://santalaya.com/';
        public $version = '1.0';

        public function run($options)
        {
                $this->load->model('news/news_m');
                $this->lang->load('news/news');

                return array(
                    'archive_months' => $this->news_m->get_archive_months()
                );
        }

}
