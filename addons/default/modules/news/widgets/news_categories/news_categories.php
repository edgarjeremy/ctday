<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Show a list of news categories.
 * 
 * @author		PyroCMS Dev Team
 * @author		Stephen Cozart
 * @package 	PyroCMS\Core\Modules\News\Widgets
 */
class Widget_News_categories extends Widgets
{

        public $title = array(
            'en'         => 'News Categories',
            'id'         => 'Kateori Berita',
        );
        public $description = array(
            'en'     => 'Show a list of news categories',
            'id'     => 'Menampilkan daftar kategori berita',
        );
        public $author  = 'Stephen Cozart';
        public $website = 'http://github.com/clip/';
        public $version = '1.0';

        public function run()
        {
                $this->load->model('news/news_categories_m');

                $categories = $this->news_categories_m->order_by('title')->get_all();

                return array('categories' => $categories);
        }

}
