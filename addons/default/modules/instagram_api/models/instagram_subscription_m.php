<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Instagram_subscription_m extends MY_Model
{
	protected $_table = 'instagram_subscriptions';

	function __construct()
	{
		parent::__construct();
	}

	function insert_subscription($input=array())
	{
		return $this->db->insert('instagram_subscriptions', $input);
	}

	function get_subscriptions()
	{
	}
}


// end of file models/instagram_m.php