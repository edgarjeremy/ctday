<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Instagram_m extends MY_Model
{
	protected $_table = 'instagram_posts';

	function __construct()
	{
		parent::__construct();
	}

	function insert_batch($input=array())
	{
		return $this->db->insert_batch('instagram_posts', $input);
	}

	function get_many_by($params=array())
	{
		if (!empty($params['hashtag']))
		{
			$this->db->where('hashtag', $params['hashtag']);
		}
		
		if(!empty($params['year']))
    	{
			$this->db->where('YEAR(FROM_UNIXTIME(instagram_time))', $params['year']);
    	}
		
		if (!isset($params['year']))
		{
			$this->db->where('YEAR(FROM_UNIXTIME(instagram_time))', date("Y"));
		}

		if (!empty($params['order_by']))
		{
			$this->db->order_by($params['order_by'], ( !empty($params['sort']) ? $params['sort'] : 'desc' ) );
		}

		if (!empty($params['finalist']))
		{
			$this->db->where('finalist', '1');
		}

		return $this->get_all();
	}

	function check_instagram_id($id)
	{
		$ret = $this->db->where('instagram_object_id', $id)->get('instagram_posts')->row();
		if ($ret)
			return $ret->instagram_object_id;
		else
			return false;
		//return $this->db->where('instagram_object_id', $id)->count_all_results('instagram_posts');
	}

	function update_instagram($instagram_object_id, $update)
	{
		return $this->db->where('instagram_object_id', $instagram_object_id)->update('instagram_posts', $update);
	}

	function insert_subscription($input=array())
	{
		return $this->db->insert('instagram_subscriptions', $input);
	}

	function get_subscriptions()
	{
		return $this->db->get('instagram_subscriptions')->result();
	}

	function record_push($input)
	{
		return $this->db->insert('instagram_receive', array('received_data'=>$input, 'created_on'=>now()));
	}

	function get_record()
	{
		return $this->db
			->where('processed != 1')
			->or_where('processed IS NULL')
			->limit(5)
			->order_by('created_on')
			->get('instagram_receive')->result();
	}

	function mark_as_finalist($id=0)
	{
		return $this->db->where('instagram_object_id', $id)->update('instagram_posts', array('finalist'=>1));
	}

	function remove_nomination($id=0)
	{
		return $this->db->where('instagram_object_id', $id)->update('instagram_posts', array('finalist'=>'0'));
	}

	function get_by_instagram_id($id)
	{
		return $this->db->where('instagram_object_id', $id)->get('instagram_posts')->row();
	}
	
	function get_year(){
		$this->db->select('instagram_posts.*')->order_by( 'YEAR(FROM_UNIXTIME(instagram_time))','desc' );
		return $this->db->get('instagram_posts')->result();
	}
	
	function count_by($params = array())
    {
    	if(!empty($params['year']))
    	{
			$this->db->where('YEAR(FROM_UNIXTIME(instagram_time))', $params['year']);
    	}
		
		if (!isset($params['year']))
		{
			$this->db->where('YEAR(FROM_UNIXTIME(instagram_time))', date("Y"));
		}
		
		return $this->db->count_all_results('instagram_posts');
    }
}


// end of file models/instagram_m.php