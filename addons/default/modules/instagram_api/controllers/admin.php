<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends Admin_Controller
{
	protected $instagram_obj;
	
	/* years */
	protected $current_year;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('instagram_m');
		$this->lang->load('instagram');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			$this->session->set_flashdata('error', 'Please insert your Instagram API key & secret first.');
			redirect('admin/settings#instagram');
		}
		
		$f_year_backend = $this->instagram_m->get_year();
		$current_year = 0;
		foreach($f_year_backend as $y)
		{
			$this->current_year = ($current_year < date('Y', $y->instagram_time)) ? date('Y', $y->instagram_time) : $current_year;
			$filter_year[date('Y', $y->instagram_time)] = date('Y', $y->instagram_time);
		}
		
		$deadline_gmt = gmmktime(16, 5, 0, 6, 9, 2015);
		$deadline = mktime(16, 5, 0, 6, 9, 2015);
		$now = time(); //mktime(14, 5, 0, 6, 8, 2015);
		$gmt_to_local = gmt_to_local($now);
		$gmt = local_to_gmt($now);
		$wib = $gmt + (60*60*7);

		/* if ($wib < $deadline)
		{
			echo "Deadline belum lewat<br />";
		}
		else
		{
			echo "Deadline <b>sudah</b> lewat<br />";
		} */
		
		$this->template
			->set('wib', $wib)
			->set('deadline', $deadline)
			->set('filter_year', $filter_year)
			;
	}

	public function index($sort='likes')
	{
		$base_where = array();

		if ($sort <> 'likes' AND $sort <> 'instagram_time')
			$sort = 'likes';

		if ($sort=='instagram_time')
			$sort = 'instagram_time';

		$base_where['order_by'] = $sort;
		
		if ($this->input->post('f_year'))
		{
			$base_where['year'] = $this->input->post('f_year');
		}else{
			//$base_where['year'] = $f_datenow;
			$base_where['year'] = date('Y');
		}
		
		// Create pagination links
		$total_rows = $this->instagram_m->count_by($base_where);
		$pagination = create_pagination('admin/instagram_api/index/'.$sort.'/', $total_rows, NULL, 5);
		
		// Using this data, get the relevant results
		$posts = $this->instagram_m->limit($pagination['limit'], $pagination['offset'])->get_many_by($base_where);

		//do we need to unset the layout because the request is ajax?
		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);
		
		$this->template
			->title('Instagram Feeds')
			->append_css('module::files.css')
			->set('sort', $sort)
			->set('total_rows', $total_rows)
			->set('posts', $posts)
			->append_js('admin/filter.js')
			->set_partial('filters', 'admin/partials/filters')
			->set('pagination', $pagination)
			//->build('admin/index')
			;
			
		$this->input->is_ajax_request()
			? $this->template->build('admin/tables/posts')
			: $this->template->build('admin/index');
	}

	public function finalist($id=0)
	{
		$ids = ($id) ? array($id) : $this->input->post('finalists');

		// Go through the array of ids to mark
		if ( ! empty($ids))
		{
			$users = array();
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($post = $this->instagram_m->get_by('instagram_object_id', $id))
				{
					if ($this->instagram_m->mark_as_finalist($id))
					{
						// Wipe cache for this model, the content has changed
						$this->pyrocache->delete_all('instagram_m');
						$users[] = $post->user?$post->user:$post->username;
					}
				}
			}

		}

		// Some pages have been deleted
		if ( ! empty($users))
		{
			// Only deleting one page
			if (count($users) == 1)
			{
				$this->session->set_flashdata('success', sprintf('Entry by %s has been marked as finalist.', $users[0]));
			}
			// Deleting multiple pages
			else
			{
				$this->session->set_flashdata('success', sprintf('Entries by "%s" have been marked as finalists', implode('", "', $users)));
			}
		}
		// For some reason, none of them were marked
		else
		{
			$this->session->set_flashdata('notice', 'No entries has been marked as finalist');
		}

		redirect('admin/instagram_api');

	}

}


// end of controllers/admin.php