<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Admin_Subscriptions extends Admin_Controller
{
	protected $section = 'subscription';
	protected $instagram_obj;

	protected $validation_rules = array(
		array(
			'field' => 'hashtag',
			'label' => 'Hashtag',
			'rules' => 'trim|required'
		),
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->model('instagram_subscription_m');
		$this->lang->load('instagram');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			$this->session->set_flashdata('error', 'Please insert your Instagram API key & secret first.');
			redirect('admin/settings#instagram');
		}
	}

	public function index()
	{
		$base_where = array();

		// Create pagination links
		$total_rows = $this->instagram_subscription_m->count_by($base_where);
		$pagination = create_pagination('admin/instagram_api/subscriptions/index', $total_rows);

		// Using this data, get the relevant results
		$subscriptions = $this->instagram_subscription_m->limit($pagination['limit'], $pagination['offset'])->get_many_by($base_where);

		$this->template
			->title('Instagram Subscriptions')
			->set('subscriptions', $subscriptions)
			->build('admin/subscriptions/index');
	}

	public function subscribe()
	{
		$hashtag = new stdClass();
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validation_rules);
		if ($this->form_validation->run())
		{
			$fields = array(
				'client_id'		=> $this->settings->instagram_api_key,
				'client_secret'	=> $this->settings->instagram_api_secret,
				'object'		=> 'tag',
				'aspect'		=> 'media',
				'object_id'		=> $this->input->post('hashtag'),
				'callback_url'	=> site_url('instagram_api/api')
			);
			$res = $this->fetchURL('https://api.instagram.com/v1/subscriptions/', $fields);
			$this->template->set('debug', $res);
			$res_decoded = json_decode($res);

			if ($res_decoded->meta->code=='400')
			{
				$this->template->set('err_msg', 'Error subscribing to Instagram. Message from Instagram: '.$res_decoded->meta->error_message);
			}
			elseif ($res_decoded->meta->code=='200')
			{
				$input = array(
					'date_subscribed'		=> now(),
					'hashtag'				=> $this->input->post('hashtag'),
					'subscription_id'		=> $res_decoded->data->id,
					'data_type'				=> $res_decoded->data->type,
					'data_object'			=> $res_decoded->data->object,
					'data_aspect'			=> $res_decoded->data->aspect,
					'data_callback_url'		=> $res_decoded->data->callback_url,
				);
				if ($this->instagram_subscription_m->insert_subscription($input))
				{
					$this->session->set_flashdata('success', 'You are now subscribed to #'.$this->input->post('hashtag'));
					redirect('admin/instagram_api/subscriptions');
				}
				else
				{
					$this->template->set('err_msg', 'Error storing subscription data to our database. Please send this info to the developer:<br /><pre>'.$res.'</pre>');
				}
			}

			$this->template->set('res', $res_decoded);

			$hashtag->hashtag = $this->input->post('hashtag');
		}

		$hashtag->hashtag = set_value('hashtag');
		$this->template
			->set('hashtag', $hashtag)
			->build('admin/subscriptions/form');
	}

	function fetchURL($endpoint, $fields)
	{
		$fields_string = '';
		foreach($fields as $key=>$value)
		{
			$fields_string .= $key.'='.$value.'&';
		}
		rtrim($fields_string, '&');

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $endpoint);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		return $result;
	}

}


// end of controllers/admin.php