<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Admin_finalists extends Admin_Controller
{
	protected $section = 'finalist';
	protected $instagram_obj;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('instagram_m');
		$this->lang->load('instagram');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			$this->session->set_flashdata('error', 'Please insert your Instagram API key & secret first.');
			redirect('admin/settings#instagram');
		}
	}

	public function index($sort='likes')
	{
		$base_where = array('finalist'=>TRUE);

		if ($sort <> 'likes' AND $sort <> 'date')
			$sort = 'date';

		if ($sort=='date')
			$sort = 'instagram_time';

		// Create pagination links
		$total_rows = $this->instagram_m->count_by($base_where);
		$pagination = create_pagination('admin/instagram_api/index', $total_rows);

		$base_where['order_by'] = $sort;
		// Using this data, get the relevant results
		$posts = $this->instagram_m->limit($pagination['limit'], $pagination['offset'])->get_many_by($base_where);

		$this->template
			->title('Instagram Feeds')
			->append_css('module::files.css')
			->set('total_rows', $total_rows)
			->set('posts', $posts)
			->set('pagination', $pagination)
			->build('admin/finalist');
	}

	public function remove()
	{
		$id = $this->input->post('id');
		if (!$id)
		{
			return $this->template->build_json(array(
				'message' => 'Could not complete your request.',
				'status' => 'error'
			));
		}

		$entry = $this->instagram_m->get_by_instagram_id($id);
		$ret = $this->instagram_m->remove_nomination($id);
		if ($ret)
		{
			return $this->template->build_json(array(
				'message' => 'Entry by "'.($entry->user?$entry->user:$entry->username).'" has been removed from nominations.',
				'status' => 'ok'
			));
		}
		else
		{
			return $this->template->build_json(array(
				'message' => 'Could not remove entry by "'.($entry->user?$entry->user:$entry->username).'"',
				'status' => 'error'
			));
		}

	}

}


// end of controllers/admin.php