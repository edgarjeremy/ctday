<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Instagram_api extends Public_Controller
{
	protected $instagram; // instagram library

	public function __construct()
	{
		parent::__construct();
		$this->load->model('instagram_m');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			$this->session->set_flashdata('error', 'Please insert your API key & secret first.');
			redirect('settings');
		}

		$this->instagram = $this->instagram_wrapper->init(
			array(
				'apiKey' => $this->settings->instagram_api_key,
				'apiSecret'   => $this->settings->instagram_api_secret,
				'apiCallback' => site_url('instagram_api/api'),//'http://labs.local/Instagram-PHP-API/example/hashtag.php'
			)
		);
	}

	public function index()
	{
		echo md5('subscribe-to-instagram')."<br />\n";
		echo "This is the index page";
	}

	public function api()
	{
		$xhs_value = '';
		if ($this->input->server('REQUEST_METHOD') == 'GET')
		{
			$hub_mode = $this->input->get('hub_mode');
			$hub_challenge = $this->input->get('hub_challenge');
			$hub_verify_token = $this->input->get('hub_verify_token');
			echo $hub_challenge;
		}
		else if ($this->input->server('REQUEST_METHOD') == 'POST')
		{
			$input = file_get_contents('php://input');
			$digest = hash_hmac( 'sha1', $input, $this->settings->instagram_api_secret);

			$headers = apache_request_headers();
			foreach($headers as $header => $value)
			{
				if ($header=='X-Hub-Signature')
				{
					$xhs_value = $value;
					//if ($value !== $digest)
					//	return false;
					//else
					//	break;
				}
			}

			//$feeds = json_decode($input);
			$post = serialize(array('input'=>$input, 'post'=>$_POST, 'digest'=>$digest, 'x-hub-signature'=>$xhs_value));
			$this->instagram_m->record_push($post);
		}
		
	}

	public function realtime_processXYZ($key='')
	{
		$hashtag = 'noplace4plastic';
		$subscriptions = array();
		if (!$key) return false;
		if ($key <> $this->settings->instagram_api_key) return false;
		$subscriptions = $this->instagram_m->get_subscriptions();

		$result = $this->instagram->getTagMedia($hashtag, 20);

		if (!empty($result->pagination))
			$this->get_next($result->pagination, $subscription);
	}

	public function realtime_process($key='')
	{
		echo "<pre>\n";

		//                   ,-- this number is always +7 than the real deadline
		//                   v
		$deadline = gmmktime(23, 5, 0, 6, 9, 2016);
		$now = time(); //mktime(14, 5, 0, 6, 8, 2015);
		$gmt = local_to_gmt($now);
		$wib = $gmt + (60*60*7);
		?>
		$deadline: <?=$deadline?> - <? echo date('d F Y H:i', $deadline) ?> (server time)
		$now: <?=$now?> - <? echo date('d F Y H:i', $now) ?>

		$wib: <?=$wib?> - <? echo date('d F Y H:i', $wib) ?>
		<?
		if ($wib > $deadline)
			exit('Deadline ('.date('d F Y, H:i', $deadline).') has passed!');


		$num = 1;
		$hashtag = 'noplace4plastic';
		$subscriptions = array();
		if (!$key) return false;
		if ($key <> $this->settings->instagram_api_key) return false;
		$subscriptions = $this->instagram_m->get_subscriptions();

		//echo "\$subscriptions: $subscriptions<br />\n";

				$result = $this->instagram->getTagMedia($hashtag, 20);

				if (!empty($result->data))
				{
					$now = now();
					$input = array();
					foreach ($result->data as $media)
					{
						$past = false;

						echo "$num. Processing <a href=\"".$media->link."\">".$media->link."</a> (".$media->likes->count." likes) by <a href=\"https://instagram.com/".$media->user->username."\">".$media->user->username."</a>\n";
						$num++;

						//foreach($input_time as $time)
						//{
						//	if ($media->created_time < $time)
						//	{
						//		$past = true;
						//		break;
						//	}
						//}
						//if ($media->type === 'video' OR $media->filter <> 'Normal' OR $past)
						//{
						//	continue;
						//}

						$instagram_object_exists = $this->instagram_m->check_instagram_id($media->id);
						if (!$instagram_object_exists)
						{
							$input[] = array(
								'subscription_id'			=> '0',
								'hashtag'					=> $hashtag,
								'instagram_object_id'		=> $media->id,
								'instagram_time'			=> $media->created_time,
								'updated_on'				=> $now,
								'link'						=> $media->link,
								'likes'						=> $media->likes->count,
								'images'					=> serialize($media->images),
								'user'						=> utf8_encode($media->user->full_name),
								'username'					=> $media->user->username,
								'profile_picture'			=> $media->user->profile_picture,
								'user_id'					=> $media->user->id,
								'caption'					=> utf8_encode($media->caption->text),
								'location'					=> serialize($media->location),
							);
						}
						else
						{
							$update = array(
								'subscription_id'			=> '0',
								'hashtag'					=> $hashtag,
								'instagram_object_id'		=> $media->id,
								'instagram_time'			=> $media->created_time,
								'updated_on'				=> $now,
								'link'						=> $media->link,
								'likes'						=> $media->likes->count,
								'images'					=> serialize($media->images),
								'user'						=> utf8_encode($media->user->full_name),
								'username'					=> $media->user->username,
								'profile_picture'			=> $media->user->profile_picture,
								'user_id'					=> $media->user->id,
								'caption'					=> utf8_encode($media->caption->text),
								'location'					=> serialize($media->location),
							);
							$this->instagram_m->update_instagram($instagram_object_exists, $update);
						}
					}

					if (!empty($input))
					{
						$this->instagram_m->insert_batch($input);
					}

					if (!empty($result->pagination))
						$this->get_next($result->pagination);
				}
	}

	public function get_next($pagination, $subscription='')
	{
		$hashtag = 'noplace4plastic';
		//static $result;
		static $idx = 21;
		$min_tag_id = $max_tag_id = NULL;

		if (!empty($pagination->min_tag_id))
			$min_tag_id = $pagination->min_tag_id;

		if (!empty($pagination->next_max_tag_id))
			$max_tag_id = $pagination->next_max_tag_id;

		$result = $this->instagram->getTagMedia($hashtag, 20, array('max_tag_id'=>$max_tag_id));

		if (!empty($result->data))
		{

			$now = now();
			$input = array();
			foreach ($result->data as $media)
			{
				$past = false;

				echo "$idx. Processing <a href=\"".$media->link."\">".$media->link."</a> (".$media->likes->count." likes) by <a href=\"https://instagram.com/".$media->user->username."\">".$media->user->username."</a>\n";
				$idx++;

				//foreach($input_time as $time)
				//{
				//	if ($media->created_time < $time)
				//	{
				//		$past = true;
				//		break;
				//	}
				//}
				//if ($media->type === 'video' OR $media->filter <> 'Normal' OR $past)
				//{
				//	continue;
				//}
				$instagram_object_exists = $this->instagram_m->check_instagram_id($media->id);
				if (!$instagram_object_exists)
				{
					$input[] = array(
						'subscription_id'			=> '0',
						'hashtag'					=> $hashtag,
						'instagram_object_id'		=> $media->id,
						'instagram_time'			=> $media->created_time,
						'updated_on'				=> $now,
						'link'						=> $media->link,
						'likes'						=> $media->likes->count,
						'images'					=> serialize($media->images),
						'user'						=> utf8_encode($media->user->full_name),
						'username'					=> $media->user->username,
						'profile_picture'			=> $media->user->profile_picture,
						'user_id'					=> $media->user->id,
						'caption'					=> utf8_encode($media->caption->text),
						'location'					=> serialize($media->location),
					);
				}
				else
				{
					$update = array(
						'subscription_id'			=> '0',
						'hashtag'					=> $hashtag,
						'instagram_object_id'		=> $media->id,
						'instagram_time'			=> $media->created_time,
						'updated_on'				=> $now,
						'link'						=> $media->link,
						'likes'						=> $media->likes->count,
						'images'					=> serialize($media->images),
						'user'						=> utf8_encode($media->user->full_name),
						'username'					=> $media->user->username,
						'profile_picture'			=> $media->user->profile_picture,
						'user_id'					=> $media->user->id,
						'caption'					=> utf8_encode($media->caption->text),
						'location'					=> serialize($media->location),
					);
					$this->instagram_m->update_instagram($instagram_object_exists, $update);
				}
			}
			if (!empty($input))
			{
				$this->instagram_m->insert_batch($input);
			}

			if (!empty($result->pagination->next_max_tag_id))
				$this->get_next($result->pagination);
		}
echo "</pre>\n";
	}

	public function realtime_processXX($key='')
	{
		$hashtag = 'noplace4plastic';
		$subscriptions = array();
		if (!$key) return false;
		if ($key <> $this->settings->instagram_api_key) return false;
		$feeds = $this->instagram_m->get_subscriptions();

echo "<pre>\n";
echo "\$feeds:\n";
print_r($feeds);
echo "<hr /></pre>\n";
exit;

		if (!empty($feeds))
		{
			foreach($feeds as $feed)
			{
				// read and get feeds
				$received_data = unserialize($feed->received_data);
				$input_value_tmp = json_decode($received_data['input']);
				foreach($input_value_tmp as $ivt)
				{
					$input_time[] = $ivt->time;
					$subscriptions[] = array(
						'hashtag' => $ivt->object_id,
						'subscription_id' => $ivt->subscription_id,
					);
				}
				$input_value[] = $input_value_tmp;
			}

echo "<pre>\n";
echo "\$subscriptions:\n";
print_r($subscriptions);
echo "<hr /></pre>\n";
exit;
			foreach($subscriptions as $subscription)
			{
				$result = $this->instagram->getTagMedia($subscription['hashtag'], 10);
				if (!empty($result->data))
				{
					$now = now();
					$input = array();
					foreach ($result->data as $media)
					{
						$past = false;
						//foreach($input_time as $time)
						//{
						//	if ($media->created_time < $time)
						//	{
						//		$past = true;
						//		break;
						//	}
						//}
						//if ($media->type === 'video' OR $media->filter <> 'Normal' OR $past)
						//{
						//	continue;
						//}
						if (!$this->instagram_m->check_instagram_id($media->id))
						{
							$input[] = array(
								'subscription_id'			=> $subscription['subscription_id'],
								'hashtag'					=> $subscription['hashtag'],
								'instagram_object_id'		=> $media->id,
								'instagram_time'			=> $media->created_time,
								'updated_on'				=> $now,
								'link'						=> $media->link,
								'likes'						=> $media->likes->count,
								'images'					=> serialize($media->images),
								'user'						=> utf8_encode($media->user->full_name),
								'username'					=> $media->user->username,
								'profile_picture'			=> $media->user->profile_picture,
								'user_id'					=> $media->user->id,
								'caption'					=> utf8_encode($media->caption->text),
								'location'					=> serialize($media->location),
							);
						}
					}
					if (!empty($input))
					{
						$this->instagram_m->insert_batch($input);
					}
				}
			}
		}
	}

	function get_more($max_tag_id=0, $hashtag='noplace4plastic')
	{
		if ( !$max_tag_id OR !$hashtag ) return 'Nothing to work on..<br />';
		$result = $this->instagram->getTagMedia($hashtag, 18, array('max_tag_id'=>$max_tag_id));

		$view = NULL;
		if (!empty($result->data))
		{
			$view = $this->load->view('ajax/more', array('data'=>$result), true);
			$max_tag_id = !empty($result->pagination->next_max_id) ? $result->pagination->next_max_id : 0;
//			echo json_encode(array('status'=>'ok', 'feeds'=>$view, 'max_tag_id'=>$max_tag_id));
echo "\$max_tag_id: $max_tag_id<br />\n";
?>
<pre>
$result->pagination:
<? print_r($result->pagination) ?>
</pre>
<?
echo $view;
			exit;
		}
		echo $view;
		//return json_encode($this->_process_results($result, $hashtag));
	}

	private function _process_results($result, $hashtag)
	{
		$ret = array();
		if (!empty($result->data))
		{
			$ret = array(
				'min_tag_id'				=> !empty($result->pagination->min_tag_id)?$result->pagination->min_tag_id:'',
				'max_tag_id'				=> !empty($result->pagination->max_tag_id)?$result->pagination->max_tag_id:'',
				'next_url'					=> !empty($result->pagination->next_url)?$result->pagination->next_url:'',
				'prev_url'					=> !empty($result->pagination->prev_url)?$result->pagination->prev_url:'',
			);
			foreach ($result->data as $media)
			{
/*
				if ($media->type === 'video')
				{
					$videos[] = 'video';
					continue;
				}
*/
				$feeds[] = array(
					'hashtag'					=> $hashtag,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->created_time,
					'link'						=> $media->link,
					'likes'						=> $media->likes->count,
					'images'					=> $media->images,
					'user'						=> $media->user->full_name,
					'username'					=> $media->user->username,
					'profile_picture'			=> $media->user->profile_picture,
					'user_id'					=> $media->user->id,
					'caption'					=> $media->caption->text,
					'location'					=> $media->location,
				);
			}
			if (!empty($feeds))
			{
				$ret['feeds'] = $feeds;
			}
		}
		return $ret;
	}

}

// end of controllers/instagram.php