<section class="title">
	<h4><?php echo lang('instagram:posts_title'); ?> | Total entries: <b id="total_entries"><?php echo $total_rows ?></b></h4>
</section>

<section class="item">
<div class="content">

<p>
	Sort by <a href="admin/instagram_api/index/date">Date</a> or by <a href="admin/instagram_api/index/likes">Likes</a>
</p>

<?php if (!empty($posts)): ?>

<?php echo form_open('admin/instagram_api/finalist', 'class="crud"'); ?>

<?php if ($pagination['links']): ?>
	<div class="pagination">
		<?php echo $pagination['links']; ?>

	<!--
		<div class="buttons align-right padding-top clear-both">
			<button type="submit" name="btnAction" value="finalists" class="btn red blue"><span>Process Finalists</span></button>
		</div>
	-->
	</div>
<?php endif; ?>

<ul id="files-uploaded" class="grid clearfix">
	<?php foreach($posts as $post): ?>
		<?php $images = unserialize($post->images); ?>
		<li class="ui-corner-all" style="height:300px;padding-top:20px;">
			<a href="<?php echo $images->standard_resolution->url?>" target="_blank"><img src="<?php echo $images->thumbnail->url ?>"></a><br />
			<div class="meta">
				Likes: <?php echo number_format($post->likes); ?> | <a target="_blank" href="<?php echo $post->link ?>">View @ Instagram</a><br />
				User: <a href="https://instagram.com/<?php echo $post->username ?>" target="_blank"><?php echo $post->user?$post->user:$post->username ?></a><br />
				Date: <?php echo date('j M Y, H:i', $post->instagram_time); ?>
			</div>
			<div class="actions">
				<a href="#" class="btn small red remove_finalist" rel="<?php echo $post->instagram_object_id ?>">Cancel nomination</a>
			</div>
		</li>
	<?php endforeach; ?>
</ul>

<!--
	<div class="buttons align-right padding-top clear-both">
		<button type="submit" name="btnAction" value="finalists" class="btn red blue"><span>Process Finalists</span></button>
	</div>
-->

<?php echo form_close() ?>

<div class="pagination">
	<?php echo $pagination['links']; ?>
</div>

<?php else: ?>

	<div class="no_data">Feeds are not available at the moment</div>

<?php endif; ?>

</div>
</section>

<script>
$(document).ready(function(){

	$('a.remove_finalist').click(function(e){
		e.preventDefault();
		var ioid = $(this).attr('rel');
		var $parent = $(this).parent().parent();
		$parent.css('opacity', '0.5');
		if (!confirm('Are you sure you want to cancel the nomination?'))
		{
			$parent.css('opacity', '1');
			return false;
		}

		$.ajax({
			type: 'POST',
			url: '<?php echo site_url('admin/instagram_api/adminfinalist/remove') ?>',
			data: {id: ioid}
		}).
		done(function(obj){
			if (obj.status=='ok')
			{
				$parent.fadeOut(function(){
					$(this).remove();
					var te = $('#total_entries').html();
					te = te - 1;
					$('#total_entries').html(te);
				}).remove();
			}
			pyro.add_notification('<div class="alert '+(obj.status=='ok'?'success':'error')+'"><p>'+obj.message+'</p></div>')
			$parent.css('opacity', '1');
		});

	});

});
</script>