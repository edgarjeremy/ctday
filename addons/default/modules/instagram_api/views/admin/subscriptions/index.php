<section class="title">
	<h4>Instagram Subscriptions</h4>
</section>

<section class="item">
<div class="content">

<pre>
$subscriptions:
<? print_r($subscriptions) ?>
</pre>


	<?php echo form_open('admin/instagram_api/subscriptions/action'); ?>

<?php if ($subscriptions) : ?>
	<table border="0" class="table-list">
		<thead>
			<tr>
				<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')); ?></th>
				<th width="40%">Hashtag</th>
				<th class="collapse">Date Subscribed</th>
				<th class="collapse">Instagram<br />Subscription ID</th>
				<th width="190"></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="8">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ($subscriptions as $post) : ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $post->id); ?></td>
					<td><?php echo $post->hashtag; ?></td>
					<td class="collapse"><?php echo date("d/m/Y H:i", $post->date_subscribed); ?></td>
					<td class="collapse"><?php echo $post->subscription_id; ?></td>
					<td>
						<?php echo anchor('admin/news/edit/' . $post->id, lang('global:edit'), 'class="button  edit"'); ?>
						<?php echo anchor('admin/news/delete/' . $post->id, lang('global:delete'), array('class'=>'confirm button  delete')); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="no_data">You have not subscribed to any hashtags. <a href="<?php echo site_url('admin/instagram_api/subscriptions/subscribe') ?>">Click here to subscribe one now</a>.</div>
<?php endif; ?>

		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete', 'publish'))); ?>
		</div>

	<?php echo form_close(); ?>

</div>
</section>
