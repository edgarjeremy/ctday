<section class="title">
	<h4>Subscribe to hashtag</h4>
</section>

<section class="item">
<div class="content">

<?php echo form_open($this->uri->uri_string(), 'class="crud" id="subscribe"'); ?>

<?php if (!empty($err_msg)): ?>
<div class="alert error animated fadeIn">
<p><?php echo($err_msg); ?></p>
</div>
<?php endif; ?>

<div class="form_inputs">

	<ul>
		<li class="even">
			<label for="hashtag">Hashtag: <span>*</span></label>
			<div class="input"><?php echo '#'.form_input('hashtag', $hashtag->hashtag); ?></div>
		</li>
	</ul>
	
</div>

	<div>
		<button type="submit" name="btnAction" value="save" class="btn blue"><span>Subscribe</span></button>
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('cancel') )); ?>
	</div>

<?php echo form_close(); ?>

<?php if (!empty($res)): ?>
<hr />
<pre>
<?php print_r($res); ?>
</pre>
<?php endif; ?>

<?php if (!empty($debug)): ?>
<hr />
<pre>
<?php print_r($debug); ?>
</pre>
<?php endif; ?>


</div>
</section>