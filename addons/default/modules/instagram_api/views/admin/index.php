<?
		//                    ,-- this number is always +7 than the real deadline
		//                   v
		$deadline = gmmktime(23, 5, 0, 6, 9, 2015);
		//$deadline = gmmktime(22, 5, 0, 6, 8, 2015); // 8 June 2015, 15:00 WIB
		$now = time(); //mktime(14, 5, 0, 6, 8, 2015);
		$gmt_to_local = gmt_to_local($now);
		$gmt = local_to_gmt($now);
		$wib = $gmt + (60*60*7);
?>

<style>
ul.grid > li.ui-corner-all.finalist {
	background: #ddd;
}
</style>
<section class="title">
	<h4><?php echo lang('instagram:posts_title'); ?> | Total entries: <b><?php echo $total_rows ?></b></h4>
</section>

<section class="item">
<div class="content">

<p>
	Sort by <a href="admin/instagram_api/index/instagram_time">Date</a> or by <a href="admin/instagram_api/index/likes">Likes</a>
</p>

<?php $this->load->view('admin/partials/filters'); ?>

<div id="filter-stage">
	<?php $this->load->view('admin/tables/posts'); ?>
</div>

</div>
</section>
<script>
$(document).ready(function(e){

		$('a.refreshfeeds').livequery(function() {
			var h = $(this).attr('href');
			var $loading = $('#cboxLoadingOverlay, #cboxLoadingGraphic');
			$(this).colorbox({
				width: "80%",
				height: "90%",
				fixed: true,
				overlayClose: false,
				escKey: false,
				html: '<p style="font-size:1.2em;height:25px;width:100%;text-align:center;">Please wait, fetching data from Instagram<br /></p>',
				onLoad: function(){
					//$('#cboxLoadedContent').html();
				},
				onComplete: function(){
					$.ajax({
						url: h,
						success: function(data){
							$('#cboxLoadedContent').html(data);
							//$.colorbox.resize();
						}
					})
					//$.colorbox.resize();
				},
				onClosed: function(){
					location.reload(true);
				}
			});
		});

});
</script>