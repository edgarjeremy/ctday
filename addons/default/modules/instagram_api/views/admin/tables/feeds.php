<?php if ($news) : ?>
	<table border="0" class="table-list">
		<thead>
			<tr>
				<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')); ?></th>
				<th width="40%"><?php echo lang('instagram:post_label'); ?></th>
				<th class="collapse"><?php echo lang('instagram:category_label'); ?></th>
				<th class="collapse"><?php echo lang('instagram:date_label'); ?></th>
				<th class="collapse"><?php echo lang('instagram:written_by_label'); ?></th>
				<th class="collapse"><?php echo lang('instagram:status_label'); ?></th>
				<th class="collapse">Viewed</th>
				<th width="190"></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="8">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ($news as $post) : ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $post->id); ?></td>
					<td><?php echo $post->title; ?></td>
					<td class="collapse"><?php echo $post->category_title; ?></td>
					<td class="collapse"><?php echo date("d/m/Y H:i", $post->created_on); ?></td>
					<td class="collapse">
					<?php if (isset($post->display_name)): ?>
						<?php echo anchor('user/' . $post->author_id, $post->display_name, 'target="_blank"'); ?>
					<?php else: ?>
						<?php echo lang('news_author_unknown'); ?>
					<?php endif; ?>
					</td>
					<td><?php echo lang('instagram:'.$post->status.'_label'); ?></td>
					<td class="collapse"><?php echo $post->viewed; ?></td>
					<td>

                        <?php if($post->status=='live') : ?>
                            <?php echo anchor('news/' . date('Y/m',$post->created_on). '/'. $post->slug, lang('global:view'), 'class="button modal-large" target="_blank"');?>
                        <?php else: ?>
                            <?php echo anchor('news/preview/' . $post->preview_hash, lang('global:preview'), 'class="button  modal-large" target="_blank"');?>
                        <?php endif; ?>
						<?php echo anchor('admin/news/edit/' . $post->id, lang('global:edit'), 'class="button  edit"'); ?>
						<?php echo anchor('admin/news/delete/' . $post->id, lang('global:delete'), array('class'=>'confirm button  delete')); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="no_data"><?php echo lang('instagram:currently_no_posts'); ?></div>
<?php endif; ?>