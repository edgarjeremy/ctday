<?php if (!empty($posts)): ?>

<?php echo form_open('admin/instagram_api/finalist', 'class="crud"'); ?>

<div class="pagination">
	<?php echo $pagination['links']; ?>

	<div class="buttons align-right padding-top clear-both">
		<button type="submit" name="btnAction" value="finalists" class="btn blue"><span>Process Finalists</span></button>
		<?php if ($wib < $deadline): ?>
			<a href="<?php echo site_url('instagram_api/realtime_process/'.$this->settings->instagram_api_key); ?>" class="btn orange refreshfeeds">Refresh Feeds</a>
		<?php endif; ?>
	</div>
</div>

<ul id="files-uploaded" class="grid clearfix">
	<?php foreach($posts as $post): ?>
		<?php $images = unserialize($post->images); ?>
		<li class="ui-corner-all <?php echo $post->finalist?'finalist':''; ?>">
			<div class="actions">
				<?php if ($post->finalist): ?>
					<label>Finalist</label>
				<?php else: ?>
					<label class="inline" title="Please mark images page by page"><input type="checkbox" name="finalists[]" <?php echo $post->finalist?'disabled="disabled"':''; ?> value="<?php echo $post->instagram_object_id ?>" /> Mark as a finalist
				<?php endif; ?>
			</div>
			<a href="<?php echo $images->standard_resolution->url?>" target="_blank"><img src="<?php echo $images->thumbnail->url ?>"></a>
			<div class="meta">
				Likes: <?php echo number_format($post->likes); ?> | <a target="_blank" href="<?php echo $post->link ?>">View @ Instagram</a><br />
				User: <a href="https://instagram.com/<?php echo $post->username ?>" target="_blank"><?php echo $post->user?$post->user:$post->username ?></a><br />
				Date: <?php echo date('j M Y, H:i', $post->instagram_time); ?>
			</div>
		</li>
	<?php endforeach; ?>
</ul>


	<div class="buttons align-right padding-top clear-both">
		<button type="submit" name="btnAction" value="finalists" class="btn red blue"><span>Process Finalists</span></button>
		<?php if ($wib < $deadline): ?>
			<a href="<?php echo site_url('instagram_api/realtime_process/'.$this->settings->instagram_api_key); ?>" class="btn orange refreshfeeds">Refresh Feeds</a>
		<?php endif; ?>
	</div>

<?php echo form_close() ?>

<div class="pagination">
	<?php echo $pagination['links']; ?>
</div>

<?php else: ?>

	<div class="no_data">Feeds are not available at the moment</div>

<?php endif; ?>