<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php foreach($data->data as $feed): ?>
	<div class="col-sm-2 instagram-feeds">
		<div class="insta_meta_wrapper">
			<div class="instagram_meta">
				<p>By <a href="https://instagram.com/<?php echo $feed->user->username ?>/" target="_blank"><?php echo $feed->user->full_name ? $feed->user->full_name : $feed->user->username ?></a><br />
				Likes: <?php echo $feed->likes->count ?></p>
				<p><a class="insta_link" href="<?php echo $feed->link ?>" target="_blank">View on Instagram</a></p>
			</div>
		</div>
		<div class="instagram_img_wrapper">
			<img class="instagram_img img-responsive" data-pyroimage="true" src="<?php echo $feed->images->low_resolution->url ?>" />
		</div>
	</div>
<?php endforeach; ?>