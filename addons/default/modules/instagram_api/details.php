<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * News module
 *
 * @author PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\News
 */
class Module_Instagram_api extends Module {

	public $version = '2.2.5';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Instagram',
			),
			'description' => array(
				'en' => 'Instagram',
			),
			'frontend'	=> true,
			'backend'	=> true,
			'skip_xss'	=> true,
			'menu'		=> 'content',

			'roles' => array(
				'get_subscription', 'new_subscription', 'delete_subscription'
			),

			'sections' => array(
				'posts' => array(
				    'name' => 'instagram:posts_label',
				    'uri' => 'admin/instagram_api',
			    ),
				'finalist' => array(
				    'name' => 'instagram:finalists_label',
				    'uri' => 'admin/instagram_api/adminfinalist',
			    ),
/*
			    'subscription' => array(
				    'name' => 'instagram:subscription_label',
				    'uri' => 'admin/instagram_api/subscriptions',
				    'shortcuts' => array(
						array(
					 	   'name' => 'instagram:new_subscription_label',
						    'uri' => 'admin/instagram_api/subscriptions/subscribe',
						    'class' => 'add'
						),
					),
				),
*/
		    ),
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('instagram_subscriptions');
		$this->dbforge->drop_table('instagram_posts');

		$settings = "
			DELETE FROM " . $this->db->dbprefix('settings') . "
			WHERE 
			slug = 'instagram_api_key' OR
			slug = 'instagram_api_secret'
			;
		";
		$this->db->query($settings);

		$subscriptions = "
			CREATE TABLE " . $this->db->dbprefix('instagram_subscriptions') . " (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `date_subscribed` int(11) NOT NULL,
			  `subscription_id` int(11) NOT NULL,
			  `hashtag` varchar(32) NOT NULL,
			  `data_type` varchar(12) NOT NULL,
			  `data_object` varchar(12) NOT NULL,
			  `data_aspect` varchar(12) NOT NULL,
			  `data_callback_url` varchar(128) NOT NULL,
			  `position` int(11) DEFAULT '0',
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";


		$posts = "
			CREATE TABLE " . $this->db->dbprefix('instagram_posts') . " (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `subscription_id` int(11) NOT NULL DEFAULT '0',
			  `instagram_object` varchar(12) NOT NULL,
			  `instagram_object_id` varchar(64) NOT NULL,
			  `instagram_changed_aspect` varchar(64) NOT NULL,
			  `instagram_time` int(11) NOT NULL,
			  `updated_on` int(11) NOT NULL DEFAULT '0',
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";

		$settings = "
			INSERT INTO " . $this->db->dbprefix('settings') . "
				(`slug`, `title`, `description`, `type`, `default`, `value`, `options`, `is_required`, `is_gui`, `module`, `order`) VALUES
			('instagram_api_key', 'Instagram API Key', 'Your Instagram API key', 'text', '', '', '', 0, 1, 'instagram', 1008),
			('instagram_api_secret', 'Instagram API Secret', 'Your Instagram API secret', 'text', '', '', '', 0, 1, 'instagram', 1007)
			;
		";

		if ( $this->db->query($subscriptions) && $this->db->query($settings) && $this->db->query($posts) )
			return TRUE;

		return FALSE;
	}

	public function uninstall()
	{
		$this->dbforge->drop_table('instagram_posts');
		$this->dbforge->drop_table('instagram_subscriptions');

		$settings = "
			DELETE FROM " . $this->db->dbprefix('settings') . "
			WHERE 
			slug = 'instagram_api_key' OR
			slug = 'instagram_api_secret'
			;
		";

		if ( $this->db->query($settings) )
			return TRUE;


		return false;
	}

	public function upgrade($old_version)
	{
		return TRUE;
	}
}
