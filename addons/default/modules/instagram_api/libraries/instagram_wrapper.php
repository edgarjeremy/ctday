<?php

require_once("instagram/Instagram.php");
use MetzWeb\Instagram\Instagram;

class instagram_wrapper
{
	private $my_instagram;
	public $my_font_path;

	public function __construct()
	{

		/* Create and populate the pData object */
		//$this->MyData = new pData();
		//$this->MyData->loadPalette("pchart/palettes/summer.color",TRUE);
	}

	public function init($params=array())
	{
		$this->my_instagram = new Instagram( array(
				'apiKey' 		=> $params['apiKey'],
				'apiSecret'		=> $params['apiSecret'],
				'apiCallback'	=> $params['apiCallback']
			)
		);
		return $this->my_instagram;
		//$this->MyData->loadPalette(realpath('.') . '/addons/shared_addons/libraries/pchart/palettes/'.$palette.'.color', TRUE);
		//return $this->MyData;
	}

}