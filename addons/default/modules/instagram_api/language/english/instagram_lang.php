<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['instagram:posts_title']			= 'Instagram Feeds';

$lang['instagram:new_subscription_label'] = 'Subscribe';

$lang['instagram:posts_label']			= 'Feeds';
$lang['instagram:subscription_label']	= 'Subscription';
$lang['instagram:finalists_label']		= 'Finalists';

