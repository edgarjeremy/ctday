<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['instagram:role_get_subscription']		= 'Get subscription';
$lang['instagram:role_new_subscription']	= 'Create new subscription';
$lang['instagram:role_delete_subscription'] 	= 'Delete subscription';
