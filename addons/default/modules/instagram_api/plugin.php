<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Instagram Plugin
 *
 * Get Instagram feeds
 * 
 * @author		Catalyze Web Team
 * @package		PyroCMS\Addons\Modules\Instagram_API\Plugins
 */
class Plugin_Instagram_api extends Plugin
{

	public $version = '1.0';
	public $name = array(
		'en' => 'Instagram',
	);
	public $description = array(
		'en' => 'Gets Instagram feeds',
	);

	protected $instagram;

	public function feeds2016()
	{
		$limit		= $this->attribute('limit', 12);
		$hashtag	= $this->attribute('hashtag', 'noplace4plastic');

		$result = array();

		$instagram_keys = array(
			'apiKey' => $this->settings->instagram_api_key,
			'apiSecret'   => $this->settings->instagram_api_secret,
			'apiCallback' => site_url('instagram_api/code'),
		);

		$this->load->model('instagram_m');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			return false;
		}

		$this->instagram = $this->instagram_wrapper->init($instagram_keys);

		$videos = array();

		// instagram api test
		$loginurl = $this->instagram->getLoginUrl();
		//$loginurl = 'https://api.instagram.com/oauth/authorize/?client_id='.$instagram_keys['apiKey'].'&redirect_uri='.$instagram_keys['apiCallback'].'&response_type=token';
		$loginurl = 'https://www.instagram.com/explore/tags/noplace4plastic/';
		$logincallback = $this->fetchInstagram($loginurl);

		$output = $logincallback; //"<html><head></head><body><!-- Your stuff --></body></html>"
		$content = '';
		$js = '';
		$test = '';

		// 1) Grab <body>
		preg_match_all('#(<body[^>]*>.*?<\/body>)#ims', $output, $body);
		$content = implode('',$body[0]);

		// 2) Find <script>s in <body>
		preg_match_all('#<script(.*?)>(.*?)<\/script>#is', $content, $matches);

		// by OS
		/*
		echo "<pre>\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
		echo "\$matches[2]:\n";
		print_r($matches[2]);
		echo "</pre>";
		*/
		$parsed = false;
		foreach($matches[2] as $value)
		{
			if (substr($value, 0, 21)=='window._sharedData = ')
			{
				$json_input = (trim(str_ireplace('window._sharedData = ', '', $value), ';'));
				$parsed = true;
				break;
			}
		}

		if (!$parsed)
			return false;

		//echo "\$json_input <b>FOUND!</b>:<br />";
		//echo $json_input;

		$decoded_json = json_decode($json_input);

		if (json_last_error() !== JSON_ERROR_NONE)
		{
			return false;
		}

		/* 
		$posts = $decoded_json->entry_data->TagPage[0]->tag->media->nodes;
		$top_posts = $decoded_json->entry_data->TagPage[0]->tag->top_posts->nodes;
		 */

		$posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_media->edges;
		$top_posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_top_posts->edges;
		 
		/*
		echo "<pre>\n";
		echo "\$posts:\n";
		print_r($posts);
		echo "\n \$media (ends):\n";
		echo "<hr />\n";
		echo "\$top_posts:\n";
		print_r($top_posts);
		echo "</pre>\n";
		exit;
		*/

		/*
		foreach ($matches[0] as $value) {
			$test .= htmlspecialchars($value) . "<br />\n<br />\n";
			$js .= '<!-- Moved from [body] --> '.$value;
		}

		// 3) Remove <script>s from <body>
		$content2 = preg_replace('#<script(.*?)<\/script>#is', '<!-- Moved to [/body] -->', $content); 

		// 4) Add <script>s to bottom of <body>
		$content2 = preg_replace('#<body(.*?)</body>#is', '<body$1'.$js.'</body>', $content2);

		// 5) Replace <body> with new <body>
		$output = str_replace($content, $content2, $output);

				//$access_token = $this->instagram->getAccessToken();

				//$result = $this->instagram->getTagMedia($hashtag, $limit);
		*/

		$feeds2016 = $ret = array();

		if (!empty($posts))
		{
			foreach ($posts as $idx=>$media)
			{
				if ($idx>=18)
					break;

				$feeds2016[] = array(
					/* 
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->owner->id,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->date,
					'link'						=> 'https://instagram.com/p/'.$media->code.'/?tagged='.$hashtag,
					'likes'						=> $media->likes->count,
					'images'					=> $media->thumbnail_src, 
					*/
					//'full_name'				=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					//'username'				=> $media->user->username,
					//'profile_picture'			=> $media->user->profile_picture,
					// 'caption'				=> $media->caption,
					//'location'				=> $media->location,
					
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->node->owner->id,
					'instagram_object_id'		=> $media->node->id,
					'instagram_time'			=> date('Y-m-d', $media->node->taken_at_timestamp),
					'link'						=> 'https://instagram.com/p/'.$media->node->shortcode.'/?tagged='.$hashtag,
					'likes'						=> $media->node->edge_media_preview_like->count,
					'images'					=> $media->node->thumbnail_src,
				);
			}
		}
		else
		{
			return false;
		}

		/*
		if (!empty($result->data))
		{
			$ret = array(
				'next_max_tag_id'			=> !empty($result->pagination->next_max_tag_id)?$result->pagination->next_max_tag_id:'',
				'min_tag_id'				=> !empty($result->pagination->min_tag_id)?$result->pagination->min_tag_id:'',
				'max_tag_id'				=> !empty($result->pagination->max_tag_id)?$result->pagination->max_tag_id:'',
				'next_url'					=> !empty($result->pagination->next_url)?$result->pagination->next_url:'',
				'prev_url'					=> !empty($result->pagination->prev_url)?$result->pagination->prev_url:'',
			);
			foreach ($result->data as $media)
			{
				$feeds2016[] = array(
					'hashtag'					=> $hashtag,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->created_time,
					'link'						=> $media->link,
					'likes'						=> $media->likes->count,
					'images'					=> $media->images,
					'full_name'					=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					'username'					=> $media->user->username,
					'profile_picture'			=> $media->user->profile_picture,
					'user_id'					=> $media->user->id,
					'caption'					=> $media->caption->text,
					'location'					=> $media->location,
				);
			}
			if (!empty($feeds2016))
			{
				$ret['feeds2016'] = $feeds2016;
			}
		}
		else
		{
			return false;
		}
		*/

		/*
		echo "<pre>\n";
		echo "\n\n\n\n\n\n\n";
		print_r(array('pagination'=>$ret, 'feeds2016'=>$feeds2016));
		echo "</pre>\n";
		*/	

		return $feeds2016;
		//return array('pagination'=>$ret, 'feeds2016'=>$feeds2016);
	}
	
	public function feeds2015()
	{
		$limit		= $this->attribute('limit', 12);
		$hashtag	= $this->attribute('hashtag', 'coraltriangle');
		
		$result = array();

		$instagram_keys = array(
			'apiKey' => $this->settings->instagram_api_key,
			'apiSecret'   => $this->settings->instagram_api_secret,
			'apiCallback' => site_url('instagram_api/code'),
		);

		$this->load->model('instagram_m');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			return false;
		}

		$this->instagram = $this->instagram_wrapper->init($instagram_keys);

		$videos = array();

		// instagram api test
		$loginurl = $this->instagram->getLoginUrl();
		//$loginurl = 'https://api.instagram.com/oauth/authorize/?client_id='.$instagram_keys['apiKey'].'&redirect_uri='.$instagram_keys['apiCallback'].'&response_type=token';
		$loginurl = 'https://www.instagram.com/explore/tags/coraltriangle/';
		$logincallback = $this->fetchInstagram($loginurl);

		$output = $logincallback; //"<html><head></head><body><!-- Your stuff --></body></html>"
		$content = '';
		$js = '';
		$test = '';

		// 1) Grab <body>
		preg_match_all('#(<body[^>]*>.*?<\/body>)#ims', $output, $body);
		$content = implode('',$body[0]);

		// 2) Find <script>s in <body>
		preg_match_all('#<script(.*?)>(.*?)<\/script>#is', $content, $matches);

		// by OS
		/*
		echo "<pre>\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
		echo "\$matches[2]:\n";
		print_r($matches[2]);
		echo "</pre>";
		*/
		$parsed = false;
		foreach($matches[2] as $value)
		{
			if (substr($value, 0, 21)=='window._sharedData = ')
			{
				$json_input = (trim(str_ireplace('window._sharedData = ', '', $value), ';'));
				$parsed = true;
				break;
			}
		}

		if (!$parsed)
			return false;

		//echo "\$json_input <b>FOUND!</b>:<br />";
		//echo $json_input;

		$decoded_json = json_decode($json_input);

		if (json_last_error() !== JSON_ERROR_NONE)
		{
			return false;
		}

		/* 
		$posts = $decoded_json->entry_data->TagPage[0]->tag->media->nodes;
		$top_posts = $decoded_json->entry_data->TagPage[0]->tag->top_posts->nodes;
		 */
		
		$posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_media->edges;
		$top_posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_top_posts->edges;
		
		/*
		echo "<pre>\n";
		echo "\$posts:\n";
		print_r($posts);
		echo "\n \$media (ends):\n";
		echo "<hr />\n";
		echo "\$top_posts:\n";
		print_r($top_posts);
		echo "</pre>\n";
		exit;
		*/

		/*
		foreach ($matches[0] as $value) {
			$test .= htmlspecialchars($value) . "<br />\n<br />\n";
			$js .= '<!-- Moved from [body] --> '.$value;
		}

		// 3) Remove <script>s from <body>
		$content2 = preg_replace('#<script(.*?)<\/script>#is', '<!-- Moved to [/body] -->', $content); 

		// 4) Add <script>s to bottom of <body>
		$content2 = preg_replace('#<body(.*?)</body>#is', '<body$1'.$js.'</body>', $content2);

		// 5) Replace <body> with new <body>
		$output = str_replace($content, $content2, $output);

				//$access_token = $this->instagram->getAccessToken();

				//$result = $this->instagram->getTagMedia($hashtag, $limit);
		*/

		$feeds2015 = $ret = array();

		if (!empty($posts))
		{
			foreach ($posts as $idx=>$media)
			{
				if ($idx>=$limit)
					break;

				$feeds2015[] = array(
					/* 
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->owner->id,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->date,
					'link'						=> 'https://instagram.com/p/'.$media->code.'/?tagged='.$hashtag,
					'likes'						=> $media->likes->count,
					'images'					=> $media->thumbnail_src, 
					*/
					//'full_name'				=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					//'username'				=> $media->user->username,
					//'profile_picture'			=> $media->user->profile_picture,
					// 'caption'				=> $media->caption,
					//'location'				=> $media->location,
					
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->node->owner->id,
					'instagram_object_id'		=> $media->node->id,
					'instagram_time'			=> date('Y-m-d', $media->node->taken_at_timestamp),
					'link'						=> 'https://instagram.com/p/'.$media->node->shortcode.'/?tagged='.$hashtag,
					'likes'						=> $media->node->edge_media_preview_like->count,
					'images'					=> $media->node->thumbnail_src,
				);
			}
		}
		else
		{
			return false;
		}

		/*
		if (!empty($result->data))
		{
			$ret = array(
				'next_max_tag_id'			=> !empty($result->pagination->next_max_tag_id)?$result->pagination->next_max_tag_id:'',
				'min_tag_id'				=> !empty($result->pagination->min_tag_id)?$result->pagination->min_tag_id:'',
				'max_tag_id'				=> !empty($result->pagination->max_tag_id)?$result->pagination->max_tag_id:'',
				'next_url'					=> !empty($result->pagination->next_url)?$result->pagination->next_url:'',
				'prev_url'					=> !empty($result->pagination->prev_url)?$result->pagination->prev_url:'',
			);
			foreach ($result->data as $media)
			{
				$feeds2015[] = array(
					'hashtag'					=> $hashtag,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->created_time,
					'link'						=> $media->link,
					'likes'						=> $media->likes->count,
					'images'					=> $media->images,
					'full_name'					=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					'username'					=> $media->user->username,
					'profile_picture'			=> $media->user->profile_picture,
					'user_id'					=> $media->user->id,
					'caption'					=> $media->caption->text,
					'location'					=> $media->location,
				);
			}
			if (!empty($feeds2015))
			{
				$ret['feeds2015'] = $feeds2015;
			}
		}
		else
		{
			return false;
		}
		*/

		/*
		echo "<pre>\n";
		echo "\n\n\n\n\n\n\n";
		print_r(array('pagination'=>$ret, 'feeds2015'=>$feeds2015));
		echo "</pre>\n";
		*/

		return $feeds2015;
		//return array('pagination'=>$ret, 'feeds2015'=>$feeds2015);
	}

	public function feeds2017()
	{
		$limit		= $this->attribute('limit', 12);
		$hashtag	= $this->attribute('hashtag', 'sayno2plastic');
		
		$result = array();

		$instagram_keys = array(
			'apiKey' => $this->settings->instagram_api_key,
			'apiSecret'   => $this->settings->instagram_api_secret,
			'apiCallback' => site_url('instagram_api/code'),
		);

		$this->load->model('instagram_m');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			return false;
		}

		$this->instagram = $this->instagram_wrapper->init($instagram_keys);

		$videos = array();

		// instagram api test
		$loginurl = $this->instagram->getLoginUrl();
		//$loginurl = 'https://api.instagram.com/oauth/authorize/?client_id='.$instagram_keys['apiKey'].'&redirect_uri='.$instagram_keys['apiCallback'].'&response_type=token';
		$loginurl = 'https://www.instagram.com/explore/tags/sayno2plastic/';
		$logincallback = $this->fetchInstagram($loginurl);
		
		$output = $logincallback; //"<html><head></head><body><!-- Your stuff --></body></html>"
		$content = '';
		$js = '';
		$test = '';

		// 1) Grab <body>
		preg_match_all('#(<body[^>]*>.*?<\/body>)#ims', $output, $body);
		$content = implode('',$body[0]);

		// 2) Find <script>s in <body>
		preg_match_all('#<script(.*?)>(.*?)<\/script>#is', $content, $matches);

		// by OS
		/*
		echo "<pre>\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
		echo "\$matches[2]:\n";
		print_r($matches[2]);
		echo "</pre>";
		*/
		$parsed = false;
		foreach($matches[2] as $value)
		{
			if (substr($value, 0, 21)=='window._sharedData = ')
			{
				$json_input = (trim(str_ireplace('window._sharedData = ', '', $value), ';'));
				$parsed = true;
				break;
			}
		}

		if (!$parsed)
			return false;

		//echo "\$json_input <b>FOUND!</b>:<br />";
		//echo $json_input;

		$decoded_json = json_decode($json_input);

		if (json_last_error() !== JSON_ERROR_NONE)
		{
			return false;
		}
		
		/* 
		$posts = $decoded_json->entry_data->TagPage[0]->tag->media->nodes;
		$top_posts = $decoded_json->entry_data->TagPage[0]->tag->top_posts->nodes;
		*/
		
		$posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_media->edges;
		$top_posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_top_posts->edges;
		
		/*
		echo "<pre>\n";
		echo "\$posts:\n";
		print_r($posts);
		echo "\n \$media (ends):\n";
		echo "<hr />\n";
		echo "\$top_posts:\n";
		print_r($top_posts);
		echo "</pre>\n";
		exit;
		*/

		/*
		foreach ($matches[0] as $value) {
			$test .= htmlspecialchars($value) . "<br />\n<br />\n";
			$js .= '<!-- Moved from [body] --> '.$value;
		}

		// 3) Remove <script>s from <body>
		$content2 = preg_replace('#<script(.*?)<\/script>#is', '<!-- Moved to [/body] -->', $content); 

		// 4) Add <script>s to bottom of <body>
		$content2 = preg_replace('#<body(.*?)</body>#is', '<body$1'.$js.'</body>', $content2);

		// 5) Replace <body> with new <body>
		$output = str_replace($content, $content2, $output);

				//$access_token = $this->instagram->getAccessToken();

				//$result = $this->instagram->getTagMedia($hashtag, $limit);
		*/

		$feeds2018 = $ret = array();

		if (!empty($posts))
		{
			foreach ($posts as $idx=>$media)
			{
				if ($idx>=$limit)
					break;

				$feeds2018[] = array(
					/* 
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->owner->id,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->date,
					'link'						=> 'https://instagram.com/p/'.$media->code.'/?tagged='.$hashtag,
					'likes'						=> $media->likes->count,
					'images'					=> $media->thumbnail_src, 
					*/
					//'full_name'				=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					//'username'				=> $media->user->username,
					//'profile_picture'			=> $media->user->profile_picture,
					// 'caption'				=> $media->caption,
					//'location'				=> $media->location,
					
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->node->owner->id,
					'instagram_object_id'		=> $media->node->id,
					'instagram_time'			=> date('Y-m-d', $media->node->taken_at_timestamp),
					'link'						=> 'https://instagram.com/p/'.$media->node->shortcode.'/?tagged='.$hashtag,
					'likes'						=> $media->node->edge_media_preview_like->count,
					'images'					=> $media->node->thumbnail_src,
				);
			}
		}
		else
		{
			return false;
		}

		/*
		if (!empty($result->data))
		{
			$ret = array(
				'next_max_tag_id'			=> !empty($result->pagination->next_max_tag_id)?$result->pagination->next_max_tag_id:'',
				'min_tag_id'				=> !empty($result->pagination->min_tag_id)?$result->pagination->min_tag_id:'',
				'max_tag_id'				=> !empty($result->pagination->max_tag_id)?$result->pagination->max_tag_id:'',
				'next_url'					=> !empty($result->pagination->next_url)?$result->pagination->next_url:'',
				'prev_url'					=> !empty($result->pagination->prev_url)?$result->pagination->prev_url:'',
			);
			foreach ($result->data as $media)
			{
				$feeds2018[] = array(
					'hashtag'					=> $hashtag,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->created_time,
					'link'						=> $media->link,
					'likes'						=> $media->likes->count,
					'images'					=> $media->images,
					'full_name'					=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					'username'					=> $media->user->username,
					'profile_picture'			=> $media->user->profile_picture,
					'user_id'					=> $media->user->id,
					'caption'					=> $media->caption->text,
					'location'					=> $media->location,
				);
			}
			if (!empty($feeds2018))
			{
				$ret['feeds2017'] = $feeds2018;
			}
		}
		else
		{
			return false;
		}
		*/

		/*
		echo "<pre>\n";
		echo "\n\n\n\n\n\n\n";
		print_r(array('pagination'=>$ret, 'feeds2017'=>$feeds2018));
		echo "</pre>\n";
		*/
		// return [];
		return $feeds2018;
		//return array('pagination'=>$ret, 'feeds2017'=>$feeds2018);
	}
	
	public function feeds2018() {
		$limit		= $this->attribute('limit', 12);
		$hashtag	= $this->attribute('hashtag', 'it');
		
		$result = array();

		$instagram_keys = array(
			'apiKey' => $this->settings->instagram_api_key,
			'apiSecret'   => $this->settings->instagram_api_secret,
			'apiCallback' => site_url('instagram_api/code'),
		);

		$this->load->model('instagram_m');
		$this->load->library('instagram_wrapper');

		if (!$this->settings->instagram_api_key OR !$this->settings->instagram_api_secret)
		{
			return false;
		}

		$this->instagram = $this->instagram_wrapper->init($instagram_keys);

		$videos = array();

		// instagram api test
		$loginurl = $this->instagram->getLoginUrl();
		//$loginurl = 'https://api.instagram.com/oauth/authorize/?client_id='.$instagram_keys['apiKey'].'&redirect_uri='.$instagram_keys['apiCallback'].'&response_type=token';
		$loginurl = 'https://www.instagram.com/explore/tags/plasticresistance/';
		$logincallback = $this->fetchInstagram($loginurl);
		
		$output = $logincallback; //"<html><head></head><body><!-- Your stuff --></body></html>"
		$content = '';
		$js = '';
		$test = '';

		// 1) Grab <body>
		preg_match_all('#(<body[^>]*>.*?<\/body>)#ims', $output, $body);
		$content = implode('',$body[0]);

		// 2) Find <script>s in <body>
		preg_match_all('#<script(.*?)>(.*?)<\/script>#is', $content, $matches);

		// by OS
		/*
		echo "<pre>\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
		echo "\$matches[2]:\n";
		print_r($matches[2]);
		echo "</pre>";
		*/
		$parsed = false;
		foreach($matches[2] as $value)
		{
			if (substr($value, 0, 21)=='window._sharedData = ')
			{
				$json_input = (trim(str_ireplace('window._sharedData = ', '', $value), ';'));
				$parsed = true;
				break;
			}
		}

		if (!$parsed)
			return false;

		//echo "\$json_input <b>FOUND!</b>:<br />";
		//echo $json_input;

		$decoded_json = json_decode($json_input);

		if (json_last_error() !== JSON_ERROR_NONE)
		{
			return false;
		}
		
		/* 
		$posts = $decoded_json->entry_data->TagPage[0]->tag->media->nodes;
		$top_posts = $decoded_json->entry_data->TagPage[0]->tag->top_posts->nodes;
		*/
		
		$posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_media->edges;
		$top_posts = $decoded_json->entry_data->TagPage[0]->graphql->hashtag->edge_hashtag_to_top_posts->edges;
		
		/*
		echo "<pre>\n";
		echo "\$posts:\n";
		print_r($posts);
		echo "\n \$media (ends):\n";
		echo "<hr />\n";
		echo "\$top_posts:\n";
		print_r($top_posts);
		echo "</pre>\n";
		exit;
		*/

		/*
		foreach ($matches[0] as $value) {
			$test .= htmlspecialchars($value) . "<br />\n<br />\n";
			$js .= '<!-- Moved from [body] --> '.$value;
		}

		// 3) Remove <script>s from <body>
		$content2 = preg_replace('#<script(.*?)<\/script>#is', '<!-- Moved to [/body] -->', $content); 

		// 4) Add <script>s to bottom of <body>
		$content2 = preg_replace('#<body(.*?)</body>#is', '<body$1'.$js.'</body>', $content2);

		// 5) Replace <body> with new <body>
		$output = str_replace($content, $content2, $output);

				//$access_token = $this->instagram->getAccessToken();

				//$result = $this->instagram->getTagMedia($hashtag, $limit);
		*/

		$feeds2018 = $ret = array();

		if (!empty($posts))
		{
			foreach ($posts as $idx=>$media)
			{
				if ($idx>=$limit)
					break;

				$feeds2018[] = array(
					/* 
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->owner->id,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->date,
					'link'						=> 'https://instagram.com/p/'.$media->code.'/?tagged='.$hashtag,
					'likes'						=> $media->likes->count,
					'images'					=> $media->thumbnail_src, 
					*/
					//'full_name'				=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					//'username'				=> $media->user->username,
					//'profile_picture'			=> $media->user->profile_picture,
					// 'caption'				=> $media->caption,
					//'location'				=> $media->location,
					
					'hashtag'					=> $hashtag,
					'user_id'					=> $media->node->owner->id,
					'instagram_object_id'		=> $media->node->id,
					'instagram_time'			=> date('Y-m-d', $media->node->taken_at_timestamp),
					'link'						=> 'https://instagram.com/p/'.$media->node->shortcode.'/?tagged='.$hashtag,
					'likes'						=> $media->node->edge_media_preview_like->count,
					'images'					=> $media->node->thumbnail_src,
				);
			}
		}
		else
		{
			return false;
		}

		/*
		if (!empty($result->data))
		{
			$ret = array(
				'next_max_tag_id'			=> !empty($result->pagination->next_max_tag_id)?$result->pagination->next_max_tag_id:'',
				'min_tag_id'				=> !empty($result->pagination->min_tag_id)?$result->pagination->min_tag_id:'',
				'max_tag_id'				=> !empty($result->pagination->max_tag_id)?$result->pagination->max_tag_id:'',
				'next_url'					=> !empty($result->pagination->next_url)?$result->pagination->next_url:'',
				'prev_url'					=> !empty($result->pagination->prev_url)?$result->pagination->prev_url:'',
			);
			foreach ($result->data as $media)
			{
				$feeds2018[] = array(
					'hashtag'					=> $hashtag,
					'instagram_object_id'		=> $media->id,
					'instagram_time'			=> $media->created_time,
					'link'						=> $media->link,
					'likes'						=> $media->likes->count,
					'images'					=> $media->images,
					'full_name'					=> !empty($media->user->full_name) ? $media->user->full_name : $media->user->username,
					'username'					=> $media->user->username,
					'profile_picture'			=> $media->user->profile_picture,
					'user_id'					=> $media->user->id,
					'caption'					=> $media->caption->text,
					'location'					=> $media->location,
				);
			}
			if (!empty($feeds2018))
			{
				$ret['feeds2017'] = $feeds2018;
			}
		}
		else
		{
			return false;
		}
		*/

		/*
		echo "<pre>\n";
		echo "\n\n\n\n\n\n\n";
		print_r(array('pagination'=>$ret, 'feeds2017'=>$feeds2018));
		echo "</pre>\n";
		*/
		// return [];
		return $feeds2018;
		//return array('pagination'=>$ret, 'feeds2017'=>$feeds2018);
	}

	function fetchInstagram($token_endpoint)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $token_endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		return $result;
	}

	function fetchURL($token_endpoint, $fields=array(), $token='')
	{
		$fields_string = '';
		foreach($fields as $key=>$value)
		{
			$fields_string .= $key.'='.$value.'&';
		}
		rtrim($fields_string, '&');

		$ch = curl_init();
		if ($token)
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Authorization: OAuth $token") );

		curl_setopt($ch, CURLOPT_URL, $token_endpoint);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		return $result;
	}

}



// end of Instagram plugin