<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 *
 * The galeri module enables users to create albums, upload photos and manage their existing albums.
 *
 * @author 		Okky Sari
 * @package  	Galeri
 * @subpackage	Models
 * @category  	Module
 */

class Galleries_m extends MY_Model {

	/**
	 * Get all galleries along with the total number of media in each gallery
	 *
	 * @access public
	 * @return mixed
	 */
	public function get_all()
	{
		$galerias	= parent::get_all();

		$results	= array();

		// Loop through each gallery and add the count of photos to the results
		foreach ($galerias as $gallery)
		{
			$count = $this->db
				->where('gallery_id', $gallery->id)
				->count_all_results('gallery_media');

			$cat = $this->db->where('id', $gallery->category_id)->get('gallery_categories')->row();
			if ($cat)
			{
				$gallery->category = $cat->title;
			}
			else
			{
				$gallery->category = '--';
			}

			$gallery->media_count = $count;
			$results[] = $gallery;
		}

		// Return the results
		return $results;
	}

	/**
	 * Insert a new gallery into the database
	 *
	 * @access public
	 * @param array $input The data to insert (a copy of $_POST)
	 * @return bool
	 */
	public function insert($input, $skip_validation = false)
	{
		return (int) parent::insert(array(
			'title'				=> $input['title'],
			'subtitle'			=> $input['subtitle'],
			'slug'				=> $input['slug'],
			'description'		=> $input['description'],
			'status'			=> $input['status'],
			'category_id'		=> (!empty($input['category_id']) ? $input['category_id'] : 0),
			'thumbnail'			=> (int)$input['status'],
			'created_on'		=> now(),
			'gallery_year'		=> date('Y')
		));
	}

	/**
	 * Get ID of a gallery from its slug
	 *
	 * @access public
	 * @param string $slug Slug of the gallery
	 * @return int ID of the gallery
	 */
	public function get_id_from_slug($slug='')
	{
		$this->db->where('slug', $slug);
		$res = $this->db->get('galleries')->row();
		if ($res)
			return $res->id;
		return false;
	}

	public function publish_image($id)
	{
		if (!$id) return false;
		return $this->db->where('id', $id)->update('gallery_media', array('status'=>'live'));
	}

	/**
	 * Get all media from a gallery
	 *
	 * @access public
	 * @param int ID of the gallery
	 * @return mixed
	 */
	public function get_all_media($id=0)
	{
		if ($id)
			$this->db->where('gallery_id', $id);
		else
		{
			$this->db->where('gallery_id', '0');
			$this->db->where('uploaded_by', $this->current_user->id);
		}
		$this->db->order_by('position');
		return $this->db->get('gallery_media')->result();
	}

	/**
	 * Get the first media of a gallery
	 *
	 * @access public
	 * @param int ID of the gallery
	 * @return mixed
	 */
	public function get_galeria_thumbnail($id=0)
	{
		$this->db->limit(1);
		$this->db->order_by('position');
		$this->db->where('gallery_id', $id);
		return $this->db->get('gallery_media')->row();
	}

	/**
	 * Insert new media to database
	 *
	 * @param array array of data to input
	 * @return int|FALSE ID of inserted data on success, or FALSE on failure
	 */
	function insertimg($gallery_id, $input)
	{
		if ($this->db->insert('gallery_media', $input))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Update image media database
	 *
	 * @param int ID of image database to update
	 * @param array Input array
	 * @return bool
	 */
	function updateimg($id, $input)
	{
		$this->db->where('id', $id);
		return $this->db->update('gallery_media', $input);
	}

	/**
	 * Retrieve single image
	 *
	 * @param int|array ID of image(s) to retrieve
	 * @return objects Table row data object
	 */
	function getsingleimage($id)
	{
		if (empty($id)) return FALSE;
		$this->db->where('id', $id);
		return $this->db->get('gallery_media')->row();
	}

	/**
	 * Delete image from database
	 *
	 * @param int ID of the image to delete
	 * @return bool
	 */
	function imgdelete($imgid=0)
	{
		if (!$imgid) return FALSE;
		$this->db->where('id', $imgid);
		return $this->db->delete('gallery_media');
	}

	/**
	 * Check if certain field that contains value exists in the database
	 *
	 * @param string $field Field name to check
	 * @param string $value Value to check it against
	 * @param int $id ID to ignore
	 * @return bool
	 */
	function check_exists($field, $value = '', $id = 0)
	{
		if (is_array($field))
		{
			$params = $field;
			$id = $value;
		}
		else
		{
			$params[$field] = $value;
		}
		$params['id !='] = (int) $id;

		return parent::count_by($params) == 0;
	}

	function get_many_by($params = array())
    {
    	
    	if(!empty($params['year']))
    	{
			$this->db->where('gallery_year', $params['year']);
    	}
    	
		if (!isset($params['year']))
		{
			$this->db->where('gallery_year', date("Y"));
		}

       	// Limit the results based on 1 number or 2 (2nd is offset)
       	if(isset($params['limit']) && is_array($params['limit'])) $this->db->limit($params['limit'][0], $params['limit'][1]);
       	elseif(isset($params['limit'])) $this->db->limit($params['limit']);
    	
    	return $this->get_all($params);
    }
	
	function count_by($params = array())
    {
    	if(!empty($params['year']))
    	{
    		$this->db->where('gallery_year', $params['year']);
    	}

		if (!isset($params['year']))
		{
			$this->db->where('gallery_year', date("Y"));
		}
		
		return $this->db->count_all_results('galleries');
    }
	
	function get_year(){
		$this->db->select('galleries.*')->order_by( 'gallery_year','desc' );
		return $this->db->get('galleries')->result();
	}
}

/* End of file details.php */
