<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 * The galleries module enables users to create albums, upload photos and manage their existing albums.
 *
 * @author 		Okky Sari
 * @package  	Galeri
 * @subpackage	Models
 * @category  	Module
 */
class Gallery_media_m extends MY_Model
{
	protected $_table = 'gallery_media';
	/**
	 * Constructor method
	 * 
	 * @author (expert) creations
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * Get all gallery images in a folder
	 *
	 * @author (expert) creations
	 * @access public
	 * @param int $id The ID of the gallery
	 * @param array $options Options
	 * @return mixed
	 */
	public function get_media_by_gallery($id, $options = array())
	{
		// Find new images on files
		$this->set_new_image_files($id);

		// Clear old files on images
		$this->unset_old_image_files($id);

		if (isset($options['offset']))
		{
			$this->db->offset($options['offset']);
		}

		if (isset($options['limit']))
		{
			$this->db->limit($options['limit']);
		}

		$this->db->where('galeria_id', $id);
		$this->db->order_by('position');
		return $this->db->get('gallery_media')->result();
	}

	public function media_left($type='', $status=NULL, $limit=8, $offset=0)
	{
		if ($status)
			$this->db->where('status', $status);

		$this->db->limit(0, $offset);

		if ($type=='image')
		{
			$this->db->where("( LOWER(extension) = '.jpg' OR LOWER(extension) = '.jpeg' OR LOWER(extension) = '.gif' OR LOWER(extension) = '.png' )");
		}
		elseif ($type=='video')
		{
			$this->db->where('extension', 'yt');
			$this->db->or_where('extension', 'vim');
		}

		return $this->db->count_all_results('gallery_media');
	}

	public function get_media_by_type($type='', $status=NULL, $limit=8, $offset=0, $slug='')
	{
		if ($status)
			$this->db->where('gallery_media.status', $status);

		if ($slug)
		{
			$this->db->select('gallery_media.*');
			$this->db->join('galleries', 'galleries.id = gallery_media.gallery_id');
			$this->db->where('galleries.slug', $slug);
		}

		if ($limit && $offset)
		{
			$this->db->limit($limit, $offset);
		}
		elseif ($limit && !$offset)
		{
			$this->db->limit($limit);
		}

		if ($type=='image')
		{
			$this->db->where("( LOWER(extension) = '.jpg' OR LOWER(extension) = '.jpeg' OR LOWER(extension) = '.gif' OR LOWER(extension) = '.png' )");
/*
			$this->db->where('extension', '.jpg');
			$this->db->or_where('extension', '.gif');
			$this->db->or_where('extension', '.png');
*/
		}
		elseif ($type=='video')
		{
			$this->db->where('extension', 'yt');
			$this->db->or_where('extension', 'vim');
		}

		$this->db->order_by('position,id', 'desc');

		return $this->db->get('gallery_media')->result();

	}

}

// end of galeria_media_m.php