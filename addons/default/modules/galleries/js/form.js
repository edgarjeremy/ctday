(function ($) {
	$(function () {

		form = $("form.crud");

		// generate a slug when the user types a title in
		pyro.generate_slug('form#frmGallery input[name="title"]', 'form#frmGallery input[name="slug"]');

		$(document.getElementById('gallery-content')).find('ul').find('li').first().find('a').colorbox({
			srollable: false,
			innerWidth: 600,
			innerHeight: 280,
			href: SITE_URL + 'admin/galleries/categories/create_ajax',
			onComplete: function () {
				$.colorbox.resize();
				var $form_categories = $('form#categories');
				$form_categories.removeAttr('action');
				$form_categories.live('submit', function (e) {

					pyro.generate_slug('form#categories #title', 'form#categories #slug');
					var form_data = $(this).serialize();

					$.ajax({
						url: SITE_URL + 'admin/galleries/categories/create_ajax',
						type: "POST",
						data: form_data,
						success: function (obj) {

							if (obj.status == 'ok') {

								//succesfull db insert do this stuff
								var $select = $('select[name=category_id]');
								//append to dropdown the new option
								$select.append('<option value="' + obj.category_id + '" selected="selected">' + obj.title + '</option>');
								$select.trigger("liszt:updated");
								// TODO work this out? //uniform workaround
								$(document.getElementById('gallery-content')).find('li').first().find('span').html(obj.title);

								//close the colorbox
								$.colorbox.close();
							} else {
								//no dice

								//append the message to the dom
								var $cboxLoadedContent = $(document.getElementById('cboxLoadedContent'));
								$cboxLoadedContent.html(obj.message + obj.form);
								$cboxLoadedContent.find('p').first().addClass('notification error').show();
							}
						}
					});
					e.preventDefault();
				});

			}
		});

	})
})(jQuery);