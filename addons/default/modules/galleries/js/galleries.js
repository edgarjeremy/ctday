
var apiScroll, apiTabs, apiSlide;

(function($) {
	$(function(){

		$('a.galeriabox').colorbox({
				scrolling	: false,
				width		: '800px',
				height		: '600px',
				iframe		: false,
				opacity 	: 0.5,
				esc			: false,
				title		: false,
				onOpen		: function(){
					$('#cboxClose').hide();
				},
				onComplete	: function(){
					$.colorbox.resize();
					$('#cboxClose').show();
				},
				onCleanup	: function() {
					$(".litebox_center").removeData();
					$("#thumbsline").removeData();
					apiTabs.destroy();
					apiSlide.stop();
				},
				onClose		: function() {
					$('#liteBox').empty().remove();
					$('#colorbox').remove();
					$(".litebox_center").removeData();
					$("#thumbsline").removeData();
				}
		});

	});
})(jQuery);


