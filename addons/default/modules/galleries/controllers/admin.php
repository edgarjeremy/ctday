<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The gallery module enables users to create albums, upload photos and manage their existing albums.
 *
 * @author 		Okky Sari
 *
 * @package  	Galleries
 * @subpackage  Admin
 * @category  	Module
 */
class Admin extends Admin_Controller
{
	public $section = 'galleries';

	/**
	 * Database table name
	 * @access public
	 * @var string
	 */
	public $_table = 'galleries';

	public $id = 0;

	/**
	 * Validation rules for creating a new gallery
	 *
	 * @var array
	 * @access private
	 */
	private $gallery_validation_rules = array(
		array(
			'field' => 'title',
			'label' => 'lang:galleries.title_label',
			'rules' => 'trim|max_length[255]|required'
		),
		array(
			'field' => 'subtitle',
			'label' => 'lang:galleries.subtitle_label',
			'rules' => 'trim|max_length[255]'
		),
		array(
			'field' => 'description',
			'label' => 'lang:galleries.description_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'thumbnail',
			'label' => 'lang:galleries.thumbnail_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'status',
			'label' => 'lang:galleries.status_label',
			'rules' => 'trim'
		),
		array(
			'field'	=> 'category_id',
			'label'	=> 'lang:galleries.category_label',
			'rules'	=> 'trim'
		)
	);

	/**
	 * Validation rules for file upload
	 *
	 * @var array
	 * @access private
	 */
	private $_file_rules = array(
		array(
			'field' => 'name',
			'label' => 'lang:files.name_label',
			'rules' => 'trim|max_length[250]'
		),
		array(
			'field' => 'mediatitle',
			'label' => 'lang:galleries.title_label',
			'rules' => 'trim|max_length[100]'
		),
		array(
			'field' => 'mediadescription',
			'label' => 'lang:galleries.description_label',
			'rules' => 'trim'
		),
	);

	/**
	 * Validation rules for media
	 *
	 * @var array
	 * @access private
	 */
	private $_media_rules = array(
		array(
			'field' => 'title',
			'label' => 'lang:galleries.title_label',
			'rules' => 'trim|max_length[100]'
		),
		array(
			'field' => 'subtitle',
			'label' => 'lang:galleries.subtitle_label',
			'rules' => 'trim|max_length[100]'
		),
		array(
			'field' => 'thumbnail',
			'label' => 'lang:galleries.thumbnail_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'youtube_thumb',
			'label' => 'lang:galleries.thumbnail_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'useytthumb',
			'label' => 'lang:galleries.thumbnail_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'usedefault',
			'label' => 'lang:galleries.thumbnail_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'choosethumb',
			'label' => 'lang:galleries.thumbnail_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'ytimgurl',
			'label' => 'lang:galleries.thumbnail_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'status',
			'label' => 'lang:galleries.status_label',
			'rules' => 'trim'
		),

		array(
			'field' => 'description',
			'label' => 'lang:galleries.description_label',
			'rules' => 'trim'
		),
		array(
			'field' => 'meta_title',
			'label' => 'lang:galleries.meta_title_label',
			'rules' => 'trim|max_length[100]'
		),
		array(
			'field' => 'meta_keywords',
			'label' => 'lang:galleries.meta_keywords_label',
			'rules' => 'trim|max_length[250]'
		),
		array(
			'field' => 'meta_description',
			'label' => 'lang:galleries.meta_description_label',
			'rules' => 'trim'
		),
	);

	/**
	 * Configuration for upload library
	 *
	 * @var array
	 * @access public
	 */
	public $upload_cfg = array();

	/**
	 * Width of thumbnail image
	 */
	public $t_gallery_w = 100;

	/**
	 * Height of thumbnail image
	 */
	public $t_gallery_h = 75;


	/**
	 * Width of small images
	 */
	public $s_gallery_w = 350;

	/**
	 * Height of small image
	 */
	public $s_gallery_h = 220;

	/**
	 * Maximum width of image
	 */
	public $m_gallery_w = 705;

	/**
	 * Maximum height of the image
	 */
	public $m_gallery_h = 469;

	/* years */
	protected $current_year;
	
	/**
	 * Accepted video formats (not used anymore)
	 */
	public $format_videos = array('ogg','flv', 'mpg','mpeg', 'mp4', 'mov', '3gp', 'webm', 'yt', 'vim');

	/**
	 * Constructor method
	 *
	 * @author Yorick Peterse - PyroCMS Dev Team
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->load->model('galleries_m');
		$this->load->model('gallery_categories_m');
		$this->load->library('form_validation');
		$this->lang->load('galleries');
		$this->lang->load('gallery_media');
		$this->lang->load('categories');
		$this->load->helper(array('html','filename'));

		// get the categories
		$categories = array();
		if ($cat = $this->gallery_categories_m->order_by('title')->get_all())
		{
			$categories[0] = '';
			foreach ($cat as $category)
			{
				$categories[$category->id] = $category->title;
			}
		}

	/**
	 * Set upload library configuration
	 */
		$this->upload_cfg['upload_path']		= UPLOAD_PATH . 'galleries';
		$this->upload_cfg['allowed_types'] 		= 'jpg|gif|png|jpeg'; //|ogg|flv|mpg|mpeg|mp4|mov|3gp';
		$this->upload_cfg['max_size'] 			= '30000';
		$this->upload_cfg['remove_spaces'] 		= TRUE;
		$this->upload_cfg['overwrite']     		= FALSE;

		$this->_path = UPLOAD_PATH . 'galleries/';//$this->upload_cfg['upload_path'].'/';
		$this->check_dir($this->_path);

		$f_year_backend = $this->galleries_m->get_year();
		$current_year = 0;
		foreach($f_year_backend as $y)
		{
			$this->current_year = ($current_year < $y->gallery_year) ? $y->gallery_year : $current_year;
			$filter_year[$y->gallery_year] = $y->gallery_year;
		}
		
		$this->template
			->set('categories', $categories)
			->set_partial('shortcuts', 'admin/partials/shortcuts')
			->set('filter_year', $filter_year);
			;
	}


	/**
	 * List all existing gallery
	 *
	 * @access public
	 * @return void
	 */
	public function index()
	{
		if ($this->input->post('f_year'))
		{
			$base_where['year'] = $this->input->post('f_year');
		}else{
			//$base_where['year'] = $f_datenow;
			$base_where['year'] = date('Y');
		}
		
		$total_rows = $this->galleries_m->count_by($base_where);
		$pagination = create_pagination('admin/galleries/index', $total_rows);

		// Using this data, get the relevant results
		$galleries = $this->galleries_m->limit($pagination['limit'], $pagination['offset'])->get_many_by($base_where);
		
		//$this->output->enable_profiler(TRUE);
		
		//Get all the galleries
		//$galleries = $this->galleries_m->get_all();

		//do we need to unset the layout because the request is ajax?
		$this->input->is_ajax_request() and $this->template->set_layout(FALSE);
		
		// Load the view
		$this->template
			->title($this->module_details['name'])
			->set('galleries',	$galleries)
			->append_js('admin/filter.js')
			->set_partial('filters', 'admin/partials/filters')
			->set('pagination', $pagination)
			//->build('admin/index')
			;
			
		$this->input->is_ajax_request()
			? $this->template->build('admin/tables/posts')
			: $this->template->build('admin/index');
	}

	/**
	 * Create a new gallery
	 *
	 * @access public
	 * @return void
	 */
	public function create()
	{
		$gallery = new stdClass();

		$this->gallery_validation_rules[] = array(
			'field' => 'slug',
			'label' => 'lang:galleries.slug_label',
			'rules' => 'trim|max_length[255]|required|callback__check_slug'
		);

		// Set the validation rules
		$this->form_validation->set_rules($this->gallery_validation_rules);

		if ($this->form_validation->run() )
		{
			if ($id = $this->galleries_m->insert($this->input->post()))
			{
				// Everything went ok..
				$this->session->set_flashdata('success', lang('galleries.create_success'));

				// Redirect back to the form or main page
				$this->input->post('btnAction') == 'save_exit'
					? redirect('admin/galleries')
					: redirect('admin/galleries/manage/' . $id);
			}
			
			// Something went wrong..
			else
			{
				$this->session->set_flashdata('error', lang('galleries.create_error'));
				redirect('admin/galleries/create');
			}
		}

		// Required for validation
		foreach ($this->gallery_validation_rules as $rule)
		{
			$gallery->{$rule['field']} = $this->input->post($rule['field']);
		}

		$this->template
			->title($this->module_details['name'], lang('galleries.new_gallery_label'))
			->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			->append_js('module::form.js')
			->set('gallery', $gallery)
			->build('admin/form');
	}

	/**
	 * Edit a gallery
	 *
	 * @access public
	 * @param int ID of the gallery
	 * @return void
	 */
	public function edit($id)
	{
		$gallery = $this->galleries_m->get($id);
		if (!$id OR !$gallery)
		{
			$this->session->set_flashdata('error', lang('galleries.edit_empty_gallery_error'));
			redirect('admin/galleries');
		}

		$this->gallery_validation_rules[] = array(
			'field' => 'slug',
			'label' => 'lang:galleries.slug_label',
			'rules' => 'trim|max_length[255]|required|callback__check_slug['.$id.']'
		);

		// Set the validation rules
		$this->form_validation->set_rules($this->gallery_validation_rules);

		if ($this->form_validation->run() )
		{
			$input = array(
				'slug'			=> $this->input->post('slug'),
				'title'			=> $this->input->post('title'),
				'subtitle'		=> $this->input->post('subtitle'),
				'description'	=> $this->input->post('description'),
				'category_id'	=> $this->input->post('category_id'),
				'status'		=> $this->input->post('status')
			);
			if ($result = $this->galleries_m->update($id, $input))
			{
				// Everything went ok..
				$this->session->set_flashdata('success', lang('galleries.update_success'));

				// Redirect back to the form or main page
				$this->input->post('btnAction') == 'save_exit' ? redirect('admin/galleries') : redirect('admin/galleries/manage/' . $id);
			}
			
			// Something went wrong..
			else
			{
				$this->session->set_flashdata('error', lang('galleries.update_error'));
				redirect('admin/galleries');
			}
		}

		// Required for validation
		foreach ($this->gallery_validation_rules as $rule)
		{
			if (isset($_POST[$rule['field']]))
				$gallery->{$rule['field']} = set_value($rule['field']);
		}

		$this->template
			->title($this->module_details['name'], lang('galleries.edit_gallery_label'))
			->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			->append_js('module::form.js')
			->set('gallery', $gallery)
			->build('admin/form');
	}

	/**
	 * Manage an existing gallery
	 *
	 * @access public
	 * @param int $id The ID of the gallery to manage
	 * @return void
	 */
	public function manage($id=0)
	{
		$id OR redirect('galleries');
		$gallery = $this->galleries_m->get($id);

		if (empty($gallery))
		{
			$this->session->set_flashdata('error', lang('galleries.exists_error'));
			redirect('admin/galleries');
		}

		$media = $this->galleries_m->get_all_media($id);

		$this->module_details['slug'] = 'galleries/manage/'.$id;
		
		$this->template
			->title($this->module_details['name'], lang('galleries.manage_gallery_label').' - ' . $gallery->title)
			->append_css('module::files.css')
			->append_css('module::jquery.fileupload-ui.css')
			->append_js('module::jquery.cooki.js')
			->append_js('module::jquery.fileupload.js')
			->append_js('module::jquery.fileupload-ui.js')
			->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			->append_js('module::manage.js')
			->append_js('module::jquery.Jcrop.min.js')
			->append_js('module::jcrop_init.js')
			->append_js('module::jquery.form.js')
			->set('format_videos', $this->format_videos)
			->set('gallery', $gallery)
			->set('media', $media)
			->build('admin/manage');
	}

	/**
	 * Upload
	 *
	 * Upload a file and attach it to gallery
	 *
	 * @params int	The gallery id
	 */
	public function upload($gallery_id = '')
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_file_rules);

		// Setup upload config
		$this->upload_cfg['upload_path'] .= '/'.$gallery_id;
		$this->load->library('upload');
		$this->upload->initialize($this->upload_cfg);

		if ($this->form_validation->run())
		{

			// check directory exists
			$this->check_dir($this->upload_cfg['upload_path']);

			// File upload error
			if ( ! $this->upload->do_upload('userfile'))
			{
				$status		= 'error';
				$message	= $this->upload->display_errors();

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message
					));
				}

				$this->data->messages[$status] = $message;
			}

			// File upload success
			else
			{
				$file = $this->upload->data();
				$data = array(
					'media'			=> str_ireplace(array('(', ')'), '', $file['file_name']),
					'gallery_id'	=> $gallery_id,
					'title'			=> $this->input->post('mediatitle'),
					'subtitle'		=> $this->input->post('mediasubtitle'),
					'description'	=> $this->input->post('mediadescription'),
					'extension'		=> $file['file_ext'],
					'uploaded_by'	=> $this->current_user->id,
					'uploaded_on'	=> now(),
					'position'		=> 999
				);

				if ($file['is_image'])
				{
					$this->load->library('image_lib');
					//$filename = $file['file_name'];
					$filename = str_ireplace(array('(', ')'), '', $file['file_name']);

					$ext = substr(strrchr($filename, "."), 1);
					$fn = str_ireplace('.'.$ext, '', $filename);
					$thumbfile = $fn . '_thumb.' . $ext;
					$smallfile = $fn . '_small.' . $ext;
					$medfile = $fn . '_med.' . $ext;
					/*---------------------------------------------------------------------------------
					// create thumb - admin
					*/
					$image_cfg['source_image'] = $file['full_path'];
					$image_cfg['maintain_ratio'] = TRUE;
					$image_cfg['master_dim'] = 'height';
					$image_cfg['width'] = $this->t_gallery_w;// ($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
					$image_cfg['height'] = $this->t_gallery_h; //($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
					$image_cfg['create_thumb'] = FALSE;
					$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
					$this->image_lib->initialize($image_cfg);
					$img_ok = $this->image_lib->resize();
					unset($image_cfg);
					$this->image_lib->clear();
	
					/*---------------------------------------------------------------------------------
					// create thumb - frontend
					*/
					$image_cfg['source_image'] = $file['full_path'];
					$image_cfg['maintain_ratio'] = TRUE;
					$image_cfg['master_dim'] = 'height';
					$image_cfg['width'] = $this->s_gallery_w; //($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
					$image_cfg['height'] = $this->s_gallery_h; //($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
					$image_cfg['create_thumb'] = FALSE;
					$image_cfg['new_image'] = $file['file_path'] . $smallfile;
					$this->image_lib->initialize($image_cfg);
					$img_ok = $this->image_lib->resize();
					unset($image_cfg);
					$this->image_lib->clear();

						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = $this->m_gallery_w; //($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
						$image_cfg['height'] = $this->m_gallery_h; //($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $medfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

				}

				// Insert success
				if ($id = $this->galleries_m->insertimg($gallery_id, $data))
				{
					$status		= 'success';
					$message	= lang('galleries.upload_img_success');
				}
				// Insert error
				else
				{
					$this->unlinkfile($file['file_name'], $gallery_id);
					$status		= 'error';
					$message	= sprintf(lang('galleries.dbstore_img_error'), $file['file_name']);
				}

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'file'		=> array(
							'name'	=> $file['file_name'],
							'type'	=> $file['file_type'],
							'size'	=> $file['file_size']
						)
					));
				}
				else
				{
					$this->session->set_flashdata($status, $message);
					redirect('admin/galleries');
				}

				if ($status === 'success')
				{
					$this->session->set_flashdata($status, $message);
					redirect('admin/galleries');
				}
				else
				{
					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);
		
						return $this->template->build_json(array(
							'status'	=> 'error',
							'message'	=> $message
						));
					}
				}
			}
		}
		elseif (validation_errors())
		{
			// if request is ajax return json data, otherwise do normal stuff
			if ($this->input->is_ajax_request())
			{
				$data = array();
				$status = 'error';
				$data['messages'][$status] = validation_errors();
				$message = $this->load->view('admin/partials/notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
		}

		if ($this->input->is_ajax_request())
		{
			// todo: debug errors here
			$status		= 'error';
			$message	= 'unknown';

			$data = array();
			$data['messages'][$status] = $message;
			$message = $this->load->view('admin/partials/notices', $data, TRUE);

			return $this->template->build_json(array(
				'status'	=> $status,
				'message'	=> $message
			));
		}

		$this->template
			->title()
			->build('admin/files/upload', $this->data);
	}

	/**
	 * Upload Youtube
	 *
	 * Upload/embed youtube video
	 *
	 * @params int	The gallery id
	 */
	public function vupload($gid=0)
	{
		$thisdata = new stdClass();
		$media = new stdClass();
		$rules = $this->_media_rules;
		$rules[] = array(
			'field' => 'media',
			'label' => 'lang:galleries.youtube_link_label',
			'rules' => 'trim'
		);
		$rules[] = array(
			'field' => 'gallery_id',
			'label' => 'lang:galleries.gallery_label',
			'rules' => 'trim|max_length[255]'
		);

		if (!$gid)
			$gallery_id = $this->input->post('gallery_id');
		else
			$gallery_id = $gid;

		$dir_ok = $this->check_dir($this->_path . $gallery_id);
		if (!is_dir($this->_path . $gallery_id))
		{
			$this->session->set_flashdata('error', sprintf(lang('galleries.unable_create_dir'), $this->_path . $gallery_id));
			redirect('admin/galleries/manage/'.$gallery_id);
		}

		$this->module_details['slug'] = 'gallery/manage/'.$gallery_id;

		$this->form_validation->set_rules($rules);

		$this->method = 'create';	
		$thisdata->gallery_id =& $gallery_id;
		$thisdata->media =& $media;

		$video = array('file_ext'=>NULL, 'file_name'=>NULL);

		if ($this->form_validation->run())
		{
			$this->load->library('upload');

			$use_default_image = $this->input->post('usedefault');

			// check directory exists
			$this->check_dir($this->upload_cfg['upload_path']);

			if ( ! empty($_FILES['videofile']['name']))
			{
				$config = $this->upload_cfg;
				$config['upload_path'] .= '/'.$gallery_id;
				$config['allowed_types'] = 'ogg|flv|mpg|mpeg|mp4|mov|3gp|webm';
				$this->upload->initialize($config);

				if ($this->upload->do_upload('videofile'))
				{
					$video = $this->upload->data();
				}
				else
				{
					$xtramsg = '<br />'.$this->upload->display_errors();
					$this->session->set_flashdata('error', sprintf(lang('gallery_video.upload_error'), $_FILES['videofile']['name']) . $xtramsg);

					redirect('admin/galleries/manage/'.$gallery_id);
				}
			}

			if ( ! empty($_FILES['userfile']['name']))
			{
				// Setup upload config
				$this->upload_cfg['upload_path'] .= '/'.$gallery_id;
				$this->upload->initialize($this->upload_cfg);

	
				// File upload error
				if ( ! $this->upload->do_upload('userfile'))
				{
					$status		= 'error';
					$message	= $this->upload->display_errors();
					$this->session->set_flashdata('notice', sprintf(lang('gallery_video.thumbnail_error'), $_FILES['userfile']['name']) . '<br />'.$message);
	
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);
	
						return $this->template->build_json(array(
							'status'	=> $status,
							'message'	=> $message
						));
				}
				// File upload success
				else
				{
					$file = $this->upload->data('userfile');

					if ($file['is_image'])
					{
						$this->load->library('image_lib');
						$filename = $this->_filename($file['file_name']);

						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = FALSE;
						$image_cfg['width'] = ($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
						$image_cfg['height'] = ($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['thumb'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

		
						/*---------------------------------------------------------------------------------
						// create thumb - frontend
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = FALSE;
						$image_cfg['width'] = ($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
						$image_cfg['height'] = ($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['small'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();


						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
						$image_cfg['height'] = ($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['med'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

					}

				}
			}

			$data = array(
//				'media'				=> $this->input->post('media') ? $this->input->post('media') : $video['file_name'],
				'media'				=> $this->input->post('video_id') ? $this->input->post('video_id') : $video['file_name'],
				'gallery_id'		=> $gallery_id,
				'extension'			=> $video['file_ext'] ? $video['file_ext'] : 'yt',
				'title'				=> $this->input->post('title'),
				'subtitle'			=> $this->input->post('subtitle'),
				'description'		=> $this->input->post('description'),
				'meta_title'		=> $this->input->post('meta_title'),
				'meta_keywords'		=> $this->input->post('meta_keywords'),
				'meta_description'	=> $this->input->post('meta_description'),
				'uploaded_by'		=> $this->current_user->id,
				'uploaded_on'		=> now(),
				'position'			=> 999
			);

			if ($use_default_image)
				$data['thumbnail'] = '';
			else
				$data['thumbnail'] = $this->input->post('youtube_thumb');

			if (!empty($file['file_name']))
			{
				$data['thumbnail'] = $file['file_name'];
			}

			$id = $this->galleries_m->insertimg($gallery_id, $data);
			if($id > 0)
			{
				$this->session->set_flashdata('success', lang('galleries.video_upload_success'));
				redirect('admin/galleries/manage/'.$gallery_id);
			}
			else
			{
				$this->session->set_flashdata('error', lang('galleries.media_save_error'));
			}

		}

		// Loop through each validation rule
		foreach($rules as $rule)
		{
			$media->{$rule['field']} = set_value($rule['field']);
		}

		if ($this->input->post('youtube_thumb'))
			$media->thumbnail = $this->input->post('youtube_thumb');

		$this->template
			->title($this->module_details['name'], lang('galleries.create_youtube_label'))
			->append_css('module::galleries.css')
			->set("gallery_id", $gallery_id)
			->set("media", $media)
			->build('admin/files/upload-video');
	}

	/**
	 * Try to retrieve thumbnail image from youtube
	 *
	 * @var array $_POST['media'] 
	 * @access public
	 * @return string HTML image tag
	 */
	public function yt_thumbnail()
	{
		$html = $this->input->post('media');
		$html = trim($html);
		$video_id = substr(strrchr($html, "/"), 1);

		if (substr($video_id, 0, 5) == 'watch')
		{
			$video_id = str_ireplace('watch?v=', '', $video_id);
		}

		$url = "https://gdata.youtube.com/feeds/api/videos/$video_id?v=2&alt=jsonc";
		$html = $this->_fetchUrl($url);

		$html = json_decode($html);

		if (!empty($html->data))
		{
			$ret = array(
				'thumbnail_small'	=> $html->data->thumbnail->sqDefault,
				'thumbnail'			=> $html->data->thumbnail->hqDefault,
				'title'				=> $html->data->title,
				'description'		=> $html->data->description,
				'video_id'			=> $video_id
			);
			echo json_encode($ret);
		}

// http://www.youtube.com/watch?v=KxsUVzog_IM
// http://youtu.be/KxsUVzog_IM
/*
echo "url: $url\n";
print_r($html);
*/

//http://gdata.youtube.com/feeds/api/videos/'+video_id+'?v=2&alt=jsonc
/*
		$doc=new DOMDocument();
		$doc->loadHTML($html);
		$xml=simplexml_import_dom($doc); // just to make xpath more simple
		$images=$xml->xpath('//iframe');
		$img = $images[0];

		$ytID = substr(strrchr($img['src'], "/"), 1);

		$html = file_get_contents('http://www.youtube.com/watch?v='.$ytID);
*/
/*
		$yt = new DOMDocument();
		@$yt->loadHTML($html);
		$xml = simplexml_import_dom($yt);
		$ytdom = $xml->xpath("//link[@itemprop='thumbnailUrl']");
		$thumbnail = str_ireplace('hqdefault.jpg', 'default.jpg', $ytdom[0]['href']);

		if (substr($thumbnail, 0, 4) <> 'http')
			return;
		else
			echo $thumbnail;
*/

	}


	public function vimeo_thumbnail()
	{
		$html = $this->input->post('media');
		$html = trim($html);
		$video_id = substr(strrchr($html, "/"), 1);

		if (substr($video_id, 0, 5) == 'watch')
		{
			$video_id = str_ireplace('watch?v=', '', $video_id);
		}
// http://vimeo.com/api/v2/video/$video_id.json
		$url = "http://vimeo.com/api/v2/video/$video_id.json";
		$html = $this->_fetchUrl($url);

		$html = json_decode($html);


//print_r($html);

		foreach($html as $h=>$v)
		{
			if (!empty($v))
			{
				$ret = array(
					'thumbnail_small'	=> $v->thumbnail_small,
					'thumbnail'			=> $v->thumbnail_medium,
					'title'				=> $v->title,
					'description'		=> $v->description ? $v->description : $v->title,
					'video_id'			=> $video_id,
					'vid'				=> 'vim'
				);
				echo json_encode($ret);
			}
		}
	}

	public function vidthumb()
	{
		$url = $this->input->post('media');
		if (stripos($url, 'youtube.com') or stripos($url, 'youtu.be'))
		{
			$this->yt_thumbnail();
		}
		elseif (stripos($url, 'vimeo.com'))
		{
			$this->vimeo_thumbnail();
		}
	}

	/**
	 * Get the images just uploaded.
	 *
	 * View file: views/admin/files/contents.php
	 *
	 * @param int ID of the product
	 * @return void
	 *
	 */
	public function getuploaded($id=0)
	{
		$id		= $this->input->post('gallery_id');

		if (!$id && $this->input->get('gallery_id'))
		{
			$id = $this->input->get('gallery_id', TRUE);
		}
		$gallery = $this->galleries_m->get($id);
		$files = $this->galleries_m->get_all_media($id);
		$this->load->view('admin/files/contents', array('gallery'=>$gallery, 'files'=>$files));
	}

	/**
	 * Crop uploaded media
	 *
	 * @var int $id ID of the media to crop
	 * @access public
	 * @return void
	 */
	public function crop($id=0)
	{
		$data = new stdClass;
		$this->load->library('form_validation');

		$this->form_validation->set_rules($this->_file_rules);
		// check directory exists
		$this->check_dir($this->upload_cfg['upload_path']);

		if ( ! ($id && ($file = $this->galleries_m->getsingleimage($id))))
		{
			$status		= 'error';
			$message	= lang('galleries.media_not_found_error');

			if ($this->input->is_ajax_request())
			{
				$data = array();
				$data['messages'][$status] = $message;
				$message = $this->load->view('admin/partials/notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> $status,
					'message'	=> $message
				));
			}

			$this->session->set_flashdata($status, $message);
			redirect('admin/galleries');
		}

		$data->media =& $file;

		if ($this->form_validation->run())
		{
			// We are uploading a new file
			if ( ! empty($_FILES['userfile']['name']))
			{
				// Setup upload config
				$this->load->library('upload', array(
					'upload_path'	=> $this->_path,
					'allowed_types'	=> $this->_ext
				));

				// File upload error
				if ( ! $this->upload->do_upload('userfile'))
				{
					$status		= 'error';
					$message	= $this->upload->display_errors();

					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);

						return $this->template->build_json(array(
							'status'	=> $status,
							'message'	=> $message
						));
					}

					$data->messages[$status] = $message;
				}
				// File upload success
				else
				{

				}
			}

			// Upload data
			else
			{
				$g_id = $data->media->gallery_id;
				$full_path 	= $this->_path.''.$g_id;	
				$source_name 	= FCPATH.$this->_path.''.$g_id.'/'.$data->media->media;	
				$filename = $data->media->media;
				$filename_array = $this->_filename($filename);
				$cropfile = $filename_array['full'];
				$cropfile_name 	= FCPATH.$this->_path.''.$g_id.'/'.$cropfile;

				// Crop an existing file
				$this->load->library('image_lib');
				if ( $this->input->post('thumb_width') && $this->input->post('thumb_height') > '1')
				{
					// Get the required values for cropping the thumbnail
					$size_array = getimagesize($source_name);
					$width 		= $size_array[0];
					$height 	= $size_array[1];
					$scaled_height		 	= $this->input->post('scaled_height');
					$scaled_percent			= $scaled_height/$height;

					$scaled_height		 	= $this->input->post('thumb_height');
					$scaled_percent			= $height/$scaled_height;
					$scaled_percent			= $this->m_gallery_h/$scaled_height;

					
					$options['width'] 			= $this->input->post('thumb_width');//$scaled_percent;
					$options['height']			= $this->input->post('thumb_height');//$scaled_percent;
					$options['x_axis']			= $this->input->post('thumb_x');//$scaled_percent;
					$options['y_axis']			= $this->input->post('thumb_y');//$scaled_percent;			
					$options['create_thumb']	= FALSE;
					$options['maintain_ratio']	= FALSE;
					$options['source_image']	= $source_name;
					$options['new_image']		= FCPATH.$this->_path.''.$g_id.'/'.$filename_array['full'];
					$options['quality']			= 100;
					$options['image_library']	= 'gd2';

					$this->image_lib->initialize($options);

					// Crop the fullsize image first
					if ($this->image_lib->image_process_gd('crop'))
					{
						if ($scaled_percent < 1)
						{
							$this->image_lib->clear();
		
							// cropped image will be larger than 705 x 469px, so resize it
							$image_cfg['source_image'] = FCPATH.$this->_path.''.$g_id.'/'.$filename_array['full'];
							$image_cfg['maintain_ratio'] = FALSE;
							$image_cfg['width'] = $this->m_gallery_w;
							$image_cfg['height'] = $this->m_gallery_h;
							$image_cfg['create_thumb'] = FALSE;
							$image_cfg['quality'] = 100;
							$image_cfg['image_library']	= 'gd2';
							$this->image_lib->initialize($image_cfg);
		
							if (!$this->image_lib->image_process_gd('resize'))
							{
								$status		= 'error';
								$message = lang('galleries.media_crop_error');
					
								if ($this->input->is_ajax_request()) {
									$data = array();
									$data['messages'][$status] = $message;
									$message = $this->load->view('admin/partials/notices', $data, TRUE);
					
									$thumbnail = '/'.UPLOAD_PATH .'news/title/'. thumbnail($post->image_title);//substr($post->image_title, 0, -4) . '_thumb' . substr($post->image_title, -4);
					
									return $this->template->build_json(array(
										'status'	=> $status,
										'message'	=> $message,
										'file'		=> $thumbnail,
		//								'title'		=> $status === 'error' ? sprintf(lang('news_edit_error'), $post->title) : $post->title,
										'image'		=> thumbnail($post->image_title)
									));
								}
		
							}
						}

						// create thumb
						$this->image_lib->clear();
						$image_cfg['source_image'] = FCPATH.$this->_path.''.$g_id.'/'.$filename_array['full'];
						$image_cfg['new_image'] = FCPATH.$this->_path.''.$g_id.'/'.$filename_array['thumb'];
						$image_cfg['width'] = $this->t_gallery_w;
						$image_cfg['height'] = $this->t_gallery_h;
						$this->image_lib->initialize($image_cfg);
						$this->image_lib->image_process_gd('resize');

						// create small
						$this->image_lib->clear();
						$image_cfg['source_image'] = FCPATH.$this->_path.''.$g_id.'/'.$filename_array['full'];
						$image_cfg['new_image'] = FCPATH.$this->_path.''.$g_id.'/'.$filename_array['small'];
						$image_cfg['width'] = $this->s_gallery_w;
						$image_cfg['height'] = $this->s_gallery_h;
						$this->image_lib->initialize($image_cfg);
						$this->image_lib->image_process_gd('resize');

						// create medium
						$this->image_lib->clear();
						$image_cfg['source_image'] = FCPATH.$this->_path.''.$g_id.'/'.$filename_array['full'];
						$image_cfg['new_image'] = FCPATH.$this->_path.''.$g_id.'/'.$filename_array['med'];
						$image_cfg['width'] = $this->m_gallery_w;
						$image_cfg['height'] = $this->m_gallery_h;
						$this->image_lib->initialize($image_cfg);
						$this->image_lib->image_process_gd('resize');
					}		
				}

				$status		= 'success';
				$message = lang('galleries.media_crop_success');

				if ($this->input->is_ajax_request()) {
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'title'		=> $status === 'success' ? lang('galleries.media_update_success') : $file->media
					));
				}

				if ($status === 'success')
				{
					$this->session->set_flashdata($status, $message);
					redirect ('admin/galleries/manage/'.$g_id.'');
				}
			}
		}
		elseif (validation_errors())
		{
			// if request is ajax return json data, otherwise do normal stuff
			if ($this->input->is_ajax_request())
			{
				$message = $this->load->view('admin/partials/notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
		}

		$this->input->is_ajax_request() && $this->template->set_layout(FALSE);

		$this->template
			->title('')
			->build('admin/files/crop', $data);

	}

	/**
	 * Edit existing video
	 *
	 * @var int $id ID of the video to edit
	 * @return void
	 * @access public
	 */
	public function editvideo($id=0)
	{
		$rules = $this->_media_rules;
		$rules[] = array(
			'field' => 'media',
			'label' => 'lang:galleries.youtube_link_label',
			'rules' => 'trim|max_length[255]'
		);
		$rules[] = array(
			'field' => 'gallery_id',
			'label' => 'lang:galleries.gallery_label',
			'rules' => 'trim|max_length[255]'
		);

		$this->form_validation->set_rules($rules);

		if ( ! ($id && ($media = $this->galleries_m->getsingleimage($id))))
		{
			$status		= 'error';
			$message	= lang('galleries.media_not_found_error');

			if ($this->input->is_ajax_request())
			{
				$data = array();
				$data['messages'][$status] = $message;
				$message = $this->load->view('admin/partials/notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> $status,
					'message'	=> $message
				));
			}

			$this->session->set_flashdata($status, $message);
			redirect('admin/galleries');
		}

//		$this->template->module_details['slug'] = 'gallery/manage/'.$media->gallery_id;
//		$this->module_details['slug'] = 'gallery/manage/'.$media->gallery_id;
		//$this->method = 'edit';
		$gallery_id = $media->gallery_id;

		//$this->module['slug'] = '/manage/'.$gallery_id;

		$video = array('file_ext'=>NULL, 'file_name'=>NULL);

		if ($this->form_validation->run())
		{
			$this->load->library('upload');

			// check directory exists
			$this->check_dir($this->upload_cfg['upload_path']);

			if ( ! empty($_FILES['videofile']['name']))
			{
				$config = $this->upload_cfg;
				$config['upload_path'] .= '/'.$gallery_id;
				$config['allowed_types'] = 'ogg|flv|mpg|mpeg|mp4|mov|3gp';
				$this->upload->initialize($config);

				if ($this->upload->do_upload('videofile'))
				{
					$video = $this->upload->data();
				}
				else
				{
					$xtramsg = '<br />'.$this->upload->display_errors();
					$this->session->set_flashdata('error', sprintf(lang('gallery_video.upload_error'), $_FILES['videofile']['name']) . $xtramsg);
					redirect('admin/galleries/manage/'.$gallery_id);
				}

				// delete old video file
				$this->unlinkfile($media->media, $gallery_id);
			}

			if ( $this->input->post('media') && $media->media <> $this->input->post('media') )
			{
				// the media has changed, delete the old one
				$this->unlinkfile($media->media, $gallery_id);
			}

			if ( ! empty($_FILES['userfile']['name']))
			{
				// Setup upload config
				$this->upload_cfg['upload_path'] .= '/'.$gallery_id;
				$this->upload->initialize($this->upload_cfg);

	
				// File upload error
				if ( ! $this->upload->do_upload('userfile'))
				{
					$status		= 'error';
					$message	= $this->upload->display_errors();
					$this->session->set_flashdata('notice', sprintf(lang('gallery_video.thumbnail_error'), $_FILES['userfile']['name']) . '<br />'.$message);
	
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);
	
						return $this->template->build_json(array(
							'status'	=> $status,
							'message'	=> $message
						));
				}
				// File upload success
				else
				{
					$file = $this->upload->data('userfile');

					if ($file['is_image'])
					{
						$this->load->library('image_lib');
						$filename = $this->_filename($file['file_name']);

						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = FALSE;
						$image_cfg['width'] = ($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
						$image_cfg['height'] = ($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['thumb'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

		
						/*---------------------------------------------------------------------------------
						// create thumb - frontend
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = FALSE;
						$image_cfg['width'] = ($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
						$image_cfg['height'] = ($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['small'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();


						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
						$image_cfg['height'] = ($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['med'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

						$this->unlinkfile($media->thumbnail, $media->gallery_id);
					}

				}
			}

			$data = array(
				//'media'				=> $video['file_name'] ? $video['file_name'] : $this->input->post('media'),
				//'extension'			=> $video['file_ext'] ? $video['file_ext'] : 'yt',
				'media'				=> $this->input->post('video_id') ? $this->input->post('video_id') : $video['file_name'],
				'title'				=> $this->input->post('title'),
				'subtitle'			=> $this->input->post('subtitle'),
				'description'		=> $this->input->post('description'),
				'status'			=> $this->input->post('status'),
				'meta_title'		=> $this->input->post('meta_title'),
				'meta_keywords'		=> $this->input->post('meta_keywords'),
				'meta_description'	=> $this->input->post('meta_description'),
				'uploaded_by'		=> $this->current_user->id,
				'uploaded_on'		=> now(),
			);

			if (!empty($video['file_name']))
			{
				$data['media'] = $video['file_name'];
				$this->unlinkfile($media->media, $gallery_id);
			}

			if ($this->input->post('youtube_thumb'))
			{
				$this->unlinkfile($media->thumbnail, $gallery_id);
				$data['thumbnail'] = $this->input->post('youtube_thumb');
			}

			if (!empty($file['file_name']))
			{
				$data['thumbnail'] = $file['file_name'];
			}
			if ($this->input->post('imgremove'))
			{
				$this->unlinkfile($media->thumbnail, $gallery_id);
				$data['thumbnail'] = '';
			}

			$ret = $this->galleries_m->updateimg($id, $data);
			if($ret)
			{
				$this->session->set_flashdata('success', lang('galleries.video_edit_success'));
				redirect('admin/galleries/manage/'.$gallery_id);
			}
			else
			{
				$this->session->set_flashdata('error', lang('galleries.media_save_error'));
			}

		}

		//$this->data->media =& $media;


		foreach ($this->_media_rules as $rule)
		{
			if (isset($_POST[$rule['field']]))
			{
				//$this->data->media->{$rule['field']} = set_value($rule['field']);
				$media->{$rule['field']} = set_value($rule['field']);
			}
		}

		if ($this->input->post('useytthumb'))
		{
			//$this->data->media->thumbnail = $this->input->post('youtube_thumb');
			$media->thumbnail = $this->input->post('youtube_thumb');
		}

		if ($this->input->post('usedefault'))
		{
			//$this->data->media->thumbnail = '';
			$media->thumbnail = '';
		}

		$this->template
			->title($this->module_details['name'], lang('galleries.create_youtube_label'))
			->append_css('module::galleries.css')
			->set('gallery_id', $media->gallery_id)
			->set('media', $media)
			->build('admin/files/upload-video');
	}



	/**
	 * Edit image media
	 *
	 * @var int $id ID of the image to edit
	 * @return void
	 * @access public
	 */
	public function editmedia($id=0)
	{
		$thisdata = new stdClass;
		$this->load->library('form_validation');

		$this->form_validation->set_rules($this->_media_rules);

		if ( ! ($id && ($media = $this->galleries_m->getsingleimage($id))))
		{
			$status		= 'error';
			$message	= lang('galleries.media_not_found_error');

			if ($this->input->is_ajax_request())
			{
				$data = array();
				$data['messages'][$status] = $message;
				$message = $this->load->view('admin/partials/notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> $status,
					'message'	=> $message
				));
			}

			$this->session->set_flashdata($status, $message);
			redirect('admin/galleries');
		}
		$gallery_id = $media->gallery_id;

		if (in_array(strtolower(trim($media->extension,'.')), $this->format_videos))
		{
			$this->session->set_flashdata('notice', lang('gallery_images.exists_error'));
			redirect('admin/galleries/manage/'.$gallery_id);
		}
		else
		{
			$type = 'image';
		}

		$this->module_details['slug'] = 'galleries/manage/'.$media->gallery_id;
		$thisdata->media =& $media;

		if ($this->form_validation->run())
		{
			// We are uploading a new file
			if ( ! empty($_FILES['userfile']['name']))
			{
				$this->load->library('upload');

				// Setup upload config
				$config = $this->upload_cfg;
				$config['upload_path'] .= '/'.$gallery_id;
				$this->upload->initialize($config);

				// File upload error
				if ( ! $this->upload->do_upload('userfile'))
				{
					$status		= 'error';
					$message	= $this->upload->display_errors();

					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $thisdata, TRUE);

						return $this->template->build_json(array(
							'status'	=> $status,
							'message'	=> $message
						));
					}

					$thisdata->messages[$status] = $message;
				}
				// File upload success
				else
				{
					$file = $this->upload->data();

					if ($file['is_image'])
					{
						$this->load->library('image_lib');
						$filename = $this->_filename($file['file_name']);

						/*---------------------------------------------------------------------------------
						// create thumb - admin
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['master_dim'] = 'height';
						$image_cfg['width'] = $this->t_gallery_w; //($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
						$image_cfg['height'] = $this->t_gallery_h; //($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['thumb'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

		
						/*---------------------------------------------------------------------------------
						// create thumb - frontend
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['master_dim'] = 'height';
						$image_cfg['width'] = $this->s_gallery_w; //($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
						$image_cfg['height'] = $this->s_gallery_h; //($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['small'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();


						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['master_dim'] = 'height';
						$image_cfg['width'] = $this->m_gallery_w; //($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
						$image_cfg['height'] = $this->m_gallery_h; //($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $filename['med'];
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

						$this->unlinkfile($media->media, $media->gallery_id);
					}

					$data = array(
						'title'				=> $this->input->post('title'),
						'subtitle'			=> $this->input->post('subtitle'),
						'description'		=> $this->input->post('description')?$this->input->post('description'):'',
						'status'			=> $this->input->post('status'),
						'external_url'		=> $this->input->post('external_url'),
						'meta_title'		=> '',
						'meta_keywords'		=> '',
						'meta_description'	=> '',
					);

					if (!empty($file['file_name']))
						$data['media'] = $file['file_name'];

					if ($this->galleries_m->updateimg($id, $data))
					{
						$status		= 'success';
						$message	= lang('galleries.media_update_success');
					}
					else
					{
						$status		= 'error';
						$message	= lang('galleries.media_update_error');
					};

					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('admin/partials/notices', $data, TRUE);

						return $this->template->build_json(array(
							'status'	=> $status,
							'message'	=> $message,
							'title'		=> $status === 'success' ? sprintf(lang('files.edit_title'), $this->input->post('name')) : $file->name
						));
					}

					if ($status === 'success')
					{
						$this->session->set_flashdata($status, $message);
						redirect ('admin/galleries/manage/'.$gallery_id);
					}
				}
			}

			// Upload data
			else
			{
				$data = array(
					'title'				=> $this->input->post('title'),
					'external_url'		=> $this->input->post('external_url'),
					'subtitle'			=> $this->input->post('subtitle'),
					'description'		=> $this->input->post('description'),
					'status'			=> $this->input->post('status'),
					'meta_title'		=> $this->input->post('meta_title'),
					'meta_keywords'		=> $this->input->post('meta_keywords'),
					'meta_description'	=> $this->input->post('meta_description'),
				);

				if ($this->input->post('media'))
					$data['media'] = $this->input->post('media');

				if ($this->galleries_m->updateimg($id, $data))
				{
					$status		= 'success';
					$message	= lang('galleries.media_update_success');
				}
				else
				{
					$status		= 'error';
					$message	= lang('galleries.media_update_error');
				};

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('admin/partials/notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'title'		=> $status === 'success' ? lang('galleries.media_update_success') : $file->media
					));
				}

				if ($status === 'success')
				{
					$this->session->set_flashdata($status, $message);
					redirect ('admin/galleries/manage/'.$gallery_id);
				}
			}
		}
		elseif (validation_errors())
		{
			// if request is ajax return json data, otherwise do normal stuff
			if ($this->input->is_ajax_request())
			{
				$message = $this->load->view('admin/partials/notices', array(), TRUE);

				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
		}

		foreach ($this->_media_rules as $rule)
		{
			if (isset($_POST[$rule['field']]))
				$thisdata->media->{$rule['field']} = set_value($rule['field']);
		}

		$this->input->is_ajax_request() && $this->template->set_layout(FALSE);
		$this->method = 'edit';
		$this->template
			->title('')
			->set('format_videos', $this->format_videos)
			->set('gallery_id', $gallery_id)
			->build( $type == 'video' ? 'admin/files/upload-video' : 'admin/files/upload', $thisdata);
	}

	/**
	 * Delete many slide images.
	 *
	 * @param array IDs of image to delete
	 */
	public function deletemany()
	{
		$title = $this->deletemedia();

		$redirect_to = $this->input->post('redirect_to') ? $this->input->post('redirect_to') : 'admin/galleries';

		if (!empty($title))
		{
			$this->session->set_flashdata('success', sprintf(lang('gallery_images.mass_delete_success'), ( is_array($title) ? implode(', ', $title) : $title ) ) );
			redirect($redirect_to);
		}
		else
		{
			$this->session->set_flashdata('error', lang('gallery_images.mass_delete_error'));
			redirect($redirect_to);
		}

	}

	public function action()
	{
		$btn = $this->input->post('btnAction');
		switch($btn)
		{
			case 'publish':
				$this->publish_media();
				break;
			case 'delete':
				$this->deletemany();
				break;
		}
	}

	public function publish_media($id=0)
	{
		$ids = ( $id ? (array) $id : (array) $this->input->post('imgid') );
		foreach($ids as $id)
		{
			if ($stat = $this->galleries_m->publish_image($id))
			{
				$published[] = $stat;
			}
		}

		$redirect_to = $this->input->post('redirect_to') ? $this->input->post('redirect_to') : 'admin/galleries';

		$this->session->set_flashdata('success', count($published).' of '.count($ids).' images were published successfully.');
		redirect($redirect_to);
	}

	/**
	 * Delete slide images.
	 * This is an AJAX call
	 *
	 * @param int ID of image to delete
	 */
	public function deletemedia($id=0)
	{
		$ids = ( $id ? (array) $id : (array) $this->input->post('imgid') );

		if (!$ids) {
			if ($this->input->is_ajax_request())
			{
				$status = 'error';
				$data = array();
				$data['messages'][$status] = lang('galleries.delete_img_db_error');
				$message = $this->load->view('admin/partials/notices', $data, TRUE);
				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message
				));
			}
			else
			{
				return FALSE;
			}
		}

		$retval = array();

		foreach($ids as $imgid)
		{
			// get image detail first
			$file 			= $this->galleries_m->getsingleimage($imgid);
			$filename 		= $file->media;
			$gallery_id 	= $file->gallery_id;

			$stat 	= array();
			$msg 	= array();

			if($this->galleries_m->imgdelete($imgid))
			{
				$retval[] = ( $file->title ? $file->title : $file->media ); //return TRUE;

				if (!empty($file))
				{
					$delfile = $this->unlinkfile($file->media, $gallery_id);
					$status = $delfile[0];
					$message = $delfile[1];

					if (substr($file->thumbnail, 0, 4) <> 'http')
					{
						$delthumb = $this->unlinkfile($file->thumbnail, $gallery_id);
					}
				}
				else
				{
					$status = 'success';
					$message = lang('galleries.delete_img_db_success');
				}
			}
			else
			{
				if ($this->input->is_ajax_request())
				{
					$status = 'error';
					$data = array();
					$data['messages'][$status] = lang('galleries.delete_img_db_error');
					$message = $this->load->view('admin/partials/notices', $data, TRUE);
					return $this->template->build_json(array(
						'status'	=> 'error',
						'message'	=> $message
					));
				}
			}
		}

		if ($this->input->is_ajax_request())
		{
			$data = array();
			$data['messages'][$status] = $message;
			$message = $this->load->view('admin/partials/notices', $data, TRUE);
			return $this->template->build_json(array(
				'status'	=> $status,
				'message'	=> $message
			));
		}
		else
		{
			return $retval;
		}
	}


	/**
	 * Phisically delete a file
	 *
	 * @var string $filename File name to delete
	 * @var int $gallery_id gallery ID the file is attached to
	 * @access public
	 */
	public function unlinkfile($filename, $gallery_id)
	{
			$fullfile = substr($filename, 0, -4) . '_full' . substr($filename, -4);
			$thumbfile = substr($filename, 0, -4) . '_thumb' . substr($filename, -4);
			$smallfile = substr($filename, 0, -4) . '_small' . substr($filename, -4);
			$medfile = substr($filename, 0, -4) . '_med' . substr($filename, -4);

			$stat = array();
					// delete the files
					if (file_exists($this->_path . $gallery_id .'/' . $fullfile) && $fullfile)
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $fullfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $fullfile);
						}
					}

					if (file_exists($this->_path . $gallery_id .'/' . $thumbfile) && $thumbfile)
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $thumbfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $thumbfile);
						}
					}
	
					if (file_exists($this->_path . $gallery_id .'/' . $smallfile) && $smallfile)
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $smallfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $smallfile);
						}
					}
	
					if (file_exists($this->_path . $gallery_id .'/' . $medfile) && $medfile)
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $medfile))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $medfile);
						}
					}
	
					if (file_exists($this->_path . $gallery_id .'/' . $filename) && $filename)
					{
						if (!@unlink($this->_path . $gallery_id .'/' . $filename))
						{
							$stat[] = 'error';
							$msg[] = sprintf(lang('galleries.delete_img_error'), $filename);
						}
					}

					if (in_array('error', $stat))
					{
						$status = 'error';
						$message = implode('<br />', $msg);
					}
					else
					{
						$status = 'success';
						$message = sprintf(lang('galleries.delete_img_success'), $filename);
					}

		return (array($status, $message));
	}

	/**
	 * Delete a gallery
	 *
	 * @var int $gid ID of gallery to delete
	 * @access public
	 */
	public function delete($gid)
	{
		$msg_ok = $msg_error = '';

#		$gallery_ids = $gid ? $gid : $this->input->post('action_to');
		$id = $gid;

		if (empty($gid))
		{
			$this->session->set_flashdata('error', lang('galleries.no_delete_error'));
			redirect('admin/galleries');
		}

#		foreach($gallery_ids as $id)
#		{
			$media = $this->galleries_m->get_all_media($id);

			foreach($media as $m)
			{
				if ($this->deletemedia($m->id))
				{
					$res[] = array(
						'status' => 'status',
						'file'	=> $m->media
					);
				}
				else
				{
					$res[] = array(
						'status' => 'error',
						'file'	=> $m->media
					);
				}
			}

			foreach($res as $r)
			{
				if ($r['status']=='error')
					$msg_error .= sprintf(lang('galleries.delete_img_error'), $r['file']) . '<br />';
#				else
#					$msg_ok .= sprintf(lang('galleries.delete_img_success'), $r['file']) . '<br />';
			}

			if (!empty($msg_error))
			{
				$this->session->set_flashdata('notice', $msg_error);
			}
			else
			{
				if ($this->galleries_m->delete($id))
				{
					$msg_ok .= lang('galleries.delete_success') . '<br />';
					@rmdir($this->_path . $id);
				}
				else
					$this->session->set_flashdata('notice', lang('galleries.delete_error'));
			}

			$this->session->set_flashdata('success', $msg_ok);

#		}

		redirect('admin/galleries');
	}


	/**
	 *
	 * Re-order image positions
	 *
	 */
	public function imgorder()
	{
		$ids = explode(',', $this->input->post('order'));

		$i = 1;
		foreach ($ids as $id)
		{
			$this->galleries_m->updateimg($id, array(
				'position' => $i
			));
			++$i;
		}
	}

	/**
	 * A better than default thumbnail, medium, small, and full filename constructor
	 *
	 * @var string $filename File name to construct from
	 * @access public
	 */
	public function _filename($filename)
	{
		if (!$filename) return FALSE;
		$_ret = array();
		$ext = substr(strrchr($filename, "."), 1);
		$fn = str_ireplace('.'.$ext, '', $filename);
		$_ret['full'] = $fn . '_full.' . $ext;
		$_ret['med'] = $fn . '_med.' . $ext;
		$_ret['small'] = $fn . '_small.' . $ext;
		$_ret['thumb'] = $fn . '_thumb.' . $ext;
		return $_ret;
	}


	/**
	 * Callback method that checks the slug of a gallery
	 * @access public
	 * @param string slug The Slug to check
	 * @return bool
	 */
	public function _check_slug($slug = '', $id=0)
	{
		if ( ! $this->galleries_m->check_exists('slug', $slug, $id))
		{
			$this->form_validation->set_message('_check_slug', sprintf(lang('galleries.already_exist_error'), $slug));
			return FALSE;
		}

		return TRUE;
	}

	private function _fetchUrl($url){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		// You may need to add the line below
		// curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);

		$feedData = curl_exec($ch);
		curl_close($ch); 

		return $feedData;
	
	}

	/**
	 * Check attachment dir, and create accordingly
	 *
	 * @param string Directory to check
	 * @return array
	 */
	function check_dir($dir)
	{
		// check directory
		$fileOK = array();
		$fdir = explode('/', $dir);
		$ddir = '';
		for($i=0; $i<count($fdir); $i++)
		{
			$ddir .= $fdir[$i] . '/';
			if (!is_dir($ddir))
			{
				if (!@mkdir($ddir, 0777)) {
					$fileOK[] = 'not_ok';
				}
				else
				{
					$fileOK[] = 'ok';
				}
			}
			else
			{
				$fileOK[] = 'ok';
			}
		}
		return $fileOK;

	}


}
