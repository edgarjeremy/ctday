<?php
/**
 * Galeri language definitions
 *
 * @package  	Galeri
 * @subpackage	Languages
 * @category  	Module
 */

// Success messages
$lang['gallery_images.upload_success']		= 'The image has been uploaded successfully.';
$lang['gallery_images.delete_success']		= 'The image has been deleted.';
$lang['gallery_images.changes_success']	 	= 'The changes have been saved.';
$lang['gallery_images.mass_delete_success']	= 'The media "%s" have been deleted.';

// Errors
$lang['gallery_images.upload_error']		= 'The image could not be uploaded.';
$lang['gallery_images.exists_error'] 		= 'The specified media does not exist.';
$lang['gallery_images.delete_error']	 	= 'The image could not be deleted.';
$lang['gallery_images.changes_error']		= 'The changes could not be saved.';
$lang['gallery_images.delete_no_img_error']	= 'No media have been deleted.';
$lang['gallery_images.mass_delete_error']	= 'Error deleting images.';
$lang['gallery_video.upload_error']			= 'The video "%s" could not be uploaded.';
$lang['gallery_video.thumbnail_error']		= 'The thumbnail file "%s" could not be processed.';

// Labels
$lang['gallery_images.upload_label']		= 'Upload Image';
$lang['gallery_images.edit_image_label']	= 'Edit Image';
$lang['gallery_images.thumbnail_label']		= 'Thumbnail';
$lang['gallery_images.action_label']		= 'Action';
$lang['gallery_images.crop_label']			= 'Crop Image';
$lang['gallery_images.recreate_label']		= 'Recreate thumbnail';
$lang['gallery_images.delete_label']		= 'Delete Image';
$lang['gallery_images.title_label']		 	= 'Title';
$lang['gallery_images.image_label']			= 'Image';
$lang['gallery_images.description_label']	= 'Caption';
$lang['gallery_images.gallery_label']		= 'Gallery';
$lang['gallery_images.no_images_label']		= 'No images have been added yet';
$lang['gallery_images.ratio_label']			= 'Maintain Ratio';
$lang['gallery_images.options_label']		= 'Options:';
$lang['gallery_images.crop.save_label']		= 'Click Save to apply the crop';
