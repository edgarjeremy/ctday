<?php
/**
 * Galeri admin view - list galleries
 *
 * @package  	Galeri
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>

<section class="title">
	<h4>Galleries</h4>
</section>

<section class="item">
	<div class="content">
		<?php $this->load->view('admin/partials/filters'); ?>
		<?php echo form_open('admin/galleries/action'); ?>

			<div id="filter-stage">
				<?php $this->load->view('admin/tables/posts'); ?>
			</div>
				
		<?php echo form_close(); ?>
	</div>
</section>