<?php
/**
 * Galeri admin view - upload image form
 *
 * @package  	Galeri
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<section class="title">

	<?php if($media->gallery_id): ?>
	 <h4><?php echo lang('galleries.media_edit_label'); ?> - <?php echo $media->media;?></h4>
	<?php else: ?>
	 <h4><?php echo "Upload" ?> - Media</h4>
	<?php endif; ?> 

</section>

<section class="item">

<?php echo form_open_multipart(uri_string(), array('class' => 'media_crud crud', 'id' => 'files_crud')); ?>

<div class="form_inputs">

<fieldset>
	<ul>

		<?php //if($media->galeria_id): ?>
		<li class="even">
		<div class="input">
			<?php if (in_array(strtolower(trim($media->extension,'.')), $format_videos)): ?>
				<?php if ($media->thumbnail): ?>
					<img title="<?php echo $media->thumbnail; ?>" src="<?php echo site_url().UPLOAD_PATH.'galleries/'.$media->galeria_id.'/'. substr($media->thumbnail, 0, -4) . '_thumb' . substr($media->thumbnail, -4);?>" alt="<?php echo $media->thumbnail; ?>" />
				<?php else: ?>
					<?php echo image('icon-video_thumb.jpg', 'galeria', array('alt' => 'Video file - No Thumbnail')); ?>
				<?php endif; ?>
			<?php else: ?>
				<img title="<?php echo $media->media; ?>" src="<?php echo site_url().UPLOAD_PATH.'galleries/'.$media->gallery_id.'/'. substr($media->media, 0, -4) . '_thumb' . substr($media->media, -4);?>" alt="<?php echo $media->media; ?>" />
			<?php endif; ?>
		</div>
		</li>
		<?php //else: ?>
		<li class="even">
			<label for="nothing"><?php echo lang('gallery_images.upload_label'); ?></label>
			<div class="input"><?php echo form_upload('userfile'); ?></div>
		</li>
		<?php //endif; ?>

		<?php if ($media->extension == 'yt'): ?>
		<li>
			<?php echo form_label(lang('galleries.youtube_link_label'), 'media'); ?>
			<div class="input"><?php echo form_input('media', $media->media); ?></div>
		</li>
		<?php endif; ?>

		<li>
			<?php echo form_label(lang('galleries.title_label'), 'title'); ?>
			<div class="input"><?php echo form_input('title', $media->title); ?></div>
		</li>
		<li class="even">
			<?php echo form_label(lang('galleries.subtitle_label'), 'title'); ?>
			<div class="input"><?php echo form_input('subtitle', $media->subtitle); ?></div>
		</li>
		<li>
			<?php echo form_label(lang('galleries.description_label'), 'description'); ?>
			<div class="clear"><br /></div>
			<?php echo form_textarea(array(
				'name'	=> 'description',
				'id'	=> 'description',
				'value'	=> $media->description,
				'style' => 'width:60%',
				'rows'	=> '3',
				'cols'	=> '5'
			)); ?>
		</li>
		<li class="<?php echo alternator('', 'even'); ?>">
			<label for="status"><?php echo lang('galleries.status_label'); ?></label>
			<div class="input">
				<?php echo form_dropdown('status', array('live'=>lang('galleries.status_live_label'), 'draft'=>lang('galleries.status_draft_label')), $media->status); ?>
			</div>
		</li>

		<li class="even">
			<?php echo form_label(lang('galleries.meta_title_label'), 'meta_title'); ?>
			<div class="input"><?php echo form_input('meta_title', $media->meta_title); ?></div>
		</li>
		<li>
			<?php echo form_label(lang('galleries.meta_keywords_label'), 'meta_keywords'); ?>
			<div class="input"><?php echo form_input('meta_keywords', $media->meta_keywords); ?></div>
		</li>
		<li class="even">
			<?php echo form_label(lang('galleries.meta_description_label'), 'meta_description'); ?>
			<div class="clear"><br /></div>
			<?php echo form_textarea(array(
				'name'	=> 'meta_description',
				'id'	=> 'meta_description',
				'value'	=> $media->meta_description,
				'style' => 'width:60%',
				'rows'	=> '3',
				'cols'	=> '5'
			)); ?>
		</li>
	</ul>

	<div class="align-right buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
		<a href="<?php echo site_url() ?>admin/galleries/manage/<?php echo $gallery_id?>" class="btn gray cancel">Cancel</a>
	</div>
</fieldset>

</div>

<?php echo form_close(); ?>

</section>