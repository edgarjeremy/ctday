<?php
/**
 * Galeri admin view - view to display newly uploaded images
 *
 * @package  	Galeri
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<!-- newly uploaded images -->
<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * views/admin/files/contents.php
 *
 * Contents view file, used when displaying newly uploaded files or
 * attached file images
 *
 * @version 2.0
 * @package galleries
 *
 */
?>
	<?php if (!empty($files)): ?>

    <?php foreach($files as $file): ?>
        <li class="ui-corner-all <?php echo 'img_'.$file->status ?>">
            <div class="actions">
        	<?php echo form_checkbox('imgid[]', $file->id); ?>
			<?php
				if (!in_array(strtolower(trim($file->extension,'.')), $this->format_videos))
				{
					echo anchor('admin/galleries/crop/'.$file->id, lang('galleries.crop_label'), 'class="imgcrop"' ) . ' | ';
				}

				echo anchor( (in_array(strtolower(trim($file->extension,'.')), $this->format_videos) ? 'admin/galleries/editvideo/' : 'admin/galleries/editmedia/') .$file->id, lang('galleries.edit_label'), 'class="no-edit"' );

				if (group_has_role('galleries', 'delete_image')) 
				{
					echo ' |'. anchor('admin/galleries/deletemedia', lang('galleries.delete_label'), 'class="imgdel"' );
				}
			?>
            </div>

		<?php if ((trim($file->extension,'.') == 'yt')): ?>

			<?php if ($file->thumbnail): ?>

					<?php if (substr($file->thumbnail, 0, 4) <> 'http'): ?>
				        <a title="<?php echo $file->thumbnail; ?>" href="<?php echo '/'.UPLOAD_PATH .'galleries/'.$gallery->id.'/'. $file->thumbnail; ?>" rel="cb_0" class="modal">
						<img title="<?php echo $file->thumbnail; ?>" src="<?php echo '/'.UPLOAD_PATH.'galleries/'.$gallery->id.'/'. substr($file->thumbnail, 0, -4) . '_thumb' . substr($file->thumbnail, -4);?>" alt="<?php echo $file->thumbnail; ?>" />
					<?php else: ?>
				        <a title="<?php echo $file->thumbnail; ?>" href="<?php echo str_ireplace('default.jpg', 'hqdefault.jpg', $file->thumbnail); ?>" rel="cb_0" class="modal">
						<img title="<?php echo $file->thumbnail; ?>" src="<?php echo $file->thumbnail;?>" alt="<?php echo $file->thumbnail; ?>" width="115" />
					<?php endif; ?>

			<?php else: ?>

		        <a title="<?php echo $file->title; ?>" href="<?php echo image_path('icon-video.jpg', 'galleries'); ?>" rel="cb_0" class="modal">
				<?php echo image('icon-video_thumb.jpg', 'galleries', array('alt' => 'Video file - No Thumbnail')); ?>

			<?php endif; ?>

		<?php elseif (in_array(strtolower(trim($file->extension,'.')), $this->format_videos)): ?>

			<?php if ($file->thumbnail): ?>

		        <a title="<?php echo $file->thumbnail; ?>" href="<?php echo '/'.UPLOAD_PATH .'galleries/'.$gallery->id.'/'. $file->thumbnail; ?>" rel="cb_0" class="modal">
	            <img title="<?php echo $file->title; ?>" src="<?php echo '/'.UPLOAD_PATH.'galleries/'.$gallery->id.'/'. substr($file->thumbnail, 0, -4) . '_thumb' . substr($file->thumbnail, -4);?>" alt="<?php echo $file->thumbnail; ?>" />

			<?php else: ?>

		        <a title="<?php echo $file->title; ?>" href="<?php echo image_path('icon-video.jpg', 'galleries'); ?>" rel="cb_0" class="modal">
				<?php echo image('icon-video_thumb.jpg', 'galleries', array('alt' => 'Video file - No Thumbnail')); ?>

			<?php endif; ?>

		<?php else: ?>
			<?php
				$filename = $file->media;
				$ext = substr(strrchr($filename, "."), 1);
				$fn = str_ireplace('.'.$ext, '', $filename);
				$fullfile = ( file_exists(UPLOAD_PATH .'galleries/'.$gallery->id.'/'. fullname($filename)) ? $fn . '_full.' . $ext : $filename );
				$thumbfile = $fn . '_thumb.' . $ext;
				$smallfile = $fn . '_small.' . $ext;
				$medfile = $fn . '_med.' . $ext;
			?>
	        <a title="<?php echo $file->title; ?>" href="<?php echo site_url().UPLOAD_PATH .'galleries/'.$gallery->id.'/'. $fullfile; ?>" rel="cb_0" class="modal">
            <img title="<?php echo $file->title; ?>" src="<?php echo site_url().UPLOAD_PATH.'galleries/'.$gallery->id.'/'. $thumbfile;?>" alt="<?php echo $file->media; ?>" />
		<?php endif; ?>
        </a>
				
<!--         <div style="font-size: 0.90em;padding:5px 0 0 9px; border-top: 1px solid #ccc;"> -->
        <div style="font-size: 0.90em;padding:5px 0 0 9px;">
        	<p><?php echo ($file->title ? $file->title : '[no title]'); ?><br />
        	<?php echo ($file->subtitle ? $file->subtitle : '[no subtitle]'); ?></p>
        	<?php echo ($file->description ? $file->description : '[no description]'); ?></p>
        </div>
        <input type="hidden" name="img_media_id[]" value="<?php echo $file->id; ?>" />
        </li>
    <?php endforeach; ?>

<?php else: ?>
	<li style="list-style-type: none;">
	<div class="blank-slate">	
		<h2><?php echo lang('galeri.no_media_error'); ?></h2>
	</div>
	</li>

<?php endif; ?>
