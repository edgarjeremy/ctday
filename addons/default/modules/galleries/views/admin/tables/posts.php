<?php if ( ! empty($galleries)): ?>

	<table border="0" class="table-list">
		<thead>
			<tr>
				<!-- <th width="30"><?php //echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th> -->
				<th><?php echo lang('galleries.gallery_label'); ?></th>
				<th>Slug</th>
				<th><?php echo lang('galleries.num_photos_label'); ?></th>
				<th width="100"><?php echo lang('cat_category_label'); ?></th>
				<th width="140"><?php echo lang('galleries.created_label'); ?></th>
				<th width="50"><?php echo lang('galleries.status_label'); ?></th>
				<th width="300" class="align-center"><?php echo lang('galleries.actions_label'); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="7">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach( $galleries as $galeria ): ?>
			<tr>
				<!-- <td><?php //echo form_checkbox('action_to[]', $galeria->id); ?></td> -->
				<td><?php echo $galeria->title; ?></td>
				<td><?php echo $galeria->slug; ?></td>
				<td><?php echo $galeria->media_count; ?></td>
				<td><?php echo $galeria->category; ?></td>
				<td><?php echo format_date($galeria->created_on); ?></td>
				<td><?php echo lang('galleries.status_'.$galeria->status.'_label'); ?></td>
				<td class="align-center buttons buttons-small">
					<?php echo anchor('admin/galleries/edit/'		. $galeria->id, 			lang('galleries.edit_label'), 'class="btn orange edit"'); ?>
					<?php echo anchor('admin/galleries/manage/'	. $galeria->id, 			lang('galleries.manage_label'), 'class="btn green"'); ?>
					<?php echo anchor('admin/galleries/delete/'	. $galeria->id, 			lang('galleries.delete_label'), array('class'=>'confirm btn red delete')); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div class="buttons align-right padding-top">
		<?php //$this->load->view('admin/partials/buttons', array('buttons' => array('delete') )); ?>
	</div>

<?php else: ?>

	<div class="no_data"><?php echo lang('galleries.no_galeria_error'); ?></div>

<?php endif;?>
