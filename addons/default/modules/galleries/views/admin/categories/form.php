<?php
/**
 * Galeri admin view - category form
 *
 * @package  	Galeri
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>

<section class="title">

<?php if ($this->controller == 'admin_categories' && $this->method === 'edit'): ?>
	<h4><?php echo sprintf(lang('cat_edit_title'), $category->title);?></h4>
<?php else: ?>
	<h4><?php echo lang('cat_create_title');?></h4>
<?php endif; ?>

</section>

<section class="item">
<div class="content">

<?php echo form_open($this->uri->uri_string(), 'class="crud" id="categories"'); ?>

<div class="form_inputs">
<fieldset>
	<ul>
		<li>
			<label for="title"><?php echo lang('cat_title_label');?><span>*</span></label>
			<?php echo  form_input('title', $category->title, 'id="title"'); ?>
		</li>
		<li class="<?php echo alternator('', 'even'); ?>">
			<label for="slug"><?php echo lang('cat_slug_label'); ?><span>*</span></label>
			<?php echo form_input('slug', $category->slug, 'id="slug" class="width-15"'); ?>
		</li>
		<li>
			<label for="status"><?php echo lang('cat_status_label'); ?><span>*</span></label>
			<?php echo form_dropdown('status', array('live' => lang('cat_live_label'), 'draft' => lang('cat_draft_label') ), $category->status) ?>
		</li>
	</ul>

	<div class="buttons float-right padding-top">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
	</div>
</fieldset>
</div>
<?php echo form_close(); ?>

</div>
</section>

<script>
(function ($) {
	$(function () {
		
		$("body").on("keyup", "#categories" ,function(e) {
        pyro.generate_slug($(this).find('input[name="title"]'), $(this).find('input[name="slug"]'));
        });
		
	});
})(jQuery);
</script>