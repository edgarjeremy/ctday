<nav id="shortcuts">
	<h6><?php echo lang('cp_shortcuts_title'); ?></h6>
	<ul class="list-links">
		<li><?php echo anchor('admin/galeri/create', lang('galeria.new_gallery_label'), 'class="add"') ?></li>
		<li><?php echo anchor('admin/galeri', lang('galeria.list_label')); ?></li>
		<li><?php echo anchor('admin/galeri/categories/create', lang('cat_create_title'), 'class="add"'); ?></li>
		<li><?php echo anchor('admin/galeri/categories', lang('cat_list_title'))?></li>
	</ul>
	<br class="clear-both" />
</nav>

