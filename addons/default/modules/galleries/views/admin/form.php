<?php
/**
 * Galeri admin view - gallery form
 *
 * @package  	Galeri
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<section class="title">

	<?php if ($this->method === 'create'): ?>
	<h4><?php echo lang('galleries.new_gallery_label'); ?></h4>
	<?php else: ?>
	<h4><?php echo sprintf(lang('galleries.edit_galeria_label'), $gallery->title); ?></h4>
	<?php endif; ?>
</section>

<section class="item">
<div class="content">

<?php echo form_open(uri_string(), 'class="crud" id="frmGallery"'); ?>

		<div class="form_inputs" id="gallery-content">

			<fieldset>
			<ul>
				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="category_id"><?php echo lang('cat_category_label'); ?></label>
					<div class="input">
						<?php echo form_dropdown('category_id', $categories, @$gallery->category_id) ?>
						[ <?php echo anchor('admin/galleries/categories/create_ajax', lang('cat_create_title'), 'target="_blank"'); ?> ]
					</div>
				</li>

				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="title"><?php echo lang('galleries.title_label'); ?><span>*</span></label>
					<div class="input">
						<?php echo form_input('title', htmlspecialchars_decode($gallery->title), 'maxlength="100" id="title"'); ?>
					</div>
				</li>

				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="subtitle"><?php echo lang('galleries.subtitle_label'); ?><span>*</span></label>
					<div class="input">
						<input type="text" id="subtitle" name="subtitle" maxlength="255" value="<?php echo $gallery->subtitle; ?>" />
					</div>
				</li>

				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="slug"><?php echo lang('galleries.slug_label'); ?><span>*</span></label>
					<div class="input">
						<?php echo form_input('slug', $gallery->slug, 'id="slug" class="width-15"'); ?>
					</div>
				</li>

				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="description"><?php echo lang('galleries.description_label'); ?></label>
					<br style="clear:both"/>
					<textarea id="description" name="description" class="wysiwyg-simple" rows="10"><?php echo $gallery->description ?></textarea>
					<?php //echo form_textarea(array('id'=>'description', 'name'=>'description', 'value' => $gallery->description, 'rows' => 10, 'class' => 'wysiwyg-simple')); ?>
				</li>

				<li class="<?php echo alternator('', 'even'); ?>">
					<label for="status"><?php echo lang('galleries.status_label'); ?></label>
					<div class="input">
						<?php echo form_dropdown('status', array('live'=>lang('galleries.status_live_label'), 'draft'=>lang('galleries.status_draft_label')), $gallery->status); ?>
					</div>
				</li>

				<li style="display: none;" class="images-placeholder <?php echo alternator('', 'even'); ?>">
					<label for="gallery_images"><?php echo lang('galleries.preview_label'); ?></label>
					<div class="clear-both"></div>
					<ul id="gallery_images_list">

					</ul>
					<div class="clear-both"></div>
				</li>

			</ul>
			</fieldset>

		</div>

	<div class="buttons align-right padding-top">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
	</div>

<?php echo form_close(); ?>

</div>
</section>