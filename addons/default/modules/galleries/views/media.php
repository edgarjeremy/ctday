<!-- Div containing all galleries -->
<div class="galleries_container" id="gallery_single">
<div id="thecontent" style="width:450px;height:350px;display:none;z-index:9999;">
	<iframe id="vidcontent" src="" frameborder="0" allowfullscreen width="450" height="350"></iframe>
	<a href="#" class="btn btn-mini" id="hidecontent">Close</a>
</div>
	<div class="clearfix"></div>
	<div class="gallery_content clearfix">
		<!-- The list containing the gallery images -->
		<ul class="galleries_list">
			<?php if ($images): ?>
			<?php foreach ( $images as $image): ?>
			<li style="width:120px; display:block; float: left; padding-right: 20px; padding-bottom: 25px;">
				<?php if ($image->extension == 'vim'): ?>
					<a target="_blank" rel="prettyPhoto" class="tips2" href="<?php echo $image->media ?>">
					<img width="120" class="tiptip" src="<?php echo $image->thumbnail  ?>" /></a>
				<?php elseif ($image->extension == 'yt'): ?>
					<a target="_blank" rel="prettyPhoto" class="tips2" href="<?php echo substr($image->media, 0, 5) == 'http:' ? $image->media : 'http://www.youtube.com/watch?v='.$image->media; ?>">
					<img class="tiptip" src="<?php echo ($image->thumbnail && substr($image->thumbnail,0,4) <> 'http') ? '/'.UPLOAD_PATH . 'galleries/'.$image->gallery_id.'/'.$image->thumbnail : str_ireplace("hqdefault.jpg", "default.jpg", $image->thumbnail)  ?>" /></a>
				<?php else: ?>
					<a href="<?php echo site_url(UPLOAD_PATH.'galleries/'.$image->gallery_id.'/'.$image->media); ?>"
					rel="prettyPhoto[pp_gal]" title="<?php echo $image->title; ?>">
					<img class="tiptip" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$image->gallery_id.'/'.thumbnail($image->media)); ?>" /></a>
				<?php endif; ?>

					<div class="tiptipContent" style="display:none;">
					<?php if ($image->description) echo '<div class="imageSubtitle">'.$image->description.'</div>';?>
					</div>

			</li>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</div>
<br style="clear: both;" />

<script>
$(document).ready(function() {

/*
	$("a[rel^='prettyPhoto']").click(function(e){
		e.preventDefault();
		var href = $(this).attr('href');
		$("#vidcontent").attr('src', href);
		$("#thecontent").fadeIn();
		//$.colorbox.resize();
	});

	$("#hidecontent").click(function(e){
		$("#thecontent").slideUp();
		$("#thecontent").html('');
	});
*/

	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools : '',
		modal:true,
		default_width:500,
		default_height:400
	});

	$('ul.galleries_list li a.ytvid').livequery('click', function(e){
		e.preventDefault();
		//var refdiv = $(this).attr('href');
/*
		var ytv = $(this).attr('rel');
		var el = '<iframe src="'+ytv+'" frameborder="0" allowfullscreen width="420" height="315"></iframe>';
*/
		//$("#"+refdiv).slideToggle();
		//$.colorbox.resize();
/*
		$(this).colorbox({
			inline: true,
			href: "#"+refdiv,
			innerWidth: "420px",
			innerHeight: "315px",
			onComplete: function(){ $.colorbox.resize(); }
		});
*/
	});

	$(".tiptip").each(function(){
		var tiptipContent = $(this).parent().next().html();
		if(!tiptipContent) tiptipContent = $(this).attr("title");
		var content = tiptipContent;
		$(this).removeAttr("title");
		$(this).tipTip({
			maxWidth: "220px",
			defaultPosition: 'bottom',
			content: tiptipContent
		});
	});


	$('.imgmodal').colorbox();

	// Tipsy
/*
	if (jQuery().tipsy)
	{
		$('ul.galleries_list li img.tiptip').livequery(function(){
			$(this).tipsy({
				gravity: 'n',
				title: function(){
			        	return $(this).nextAll('.tipContent').html();
			        },
				fade: true,
				html: true
			});
		});
	}
*/

});
</script>
