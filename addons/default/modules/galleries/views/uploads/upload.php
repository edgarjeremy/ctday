<div id="form-container" class="row">
<div class="form-event-wrapper">

	<?php echo form_open_multipart('', array('class' => 'crud', 'id' => 'frmUpload')); ?>

	<label class="col-xs-12 title">UPLOAD EVENT PHOTO</label>

	<div class="row">
	    <div class="col-md-6 col-xs-12 form-group">
	        <label for="name">Your Name*</label>
	        <input name="name" value="<?php echo $file->name ?>" type="text" class="form-control" id="name">
	        <ul class="dropdown-menu popovernote" role="menu">
	            <li role="presentation">
	                Name are for event clarification only, and will not be displayed on the website
	            </li>
	        </ul>
	    </div>
	    <div class="col-md-6 col-xs-12 form-group">
	        <label for="email">Your Email Address*</label>
	        <input name="email" value="<?php echo $file->email ?>" type="text" class="form-control" id="email">
	        <ul class="dropdown-menu popovernote" role="menu">
	            <li role="presentation">
	                Email are for clarification only, and will not be displayed on the website
	            </li>
	        </ul>
	    </div>
	</div>

	<div class="clearfix"></div>

	<div class="form-group">
		<div class="row">

			<div class="col-sm-6">
				<label>Event</label>
                <div class="form-group">

					<?php echo form_dropdown('event', $events, '', 'class="form-control"'); ?>

                </div>
			</div>

			<div class="col-sm-6">
				<label>Image</label>
                <div class="input-group">
                    <input type="text" class="form-control" data-name="textimgorganizer" placeholder="Upload event photo" style="margin-bottom: 0px;" readonly>
                    <div class="input-group-btn">
                        <div class="fileUpload btn btn-default btn-browse">
                            <span>Browse</span>
                            <input type="file" class="upload" name="imgfile" />
                        </div>
                    </div>
                </div>
			</div>

		</div>

		<!-- <input type="file" size="20" name="imgfile" /> -->
	</div>

	<div class="form-group">
		<label>Image caption<small>100 chars max</small></label>
		<input class="form-control" type="text" size="50" maxlength="100" name="caption" value="<?php echo set_value('caption'); ?>"/>
	</div>

	<div class="form-group">
		<button class="btn btn-primary pull-right">Upload</button>

<!-- 		<input type="submit" name="btnSubmit" value="Upload" /> -->
	</div>

	<div class="clearfix"></div>

	<div class="form-group">
		By uploading this image:
		<ul>
			<li>you confirm that you are the rightful owner of the image or that you have obtained the necessary permission to share it.</li>
			<li>you agree for the image to be posted on our website.</li>
		</ul>
	</div>

<?php echo form_close() ?>

</div>
</div>
