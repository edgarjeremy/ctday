<!-- Div containing all galleries -->
<div class="galleries_container" id="gallery_single">
	<div class="gallery_content clearfix">
		<!-- The list containing the gallery images -->
		<ul class="galleries_list">
			<?php if ($images): ?>
			<?php foreach ( $images as $image): ?>
			<li style="width:120px; display:block; float: left; padding-right: 20px; padding-bottom: 25px;">
				<?php if ($image->extension == 'yt'): ?>
					<a rel="prettyPhoto" class="" href="http://www.youtube.com/watch?v=<?php echo $image->media ?>">
					<img class="tiptip" src="<?php echo ($image->thumbnail && substr($image->thumbnail,0,4) <> 'http') ? '/'.UPLOAD_PATH . 'galleries/'.$image->gallery_id.'/'.$image->thumbnail : str_ireplace("hqdefault.jpg", "default.jpg", $image->thumbnail)  ?>" /></a>
				<?php else: ?>
					<a href="<?php echo site_url(UPLOAD_PATH.'galleries/'.$image->gallery_id.'/'.$image->media); ?>"
					rel="prettyPhoto[pp_gal]" title="<?php echo $image->title; ?>">
					<img class="tiptip" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$image->gallery_id.'/'.thumbnail($image->media)); ?>" /></a>
				<?php endif; ?>

<!--
					<div class="tipContent">
					<?php if ($image->title): ?>
						<?php if ($image->title) echo '<p class="title">'.$image->title.'</p>';?>
						<?php if ($image->subtitle) echo '<div class="imageSubtitle">'.$image->subtitle.'</div>';?>
						<?php if ($image->description) echo '<div class="imageSubtitle">'.$image->description.'</div>';?>
					<?php else: ?>
						<?php echo $image->media; ?>
					<?php endif; ?>
					</div>
-->

			</li>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</div>
<br style="clear: both;" />

<script>
$(document).ready(function() {

	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools : ''
	});

	$('ul.galleries_list li a.ytvid').livequery('click', function(e){
		e.preventDefault();
		//var refdiv = $(this).attr('href');
/*
		var ytv = $(this).attr('rel');
		var el = '<iframe src="'+ytv+'" frameborder="0" allowfullscreen width="420" height="315"></iframe>';
*/
		//$("#"+refdiv).slideToggle();
		//$.colorbox.resize();
/*
		$(this).colorbox({
			inline: true,
			href: "#"+refdiv,
			innerWidth: "420px",
			innerHeight: "315px",
			onComplete: function(){ $.colorbox.resize(); }
		});
*/
	});


	$('.imgmodal').colorbox();

	// Tipsy
/*
	if (jQuery().tipsy)
	{
		$('ul.galleries_list li img.tiptip').livequery(function(){
			$(this).tipsy({
				gravity: 'n',
				title: function(){
			        	return $(this).nextAll('.tipContent').html();
			        },
				fade: true,
				html: true
			});
		});
	}
*/

});
</script>
