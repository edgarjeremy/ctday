<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Endorsement English language definitions
 *
 * @package  	Endorsement
 * @subpackage	Languages
 * @category  	Module
 */

$lang['sponsors.role_put_live']		= 'Put Sponsor live';
$lang['sponsors.role_edit_live']		= 'Edit live Sponsor';
$lang['sponsors.role_delete_live'] 	= 'Delete live Sponsor';