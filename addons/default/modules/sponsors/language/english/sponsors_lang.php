<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sponsors English language definitions
 *
 * @package  	Sponsors
 * @subpackage	Languages
 * @category  	Module
 */

// labels
$lang['sponsors_name_label']				= 'Name';
$lang['sponsors_actions_label']				= 'Actions';
$lang['sponsors_add_label']					= 'Add Sponsor';
$lang['sponsors_browser_label']				= 'Browser';
$lang['sponsors_browser_version_label']		= 'Browser Version';
$lang['sponsors_clear_stats_label']			= 'Clear Stats';
$lang['sponsors_clicks_label']				= 'Clicks';
$lang['sponsors_country_code_label']			= 'Country Code';
$lang['sponsors_country_ip_label']			= 'IP';
$lang['sponsors_country_label']				= 'Country';
$lang['sponsors_date_label']					= 'Date';
$lang['sponsors_referer_label']				= 'Referer';

$lang['sponsors_delete_label']				= 'Delete';
$lang['sponsors_detail_label']				= 'More info';
$lang['sponsors_details_label']				= 'Details';
$lang['sponsors_draft_label']				= 'Draft';
$lang['sponsors_edit_label']					= 'Edit';
$lang['sponsors_end_date_label']				= 'End Date';
$lang['sponsors_filter_label']				= 'Filter';
$lang['sponsors_image_url_label']			= 'Or Enter Image URL';
$lang['sponsors_impressions_label']			= 'Impressions';
$lang['sponsors_link_url_label']				= 'Link URL';
$lang['sponsors_live_label']					= 'Live';
$lang['sponsors_location_label']				= 'Location';
$lang['sponsors_new_location_label']			= 'Add Location';
$lang['sponsors_new_window_label']			= 'New Window';
$lang['sponsors_no_records_msg']				= 'No records currently available for this sponsor.';
$lang['sponsors_platform_label']				= 'Platform';
$lang['sponsors_preview_label']				= 'Preview';
$lang['sponsors_self_window_label']			= 'Same Window';
$lang['sponsors_start_date_label']			= 'Start Date';
$lang['sponsors_time_label']					= 'Time';
$lang['sponsors_from_label']					= 'From';
$lang['sponsors_to_label']					= 'To';
$lang['sponsors_stats_label']				= 'Stats';
$lang['sponsors_status_label']				= 'Status';
$lang['sponsors_sticky_label']				= 'Static';
$lang['sponsors_target_label']				= 'Target Window';
$lang['sponsors_upload_image_label']			= 'Upload Image';
$lang['sponsors_view_label']					= 'View';

// titles
$lang['sponsors_sponsor_title']				= 'Sponsor';
$lang['sponsors_list_title']					= 'Sponsors';
$lang['sponsors_settings_title']				= 'Sponsors Settings';
$lang['sponsors_active_title']				= 'Active and Expirations Settings';
$lang['sponsors_stats_title']				= 'Current Statistics';
$lang['sponsors_no_sponsors']						= 'Currently No Sponsors';
$lang['sponsors_add_title']					= 'Add Sponsor';

// texts
$lang['sponsors_location_msg']				= 'Select where the sponsor would show.';
$lang['sponsors_exempt_groups_msg']			= 'Users in these groups will not see the sponsor.';
$lang['sponsors_ad_group_msg']				= 'Groups of categories where the sponsors will fit.';
$lang['sponsors_expire_msg']					= 'Leave blank to automatically expires on End Date.';
$lang['sponsors_start_date_msg']				= 'If you wish to use a start date, you must set the setting \'Active\' above to yes.<br />The sponsor will not show until the date you provide.';
$lang['sponsors_end_date_msg']				= 'Leave blank to not expire on certain date.<br />If you provide expiration terms above, the sponsor will expire when whichever happens first.';

// msgs
$lang['sponsors_add_error']					= 'Error while adding sponsor.';
$lang['sponsors_add_success']				= 'Sponsor has been added successfully.';
$lang['sponsors_clear_stats_empty']			= 'Statistics were already cleared or empty.';
$lang['sponsors_clear_stats_error']			= 'Error trying to clear the statistics.';
$lang['sponsors_clear_stats_success']		= 'Statistics were cleared successfully.';
$lang['sponsors_confirm_clearstats_title']	= 'Are you sure you want to clear the statistics? It cannot be undone.';
$lang['sponsors_confirm_delete_title']		= 'Are you sure you want to delete this? It cannot be undone.';
$lang['sponsors_delete_error']				= 'Error while deleting sponsor.';
$lang['sponsors_delete_success']				= 'Sponsor was deleted.';
$lang['sponsors_edit_error']					= 'Error while saving sponsor.';
$lang['sponsors_edit_success']				= 'Sponsor has been saved successfully.';
$lang['sponsors_image_change_notice']		= 'Old image "%s" was not deleted. You need to delete it yourself from %s.';
$lang['sponsors_image_delete_notice']		= 'Image "%s" was not deleted. You need to delete it yourself from %s.';
$lang['sponsors_image_larger_msg']			= 'The image you uploaded is larger (%s x %s) than what you have set (%s x %s).';
$lang['sponsors_mass_delete_success']		= '%s of %s sponsors was deleted.';
$lang['sponsors_new_image_upload_msg']		= 'Uploading new image will delete the original image file from the system.';
$lang['sponsors_no_image_msg']				= 'n/a';
$lang['sponsors_not_exist_error']			= 'Sponsor does not exist.';
$lang['sponsors_sticky_msg']				= 'Static sponsor will always be displayed';

/* End of file sponsors_lang.php */
