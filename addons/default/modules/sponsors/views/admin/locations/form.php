<?php
/**
 * Partners admin view - location form
 *
 * @package  	Partners
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<section class="title">
<?php if ($this->method == 'create'): ?>

	<h4><?php echo lang('ads_location_create_title');?></h4>

<?php else: ?>

	<h4><?php echo sprintf(lang('ads_location_edit_title'), $location->name);?></h4>

<?php endif; ?>
</section>

<section class="item">

<?php echo form_open($this->uri->uri_string(), 'class="crud" id="frmlocation"'); ?>

<span id="frmresultloc" class="form_inputs" >
	<ul>
		<li>
		<label for="name"><?php echo lang('ads_location_name_label');?><span>*</span></label>
		<?php echo  form_input('name', $location->name); ?>
		</li>

		<li class="even">
		<label for="description"><?php echo lang('ads_location_description_label'); ?></label>
		<div class="clear-both">
		<textarea name="description" id="description" rows="5" cols="30" style="width: 50%;"><?php echo $location->description ?></textarea>
		</div>
		</li>

		<li>
		<label><?php echo lang('ads_location_image_dim_label');?><small><?php echo lang('ads_location_image_limit_msg'); ?></small></label>
		<?php echo lang('ads_location_image_width_label'); ?>: <?php echo  form_input('maxwidth', $location->maxwidth, ' style="width:30px"'); ?> X <?php echo lang('ads_location_image_height_label'); ?>: <?php echo  form_input('maxheight', $location->maxheight, ' style="width:30px"'); ?>
		</li>
 	</ul>

<div class="buttons float-right padding-top">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
</div>

</span>

<?php echo form_close(); ?>

</section>