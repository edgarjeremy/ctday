<?php
/**
 * Advertisement admin view - lists available locations
 *
 * @package  	Advertisement
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>

<section class="title">
		<h4><?php echo lang('ads_location_list_title');?></h4>
</section>

<section class="item">

<?php echo form_open('admin/ads/locations/delete'); ?>
	<table border="0" class="table-list">
		<thead>
		<tr>
			<th style="width: 20px;"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
			<th style="width-10"><?php echo lang('ads_location_location_label');?></th>
			<th style="width-10"><?php echo lang('ads_location_tag_label');?></th>
			<th style="width-20"><?php echo lang('ads_location_description_label');?></th>
			<th style="width-15"><?php echo lang('ads_location_image_dim_label');?></th>
			<th style="width:10em"><span><?php echo lang('ads_location_actions_label');?></span></th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="6">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php if (!empty($locations)): ?>
			<?php foreach ($locations as $location): ?>
			<tr>
				<td><?php echo form_checkbox('action_to[]', $location->id); ?></td>
				<td><?php echo $location->name;?></td>
				<td><?php printf('{{ ads:display location="%s" }}', $location->slug);?></td>
				<td><?php echo $location->description;?></td>
				<td><?php if (!empty($location->maxwidth)) echo $location->maxwidth; ?><?php if (!empty($location->maxheight)) echo ' x ' . $location->maxheight; ?></td>
				<td>
					<?php echo anchor('admin/ads/locations/edit/' . $location->id, lang('ads_location_edit_label')) . ' | '; ?>
					<?php echo anchor('admin/ads/locations/delete/' . $location->id, lang('ads_location_delete_label'), array('class'=>'confirm'));?>
				</td>
			</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="3"><?php echo lang('ads_location_no_locations');?></td>
			</tr>
		<?php endif; ?>
		</tbody>
	</table>
<div class="buttons float-right padding-top">
	<?php if (!empty($locations)) $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )); ?>
</div>
<?php echo form_close(); ?>

</section>