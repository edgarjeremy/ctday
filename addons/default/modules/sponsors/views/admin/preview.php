<?php
/**
 * Partner admin view - preview an ad
 *
 * @package  	Sponsors
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<h2 id="preview_heading">Sponsor Preview</h2>

<hr />
<div style="display:block;clear:both;">
<?php echo (isset($content->content) && $content->content) ? $content->content : '<p>[nothing to display]</p>'; ?>
</div>


