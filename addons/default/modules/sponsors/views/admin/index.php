<?php
/**
 * Sponsors admin view - lists available sponsors
 *
 * @package  	Sponsors
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<section class="title">
		<h4><?php echo lang('sponsors_list_title');?></h4>
</section>

<section class="item">
	<div class="content">

	<?php echo form_open('admin/sponsors/action');?>

		<?php if (!empty($sponsors)): ?>

			<table border="0" class="table-list sortable">    
				<thead>
					<tr>
						<th class="width-5"><?php echo form_checkbox('action_to_all', NULL, NULL, 'class="check-all"');?></th>
						<th class="width-15"><?php echo lang('sponsors_preview_label');?></th>
						<th class="width-5"><?php echo lang('sponsors_sticky_label');?></th>
						<th class="width-5"><?php echo lang('sponsors_clicks_label');?></th>
						<th class="width-5"><?php echo lang('sponsors_impressions_label');?></th>
						<th class="width-5"><?php echo lang('sponsors_status_label');?></th>
						<th width="400" class="align-center"><span><?php echo lang('sponsors_actions_label'); ?></span></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="8">
							<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($sponsors as $article): ?>
						<tr>
							<td><?php echo form_checkbox('action_to[]', $article->id);?></td>
							<td>
								<?php
									if (!empty($article->image))
										echo anchor(site_url($sponsors_image_dir.'/'.$article->image), '<img src="'.site_url($sponsors_image_dir.'/'.substr($article->image, 0, -4) . '_thumb' . substr($article->image, -4)).'" />', 'class="modal" target="_blank" title="'.lang('sponsors_view_label').' '.$article->image.'"');
									else
										echo lang('sponsors_no_image_msg');
								?>
							</td>
							<td><span style="font-size:20px;"><?php echo ($article->sticky ? '&#8226;' : '&ordm;');?></span></td>
							<td ><?php echo $article->clicks;?></td>
							<td ><?php echo $article->impressions;?></td>
							<td ><?php echo lang('sponsors_'.$article->status.'_label');?></td>
							<td class="align-center buttons buttons-small">
								<?php //echo anchor('admin/sponsors/stats/' . $article->id, lang('sponsors_stats_label'), 'class="button colorbox stats" ');?>
								<?php echo anchor('admin/sponsors/clearstats/' . $article->id, lang('sponsors_clear_stats_label'), 'title="'.lang('sponsors_confirm_clearstats_title').'" class="btn colorbox orange clearstats" ');?>
								<?php
									if (!empty($article->image))
									{
										echo anchor(site_url($sponsors_image_dir.'/'.$article->image), lang($article->status == 'live' ? 'sponsors_view_label' : 'sponsors_preview_label'), 'rel="modal" class="colorbox btn blue preview" target="_blank"');
									}
								?>
								<?php echo anchor('admin/sponsors/edit/' . $article->id, lang('sponsors_edit_label'), 'class="btn green edit"');?>
								<?php echo anchor('admin/sponsors/delete/' . $article->id, lang('sponsors_delete_label'), array('class'=>'confirm btn red delete', 'title'=>lang('sponsors_confirm_delete_title'))); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>	
			</table>
			
			<div class="table_action_buttons">
				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )); ?>
			</div>

		<?php else: ?>

			<div class="no_data"><?php echo lang('sponsors_no_sponsors'); ?></div>

		<?php endif; ?>

	<?php echo form_close();?>

	</div>
</section>

