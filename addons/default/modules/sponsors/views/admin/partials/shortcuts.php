<nav id="shortcuts">
	<h6><?php echo lang('cp_shortcuts_title'); ?></h6>
	<ul class="list-links">
		<li><?php echo anchor('admin/ads/create', lang('ads_add_label'), 'class="add"') ?></li>
		<li><?php echo anchor('admin/ads', lang('ads_list_title')); ?></li>
		<li><?php echo anchor('admin/ads/locations/create', lang('ads_location_create_title'), 'class="add"'); ?></li>
		<li><?php echo anchor('admin/ads/locations', lang('ads_location_list_title'))?></li>
	</ul>
	<br class="clear-both" />
</nav>

