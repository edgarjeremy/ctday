<?php
/**
 * Partners admin view - filter form
 *
 * @package  	Partners
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<div class="filter">
<?php echo form_open('', 'id="filter"'); ?>
<?php echo form_hidden('f_module', $module_details['slug']); ?>
<ul>
	<li>
		<?php echo lang('partners_country_label'); ?>
		<?php echo form_dropdown('f_country', array(0 => lang('select.all')) + $countries, NULL, 'class="no-uniform"'); ?> |&nbsp;
	</li>

	<li>
		<?php echo lang('partners_platform_label'); ?>
		<?php echo form_dropdown('f_platform', array(0 => lang('select.all')) + $platforms, NULL, 'class="no-uniform"'); ?> |&nbsp;
	</li>

	<li>
		<?php echo lang('partners_browser_label'); ?>
		<?php echo form_dropdown('f_browser', array(0 => lang('select.all')) + $browsers, NULL, 'class="no-uniform"'); ?> |&nbsp;
	</li>
</ul>
<br class="clear-both">
<ul>
	<li class="datefilter">
		<?php echo lang('partners_start_date_label'); ?>
		<?php echo form_input('f_start_date', NULL, 'class="no-uniform dpicker" size="9"'); ?>

		<?php echo lang('partners_end_date_label'); ?>
		<?php echo form_input('f_end_date', NULL, 'class="no-uniform dpicker" size="9"'); ?>
	</li>

	<li>&nbsp;|&nbsp; </li>

	<li class="datefilter">
		<?php echo lang('partners_time_label'); ?>
		<?php echo lang('partners_from_label'); ?>
		<?php echo form_dropdown('f_starttime', array(0 => lang('select.all')) + $hours, NULL, 'class="no-uniform"'); ?> |&nbsp;

		<?php echo lang('partners_to_label'); ?>
		<?php echo form_dropdown('f_endtime', array(0 => lang('select.all')) + $hours, NULL, 'class="no-uniform"'); ?> |&nbsp;
	</li>

	<li><?php echo anchor(current_url(), lang('partners_filter_label'), 'id="go" class="go"'); ?></li>

	<li><?php echo anchor(current_url(), lang('buttons.cancel'), 'class="cancel"'); ?></li>
</ul>
<?php echo form_close(); ?>
<br class="clear-both">
</div>
