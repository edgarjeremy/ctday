<?php
/**
 * Sponsor admin view - lists records/statistics of sponsor
 *
 * @package  	Sponsor
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<section class="title">
		<h4><?php echo lang('sponsors_stats_title');?></h4>
</section>

<section class="item">

		<?php echo '&nbsp;<img src="'.site_url($sponsors_image_dir.'/'.substr($sponsor->image, 0, -4) . '_thumb' . substr($sponsor->image, -4)).'" />'; ?>

		<?php if (!empty($records)): ?>

			<span class="buttons buttons-small" style="float: right">

			<?php echo anchor('admin/sponsors/clearstats/' . $sponsor->id, lang('sponsors_clear_stats_label'), 'rel="admin/sponsors/stats/'.$sponsor->id.'" title="'.lang('sponsors_confirm_clearstats_title').'" class="button colorbox clearstats" ');?></span>


			<table border="0" class="table-list">    
				<thead>
					<tr>
						<th class="width-15"><?php echo lang('sponsors_date_label');?></th>
						<th class="width-15"><?php echo lang('sponsors_referer_label');?></th>
						<th class="width-15"><?php echo lang('sponsors_country_ip_label');?></th>
						<th class="width-10"><?php echo lang('sponsors_platform_label');?></th>
						<th class="width-5"><?php echo lang('sponsors_browser_label');?></th>
						<th class="width-5"><?php echo lang('sponsors_browser_version_label');?></th>
						<th class="width-5"><?php echo lang('sponsors_country_label');?></th>
						<!-- <th class="width-5"><?php echo lang('sponsors_clicks_label');?></th> -->
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="8">
							<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($records as $article): ?>
						<tr>
							<td><?php echo $article->click_date ?  ( strstr($this->settings->date_format, '%') ? strftime($this->settings->date_format . ', %H:%M', $article->click_date) : date($this->settings->date_format . ', H:i', $article->click_date ) ) : '--';?></td>
							<td><?php echo $article->referer ? $article->referer : '--';?></td>
							<td><?php echo $article->countryip ? $article->countryip : '--';?></td>
							<td><?php echo $article->platform ? $article->platform : '--';?></td>
							<td ><?php echo $article->browser ? $article->browser : '--';?></td>
							<td ><?php echo $article->browser_version ? $article->browser_version : '--';?></td>
							<td ><?php echo $article->countryname ? $article->countryname : 'Unknown';?></td>
							<!-- <td ><?php echo !empty($article->total) ? number_format($article->total) : '--';?></td> -->
						</tr>
					<?php endforeach; ?>
				</tbody>	
			</table>

		<?php else: ?>

			<div class="no_data"><?php echo lang('sponsors_no_records_msg'); ?></div>

		<?php endif; ?>

</section>
