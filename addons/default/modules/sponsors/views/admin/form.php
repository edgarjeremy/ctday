<?php
/**
 * Sponsors admin view - ad form
 *
 * @package  	Sponsors
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>

<section class="title">

<?php if($this->method == 'create'): ?>
	<h4><?php echo lang('sponsors_add_title');?></h4>				
<?php else: ?>
	<h4><?php echo lang('sponsors_edit_label') . ' - ' . $sponsor->name;?></h4>
<?php endif; ?>

</section>

<section class="item">
	<div class="content">

	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>

			<div class="tabs">
			
				<ul class="tab-menu">
					<li><a href="#sponsors-content"><span><?php echo lang('sponsors_sponsor_title'); ?></span></a></li>
					<li><a href="#sponsors-statistics"><span>Stats</span></a></li>
				</ul>
				
				<!-- content -->
				<div class="form_inputs" id="sponsors-content">

					<fieldset>
					<ul>

						<li>
							<label for="name"><?php echo lang('sponsors_name_label');?><span>*</span></label>
							<div class="input"><?php echo form_input('name', $sponsor->name, 'maxlength="250"'); ?></div>
						</li>

						<li class="even date-meta">
                		<!-- date from -->
			            <label><?php echo lang('sponsors_start_date_label');?></label>
                      	<div class="input">
		                      <?php
		                      if(!$sponsor->start_date){
		                        $date_from = date('Y-m-d');
		                      }
		                      else{
				                $date_from = isset($sponsor->start_date) ? date('Y-m-d',$sponsor->start_date) : date('Y-m-d', strtotime($sponsor->date_from_year.'-'.$sponsor->date_from_month.'-'.$sponsor->date_from_day));
		                      }

		                      echo form_input('start_date_str', htmlspecialchars_decode($date_from), 'maxlength="10" id="start_date" class="text width-15 datepicker"');
		                      ?>
	                     </div>
				        </li>

<!--
						<li>
							<label for="sticky"><?php echo lang('sponsors_sticky_label'); ?><small><?php echo lang('sponsors_sticky_msg'); ?></small></label>
							<div class="input type-radio"><label class="inline"><input id="sticky" type="checkbox" value="1" <?php echo ( $sponsor->sticky ? 'checked="checked"' : '' ); ?> name="sticky"> Static</label></div>
						</li>
-->

						<li class="even">
							<label><?php echo lang('sponsors_upload_image_label'); ?><small><?php echo lang('sponsors_new_image_upload_msg'); ?></small></label>
							<div class="input">
							<?php
							if (!empty($sponsor->image))
							{
									//echo '<div style="width:auto;padding:5px;padding-left:160px;padding-top: 9px;">';
									echo '<div>';
									echo anchor(site_url($sponsors_image_dir.'/'.$sponsor->image), '<img src="'.site_url($sponsors_image_dir.'/'.substr($sponsor->image, 0, -4) . '_thumb' . substr($sponsor->image, -4)).'" />', 'rel="modal" class="colorbox" target="_blank"');
									echo form_hidden('oldcontent', $sponsor->image);
									echo '</div>';
							}
							?>
								<input type="file" name="adimage" size="32" />
							</div>
						</li>

						<li>
							<label><?php echo lang('sponsors_target_label');?></label>
							<div class="input"><?php echo form_dropdown('target', array('_self'=>lang('sponsors_self_window_label'), '_blank'=>lang('sponsors_new_window_label')), $sponsor->target) ?></div>
						</li>

						<li class="even">
							<label for="link_url"><?php echo lang('sponsors_link_url_label');?><span>*</span></label>
							<div class="input"><?php echo form_input('link_url', (!empty($sponsor->link_url) ? urldecode($sponsor->link_url) : ''), 'maxlength="250"'); ?></div>
						</li>

						<li class="even">
							<label><?php echo lang('sponsors_status_label');?></label>
							<div class="input"><?php echo form_dropdown('status', array('live'=>lang('sponsors_live_label'), 'draft'=>lang('sponsors_draft_label')), $sponsor->status) ?></div>
						</li>

					</ul>
					</fieldset>

				</div>

				<!-- Statistics tab -->
				<div class="form_inputs" id="sponsors-statistics">
					<fieldset>
					<ul>
						<li class="even"><label><?php echo lang('sponsors_impressions_label'); ?></label>
						<div class="input"><?php echo ($sponsor->impressions ? $sponsor->impressions : '0'); ?></div></li>
						<li><label><?php echo lang('sponsors_clicks_label'); ?></label><div class="input"><?php echo ($sponsor->clicks ? $sponsor->clicks : '0'); ?></div></li>
						<?php if ($this->method<>'create'): ?>
						<li><label></label><div class="input"><?php echo anchor('admin/sponsors/stats/'.$sponsor->id, lang('sponsors_detail_label') . ' &raquo;', 'class="modal"') ?></div></li>
						<?php endif; ?>
					</ul>
					</fieldset>
				</div>

			</div>	
		
			<div class="buttons">
					<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
			</div>

		<?php echo form_close(); ?>

	</div>
</section>