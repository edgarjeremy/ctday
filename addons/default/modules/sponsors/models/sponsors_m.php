<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sponsors model
 *
 * @package		Sponsors
 * @subpackage	Models
 * @category  	Module
 */
class Sponsors_m extends MY_Model
{
	public $upload_cfg = array();

	/**
	 * Maximum width of image before being resized. Read from sponsors location
	 */
	public $maxwidth;

	/**
	 * Maximum height of image before being resized. Read from sponsors location
	 */
	public $maxheight;

	/**
	 * Initial thumbnail image width.
	 */
	public $thumbwidth = 70;

	/**
	 * Initial thumbnail image height
	 */
	public $thumbheight = 75;

	/**
	 * Width and height of ad location
	 */
#	public $loc = array();

/*
	function __construct()
	{
		parent::__construct();
		echo '$thumbwidth: ' . $this->thumbwidth . '; $maxheight' .$this->maxheight . '<br />';
	}
*/

	/**
	 * Retrieve all sponsor
	 * @access public
	 * @param bool $front Whether it is called from frontend or admin
	 * @return object
	 */
    function get_all($front = false)
    {
    	$select = '
    		sponsors.*
    	';
    	$this->db->select($select);

		$this->db->order_by('sticky', 'DESC');
		$this->db->order_by('position', 'ASC');
    	//$this->db->order_by('start_date', 'ASC');
           
        return $this->db->get('sponsors')->result();
    }

	/**
	 * Retrieve a single sponsor
	 * @access public
	 * @param int $id ID of sponsor to retrieve
	 * @return object
	 */
	function get($id=null) {
		if (!$id) return false;
    	$select = '
    		sponsors.*
    	';
    	$this->db->select($select);
		$this->db->where('sponsors.id', $id);
    	$this->db->order_by('start_date', 'ASC');
           
		return $this->db->get('sponsors')->row();
	}

	/**
	 * Retrieve random sponsors
	 * @access public
	 * @param int $limit How many to retrieve
	 * @param string $location Partner's location to retrieve
	 * @param bool $sticky Retrieve sticky sponsors?
	 * @return object
	 */
	function get_random($limit=0, $location=null, $sticky=null)
	{
		$stickies = '';
		if ($sticky == 'yes')
		{
			$this->db->order_by('sticky');
		}

		$this->db->order_by('RAND()');
		$this->db->order_by('position', 'asc');
		$this->db->where('status', 'live');

		if ($limit)
			$this->db->limit($limit);

		return $this->db->get('sponsors')->result();
	}

	/**
	 * Retrieve many sponsors based on certain parameters
	 * @access public
	 * @param array $params The parameters
	 * @return object
	 */
	function get_many_by($params = array())
    {
    	$this->load->helper('date');

    	// Is a status set?
    	if( !empty($params['status']) )
    	{
    		// If it's all, then show whatever the status
    		if($params['status'] != 'all')
    		{
	    		// Otherwise, show only the specific status
    			$this->db->where('status', $params['status']);
    		}
    	}
    	// Nothing mentioned, show live only (general frontend stuff)
    	else
    	{
    		$this->db->where('status', 'live');
    	}

    	// By default, dont show future articles
    	if(!isset($params['show_future']) || (isset($params['show_future']) && $params['show_future'] == FALSE))
    	{
    		$this->db->where('sponsors.start_date <=', now());
    	}


       	// Limit the results based on 1 number or 2 (2nd is offset)
       	if(isset($params['limit']) && is_array($params['limit'])) $this->db->limit($params['limit'][0], $params['limit'][1]);
       	elseif(isset($params['limit'])) $this->db->limit($params['limit']);
    	
    	return $this->get_all();
    }

	/**
	 * Count how many sponsors we have
	 * @access public
	 * @param array $params The parameters
	 * @return int
	 */
	function count_by($params = array())
    {
    	// Is a status set?
    	if( !empty($params['status']) )
    	{
    		// If it's all, then show whatever the status
    		if($params['status'] != 'all')
    		{
	    		// Otherwise, show only the specific status
    			$this->db->where('status', $params['status']);
    		}
    	}
    	// Nothing mentioned, show live only (general frontend stuff)
    	else
    	{
    		$this->db->where('status', 'live');
    	}

		return $this->db->count_all_results('sponsors');
    }

	/**
	 * Count how many records an sponsor has
	 * @access public
	 * @param array $params The parameters
	 * @param array $params['ad_id'] ID of ad to check
	 * @param array $params['id'] ID of the record to check
	 * @param array $params['platform'] Platform (O/S) to check
	 * @param array $params['browser'] Browser to check
	 * @param array $params['countryname'] Country (O/S) to check
	 * @param array $params['start_date'] Count records from this date
	 * @param array $params['end_date'] Count records before this date
	 * @return int
	 */
	function count_record_by($params = array())
    {
    	// ad_id?
    	if( !empty($params['ad_id']) )
    	{
   			$this->db->where('ad_id', $params['ad_id']);
    	}

    	// Is id set?
    	if( !empty($params['id']) )
    	{
   			$this->db->where('id', $params['id']);
    	}

    	// Count by platform
    	if( !empty($params['platform']) )
    	{
   			$this->db->where('platform', $params['platform']);
    	}

    	// Count by browser
    	if( !empty($params['browser']) )
    	{
   			$this->db->where('browser', $params['browser']);
    	}

    	// Count by country name
    	if( !empty($params['countryname']) )
    	{
   			$this->db->where('countryname', $params['countryname']);
    	}

    	// Count by start date
    	if( !empty($params['start_date']) )
    	{
    		$sdate = explode('-', $params['start_date']);
    		$ddate = mktime(0,0,0, $sdate[1], $sdate[2], $sdate[0]);
   			$this->db->where('click_date >', $ddate);
    	}

    	// Count by start date
    	if( !empty($params['end_date']) )
    	{
    		$edate = explode('-', $params['end_date']);
    		$ddate = mktime(0,0,0, $edate[1], $edate[2], $edate[0]);
   			$this->db->where('click_date <', $ddate);
    	}

		return $this->db->count_all_results('sponsors_records');

    }


	/**
	 * Get the statistics of an sponsor
	 * @access public
	 * @param array $params The parameters
	 * @param array $params['ad_id'] ID of ad to check
	 * @param array $params['id'] ID of the record to check
	 * @param array $params['platform'] Platform (O/S) to check
	 * @param array $params['browser'] Browser to check
	 * @param array $params['countryname'] Country (O/S) to check
	 * @param array $params['start_date'] Count records from this date
	 * @param array $params['end_date'] Count records before this date
	 * @return object
	 */
	function get_stats($params=array())
	{

		$id = $params['id'];
       	// Limit the results based on 1 number or 2 (2nd is offset)
       	if(isset($params['limit']) && is_array($params['limit'])) $this->db->limit($params['limit'][0], $params['limit'][1]);
       	elseif(isset($params['limit'])) $this->db->limit($params['limit']);

    	// Filter by platform
    	if( !empty($params['platform']) )
    	{
   			$this->db->where('platform', $params['platform']);
    	}

    	// Filter by browser
    	if( !empty($params['browser']) )
    	{
   			$this->db->where('browser', $params['browser']);
    	}

    	// Filter by country name
    	if( !empty($params['countryname']) )
    	{
   			$this->db->where('countryname', $params['countryname']);
    	}

    	// Filter by start date
    	if( !empty($params['start_date']) )
    	{
    		$sdate = explode('-', $params['start_date']);
    		$ddate = mktime(0,0,0, $sdate[1], $sdate[2], $sdate[0]);
   			$this->db->where('click_date >', $ddate);
    	}

    	// Filter by end date
    	if( !empty($params['end_date']) )
    	{
    		$edate = explode('-', $params['end_date']);
    		$ddate = mktime(23,59,0, $edate[1], $edate[2], $edate[0]);
   			$this->db->where('click_date <', $ddate);
    	}

    	// Filter by start time
    	if( !empty($params['start_time']) )
    	{
    		$start = "HOUR( FROM_UNIXTIME(click_date) ) >= " . $params['start_time'];
   			$this->db->where($start);
    	}

    	// Filter by end time
    	if( !empty($params['end_time']) )
    	{
    		$end = "HOUR( FROM_UNIXTIME(click_date) ) < " . $params['end_time'];
   			$this->db->where($end);
    	}

		if (isset($params['summary']))
		{
			return $this->db->select('*, COUNT(ad_id) AS total, COUNT(countryip) AS IP')
				->where('ad_id', $id)
				->group_by('countryip')
				->order_by('total', 'DESC')
				->get('sponsors_records')->result();
		}
		else
		{
			return $this->db
				->where('ad_id', $id)
				->order_by('click_date', 'DESC')
				->get('sponsors_records')->result();
		}

	}


	/**
	 * Clear the statistics of an sponsor
	 * @access public
	 * @param int $id ID of the sponsor
	 * @return bool
	 */
	function clear_stats($id)
	{
		$this->update($id, array('impressions' => 0, 'clicks'=>0));
		$this->db->where('ad_id', $id)->delete('sponsors_records');
		return $this->db->affected_rows();
	}


	/**
	 * Get statistics, grouped by the parameter supplied
	 * @access public
	 * @param array $params The parameters to use
	 * @return object
	 */
	function get_grouped_stats($params=array())
	{
		if( !empty($params['group']) )
		{
			$this->db->group_by($params['group'])
				->order_by($params['group'], 'ASC');
		}

		return $this->db->get('sponsors_records')->result();
		
	}

	/**
	 * Insert new sponsor to the database
	 * @access public
	 * @param array $input Data to insert
	 * @param array $image Image data
	 * @return bool
	 */
    function insert($input = array(), $image = array())
    {
   		$this->load->helper('date');
   		$input['created_on'] = now();
   		$input['updated_on'] = now();

		return parent::insert($input);
    }
    
	/**
	 * Update a sponsor
	 * @access public
	 * @param int $id ID of sponsor to update
	 * @param array $input Data to update
	 * @param array $image Image data
	 * @return bool
	 */
    function update($id, $input, $image=array())
    {
   		$this->load->helper('date');
   		$input['updated_on'] = now();

		$chk_dir = $this->check_dir($this->upload_cfg['upload_path']);
		if (in_array('not_ok', $chk_dir))
			$dirOK = FALSE;
		else
			$dirOK = TRUE;

		return $this->db->update('sponsors', $input, "id = $id");

/*
		if ($img_ok)
	    	return $this->db->update('sponsors', $input, "id = $id");
	    else
	    	return false;
*/
    }

	/**
	 * Update statistics of an sponsor
	 * @access public
	 * @param int $id ID of sponsor to update
	 * @param array $input Data to update
	 * @return bool
	 */
	function update_stats($id, $input) {
		return $this->db->update('sponsors', $input, "id = $id");
	}

	/**
	 * Publish an sponsor
	 * @access public
	 * @param int $id ID of sponsor to publish
	 * @return bool
	 */
    function publish($id = 0)
    {
    	return parent::update($id, array('status' => 'live'));
    }

	/**
	 * Check sponsor directory, and create it accordingly
	 * @access public
	 * @param string $dir Directory to check
	 * @return array
	 */
	function check_dir($dir)
	{
		// check directory
		$fileOK = array();
		$fdir = explode('/', $dir);
		$ddir = '';
		for($i=0; $i<count($fdir); $i++)
		{
			$ddir .= $fdir[$i] . '/';
			if (!is_dir($ddir))
			{
				if (!@mkdir($ddir, 0777)) {
					$fileOK[] = 'not_ok';
				}
				else
				{
					$fileOK[] = 'ok';
				}
			}
			else
			{
				$fileOK[] = 'ok';
			}
		}
		return $fileOK;

	}

}

?>
