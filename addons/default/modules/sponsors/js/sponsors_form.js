(function($) {
	$(function(){

		// add location
		// form options for locations
	    var optLocation = {
	    	url: BASE_URL + 'admin/ads/locations/create_ajax',
	    	type: 'POST',
	        target:        '#frmresultloc',
	        beforeSubmit:  function(){
	        	$('#frmresultloc').fadeOut();
			},  	// pre-submit callback
			success: function (d){
				var obj = $.parseJSON(d);
				if(obj.status == 'ok') {
					//succesfull db insert do this stuff
					var select = 'select[id=locations]';
					var opt_val = obj.location_id;
					var opt_text = obj.name;
					var option = '<option value="'+opt_val+'" selected="selected">'+opt_text+'</option>';

					//append to dropdown the new option
					$(select).append(option);

					//uniform workaround
					$('#ads-content li.locations span').html(obj.name);

					//close the colorbox
					$.colorbox.close();
				} else {
					//no dice
					//append the message to the dom
					$('#cboxLoadedContent').html(obj.message + obj.form);
					$('#cboxLoadedContent p:first').addClass('notification error').show();
				}
			}
	    }; 
	 
	    // bind form using 'ajaxForm' 
		$('#frmlocation').livequery(function() {
			$(this).ajaxForm(optLocation);
		});

			$('#addLocation').colorbox({
				srollable: false,
				innerWidth: 550,
				innerHeight: 500,
				iframe: false,
				href: BASE_URI + 'admin/ads/locations/create_ajax',
			});


	});
})(jQuery);


