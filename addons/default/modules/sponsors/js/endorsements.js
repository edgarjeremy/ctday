(function($) {
	$(function(){

		$('.stats').livequery('click', function(){
			$(this).colorbox({
				scrolling: true,
				width: '60%',
				height: '50%',
				maxHeight: '90%',
				onComplete: function(){
					$.colorbox.resize();
				}
			});
		});

	});
})(jQuery);