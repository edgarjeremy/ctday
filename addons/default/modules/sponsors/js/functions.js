(function($) {
	$(function(){

		$(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});

		// Clear stats confirmation
		$('a.clearstats').live('click', function(e){
			e.preventDefault();

			var href		= $(this).attr('href'),
				removemsg	= $(this).attr('title');

			if (confirm(removemsg || 'Clearing statistics. Are you sure?'))
			{
				$(this).trigger('click-confirmed');

				if ($.data(this, 'stop-click')){
					$.data(this, 'stop-click', false);
					return;
				}

				//submits it whether uniform likes it or not
				window.location.replace(href);
			}
		});

		$('.sortable tbody').livequery(function(){
			$(this).sortable({
				handle: 'td',
				items: 'tr',
				start: function(event, ui) {
					ui.helper.find('a').unbind('click').die('click');
				},
				update: function() {
					order = new Array();
					$('tr', this).each(function(){
						order.push( $(this).find('input[name="action_to[]"]').val() );
					});
					order = order.join(',');
					$.post(SITE_URL + 'admin/ads/reorder', { order: order });
				}
			}).disableSelection();
		});

	});
})(jQuery);