<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Specification details for Sponsors module
 *
 *
 * @version 1.07
 * @package		Sponsors
 */
class Module_Sponsors extends Module {

	public $version = '2.0';
	
	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Sponsors'
			),
			'description' => array(
				'en' => 'Manage your sponsors.',
			),
			'frontend' => TRUE,
			'backend'  => TRUE,
			'menu'	  => 'content',
			'roles' => array(
				'put_live', 'edit_live', 'delete_live', 'put_img_live', 'edit_img_live', 'delete_img_live'),
			'sections' => array(
			    'sponsors' => array(
				    'name' => 'sponsors_list_title',
				    'uri' => 'admin/sponsors',
				    'shortcuts' => array(
						array(
					 	   'name' => 'sponsors_add_label',
						    'uri' => 'admin/sponsors/create',
						    'class' => 'add'
						),
					),
				)
		    ),
		);
	}
	
	public function install()
	{
		$this->dbforge->drop_table('sponsors');
		$this->dbforge->drop_table('sponsors_records');

		$sponsors = "
			CREATE TABLE IF NOT EXISTS ".$this->db->dbprefix('sponsors')." (
			  `name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `sticky` tinyint(1) DEFAULT NULL,
			  `sponsor_year` INT NOT NULL DEFAULT  '2015',
			  `position` INT NOT NULL DEFAULT  '9999',
			  `image` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `location` text COLLATE utf8_unicode_ci DEFAULT NULL,
			  `target` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '_self',
			  `status` set('draft','live') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
			  `start_date` int(11) NOT NULL DEFAULT '0',
			  `end_date` int(11) NOT NULL DEFAULT '0',
			  `impressions` int(11) NOT NULL DEFAULT '0',
			  `clicks` int(11) NOT NULL DEFAULT '0',
			  `link_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `created_on` int(11) NOT NULL,
			  `updated_on` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";

		$sponsors_record = "
			CREATE TABLE IF NOT EXISTS ".$this->db->dbprefix('sponsors_records')." (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `ad_id` int(11) NOT NULL,
			  `click_date` int(11) NOT NULL,
			  `countryip` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unknown',
			  `countryid` int(11) NOT NULL DEFAULT '0',
			  `countrycode` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unknown',
			  `countryname` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unknown',
			  `total_clicks` int(11) NOT NULL,
			  `platform` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `browser` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `browser_version` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `referer` VARCHAR( 255 ) NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
		";

		$sponsors_sql = $this->db->query($sponsors);
		$sponsors_record_sql = $this->db->query($sponsors_record);
		
		if($sponsors_sql && $sponsors_record_sql)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function uninstall()
	{
		if ($this->dbforge->drop_table('sponsors') &&  $this->dbforge->drop_table('sponsors_records'))
		{
			return TRUE;
		}
		return FALSE;
	}

	public function upgrade($old_version)
	{
		return TRUE;
	}
	
	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "Sponsor management is pretty straight forward.";
	}
}
/* End of file details.php */
