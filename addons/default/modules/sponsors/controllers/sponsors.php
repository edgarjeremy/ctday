<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Frontend controller for Partners module.
 *
 *
 * @package		Partners
 * @subpackage  Frontend
 * @category  	Module
 */
class Sponsors extends Public_Controller
{

	function __construct()
	{
		parent::__construct();		

		$this->load->model('sponsors_m');
		$this->load->helper('geoip');

	}

	function index($id=0, $redir='')
	{
		$this->redirect($id,$redir);
	}

/**
 * Redirect visitor on clicking an advertisement
 */
	function redirect($id=0, $redir='')
	{

		$sponsor = $this->sponsors_m->get($id);

		if ($id && $sponsor)
		{
			$redirect = $this->_make_clickable(urldecode($sponsor->link_url));
			$dir = './addons/default/modules/'.$this->module_details['slug'].'/helpers/';

			$this->load->helper('geoip');

			// open the geoip database
			$gi = geoip_open($dir."GeoIP.dat",GEOIP_STANDARD);

			// to get country code
			$country_ip		= $_SERVER['REMOTE_ADDR'];
			$country_code	= geoip_country_code_by_addr($gi, $country_ip);
			$country_id		= geoip_country_id_by_addr($gi, $country_ip);
			$country_name	= geoip_country_name_by_addr($gi, $country_ip);
			$platform		= $this->agent->platform();
			$browser		= $this->agent->browser();
			$browser_version= $this->agent->version();
			$referer		= !empty($_SESSION['referer']) ? $_SESSION['referer'] : !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

			// close the database
			geoip_close($gi);

			// first, check if sponsors id=$id actually redirects to $redirect!
			$sponsors = $this->sponsors_m->get($id);

			if (!empty($sponsors))
			{
				$link_url = urldecode($sponsors->link_url);
				$url = $this->_make_clickable($link_url);
// DEBUG
/*
echo "<pre>";
echo "\$sponsors->link_url: " . $sponsors->link_url . "\n";
echo "\$link_url: $link_url\n";
echo "\$url: $url\n";
echo "\$this->_make_clickable(\$link_url): " . $this->_make_clickable($link_url) . "\n";
echo "urldecode(\$sponsors->link_url): " . urldecode($sponsors->link_url) . "\n";
echo "urldecode: " . urldecode($this->_make_clickable($sponsors->link_url)) . "<br />";
echo "redirect: $redirect\n";
echo "<pre>";
*/
//exit;

//				if ($url==$redirect)
//				{
					$sql = 'UPDATE '.$this->db->dbprefix('sponsors').' SET clicks=clicks+1 WHERE id=\''.$id.'\'';
					$this->db->query($sql);

					// insert it
					$this->db->insert('sponsors_records', array(
						'ad_id'				=> $id,
						'click_date'		=> now(),
						'countryip'			=> $country_ip,
						'countryid'			=> (int)$country_id,
						'countrycode'		=> (int)$country_code,
						'countryname'		=> ($country_name ? $country_name : 'Unknown'),
						'total_clicks'		=> 1,
						'platform'			=> ($platform ? $platform : 'Unknown'),
						'browser'			=> ($browser ? $browser : 'Unknown'),
						'browser_version'	=> ($browser_version ? $browser_version : 'Unknown'),
						'referer'			=> ($referer ? $referer : NULL)
					));

					header("location: ".$redirect);
/*
				}
				else
				{
					// hmm... someone try to inject something..
					echo '<script>history.go(-1);</script>';
					//echo "<pre>\n";
					//print_r($sponsors);
					//echo "</pre>\n";
					
				}
*/
			}
		}
		else
		{
			//echo "redirect: $redirect; d: $id<br />\n";
			//echo "error!";
			echo '<script>history.go(-1);</script>';
		}
	}

	public function _make_clickable($text)
	{
/*
echo "<br /><b>_make_clickable</b><br />";
echo "\$text: $text<br />";
echo "substr(\$text, 0, 7): " . substr($text, 0, 7) . "<br />";
echo "<br />";
*/
		if (substr($text, 0, 7) <> 'http://')
			return 'http://'.$text;
		return $text;
	}

}
?>