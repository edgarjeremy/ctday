<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Admin controller for Sponsors module.
 *
 * @author 		Okky Sari
 *
 * @package  	Sponsors
 * @subpackage  Admin
 * @category  	Module
 */
class Admin extends Admin_Controller
{
	/**
	 * The current active section
	 * @access protected
	 * @var string
	 */
	protected $section = 'sponsors';

	/**
	* Validation rules to be used for create and edit
	*/
	public $validation_rules = array();

	/**
	 * Image upload library configs
	 */
	public $upload_cfg = array();

	/**
	* Sponsors image upload directory
	*/
	public $sponsors_image_dir;

	/**
	 * Maximum width of image before being resized. Read from sponsors location
	 */
	public $maxwidth = 150;

	/**
	 * Maximum height of image before being resized. Read from sponsors location
	 */
	public $maxheight = 80;

	/**
	 * Initial thumbnail image width.
	 */
	public $thumbwidth = 150;

	/**
	 * Initial thumbnail image height
	 */
	public $thumbheight = 100;

	/**
	 * Width and height of ad location
	 */
	public $loc = array();

	/** 
	 * The constructor
	 * @access public
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();

		/**
		 * Load needed models
		 *
		 * Models: sponsors_m
		*/
		$this->load->model('sponsors_m');

		$this->module_details['slug'] = 'sponsors';
		$this->load->helper(array('date', 'filename'));
		$this->lang->load('sponsors');

		$data = new stdClass();

		$this->validation_rules = array(
			array(
				'field'				=> 'name',
				'label'				=> 'lang:sponsors_name_label',
				'rules'	 			=> 'trim|required'
			),
			array(
				'field' 			=> 'status',
				'label'				=> 'lang:sponsors_status_label',
				'rules' 			=> 'trim|required'
			),
			array(
				'field' 			=> 'image',
				'label'				=> 'lang:sponsors_upload_image_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'sticky',
				'label'				=> 'lang:sponsors_sticky_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'target',
				'label'				=> 'lang:sponsors_target_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'impressions',
				'label'				=> 'lang:sponsors_impressions_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'link_url',
				'label'				=> 'lang:sponsors_link_url_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'clicks',
				'label'				=> 'lang:sponsors_clicks_label',
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'start_date_str',
				'label'				=> lang('sponsors_start_date_label'),
				'rules' 			=> 'trim'
			),
			array(
				'field' 			=> 'date_from_hour',
				'label'				=> lang('sponsors_date_label') . ' ' . lang('sponsors_from_label') . ' ' .lang('sponsors_hour_label'),
				'rules' 			=> 'trim|numeric'
			),
			array(
				'field' 			=> 'date_from_minute',
				'label'				=> lang('sponsors_date_label') . ' ' . lang('sponsors_from_label') . ' ' .lang('sponsors_minute_label'),
				'rules' 			=> 'trim|numeric'
			)
		);

		// load file config
		//$this->config->load('files/files');

		// image upload configs - by OS
		$this->sponsors_image_dir				= UPLOAD_PATH.'sponsors';
		$this->upload_cfg['upload_path']		= UPLOAD_PATH.'sponsors';
		$this->upload_cfg['allowed_types'] 		= 'jpg|gif|png|jpeg';
		$this->upload_cfg['max_size'] 			= '12000';
		$this->upload_cfg['remove_spaces'] 		= TRUE;
		$this->upload_cfg['overwrite']     		= FALSE;
		$this->upload_cfg['encrypt_name']		= TRUE;

		// check and create dir accordingly
		$this->sponsors_m->check_dir($this->sponsors_image_dir);

		$data->hours = array_combine($hours = range(0, 23), $hours);
		$data->minutes = array_combine($minutes = range(0, 59), $minutes);

		// ad types
		$data->ad_types = array(''=>'', 'html'=>'HTML', 'image'=>'Image');

		$data->locations = array();

		$vars['maxwidth'] = $this->maxwidth;
		$vars['maxheight'] = $this->maxheight;
		$vars['thumbwidth'] = $this->thumbwidth;
		$vars['thumbheight'] = $this->thumbheight;

		$this->load->vars($vars);

		$this->template
			->set('sponsors_image_dir', $this->sponsors_image_dir)
			->append_js('module::functions.js')
			->set_partial('shortcuts', 'admin/partials/shortcuts');
	}
	
	/**
	 * Index method, lists all advertisement with pagination
	 * @access public
	 * @return void
	 */
	function index()
	{
		// Create pagination links
		$total_rows = $this->sponsors_m->count_by(array('show_future'=>TRUE, 'status' => 'all'));
		$pagination = create_pagination('admin/sponsors/index', $total_rows);
		
		// Using this data, get the relevant results
		$sponsors = $this->sponsors_m->limit($pagination['limit'],$pagination['offset'])->get_many_by(array(
			'show_future'=>TRUE,
			'status' => 'all'
		));

		$this->template
			->set('pagination', $pagination)
			->set('sponsors', $sponsors)
			->build('admin/index');
	}
	
	/**
	 * Create method, create new advertisement
	 * @access public
	 * @return void
	 */
	function create()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validation_rules);

		$sponsor = new stdClass();

		if ($this->input->post('start_date_str'))
		{
			$df = explode('-', $this->input->post('start_date_str'));
			$date_from_day = $df[2];
			$date_from_month = $df[1];
			$date_from_year = $df[0];
			$start_date = mktime($this->input->post('date_from_hour'), $this->input->post('date_from_minute'), 0, $date_from_month, $date_from_day, $date_from_year);
			unset($df);
		} else {
			$start_date = now();
		}

		if ($this->form_validation->run())
		{
			// They are trying to put this live
			if ($this->input->post('status') == 'live')
			{
				role_or_die('sponsors', 'put_live');
			}

			$config = $this->upload_cfg;
			$this->load->library('upload');
			$this->upload->initialize($config);
			$image = array();
			if(isset($_FILES['adimage']['tmp_name']) && $_FILES['adimage']['name'])
			{
				if ($this->upload->do_upload('adimage')) {
					$image = $this->upload->data();
					$this->_image_processing($image['file_name']);
					$filename = $image['file_name'];
				}
				else
				{
					$this->output->set_flashdata('error', $this->upload->display_errors());
					redirect('/admin/sponsors/create');
				}
			}

			$input = array(
	            'name'				=> $this->input->post('name'),
	            'location'			=> $this->input->post('location'),
	            'status'			=> $this->input->post('status'),
	            'sticky'			=> (int)$this->input->post('sticky'),
				'start_date'		=> $start_date,
				'target'			=> $this->input->post('target'),
				'link_url'			=> urlencode($this->input->post('link_url')),
	    	);

			if (!empty($filename))
				$input['image'] = $filename;

			$this->sponsors_m->maxwidth = $this->maxwidth;
			$this->sponsors_m->maxheight = $this->maxheight;
			$this->sponsors_m->thumbwidth = $this->thumbwidth;
			$this->sponsors_m->thumbheight = $this->thumbheight;

			$this->sponsors_m->upload_cfg =& $this->upload_cfg;

			$id = $this->sponsors_m->insert($input);
    	
			if (!empty($id))
			{
				$this->pyrocache->delete_all('sponsors_m');
				$this->session->set_flashdata('success', lang('sponsors_add_success'));

			}
			else
			{
				$this->session->set_flashdata('error', $this->lang->line('sponsors_article_add_error'));
			}			

			// Redirect back to the form or main page
			$this->input->post('btnAction') == 'save_exit' ? redirect('admin/sponsors') : redirect('admin/sponsors/edit/'.$id);
		}
		else
		{
			// Go through all the known fields and get the post values
			foreach($this->validation_rules as $key => $field)
			{
				$sponsor->$field['field'] = set_value($field['field']);

				if ($field['field']=="location")
				{
					$data->sponsors_locations = set_value($field['field']); //explode(',',trim(set_value($field['field']),','));
				}

			}
		}
		
		$sponsor->start_date = $start_date;

		$this->template
			->title($this->module_details['name'], lang('sponsors_add_title'))
			->append_css('module::sponsors.css' )
			->append_js('module::jquery.form.js' )
			//->append_js('module::sponsors_form.js' )
			->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			->set('sponsor', $sponsor)
			->build('admin/form');
	}
	
	/**
	 * Edit method, edits an existing advertisement
	 * @access public
	 * @param int id The ID of the advertisement to edit 
	 * @return void
	 */
	function edit($id = 0)
	{
		if (!$id)
		{
			redirect('admin/sponsors');
		}

	    $this->id	 	= $id;
	    $sponsor		= $this->sponsors_m->get($id);

		if (empty($sponsor)) {
			$this->session->set_flashdata('error', $this->lang->line('sponsors_retrieve_error'));
			redirect('admin/sponsors');
		}

		if ($this->input->post('start_date_str'))
		{
			$df = explode('-', $this->input->post('start_date_str'));
			$date_from_day = $df[2];
			$date_from_month = $df[1];
			$date_from_year = $df[0];
			$start_date = mktime(0, 0, 0, $date_from_month, $date_from_day, $date_from_year);
			unset($df);
		} else {
			$start_date = now();
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->validation_rules);

		if ($this->form_validation->run())
		{
			// They are trying to put this live
			if ($sponsor->status != 'live' and $this->input->post('status') == 'live')
			{
				role_or_die('sponsors', 'put_live');
			}

			$this->load->library('upload', $this->upload_cfg);
			$image = array();
			if(isset($_FILES['adimage']['tmp_name']) && $_FILES['adimage']['name'])
			{
				if ($this->upload->do_upload('adimage')) {
					$image = $this->upload->data();
					$this->_image_processing($image['file_name']);
					$filename = $image['file_name'];
				}
			}

			$input = array(
	            'name'				=> $this->input->post('name'),
	            'location'			=> $this->input->post('location'),
	            'status'			=> $this->input->post('status'),
	            'sticky'			=> (int)$this->input->post('sticky'),
				'start_date'		=> $start_date,
				'target'			=> $this->input->post('target'),
				'link_url'			=> urlencode($this->input->post('link_url'))
	    	);

			if (!empty($filename))
				$input['image'] = $filename;

			$this->sponsors_m->maxwidth = $this->maxwidth;
			$this->sponsors_m->maxheight = $this->maxheight;
			$this->sponsors_m->thumbwidth = $this->thumbwidth;
			$this->sponsors_m->thumbheight = $this->thumbheight;

			$this->sponsors_m->upload_cfg =& $this->upload_cfg;

			$result = $this->sponsors_m->update($id, $input);

			if (!empty($result))
			{
				$this->pyrocache->delete_all('sponsors_m');
				$this->session->set_flashdata('success', lang('sponsors_edit_success'));

				// check & delete old uploaded files
				if (!empty($image) && $image['file_name'] <> $this->input->post('oldcontent') && $this->input->post('oldcontent'))
				{
					if (file_exists($this->upload_cfg['upload_path'].'/'.$this->input->post('oldcontent')))
						$fimg = unlink($this->upload_cfg['upload_path'].'/'.$this->input->post('oldcontent'));
					else
						$fimg = true; // somehow the image does not exist!
					
					if (file_exists($this->upload_cfg['upload_path'].'/'. substr($this->input->post('oldcontent'), 0, -4) . '_thumb' . substr($this->input->post('oldcontent'), -4)))
						$fimgthumb = unlink($this->upload_cfg['upload_path'].'/'. substr($this->input->post('oldcontent'), 0, -4) . '_thumb' . substr($this->input->post('oldcontent'), -4) );
					else
						$fimgthumb = true; // somehow the image thumb is missing!

					if (!$fimg && !$fimgthumb)
					{
						$this->session->set_flashdata('notice', sprintf(lang('sponsors_image_change_notice'), $this->input->post('oldcontent'), $this->sponsors_image_dir ) );
					}
				}

			}
			
			else
			{
				$this->session->set_flashdata('error', $this->lang->line('sponsors_article_edit_error'));
			}			

			// Redirect back to the form or main page
			$this->input->post('btnAction') == 'save_exit' ? redirect('admin/sponsors') : redirect('admin/sponsors/edit/'.$id);

		}

			// Go through all the known fields and get the post values
		foreach($this->validation_rules as $key => $field)
		{
			if (isset($_POST[$field['field']]))
			{
				$sponsor->$field['field'] = set_value($field['field']);
			}
		}

		$this->template
			->title($this->module_details['name'], lang('sponsors_edit_label'))
			->append_css('module::sponsors.css' )
			->append_js('module::jquery.form.js' )
			->append_js('module::sponsors_form.js' )
			->append_metadata( $this->load->view('fragments/wysiwyg', array(), TRUE) )
			->set('sponsor', $sponsor)
			->build('admin/form');
	}	
	
	/**
	 * Show a preview
	 *
	 * @access public
	 * @param int $id The ID of the sponsors to preview
	 * @return void
	 */
	public function preview($id)
	{
		$data['content'] = $this->sponsors_m->get($id);
		$this->template->set_layout('modal', 'admin')
				->build('admin/preview', $data);
	}

	/**
	 * Reorder the advertisement
	 *
	 * @access public
	 * @param mix $id The IDs of the sponsors to reorder
	 * @return void
	 */
	public function reorder()
	{
		$ids = explode(',', $this->input->post('order'));

		$i = 1;
		foreach ($ids as $id)
		{
			$this->sponsors_m->update($id, array(
				'position' => $i
			));
			++$i;
		}
	}

	/**
	 * Action based on the clicked button
	 *
	 * @access public
	 * @return void
	 */
	function action()
	{
		switch($this->input->post('btnAction'))
		{
			case 'publish':
				$this->publish();
			break;
			case 'delete':
				$this->delete();
			break;
			default:
				redirect('admin/sponsors');
			break;
		}
	}

	/**
	 * Action based on the clicked button
	 *
	 * @access public
	 * @param mix array of $id The IDs of the sponsors to publish
	 * @return void
	 */
	function publish($id = 0)
	{
		// Publish one
		$ids = ($id) ? array($id) : $this->input->post('action_to');
		
		if(!empty($ids))
		{
			// Go through the array of slugs to publish
			$article_titles = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if($article = $this->sponsors_m->get($id) )
				{
					$this->sponsors_m->publish($id);
					
					// Wipe pyrocache for this model, the content has changed
					$this->pyrocache->delete('sponsors_m');				
					$article_titles[] = $article->title;
				}
			}
		}
	
		// Some articles have been published
		if(!empty($article_titles))
		{
			// Only publishing one article
			if( count($article_titles) == 1 )
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('sponsors_publish_success'), $article_titles[0]));
			}			
			// Publishing multiple articles
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('sponsors_mass_publish_success'), implode('", "', $article_titles)));
			}
		}		
		// For some reason, none of them were published
		else
		{
			$this->session->set_flashdata('notice', $this->lang->line('sponsors_publish_error'));
		}
		
		redirect('admin/sponsors');
	}

	
	/**
	 * Delete method, deletes an existing advertisement
	 * @access public
	 * @param int id The ID of the ad to delete
	 * @return void
	 */
	function delete($aid = 0)
	{
		// Delete one
		$array_id[] = $aid;
		$ids = ($aid) ? $array_id : $this->input->post('action_to');
		$todelete = count($ids);

		// Go through the array of slugs to delete
		if(!empty($ids))
		{
			$deleted = 0;

			foreach ($ids as $id)
			{
				// Get the current event so we can grab the id too
				$article = $this->sponsors_m->get($id);
				$this->db->flush_cache();
				if( !empty($article) )
				{
					// try to delete the images first
					if (file_exists($this->upload_cfg['upload_path'].'/'.$article->image))
						$fimg = unlink($this->upload_cfg['upload_path'].'/'.$article->image);
					else
						$fimg = true; // somehow the image does not exist!

					if (file_exists($this->upload_cfg['upload_path'].'/'. substr($article->image, 0, -4) . '_thumb' . substr($article->image, -4)))
						$fimgthumb = unlink($this->upload_cfg['upload_path'].'/'. substr($article->image, 0, -4) . '_thumb' . substr($article->image, -4) );
					else
						$fimgthumb = true; // somehow the image thumb is missing!

					if (!$fimg && !$fimgthumb)
					{
						$this->session->set_flashdata('notice', sprintf(lang('sponsors_image_change_notice'), $article->image, $this->sponsors_image_dir ) );
					}

					if ($fimg && $fimgthumb)
					{
						$deleted++;
						$this->sponsors_m->delete($id);
					}
					
				}
			}
		}

		// Some data have been deleted
		if ($deleted == 1)
		{
			$this->session->set_flashdata('success', lang('sponsors_delete_success'));
		}
		elseif ($deleted > 1)
		{
			$this->session->set_flashdata('success', sprintf($this->lang->line('sponsors_mass_delete_success'), $deleted, $todelete));
		}
		// For some reason, none of them were deleted
		elseif ($deleted===0)
		{
			$this->session->set_flashdata('notice', lang('sponsors_delete_error'));
		}

		// Wipe pyrocache for this model, the content has changed
		//$this->pyrocache->delete('sponsors_m');				

		redirect('admin/sponsors');
	}

	/**
	 * Clear statistics of an ad
	 * @access public
	 * @param int id The ID of the ad
	 * @return void
	 */
	function clearstats($id)
	{
		if (!empty($_SERVER['HTTP_REFERER']))
			$redirect_to = $_SERVER['HTTP_REFERER'];
		else
			$redirect_to = 'admin/sponsors';

		$res = $this->sponsors_m->clear_stats($id);
		if ($res)
			$this->session->set_flashdata('success', lang('sponsors_clear_stats_success'));
		elseif ($res === 0)
			$this->session->set_flashdata('notice', lang('sponsors_clear_stats_empty'));
		else
			$this->session->set_flashdata('error', lang('sponsors_clear_stats_error'));
		redirect($redirect_to);
	}

	/**
	 * View statistics of an ad
	 * @access public
	 * @param int id The ID of the ad
	 * @return void
	 */
	function stats($id=null)
	{
		if (!$id)
		{
			echo '<div class="blank-slate">';
			echo '<h2>' . lang('sponsors_not_exist_error') . '</h2>';
			echo '</div>';
			return false;
		}

		// get list of countries from sponsors_records
		$countries = array();
		$co = $this->sponsors_m->get_grouped_stats(array('group'=>'countryname'));
		if (!empty($co))
		{
			foreach($co as $c)
			{
				$countries[$c->countryname] = ucfirst($c->countryname);
			}
		}

		$platforms = array();
		$pl = $this->sponsors_m->get_grouped_stats(array('group'=>'platform'));
		if (!empty($pl))
		{
			foreach($pl as $p)
			{
				$platforms[$p->platform] = $p->platform;
			}
		}

		$browsers  = array();
		$br = $this->sponsors_m->get_grouped_stats(array('group'=>'browser'));
		if (!empty($br))
		{
			foreach($br as $b)
			{
				$browsers[$b->browser] = $b->browser;
			}
		}

		$this->template->set('countries', $countries)->set('platforms', $platforms)->set('browsers', $browsers);

		//base where clause
		$base_where = array('id' => $id);


		//determine country param
		$base_where = $this->input->post('f_country') ? $base_where + array('countryname' => $this->input->post('f_country')) : $base_where;

		//platform param
		$base_where = $this->input->post('f_platform') ? $base_where + array('platform' => $this->input->post('f_platform')) : $base_where;

		//platform param
		$base_where = $this->input->post('f_browser') ? $base_where + array('browser' => $this->input->post('f_browser')) : $base_where;

		//start date param
		$base_where = $this->input->post('f_start_date') ? $base_where + array('start_date' => $this->input->post('f_start_date')) : $base_where;

		//end date param
		$base_where = $this->input->post('f_end_date') ? $base_where + array('end_date' => $this->input->post('f_end_date')) : $base_where;

		//start date param
		$base_where = $this->input->post('f_starttime') ? $base_where + array('start_time' => $this->input->post('f_starttime')) : $base_where;

		//end date param
		$base_where = $this->input->post('f_endtime') ? $base_where + array('end_time' => $this->input->post('f_endtime')) : $base_where;

		// Create pagination links
		$pagination = create_pagination('admin/sponsors/stats', $this->sponsors_m->count_record_by($base_where));

		$partner = $this->sponsors_m->get($id);

		// Create pagination links
		$total_rows = $this->sponsors_m->count_record_by(array('ad_id'=>$id));

		$data->pagination = create_pagination('admin/sponsors/stats', $total_rows, NULL, 5);

		// Using this data, get the relevant results
		$data->records = $this->sponsors_m->limit($data->pagination['limit'])->get_stats($base_where);

		$data->sponsor =& $sponsor;

		$this->input->is_ajax_request() ? $this->template->set_layout(FALSE) : '';
		$this->template
			->title($this->module_details['name'], lang('sponsors_stats_title'))
			->set_partial('filters', 'admin/partials/filters')
			->append_css('module::sponsors.css')
			->append_js('module::filter.js')
			->set('pagination', $pagination)
			->set('partner', $partner)
			->build('admin/records', $data);
	}

	/**
	 * Check slug/title of an ad
	 * @access public
	 * @param string slug to check
	 * @return void
	 */
	function _check_slug($slug = '')
	{
		if(!$this->sponsors_m->check_slug($slug, (isset($this->id) ? $this->id : 0)))
		{
			$this->form_validation->set_message('_check_slug', lang('sponsors_already_exist_error'));
			return FALSE;
		}
		
		return TRUE;
	}

	function _image_processing($image)
	{
		$config['source_image']	= UPLOAD_PATH . 'sponsors/'.$image;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']	 	= $this->maxwidth;
		$config['height']		= $this->maxheight;
		$config['master_dim']	= 'height';
//		$config['new_image']	= UPLOAD_PATH . 'sponsors';

		$this->load->library('image_lib');
		$this->image_lib->initialize($config);
		return $this->image_lib->resize();
	}

}
?>