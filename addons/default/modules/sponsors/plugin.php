<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Sponsors plugin
 *
 * Usage:
 *   {{ sponsors:display }}
 *
 * Options:
 *    - limit="<limit>" 	Limit the number of sponsors to retrieve (default to 3)
 *    - sticky="[0|NULL]|yes" 		Retrieve sticky sponsors only (default to true)
 *
 * @package		Sponsors
 * @subpackage  Plugin
 * @category  	Module
 */
class Plugin_Sponsors extends Plugin
{

	/**
	 * Display sponsors
	 * @access public
	 * @param int $limit Limit number of advertisement to display
	 * @param string $sticky Whether to retrieve sticky advertisement only
	 * @param string $location Retrieves advertisement in this location.
	 * @return array
	 */
	function display()
	{
		$limit = $this->attribute('limit');
		$sticky = $this->attribute('sticky', 'no');
		$location = $this->attribute('location');
		$this->load->model('sponsors/sponsors_m');
		$this->load->helper('filename');

		$result = array();
		$ret = $this->sponsors_m->get_random($limit, NULL, $sticky);

		if (!empty($ret))
		{
			foreach($ret as $r)
			{
				$r->fullimage	= base_url() . UPLOAD_PATH.'sponsors/'.$r->image;
				$r->image		= base_url() . UPLOAD_PATH.'sponsors/'.thumbnail($r->image);

				if ($r->link_url)
				{
					//$r->link_url = base_url() . 'sponsors/redirect?d='.$r->id.'&to='.urlencode($this->_make_clickable(urldecode($r->link_url)));
					$r->link_url = site_url('sponsors/redirect/'.$r->id.'/');
					$r->url = '<a target="'.$r->target.'" href="'.$r->link_url.'"><img src="'.$r->image.'" /></a>';
				}
				else
				{
					$r->url = '<img src="'.$r->image.'" />';
					$r->link_url = '';
				}

				// update number of impressions
				$impression = (int)$r->impressions + 1;
				$this->sponsors_m->update_stats($r->id, array('impressions' => $impression));
			}
		}
		return $ret;

	}

	public function _make_clickable($text)
	{
		if (substr($text, 0, 7) <> 'http://')
			return 'http://'.$text;
			
		return $text;
	}

}

/* End of file plugin.php */