<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Galeri categories model
 *
 * @package  	Galeri
 * @subpackage	Models
 * @category  	Module
 */
class Gallery_categories_m extends MY_Model
{
	/**
	 * Insert a new category into the database
	 * @access public
	 * @param array $input The data to insert
	 * @return string
	 */
	public function insert($input = array(), $skip_validation = false)
	{
		$this->load->helper('text');
		parent::insert(array(
			'title'=>$input['title'],
			'slug'=>$input['slug'],
			'status'=>$input['status']
		));
		
		return $input['title'];
	}
    
	/**
	 * Update an existing category
	 * @access public
	 * @param int $id The ID of the category
	 * @param array $input The data to update
	 * @return bool
	 */
	public function update($id, $input, $skip_validation = false)
	{
		return parent::update($id, array(
			'title'=>$input['title'],
			'slug'=>$input['slug'],
			'status'=>$input['status']
		));
	}

	/**
	 * Callback method for validating the title
	 * @access public
	 * @param string $title The title to validate
	 * @return mixed
	 */
	public function check_title($title = '', $id=NULL)
	{
		if ($id)
		{
			return parent::count_by(array('slug'=>url_title($title), 'id'=>'!='.$id)) > 0;
		}
		else
		{
			return parent::count_by('slug', url_title($title)) > 0;
		}
	}
	
	/**
	 * Insert a new category into the database via ajax
	 * @access public
	 * @param array $input The data to insert
	 * @return int
	 */
	public function insert_ajax($input = array())
	{
		$this->load->helper('text');
		return parent::insert(array(
			'title'=>$input['title'],
			'slug'=>$input['slug'],
			'status'=>'live'
		));
	}
}