<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Galeri Plugin
 *
 * Create lists of galleries
 * 
 * @author		Okky Sari
 * @package		Addons\Modules\Galeri\Plugins
 */
class Plugin_Galleries extends Plugin
{

	/**
	 * Galeri
	 *
	 * Creates a list of galleries
	 *
	 * Usage:
	 * {{ galeri:galeri order-by="title" limit="4" }}
	 *		<a href="{{ url }}" class="{{ slug }}">{{ thumbnail }}<br />{{ title }}</a>
	 * {{ /galeri:galeri }}
	 *
	 * @param	array
	 * @return	array
	 */
	public function galleries()
	{
		$this->load->helper('filename');
		$this->load->model('galleries/galleries_m');
		$limit		= $this->attribute('limit', 6);
		$type		= $this->attribute('type');
		$category	= $this->attribute('category');

		if ($type)
		{
			if ($type == 'yt')
			{
				$this->db->where('extension', $type);
			}
			elseif ($type == 'image')
			{
				$this->db->where('extension', '.jpg');
				$this->db->or_where('extension', '.gif');
				$this->db->or_where('extension', '.png');
			}
		}

		if ($category)
		{
			$categories = explode('|', $category);
			$category = array_shift($categories);

			$this->db->where('gallery_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);

			foreach($categories as $category)
			{
				$this->db->or_where('gallery_categories.' . (is_numeric($category) ? 'id' : 'slug'), $category);
			}
		}

		$this->db->order_by('position,id, ', 'desc');

		$galleries = $this->galleries_m->limit($limit)->get_all();

		foreach($galleries as $g)
		{
			$thumb = $this->galleries_m->get_galeria_thumbnail($g->id);

			if ( $thumb->extension == 'yt' )
			{
					if (substr($thumb->thumbnail, 0, 4) == 'http')
					{
						$g->thumbnail = '<img src="'.($thumb->thumbnail).'" alt="" />';
					}
					elseif ( in_array(substr(strrchr($thumb->thumbnail, "."), 1), $this->format_images ) )
					{
						$thumbnail = '/'.UPLOAD_PATH . 'galleries/' .$g->id.'/' . substr($thumb->thumbnail, 0, -4) . '_thumb' . substr($thumb->thumbnail, -4) ;
						$g->thumbnail = '<img src="'.$thumbnail.'" alt="" />';
					}
					else
					{
						$g->thumbnail = image('icon-video_thumb.jpg', 'galeri');
					}
			}
			else
			{
				$thumbnail = '/'.UPLOAD_PATH . 'galleries/' .$g->id.'/' . substr($thumb->media, 0, -4) . '_thumb' . substr($thumb->media, -4) ;
				$g->thumbnail = '<img src="'.$thumbnail.'" alt="" />';
			}
			$g->url = site_url('galleries/view/'.$g->slug);
		}

		return $galleries;
	}

	public function images()
	{
		$this->load->model('galleries/gallery_media_m');
		$this->load->helper('filename');

		$limit		= $this->attribute('limit');
		$type		= $this->attribute('type', 'image');
		$slug		= $this->attribute('slug');
		$category	= $this->attribute('category');
		$order_by 	= $this->attribute('order-by', 'position,id');
		$order_dir	= $this->attribute('order-dir', 'asc');

		if ($type)
		{
			if ($type == 'vid')
			{
				//$this->db->where("(extension='yt' OR extension='vim')");
				$this->db->where("(extension='yt' OR extension='vim')");
			}
			elseif ($type == 'image')
			{
				$this->db->where("( LOWER(extension) = '.jpg' OR LOWER(extension) = '.jpeg' OR LOWER(extension) = '.gif' OR LOWER(extension) = '.png') ");
/*
				$this->db->where('extension', '.jpg');
				$this->db->or_where('extension', '.gif');
				$this->db->or_where('extension', '.png');
*/
			}
		}

		if ($slug)
		{
			$this->db->select('gallery_media.*, galleries.slug');
			$this->db->join('galleries', 'galleries.id = gallery_media.gallery_id', 'left');
			$this->db->where('galleries.slug', $slug);
		}

		if ($limit)
			$ret = $this->db
				->where('gallery_media.status', 'live')
				->order_by($order_by, $order_dir)
				->limit($limit)
				->get('gallery_media')
				->result();
		else
			$ret = $this->db
				->where('gallery_media.status', 'live')
				->order_by($order_by, $order_dir)
				->get('gallery_media')
				->result();

		foreach($ret as $r)
		{
			if ($r->description)
				$r->description = htmlspecialchars_decode($r->description);

			if ($type=='image')
			{
				$r->thumbnail = '<img src="'.site_url(UPLOAD_PATH . 'galleries/'.$r->gallery_id.'/'.smallname($r->media)).'" />';
				$r->thumbnail_small = site_url(UPLOAD_PATH . 'galleries/'.$r->gallery_id.'/'.smallname($r->media));
				$r->image = site_url().UPLOAD_PATH . 'galleries/' .$r->gallery_id.'/' . urlencode($r->media);
			}
			elseif ($type=='yt')
			{
				$r->thumbnail = '<iframe src="http://www.youtube.com/embed/'.$r->media.'" frameborder="0" allowfullscreen width="330" height="215" wmode="opaque"></iframe>'; //'<img src="'.($r->thumbnail).'" />';
//				$r->thumbnail = '<img src="'.$r->thumbnail.'" width="330" />';
			}
/* */
			elseif ($type=='vid')
			{
				if ($r->extension == 'yt')
				{
					$r->media = trim(str_ireplace('http://youtu.be', '', $r->media), '/');

					$media = substr($r->media, 0, 5) == 'http:' ? $r->media : 'http://www.youtube.com/embed/'.$r->media;
					$r->thumbnail = '<iframe src="'.$media.'" frameborder="0" allowfullscreen width="330" height="215" wmode="opaque"></iframe>'; //'<img src="'.($r->thumbnail).'" />';
				}
				elseif ($r->extension == 'vim')
				{
					$media = substr(strrchr($r->media, "/"), 1);
					$r->thumbnail = '<iframe src="http://player.vimeo.com/video/'.$media.'" frameborder="0" allowfullscreen width="330" height="215"></iframe>'; //'<img src="'.($r->thumbnail).'" />';
	//				$r->thumbnail = '<img src="'.$r->thumbnail.'" width="330" />';
				}
			}
/* */
		}
		return $ret;
	}

	/**
	 * Categories
	 *
	 * Creates a list of news categories
	 *
	 * Usage:
	 * {{ galeri:categories order-by="title" limit="5" }}
	 *		<a href="{{ url }}" class="{{ slug }}">{{ title }}</a>
	 * {{ /galeri:categories }}
	 *
	 * @param	array
	 * @return	array
	 */
	public function categories()
	{
		$this->load->helper('filename');
		$limit		= $this->attribute('limit', 10);
		$order_by 	= $this->attribute('order-by', 'title');
		$order_dir	= $this->attribute('order-dir', 'ASC');

		$categories = $this->db
			->select('title, slug')
			->order_by($order_by, $order_dir)
			->limit($limit)
			->get('galeri_categories')
			->result();

		foreach ($categories as &$category)
		{
			$category->url = site_url('galleries/category/'.$category->slug);
		}
		
		return $categories;
	}

}

/*
if (!function_exists('smallname'))
{
	function smallname($filename)
	{
		//return strrchr($filename, ".");
		$ext = substr(strrchr($filename, "."), 1);
		return str_ireplace(substr(strrchr($filename, "."), 0), '_small.'.$ext, $filename);
	}
}
*/

/* End of file plugin.php */