<?php if (!empty($images)): ?>
	<?php foreach($images as $i): ?>
		<div class="col-md-3 col-sm-3">
			<a class="thumbnail colorbox" href="<?php echo site_url(UPLOAD_PATH.'galleries/'.$i->gallery_id.'/'.($i->media)); ?>">
				<img src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$i->gallery_id.'/'.smallname($i->media)); ?>" class="img-responsive" alt="<?php echo $i->title  ?>" />
				<p><?php echo $i->subtitle ? $i->subtitle : '&nbsp;'; ?></p>
			</a>
		</div>
	<?php endforeach; ?>
<?php endif; ?>
