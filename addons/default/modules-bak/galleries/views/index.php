<h2 id="page_title"><?php echo lang('galleries.galleries_label'); ?></h2>

<div class="galleries_container" id="gallery_index">

	<?php if ( ! empty($galleries)): foreach ($galleries as $gallery): if (empty($gallery->parent)): ?>
	<div class="gallery" style="margin-right: 20px; width: 120px; display:block;float:left;">

		<div class="gallery_heading">
			<?php if ( ! empty($gallery->thumbnail)): ?>
			<a href="<?php echo site_url('galleries/view/'.$gallery->slug); ?>">
				<?php echo $gallery->thumbnail ?>
			</a>
			<?php endif; ?>
			<div class="gallery_title"><?php echo anchor('galleries/view/' . $gallery->slug, $gallery->title); ?></div>
		</div>

	</div>
	<?php endif; endforeach; ?>

	<div class="pagination">
		<?php	echo $this->pagination->create_links(); ?>
	</div>

	<?php else: ?>
		
	<p><?php echo lang('galleries.no_galleries_error'); ?></p>
	
	<?php endif; ?>
</div>