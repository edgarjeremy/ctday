<?php
/**
 * Galeria view - Lightbox view
 *
 * @package  	Galeria
 * @subpackage	Frontend_Views
 * @category  	Module
 */
?>
<!--lightbox gallry-->
<div id="GcboxClose" onClick="jQuery.colorbox.close();"></div>
<div id="liteBox">
	<div id="litebox_gallery">

    	<figure id="litebox_preview" style="z-index: 1; margin-bottom: 0px;">
        
          	<div class="litebox_center" style="padding-left:15px; position: relative; overflow: hidden; width: 600px; height: 350px;">

          	<ul class="items" style="width:9000em; position: relative;">
            <?php $i=0; foreach($images as $img){ ?>
            	<li style="float: left; display: none; position: absolute;">
   					<div style="width: 600px; height: 100%; display:block; z-index: 99;overflow:hidden;">

   						<div style="height: 350px; vertical-align:bottom; display:block;overflow:hidden;padding-bottom:1px;">
						<?php if ($img->extension == 'yt'): ?>
	
	    					<?php if ($img->media): ?>
		    						<?php echo $img->media; ?>
	    					<?php endif; ?>
	
						<?php else: ?>
	
		        			<img align="absbottom" class="litebox_img" src="<?php echo site_url(UPLOAD_PATH) . '/galleries/' .$img->gallery_id.'/' . substr($img->media, 0, -4) . '_med' . substr($img->media, -4) ;?>" class="big_img" width="580" alt="<?php echo $img->title; ?>" />
	
	        			<?php endif; ?>
   						</div>
	
			            <div class="litebox_imgdetail"></div>
			            <figcaption>
							<p><?php echo strip_tags($img->description); ?></p>
			            </figcaption>
	
						<span class="imgext" style="display: none;"><?php echo $img->extension; ?></span>

   					</div>
				</li>
            <?php $i++; } ?>
            </div>
          	</ul>
        	<a class="litebox_arrowleft th_arrowleft prev"></a>
          	<a class="litebox_arrowright th_arrowright next"></a>
			<span id="playflag" style="display: none;">played</span>
        </figure>

		<div class="scrollable">
			<ul class="lightbox_thumnails">
		   	    <li class="lightbox_thObject th_arrowleft" id="thumbPrev"><a class="backward"></a></li>
            
	            <li class="lightbox_thObject thumbsline" id="thumbsline">
	            	<ul class="lightbox_thumbsStripe items">
	                	<?php foreach($images as $img): ?>
	                			<li class="lightbox_thumbItem">

	                				<?php if ( $img->extension == 'yt' ): ?>

										<?php if ($img->thumbnail): ?>
							
												<?php if (substr($img->thumbnail, 0, 4) <> 'http'): ?>
													<img title="<?php echo $img->thumbnail; ?>" src="<?php echo site_url(UPLOAD_PATH).'/galleries/'.$img->gallery_id.'/'. substr($img->thumbnail, 0, -4) . '_thumb' . substr($img->thumbnail, -4);?>" alt="<?php echo $img->thumbnail; ?>" />
												<?php else: ?>
													<img title="<?php echo $img->thumbnail; ?>" src="<?php echo $img->thumbnail;?>" alt="<?php echo $img->thumbnail; ?>" />
												<?php endif; ?>
							
										<?php else: ?>
							
											<?php echo image('icon-video_thumb.jpg', 'galeria', array('alt' => 'Video file - No Thumbnail')); ?>
							
										<?php endif; ?>

	                				<?php else: ?>
		                				<img src="<?php echo site_url(UPLOAD_PATH) . '/galleries/' .$img->gallery_id.'/' . substr($img->media, 0, -4) . '_thumb' . substr($img->media, -4) ;?>" alt="" />
		                			<?php endif; ?>

									<div class="caph1" style="display:none;">
						            	<p><?php echo strip_tags($img->description); ?></p>
									</div>
	                			</li>
						<?php endforeach; ?>
	                </ul>
	            </li>
	            <li class="lightbox_thObject th_arrowright" id="thumbNext"><a class="forward"></a></li>
			</ul>
		</div>        
        
    </div>
</div>
<!--lightbox gallry-->

<script>
(function($) {
	$(function(){

			$.tools.tabs.addEffect("betterfade", function(tabIndex, done) {
				var gp = this.getPanes();

				this.getPanes().fadeOut(900);
				gp.eq(tabIndex).fadeIn(900);

				done.call();
			});


			$('.thumbsline').scrollable({
				items: '.lightbox_thumbItem',
			})

/*
			$('.thumbsline').scrollable({
				circular: false,
				items: '.lightbox_thumbItem',
				keyboard: false,
				speed: 400,
				autoplay: false,
				easing: 'linear'
			})
*/

			apiScroll = $('.thumbsline').data('scrollable');

			$('ul.lightbox_thumbsStripe').tabs('.litebox_center ul.items li', {
				effect: "betterfade",
				fadeOutSpeed: "slow",
				rotate: false,
				circular: false,
				current: 'hover'
			})
			.slideshow({
				autoplay: false,
				autopause: true,
				clickable: false,
				next: '.forward',
				prev: '.backward',
/*
				next: '.th_arrowright',
				prev: '.th_arrowleft',
*/
				interval: '5000'
			});

			apiTabs = $('ul.lightbox_thumbsStripe').data("tabs");
//			apiSlide = $('ul.lightbox_thumbsStripe').data("slideshow");

			apiTabs.onClick(function(e, index){
				apiScroll.seekTo(index);
				var imgext = $('ul.items li').eq(index).find('.imgext').text();
				return false;
			});

			$('.th_arrowleft').click(function(){
				if ($(this).hasClass('disabled'))
					return false;
				apiTabs.prev();
			});

			$('.th_arrowright').click(function(){
				if ($(this).hasClass('disabled'))
					return false;
				apiTabs.next();
			});

/*
			$('.playpause_btn, .pauseplay_btn').click(function(){
	
				if ($(this).attr('class') == 'playpause_btn')
				{
					$(this).removeClass('playpause_btn');
					$(this).addClass('pauseplay_btn');
					$('#playflag').text('paused');
					apiSlide.stop();
				}
				else if ($(this).attr('class') == 'pauseplay_btn')
				{
					$(this).removeClass('pauseplay_btn');
					$(this).addClass('playpause_btn');
					$('#playflag').text('played');
					apiSlide.play();
				}
			});
	
			$('ul.items, #litebox_controls, #litebox_preview, .lightbox_thumnails, .lightbox_thObject').hover(
				function(){
					apiSlide.pause();
				},
				function(){
					if ($('#playflag').text()=="played")
					{
						apiSlide.play();
					}
				}
			);
*/

			apiTabs.click(0);
			$('.items').show();

//			apiSlide.play();

/*
			$('ul.items li span.imgext').livequery(function(){
				if ($(this).text() == 'yt')
				{
					$('#playflag').text('paused');
					apiSlide.stop();
					if ($('ul#litebox_controls li').eq(1).attr('class') == 'playpause_btn')
					{
						$('ul#litebox_controls li').removeClass('playpause_btn');
						$('ul#litebox_controls li').addClass('pauseplay_btn');
						$('#playflag').text('paused');
						apiSlide.stop();
					}
				}
			});
*/

	});
})(jQuery);


</script>
