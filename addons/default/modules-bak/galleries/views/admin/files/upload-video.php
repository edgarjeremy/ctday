<?php
/**
 * Gallery admin view - upload video form
 *
 * @package  	Gallery
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<section class="title">
	<h4><?php echo ( $this->method == 'edit' ? lang('galleries.video_edit_title') : lang('galleries.create_video_label') ); ?></h4>
</section>

<section class="item">

<div class="form_inputs">

<div id="frmResult"></div>

<?php echo form_open_multipart(uri_string(), array('class' => 'crud media_crud', 'id' => 'files_crud')); ?>

<fieldset>
	<ul>
		<input type="hidden" name="gallery_id" value="<?php echo $gallery_id; ?>" />

		<li class="even">
			<?php echo form_label(lang('galleries.youtube_link_label'), 'media'); ?>
			<div class="input">
			<?php if ($this->method == 'edit' && $media->extension <> 'yt'): ?>
				<?php echo form_input('media', $media->media, 'style="width: 35em; height: 3em;" disabled="disabled"'); ?><br />
			<?php else: ?>
				<?php echo form_input('media', $media->media, 'class="skip" id="media" style="width: 300px;"'); ?><br />
			<?php endif; ?>
			<textarea id="media_ori_id" name="media_ori" style="display: none;"><?php echo $media->media; ?></textarea>
			</div>
		</li>

		<li>
			<label for="nothing"><?php echo lang('galleries.thumbnail_label'); ?></label>
			<div class="input"><?php echo form_upload('userfile'); ?> | <a href="#" id="getYTthumb"><?php echo lang('galleries.get_youtube_thumbnail'); ?></a></div>
		</li>

		<li>
		<label><small><?php echo lang('galleries.default_thumbnail_label'); ?></small></label>
			<div id="checkingthumb" class="checkingthumb"><?php echo lang('galleries.checking_thumbnail');?></div>

			<div id="thumb" >

				<?php if (!empty($media->thumbnail)): ?>

					<?php if (substr($media->thumbnail, 0, 4) <> 'http'): ?>

						<img title="<?php echo $media->thumbnail; ?>" src="<?php echo '/'.UPLOAD_PATH.'galleries/'.$galeria_id.'/'. substr($media->thumbnail, 0, -4) . '_thumb' . substr($media->thumbnail, -4);?>" alt="<?php echo $media->thumbnail; ?>" width="115" />
						<br />
						<label class="normal"><input type="checkbox" name="imgremove" value="1"> <?php echo lang('galleries.remove_image_label');?></label>


					<?php else: ?>

						<img title="<?php echo $media->thumbnail; ?>" src="<?php echo $media->thumbnail;?>" alt="<?php echo $media->thumbnail; ?>" width="200" />
						<br /><label class="inline"><input type="checkbox" name="imgremove" value="1"> <?php echo lang('galleries.remove_image_label');?></label>


					<?php endif; ?>

				<?php else: ?>

					<?php echo Asset::img('module::icon-video_thumb.jpg', 'gallery', array('alt' => 'Video file - No Thumbnail')); ?><br />

				<?php endif; ?>

			</div>

			<div class="clear-both"></div>
			<label>&nbsp;</label>
			<div class="input">
				<a href="#" style="display: none;" id="resettodefault"><?php echo lang('galleries.remove_image_label'); ?></a>
				<input type="hidden" name="youtube_thumb" value="<?php echo $media->thumbnail; ?>" id="thumburl">
				<input type="hidden" name="video_id" value="<?php echo $media->media; ?>" id="video_id">
				<div class="clear-both"><br /></div>
<!-- 				<div class="float-left text-small1">[<?php echo lang('galleries.default_thumbnail_label'); ?>]</div> -->
			</div>

		</li>

		<li class="even">
			<?php echo form_label(lang('galleries.title_label'), 'title'); ?>
			<div class="input"><?php echo form_input('title', $media->title, 'id="vtitle"'); ?></div>
		</li>
		<li>
			<?php echo form_label(lang('galleries.subtitle_label'), 'subtitle'); ?>
			<div class="input"><?php echo form_input('subtitle', $media->subtitle); ?></div>
		</li>
		<li class="even">
			<?php echo form_label(lang('galleries.description_label'), 'description'); ?>
			<div class="clear"><br /></div>
			<?php echo form_textarea(array(
				'name'	=> 'description',
				'id'	=> 'vdescription',
				'value'	=> $media->description,
				'style' => 'width:60%',
				'rows'	=> '3',
				'cols'	=> '5'
			)); ?>
		</li>
		<li class="<?php echo alternator('', 'even'); ?>">
			<label for="status"><?php echo lang('galleries.status_label'); ?></label>
			<div class="input">
				<?php echo form_dropdown('status', array('live'=>lang('galleries.status_live_label'), 'draft'=>lang('galleries.status_draft_label')), $media->status); ?>
			</div>
		</li>
		<li>
			<?php echo form_label(lang('galleries.meta_title_label'), 'meta_title'); ?>
			<div class="input"><?php echo form_input('meta_title', $media->meta_title); ?></div>
		</li>
		<li class="even">
			<?php echo form_label(lang('galleries.meta_keywords_label'), 'meta_keywords'); ?>
			<div class="input"><?php echo form_input('meta_keywords', $media->meta_keywords); ?></div>
		</li>
		<li>
			<?php echo form_label(lang('galleries.meta_description_label'), 'meta_description'); ?>
			<div class="clear"><br /></div>
			<?php echo form_textarea(array(
				'name'	=> 'meta_description',
				'id'	=> 'meta_description',
				'value'	=> $media->meta_description,
				'style' => 'width:60%',
				'rows'	=> '3',
				'cols'	=> '5'
			)); ?>
		</li>
	</ul>

<?php $module_details['slug'] = 'galleries/manage/'.$media->gallery_id; ?>

	<div class="align-right buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
		<a href="<?php echo site_url() ?>admin/galleries/manage/<?php echo $gallery_id?>" class="btn gray cancel">Cancel</a>
	</div>
</fieldset>
<?php echo form_close(); ?>

</div>

</section>

<style>
.loading {
	display: block;
	position: absolute;
	width: 170px;
	text-align: center;
	height: 22px;
	vertical-align: middle;
	line-height: 22px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	border: 1px solid #fff;
	background: url('pbar-ani.gif') repeat-x 50% 50%;
}
.checkingthumb {
	display:none;
	position: absolute;
	line-height: 100px;
	vertical-align: middle;
	text-align: center;
	padding-left: 170px;
	width:150px;
	height: 100px;
	margin: auto;
}

</style>

<script>
(function ($) {
	$(function () {

		form = $("form.crud");

		$('#getYTthumb').click(function(e){
			e.preventDefault();

			if ($('#media').val() == '') {
				alert('<?php echo lang('galleries.media_field_empty_msg');?>');
				$('#media').focus();
				return false;
			}

			$('#thumb').fadeTo('slow', 0, function(){
				$('#checkingthumb').fadeIn('fast');
				var jqxhr = $.post(SITE_URL + "admin/galleries/vidthumb", {media: $('#media').val()}, function(obj) {
					if (obj)
					{
						yt = JSON && JSON.parse(obj) || $.parseJSON(obj);
						$('#thumb img').attr('src', yt.thumbnail);
						$('#thumb img').attr('width', '200');
						$('#thumburl').val(yt.thumbnail);
						$('#vtitle').val(yt.title);
						$('#vdescription').val(yt.description);
						$('#video_id').val(yt.video_id);
						$('#thumb img').load();
						$('#thumb label').remove();
						$('#checkingthumb').fadeOut('fast');
					}
				})
				.success(function() {
						// $('#checkingthumb').fadeOut('fast');//, function(){
	/*
							$('#resettodefault').fadeIn('fast');
							$('#thumb').fadeTo('fast', 1);
						});
	*/
							$('#resettodefault').fadeIn('fast');
							$('#thumb').fadeTo('fast', 1);
				})
				.error(function() {
					$('#checkingthumb').fadeOut(function(){
						//$('#checkingthumb').css('z-index', '-1');
						$('#thumb').fadeTo('slow', 1);
					});			
				 });
			});


		});

		$('#resettodefault').click(function(e){
			e.preventDefault();
			var srcdef = '<?php echo Asset::img('module::icon-video_thumb.jpg', 'Default thumbnail'); ?>';
			$('#thumburl').val('');
			$('#thumb img').fadeOut('slow', function(){
				$('#thumb').html(srcdef);
				$('#thumb img').load(function(){
					$('#thumb').fadeTo('fast', 1);
				});
				$('#resettodefault').fadeOut('fast');
			});
		});

		$('#media').on("blur", function(){
			if ($('#media').val())
				$('#getYTthumb').trigger('click');
		})

/*
		$('#files_crud').on("submit", function(e){
			e.preventDefault();
			var d = $('form#files_crud').serialize();

			var jqxhr = $.ajax({
				type: "POST",
				url: SITE_URL + "admin/galleries/vupload",
				dataType: "json",
				data: d
			})
			.success(function(obj) {
				alert('success: '+obj.status);
			})
			.error(function() {
				alert('error!');
			 });

			return false;
		});
*/

	})
})(jQuery);
</script>

