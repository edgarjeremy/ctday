<?php
/**
 * Galeri admin view - manage images/videos in gallery
 *
 * @package  	Galeri
 * @subpackage	Admin_Views
 * @category  	Module
 */
?>
<section class="title">
		<h4>Manage Gallery - <?php echo $gallery->title; ?></h4>
</section>


<section class="item">
<div class="content">

<div class="buttons">
<a href="<?php echo site_url('admin/galleries/upload/'.$gallery->id);?>" class="btn green open-files-uploader"><?php echo lang('galleries.upload_label'); ?> [Max <?php echo ini_get('upload_max_filesize'); ?>]</a>
<a href="<?php echo site_url('admin/galleries/vupload/'.$gallery->id);?>" class="btn green open-youtube"><?php echo lang('galleries.create_youtube_label'); ?></a>
<br />&nbsp;

</div>
<div class="clear-both text-small1"><?php echo lang('galleries.media_image_msg'); ?></div>

<?php echo form_open('admin/galleries/deletemany', 'class="crud"'); ?>

<?php echo form_hidden('redirect_to', uri_string()); ?>

<div style="display: block;" id="grid" class="list-items">

<?php if (!empty($media)) $display = 'block'; else $display = 'none'; ?>

<div id="mark_all" style="display: <?php echo $display; ?>">
	<label class="inline"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'mark-all')) . ' All'; ?></label>
</div>

<ul id="files-uploaded" class="grid clearfix">
<?php if (!empty($media)): ?>

    <?php foreach($media as $file): ?>
        <li class="ui-corner-all">
            <div class="actions" style="text-align: center;">
        	<?php echo form_checkbox('imgid[]', $file->id); ?>
			<?php
				if (!in_array(strtolower(trim($file->extension,'.')), $format_videos))
				{
					echo anchor('admin/galleries/crop/'.$file->id, lang('galleries.crop_label'), 'class="imgcrop"' ) . ' | ';
				}

				echo anchor( (in_array(strtolower(trim($file->extension,'.')), $format_videos) ? 'admin/galleries/editvideo/' : 'admin/galleries/editmedia/') .$file->id, lang('galleries.edit_label'), 'class="no-edit"' );

				if (group_has_role('galeri', 'delete_image')) 
				{
					echo ' |'. anchor('admin/galleries/deletemedia', lang('galleries.delete_label'), 'class="imgdel"' );
				}
			?>
            </div>

		<?php if ((trim($file->extension,'.') == 'yt') OR (trim($file->extension,'.') == 'vim')): ?>

			<?php if ($file->thumbnail): ?>

					<?php if (substr($file->thumbnail, 0, 4) <> 'http'): ?>
				        <a title="<?php echo $file->thumbnail; ?>" href="<?php echo site_url().UPLOAD_PATH .'galleries/'.$gallery->id.'/'. $file->thumbnail; ?>" rel="cb_0" class="modal">
						<img title="<?php echo $file->thumbnail; ?>" src="<?php echo site_url().UPLOAD_PATH.'galleries/'.$gallery->id.'/'. substr($file->thumbnail, 0, -4) . '_thumb' . substr($file->thumbnail, -4);?>" alt="<?php echo $file->thumbnail; ?>" width="115" />
					<?php else: ?>
				        <a title="<?php echo $file->thumbnail; ?>" href="<?php echo str_ireplace('default.jpg', 'hqdefault.jpg', $file->thumbnail); ?>" rel="cb_0" class="modal">
						<img title="<?php echo $file->thumbnail; ?>" src="<?php echo $file->thumbnail;?>" alt="<?php echo $file->thumbnail; ?>" width="115" />
					<?php endif; ?>

			<?php else: ?>

		        <a title="<?php echo $file->title; ?>" href="<?php //echo image_path('icon-video.jpg', 'galleries'); ?>" rel="cb_0" class="modal">
				<?php //echo image('icon-video_thumb.jpg', 'galleries', array('alt' => 'Video file - No Thumbnail')); ?>

			<?php endif; ?>

		<?php elseif (in_array(strtolower(trim($file->extension,'.')), $format_videos)): ?>

			<?php if ($file->thumbnail): ?>

		        <a title="<?php echo $file->thumbnail; ?>" href="<?php echo site_url().UPLOAD_PATH .'galleries/'.$gallery->id.'/'. $file->thumbnail; ?>" rel="cb_0" class="modal">
	            <img title="<?php echo $file->title; ?>" src="<?php echo site_url().UPLOAD_PATH.'galleries/'.$gallery->id.'/'. substr($file->thumbnail, 0, -4) . '_thumb' . substr($file->thumbnail, -4);?>" alt="<?php echo $file->thumbnail; ?>" />

			<?php else: ?>

		        <a title="<?php echo $file->title; ?>" href="<?php echo image_path('icon-video.jpg', 'galeri'); ?>" rel="cb_0" class="modal">
				<?php echo image('icon-video_thumb.jpg', 'galeri', array('alt' => 'Video file - No Thumbnail')); ?>

			<?php endif; ?>

		<?php else: ?>
			<?php
				$filename = $file->media;
				$ext = substr(strrchr($filename, "."), 1);
				$fn = str_ireplace('.'.$ext, '', $filename);
				$fullfile = ( file_exists(UPLOAD_PATH .'galleries/'.$gallery->id.'/'. $fn . '_full.' . $ext) ? $fn . '_full.' . $ext : $filename );
				$thumbfile = $fn . '_thumb.' . $ext;
				$smallfile = $fn . '_small.' . $ext;
				$medfile = $fn . '_med.' . $ext;
			?>
	        <a title="<?php echo $file->title; ?>" href="<?php echo site_url().UPLOAD_PATH .'galleries/'.$gallery->id.'/'. $fullfile; ?>" rel="cb_0" class="modal">
            <img title="<?php echo $file->title; ?>" src="<?php echo site_url().UPLOAD_PATH.'galleries/'.$gallery->id.'/'. $thumbfile;?>" alt="<?php echo $file->media; ?>" />
		<?php endif; ?>
        </a>
				
<!--         <div style="font-size: 0.90em;padding:5px 0 0 9px; border-top: 1px solid #ccc;"> -->
        <div style="font-size: 0.90em;padding:5px 0 0 9px;">
        	<p><?php echo ($file->title ? $file->title : '[no title]'); ?><br />
        	<?php echo ($file->subtitle ? $file->subtitle : '[no subtitle]'); ?></p>
        	<?php echo ($file->description ? character_limiter($file->description,60) : '[no description]'); ?></p>
        </div>
        <input type="hidden" name="img_media_id[]" value="<?php echo $file->id; ?>" />
        </li>
    <?php endforeach; ?>

<?php else: ?>
	<li style="list-style-type: none;">
	<div class="blank-slate">	
		<h2><?php echo lang('galleries.no_media_error'); ?></h2>
	</div>
	</li>

<?php endif; ?>

	</ul>
</div>

<?php if (!empty($media)) $hidden = ''; else $hidden = 'hidden'; ?>
	<div id="btnarea" class="buttons align-right padding-top clear-both <?php echo $hidden; ?>">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
	</div>
<?php //endif; ?>

</form>

</div>
</section>
			<div class="hidden">
				<div id="files-uploader">
					<div class="files-uploader-browser">
						<form action="<?php echo site_url('/admin/galleries/upload/'.(!empty($gallery->id)?$gallery->id:'')) ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" name="frmUpload">
							<label for="userfile" class="upload">Upload Files</label>
							<input type="file" name="userfile" value="" class="no-uniform" multiple="multiple" />
							<div style="visibility:hidden;">
							<input type="text" name="mediatitle">
							<textarea rows="3" cols="30" name="mediadescription"></textarea>
							</div>
							<?php if (!empty($gallery->id)): ?>
								<input type="hidden" id="gallery_id" name="gallery_id" value="<?php echo $gallery->id; ?>" />
							<?php endif; ?>
						</form>
						<ul id="files-uploader-queue" class="ui-corner-all"></ul>
		
					</div>
					<div class="buttons align-right padding-top">
						<a href="#" title="" class="button start-upload">Upload</a>
						<a href="#" title="" class="button cancel-upload">Cancel</a>
					</div>
				</div>
			</div>

<script type="text/javascript">

	var gallery_id = jQuery('#gallery_id').val();
	var startbtn = '<?php echo lang('galleries.start_label'); ?>';
	var cancelbtn = '<?php echo lang('galleries.delete_label'); ?>';

(function($){
	// Store data for filesUpload plugin
	$('#files-uploader form').data('fileUpload', {
		lang : {
			start : 'Start',
			cancel : 'Delete'
		}
	});
})(jQuery);

</script>
