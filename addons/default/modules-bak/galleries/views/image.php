<style type="text/css">
#cboxTitle {
	display: none !important;
}
</style>
<div style="width:690px;margin:auto;position:relative;height:100%;">
	<div id="cboxImg" style="max-width:690px;margin:auto;height:100%;max-height:100%;z-index:99;">
		<img class="cbImg" style="width:100%;max-width:690px;height:100%;max-height:100%;margin:auto;display:block;z-index:99;" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$image->gallery_id.'/'.$image->media); ?>" />
		<div class="caption" style="width: 100%;z-index:100;">
			<p><?php echo strip_tags($image->description); ?></p>
		</div>
	</div>
</div>
