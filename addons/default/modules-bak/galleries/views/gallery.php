
<h1 id="page_title"><?php echo $gallery->title; ?></h1>
<?php if ($gallery->subtitle): ?>
	<b><?php echo $gallery->subtitle; ?></b>
<?php endif; ?>
<!-- A gallery needs a description.. -->
<div class="gallery_heading">
	<?php echo $gallery->description; ?>
</div>
<hr size="1" />
<div class="clearfix"></div>

<!-- Div containing all galleries -->
<div class="galleries_container" id="gallery_single">
	<div class="gallery_content clearfix">
		<!-- The list containing the gallery images -->
		<ul class="galleries_list">
			<?php if ($images): ?>
			<?php foreach ( $images as $image): ?>
			<li style="width:400px; display:block; float: left; padding-right: 20px; padding-bottom: 25px;">
				<?php if ($image->extension == 'yt'): ?>
					<a href="h_<?php echo $image->id?>" class="gallery-image ytvid">
<!-- 					<a href="<?php echo $image->media ?>" class="gallery-image"> -->
					<img width="350" class="tiptip" src="<?php echo ($image->thumbnail && substr($image->thumbnail,0,4) <> 'http') ? '/'.UPLOAD_PATH . 'galleries/'.$gallery->id.'/'.$image->thumbnail : $image->thumbnail  ?>" /></a>
					<div style="display: none;">
						<div id="h_<?php echo $image->id?>" style="width: 420px; height: 315px;">
							<!-- <iframe width="420" height="315" src="http://www.youtube.com/embed/bSiDLCf5u3s" frameborder="0" allowfullscreen></iframe> -->
							<iframe src="http://www.youtube.com/embed/<?php echo $image->media ?>" frameborder="0" allowfullscreen width="420" height="315"></iframe>
						</div>
					</div>
				<?php else: ?>
					<a
					href="<?php echo site_url(UPLOAD_PATH.'galleries/'.$gallery->id.'/'.$image->media); ?>"
					class="gallery-image imgmodal" rel="gallery-image" title="<?php echo $image->title; ?>">
					<img class="tiptip" src="<?php echo site_url(UPLOAD_PATH.'galleries/'.$gallery->id.'/'.thumbnail($image->media)); ?>" /></a>
				<?php endif; ?>

					<div class="tipContent">
					<?php if ($image->title): ?>
						<?php if ($image->title) echo '<p class="title">'.$image->title.'</p>';?>
						<?php if ($image->subtitle) echo '<div class="imageSubtitle">'.$image->subtitle.'</div>';?>
						<?php if ($image->description) echo '<div class="imageSubtitle">'.$image->description.'</div>';?>
					<?php else: ?>
						<?php echo $image->media; ?>
					<?php endif; ?>
					</div>
					<!-- </a> -->
			</li>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</div>
<br style="clear: both;" />

<script>
$(document).ready(function() {

	$('ul.galleries_list li a.ytvid').livequery('click', function(){
		var refdiv = $(this).attr('href');

		$(this).colorbox({
			inline: true,
			href: "#"+refdiv,
			innerWidth: "420px",
			innerHeight: "315px",
			onComplete: function(){ $.colorbox.resize(); }
		});
	});


	$('.imgmodal').colorbox();

	// Tipsy
	if (jQuery().tipsy)
	{
		$('ul.galleries_list li img.tiptip').livequery(function(){
			$(this).tipsy({
				gravity: 'n',
				title: function(){
			        	return $(this).nextAll('.tipContent').html();
			        },
				fade: true,
				html: true
			});
		});
	}

});
</script>
