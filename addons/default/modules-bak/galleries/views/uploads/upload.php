<div id="form-container" style="padding:10px;">

<?php echo form_open_multipart(site_url('galleries/uploadimage'), array('class' => 'crud', 'id' => 'frmUpload')); ?>

<h3 class="upload-image">Upload your image</h3>

<div style="display:block; padding-bottom:15px;">
	<label>Your name</label>
	<input type="text" size="50" name="name" value="<?php echo set_value('name'); ?>"/>
</div>

<div style="display:block; padding-bottom:15px;">
	<label>Your email<small>Your email will not be shared to a third party</small></label>
	<input type="text" size="50" name="email" />
</div>
	<div class="clearfix"></div>

<div style="display:block; padding-bottom:15px;">
	<label>Image</label>
	<input type="file" size="20" name="imgfile" />
</div>

<div style="display:block; padding-bottom:15px;">
	<label>Image caption<small>100 chars max</small></label>
	<input type="text" size="50" maxlength="100" name="caption" value="<?php echo set_value('caption'); ?>"/>
</div>

<div style="display:block; padding-bottom:15px;">
	<label></label>
	<input type="submit" name="btnSubmit" value="Upload" />
</div>

<small>
By uploading this image to thecoraltriangle.com:
<ul>
	<li>you confirm that you are the rightful owner of the image or that you have obtained the necessary permission to share it.</li>
	<li>you agree for the image to be posted on thecoraltriangle.com</li>
</ul>
</small>

<?php echo form_close() ?>

</div>
