<div id="form-container" style="padding:10px;">

<?php echo form_open_multipart(site_url('galleries/uploadvideo'), array('class' => 'crud', 'id' => 'frmUpload')); ?>

<h3 class="upload-image">Upload your video</h3>

<div style="display:block; padding-bottom:15px;">
	<label>URL</label>
	<input onblur="getThumb();" id="media" type="text" size="50" name="vidname" value="<?php echo !empty($file->vidname) ? $file->vidname : '' ?>" /><br />
	<div style="display:block; padding-bottom:15px; float:left; padding-left:175px;">
		<label><a href="#" id="getthumb">Refresh thumbnail</a></label>
			<div id="videothumbnail" style="width:200px;display:block;float:left;">
				<?php echo !empty($file->thumbnail_small) ? '<img src="'.$file->thumbnail_small.'" />' : '' ?>
			</div>
			<input type="hidden" id="thumbnail" name="thumbnail" value="<?php echo !empty($file->thumbnail) ? $file->thumbnail : '' ?>" />
			<input type="hidden" id="thumbnail_small" name="thumbnail_small" value="<?php echo !empty($file->thumbnail_small) ? $file->thumbnail_small : '' ?>" />
			<input type="hidden" id="vid" name="vid" value="<?php echo !empty($file->vid) ? $file->vid : '' ?>" />
	</div>
</div>

	<div class="clearfix"></div>

<div style="display:block; padding-bottom:15px;">
	<label>Description<small>100 chars max</small></label>
	<input id="caption" type="text" size="50" maxlength="100" name="caption" value="<?php echo !empty($file->caption) ? $file->caption : '' ?>"/>
</div>


<div style="display:block; padding-bottom:15px;">
	<label>Your name</label>
	<input type="text" size="50" name="name" value="<?php echo !empty($file->name) ? $file->name : '' ?>"/>
</div>
	<div class="clearfix"></div>

<div style="display:block; padding-bottom:15px;">
	<label>Your email<small>Your email will not be shared to a third party</small></label>
	<input type="text" size="50" name="email" value="<?php echo !empty($file->email) ? $file->email : '' ?>" />
</div>
	<div class="clearfix"></div>

<div style="display:block; padding-bottom:15px;">
	<label></label>
	<input type="submit" name="btnSubmit" value="Upload" />
</div>

<small>
By sharing this video to thecoraltriangle.com:
<ul>
	<li>you confirm that you are the rightful owner of the video or that you have obtained the necessary permission to share it.</li>
	<li>you agree for the video to be posted on thecoraltriangle.com</li>
</ul>
</small>

<?php echo form_close() ?>

</div>

<script>
$(document).ready(function(){

		form = $("form.crud");

		$('#getthumb').click(function(e){
			e.preventDefault();

			if ($('#media').val() == '') {
				alert('Please embed the video code first.');
				$('#media').focus();
				return false;
			}

			$('#videothumbnail').fadeTo('slow', 0, function(){
				//$('#checkingthumb').fadeIn('slow');
				$('#cboxLoadingOverlay, #cboxLoadingGraphic').show();
			});

			getThumb();

		});

		$('#resettodefault').click(function(e){
			e.preventDefault();
			var srcdef = '<img alt="Default thumbnail" src="{{url:site}}addons/default/modules/galleries/img/icon-video_thumb.jpg" />';
			$('#thumburl').val('');
			$('#thumb img').fadeOut('slow', function(){
				$('#thumb').html(srcdef);
				$('#thumb img').load(function(){
					$('#thumb').fadeTo('fast', 1);
				});
				$('#resettodefault').fadeOut('fast');
			});
		});

	$('a#showembed').livequery('click', function(){
		$('#attach').slideUp();
		$('#embed').slideDown();
	});

	$('a#showattach').livequery('click', function(){
		$('#embed').slideUp();
		$('#attach').slideDown();
	});

});

function getThumb()
{
			$('#cboxLoadingOverlay, #cboxLoadingGraphic').show();

			var jqxhr = $.post(SITE_URL + "galleries/vidthumb", {media: $('#media').val()}, function(obj) {
				if (obj)
				{
					yt = JSON && JSON.parse(obj) || $.parseJSON(obj);
					$('#videothumbnail').html('<img src="'+yt.thumbnail_small+'" id="vthumb" />')
					if (yt.description)
						$('#caption').val(yt.description);
					$('#thumbnail').val(yt.thumbnail);
					$('#thumbnail_small').val(yt.thumbnail_small);
					$('#vid').val(yt.vid);
					
					//$('#videothumbnail').html(yt.thumbnail_small);

/*
					$('#thumb img').attr('src', yt.thumbnail);
					$('#title').val(yt.title);
					$('#description').val(yt.description);
					$('#video_id').val(yt.video_id);
					$('#thumb img').load();
					$('#thumb label').remove();
*/
				}
			})
			.success(function() {
					$('#cboxLoadingOverlay, #cboxLoadingGraphic').fadeOut('slow', function(){
						$('#resettodefault').fadeIn('fast');
						$('#videothumbnail').fadeTo('fast', 1);
					});
					$.colorbox.resize();
			})
			.error(function() {
				$('#cboxLoadingOverlay, #cboxLoadingGraphic').fadeOut(function(){
					$('#checkingthumb').css('z-index', '-1');
					$('#videothumbnail').fadeTo('slow', 1);
				});			
			 });
			 //$('#cboxLoadingOverlay, #cboxLoadingGraphic').hide();
}
</script>
