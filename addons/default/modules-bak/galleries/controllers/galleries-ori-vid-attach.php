<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * The galeri module enables users to create albums, upload photos and manage their existing albums.
 *
 * @author 		Okky Sari
 *
 * @package  	Galeri
 * @subpackage  Frontend
 * @category  	Module
 */
class Galleries extends Public_Controller
{
	/**
	 * Limit of Galleries to retrieve before paginated
	 */
	public $limit = 8;

	/**
	 * Accepted video formats (not used anymore)
	 */
	public $format_videos = array('ogg', 'flv', 'mpg', 'mpeg', '3gp', 'mp4', 'mov', 'webm', 'yt');

	/**
	 * Accepted image formats
	 */
	public $format_images = array('jpg', 'gif', 'png', 'jpeg');


	private $_file_rules = array(
		array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|max_length[250]'
		),
		array(
			'field' => 'email',
			'label' => 'Email address',
			'rules' => 'trim|required|valid_email|max_length[100]'
		),
	);

	private $_video_rules = array(
		array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|max_length[250]'
		),
		array(
			'field' => 'email',
			'label' => 'Email address',
			'rules' => 'trim|required|valid_email|max_length[100]'
		),
		array(
			'field' => 'vidname',
			'label' => 'URL Video',
			'rules' => 'trim|required|max_length[128]'
		),
	);

	public $upload_cfg = array();

	/**
	 * Width of thumbnail image
	 */
	public $t_gallery_w = 115;

	/**
	 * Height of thumbnail image
	 */
	public $t_gallery_h = 77;


	/**
	 * Width of small images
	 */
	public $s_gallery_w = 310;

	/**
	 * Height of small image
	 */
	public $s_gallery_h = 206;

	/**
	 * Maximum width of image
	 */
	public $m_gallery_w = 705;

	/**
	 * Maximum height of the image
	 */
	public $m_gallery_h = 469;

	/**
	 * Constructor method
	 *
	 * @author Yorick Peterse - PyroCMS Dev Team
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('galleries_m');
		$this->load->model('gallery_categories_m');
		$this->load->helper(array('text', 'filename'));
		$this->lang->load('galleries');
		$this->lang->load('categories');

		$codecs = array(
			'mp4'	=> 'avc1.42E01E, mp4a.40.2',
			'webm'	=> 'vp8, vorbis',
			'ogg'	=> 'theora, vorbis',
			'flv'	=> '',
			'3gp'	=> ''
		);
//		$this->data->codecs =& $codecs;

		$this->upload_cfg['upload_path']		= UPLOAD_PATH . 'galleries';
		$this->upload_cfg['allowed_types'] 		= 'jpg|gif|png|jpeg'; //|ogg|flv|mpg|mpeg|mp4|mov|3gp';
		$this->upload_cfg['max_size'] 			= '30000';
		$this->upload_cfg['remove_spaces'] 		= TRUE;
		$this->upload_cfg['overwrite']     		= FALSE;

		$this->_path = UPLOAD_PATH . 'galleries/';//$this->upload_cfg['upload_path'].'/';
//		$this->check_dir($this->_path);

		// get the categories
		$categories = $this->gallery_categories_m->order_by('title')->get_all();
		$this->template
			->set('codecs', $codecs)
			->set('categories', $categories);
	}

	/**
	 * List all existing galeri
	 *
	 * @access public
	 * @return void
	 */
	public function index()
	{
		$res = $this->gallery_categories_m->order_by('id')->get_by(array('status'=>'live'));
		$get_params = array('status' => 'live');

		$pagination = create_pagination('galeri/page', $this->galleries_m->count_by($get_params), $this->limit, 3);
		$galleries = $this->galleries_m->limit($pagination['limit'])->get_many_by($get_params);

		foreach($galleries as $g)
		{
			$thumb = $this->galleries_m->get_galeria_thumbnail($g->id);

			if ( $thumb->extension == 'yt' )
			{
					if (substr($thumb->thumbnail, 0, 4) == 'http')
					{
						$g->thumbnail = '<img src="'.$thumb->thumbnail.'" alt="" width="115" />';
					}
					elseif ( in_array(substr(strrchr($thumb->thumbnail, "."), 1), $this->format_images ) )
					{
						$thumbnail = '/'.UPLOAD_PATH . 'galleries/' .$g->id.'/' . thumbnail($thumb->thumbnail) ;
						$g->thumbnail = '<img src="'.$thumbnail.'" alt="" />';
					}
					else
					{
						$g->thumbnail = image('icon-video_thumb.jpg', 'galeri');
					}
			}
			else
			{
				$thumbnail = '/'.UPLOAD_PATH . 'galleries/' .$g->id.'/' . thumbnail($thumb->media) ;
				$g->thumbnail = '<img src="'.$thumbnail.'" alt="" />';
			}
		}

		// Set meta description based on post titles
		$meta = $this->_posts_metadata($galleries);

		$this->template
			->title($this->module_details['name'])
			->set_breadcrumb( lang('galleries.galleries_label'))
			->set_metadata('description', $meta['description'])
			->set_metadata('keywords', $meta['keywords'])
			->set("galleries", $galleries)
			->build('index');
	}

	/**
	 * List all existing galeri based on category supplied
	 *
	 * @params string $cat Category to use
	 * @access public
	 * @return void
	 */
	public function category($cat='')
	{
		if (!$cat)
			redirect('galleries');

		if ($cat)
		{
			$res = $this->gallery_categories_m->get_by('slug', $cat);
			$cat_id = $res->id;
			$current_cat = $res->title;
			$get_params = array('status' => 'live', 'category_id'=>$cat_id);
			$this->template->set('current_cat', $res);
		}
		else
		{
			$res = $this->gallery_categories_m->order_by('id')->get_by(array('status'=>'live'));
			$get_params = array('status' => 'live');
		}

		$pagination = create_pagination('galeri/category/'.$cat.'/page', $this->galleries_m->count_by($get_params), $this->limit, 5);
		$galleries = $this->galleries_m->limit($pagination['limit'])->get_many_by($get_params);

		foreach($galleries as $g)
		{
			$thumb = $this->galleries_m->get_galeria_thumbnail($g->id);
			if ( in_array(strtolower($thumb->extension), $this->format_videos) )
			{
				if (!$thumb->thumbnail)
				{
					$g->thumbnail = image('icon-video.jpg', 'galeri', 'width="310"');
				}
				else
				{
					$thumbnail = '/'.UPLOAD_PATH . 'galeri/' .$g->id.'/' . thumbnail($thumb->thumbnail) ;
					$g->thumbnail = '<img src="'.$thumbnail.'" alt="" />';
				}
			}
			else
			{
				$thumbnail = '/'.UPLOAD_PATH . 'galeri/' .$g->id.'/' . substr($thumb->media, 0, -4) . '_small' . substr($thumb->media, -4) ;
				$g->thumbnail = '<img src="'.$thumbnail.'" alt="Doña Guadalupe" />';
			}
		}

		$this->data->galeris =& $galleries;
		// Set meta description based on post titles
		$meta = $this->_posts_metadata($this->data->galeris);

		$this->template
			->title($this->module_details['name'])
			->set_breadcrumb( lang('galleries.galleries_label'))
			->set_metadata('description', $meta['description'])
			->set_metadata('keywords', $meta['keywords'])
			->set("galleries", $galleries)
			->build('index');
	}

	public function media($type='image')
	{
		$this->load->model('gallery_media_m');
		if ($type == 'image')
		{
			$ext = array('jpg', 'png', 'gif');
		}
		elseif ($type == 'video')
		{
			$ext = array('yt', 'vm');
		}
		$images = $this->gallery_media_m->get_media_by_type($type);

		if ($this->input->is_ajax_request())
		{
			$this->template->set_layout(FALSE);
		}

		$this->template
			->set("images", $images)
			->build('media');
	}

	public function view($slug='')
	{
		if (!$slug) redirect('galleries');
		$id = $this->galleries_m->get_id_from_slug($slug);
		$images = $this->galleries_m->get_all_media($id);
		$galleries = $this->galleries_m->get($id);

		// Set meta description based on post titles
		$meta = $this->_posts_metadata($images);

		if ($this->input->is_ajax_request())
		{
			$this->template->set_layout(FALSE);
		}

		$this->template
			->title($this->module_details['name'])
			->set_breadcrumb( lang('galleries.galleries_label'), site_url('galleries'))
			->set_metadata('description', $meta['description'])
			->set_metadata('keywords', $meta['keywords'])
			->set("gallery", $galleries)
			->set("images", $images)
			->build('gallery');
	}

	/**
	 * Construct lightbox gallery images
	 *
	 * @params string $slug Gallery slug to retrieve
	 * @access public
	 * @return void
	 */
	public function lightbox($slug='')
	{
		if ($slug)
		{
			$id = $this->galleries_m->get_id_from_slug($slug);
			$this->data->images = $this->galleries_m->get_all_media($id);
			$this->load->view('lightbox', $this->data);
		}
	}

	public function upload($type='')
	{
//echo "\$type: $type<br />\n";
		if ($type=='image')
			$this->load->view('uploads/upload', array());
		elseif ($type=='video')
			$this->load->view('uploads/upload-video', array());
	}


	public function uploadvideo()
	{
	}


	public function uploadimage()
	{
		$gallery_id = 1;

		foreach($this->_file_rules as $rule)
		{
			$file->{$rule['field']} = $this->input->post($rule['field']);
		}

		$form		= $this->load->view('uploads/upload-form', array('file'=>$file), TRUE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_file_rules);

		// Setup upload config
		$this->upload_cfg['upload_path'] .= '/'.$gallery_id;
		$this->load->library('upload');
		$this->upload->initialize($this->upload_cfg);

		if ($this->form_validation->run())
		{

			// check directory exists
			//$this->check_dir($this->upload_cfg['upload_path']);

			// File upload error
			if ( ! $this->upload->do_upload('imgfile'))
			{
				$status		= 'error';
				$message	= $this->upload->display_errors();

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'form'		=> $form,
					));
				}

				$this->data->messages[$status] = $message;
			}

			// File upload success
			else
			{
				$file = $this->upload->data();
				$data = array(
					'media'			=> $file['file_name'],
					'gallery_id'	=> $gallery_id,
					'title'			=> $this->input->post('mediatitle'),
					'subtitle'		=> $this->input->post('mediasubtitle'),
					'description'	=> 'Uploaded by '.$this->input->post('name'),
					'extension'		=> $file['file_ext'],
					'status'		=> 'draft',
					'uploaded_by'	=> 0,
					'uploaded_on'	=> now(),
					'position'		=> 999
				);

				if ($file['is_image'])
				{
					$this->load->library('image_lib');
					$filename = $file['file_name'];

					$ext = substr(strrchr($filename, "."), 1);
					$fn = str_ireplace('.'.$ext, '', $filename);
					$thumbfile = $fn . '_thumb.' . $ext;
					$smallfile = $fn . '_small.' . $ext;
					$medfile = $fn . '_med.' . $ext;
					/*---------------------------------------------------------------------------------
					// create thumb - admin
					*/
					$image_cfg['source_image'] = $file['full_path'];
					$image_cfg['maintain_ratio'] = FALSE;
					$image_cfg['width'] = ($file['image_width'] < $this->t_gallery_w ? $file['image_width'] : $this->t_gallery_w);
					$image_cfg['height'] = ($file['image_height'] < $this->t_gallery_h ? $file['image_height'] : $this->t_gallery_h);
					$image_cfg['create_thumb'] = FALSE;
					$image_cfg['new_image'] = $file['file_path'] . $thumbfile;
					$this->image_lib->initialize($image_cfg);
					$img_ok = $this->image_lib->resize();
					unset($image_cfg);
					$this->image_lib->clear();
	
					/*---------------------------------------------------------------------------------
					// create thumb - frontend
					*/
					$image_cfg['source_image'] = $file['full_path'];
					$image_cfg['maintain_ratio'] = FALSE;
					$image_cfg['width'] = ($file['image_width'] < $this->s_gallery_w ? $file['image_width'] : $this->s_gallery_w);
					$image_cfg['height'] = ($file['image_height'] < $this->s_gallery_h ? $file['image_height'] : $this->s_gallery_h);
					$image_cfg['create_thumb'] = FALSE;
					$image_cfg['new_image'] = $file['file_path'] . $smallfile;
					$this->image_lib->initialize($image_cfg);
					$img_ok = $this->image_lib->resize();
					unset($image_cfg);
					$this->image_lib->clear();

						/*
						/* image resize - medium
						*/
						$image_cfg['source_image'] = $file['full_path'];
						$image_cfg['maintain_ratio'] = TRUE;
						$image_cfg['width'] = ($file['image_width'] < $this->m_gallery_w ? $file['image_width'] : $this->m_gallery_w);
						$image_cfg['height'] = ($file['image_height'] < $this->m_gallery_h ? $file['image_height'] : $this->m_gallery_h);
						$image_cfg['create_thumb'] = FALSE;
						$image_cfg['new_image'] = $file['file_path'] . $medfile;
						$this->image_lib->initialize($image_cfg);
						$img_ok = $this->image_lib->resize();
						unset($image_cfg);
						$this->image_lib->clear();

				}

				// Insert success
				if ($id = $this->galleries_m->insertimg($gallery_id, $data))
				{
					$status		= 'success';
					$message	= lang('galleries.upload_img_success');
				}
				// Insert error
				else
				{
					$this->unlinkfile($file['file_name'], $gallery_id);
					$status		= 'error';
					$message	= sprintf(lang('galleries.dbstore_img_error'), $file['file_name']);
				}

				if ($this->input->is_ajax_request())
				{
					$data = array();
					$data['messages'][$status] = $message;
					$message = $this->load->view('notices', $data, TRUE);

					return $this->template->build_json(array(
						'status'	=> $status,
						'message'	=> $message,
						'file'		=> array(
							'name'	=> $file['file_name'],
							'type'	=> $file['file_type'],
							'size'	=> $file['file_size']
						)
					));
				}
				else
				{
					$this->session->set_flashdata($status, $message);
					redirect('');
				}

				if ($status === 'success')
				{
					$this->session->set_flashdata($status, $message);
					redirect('');
				}
				else
				{
					if ($this->input->is_ajax_request())
					{
						$data = array();
						$data['messages'][$status] = $message;
						$message = $this->load->view('notices', $data, TRUE);
		
						return $this->template->build_json(array(
							'status'	=> 'error',
							'message'	=> $message,
							'form'		=> $form,
						));
					}
				}
			}
		}
		elseif (validation_errors())
		{
			// if request is ajax return json data, otherwise do normal stuff
			if ($this->input->is_ajax_request())
			{
				$data = array();
				$status = 'error';
//				$data['messages'][$status] = validation_errors();
				$message = $this->load->view('notices', $data, TRUE);

				return $this->template->build_json(array(
					'status'	=> 'error',
					'message'	=> $message,
					'form'		=> $form,
				));
			}
		}

		if ($this->input->is_ajax_request())
		{
			// todo: debug errors here
			$status		= 'error';
			$message	= 'unknown';

			$data = array();
			$data['messages'][$status] = $message;
			$message = $this->load->view('notices', $data, TRUE);

			return $this->template->build_json(array(
				'status'	=> $status,
				'message'	=> $message
			));
		}

		$this->template
			->title()
			->build('uploads/upload', $this->data);
	}

	public function vidthumb()
	{
		$url = $this->input->post('media');
		if (stripos($url, 'youtube.com'))
		{
			$this->yt_thumbnail();
		}
		elseif (stripos($url, 'vimeo.com'))
		{
			$this->vimeo_thumbnail();
		}
	}

	public function yt_thumbnail()
	{
		$html = $this->input->post('media');
		$html = trim($html);
		$video_id = substr(strrchr($html, "/"), 1);

		if (substr($video_id, 0, 5) == 'watch')
		{
			$video_id = str_ireplace('watch?v=', '', $video_id);
		}

		$url = "https://gdata.youtube.com/feeds/api/videos/$video_id?v=2&alt=jsonc";
		$html = $this->_fetchUrl($url);

		$html = json_decode($html);

		if (!empty($html->data))
		{
			$ret = array(
				'thumbnail_small'	=> $html->data->thumbnail->sqDefault,
				'thumbnail'			=> $html->data->thumbnail->hqDefault,
				'title'				=> $html->data->title,
				'description'		=> $html->data->description,
				'video_id'			=> $video_id
			);
			echo json_encode($ret);
		}
	}

	public function vimeo_thumbnail()
	{
		$html = $this->input->post('media');
		$html = trim($html);
		$video_id = substr(strrchr($html, "/"), 1);

		if (substr($video_id, 0, 5) == 'watch')
		{
			$video_id = str_ireplace('watch?v=', '', $video_id);
		}
// http://vimeo.com/api/v2/video/$video_id.json
		$url = "http://vimeo.com/api/v2/video/$video_id.json";
		$html = $this->_fetchUrl($url);

		$html = json_decode($html);


//print_r($html);

		foreach($html as $h=>$v)
		{
			if (!empty($v))
			{
				$ret = array(
					'thumbnail_small'	=> $v->thumbnail_medium,
					'thumbnail'			=> $v->thumbnail_small,
					'title'				=> $v->title,
					'description'		=> $v->description,
					'video_id'			=> $video_id
				);
				echo json_encode($ret);
			}
		}
	}

	private function _fetchUrl($url){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		// You may need to add the line below
		// curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);

		$feedData = curl_exec($ch);
		curl_close($ch); 

		return $feedData;
	
	}



	// Private methods not used for display
	private function _posts_metadata(&$posts = array())
	{
		$keywords = array();
		$description = array();

		// Loop through posts and use titles for meta description
		if(!empty($posts))
		{
			foreach($posts as &$post)
			{
				if($post->title)
				{
					$keywords[$post->title] = $post->title .', '. (!empty($post->slug) ? $post->slug : '');
				}
				$description[] = $post->description;
			}
		}

		return array(
			'keywords' => implode(', ', $keywords),
			'description' => implode(', ', $description)
		);
	}

}

// end of galeri.php