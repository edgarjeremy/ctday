<?php
/**
 * Galeri language definitions
 *
 * @package  	Galeri
 * @subpackage	Languages
 * @category  	Module
 */

// Success notifications
$lang['galleries.create_success'] 	        = 'The gallery has been created successfully.';
$lang['galleries.update_success'] 	        = 'The gallery has been successfully updated.';
$lang['galleries.delete_success']		        = 'All galleries have been deleted successfully.';
$lang['galleries.install_success']	        = 'The module has been installed successfully.';
$lang['galleries.delete_img_db_success']  	= 'Image was removed from database.';
$lang['galleries.delete_img_success']		    = 'File "%s" was deleted.';
$lang['galleries.media_update_success'] 	    = 'The media has been successfully updated.';
$lang['galleries.media_crop_success'] 	    = 'The media has been cropped successfully.';
$lang['galleries.video_upload_success'] 	    = 'The video has been successfully uploaded.';
$lang['galleries.video_edit_success'] 	    = 'The video has been successfully edited.';
$lang['galleries.video_edit_title'] 		    = 'Edit Video';

// Error notifications
$lang['galleries.delete_error']			    = 'The "%s" gallery could not be deleted.';
$lang['galleries.edit_empty_gallery_error']	= 'Error trying to edit the gallery.';

$lang['galleries.already_exist_error']	    = 'A gallery with the URL "%s" already exists.';
$lang['galleries.create_error']			    = 'The gallery could not be created.';
$lang['galleries.dbstore_img_error']		    = 'The file "%s" cannot be stored in the database.';
$lang['galleries.delete_error']			    = 'The gallery could not be deleted.';
$lang['galleries.delete_img_db_error']	    = 'Could not remove image from database.';
$lang['galleries.delete_img_error']		    = 'File "%s" could not be deleted.';
$lang['galleries.exists_error']			    = 'The specified gallery does not exist.';
$lang['galleries.folder_duplicated_error']    = 'An error occurred! The folder already belongs to a gallery.';
$lang['galleries.id_error']				    = 'No IDs have been specified.';
$lang['galleries.install_error']			    = 'The module could not be installed.';
$lang['galleries.media_not_found_error']	    = 'The specified media does not exist.';
$lang['galleries.media_save_error']		    = 'The media could not be saved.';
$lang['galleries.media_update_error']		    = 'The media could not be updated.';
$lang['galleries.no_delete_error']		    = 'No galleries have been deleted.';
$lang['galleries.no_galeria_cat_error'] 	    = 'No galleries have been created in this category.';
$lang['galleries.no_galeria_error'] 		    = 'No galleries have been created yet.';
$lang['galleries.no_gallery_description']	    = 'No description has been added yet.';
$lang['galleries.no_media_error'] 		    = 'There are currently no media.';
$lang['galleries.update_error']			    = 'The gallery could not be updated.';
$lang['galleries.media_crop_error'] 	    	= 'Error cropping the media.';
$lang['galeri_video_not_supported']		= 'Your browser does not support the "%s" video format.';
$lang['galeri_unable_create_dir']			= 'Sorry, attempt to create directory "%s" has failed. Please check permissions.';

// Labels
$lang['galleries.actions_label']				= 'Actions';
$lang['galleries.categories_label']			= 'Categories';
$lang['galleries.comments_disabled_label']	= 'No';
$lang['galleries.comments_enabled_label']		= 'Yes';
$lang['galleries.create_youtube_label']		= 'Add Video';
$lang['galleries.create_video_label']			= 'Upload Video';
$lang['galleries.created_label']				= 'Created';
$lang['galleries.crop_label']					= 'Crop'; 
$lang['galleries.delete_label']				= 'Delete';
$lang['galleries.edit_galeria_label']			= 'Edit Gallery';
$lang['galleries.edit_label']					= 'Edit';
$lang['galleries.enlarge_label']				= 'Enlarge';
$lang['galleries.galleries_label']			= 'Galleries';
$lang['galleries.gallery_label']				= 'Gallery';
$lang['galleries.manage_gallery_label']		= 'Manage Gallery';
$lang['galleries.manage_label']				= 'Manage';
$lang['galleries.media_edit_label']			= 'Edit Media';
$lang['galleries.new_gallery_label']			= 'New Gallery';
$lang['galleries.num_photos_label']			= 'Number of Media';
$lang['galleries.photo_gallery_label']		= 'Photo Gallery';
$lang['galleries.start_label']				= 'Start';
$lang['galleries.status_draft_label']			= 'Draft';
$lang['galleries.status_live_label']			= 'Live';
$lang['galleries.updated_label']				= 'Last Updated';
$lang['galleries.upload_label']				= 'Upload Images';
$lang['galleries.view_label']					= 'View';
$lang['galleries.youtube_link_label']			= 'Video Embed Code';
$lang['galleries.default_thumbnail_label']	= 'Default Thumbnail. Uploaded thumbnail file will override this image.';
$lang['galleries.checking_thumbnail']			= 'Checking Thumbnail';
$lang['galleries.media_field_empty_msg']		= 'Please embed the video code first.';


// General labels
$lang['galleries.current_label']			    = 'Current Images (drag and drop to sort, click to edit)';
$lang['galleries.description_label']		    = 'Description';
$lang['galleries.meta_description_label']	    = 'Meta-description';
$lang['galleries.meta_keywords_label']	    = 'Meta-keywords';
$lang['galleries.meta_title_label']		    = 'Meta-title';
$lang['galleries.no_thumb_label']			    = 'No Thumbnail';
$lang['galleries.slug_label']				    = 'Slug';
$lang['galleries.status_label']			    = 'Status';
$lang['galleries.subtitle_label']			    = 'Subtitle';
$lang['galleries.thumbnail_label']		    = 'Thumbnail';
$lang['galleries.title_label']			    = 'Title';

$lang['galleries.current_thumbnail']			= 'Current';
$lang['galleries.default_thumbnail']			= 'Default';
$lang['galleries.youtube_thumbnail']			= 'YouTube';
$lang['galleries.get_youtube_thumbnail']		= 'Get thumbnail';

// Sidebar labels
$lang['galleries.list_label']				    = 'List Galleries';

// Frontend labels
$lang['galleries.sub-galleries_label']		= 'Sub Galleries';

// tabs
$lang['galleries.content_label']			    = 'Gallery content';

$lang['galleries.remove_image_label']			= 'Remove image and use default thumbnail.';

