<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Galeri language definitions
 *
 * @package  	Galeri
 * @subpackage	Languages
 * @category  	Module
 */

$lang['galleries.role_put_live']		= 'Put Gallery live';
$lang['galleries.role_edit_live']		= 'Edit live Gallery';
$lang['galleries.role_delete_live'] 	= 'Delete live Gallery';

$lang['galleries.role_delete_img_live'] 	= 'Delete live Images';
$lang['galleries.role_edit_img_live'] 	= 'Edit live Images';
$lang['galleries.role_pub_img_live'] 		= 'Put Images live';
