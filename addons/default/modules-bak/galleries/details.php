<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Galleries extends Module {

	public $version = '2.00';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Galleries',
				'id' => 'Galeri',
			),
			'description' => array(
				'en' => 'Manage media galleries.',
				'id' => 'Pengaturan galleries media.',
			),
			'frontend'	=> TRUE,
			'backend'	=> TRUE,
			'skip_xss'	=> TRUE,
			'menu'		=> 'content',
			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),
			'sections' => array(
			    'galleries' => array(
				    'name' => 'galleries.list_label',
				    'uri' => 'admin/galleries',
				    'shortcuts' => array(
						array(
					 	   'name' => 'galleries.new_gallery_label',
						    'uri' => 'admin/galleries/create',
						    'class' => 'add'
						),
					),
				),
				'categories' => array(
				    'name' => 'cat_list_title',
				    'uri' => 'admin/galleries/categories',
				    'shortcuts' => array(
						array(
						    'name' => 'cat_create_title',
						    'uri' => 'admin/galleries/categories/create',
						    'class' => 'add'
						),
				    ),
			    ),
		    ),
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('galleries');
		$this->dbforge->drop_table('gallery_media');
		$this->dbforge->drop_table('gallery_categories');

		$galleries = "
			CREATE TABLE " . $this->db->dbprefix('galleries') . " (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `slug` varchar(100) NOT NULL,
			  `gallery_year` int(11) NOT NULL DEFAULT '2015',
			  `category_id` int(11) NOT NULL,
			  `title` varchar(100) NOT NULL,
			  `subtitle` varchar(100) NOT NULL,
			  `description` text NOT NULL,
			  `extension` varchar(5) DEFAULT NULL,
			  `thumbnail` int(11) NOT NULL,
			  `status` enum('draft','live') NOT NULL,
			  `created_on` int(11) NOT NULL DEFAULT '0',
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;
		";

		$gallery_media = "
			CREATE TABLE " . $this->db->dbprefix('gallery_media') . " (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `gallery_id` int(11) NOT NULL,
			  `position` int(11) NOT NULL DEFAULT '0',
			  `title` varchar(100) NOT NULL,
			  `subtitle` varchar(100) NOT NULL,
			  `description` text,
			  `media` text NOT NULL,
			  `extension` varchar(5) DEFAULT NULL,
			  `meta_title` varchar(100) DEFAULT NULL,
			  `meta_description` text,
			  `meta_keywords` varchar(250) DEFAULT NULL,
			  `thumbnail` varchar(100) DEFAULT NULL,
			  `thumbnail_small` varchar(200) DEFAULT NULL,
			  `status` enum('draft','live') NOT NULL DEFAULT 'live',
			  `uploaded_on` int(11) NOT NULL DEFAULT '0',
			  `uploaded_by` int(11) NOT NULL DEFAULT '0',
			  `vuploaded_by` varchar(100) DEFAULT NULL,
			  `vuploaded_by_email` varchar(100) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;
		";

		$gallery_categories = "
			CREATE TABLE IF NOT EXISTS " . $this->db->dbprefix('gallery_categories') . " (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `status` enum('draft','live') NOT NULL DEFAULT 'live',
			  `slug` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `slug - unique` (`slug`),
			  UNIQUE KEY `title - unique` (`title`),
			  KEY `slug - normal` (`slug`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Gallery Categories'
		";

		if($this->db->query($galleries) && $this->db->query($gallery_media) && $this->db->query($gallery_categories) )
		{
			return TRUE;
		}
	}

	public function uninstall()
	{
		if ( $this->dbforge->drop_table('galleries') &&
			$this->dbforge->drop_table('gallery_media') &&
			$this->dbforge->drop_table('gallery_categories')
		)
			return TRUE;
	}

	public function upgrade($old_version)
	{
	}

	public function help()
	{
		/**
		 * Either return a string containing help info
		 * return "Some help info";
		 *
		 * Or add a language/help_lang.php file and
		 * return TRUE;
		 *
		 * help_lang.php contents
		 * $lang['help_body'] = "Some help info";
		*/
		return TRUE;
	}
}

/* End of file details.php */
