<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_CoralTriangle extends Theme {

    public $name = 'Coral Triangle';
    public $author = 'Catalyze Communications';
    public $author_website = 'http://www.catalyzecommunications.com';
    public $website = 'http://www.catalyzecommunications.com';
    public $description = 'Coral Triangle Day Template';
    public $version = '1.0.0';

}

/* End of file theme.php */