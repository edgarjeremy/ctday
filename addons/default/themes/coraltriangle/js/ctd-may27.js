"use strict";

var Catalyze = Catalyze || {};

//Google map api
Catalyze.map = Catalyze.Map || {};
Catalyze.map.element;

Catalyze.map.style = [{
						    "featureType": "administrative",
						    "elementType": "labels.text.fill",
						    "stylers": [{
						        "color": "#444444"
						    }]
						}, {
						    "featureType": "landscape",
						    "elementType": "all",
						    "stylers": [{
						        "color": "#f2f2f2"
						    }]
						}, {
						    "featureType": "poi",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "road",
						    "elementType": "all",
						    "stylers": [{
						        "saturation": -100
						    }, {
						        "lightness": 45
						    }]
						}, {
						    "featureType": "road.highway",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "simplified"
						    }]
						}, {
						    "featureType": "road.arterial",
						    "elementType": "labels.icon",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "transit",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "water",
						    "elementType": "all",
						    "stylers": [{
						        "color": "#46bcec"
						    }, {
						        "visibility": "on"
						    }]
						}];

Catalyze.map.init = function(){
	var Latlng = new google.maps.LatLng(-1.4894799, 121.8823101);
	var mapOptions = {
		styles: Catalyze.map.style,
		zoom: 5,
		scrollwheel: false,
		center: Latlng
	};

	Catalyze.map.element = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	addMarker();
};

Catalyze.map.toggleSideMenu = function(){
	Catalyze.map.toggleMap();
	$(".sidemenu-country-wrapper").animate({width:'toggle'},350);
};

Catalyze.map.toggledetailevent = function(){
	$(".sidemenu-country-wrapper").animate({width:'toggle'},350);
	$(".sidedetail-event-wrapper").animate({width:'toggle'},350);
	//$('#map-canvas').animate({width:'70%'}, 350)
};

Catalyze.map.updatedetailevent = function(){
	$(".sidedetail-event-wrapper").animate({width:'toggle'},350);
	//$(".sidedetail-event-wrapper").animate({width:'toggle'},350);
};

Catalyze.map.toggleMap = function(){
	var sidemenu = $(".sidemenu-country-wrapper").css('display');
	if(sidemenu == "none")
	{
		$('#map-canvas').animate({width:$("#map-canvas").width()-$(".sidemenu-country-wrapper").width()-70}, 350);	
	}
	else{
		$('#map-canvas').animate({width:$("#map-canvas").width()+$(".sidemenu-country-wrapper").width()+70}, 350);	
	}
}

Catalyze.map.hidemodal = function(){
	$(".map-wrapper .modal-map").hide();

	Catalyze.map.toggleSideMenu();
	
	//$('#map-canvas').animate({width:'70%'}, 350);	
};

$(function(){
	
	 $(".navbar-default .navbar-nav a").append("<span></span>");
	 
	if($("#map-canvas").length > 0)
	{
		google.maps.event.addDomListener(window, 'load', Catalyze.map.init);
	}
	
	$(".coloumn-body .head").click(function(){
		$(this).next(".coloumn-detail").slideToggle();
		$(this).parent().toggleClass("active");
		
	});
	
	$(".sidemenu-country-wrapper ul li").click(function(){
		Catalyze.map.toggledetailevent();
	});
	
	 $('#media').carousel({
	    pause: true,
	    interval: false,
	  });

	$('.to_tnc').click(function(e){
		e.preventDefault()
		var t = parseInt($("#insta_tnc").offset().top)-120;
		$("html, body").animate({
			scrollTop: t
		}, 350);
	});


});

function gaProcess(param1, param2, param3)
{
	if (window.location.hostname == 'coraltriangleday.org' || window.location.hostname == 'www.coraltriangleday.org')
	{
		_gaq.push(['_trackEvent', param1, param2, param3]);
		//ga('send', 'event', param1, param2, param3);
		//console.log(ga('send', 'event', param1, param2, param3));
	}
}

function getMetaContent(propName) {
	var metas = document.getElementsByTagName('meta');
	var i = 0;
	for (i = 0; i < metas.length; i++) {
		if (metas[i].getAttribute("name") == propName) {
			return metas[i].getAttribute("content");
		}
	}

	return "";
}

$(document).ready(function(){

	var vwidth = 500;

	$('a.socmed-link-share').click(function(e){
		e.preventDefault();
		var ttl = $(this).attr("rel");
		var href;
		var wdesc = getMetaContent("description");

		var wtitle = encodeURI(document.title);

		if (ttl)
		{
			switch(ttl)
			{
				case 'twitter':
					href = 'https://twitter.com/share?text='+wtitle+'&url='+window.location;
					break;

				case 'facebook':
					href = "https://facebook.com/share.php?s=100&p[url]="+window.location+"&p[title]="+wtitle+"&p[summary]="+wdesc;
					break;
			}	
		}

		//var href = $(this).attr("href");
		var x = screen.width/2 - vwidth/2;
		var y = screen.height/2 - vwidth/2;
		window.open(href, '_blank', 'height='+vwidth+',width='+vwidth+',left='+x+',top='+y);
	});

});