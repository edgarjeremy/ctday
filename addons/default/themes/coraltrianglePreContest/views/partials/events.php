<style>

.mCS-light .mCSB_container{
	<!-- min-height:490px!important; -->
	min-height:300px!important;
}

#mCSB_7_container{
	min-height:369px!important;
}

.mCS-dark-thick {
	min-height: 50px!important;
}
</style>

<div id="loading_event_table" style="width:100%; height:389px; background:#fff url({{url:site}}img/loading.gif) no-repeat center center; background-size:64px 64px;"></div>
<div class="row eventlist-wrapper showcontent" style="display:none;">
	<div class="title">Find an event near you!</div>
	<div class="line_after_title">&nbsp;</div>
	<?php if (!empty($events)): ?>
	<div class="table_events">
		<div id="tabs" role="tabpanel">
			<ul class="nav nav-tabs" role="tablist">
				<?php $i = 0; ?>
				<?php foreach($events as $k => $e):  ?>
					<?php $class=''; ?>
					<?php if ($i==0) $class='active'; ?>
					<li class="<?php echo $class; ?>" role="presentation"><a aria-controls="home" data-toggle="tab" href="#<?php echo $k?>" role="tab"><?php echo $countries[$k]; ?></a></li>
					<?php $i++; ?>
				<?php endforeach; ?>
			</ul>
			
			<!-- tab-content starts -->
			<div class="tab-content event-table-inner-content">
				<div class="coloumn-head">
					<div class="coloumn-when">WHEN</div>
					<div class="coloumn-event">EVENTS</div>
					<div class="coloumn-where">WHERE</div>
					<div class="coloumn-city">CITY</div>
				</div>
			<?php $i=0; ?>
			<?php foreach($events as $k => $evt):  ?>
				<?php $class=''; ?>
				<?php if ($i==0) $class='active'; ?>
					
					<div class="tab-pane <?php echo $class ?>" id="<?php echo $k; ?>" role="tabpanel">
					<?php foreach($evt as $idx => $e): ?>

						<!--Start coloumn-->
						<div class="coloumn-body">
							<span></span>
							<div class="head">
								<div class="coloumn-when">
									<?php echo $e['date_from'] ?>
									<?php 
										if (!empty($e['date_to'])) echo '- <br />'.$e['date_to'];
										//if (!empty($e['date_to'])) echo ' - '.$e['date_to'];
									?>
								</div>

								<div class="coloumn-event"><?php echo $e['title'] ?></div>
								<div class="coloumn-where"><?php echo $e['event_venue'] ?></div>
								<div class="coloumn-city"><?php echo $e['location'] ?></div>
							</div>

							<div class="coloumn-detail">
								<p class="description"><?php echo $e['intro'] ?></p>
									
								<?php 
									$venue_address = $e['venue_address'];
									if (empty($venue_address)):
								?>
								<?php else:?>
									<label class="title">VENUE ADDRESS</label>
									<p><?php echo $e['venue_address'] ?></p>
								<?php endif; ?>
									

								<div class="clearfix"></div>

								<!--<p><?php //echo $e['geolocation'] ?></p>

								<div class="clearfix"></div>-->
							<div class="row">
								<?php
									$contact_ = $e['contact_email'];
									if (empty($contact_)):
								?>
								<?php else:?>
									<div class="col-md-6">
										<label class="title">CONTACT</label>
										<?php $link = '<a href="mailto:'. $e['contact_email'] .'"/>'. $e['contact_email'] .'</a>';?>
										<p><?php echo $e['contact_email'] ? $link : '--' ?></p>
									</div>
								<?php endif; ?>
										
								<?php 
									$website_ = $e['website'];
									if (empty($website_)):
								?>
								<?php else:?>
									<div class="col-md-6">
										<label class="title">WEBSITE</label>
										<?php
											if (substr($e['website'], 0, 5) <> 'http:')
												$website = 'http://'.$e['website'];
											else
												$website = $e['website'];
										?>
										<p><a href="<?php echo $website ?>" target="_blank"><?php echo str_ireplace('http://', '', $e['website']) ?></a></p>
									</div>
								<?php endif; ?>
							</div>
							
							<div class="row">
								<?php 
									$organizer_ = $e['organizers'];
									if (empty($organizer_)):
								?>
								<?php else:?>
									<div class="organizer-wrapper col-md-<?php $sponsors_ = $e['sponsors']; if (empty($sponsors_)): echo'12'; else: echo'6'; endif;?>">
										<label class="title">ORGANIZERS</label>
                                            <?php if (!empty($e['organizers'])): ?>
                                            	<?php foreach($e['organizers'] as $organiser): ?>
                                            		<?php if ($organiser->external_url): ?>
	                                            		<?php
															if (substr($organiser->external_url, 0, 5) <> 'http:')
																$organiser->external_url = 'http://'.$organiser->external_url;
	                                            		?>
                                            			<a href="<?php echo $organiser->external_url ?>" target="_blank"><img src="<?php echo UPLOAD_PATH .'galleries/'.$organiser->gallery_id.'/'.thumbnail($organiser->media) ?>" /></a>
                                            		<?php else: ?>
	                                            		<img src="<?php echo UPLOAD_PATH .'galleries/'.$organiser->gallery_id.'/'.thumbnail($organiser->media) ?>" />
	                                            	<?php endif; ?>
                                            	<?php endforeach; ?>
                                            <?php else: ?>
                                            	--
                                            <?php endif; ?>
									</div>
								<?php endif; ?>
										
								<?php 
									$sponsors_ = $e['sponsors'];
									if (empty($sponsors_)):
								?>
								<?php else:?>
									<div class="sponsor-wrapper col-md-<?php $organizer_ = $e['organizers']; if (empty($organizer_)): echo'12'; else: echo'6'; endif;?>">
										<label class="title">SPONSORS</label>
										<?php if (!empty($e['sponsors'])): ?>
											<?php foreach($e['sponsors'] as $sponsor): ?>
											<?php if ($sponsor->external_url): ?>
										<?php if (substr($sponsor->external_url, 0, 5) <> 'http:')
											$sponsor->external_url = 'http://'.$sponsor->external_url;
										?>
										<a href="<?php echo $sponsor->external_url ?>" target="_blank"><img src="<?php echo UPLOAD_PATH .'galleries/'.$sponsor->gallery_id.'/'.thumbnail($sponsor->media) ?>" /></a>
										<?php else: ?>
											<img src="<?php echo UPLOAD_PATH .'galleries/'.$sponsor->gallery_id.'/'.thumbnail($sponsor->media) ?>" />
										<?php endif; ?>
								<?php endforeach; ?>
							<?php else: ?>
								--
							<?php endif; ?>
							</div>
							<?php endif; ?>
							</div>

							</div>
							</div>
							<!--Start coloumn-->

							<?php endforeach; ?>
							</div>

							<?php $i++; ?>
						<?php endforeach; ?>
                        </div>
						<!-- tab-content ends -->
		</div>
	<?php endif; ?>	
</div>

<script>
(function($){
	$(window).load(function(){
							
		$("#tabs").tabs({
			/* show:{effect:"fade",duration:300},
			hide:{effect:"fade",duration:300}, */
			create:function(e,ui){
				/* call mCustomScrollbar function on each tab panel upon tabs creation */
				$(".ui-tabs-panel").mCustomScrollbar({
					setHeight:478,
					theme:"dark-thick"
				});
			}
		});
	});
})(jQuery);
</script>
<script>
	$(window).load(function() {
		$("#loading_event_table").fadeOut(0);
		$('.showcontent').fadeIn(200);	
	});
</script>
{{ theme:js file="jquery-ui.min.js"}}