(function($) {
	$(function(){

		var $container = $('#mcontainer');
		$('#mcontainer .item').imagesLoaded( function() {
			$('#loading').slideUp('fast', function(){
		        $container.fadeIn('slow');
				var options = {itemSelector: ".item"};
				$container.rowGrid(options);
			});
		});

		// rollover effect
		$(".item").livequery(function(){
			$(this).hoverIntent({
				sensitivity: 2,
				over: function(){
					var $meta = $(this).find('.mosaic_meta');
					$meta.fadeIn('fast');
				},
				out: function(){
					var $meta = $(this).find('.mosaic_meta');
					$meta.fadeOut('fast');
				}
			});
		});

	});
})(jQuery);


