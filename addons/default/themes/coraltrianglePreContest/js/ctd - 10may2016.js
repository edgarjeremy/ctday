"use strict";

var Catalyze = Catalyze || {};
var Latlng;

//Google map api
Catalyze.map = Catalyze.Map || {};
Catalyze.map.element;

Catalyze.map.style = [{
						    "featureType": "administrative",
						    "elementType": "labels.text.fill",
						    "stylers": [{
						        "color": "#444444"
						    }]
						}, 
                      {
						    "featureType": "landscape",
						    "elementType": "geometry",
						    "stylers": [{
						        "color": "#f2f2f2"
						    }]
						}, 
                      {
						    "featureType": "poi",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "road",
						    "elementType": "all",
						    "stylers": [{
						        "saturation": -100
						    }, {
						        "lightness": 45
						    }]
						}, {
						    "featureType": "road.highway",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "simplified"
						    }]
						}, {
						    "featureType": "road.arterial",
						    "elementType": "labels.icon",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "transit",
						    "elementType": "all",
						    "stylers": [{
						        "visibility": "off"
						    }]
						}, {
						    "featureType": "water",
						    "elementType": "all",
						    "stylers": [{
						        "color": "#46bcec"
						    }, {
						        "visibility": "on"
						    }]
						}];

Catalyze.map.init = function(){
	Latlng = new google.maps.LatLng(2.05223157397635, 139.55360406249994);
	var mapOptions = {
		styles: Catalyze.map.style,
		zoom: 4,
		scrollwheel: false,
		zoomControlOpt: {
			style : 'SMALL',
			position: 'TOP_LEFT'
		},
		panControl : false,
		center: Latlng
	};

	Catalyze.map.element = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	var KMLOptions = { clickable: false, map: Catalyze.map.element }
    var ctmap = new google.maps.KmlLayer('http://coraltriangleday.org/Coral_Triangle_20111123_2.kml', KMLOptions);

	def_center = Catalyze.map.element.getCenter();
	def_zoom = Catalyze.map.element.getZoom();

	addMarker();
};

Catalyze.map.hideDetail = function(){
	$(".sidemenu-country-wrapper").animate({right:'0'},350);
	$(".sidedetail-event-wrapper").animate({right:'100%'},350);
	//Catalyze.map.toggleMap();
	Catalyze.map.element.setCenter();
};
Catalyze.map.toggleSideMenu = function(){
	Catalyze.map.toggleMap();
	$(".sidemenu-country-wrapper").animate({right:'0'},350);
	$(".sidedetail-event-wrapper").animate({right:'100%'},350);
};
Catalyze.map.toggledetailevent = function(){
	$(".sidemenu-country-wrapper").animate({right:'100%'},350);
	$(".sidedetail-event-wrapper").animate({right:'0'},350);
};

/*
Catalyze.map.toggleSideMenu = function(){
	Catalyze.map.toggleMap();
	$(".sidemenu-country-wrapper").animate({width:'toggle'},350);
};
Catalyze.map.toggledetailevent = function(){
	$(".sidemenu-country-wrapper").animate({width:'toggle'},350);
	$(".sidedetail-event-wrapper").animate({width:'toggle'},350);
};
*/

Catalyze.map.updatedetailevent = function(){
	$(".sidedetail-event-wrapper").animate({width:'toggle'},350);
};

Catalyze.map.toggleMap = function(){
	var sidemenu = $(".sidemenu-country-wrapper").css('display');

	if(sidemenu == "none")
	{
		$('#map-canvas-wrapper').animate({width:'100%'}, 350, function(){
			google.maps.event.trigger(Catalyze.map.element, "resize");
		});	
	}
	else{
		$('#map-canvas-wrapper').animate({width:'80%'}, 350, function(){
			google.maps.event.trigger(Catalyze.map.element, "resize");
            Catalyze.map.element.setCenter(Latlng);
		});	
	}
	infoWindow.close();
	resort_infoWindow.close();
}

Catalyze.map.hidemodal = function(){
    
    
	$(".map-wrapper .modal-map").fadeOut(function(){
		$('.sidemenu-country-wrapper').fadeIn();
	});

	Catalyze.map.toggleSideMenu(function(){
		map_width = $('#map-canvas').css('width');
	});

    
	//$('#map-canvas').animate({width:'70%'}, 350);	
};

$(function(){
	
	 $(".navbar-default .navbar-nav a").append("<span></span>");
	 
	if($("#map-canvas").length > 0)
	{
		google.maps.event.addDomListener(window, 'load', Catalyze.map.init);
	}
	
	$(".coloumn-body .head").click(function(){
		$(this).next(".coloumn-detail").slideToggle();
		$(this).parent().toggleClass("active");
		
	});
	
	 $('#media').carousel({
	    pause: true,
	    interval: false,
	  });
	
	$('.to_tnc').click(function(e){
		e.preventDefault()
		var t = parseInt($("#insta_tnc").offset().top)-105;
		$("html, body").animate({
			scrollTop: t
		}, 350);
	});


});

function gaProcess(param1, param2, param3)
{
	if (window.location.hostname == 'coraltriangleday.org' || window.location.hostname == 'www.coraltriangleday.org')
	{
		_gaq.push(['_trackEvent', param1, param2, param3]);
		//ga('send', 'event', param1, param2, param3);
		//console.log(ga('send', 'event', param1, param2, param3));
	}
}

$(document).ready(function(){
	$('a.get-the-detail-track').click(function(){
		gaProcess('Button', 'Click', 'Get the details');
	});
	$('a.see-all-contest-photos-track').click(function(){
		gaProcess('Button', 'Click', 'See all contest photos');
	});
	$('a.download-promo-kit-track').click(function(){
		gaProcess('Button', 'Click', 'Download promo kit');
	});
	
	
	$('a.follow-facebook-track').click(function(){
		gaProcess('Social media', 'Click', 'Follow Facebook');
	});
	$('a.follow-instagram-track').click(function(){
		gaProcess('Social media', 'Click', 'Follow Instagram');
	});
	$('a.follow-twitter-track').click(function(){
		gaProcess('Social media', 'Click', 'Follow Twitter');
	});
	
	$('a.share-twitter-track').click(function(){
		gaProcess('Social media', 'Click', 'Share Twitter');
	});
	$('a.share-facebook-track').click(function(){
		gaProcess('Social media', 'Click', 'Share Facebook');
	});
	
	$('a.term-and-condition-track').click(function(){
		gaProcess('Hyperlink', 'Click', 'Terms & Conditions');
	});
});

function getMetaContent(propName) {
	var metas = document.getElementsByTagName('meta');
	var i = 0;
	for (i = 0; i < metas.length; i++) {
		if (metas[i].getAttribute("name") == propName) {
			return metas[i].getAttribute("content");
		}
	}

	return "";
}

$(document).ready(function(){

	var vwidth = 500;

	$('a.socmed-link-share').click(function(e){
		e.preventDefault();
		var ttl = $(this).attr("rel");
		var href;
		var wdesc = getMetaContent("description");

		var wtitle = encodeURI(document.title);

		if (ttl)
		{
			switch(ttl)
			{
				case 'twitter':
					href = 'https://twitter.com/share?text='+wtitle+'&url='+window.location;
					break;

				case 'facebook':
					href = "https://facebook.com/share.php?s=100&p[url]="+window.location+"&p[title]="+wtitle+"&p[summary]="+wdesc;
					break;
			}	
		}

		//var href = $(this).attr("href");
		var x = screen.width/2 - vwidth/2;
		var y = screen.height/2 - vwidth/2;
		window.open(href, '_blank', 'height='+vwidth+',width='+vwidth+',left='+x+',top='+y);
	});

});